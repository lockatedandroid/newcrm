package com.android.lockated.crmadmin.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.android.lockated.crmadmin.fragment.AdminDirectorysFragment;
import com.android.lockated.crmadmin.fragment.AdminFacilitiesFragment;
import com.android.lockated.crmadmin.fragment.GateKeeperFragment;
import com.android.lockated.crmadmin.fragment.PendingClassifiedFragment;
import com.android.lockated.crmadmin.fragment.PublishedClassifiedFragment;


public class AdminMyZonePagerAdapter extends FragmentPagerAdapter {

    int tabCount;
    FragmentManager fragmentManager;
    String[] nameList;

    public AdminMyZonePagerAdapter(FragmentManager fm, int tabCount, String[] nameList) {
        super(fm);
        this.tabCount = tabCount;
        fragmentManager = fm;
        this.nameList = nameList;
    }

    @Override
    public Fragment getItem(int pos) {

        Fragment fragment;

        switch (pos) {

            case 0:
                fragment = new PendingClassifiedFragment();
                break;
            case 1:
                fragment = new PublishedClassifiedFragment();
                break;
           /* case 2:
                fragment = new GateKeeperFragment();
                break;*/
            case 2:
                fragment = new AdminFacilitiesFragment();
                break;
            case 3:
                fragment = new AdminDirectorysFragment();
                break;
            default:
                fragment = new PendingClassifiedFragment();
                break;
        }
        return fragment;
    }

    @Override
    public int getCount() {
        return tabCount;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return super.getPageTitle(position);
    }

    @Override
    public int getItemPosition(Object object) {
        return super.getItemPosition(object);
    }

    @Override
    public long getItemId(int position) {
        return super.getItemId(position);
    }

}
