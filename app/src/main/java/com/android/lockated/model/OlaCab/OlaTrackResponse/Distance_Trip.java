
package com.android.lockated.model.OlaCab.OlaTrackResponse;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Distance_Trip {

    @SerializedName("value")
    @Expose
    private float value;
    @SerializedName("unit")
    @Expose
    private String unit;

    /**
     * 
     * @return
     *     The value
     */
    public float getValue() {
        return value;
    }

    /**
     * 
     * @param value
     *     The value
     */
    public void setValue(float value) {
        this.value = value;
    }

    /**
     * 
     * @return
     *     The unit
     */
    public String getUnit() {
        return unit;
    }

    /**
     * 
     * @param unit
     *     The unit
     */
    public void setUnit(String unit) {
        this.unit = unit;
    }

}
