package com.android.lockated.categories.adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;

import com.android.lockated.HomeActivity;
import com.android.lockated.R;
import com.android.lockated.model.userSocieties.UserSociety;
import com.android.lockated.network.ConnectionDetector;
import com.android.lockated.preferences.LockatedPreferences;
import com.android.lockated.request.LockatedRequestError;
import com.android.lockated.request.LockatedVolleyRequestQueue;
import com.android.lockated.utils.ApplicationURL;
import com.android.lockated.utils.Utilities;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;

import org.json.JSONObject;

import java.util.ArrayList;

public class UserSocietyAdapter extends BaseAdapter implements Response.ErrorListener, Response.Listener<JSONObject> {

    Context context;
    ArrayList<UserSociety> userSocietyArrayList;
    LockatedPreferences mLockatedPreferences;
    public static final String USER_SOCIETIES_TAG = "HomeActivityUserSocieties";

    public UserSocietyAdapter(Context context, ArrayList<UserSociety> userSocietyArrayList) {
        this.context = context;
        this.userSocietyArrayList = userSocietyArrayList;
        mLockatedPreferences = new LockatedPreferences(context);
    }

    @Override
    public int getCount() {
        return userSocietyArrayList.size();
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        UserSocietyViewHolder userSocietyViewHolder;
        if (convertView == null) {
            userSocietyViewHolder = new UserSocietyViewHolder();
            convertView = LayoutInflater.from(context).inflate(R.layout.user_societies_list, parent, false);
            convertView.setTag(userSocietyViewHolder);
        } else {
            userSocietyViewHolder = (UserSocietyViewHolder) convertView.getTag();
        }
        userSocietyViewHolder.societyName = (CheckBox) convertView.findViewById(R.id.societyNameText);
        if (userSocietyArrayList.get(position).getSociety().getBuildingName() != null) {
            String name = userSocietyArrayList.get(position).getSociety().getBuildingName();
            userSocietyViewHolder.societyName.setText(name);
            userSocietyViewHolder.societyName.setTag(position);
            userSocietyViewHolder.societyName.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int position = (int) v.getTag();
                    new LockatedPreferences(context).setSocietyId(userSocietyArrayList.get(position).getIdSociety());
                    new LockatedPreferences(context).setUserSocietyId(userSocietyArrayList.get(position).getId());
                    setUserSocietyId(context, userSocietyArrayList.get(position).getId());
                }
            });
        }

        return convertView;
    }

    @Override
    public void onErrorResponse(VolleyError error) {
        if (context != null) {
            LockatedRequestError.onRequestError(context, error);
        }
    }

    @Override
    public void onResponse(JSONObject response) {
        if (context != null) {
            if (response.has("id")) {
                Intent intent = new Intent(context, HomeActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                context.startActivity(intent);
            }
        }
    }

    class UserSocietyViewHolder {
        CheckBox societyName;
    }

    private void setUserSocietyId(Context context, int id) {
        if (ConnectionDetector.isConnectedToInternet(context)) {
            String url = ApplicationURL.setUserSocietyId + mLockatedPreferences.getLockatedToken() + "&user_society_id=" + id;
            LockatedVolleyRequestQueue lockatedVolleyRequestQueue = LockatedVolleyRequestQueue.getInstance(context);
            lockatedVolleyRequestQueue.sendRequest(USER_SOCIETIES_TAG, Request.Method.GET, url, null, this, this);
        } else {
            Utilities.showToastMessage(context, context.getResources().getString(R.string.internet_connection_error));
        }
    }

}
