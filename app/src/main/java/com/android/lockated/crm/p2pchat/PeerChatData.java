package com.android.lockated.crm.p2pchat;

import com.android.lockated.utils.Utilities;

import org.json.JSONObject;

public class PeerChatData {
    public int id;
    public int user_id;
    public int message_count;
    public int conversation_id;
    public String body;
    public String created_at;
    public String updated_at;
    public String agent_name;

    public PeerChatData(int count, JSONObject jsonObject) {

        message_count = count;
        id = jsonObject.optInt("id");
        user_id = jsonObject.optInt("user_id");
        conversation_id = jsonObject.optInt("conversation_id");//chat_id
        body = jsonObject.optString("body");//msg
        //created_at = Utilities.dateConvertToDayMonthYear(jsonObject.optString("showtime"));//showtime
        created_at = Utilities.ddMMYYYFormat(jsonObject.optString("created_at"));//showtime
        updated_at = jsonObject.optString("updated_at");
        agent_name = jsonObject.optString("name_support");//name_support

    }

    public int getMessage_count() {
        return message_count;
    }

    public int getId() {
        return id;
    }

    public int getUser_id() {
        return user_id;
    }

    public int getConversation_id() {
        return conversation_id;
    }

    public String getBody() {
        return body;
    }

    public String getCreated_at() {
        return created_at;
    }

    public String getUpdated_at() {
        return updated_at;
    }

    public String getAgent_name() {
        return agent_name;
    }
}