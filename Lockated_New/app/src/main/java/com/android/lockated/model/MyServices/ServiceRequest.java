
package com.android.lockated.model.MyServices;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ServiceRequest {

    @SerializedName("id")
    @Expose
    private int id;
    @SerializedName("user_id")
    @Expose
    private int userId;
    @SerializedName("category_id")
    @Expose
    private int categoryId;
    @SerializedName("service_metadata_id")
    @Expose
    private int serviceMetadataId;
    @SerializedName("pdate")
    @Expose
    private String pdate;
    @SerializedName("ptime")
    @Expose
    private String ptime;
    @SerializedName("bill_address_id")
    @Expose
    private int billAddressId;
    @SerializedName("service_state")
    @Expose
    private String serviceState;
    @SerializedName("order_id")
    @Expose
    private Object orderId;
    @SerializedName("ship_address_id")
    @Expose
    private int shipAddressId;
    @SerializedName("description")
    @Expose
    private String description;
    @SerializedName("supplier_id")
    @Expose
    private Object supplierId;
    @SerializedName("title")
    @Expose
    private Object title;
    @SerializedName("comment")
    @Expose
    private Object comment;
    @SerializedName("log_name")
    @Expose
    private Object logName;
    @SerializedName("attribute_one")
    @Expose
    private AttributeOne attributeOne;
    @SerializedName("attribute_two")
    @Expose
    private String attributeTwo;
    @SerializedName("attribute_three")
    @Expose
    private String attributeThree;
    @SerializedName("sub_category_id")
    @Expose
    private int subCategoryId;
    @SerializedName("status_updated")
    @Expose
    private Object statusUpdated;
    @SerializedName("created_at")
    @Expose
    private String createdAt;
    @SerializedName("updated_at")
    @Expose
    private String updatedAt;
    @SerializedName("code")
    @Expose
    private int code;
    @SerializedName("prescription")
    @Expose
    private String prescription;
    @SerializedName("medium")
    @Expose
    private String medium;
    @SerializedName("thumb")
    @Expose
    private String thumb;
    @SerializedName("can_cancel")
    @Expose
    private boolean canCancel;
    @SerializedName("state")
    @Expose
    private String state;
    @SerializedName("category")
    @Expose
    private String category;
    @SerializedName("service_name")
    @Expose
    private String serviceName;
    @SerializedName("address")
    @Expose
    private Address address;

    /**
     * @return The id
     */
    public int getId() {
        return id;
    }

    /**
     * @param id The id
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * @return The userId
     */
    public int getUserId() {
        return userId;
    }

    /**
     * @param userId The user_id
     */
    public void setUserId(int userId) {
        this.userId = userId;
    }

    /**
     * @return The categoryId
     */
    public int getCategoryId() {
        return categoryId;
    }

    /**
     * @param categoryId The category_id
     */
    public void setCategoryId(int categoryId) {
        this.categoryId = categoryId;
    }

    /**
     * @return The serviceMetadataId
     */
    public int getServiceMetadataId() {
        return serviceMetadataId;
    }

    /**
     * @param serviceMetadataId The service_metadata_id
     */
    public void setServiceMetadataId(int serviceMetadataId) {
        this.serviceMetadataId = serviceMetadataId;
    }

    /**
     * @return The pdate
     */
    public String getPdate() {
        return pdate;
    }

    /**
     * @param pdate The pdate
     */
    public void setPdate(String pdate) {
        this.pdate = pdate;
    }

    /**
     * @return The ptime
     */
    public String getPtime() {
        return ptime;
    }

    /**
     * @param ptime The ptime
     */
    public void setPtime(String ptime) {
        this.ptime = ptime;
    }

    /**
     * @return The billAddressId
     */
    public int getBillAddressId() {
        return billAddressId;
    }

    /**
     * @param billAddressId The bill_address_id
     */
    public void setBillAddressId(int billAddressId) {
        this.billAddressId = billAddressId;
    }

    /**
     * @return The serviceState
     */
    public String getServiceState() {
        return serviceState;
    }

    /**
     * @param serviceState The service_state
     */
    public void setServiceState(String serviceState) {
        this.serviceState = serviceState;
    }

    /**
     * @return The orderId
     */
    public Object getOrderId() {
        return orderId;
    }

    /**
     * @param orderId The order_id
     */
    public void setOrderId(Object orderId) {
        this.orderId = orderId;
    }

    /**
     * @return The shipAddressId
     */
    public int getShipAddressId() {
        return shipAddressId;
    }

    /**
     * @param shipAddressId The ship_address_id
     */
    public void setShipAddressId(int shipAddressId) {
        this.shipAddressId = shipAddressId;
    }

    /**
     * @return The description
     */
    public String getDescription() {
        return description;
    }

    /**
     * @param description The description
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * @return The supplierId
     */
    public Object getSupplierId() {
        return supplierId;
    }

    /**
     * @param supplierId The supplier_id
     */
    public void setSupplierId(Object supplierId) {
        this.supplierId = supplierId;
    }

    /**
     * @return The title
     */
    public Object getTitle() {
        return title;
    }

    /**
     * @param title The title
     */
    public void setTitle(Object title) {
        this.title = title;
    }

    /**
     * @return The comment
     */
    public Object getComment() {
        return comment;
    }

    /**
     * @param comment The comment
     */
    public void setComment(Object comment) {
        this.comment = comment;
    }

    /**
     * @return The logName
     */
    public Object getLogName() {
        return logName;
    }

    /**
     * @param logName The log_name
     */
    public void setLogName(Object logName) {
        this.logName = logName;
    }

    /**
     * @return The attributeOne
     */
    public AttributeOne getAttributeOne() {
        return attributeOne;
    }

    /**
     * @param attributeOne The attribute_one
     */
    public void setAttributeOne(AttributeOne attributeOne) {
        this.attributeOne = attributeOne;
    }

    /**
     * @return The attributeTwo
     */
    public String getAttributeTwo() {
        return attributeTwo;
    }

    /**
     * @param attributeTwo The attribute_two
     */
    public void setAttributeTwo(String attributeTwo) {
        this.attributeTwo = attributeTwo;
    }

    /**
     * @return The attributeThree
     */
    public String getAttributeThree() {
        return attributeThree;
    }

    /**
     * @param attributeThree The attribute_three
     */
    public void setAttributeThree(String attributeThree) {
        this.attributeThree = attributeThree;
    }

    /**
     * @return The subCategoryId
     */
    public int getSubCategoryId() {
        return subCategoryId;
    }

    /**
     * @param subCategoryId The sub_category_id
     */
    public void setSubCategoryId(int subCategoryId) {
        this.subCategoryId = subCategoryId;
    }

    /**
     * @return The statusUpdated
     */
    public Object getStatusUpdated() {
        return statusUpdated;
    }

    /**
     * @param statusUpdated The status_updated
     */
    public void setStatusUpdated(Object statusUpdated) {
        this.statusUpdated = statusUpdated;
    }

    /**
     * @return The createdAt
     */
    public String getCreatedAt() {
        return createdAt;
    }

    /**
     * @param createdAt The created_at
     */
    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    /**
     * @return The updatedAt
     */
    public String getUpdatedAt() {
        return updatedAt;
    }

    /**
     * @param updatedAt The updated_at
     */
    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    /**
     * @return The code
     */
    public int getCode() {
        return code;
    }

    /**
     * @param code The code
     */
    public void setCode(int code) {
        this.code = code;
    }

    /**
     * @return The prescription
     */
    public String getPrescription() {
        return prescription;
    }

    /**
     * @param prescription The prescription
     */
    public void setPrescription(String prescription) {
        this.prescription = prescription;
    }

    /**
     * @return The medium
     */
    public String getMedium() {
        return medium;
    }

    /**
     * @param medium The medium
     */
    public void setMedium(String medium) {
        this.medium = medium;
    }

    /**
     * @return The thumb
     */
    public String getThumb() {
        return thumb;
    }

    /**
     * @param thumb The thumb
     */
    public void setThumb(String thumb) {
        this.thumb = thumb;
    }

    /**
     * @return The canCancel
     */
    public boolean isCanCancel() {
        return canCancel;
    }

    /**
     * @param canCancel The can_cancel
     */
    public void setCanCancel(boolean canCancel) {
        this.canCancel = canCancel;
    }

    /**
     * @return The state
     */
    public String getState() {
        return state;
    }

    /**
     * @param state The state
     */
    public void setState(String state) {
        this.state = state;
    }

    /**
     * @return The category
     */
    public String getCategory() {
        return category;
    }

    /**
     * @param category The category
     */
    public void setCategory(String category) {
        this.category = category;
    }

    /**
     * @return The serviceName
     */
    public String getServiceName() {
        return serviceName;
    }

    /**
     * @param serviceName The service_name
     */
    public void setServiceName(String serviceName) {
        this.serviceName = serviceName;
    }

    /**
     * @return The address
     */
    public Address getAddress() {
        return address;
    }

    /**
     * @param address The address
     */
    public void setAddress(Address address) {
        this.address = address;
    }

}
