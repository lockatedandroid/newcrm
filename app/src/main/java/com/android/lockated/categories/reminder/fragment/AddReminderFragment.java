package com.android.lockated.categories.reminder.fragment;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.lockated.Interfaces.IReminderAdded;
import com.android.lockated.LockatedApplication;
import com.android.lockated.R;
import com.android.lockated.categories.reminder.model.ReminderData;
import com.android.lockated.network.ConnectionDetector;
import com.android.lockated.preferences.LockatedPreferences;
import com.android.lockated.request.LockatedRequestError;
import com.android.lockated.request.LockatedVolleyRequestQueue;
import com.android.lockated.utils.ApplicationURL;
import com.android.lockated.utils.DatePickerFragment;
import com.android.lockated.utils.TimePickerFragment;
import com.android.lockated.utils.Utilities;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;

import org.json.JSONException;
import org.json.JSONObject;

public class AddReminderFragment extends DialogFragment implements View.OnClickListener, Response.Listener, Response.ErrorListener
{
    private static final String ADD_REQUEST_TAG = "AddReminderFragment";
    private static final String EDIT_REQUEST_TAG = "EditReminderFragment";

    private EditText mEditTextReminderTitle;
    private EditText mEditTextReminderDescription;

    private TextView mTextViewPreferredDeliveryDate;
    private TextView mTextViewPreferredDeliveryTime;

    private ImageView mImageViewDeliveryDate;
    private ImageView mImageViewDeliveryTime;

    private int requestId;
    private int mPosition;
    private ReminderData mReminderDataRow;
    private ProgressDialog mProgressDialog;

    private IReminderAdded mIReminderAdded;

    private LockatedPreferences mLockatedPreferences;
    private LockatedVolleyRequestQueue mLockatedVolleyRequestQueue;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setStyle(STYLE_NO_TITLE, 0);
    }

    @Override
    public void onStart()
    {
        super.onStart();
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(getDialog().getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        getDialog().getWindow().setAttributes(lp);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        View reminderView = inflater.inflate(R.layout.fragment_add_reminder, container);
        init(reminderView);
        return reminderView;
    }

    private void init(View reminderView)
    {
        mLockatedPreferences = new LockatedPreferences(getActivity());
        ((LockatedApplication) getActivity().getApplicationContext()).setmDeliveryTime("");
        ((LockatedApplication) getActivity().getApplicationContext()).setmDeliveryDate("");

        mEditTextReminderTitle = (EditText) reminderView.findViewById(R.id.mEditTextReminderTitle);
        mEditTextReminderDescription = (EditText) reminderView.findViewById(R.id.mEditTextReminderDescription);

        TextView mButtonReminderCancel = (TextView) reminderView.findViewById(R.id.mButtonReminderCancel);
        TextView mButtonReminderSubmit = (TextView) reminderView.findViewById(R.id.mButtonReminderSubmit);
        mTextViewPreferredDeliveryDate = (TextView) reminderView.findViewById(R.id.mTextViewPreferredDeliveryDate);
        mTextViewPreferredDeliveryTime = (TextView) reminderView.findViewById(R.id.mTextViewPreferredDeliveryTime);

        mImageViewDeliveryDate = (ImageView) reminderView.findViewById(R.id.mImageViewDeliveryDate);
        mImageViewDeliveryTime = (ImageView) reminderView.findViewById(R.id.mImageViewDeliveryTime);

        if (requestId == 0)
        {
            mButtonReminderSubmit.setText(R.string.submit_otp);
        }
        else
        {
            mButtonReminderSubmit.setText(R.string.update);
        }

        mButtonReminderCancel.setOnClickListener(this);
        mButtonReminderSubmit.setOnClickListener(this);
        mImageViewDeliveryDate.setOnClickListener(this);
        mImageViewDeliveryTime.setOnClickListener(this);
        mTextViewPreferredDeliveryDate.setOnClickListener(this);
        mTextViewPreferredDeliveryTime.setOnClickListener(this);

        setEditedData();
    }

    private void setEditedData()
    {
        if (requestId == 1)
        {
            mEditTextReminderTitle.setText(mReminderDataRow.getSubject());
            mEditTextReminderDescription.setText(mReminderDataRow.getDescription());

            mTextViewPreferredDeliveryDate.setText("");
            mTextViewPreferredDeliveryTime.setText("");
        }
    }

    public void setReminderDataOnEdit(int id, ReminderData data, int position)
    {
        requestId = id;
        mPosition = position;
        mReminderDataRow = data;
    }

    public void setListener(IReminderAdded listener)
    {
        mIReminderAdded = listener;
    }

    private void onSubmitReminderClicked()
    {
        String strReminderTitle = mEditTextReminderTitle.getText().toString();
        String strReminderDescription = mEditTextReminderDescription.getText().toString();

        String selectedDate = ((LockatedApplication) getActivity().getApplicationContext()).getmDeliveryDate();
        String selectedTime = ((LockatedApplication) getActivity().getApplicationContext()).getmDeliveryTime();

        boolean addReminderCancel = false;

        String message = getActivity().getResources().getString(R.string.signup_blank_field_error);

        if (TextUtils.isEmpty(strReminderTitle))
        {
            addReminderCancel = true;
            message = "Please enter title";
        }

        if (TextUtils.isEmpty(strReminderDescription))
        {
            addReminderCancel = true;
            message = "Please enter description";
        }

        if (selectedTime.equals(""))
        {
            addReminderCancel = true;
            message = "Please select reminder time.";
        }

        if (selectedDate.equals(""))
        {
            addReminderCancel = true;
            message = "Please select reminder date.";
        }

        if (addReminderCancel)
        {
            Utilities.showToastMessage(getActivity(), message);
        }
        else
        {
            if (ConnectionDetector.isConnectedToInternet(getActivity()))
            {
                mProgressDialog = ProgressDialog.show(getActivity(), "", "Please Wait...", false);
                mProgressDialog.show();

                JSONObject jsonObject = new JSONObject();
                JSONObject reminderObject = new JSONObject();

                try
                {
                    reminderObject.put("subject", strReminderTitle);
                    reminderObject.put("description", strReminderDescription);
                    reminderObject.put("date", selectedDate);
                    reminderObject.put("time", selectedTime);
                    jsonObject.put("reminder", reminderObject);

                    if (requestId == 0)
                    {
                        mLockatedVolleyRequestQueue = LockatedVolleyRequestQueue.getInstance(getActivity());
                        mLockatedVolleyRequestQueue.sendRequest(ADD_REQUEST_TAG, Request.Method.POST, ApplicationURL.getReminderDataUrl + mLockatedPreferences.getLockatedToken(), jsonObject, this, this);
                    }
                    else
                    {
                        mLockatedVolleyRequestQueue = LockatedVolleyRequestQueue.getInstance(getActivity());
                        mLockatedVolleyRequestQueue.sendRequest(ADD_REQUEST_TAG, Request.Method.PUT, ApplicationURL.deleteReminderDataUrl + mReminderDataRow.getId() +".json?token=" + mLockatedPreferences.getLockatedToken(), jsonObject, this, this);
                    }
                }
                catch (JSONException e)
                {
                    e.printStackTrace();
                }
            } else
            {
                Utilities.showToastMessage(getActivity(), getActivity().getResources().getString(R.string.internet_connection_error));
            }
        }
    }

    private void openDatePicker()
    {
        DatePickerFragment datePickerFragment = new DatePickerFragment();
        datePickerFragment.setDatePickerView(mTextViewPreferredDeliveryDate);
        DialogFragment newFragment = datePickerFragment;
        newFragment.show(getActivity().getSupportFragmentManager(), "DatePicker");
    }

    private void openTimePicker()
    {
        TimePickerFragment timePickerFragment = new TimePickerFragment();
        timePickerFragment.setTimePickerView(mTextViewPreferredDeliveryTime);
        DialogFragment newFragment = timePickerFragment;
        newFragment.show(getActivity().getSupportFragmentManager(), "TimePicker");
    }

    @Override
    public void onClick(View v)
    {
        switch (v.getId())
        {
            case R.id.mButtonReminderSubmit:
                onSubmitReminderClicked();
                break;

            case R.id.mButtonReminderCancel:
                if (getDialog() != null)
                {
                    getDialog().dismiss();
                }
                break;

            case R.id.mImageViewDeliveryDate:
                openDatePicker();
                break;

            case R.id.mImageViewDeliveryTime:
                openTimePicker();
                break;
        }
    }

    @Override
    public void onErrorResponse(VolleyError error)
    {
        if (getActivity() != null)
        {
            if (mProgressDialog != null)
            {
                mProgressDialog.dismiss();
            }
            LockatedRequestError.onRequestError(getActivity(), error);
        }
    }

    @Override
    public void onResponse(Object response)
    {
        if (getActivity() != null)
        {
            if (mProgressDialog != null)
            {
                mProgressDialog.dismiss();
            }

            if (getDialog() != null)
            {
                getDialog().dismiss();
                JSONObject jsonObject = (JSONObject) response;

                try
                {
                    JSONObject reminderObject = jsonObject.getJSONObject("reminder");
                    ReminderData reminderData = new ReminderData(reminderObject);
                    mIReminderAdded.onReminderUpdated(reminderData, requestId, mPosition);
                }
                catch (JSONException e)
                {
                    e.printStackTrace();
                }
            }
        }
    }
}
