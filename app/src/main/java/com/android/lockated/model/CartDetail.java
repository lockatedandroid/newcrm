package com.android.lockated.model;

import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class CartDetail {
    private int id;
    private String number;
    private String itemTotal;
    private String total;
    private String shipTotal;
    private String adjustmentTotal;
    private String userId;
    private String label;
    private String promoCode;

    private ArrayList<LineItemData> mLineItemData = new ArrayList<>();

    public CartDetail(JSONObject jsonObject) {
        try {
            id = jsonObject.optInt("id");
            number = jsonObject.optString("number");
            itemTotal = jsonObject.optString("item_total");
            total = jsonObject.optString("total");
            shipTotal = jsonObject.optString("ship_total");
            adjustmentTotal = jsonObject.optString("adjustment_total");
            userId = jsonObject.optString("user_id");

            JSONArray lineItemJsonArray = jsonObject.getJSONArray("line_items");
            JSONArray adjustmentJsonArray = jsonObject.getJSONArray("adjustments");
            for (int i = 0; i < lineItemJsonArray.length(); i++) {
                JSONObject lineItemObject = lineItemJsonArray.getJSONObject(i);
                LineItemData lineItemData = new LineItemData(lineItemObject);
                mLineItemData.add(lineItemData);
            }

            for (int i = 0; i < adjustmentJsonArray.length(); i++) {
                JSONObject labelObject = adjustmentJsonArray.getJSONObject(i);
                label = labelObject.optString("label");
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public int getId() {
        return id;
    }

    public String getNumber() {
        return number;
    }

    public String getItemTotal() {
        return itemTotal;
    }

    public String getTotal() {
        return total;
    }

    public String getLabel() {
        return label;
    }

    public String getShipTotal() {
        return shipTotal;
    }

    public String getUserId() {
        return userId;
    }

    public void setPromoCode(String promoCode) {
        this.promoCode = promoCode;
    }

    public String getPromoCode() {
        return promoCode;
    }

    public String getAdjustmentTotal() {
        return adjustmentTotal;
    }

    public ArrayList<LineItemData> getmLineItemData() {
        return mLineItemData;
    }
}
