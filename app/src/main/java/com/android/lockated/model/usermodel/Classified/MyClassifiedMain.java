
package com.android.lockated.model.usermodel.Classified;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class MyClassifiedMain {

    @SerializedName("id")
    @Expose
    private int id;
    @SerializedName("purpose")
    @Expose
    private String purpose;
    @SerializedName("title")
    @Expose
    private String title;
    @SerializedName("description")
    @Expose
    private String description;
    @SerializedName("price")
    @Expose
    private String price;
    @SerializedName("expire")
    @Expose
    private String expire;
    @SerializedName("publish")
    @Expose
    private Object publish;
    @SerializedName("classified_category")
    @Expose
    private String classifiedCategory;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("medium")
    @Expose
    private String medium;
    @SerializedName("thumb")
    @Expose
    private String thumb;
    @SerializedName("original")
    @Expose
    private String original;
    @SerializedName("user")
    @Expose
    private MyUser user;

    /**
     * 
     * @return
     *     The id
     */
    public int getId() {
        return id;
    }

    /**
     * 
     * @param id
     *     The id
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * 
     * @return
     *     The purpose
     */
    public String getPurpose() {
        return purpose;
    }

    /**
     * 
     * @param purpose
     *     The purpose
     */
    public void setPurpose(String purpose) {
        this.purpose = purpose;
    }

    /**
     * 
     * @return
     *     The title
     */
    public String getTitle() {
        return title;
    }

    /**
     * 
     * @param title
     *     The title
     */
    public void setTitle(String title) {
        this.title = title;
    }

    /**
     * 
     * @return
     *     The description
     */
    public String getDescription() {
        return description;
    }

    /**
     * 
     * @param description
     *     The description
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * 
     * @return
     *     The price
     */
    public String getPrice() {
        return price;
    }

    /**
     * 
     * @param price
     *     The price
     */
    public void setPrice(String price) {
        this.price = price;
    }

    /**
     * 
     * @return
     *     The expire
     */
    public String getExpire() {
        return expire;
    }

    /**
     * 
     * @param expire
     *     The expire
     */
    public void setExpire(String expire) {
        this.expire = expire;
    }

    /**
     * 
     * @return
     *     The publish
     */
    public Object getPublish() {
        return publish;
    }

    /**
     * 
     * @param publish
     *     The publish
     */
    public void setPublish(Object publish) {
        this.publish = publish;
    }

    /**
     * 
     * @return
     *     The classifiedCategory
     */
    public String getClassifiedCategory() {
        return classifiedCategory;
    }

    /**
     * 
     * @param classifiedCategory
     *     The classified_category
     */
    public void setClassifiedCategory(String classifiedCategory) {
        this.classifiedCategory = classifiedCategory;
    }

    /**
     * 
     * @return
     *     The status
     */
    public String getStatus() {
        return status;
    }

    /**
     * 
     * @param status
     *     The status
     */
    public void setStatus(String status) {
        this.status = status;
    }

    /**
     * 
     * @return
     *     The medium
     */
    public String getMedium() {
        return medium;
    }

    /**
     * 
     * @param medium
     *     The medium
     */
    public void setMedium(String medium) {
        this.medium = medium;
    }

    /**
     * 
     * @return
     *     The thumb
     */
    public String getThumb() {
        return thumb;
    }

    /**
     * 
     * @param thumb
     *     The thumb
     */
    public void setThumb(String thumb) {
        this.thumb = thumb;
    }

    /**
     * 
     * @return
     *     The original
     */
    public String getOriginal() {
        return original;
    }

    /**
     * 
     * @param original
     *     The original
     */
    public void setOriginal(String original) {
        this.original = original;
    }

    /**
     * 
     * @return
     *     The user
     */
    public MyUser getUser() {
        return user;
    }

    /**
     * 
     * @param user
     *     The user
     */
    public void setUser(MyUser user) {
        this.user = user;
    }

}
