package com.android.lockated.crmadmin.adapter;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.android.lockated.R;
import com.android.lockated.crmadmin.activity.AdminEventDetailActivity;
import com.android.lockated.crmadmin.activity.AdminEventsActivity;
import com.android.lockated.model.AdminModel.AdminEvents.Classified;
import com.android.lockated.network.ConnectionDetector;
import com.android.lockated.preferences.LockatedPreferences;
import com.android.lockated.request.LockatedRequestError;
import com.android.lockated.request.LockatedVolleyRequestQueue;
import com.android.lockated.utils.ApplicationURL;
import com.android.lockated.utils.Utilities;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class PendingEventAdapter extends RecyclerView.Adapter<PendingEventAdapter.PendingEventViewHolder> implements Response.ErrorListener, Response.Listener<JSONObject> {

    public static final String REQUEST_TAG = "PendingEventAdapter";
    private final ArrayList<Classified> classifiedArrayList;
    private final String CREATE;
    Context context;
    ProgressDialog mProgressDialog;
    JSONArray jsonArray;
    private RequestQueue mQueue;
    private LockatedPreferences mLockatedPreferences;

    public PendingEventAdapter(Context context, String CREATE, ArrayList<Classified> classifiedArrayList) {
        this.context = context;
        this.CREATE = CREATE;
        this.classifiedArrayList = classifiedArrayList;
    }

    @Override
    public PendingEventViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(context).inflate(R.layout.admin_pending_notice, parent, false);
        PendingEventViewHolder pvh = new PendingEventViewHolder(v);
        return pvh;
    }

    @Override
    public void onBindViewHolder(PendingEventViewHolder holder, final int position) {
        String firstName, lastName;
        mLockatedPreferences = new LockatedPreferences(context);
        holder.subject.setText(classifiedArrayList.get(position).getEventName());
        holder.description.setText(classifiedArrayList.get(position).getDescription());
        //holder.mFlat.setText(/*jsonArray.getJSONObject(position).getJSONObject("user").getString("firstname")*/"No Flat");
        holder.mFlat.setVisibility(View.INVISIBLE);
        holder.mFlatText.setVisibility(View.INVISIBLE);
        holder.submittedOn.setText(Utilities.ddMMYYYFormat(classifiedArrayList.get(position).getCreatedAt()));
        if (classifiedArrayList.get(position).getUser() != null) {
            firstName = classifiedArrayList.get(position).getUser().getFirstname();
            lastName = classifiedArrayList.get(position).getUser().getLastname();
        } else {
            firstName = "No";
            lastName = "name";
        }
        holder.postedName.setText(firstName + " " + lastName);
        holder.publishNotice.setTag(position);
        holder.publishNotice.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int pos = (int) v.getTag();
                String url = ApplicationURL.publishEvent + mLockatedPreferences.getLockatedToken()
                        + "&id=" + classifiedArrayList.get(pos).getId();
                request(null, url);
            }
        });
        holder.rejectNotice.setTag(position);
        holder.rejectNotice.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int pos = (int) v.getTag();
                String url = ApplicationURL.denyEvent + mLockatedPreferences.getLockatedToken()
                        + "&id=" + classifiedArrayList.get(pos).getId();
                request(null, url);
            }
        });
        holder.detailView.setTag(position);
        holder.detailView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int pos = (int) v.getTag();
                viewCardDetail(context, classifiedArrayList.get(pos));
            }
        });
    }

    public void viewCardDetail(Context context, Classified classified) {
        JSONObject jsonObjectMain = new JSONObject();
        String username, updatedDate;
        try {
            jsonObjectMain.put("event_type", classified.getEventType());
            if (classified.getUser() != null) {
                //   Log.e("Pending flat",""+jsonObject.getJSONObject("user").getString("flat"));
                username = classified.getUser().getFirstname() + " " + classified.getUser().getLastname();
            } else {
                username = "No Name";
            }
            jsonObjectMain.put("username", username);
            jsonObjectMain.put("flat", "No Flat");
            jsonObjectMain.put("event_name", classified.getEventName());
            jsonObjectMain.put("description", classified.getDescription());
            jsonObjectMain.put("event_at", classified.getEventAt());
            jsonObjectMain.put("from_time", classified.getFromTime());
            jsonObjectMain.put("to_time", classified.getToTime());
            jsonObjectMain.put("created_at", classified.getCreatedAt());
            jsonObjectMain.put("id", "" + classified.getId());
            jsonObjectMain.put("rsvp", "null");
            if (classified.getCreatedAt() != null) {
                updatedDate = classified.getCreatedAt();
            } else {
                updatedDate = "No Date";
            }
            jsonObjectMain.put("updated_at", updatedDate);
            if (classified.getDocumentsEvent().size() > 0 && classified.getDocumentsEvent() != null) {
                jsonObjectMain.put("document", "" + classified.getDocumentsEvent().get(0).getDocument());
            } else {
                jsonObjectMain.put("document", "No Document");
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        Intent allNoticeDetailIntent = new Intent(context, AdminEventDetailActivity.class);
        allNoticeDetailIntent.putExtra("EventDetailViewData", jsonObjectMain.toString());
        allNoticeDetailIntent.putExtra("pendin_publish_value", "1");
        context.startActivity(allNoticeDetailIntent);
    }

    @Override
    public int getItemCount() {
        return classifiedArrayList.size();
        //jsonArray.length();
    }


    @Override
    public void onErrorResponse(VolleyError error) {
        mProgressDialog.dismiss();
        if (context != null) {
            LockatedRequestError.onRequestError(context, error);
        }
    }

    @Override
    public void onResponse(JSONObject response) {
        mProgressDialog.dismiss();
        if (context != null) {
            try {
                if (response != null && response.length() > 0) {
                    if (response.has("id")) {
                        Intent intent = new Intent(context, AdminEventsActivity.class);
                        intent.putExtra("AdminEvents", 0);
                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        context.startActivity(intent);
                    }
                } else {
                }

            } catch (Exception e) {
                e.printStackTrace();
            }
        }

    }

    public void request(JSONObject jsonObject, String url) {
        mProgressDialog = ProgressDialog.show(context, "", "Please Wait...", false);
        mProgressDialog.show();
        if (context != null) {
            if (ConnectionDetector.isConnectedToInternet(context)) {
                mLockatedPreferences = new LockatedPreferences(context);
                String urlMain = url;
                LockatedVolleyRequestQueue lockatedVolleyRequestQueue = LockatedVolleyRequestQueue.getInstance(context);
                lockatedVolleyRequestQueue.sendRequest(REQUEST_TAG, Request.Method.GET, urlMain, jsonObject, this, this);
                /*mQueue = LockatedVolleyRequestQueue.getInstance(context).getRequestQueue();
                LockatedJSONObjectRequest lockatedJSONObjectRequest = new LockatedJSONObjectRequest(Request.Method.GET,
                        url, jsonObject, this, this);
                lockatedJSONObjectRequest.setTag(REQUEST_TAG);
                mQueue.add(lockatedJSONObjectRequest);*/
            } else {
                Utilities.showToastMessage(context, context.getResources().getString(R.string.internet_connection_error));
            }
        }

    }

    public class PendingEventViewHolder extends RecyclerView.ViewHolder {

        CardView cardView;
        TextView subject, description, submittedOn, postedName, publishNotice, rejectNotice, detailView, mFlat, mFlatText;

        public PendingEventViewHolder(View itemView) {
            super(itemView);

            subject = (TextView) itemView.findViewById(R.id.subjectName1);
            mFlat = (TextView) itemView.findViewById(R.id.mFlat);
            mFlatText = (TextView) itemView.findViewById(R.id.flatText);
            description = (TextView) itemView.findViewById(R.id.postedName1);
            submittedOn = (TextView) itemView.findViewById(R.id.submissionDate);
            postedName = (TextView) itemView.findViewById(R.id.postedBy);
            publishNotice = (TextView) itemView.findViewById(R.id.publish);
            rejectNotice = (TextView) itemView.findViewById(R.id.reject);
            detailView = (TextView) itemView.findViewById(R.id.detailView);

        }

    }

}
