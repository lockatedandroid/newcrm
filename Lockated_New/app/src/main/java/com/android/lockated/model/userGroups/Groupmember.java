
package com.android.lockated.model.userGroups;

import android.os.Parcel;
import android.os.Parcelable;

import com.android.lockated.model.usermodel.myGroups.Society;
import com.android.lockated.model.usermodel.myGroups.UserFlat;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


public class Groupmember implements Parcelable {

    @SerializedName("id")
    @Expose
    private int id;
    @SerializedName("user_id")
    @Expose
    private int userId;
    @SerializedName("usergroup_id")
    @Expose
    private int usergroupId;
    @SerializedName("active")
    @Expose
    private int active;
    @SerializedName("user")
    @Expose
    private User user;

    /**
     * @return The id
     */
    public int getId() {
        return id;
    }

    /**
     * @param id The id
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * @return The userId
     */
    public int getUserId() {
        return userId;
    }

    /**
     * @param userId The user_id
     */
    public void setUserId(int userId) {
        this.userId = userId;
    }

    /**
     * @return The usergroupId
     */
    public int getUsergroupId() {
        return usergroupId;
    }

    /**
     * @param usergroupId The usergroup_id
     */
    public void setUsergroupId(int usergroupId) {
        this.usergroupId = usergroupId;
    }

    /**
     * @return The active
     */
    public int getActive() {
        return active;
    }

    /**
     * @param active The active
     */
    public void setActive(int active) {
        this.active = active;
    }

    /**
     * @return The user
     */
    public User getUser() {
        return user;
    }

    /**
     * @param user The user
     */
    public void setUser(User user) {
        this.user = user;
    }

    @Override
    public int describeContents() {
        return this.hashCode();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(id);
        dest.writeInt(userId);
        dest.writeInt(usergroupId);
        dest.writeInt(active);
        dest.writeParcelable(user, flags);
    }

    private Groupmember(Parcel in) {
        id = in.readInt();
        userId = in.readInt();
        usergroupId = in.readInt();
        active = in.readInt();
        user = in.readParcelable(com.android.lockated.model.usermodel.myGroups.User.class.getClassLoader());
    }

    public static final Parcelable.Creator<Groupmember> CREATOR = new Parcelable.Creator<Groupmember>() {
        public Groupmember createFromParcel(Parcel in) {
            return new Groupmember(in);
        }

        public Groupmember[] newArray(int size) {
            return new Groupmember[size];
        }
    };

}
