package com.android.lockated.crm.fragment.myzone;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.android.lockated.LockatedApplication;
import com.android.lockated.R;
import com.android.lockated.crm.adapters.ExpectedVisitorInAdapter;
import com.android.lockated.crm.fragment.myzone.gatekeeper.Add_New_Visitor;
import com.android.lockated.model.expectedVisitor.GatekeeperExpected;
import com.android.lockated.network.ConnectionDetector;
import com.android.lockated.preferences.LockatedPreferences;
import com.android.lockated.request.LockatedJSONObjectRequest;
import com.android.lockated.request.LockatedRequestError;
import com.android.lockated.request.LockatedVolleyRequestQueue;
import com.android.lockated.utils.ApplicationURL;
import com.android.lockated.utils.Utilities;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class VisitorCornerFragment extends Fragment implements Response.ErrorListener, Response.Listener<JSONObject>, View.OnClickListener {

    public static final String REQUEST_TAG = "VisitorCornerFragment";
    View VisitorCornerFragmentView;
    FragmentManager fragmentManager;
    ListView visitorlistView;
    TextView errorMsg;
    boolean isFabOpen = false;
    JSONObject visitorJsonObj;
    String create, update, destroy, show, userType;
    ExpectedVisitorInAdapter visitorAdapter;
    ProgressBar progressBar;
    ArrayList<GatekeeperExpected> gatekeeper_adminModel;
    String SHOW, CREATE, INDEX, UPDATE, EDIT, DESTROY;
    private Animation fab_open, fab_close, rotate_forward, rotate_backward;
    private FloatingActionButton fab;
    private RequestQueue mQueue;
    private LockatedPreferences mLockatedPreferences;

    public FragmentManager setFragmentManager(FragmentManager fragmentManager) {
        this.fragmentManager = fragmentManager;
        return fragmentManager;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        VisitorCornerFragmentView = inflater.inflate(R.layout.fragment_visitor_corner, container, false);
        init(VisitorCornerFragmentView);
        return VisitorCornerFragmentView;
    }

    @Override
    public void onResume() {
        super.onResume();
        LockatedApplication.getInstance().trackScreenView(getString(R.string.visitorCorner));
        Utilities.lockatedGoogleAnalytics(getString(R.string.visitorCorner),
                getString(R.string.visited), getString(R.string.visitorCorner));
        visitorAdapter.notifyDataSetChanged();
        // gatekeeper_adminModel.clear();
        //    getVisitors();
    }

    public void init(View visitorCornerFragmentView) {
        gatekeeper_adminModel = new ArrayList<>();
        mLockatedPreferences = new LockatedPreferences(getActivity());
        progressBar = (ProgressBar) visitorCornerFragmentView.findViewById(R.id.mProgressBarView);
        visitorlistView = (ListView) visitorCornerFragmentView.findViewById(R.id.listView);
        errorMsg = (TextView) visitorCornerFragmentView.findViewById(R.id.noNotices);
        fab = (FloatingActionButton) visitorCornerFragmentView.findViewById(R.id.fab);
        fabAnimation();
        fab.setOnClickListener(this);
/*        try {

            visitorJsonObj = new JSONObject(mLockatedPreferences.getRolesJson());
            userType = visitorJsonObj.getString("name");
            if (!userType.equals("admin") || !userType.equals("society_admin")) {
                show = visitorJsonObj.getJSONArray("permissions").getJSONObject(8).getJSONObject("permission").getString("show");
                destroy = visitorJsonObj.getJSONArray("permissions").getJSONObject(8).getJSONObject("permission").getString("destroy");
            }
        } catch (JSONException e) {
            e.printStackTrace();
            create = "false";
            update = "false";
            show = "false";
            destroy = "false";
        }*/
        getGatekeeperRole();
        checkPermission();

    }

    private void fabAnimation() {
        fab_open = AnimationUtils.loadAnimation(getActivity(), R.anim.fab_open);
        fab_close = AnimationUtils.loadAnimation(getActivity(), R.anim.fab_close);
        rotate_forward = AnimationUtils.loadAnimation(getActivity(), R.anim.rotate_forward);
        rotate_backward = AnimationUtils.loadAnimation(getActivity(), R.anim.rotate_backward);
    }

    private void checkPermission() {
        if (SHOW != null && SHOW.equals("true")) {
            getVisitors();
        } else {
            visitorlistView.setVisibility(View.GONE);
            errorMsg.setVisibility(View.VISIBLE);
            errorMsg.setText(R.string.no_permission_error);
        }
    }

    private void getVisitors() {
        progressBar.setVisibility(View.VISIBLE);
        if (getActivity() != null) {
            if (!mLockatedPreferences.getSocietyId().equals("blank")) {
                if (ConnectionDetector.isConnectedToInternet(getActivity())) {
                    String url = ApplicationURL.getVisitorUrl + mLockatedPreferences.getLockatedToken()
                            + ApplicationURL.societyId + mLockatedPreferences.getSocietyId();
                    mLockatedPreferences = new LockatedPreferences(getActivity());
                    mQueue = LockatedVolleyRequestQueue.getInstance(getActivity()).getRequestQueue();
                    //Log.e("url get visitor",""+url);
                    LockatedJSONObjectRequest lockatedJSONObjectRequest = new LockatedJSONObjectRequest(Request.Method.GET,
                            url, null, this, this);
                    lockatedJSONObjectRequest.setTag(REQUEST_TAG);
                    mQueue.add(lockatedJSONObjectRequest);
                } else {
                    Utilities.showToastMessage(getActivity(), getActivity().getResources().getString(R.string.internet_connection_error));
                }
            } else {
                visitorlistView.setVisibility(View.GONE);
                errorMsg.setVisibility(View.VISIBLE);
                errorMsg.setText(R.string.not_in_society);
            }
        }

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.fab:
                animateFAB();
                Add_New_Visitor eventNoticePreview = new Add_New_Visitor();
                eventNoticePreview.show(getActivity().getSupportFragmentManager(), "PreviewDialog");
                break;
        }
    }

    private void animateFAB() {
        if (isFabOpen) {
            fab.startAnimation(rotate_backward);
            isFabOpen = false;
        } else {
            fab.startAnimation(rotate_forward);
            isFabOpen = true;
        }
    }

    @Override
    public void onErrorResponse(VolleyError error) {
        progressBar.setVisibility(View.GONE);
        if (getActivity() != null) {
            LockatedRequestError.onRequestError(getActivity(), error);
        }
    }

    @Override
    public void onResponse(JSONObject response) {
        //Log.e("",""+response);
        progressBar.setVisibility(View.GONE);
        if (getActivity() != null) {
            try {
                if (response != null && response.has("gatekeepers") && response.getJSONArray("gatekeepers").length() > 0) {
                    JSONArray jsonArray = response.getJSONArray("gatekeepers");
                    Gson gson = new Gson();
                    for (int i = 0; i < jsonArray.length(); i++) {
                        GatekeeperExpected gatekeeper = gson.fromJson(jsonArray.getJSONObject(i).toString(), GatekeeperExpected.class);
                        gatekeeper_adminModel.add(gatekeeper);
                    }
                    visitorAdapter = new ExpectedVisitorInAdapter(getActivity(), gatekeeper_adminModel,getChildFragmentManager()/*, EDIT, DESTROY*/);
                    visitorlistView.setAdapter(visitorAdapter);
                    visitorAdapter.notifyDataSetChanged();
                } else {
                    visitorlistView.setVisibility(View.GONE);
                    errorMsg.setVisibility(View.VISIBLE);
                    errorMsg.setText(R.string.no_data_error);
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    public String getGatekeeperRole() {
        String mySocietyRoles = getActivity().getResources().getString(R.string.blank_value);
        if (mLockatedPreferences.getRolesJson() != null && !mLockatedPreferences.getRolesJson().equals("blank")) {
            try {
                JSONObject noticeJsonObj = new JSONObject(mLockatedPreferences.getRolesJson());
                for (int i = 0; i < noticeJsonObj.getJSONArray(getActivity().getResources().getString(R.string.permissions_value)).length(); i++) {
                    JSONObject jsonObject1 = noticeJsonObj.getJSONArray(getActivity().getResources().getString(R.string.permissions_value)).getJSONObject(i);
                    if (jsonObject1.has(getActivity().getResources().getString(R.string.section_value))
                            && jsonObject1.getString(getActivity().getResources().getString(R.string.section_value)).equals("spree_visitors")) {
                        if (jsonObject1.getJSONObject(getActivity().getResources().getString(R.string.permission_value)).has(getActivity().getResources().getString(R.string.index_value))) {
                            mySocietyRoles = jsonObject1.getJSONObject(getActivity().getResources().getString(R.string.permission_value)).getString(getActivity().getResources().getString(R.string.index_value));
                            CREATE = jsonObject1.getJSONObject(getActivity().getResources().getString(R.string.permission_value)).getString("create");
                            INDEX = jsonObject1.getJSONObject(getActivity().getResources().getString(R.string.permission_value)).getString("index");
                            UPDATE = jsonObject1.getJSONObject(getActivity().getResources().getString(R.string.permission_value)).getString("update");
                            EDIT = jsonObject1.getJSONObject(getActivity().getResources().getString(R.string.permission_value)).getString("edit");
                            SHOW = jsonObject1.getJSONObject(getActivity().getResources().getString(R.string.permission_value)).getString("show");
                            DESTROY = jsonObject1.getJSONObject(getActivity().getResources().getString(R.string.permission_value)).getString("destroy");
                        }
                    }
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return mySocietyRoles;
    }
}
