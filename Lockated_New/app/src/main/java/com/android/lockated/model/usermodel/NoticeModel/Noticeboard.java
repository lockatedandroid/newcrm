package com.android.lockated.model.usermodel.NoticeModel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class Noticeboard {

    @SerializedName("date")
    @Expose
    private String date;
    @SerializedName("notices")
    @Expose
    private ArrayList<Notice> notices = new ArrayList<Notice>();

    public Noticeboard(JSONObject jsonObject) {

        try {
            date = jsonObject.optString("date");
            JSONArray jsonArray = jsonObject.getJSONArray("notices");
            for (int i = 0; i < jsonArray.length(); i++) {
                Notice notice = new Notice(jsonArray.getJSONObject(i));
                notices.add(notice);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public ArrayList<Notice> getNotices() {
        return notices;
    }

    public void setNotices(ArrayList<Notice> notices) {
        this.notices = notices;
    }

}