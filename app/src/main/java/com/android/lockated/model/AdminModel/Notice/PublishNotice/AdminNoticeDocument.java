
package com.android.lockated.model.AdminModel.Notice.PublishNotice;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class AdminNoticeDocument {

    @SerializedName("id")
    @Expose
    private int id;
    @SerializedName("relation")
    @Expose
    private String relation;
    @SerializedName("relation_id")
    @Expose
    private int relationId;
    @SerializedName("document")
    @Expose
    private String document;
    @SerializedName("doctype")
    @Expose
    private String doctype;

    /**
     * 
     * @return
     *     The id
     */
    public int getId() {
        return id;
    }

    /**
     * 
     * @param id
     *     The id
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * 
     * @return
     *     The relation
     */
    public String getRelation() {
        return relation;
    }

    /**
     * 
     * @param relation
     *     The relation
     */
    public void setRelation(String relation) {
        this.relation = relation;
    }

    /**
     * 
     * @return
     *     The relationId
     */
    public int getRelationId() {
        return relationId;
    }

    /**
     * 
     * @param relationId
     *     The relation_id
     */
    public void setRelationId(int relationId) {
        this.relationId = relationId;
    }

    /**
     * 
     * @return
     *     The document
     */
    public String getDocument() {
        return document;
    }

    /**
     * 
     * @param document
     *     The document
     */
    public void setDocument(String document) {
        this.document = document;
    }

    /**
     * 
     * @return
     *     The doctype
     */
    public String getDoctype() {
        return doctype;
    }

    /**
     * 
     * @param doctype
     *     The doctype
     */
    public void setDoctype(String doctype) {
        this.doctype = doctype;
    }

}
