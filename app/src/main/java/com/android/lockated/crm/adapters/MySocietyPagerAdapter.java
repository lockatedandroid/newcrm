package com.android.lockated.crm.adapters;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.android.lockated.crm.fragment.AboutSocietyFragment;
import com.android.lockated.crm.fragment.SocietyCommitteeListFragment;
import com.android.lockated.crm.fragment.events.UserEventsFragment;

import com.android.lockated.crm.fragment.gallery.MyGalleryFragment;
import com.android.lockated.crm.fragment.groups.MyGroupFragment;
import com.android.lockated.crm.fragment.notices.AllNoticeFragment;
import com.android.lockated.crm.fragment.CrmBillPaymentFragment;
import com.android.lockated.crm.fragment.CrmMyZoneFragment;
import com.android.lockated.crm.fragment.polls.CrmPollListFragment;
import com.android.lockated.crm.fragment.HelpDeskFragment;


public class MySocietyPagerAdapter extends FragmentPagerAdapter {

    int tabCount, itemPosition;
    FragmentManager fragmentManager;
    String[] nameList;

    public MySocietyPagerAdapter(FragmentManager fm, int tabCount, int itemPosition, String[] nameList) {
        super(fm);
        this.tabCount = tabCount;
        fragmentManager = fm;
        this.itemPosition = itemPosition;
        this.nameList = nameList;
    }

    @Override
    public Fragment getItem(int pos) {

        Fragment fragment;

        switch (pos) {

            case 0:
                fragment = new AllNoticeFragment();
                break;
            case 1:
                fragment = new UserEventsFragment();
                break;
            case 2:
                fragment = new CrmPollListFragment();
                break;
            case 3:
                fragment = new HelpDeskFragment();
                break;
            case 4:
                fragment = new CrmMyZoneFragment();
                break;
            case 5:
                fragment = new CrmBillPaymentFragment();
                break;
            case 6:
                fragment = new AboutSocietyFragment();
                break;
            case 7:
                fragment = new SocietyCommitteeListFragment();
                break;
            case 8:
                fragment = new MyGroupFragment();
                break;
            case 9:
                fragment = new MyGalleryFragment();
                break;
            default:
                fragment = new AllNoticeFragment();
                break;
        }
        return fragment;
    }

    @Override
    public int getCount() {
        return tabCount;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return nameList[itemPosition];
    }

    @Override
    public int getItemPosition(Object object) {
        return super.getItemPosition(object);
    }

    @Override
    public long getItemId(int position) {
        return super.getItemId(position);
    }

}
