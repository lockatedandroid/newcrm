package com.android.lockated.search;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.android.lockated.Interfaces.IRecyclerItemClickListener;
import com.android.lockated.LockatedApplication;
import com.android.lockated.R;
import com.android.lockated.cart.ecommerce.EcommerceCartActivity;
import com.android.lockated.categories.adapter.VendorAdapter;
import com.android.lockated.categories.vendors.fragment.VendorMenuFragment;
import com.android.lockated.categories.vendors.model.VendorData;
import com.android.lockated.information.AccountController;
import com.android.lockated.model.AccountData;
import com.android.lockated.model.CartDetail;
import com.android.lockated.network.ConnectionDetector;
import com.android.lockated.preferences.LockatedPreferences;
import com.android.lockated.request.LockatedRequestError;
import com.android.lockated.request.LockatedVolleyRequestQueue;
import com.android.lockated.utils.ApplicationURL;
import com.android.lockated.utils.Utilities;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class VendorSearchActivity extends AppCompatActivity implements View.OnClickListener, IRecyclerItemClickListener, Response.Listener, Response.ErrorListener
{
    private ProgressBar mProgressBarView;

    private EditText mEditTextSearchQuery;
    private TextView mTextViewSearchQuery;

    private int mTaxonId;
    private int mTaxonomyId;

    private ArrayList<VendorData> mVendorData;
    private VendorAdapter mVendorAdapter;

    private LockatedPreferences mLockatedPreferences;

    private static final String VENDOR_SEARCH_TAG = "VendorSearch";

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_vendor_search);

//        ((VendorActivity) getActivity()).toolbar.setVisibility(View.GONE);

        Toolbar toolbar = (Toolbar) findViewById(R.id.mSearchToolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(true);
        getSupportActionBar().setTitle("");

        init();
        setListener();
    }

    private void init()
    {
        mVendorData = new ArrayList<>();
        mLockatedPreferences = new LockatedPreferences(VendorSearchActivity.this);

        mTaxonId = ((LockatedApplication) getApplicationContext()).getTaxonId();
        mTaxonomyId = ((LockatedApplication) getApplicationContext()).getTaxonomyId();

        mEditTextSearchQuery = (EditText) findViewById(R.id.mEditTextSearchQuery);
        mTextViewSearchQuery = (TextView) findViewById(R.id.mTextViewSearchQuery);

        mProgressBarView = (ProgressBar) findViewById(R.id.mProgressBarView);
        RecyclerView mRecyclerView = (RecyclerView) findViewById(R.id.mRecyclerView);

        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(VendorSearchActivity.this);
        mRecyclerView.setLayoutManager(mLayoutManager);
        mRecyclerView.setHasFixedSize(true);

        mTextViewSearchQuery.setOnClickListener(this);

        mVendorAdapter = new VendorAdapter(mVendorData, this);
        mRecyclerView.setAdapter(mVendorAdapter);
    }

    private void setListener()
    {
        mEditTextSearchQuery.addTextChangedListener(new TextWatcher()
        {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after)
            {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count)
            {
                if (s.length() > 0)
                {
                    mEditTextSearchQuery.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_edit_text_clear, 0);
                } else
                {
                    mEditTextSearchQuery.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
                }
            }

            @Override
            public void afterTextChanged(Editable s)
            {

            }
        });

        mEditTextSearchQuery.setOnTouchListener(new View.OnTouchListener()
        {
            @Override
            public boolean onTouch(View v, MotionEvent event)
            {
                final int DRAWABLE_RIGHT = 2;
                if (event.getAction() == MotionEvent.ACTION_UP)
                {
                    if (mEditTextSearchQuery.getCompoundDrawables()[DRAWABLE_RIGHT] != null)
                    {
                        if (event.getX() >= (mEditTextSearchQuery.getRight() - mEditTextSearchQuery.getCompoundDrawables()[DRAWABLE_RIGHT].getBounds().width()))
                        {
                            mEditTextSearchQuery.setText("");
                            return true;
                        }
                    }
                }
                return false;
            }
        });

        mEditTextSearchQuery.setOnEditorActionListener(new TextView.OnEditorActionListener()
        {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event)
            {
                if (actionId == EditorInfo.IME_ACTION_SEARCH)
                {
                    onSearchQueryClicked();
                    return true;
                }
                return false;
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu)
    {
        getMenuInflater().inflate(R.menu.home, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        switch (item.getItemId())
        {
            case R.id.action_cart:
            {
                onCartClicked();
                return true;
            }
            case android.R.id.home:
            {
                super.onBackPressed();
                return true;
            }
        }
        return super.onOptionsItemSelected(item);
    }

    private void onCartClicked()
    {
        ArrayList<AccountData> accountDatas = AccountController.getInstance().getmAccountDataList();
        if (accountDatas.size() > 0)
        {
            ArrayList<CartDetail> cartDetails = accountDatas.get(0).getCartDetailList();
            if (cartDetails.size() > 0)
            {
                if (cartDetails.get(0).getmLineItemData().size() > 0)
                {
                    Intent intent = new Intent(VendorSearchActivity.this, EcommerceCartActivity.class);
                    startActivity(intent);
                } else
                {
                    Utilities.showToastMessage(VendorSearchActivity.this, getString(R.string.empty_cart));
                }
            } else
            {
                Utilities.showToastMessage(VendorSearchActivity.this, getString(R.string.empty_cart));
            }
        }
    }

    @Override
    public void onClick(View v)
    {
        switch (v.getId())
        {
            case R.id.mTextViewSearchQuery:
                onSearchQueryClicked();
                break;
        }
    }

    private void onSearchQueryClicked()
    {
        String strQuery = mEditTextSearchQuery.getText().toString();
        if (strQuery.length() > 0)
        {
            hideSoftInput();

            if (ConnectionDetector.isConnectedToInternet(VendorSearchActivity.this))
            {
                mProgressBarView.setVisibility(View.VISIBLE);
                LockatedVolleyRequestQueue lockatedVolleyRequestQueue = LockatedVolleyRequestQueue.getInstance(VendorSearchActivity.this);
                lockatedVolleyRequestQueue.sendRequest(VENDOR_SEARCH_TAG, Request.Method.GET, ApplicationURL.getSupplierBylatLongUrl + mLockatedPreferences.getLockatedToken() + "&taxonomy_id=18&location[latitude]=19.0176169000&location[longitude]=72.8561288000&q[name_cont]=" + strQuery, null, this, this);
            }
            else
            {
                Utilities.showToastMessage(VendorSearchActivity.this, getString(R.string.internet_connection_error));
            }
        }
        else
        {
            Utilities.showToastMessage(VendorSearchActivity.this, "Please enter something to search.");
        }
    }

    public boolean hideSoftInput()
    {
        InputMethodManager imm = (InputMethodManager) getSystemService(Activity.INPUT_METHOD_SERVICE);
        return imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
    }

    @Override
    public void onRecyclerItemClick(View view, int position)
    {
        switch (view.getId())
        {
            case R.id.mLinearLayoutVendorRow:
                onVendorRowClicked(mVendorData.get(position).getName(), mVendorData.get(position).getId());
                break;
        }
    }

    private void onVendorRowClicked(String vendorName, int id)
    {
        VendorMenuFragment vendorMenuFragment = new VendorMenuFragment();
        ((LockatedApplication) getApplicationContext()).setSectionName(vendorName);
        ((LockatedApplication) getApplicationContext()).setmSectionId(18);
        ((LockatedApplication) getApplicationContext()).setmSectionIdTwo(id);
        getSupportFragmentManager().beginTransaction().replace(R.id.mVendorContainer, vendorMenuFragment).addToBackStack("VendorFragment").commit();
    }

    @Override
    public void onErrorResponse(VolleyError error)
    {
        mProgressBarView.setVisibility(View.GONE);
        LockatedRequestError.onRequestError(VendorSearchActivity.this, error);
    }

    @Override
    public void onResponse(Object response)
    {
        if (!isFinishing())
        {
            mProgressBarView.setVisibility(View.GONE);
            JSONObject jsonObject = (JSONObject) response;
            try
            {
                JSONArray jsonArray = jsonObject.getJSONArray("suppliers");
                mVendorData.clear();

                for (int i = 0; i < jsonArray.length(); i++)
                {
                    JSONObject addressObject = jsonArray.getJSONObject(i);
                    VendorData vendorData = new VendorData(addressObject);
                    mVendorData.add(vendorData);
                }

                mVendorAdapter.notifyDataSetChanged();
            }
            catch (JSONException e)
            {
                e.printStackTrace();
            }
        }
    }
}
