package com.android.lockated.categories.zomato.adapter;

import android.app.Activity;
import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.android.lockated.R;
import com.android.lockated.model.zomato.Dish;

import java.util.ArrayList;

public class ZomatoItemAdapter extends ArrayAdapter<Dish> {
    Context context;
    int layoutResourceId;
    ArrayList<Dish> data = null;

    public ZomatoItemAdapter(Context context, int layoutResourceId, ArrayList<Dish> data)
    {
        super(context, layoutResourceId, data);
        this.layoutResourceId = layoutResourceId;
        this.context = context;
        this.data = data;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View row = convertView;
        ItemHolder holder = null;

        if (row == null) {
            LayoutInflater inflater = ((Activity) context).getLayoutInflater();
            row = inflater.inflate(layoutResourceId, parent, false);

            holder = new ItemHolder();
            holder.txt_item_name = (TextView) row.findViewById(R.id.txt_item_name);
            holder.txt_item_quantity = (TextView) row.findViewById(R.id.txt_item_quantity);
            holder.txt_item_total_cost = (TextView) row.findViewById(R.id.txt_item_total_cost);
            holder.txt_item_unit_cost = (TextView) row.findViewById(R.id.txt_item_unit_cost);

            row.setTag(holder);
        } else {
            holder = (ItemHolder) row.getTag();
        }

        Log.e("Size of array", String.valueOf(data.size()));
        Dish bean = data.get(position);

        Log.e("getItemName",""+bean.getItemName());
        Log.e("getQuantity",""+bean.getQuantity());
        Log.e("getTotalCost",""+bean.getTotalCost());
        Log.e("getUnitCost",""+bean.getUnitCost());

        holder.txt_item_name.setText(""+bean.getItemName());
        holder.txt_item_quantity.setText(""+bean.getQuantity()+" x");
        holder.txt_item_unit_cost.setText(" "+bean.getUnitCost());
        holder.txt_item_total_cost.setText(""+context.getResources().getString(R.string.rupees_symbol)+" "+bean.getTotalCost());

        return row;
    }

    public static class ItemHolder {
        TextView txt_item_name;
        TextView txt_item_quantity;
        TextView txt_item_total_cost;
        TextView txt_item_unit_cost;

    }

    public void updateAdapter(ArrayList<Dish> pers){
        this.data = pers;
    }

    @Override
    public int getCount() {
        return data.size();
    }
}
