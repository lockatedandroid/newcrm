package com.android.lockated.crm.fragment;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.widget.DatePicker;
import android.widget.TextView;

import com.android.lockated.R;
import com.android.lockated.utils.Utilities;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;


public class DatePickerCRMFragment extends DialogFragment implements DatePickerDialog.OnDateSetListener {

    private TextView mTextViewDate;

    public void setDatePickerView(TextView textView) {
        mTextViewDate = textView;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        final Calendar c = Calendar.getInstance();
        int year = c.get(Calendar.YEAR);
        int month = c.get(Calendar.MONTH);
        int day = c.get(Calendar.DAY_OF_MONTH);

        return new DatePickerDialog(getActivity(), this, year, month, day);
    }

    @Override
    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
        String pickedDate = year + "-" + (monthOfYear + 1) + "-" + dayOfMonth;
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-M-d");
        Date date;

        try {
            date = sdf.parse(pickedDate);
            Calendar now = Calendar.getInstance();
            Calendar yourDate = Calendar.getInstance();
            yourDate.setTimeInMillis(date.getTime());

            if (now.equals(yourDate) || yourDate.after(now)) {
                mTextViewDate.setText(pickedDate);
            } else {
                Utilities.showToastMessage(getActivity(), getActivity().getString(R.string.future_date));
            }

        } catch (ParseException e) {
            e.printStackTrace();
        }
    }
}
