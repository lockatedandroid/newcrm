package com.android.lockated.model.OlaCab.OlaAutoBookResponse;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class AutoBookResponse {

    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("booking_id")
    @Expose
    private String bookingId;
    @SerializedName("message")
    @Expose
    private String message;

    /**
     * @return The status
     */
    public String getStatus() {
        return status;
    }

    /**
     * @param status The status
     */
    public void setStatus(String status) {
        this.status = status;
    }

    /**
     * @return The bookingId
     */
    public String getBookingId() {
        return bookingId;
    }

    /**
     * @param bookingId The booking_id
     */
    public void setBookingId(String bookingId) {
        this.bookingId = bookingId;
    }

    /**
     * @return The message
     */
    public String getMessage() {
        return message;
    }

    /**
     * @param message The message
     */
    public void setMessage(String message) {
        this.message = message;
    }

}
