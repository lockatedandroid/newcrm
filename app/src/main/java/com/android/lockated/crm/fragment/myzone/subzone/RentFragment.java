package com.android.lockated.crm.fragment.myzone.subzone;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.Toast;

import com.android.lockated.R;
import com.android.lockated.crm.adapters.SellAdapter;
import com.android.lockated.information.AccountController;
import com.android.lockated.network.ConnectionDetector;
import com.android.lockated.preferences.LockatedPreferences;
import com.android.lockated.request.LockatedJSONArrayRequest;
import com.android.lockated.request.LockatedRequestError;
import com.android.lockated.request.LockatedVolleyRequestQueue;
import com.android.lockated.utils.ApplicationURL;
import com.android.lockated.utils.Utilities;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;

import org.json.JSONArray;

public class RentFragment extends Fragment implements View.OnClickListener, Response.Listener<JSONArray>, Response.ErrorListener {

    FloatingActionButton fab;
    private View rentView;
    ListView SellList;
    SellAdapter sellAdapter;
    private AccountController mAccountController;
    private RequestQueue mQueue;
    private LockatedPreferences mLockatedPreferences;
    public static final String REQUEST_TAG = "RentFragment";

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        rentView = inflater.inflate(R.layout.fragment_rent, container, false);
        SellList = (ListView) rentView.findViewById(R.id.listView);
        init();
        return rentView;
    }

    private void init() {
        fab = (FloatingActionButton) rentView.findViewById(R.id.fab);
        fab.setOnClickListener(this);
    }


    @Override
    public void onErrorResponse(VolleyError error) {
        if (getActivity() != null) {
            LockatedRequestError.onRequestError(getActivity(), error);
        }
    }

    @Override
    public void onClick(View v) {

    }

    @Override
    public void onResponse(JSONArray response) {
        if (getActivity() != null) {
            try {
                if (response != null && response.length() > 0) {
                    sellAdapter = new SellAdapter(getActivity(), response);
                    SellList.setAdapter(sellAdapter);
                } else {
                    Toast.makeText(getActivity(), "JSONArray response is empty", Toast.LENGTH_LONG).show();
                }

            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

}
