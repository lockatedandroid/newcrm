package com.android.lockated.model;

import org.json.JSONObject;

public class ServantData
{
    private int id;
    private int id_user;
    private String gender;
    private String name;
    private String mobile;
    private String dob;
    private String married;

    public ServantData(JSONObject jsonObject)
    {
        id = jsonObject.optInt("id");
        id_user = jsonObject.optInt("id_user");
        gender = jsonObject.optString("gender");
        name = jsonObject.optString("name");
        mobile = jsonObject.optString("mobile");
        dob = jsonObject.optString("dob");
        married = jsonObject.optString("married");
    }

    public int getId()
    {
        return id;
    }

    public int getId_user()
    {
        return id_user;
    }

    public String getGender()
    {
        return gender;
    }

    public String getName()
    {
        return name;
    }

    public String getMobile()
    {
        return mobile;
    }

    public String getDob()
    {
        return dob;
    }

    public String getMarried()
    {
        return married;
    }

    public void setId(int id)
    {
        this.id = id;
    }

    public void setId_user(int id_user)
    {
        this.id_user = id_user;
    }

    public void setGender(String gender)
    {
        this.gender = gender;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public void setMobile(String mobile)
    {
        this.mobile = mobile;
    }

    public void setDob(String dob)
    {
        this.dob = dob;
    }

    public void setMarried(String married)
    {
        this.married = married;
    }
}