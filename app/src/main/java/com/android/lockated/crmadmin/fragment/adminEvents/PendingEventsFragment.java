package com.android.lockated.crmadmin.fragment.adminEvents;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.android.lockated.LockatedApplication;
import com.android.lockated.R;
import com.android.lockated.crmadmin.adapter.PendingEventAdapter;
import com.android.lockated.model.AdminModel.AdminEvents.Classified;
import com.android.lockated.network.ConnectionDetector;
import com.android.lockated.preferences.LockatedPreferences;
import com.android.lockated.request.LockatedJSONObjectRequest;
import com.android.lockated.request.LockatedRequestError;
import com.android.lockated.request.LockatedVolleyRequestQueue;
import com.android.lockated.utils.ApplicationURL;
import com.android.lockated.utils.Utilities;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;


public class PendingEventsFragment extends Fragment implements Response.ErrorListener, Response.Listener<JSONObject> {

    RecyclerView recyclerView;
    private RequestQueue mQueue;
    private LockatedPreferences mLockatedPreferences;
    public static final String REQUEST_TAG = "PendingEventsFragment";
    ProgressBar progressBar;
    TextView errorMsg;
    ArrayList<Classified> classifiedArrayList;
    PendingEventAdapter pendingEventAdapter;
    String SHOW, CREATE, INDEX, UPDATE, EDIT;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View pendingEvents = inflater.inflate(R.layout.fragment_pending_events, container, false);
        Utilities.ladooIntegration(getActivity(), "Admin Pending Events");
        init(pendingEvents);
        return pendingEvents;

    }

    @Override
    public void onResume() {
        super.onResume();
        LockatedApplication.getInstance().trackScreenView(getString(R.string.publishedEvents));
        LockatedApplication.getInstance().trackEvent(getString(R.string.publishedEvents),
                getString(R.string.visited), getString(R.string.publishedEvents));
        //classifiedArrayList.clear();
        //  getPendingEvents();
    }

    private void init(View view) {
        classifiedArrayList = new ArrayList<>();
        mLockatedPreferences = new LockatedPreferences(getActivity());
        progressBar = (ProgressBar) view.findViewById(R.id.mProgressBarView);
        recyclerView = (RecyclerView) view.findViewById(R.id.recyclerView);
        errorMsg = (TextView) view.findViewById(R.id.noText);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity(),
                LinearLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(linearLayoutManager);
        getEventRole();
        checkPermission();
        pendingEventAdapter = new PendingEventAdapter(getActivity(), CREATE, classifiedArrayList);
        recyclerView.setAdapter(pendingEventAdapter);
    }

    private void checkPermission() {
        if (SHOW!=null&&SHOW.equals("true")) {
            getPendingEvents();
        } else {
            recyclerView.setVisibility(View.GONE);
            errorMsg.setVisibility(View.VISIBLE);
            errorMsg.setText(R.string.no_permission_error);
        }
    }

    public void getPendingEvents() {

        if (getActivity() != null) {
            if (!mLockatedPreferences.getSocietyId().equals("blank")) {
                if (ConnectionDetector.isConnectedToInternet(getActivity())) {
                    progressBar.setVisibility(View.VISIBLE);
                    String url = ApplicationURL.getPendingEventsUrl + mLockatedPreferences.getLockatedToken()
                            + ApplicationURL.societyId + mLockatedPreferences.getSocietyId();
                    mQueue = LockatedVolleyRequestQueue.getInstance(getActivity()).getRequestQueue();
                    LockatedJSONObjectRequest lockatedJSONObjectRequest = new LockatedJSONObjectRequest(Request.Method.GET,
                            url, null, this, this);
                    lockatedJSONObjectRequest.setTag(REQUEST_TAG);
                    mQueue.add(lockatedJSONObjectRequest);
                } else {
                    Utilities.showToastMessage(getActivity(), getActivity().getResources().getString(R.string.internet_connection_error));
                }
            } else {
                errorMsg.setVisibility(View.VISIBLE);
                errorMsg.setText(R.string.not_in_society);
            }
        }

    }

    @Override
    public void onErrorResponse(VolleyError error) {
        if (getActivity() != null) {
            progressBar.setVisibility(View.GONE);
            LockatedRequestError.onRequestError(getActivity(), error);
        }
    }

    @Override
    public void onResponse(JSONObject response) {
        //Log.e("Pending Events response", "" + response);
        if (getActivity() != null) {
            progressBar.setVisibility(View.GONE);
            try {
                if (response != null && response.has("classifieds") && response.getJSONArray("classifieds").length() > 0) {
                    JSONArray jsonArray = response.getJSONArray("classifieds");
                    Gson gson = new Gson();
                    for (int i = 0; i < jsonArray.length(); i++) {
                        Classified classified = gson.fromJson(jsonArray.getJSONObject(i).toString(), Classified.class);
                        classifiedArrayList.add(classified);
                    }
                    pendingEventAdapter.notifyDataSetChanged();

                   /* pendingEventAdapter = new PendingEventAdapter(getActivity(), jsonArray);
                    recyclerView.setAdapter(pendingEventAdapter)*/
                    ;
                } else {
                    errorMsg.setVisibility(View.VISIBLE);
                    errorMsg.setText("No data available");
                    recyclerView.setVisibility(View.GONE);
                }

            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public String getEventRole() {
        String mySocietyRoles = getActivity().getResources().getString(R.string.blank_value);
        if (mLockatedPreferences.getRolesJson() != null && !mLockatedPreferences.getRolesJson().equals("blank")) {
            try {
                JSONObject noticeJsonObj = new JSONObject(mLockatedPreferences.getRolesJson());
                for (int i = 0; i < noticeJsonObj.getJSONArray(getActivity().getResources().getString(R.string.permissions_value)).length(); i++) {
                    JSONObject jsonObject1 = noticeJsonObj.getJSONArray(getActivity().getResources().getString(R.string.permissions_value)).getJSONObject(i);
                    if (jsonObject1.has(getActivity().getResources().getString(R.string.section_value))
                            && jsonObject1.getString(getActivity().getResources().getString(R.string.section_value)).equals("spree_events")) {
                        if (jsonObject1.getJSONObject(getActivity().getResources().getString(R.string.permission_value)).has(getActivity().getResources().getString(R.string.index_value))) {
                            mySocietyRoles = jsonObject1.getJSONObject(getActivity().getResources().getString(R.string.permission_value)).getString(getActivity().getResources().getString(R.string.index_value));
                            CREATE = jsonObject1.getJSONObject(getActivity().getResources().getString(R.string.permission_value)).getString("create");
                            INDEX = jsonObject1.getJSONObject(getActivity().getResources().getString(R.string.permission_value)).getString("index");
                            UPDATE = jsonObject1.getJSONObject(getActivity().getResources().getString(R.string.permission_value)).getString("update");
                            EDIT = jsonObject1.getJSONObject(getActivity().getResources().getString(R.string.permission_value)).getString("edit");
                            SHOW = jsonObject1.getJSONObject(getActivity().getResources().getString(R.string.permission_value)).getString("show");
                            //Log.e("Permissions ", " CREATE" + CREATE + "INDEX" + " " + INDEX + "UPDATE" + " " + UPDATE + " " + "EDIT" + EDIT);
                        }
                    }
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return mySocietyRoles;
    }

}
