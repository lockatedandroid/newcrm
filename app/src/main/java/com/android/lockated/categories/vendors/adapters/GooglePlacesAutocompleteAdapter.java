package com.android.lockated.categories.vendors.adapters;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.lockated.R;
import com.android.lockated.utils.ApplicationURL;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;

public class GooglePlacesAutocompleteAdapter extends BaseAdapter implements Filterable {

    private ArrayList<String> resultList;
    Context context;
    LayoutInflater layoutInflater;
    private static final String GET_REQUEST_TAG = "VendorFragment:GetSuppliers";

    public GooglePlacesAutocompleteAdapter(Context context) {
        this.context = context;
        layoutInflater = LayoutInflater.from(context);
    }

    @Override
    public int getCount() {
        return resultList.size();
    }

    @Override
    public String getItem(int index) {
        return resultList.get(index);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder;
        if (convertView == null) {
            convertView = layoutInflater.inflate(R.layout.google_list_item, parent, false);
            viewHolder = new ViewHolder();
            viewHolder.imageView = (ImageView) convertView.findViewById(R.id.imageName);
            viewHolder.textView = (TextView) convertView.findViewById(R.id.locationText);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }
        //viewHolder.imageView.setImageResource(R.drawable.location);
        viewHolder.textView.setText(resultList.get(position));

        return convertView;
    }

    class ViewHolder {
        ImageView imageView;
        TextView textView;
    }

    @Override
    public Filter getFilter() {
        Filter filter = new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence constraint) {
                FilterResults filterResults = new FilterResults();
                if (constraint != null) {
                    resultList = autocomplete(constraint.toString());
                    filterResults.values = resultList;
                    filterResults.count = resultList.size();
                }
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {
                if (results != null && results.count > 0) {
                    notifyDataSetChanged();
                } else {
                    notifyDataSetInvalidated();
                }
            }
        };
        return filter;
    }

    public static ArrayList<String> autocomplete(String input) {
        ArrayList<String> resultList = null;

        HttpURLConnection conn = null;
        StringBuilder jsonResults = new StringBuilder();
        try {
            StringBuilder sb = new StringBuilder(ApplicationURL.PLACES_API_BASE
                    + ApplicationURL.TYPE_AUTOCOMPLETE + ApplicationURL.OUT_JSON);
            sb.append("?key=" + ApplicationURL.Server_KEY);
            sb.append("&components=country:in");
            sb.append("&components=administrative_area_level_1:MH");
            sb.append("&components=administrative_area_level_3:MUM");
            sb.append("&input=" + URLEncoder.encode(input, "utf8"));
            URL url = new URL(sb.toString());
            //Log.e("autocomplete", "" + url);
            conn = (HttpURLConnection) url.openConnection();
            InputStreamReader in = new InputStreamReader(conn.getInputStream());
            int read;
            char[] buff = new char[1024];
            while ((read = in.read(buff)) != -1) {
                jsonResults.append(buff, 0, read);
            }
        } catch (MalformedURLException e) {
            //Log.e(GET_REQUEST_TAG, "Error processing Places API URL", e);
            return resultList;
        } catch (IOException e) {
            //Log.e(GET_REQUEST_TAG, "Error connecting to Places API", e);
            return resultList;
        } finally {
            if (conn != null) {
                conn.disconnect();
            }
        }
        try {
            JSONObject jsonObj = new JSONObject(jsonResults.toString());
            JSONArray predsJsonArray = jsonObj.getJSONArray("predictions");
            resultList = new ArrayList<>(predsJsonArray.length());
            for (int i = 0; i < predsJsonArray.length(); i++) {
                /*System.out.println(predsJsonArray.getJSONObject(i).getString("description"));
                System.out.println("============================================================");*/
                String removeStateCountry;
                if (predsJsonArray.getJSONObject(i).getString("description").contains(", Maharashtra, India")) {
                    removeStateCountry = predsJsonArray.getJSONObject(i).getString("description")
                            .replaceAll(", Maharashtra, India", "");
                } else if (predsJsonArray.getJSONObject(i).getString("description").contains(", Maharashtra, India")) {
                    removeStateCountry = predsJsonArray.getJSONObject(i).getString("description")
                            .replaceAll(", Mumbai, Maharashtra, India", "");
                } else {
                    removeStateCountry = predsJsonArray.getJSONObject(i).getString("description");
                }
                resultList.add(removeStateCountry);
            }
        } catch (JSONException e) {
            //Log.e(GET_REQUEST_TAG, "Cannot process JSON results", e);
        }

        return resultList;
    }

}

    /*class GooglePlacesAutocompleteAdapter extends ArrayAdapter<String> implements Filterable {

        private ArrayList<String> resultList;

        public GooglePlacesAutocompleteAdapter(Context context, int textViewResourceId) {
            super(context, textViewResourceId);
        }

        @Override
        public int getCount() {
            return resultList.size();
        }

        @Override
        public String getItem(int index) {
            return resultList.get(index);
        }

        @Override
        public Filter getFilter() {
            Filter filter = new Filter() {
                @Override
                protected FilterResults performFiltering(CharSequence constraint) {
                    FilterResults filterResults = new FilterResults();
                    if (constraint != null) {
                        resultList = autocomplete(constraint.toString());
                        filterResults.values = resultList;
                        filterResults.count = resultList.size();
                    }
                    return filterResults;
                }

                @Override
                protected void publishResults(CharSequence constraint, FilterResults results) {
                    if (results != null && results.count > 0) {
                        notifyDataSetChanged();
                    } else {
                        notifyDataSetInvalidated();
                    }
                }
            };
            return filter;
        }
    }*/

/*public LatLng getLatLongFromPlace(Context context, String place) {

        LatLng p1;
        try {
            Geocoder selected_place_geocoder = new Geocoder(context);
            List<Address> address;
            address = selected_place_geocoder.getFromLocationName(place, 5);
            Address location = address.get(0);
            double lat = location.getLatitude();
            double lng = location.getLongitude();
            p1 = new LatLng(lat, lng);
        } catch (Exception e) {
            e.printStackTrace();
            fetchLatLongFromService fetch_latlng_from_service_abc = new fetchLatLongFromService(
                    place.replaceAll("\\s+", ""));
            fetch_latlng_from_service_abc.execute();
            p1 = new LatLng(0.000, 0.000);
        }
        return p1;
    }


    public class fetchLatLongFromService extends AsyncTask<Void, Void, StringBuilder> {

        String place;

        public fetchLatLongFromService(String place) {
            super();
            this.place = place;

        }

        @Override
        protected void onCancelled() {
            super.onCancelled();
            this.cancel(true);
        }

        @Override
        protected StringBuilder doInBackground(Void... params) {
            try {
                HttpURLConnection conn = null;
                StringBuilder jsonResults = new StringBuilder();
                String googleMapUrl = "http://maps.googleapis.com/maps/api/geocode/json?address="
                        + this.place + "&sensor=false";
                URL url = new URL(googleMapUrl);
                conn = (HttpURLConnection) url.openConnection();
                InputStreamReader in = new InputStreamReader(
                        conn.getInputStream());
                int read;
                char[] buff = new char[1024];
                while ((read = in.read(buff)) != -1) {
                    jsonResults.append(buff, 0, read);
                }
                String a = "";
                return jsonResults;
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;

        }

        @Override
        protected void onPostExecute(StringBuilder result) {
            super.onPostExecute(result);
            *//*LatLng point;*//*
            try {
                JSONObject jsonObj = new JSONObject(result.toString());
                JSONArray resultJsonArray = jsonObj.getJSONArray("results");

                // Extract the Place descriptions from the results
                // resultList = new ArrayList<String>(resultJsonArray.length());

                JSONObject before_geometry_jsonObj = resultJsonArray
                        .getJSONObject(0);
                JSONObject geometry_jsonObj = before_geometry_jsonObj
                        .getJSONObject("geometry");
                JSONObject location_jsonObj = geometry_jsonObj
                        .getJSONObject("location");
                String lat_helper = location_jsonObj.getString("lat");
                double lat = Double.valueOf(lat_helper);


                String lng_helper = location_jsonObj.getString("lng");
                double lng = Double.valueOf(lng_helper);


                point = new LatLng(lat, lng);


            } catch (JSONException e) {
                e.printStackTrace();
                point = new LatLng(0.000, 0.000);
            }
            //return point;
        }
    }*/
