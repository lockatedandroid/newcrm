
package com.android.lockated.model.userSocietyFlat;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class UserFlat implements Parcelable {

    @SerializedName("id")
    @Expose
    private int id;
    @SerializedName("id_user")
    @Expose
    private int idUser;
    @SerializedName("residence_type")
    @Expose
    private String residenceType;
    @SerializedName("society_id")
    @Expose
    private String societyId;
    @SerializedName("flat")
    @Expose
    private String flat;
    @SerializedName("block")
    @Expose
    private String block;
    @SerializedName("building_number")
    @Expose
    private String buildingNumber;
    @SerializedName("ownership")
    @Expose
    private String ownership;
    @SerializedName("intercom")
    @Expose
    private String intercom;
    @SerializedName("landline")
    @Expose
    private String landline;
    @SerializedName("address_id")
    @Expose
    private String addressId;
    @SerializedName("society")
    @Expose
    private Society society;
    @SerializedName("user_society")
    @Expose
    private UserSociety userSociety;
    @SerializedName("society_flat")
    @Expose
    private SocietyFlat societyFlat;
    @SerializedName("address")
    @Expose
    private Address address;

    /**
     * @return The id
     */
    public int getId() {
        return id;
    }

    /**
     * @param id The id
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * @return The idUser
     */
    public int getIdUser() {
        return idUser;
    }

    /**
     * @param idUser The id_user
     */
    public void setIdUser(int idUser) {
        this.idUser = idUser;
    }

    /**
     * @return The residenceType
     */
    public String getResidenceType() {
        return residenceType;
    }

    /**
     * @param residenceType The residence_type
     */
    public void setResidenceType(String residenceType) {
        this.residenceType = residenceType;
    }

    /**
     * @return The societyId
     */
    public String getSocietyId() {
        return societyId;
    }

    /**
     * @param societyId The society_id
     */
    public void setSocietyId(String societyId) {
        this.societyId = societyId;
    }

    /**
     * @return The flat
     */
    public String getFlat() {
        return flat;
    }

    /**
     * @param flat The flat
     */
    public void setFlat(String flat) {
        this.flat = flat;
    }

    /**
     * @return The block
     */
    public String getBlock() {
        return block;
    }

    /**
     * @param block The block
     */
    public void setBlock(String block) {
        this.block = block;
    }

    /**
     * @return The buildingNumber
     */
    public String getBuildingNumber() {
        return buildingNumber;
    }

    /**
     * @param buildingNumber The building_number
     */
    public void setBuildingNumber(String buildingNumber) {
        this.buildingNumber = buildingNumber;
    }

    /**
     * @return The ownership
     */
    public String getOwnership() {
        return ownership;
    }

    /**
     * @param ownership The ownership
     */
    public void setOwnership(String ownership) {
        this.ownership = ownership;
    }

    /**
     * @return The intercom
     */
    public Object getIntercom() {
        return intercom;
    }

    /**
     * @param intercom The intercom
     */
    public void setIntercom(String intercom) {
        this.intercom = intercom;
    }

    /**
     * @return The landline
     */
    public String getLandline() {
        return landline;
    }

    /**
     * @param landline The landline
     */
    public void setLandline(String landline) {
        this.landline = landline;
    }

    /**
     * @return The addressId
     */
    public String getAddressId() {
        return addressId;
    }

    /**
     * @param addressId The address_id
     */
    public void setAddressId(String addressId) {
        this.addressId = addressId;
    }

    /**
     * @return The society
     */
    public Society getSociety() {
        return society;
    }

    /**
     * @param society The society
     */
    public void setSociety(Society society) {
        this.society = society;
    }

    public UserSociety getUserSociety() {
        return userSociety;
    }

    public void setUserSociety(UserSociety userSociety) {
        this.userSociety = userSociety;
    }

    public SocietyFlat getSocietyFlat() {
        return societyFlat;
    }

    public void setSocietyFlat(SocietyFlat societyFlat) {
        this.societyFlat = societyFlat;
    }

    /**
     * @return The address
     */
    public Address getAddress() {
        return address;
    }

    /**
     * @param address The address
     */
    public void setAddress(Address address) {
        this.address = address;
    }

    public UserFlat(Parcel in) {
        id = in.readInt();
        idUser = in.readInt();
        residenceType = in.readString();
        societyId = in.readString();
        flat = in.readString();
        block = in.readString();
        buildingNumber = in.readString();
        ownership = in.readString();
        intercom = in.readString();
        landline = in.readString();
        addressId = in.readString();
        society = in.readParcelable(Society.class.getClassLoader());
        userSociety = in.readParcelable(UserSociety.class.getClassLoader());
        societyFlat = in.readParcelable(SocietyFlat.class.getClassLoader());
        address = in.readParcelable(Address.class.getClassLoader());
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(id);
        dest.writeInt(idUser);
        dest.writeString(residenceType);
        dest.writeString(societyId);
        dest.writeString(flat);
        dest.writeString(block);
        dest.writeString(buildingNumber);
        dest.writeString(ownership);
        dest.writeString(intercom);
        dest.writeString(landline);
        dest.writeString(addressId);
        dest.writeParcelable(society, flags);
        dest.writeParcelable(userSociety, flags);
        dest.writeParcelable(societyFlat, flags);
        dest.writeParcelable(address, flags);
    }

    @Override
    public int describeContents() {
        return this.hashCode();
    }

    public static final Creator<UserFlat> CREATOR = new Creator<UserFlat>() {
        @Override
        public UserFlat createFromParcel(Parcel in) {
            return new UserFlat(in);
        }

        @Override
        public UserFlat[] newArray(int size) {
            return new UserFlat[size];
        }
    };

}
