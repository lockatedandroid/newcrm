package com.android.lockated.account.fragment;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.android.lockated.Interfaces.IRecyclerItemClickListener;
import com.android.lockated.LockatedApplication;
import com.android.lockated.R;
import com.android.lockated.account.adapter.MyOrdersAdapter;
import com.android.lockated.information.AccountController;
import com.android.lockated.model.AccountData;
import com.android.lockated.model.MyOrderData;
import com.android.lockated.network.ConnectionDetector;
import com.android.lockated.preferences.LockatedPreferences;
import com.android.lockated.request.LockatedRequestError;
import com.android.lockated.request.LockatedVolleyRequestQueue;
import com.android.lockated.utils.ApplicationURL;
import com.android.lockated.utils.Utilities;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class MyOrdersFragment extends Fragment implements Response.Listener, Response.ErrorListener, IRecyclerItemClickListener {
    private View mMyOrdersView;

    private ProgressDialog mProgressDialog;
    private RecyclerView mRecyclerView;
    private RecyclerView.LayoutManager mLayoutManager;

    private MyOrdersAdapter mMyOrdersAdapter;
    private RequestQueue mQueue;
    private LockatedPreferences mLockatedPreferences;

    private ArrayList<MyOrderData> mOrderDataList;
    private AccountController mAccountController;

    private int mPosition;

    public static final String REQUEST_TAG = "MyOrdersFragment";

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mMyOrdersView = inflater.inflate(R.layout.fragment_recycler, container, false);
        init();
        Utilities.ladooIntegration(getActivity(), getActivity().getString(R.string.my_orders_screen));
        return mMyOrdersView;
    }

    @Override
    public void onResume() {
        super.onResume();
        LockatedApplication.getInstance().trackScreenView(getActivity().getString(R.string.my_orders_screen));
    }

    private void init() {
        mOrderDataList = new ArrayList<>();
        mAccountController = AccountController.getInstance();
        mLockatedPreferences = new LockatedPreferences(getActivity());

        ArrayList<AccountData> datas = mAccountController.getmAccountDataList();
        if (datas.size() > 0) {
            if (datas.get(0).getmMyOrderList().size() > 0) {
                mOrderDataList = datas.get(0).getmMyOrderList();
            }
        }

        mRecyclerView = (RecyclerView) mMyOrdersView.findViewById(R.id.mRecyclerView);

        mLayoutManager = new LinearLayoutManager(getActivity());
        mRecyclerView.setLayoutManager(mLayoutManager);
        mRecyclerView.setHasFixedSize(true);
        mMyOrdersAdapter = new MyOrdersAdapter(mOrderDataList, this);
        mRecyclerView.setAdapter(mMyOrdersAdapter);
    }

    private void onOrderCancel(int position) {
        if (ConnectionDetector.isConnectedToInternet(getActivity())) {
            mProgressDialog = ProgressDialog.show(getActivity(), "", "Please Wait...", false);
            mProgressDialog.show();
            String url = ApplicationURL.cancelOrderUrl + mOrderDataList.get(position).getNumber()
                    + "/cancel?token=" + mLockatedPreferences.getLockatedToken();
            LockatedVolleyRequestQueue lockatedVolleyRequestQueue = LockatedVolleyRequestQueue.getInstance(getActivity());
            lockatedVolleyRequestQueue.sendRequest(REQUEST_TAG, Request.Method.PUT, url, null, this, this);
        } else {
            Utilities.showToastMessage(getActivity(), getActivity().getResources().getString(R.string.internet_connection_error));
        }
    }

    @Override
    public void onErrorResponse(VolleyError error) {
        mProgressDialog.dismiss();
        if (getActivity() != null) {
            LockatedRequestError.onRequestError(getActivity(), error);
        }
    }

    @Override
    public void onResponse(Object response) {
        mProgressDialog.dismiss();
        if (getActivity() != null) {
            JSONObject jsonObject = (JSONObject) response;
            try {
                //String state = jsonObject.getString("shipment_state");
                String state = jsonObject.getString("state");
                boolean canCancel = jsonObject.getBoolean("can_cancel");
                mOrderDataList.get(mPosition).setState(state);
                mOrderDataList.get(mPosition).setCanCancel(canCancel);
                mMyOrdersAdapter.notifyDataSetChanged();
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void onRecyclerItemClick(View view, int position) {
        switch (view.getId()) {
            case R.id.mTextViewOrderCancel:
                mPosition = position;
                if (mOrderDataList.get(mPosition).getState().equalsIgnoreCase("canceled")) {
                    Utilities.showToastMessage(getActivity(), "Order is already canceled");
                } else {
                    onOrderCancel(position);
                }
                break;
        }
    }
}