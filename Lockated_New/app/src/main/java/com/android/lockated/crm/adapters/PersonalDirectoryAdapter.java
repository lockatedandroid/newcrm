package com.android.lockated.crm.adapters;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.android.lockated.R;
import com.android.lockated.crm.activity.MyZoneDirectoryActivity;
import com.android.lockated.crm.fragment.myzone.directory.AddToDirectory;
import com.android.lockated.preferences.LockatedPreferences;
import com.android.lockated.request.LockatedJSONObjectRequest;
import com.android.lockated.request.LockatedRequestError;
import com.android.lockated.request.LockatedVolleyRequestQueue;
import com.android.lockated.utils.ApplicationURL;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class PersonalDirectoryAdapter extends BaseAdapter {
    Context contextMain;
    JSONArray jsonArray;
    LayoutInflater layoutInflater;
    FragmentManager fragmentManager;
    private RequestQueue mQueue;
    String pid;
    String userType;
    TextView errorMsg;
    private LockatedPreferences mLockatedPreferences;
    public static final String REQUEST_TAG = "PersonalDirectoryFragment";
    FragmentManager childFragmentManager;

    public PersonalDirectoryAdapter(Context context, JSONArray jsonArray, FragmentManager childFragmentManager) {
        this.contextMain = context;
        this.jsonArray = jsonArray;
        this.childFragmentManager = childFragmentManager;
        layoutInflater = LayoutInflater.from(context);
    }

    @Override
    public int getCount() {
        return jsonArray.length();
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    private class ViewHolder {
        ImageView mImageDelete;
        ImageView mImageEdit;
        TextView mTextNumber;
        TextView mEmail_Id;
        TextView mTextDirectoryName;
        TextView mServiceType;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        ViewHolder holder = null;
        if (convertView == null) {

            convertView = layoutInflater.inflate(R.layout.directorychild, parent, false);
            holder = new ViewHolder();
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        holder.mImageEdit = (ImageView) convertView.findViewById(R.id.mImageEdit);
        holder.mImageDelete = (ImageView) convertView.findViewById(R.id.mImageDelete);
        holder.mTextDirectoryName = (TextView) convertView.findViewById(R.id.mTextDirectoryName);
        holder.mTextNumber = (TextView) convertView.findViewById(R.id.mTextNumber);
        holder.mEmail_Id = (TextView) convertView.findViewById(R.id.mEmail_Id);
        try {
            String name = jsonArray.getJSONObject(position).getString("firstname")
                    + " " + jsonArray.getJSONObject(position).getString("lastname");
            holder.mTextDirectoryName.setText(name);
            holder.mTextNumber.setText(jsonArray.getJSONObject(position).getString("mobile"));
            holder.mEmail_Id.setText(jsonArray.getJSONObject(position).getString("email"));
        } catch (Exception e) {
            e.printStackTrace();
        }
        holder.mImageEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    pid = jsonArray.getJSONObject(position).getString("id");
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                AddToDirectory mAddToDirectory = new AddToDirectory();
                Bundle bundle = new Bundle();
                try {
                    bundle.putString("pid", pid);
                    bundle.putString("mobile", jsonArray.getJSONObject(position).getString("mobile"));
                    bundle.putString("email", jsonArray.getJSONObject(position).getString("email"));
                    bundle.putString("firstname", jsonArray.getJSONObject(position).getString("firstname"));
                    bundle.putString("lastname", jsonArray.getJSONObject(position).getString("lastname"));
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                mAddToDirectory.setArguments(bundle);
                mAddToDirectory.show(childFragmentManager, "PreviewDialog");
            }
        });
        holder.mImageDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    pid = jsonArray.getJSONObject(position).getString("id");
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                onDirectoryDelete(pid);
            }
        });
        return convertView;
    }

    private void onDirectoryDelete(String pid) {
        mLockatedPreferences = new LockatedPreferences(contextMain);
        if (contextMain != null) {
            if (!mLockatedPreferences.getSocietyId().equals("blank")) {
                if (!userType.equals("blank")) {
                    if (pid != null) {
                        try {
                            mQueue = LockatedVolleyRequestQueue.getInstance(contextMain).getRequestQueue();
                            LockatedJSONObjectRequest lockatedJSONObjectRequest = new LockatedJSONObjectRequest
                                    (Request.Method.DELETE, ApplicationURL.deleteDirectory + pid + ".json?token="
                                            + mLockatedPreferences.getLockatedToken(), null, new Response.Listener<JSONObject>() {
                                        @Override
                                        public void onResponse(JSONObject response) {
                                            if (response != null && response.length() > 0) {
                                                if (response.has("code") && response.has("message")) {
                                                    Intent intent = new Intent(contextMain, MyZoneDirectoryActivity.class);
                                                    intent.putExtra("Listposition", 0);
                                                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                                    contextMain.startActivity(intent);
                                                }
                                            } else {
                                                errorMsg.setVisibility(View.VISIBLE);
                                                errorMsg.setText("No List To Display");
                                            }
                                        }
                                    }, new Response.ErrorListener() {
                                        @Override
                                        public void onErrorResponse(VolleyError error) {
                                            if (contextMain != null) {
                                                LockatedRequestError.onRequestError(contextMain, error);
                                            }
                                        }
                                    });
                            lockatedJSONObjectRequest.setTag(REQUEST_TAG);
                            mQueue.add(lockatedJSONObjectRequest);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }

                } else {
                    errorMsg.setVisibility(View.VISIBLE);
                    errorMsg.setText("You are not associated with any society");
                }
            } else {
                errorMsg.setVisibility(View.VISIBLE);
                errorMsg.setText("You are not associated with any society");
            }
        }
    }
}