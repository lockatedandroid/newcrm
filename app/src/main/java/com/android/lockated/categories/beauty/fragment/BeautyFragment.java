package com.android.lockated.categories.beauty.fragment;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.text.method.ScrollingMovementMethod;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.AutoCompleteTextView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.android.lockated.Interfaces.IGeocoderAddressUpdateListener;
import com.android.lockated.Interfaces.ILocationUpdateListener;
import com.android.lockated.Interfaces.IRecyclerItemClickListener;
import com.android.lockated.LockatedApplication;
import com.android.lockated.R;
import com.android.lockated.cart.ecommerce.EcommerceCartActivity;
import com.android.lockated.categories.adapter.BeautySupplierAdapter;
import com.android.lockated.categories.beauty.BeautyServiceActivity;
import com.android.lockated.categories.vendors.adapters.GooglePlacesAutocompleteAdapter;
import com.android.lockated.categories.vendors.model.VendorData;
import com.android.lockated.information.AccountController;
import com.android.lockated.location.GeocoderHandler;
import com.android.lockated.location.LocationAddress;
import com.android.lockated.location.LocationUpdateManager;
import com.android.lockated.location.model.LocationData;
import com.android.lockated.model.AccountData;
import com.android.lockated.model.CartDetail;
import com.android.lockated.network.ConnectionDetector;
import com.android.lockated.preferences.LockatedPreferences;
import com.android.lockated.request.LockatedRequestError;
import com.android.lockated.request.LockatedVolleyRequestQueue;
import com.android.lockated.search.VendorSearchFragment;
import com.android.lockated.utils.ApplicationURL;
import com.android.lockated.utils.Utilities;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.maps.model.LatLng;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class BeautyFragment extends Fragment implements ILocationUpdateListener, IGeocoderAddressUpdateListener, Response.Listener, Response.ErrorListener, IRecyclerItemClickListener, View.OnClickListener, TextWatcher {
    private static final int REQUEST_LOCATION = 10;
    private static final String GET_REQUEST_TAG = "VendorFragment:GetSuppliers";

    private View mFoodFragmentView;

    private ProgressBar mProgressBarView;
    //private EditText mEditTextFoodLocation;
    private AutoCompleteTextView mEditTextFoodLocation;
    private TextView mTextViewNoVendorFound;
    private ImageView mImageViewCurrentLocation;

    private int mSectionId;
    private double mLatitude;
    private double mLongitude;
    private boolean hasResetLocation;
    private boolean isLocationAvailable;

    private LocationUpdateManager mLocationUpdateManager;

    private RecyclerView mRecyclerView;

    private ArrayList<VendorData> mVendorData;
    private BeautySupplierAdapter mVendorAdapter;

    private LockatedPreferences mLockatedPreferences;
    boolean searchAutoComplete = false;
    String screenName = "Beauty Screen";

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        setHasOptionsMenu(true);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mFoodFragmentView = inflater.inflate(R.layout.fragment_supplier, container, false);
        init();
        Utilities.ladooIntegration(getActivity(), screenName);
        Utilities.lockatedGoogleAnalytics(screenName, getString(R.string.visited), screenName);
        checkLocationPermission();
        return mFoodFragmentView;
    }

    @Override
    public void onResume() {
        super.onResume();
        if (((LockatedApplication) getActivity().getApplicationContext()).isGPSEnabled()) {
            ((LockatedApplication) getActivity().getApplicationContext()).setIsGPSEnabled(false);
            checkLocationPermission();
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putInt("SectionId", mSectionId);
    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        super.onPrepareOptionsMenu(menu);
//        menu.findItem(R.id.action_search).setVisible(isLocationAvailable);
        menu.findItem(R.id.action_search).setVisible(false);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.grocery_home, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_search: {
                onVendorSearchClicked();
                return true;
            }

            case R.id.action_cart: {
                onCartClicked();
                return true;
            }
        }

        return super.onOptionsItemSelected(item);
    }

    private void init() {
        mSectionId = ((LockatedApplication) getActivity().getApplicationContext()).getmSectionId();

        mVendorData = new ArrayList<>();
        mLockatedPreferences = new LockatedPreferences(getActivity());
        mLocationUpdateManager = new LocationUpdateManager(getActivity(), this);

        //mEditTextFoodLocation = (EditText) mFoodFragmentView.findViewById(R.id.mEditTextFoodLocation);
        //======================================AutoComplete address=======================================
        mEditTextFoodLocation = (AutoCompleteTextView) mFoodFragmentView.findViewById(R.id.mEditTextFoodLocation);
        mEditTextFoodLocation.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus) {
                    mTextViewNoVendorFound.setVisibility(View.GONE);
                }
            }
        });
        mEditTextFoodLocation.setHorizontallyScrolling(true);
        mEditTextFoodLocation.setMovementMethod(new ScrollingMovementMethod());
        mEditTextFoodLocation.setAdapter(new GooglePlacesAutocompleteAdapter(getActivity()));
        mEditTextFoodLocation.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                String str = (String) parent.getItemAtPosition(position);
                LatLng latLng = getLocationFromAddress(getActivity(), str);
                hideSoftKeyboard();
                onLocationPointListener(latLng.latitude, latLng.longitude);
            }
        });
        //======================================AutoComplete address=======================================
        mImageViewCurrentLocation = (ImageView) mFoodFragmentView.findViewById(R.id.mImageViewCurrentLocation);
        mTextViewNoVendorFound = (TextView) mFoodFragmentView.findViewById(R.id.mTextViewNoVendorFound);
        mTextViewNoVendorFound.setVisibility(View.GONE);

        mProgressBarView = (ProgressBar) mFoodFragmentView.findViewById(R.id.mProgressBarView);
        mRecyclerView = (RecyclerView) mFoodFragmentView.findViewById(R.id.mRecyclerView);

        mEditTextFoodLocation.addTextChangedListener(this);
        mImageViewCurrentLocation.setOnClickListener(this);

        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        mRecyclerView.setLayoutManager(mLayoutManager);
        mRecyclerView.setHasFixedSize(true);

        mVendorAdapter = new BeautySupplierAdapter(mVendorData, this);
        mRecyclerView.setAdapter(mVendorAdapter);
    }

    private void onVendorSearchClicked() {
        VendorSearchFragment vendorSearchFragment = new VendorSearchFragment();
        getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.mVendorContainer, vendorSearchFragment).commit();
    }

    private void onCartClicked() {
        ArrayList<AccountData> accountDatas = AccountController.getInstance().getmAccountDataList();
        if (accountDatas.size() > 0) {
            ArrayList<CartDetail> cartDetails = accountDatas.get(0).getCartDetailList();
            if (cartDetails.size() > 0) {
                if (cartDetails.get(0).getmLineItemData().size() > 0) {
                    Intent intent = new Intent(getActivity(), EcommerceCartActivity.class);
                    startActivity(intent);
                } else {
                    Utilities.showToastMessage(getActivity(), getString(R.string.empty_cart));
                }
            } else {
                Utilities.showToastMessage(getActivity(), getString(R.string.empty_cart));
            }
        }
    }

    private void checkLocationPermission() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                mProgressBarView.setVisibility(View.GONE);
                requestPermissions(new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, REQUEST_LOCATION);
            } else {
                initializeLocationUpdater();
            }
        } else {
            initializeLocationUpdater();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == REQUEST_LOCATION) {
            if (grantResults.length == 1 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                if (mLocationUpdateManager.getGoogleApiClient() == null) {
                    mEditTextFoodLocation.setText("Getting your location...");
                    initializeLocationUpdater();
                }
            } else {
                mProgressBarView.setVisibility(View.GONE);
            }
        }
    }

    private void initializeLocationUpdater() {
        LocationManager mLocationManager = (LocationManager) getActivity().getSystemService(Context.LOCATION_SERVICE);

        if (mLocationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
            if (mLocationUpdateManager.getGoogleApiClient() == null) {
                if (Utilities.isPlayServiceAvailable(getActivity())) {
                    if (hasResetLocation) {
                        mLocationUpdateManager.buildGoogleApiClient(true);
                    } else {
                        mLocationUpdateManager.buildGoogleApiClient();
                    }
                } else {
                    Utilities.serviceUpdateError(getActivity(), "Google Play Service is not Updated. Would you like to update?");
                }
            }
        } else {
            if (getActivity() != null) {
                Utilities.alertGPSConnectionError(getActivity());
            }
        }
    }

    private void removeLocationUpdater() {
        if (mLocationUpdateManager.getGoogleApiClient() != null) {
            if (mLocationUpdateManager.isLocationUpdating()) {
                mLocationUpdateManager.setIsLocationUpdating(false);
                mLocationUpdateManager.stopLocationUpdates();
            }
            mLocationUpdateManager.getGoogleApiClient().disconnect();
            mLocationUpdateManager.releaseMemory();
        }
    }

    private void onLocationPointListener(double latitude, double longitude) {
        if (getActivity() != null) {
            if (ConnectionDetector.isConnectedToInternet(getActivity())) {
                mProgressBarView.setVisibility(View.VISIBLE);

                LockatedVolleyRequestQueue lockatedVolleyRequestQueue = LockatedVolleyRequestQueue.getInstance(getActivity());
                lockatedVolleyRequestQueue.sendRequest(GET_REQUEST_TAG, Request.Method.GET, ApplicationURL.getSupplierBylatLongUrl + mLockatedPreferences.getLockatedToken() + "&taxonomy_id=" + mSectionId + "&location[latitude]=" + latitude + "&location[longitude]=" + longitude, null, this, this);
            } else {
                Utilities.showToastMessage(getActivity(), getActivity().getResources().getString(R.string.internet_connection_error));
            }
        }
    }

    @Override
    public void onErrorResponse(VolleyError error) {
        if (getActivity() != null) {
            mProgressBarView.setVisibility(View.GONE);
            LockatedRequestError.onRequestError(getActivity(), error);
        }
    }

    @Override
    public void onResponse(Object response) {
        if (getActivity() != null) {
            mProgressBarView.setVisibility(View.GONE);
            JSONObject jsonObject = (JSONObject) response;
            try {
                JSONArray jsonArray = jsonObject.getJSONArray("suppliers");
                if (jsonArray.length() > 0) {
                    mVendorData.clear();

                    for (int i = 0; i < jsonArray.length(); i++) {
                        JSONObject addressObject = jsonArray.getJSONObject(i);
                        VendorData vendorData = new VendorData(addressObject);
                        mVendorData.add(vendorData);
                    }
                    if (mTextViewNoVendorFound.getVisibility() == View.VISIBLE) {
                        mTextViewNoVendorFound.setVisibility(View.GONE);
                    }
                } else {
                    mVendorData.clear();
                    mTextViewNoVendorFound.setVisibility(View.VISIBLE);
                }

                mVendorAdapter.notifyDataSetChanged();
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void onRecyclerItemClick(View view, int position) {
        switch (view.getId()) {
            case R.id.mLinearLayoutVendorRow:
                onVendorRowClicked(mVendorData.get(position).getName(), mVendorData.get(position).getId());
                break;
        }
    }

    private void onVendorRowClicked(String vendorName, int id) {
        Intent groceryIntent = new Intent(getActivity(), BeautyServiceActivity.class);
        ((LockatedApplication) getActivity().getApplicationContext()).setHeaderName(getString(R.string.beauty));
        ((LockatedApplication) getActivity().getApplicationContext()).setCategoryIdTwo(0);
        ((LockatedApplication) getActivity().getApplicationContext()).setCategoryId(id);
        startActivity(groceryIntent);
    }

    @Override
    public void onAddressUpdate(String address) {
        try {
            if (address != null) {
                JSONObject jsonObject = new JSONObject(address);
                LocationData locationData = new LocationData(jsonObject);
                mEditTextFoodLocation.setText(locationData.address + " " + locationData.getLocality());
                removeLocationUpdater();
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onSuccessUpdate(Bundle bundle) {
        hasResetLocation = false;
    }

    @Override
    public void onLocationUpdate(Location location) {
        if (getActivity() != null) {
            hasResetLocation = false;
            LocationAddress locationAddress = new LocationAddress();
            mLatitude = location.getLatitude();
            mLongitude = location.getLongitude();
            isLocationAvailable = true;
            getActivity().invalidateOptionsMenu();
            onLocationPointListener(mLatitude, mLongitude);
            locationAddress.getAddressFromLocation(mLatitude, mLongitude, getActivity(), new GeocoderHandler(this));
        }
    }

    @Override
    public void onFailedUpdate(ConnectionResult connectionResult) {
        hasResetLocation = false;
        Utilities.showToastMessage(getActivity(), "onFailedUpdate: " + connectionResult);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.mImageViewCurrentLocation:
                onCurrentLocationClicked();
                break;
        }
    }

    private void onCurrentLocationClicked() {
        if (getActivity() != null) {
            hasResetLocation = true;
            mVendorData.clear();
            mVendorAdapter.notifyDataSetChanged();
            mProgressBarView.setVisibility(View.VISIBLE);
            mTextViewNoVendorFound.setVisibility(View.GONE);
            isLocationAvailable = false;
            getActivity().invalidateOptionsMenu();
            checkLocationPermission();
        }
    }

    public void hideSoftKeyboard() {
        if (getActivity().getCurrentFocus() != null) {
            InputMethodManager inputMethodManager = (InputMethodManager) getActivity().getSystemService(getActivity().INPUT_METHOD_SERVICE);
            inputMethodManager.hideSoftInputFromWindow(getActivity().getCurrentFocus().getWindowToken(), 0);
        }
    }

    public LatLng getLocationFromAddress(Context context, String strAddress) {

        Geocoder coder = new Geocoder(context);
        List<Address> address;
        LatLng p1;

        try {
            address = coder.getFromLocationName(strAddress, 5);
            if (address == null) {
                return null;
            }
            Address location = address.get(0);
            location.getLatitude();
            location.getLongitude();
            p1 = new LatLng(location.getLatitude(), location.getLongitude());
            return p1;
        } catch (Exception e) {
            p1 = new LatLng(0.0, 0.0);
            e.printStackTrace();
        }
        return p1;
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {
        if (count == 0) {
            searchAutoComplete = false;
        } else {
            searchAutoComplete = true;
        }
    }

    @Override
    public void afterTextChanged(Editable s) {

    }
}