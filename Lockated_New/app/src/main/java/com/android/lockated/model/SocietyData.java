package com.android.lockated.model;

import org.json.JSONObject;

public class SocietyData
{
    private int id;
    private String building_name;
    private String url;
    private String address1;

    private String address2;
    private String area;
    private String postcode;
    private String city;
    private String latitude;
    private String longitude;
    private String state;
    private String country;

    public SocietyData(JSONObject jsonObject)
    {
        id = jsonObject.optInt("id");
        building_name = jsonObject.optString("building_name");
        url = jsonObject.optString("url");
        address1 = jsonObject.optString("address1");
        address2 = jsonObject.optString("address2");
        area = jsonObject.optString("area");
        postcode = jsonObject.optString("postcode");
        city = jsonObject.optString("city");
        latitude = jsonObject.optString("latitude");
        longitude = jsonObject.optString("longitude");
        state = jsonObject.optString("state");
        country = jsonObject.optString("country");
    }

    public int getId()
    {
        return id;
    }

    public void setId(int id)
    {
        this.id = id;
    }

    public String getBuilding_name()
    {
        return building_name;
    }

    public void setBuilding_name(String building_name)
    {
        this.building_name = building_name;
    }

    public String getUrl()
    {
        return url;
    }

    public void setUrl(String url)
    {
        this.url = url;
    }

    public String getAddress1()
    {
        return address1;
    }

    public void setAddress1(String address1)
    {
        this.address1 = address1;
    }

    public String getAddress2()
    {
        return address2;
    }

    public void setAddress2(String address2)
    {
        this.address2 = address2;
    }

    public String getArea()
    {
        return area;
    }

    public void setArea(String area)
    {
        this.area = area;
    }

    public String getPostcode()
    {
        return postcode;
    }

    public void setPostcode(String postcode)
    {
        this.postcode = postcode;
    }

    public String getCity()
    {
        return city;
    }

    public void setCity(String city)
    {
        this.city = city;
    }

    public String getLatitude()
    {
        return latitude;
    }

    public void setLatitude(String latitude)
    {
        this.latitude = latitude;
    }

    public String getLongitude()
    {
        return longitude;
    }

    public void setLongitude(String longitude)
    {
        this.longitude = longitude;
    }

    public String getState()
    {
        return state;
    }

    public void setState(String state)
    {
        this.state = state;
    }

    public String getCountry()
    {
        return country;
    }

    public void setCountry(String country)
    {
        this.country = country;
    }
}
