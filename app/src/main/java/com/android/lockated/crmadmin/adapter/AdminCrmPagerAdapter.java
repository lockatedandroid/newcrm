package com.android.lockated.crmadmin.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.android.lockated.crmadmin.fragment.PendingComplaintsFragment;
import com.android.lockated.crmadmin.fragment.adminEvents.AdminEventsFragment;
import com.android.lockated.crmadmin.fragment.AdminHelpDeskFragment;
import com.android.lockated.crmadmin.fragment.AdminMyZoneFragment;
import com.android.lockated.crmadmin.fragment.adminNotices.AdminNoticeBoardFragment;
import com.android.lockated.crmadmin.fragment.adminPolls.AdminPollListFragment;
import com.android.lockated.crmadmin.fragment.userManage.UserManageFragment;


public class AdminCrmPagerAdapter extends FragmentPagerAdapter {

    int tabCount;
    FragmentManager fragmentManager;

    public AdminCrmPagerAdapter(FragmentManager fm, int tabCount) {
        super(fm);
        this.tabCount = tabCount;
        fragmentManager = fm;
    }

    @Override
    public Fragment getItem(int pos) {

        Fragment fragment;

        switch (pos) {

            case 0:
                fragment = new UserManageFragment();
                break;
            case 1:
                fragment = new AdminNoticeBoardFragment();
                break;
            case 2:
                fragment = new AdminEventsFragment();
                break;
            case 3:
                fragment = new AdminPollListFragment();
                break;
            case 4:
                fragment = new PendingComplaintsFragment();
                //fragment = new AdminHelpDeskFragment();
                break;
            case 5:
                fragment = new AdminMyZoneFragment();
                break;
            default:
                fragment = new UserManageFragment();
                break;
        }

        return fragment;
    }

    @Override
    public int getCount() {
        return tabCount;
    }

    @Override
    public int getItemPosition(Object object) {
        return super.getItemPosition(object);
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return super.getPageTitle(position);
    }
}