package com.android.lockated.categories.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.android.lockated.Interfaces.IRecyclerItemClickListener;
import com.android.lockated.R;
import com.android.lockated.holder.RecyclerViewHolder;
import com.android.lockated.landing.model.TaxonData;

import java.util.ArrayList;

public class CategoryItemAdapter extends RecyclerView.Adapter<RecyclerViewHolder> {
    private ArrayList<TaxonData> mTaxonDatas;
    private IRecyclerItemClickListener mIRecyclerItemClickListener;
    String REQUEST_TAG;

    public CategoryItemAdapter(ArrayList<TaxonData> data, IRecyclerItemClickListener listener, String REQUEST_TAG) {
        mTaxonDatas = data;
        mIRecyclerItemClickListener = listener;
        this.REQUEST_TAG = REQUEST_TAG;
    }

    @Override
    public RecyclerViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.fragment_category_view_row, parent, false);
        return new RecyclerViewHolder(itemView, mIRecyclerItemClickListener);
    }

    @Override
    public void onBindViewHolder(RecyclerViewHolder holder, int position) {
        if (REQUEST_TAG.equals("BeautyAppointmentFragment")) {
            if ((mTaxonDatas.size() - position) != 1) {
                holder.onCategoryViewHolder(mTaxonDatas.get(position));
            }
        } else {
            holder.onCategoryViewHolder(mTaxonDatas.get(position));
        }
    }

    @Override
    public int getItemCount() {
        return mTaxonDatas.size();
    }
}