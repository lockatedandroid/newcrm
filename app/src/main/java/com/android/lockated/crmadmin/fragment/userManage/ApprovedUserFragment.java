package com.android.lockated.crmadmin.fragment.userManage;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.lockated.LockatedApplication;
import com.android.lockated.R;
import com.android.lockated.crmadmin.activity.AdminUserManageActivity;
import com.android.lockated.information.AccountController;
import com.android.lockated.model.AccountData;
import com.android.lockated.network.ConnectionDetector;
import com.android.lockated.preferences.LockatedPreferences;
import com.android.lockated.request.LockatedJSONObjectRequest;
import com.android.lockated.request.LockatedRequestError;
import com.android.lockated.request.LockatedVolleyRequestQueue;
import com.android.lockated.utils.ApplicationURL;
import com.android.lockated.utils.Utilities;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;


public class ApprovedUserFragment extends Fragment implements Response.ErrorListener, Response.Listener<JSONObject> {

    RecyclerView recyclerView;
    TextView errorMsg;
    ApprovedUserAdapter approvedUserAdapter;
    RequestQueue mQueue;
    String REQUEST_TAG = "ApprovedUserFragment";
    LockatedPreferences mLockatedPreferences;
    ProgressBar mProgressBarView;
    AccountController accountController;
    ArrayList<AccountData> data;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View pendingUser = inflater.inflate(R.layout.fragment_approved_user, container, false);
        mLockatedPreferences = new LockatedPreferences(getActivity());
        accountController = AccountController.getInstance();
        Utilities.ladooIntegration(getActivity(), getActivity().getResources().getString(R.string.approvedUser));
        data = accountController.getmAccountDataList();
        init(pendingUser);
        return pendingUser;
    }

    @Override
    public void onResume() {
        super.onResume();
        LockatedApplication.getInstance().trackScreenView(getString(R.string.approvedUser));
        Utilities.lockatedGoogleAnalytics(getString(R.string.approvedUser),
                getString(R.string.visited), getString(R.string.approvedUser));
        getApprovedUser();
    }

    private void init(View pendingUser) {
        recyclerView = (RecyclerView) pendingUser.findViewById(R.id.recyclerView);
        errorMsg = (TextView) pendingUser.findViewById(R.id.noData);
        mProgressBarView = (ProgressBar) pendingUser.findViewById(R.id.mProgressBarView);
        final LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerView.setLayoutManager(linearLayoutManager);
    }

    public void getApprovedUser() {
        if (!mLockatedPreferences.getSocietyId().equals("blank")) {
            if (ConnectionDetector.isConnectedToInternet(getActivity())) {
                mProgressBarView.setVisibility(View.VISIBLE);
                mQueue = LockatedVolleyRequestQueue.getInstance(getActivity()).getRequestQueue();
                LockatedJSONObjectRequest lockatedJSONObjectRequest = new LockatedJSONObjectRequest(Request.Method.GET,
                        ApplicationURL.getApprovedUser + mLockatedPreferences.getSocietyId() + "&token="
                                + mLockatedPreferences.getLockatedToken(), null, this, this);
                lockatedJSONObjectRequest.setTag(REQUEST_TAG);
                mQueue.add(lockatedJSONObjectRequest);
            } else {
                Utilities.showToastMessage(getActivity(), getActivity().getResources().getString(R.string.internet_connection_error));
            }
        } else {
            recyclerView.setVisibility(View.GONE);
            errorMsg.setVisibility(View.VISIBLE);
            errorMsg.setText(R.string.not_in_society);
        }

    }

    @Override
    public void onErrorResponse(VolleyError error) {
        mProgressBarView.setVisibility(View.INVISIBLE);
        if (getActivity() != null) {
            LockatedRequestError.onRequestError(getActivity(), error);
        }
    }

    @Override
    public void onResponse(JSONObject response) {
        mProgressBarView.setVisibility(View.INVISIBLE);
        if (getActivity() != null) {
            try {
                if (response != null && response.length() > 0 && response.getJSONArray("user_societies").length() > 0) {
                    approvedUserAdapter = new ApprovedUserAdapter(getActivity(), response.getJSONArray("user_societies"));
                    recyclerView.setAdapter(approvedUserAdapter);
                } else {
                    recyclerView.setVisibility(View.GONE);
                    errorMsg.setVisibility(View.VISIBLE);
                    errorMsg.setText(R.string.no_data_error);
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    public class ApprovedUserAdapter extends RecyclerView.Adapter<ApprovedUserAdapter.ApprovedUserViewHolder>
            implements Response.ErrorListener, Response.Listener<JSONObject> {

        Context contextMain;
        JSONArray jsonArray;
        RequestQueue mQueue;
        LockatedPreferences mLockatedPreferences;
        String REQUEST_TAG = "ApprovedUserAdapterSample";
        int removePosition;

        public ApprovedUserAdapter(Context context, JSONArray jsonArray) {
            this.contextMain = context;
            this.jsonArray = jsonArray;
            mLockatedPreferences = new LockatedPreferences(contextMain);
        }

        @Override
        public ApprovedUserViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View v = LayoutInflater.from(contextMain).inflate(R.layout.adapter_approved_user, parent, false);
            ApprovedUserViewHolder pvh = new ApprovedUserViewHolder(v);
            return pvh;
        }

        @Override
        public void onBindViewHolder(ApprovedUserViewHolder holder, final int position) {

            try {

                String userName = "blank";
                String myName = mLockatedPreferences.getAccountData().getFirstname() + " " + mLockatedPreferences.getAccountData().getLastname();

                if (!jsonArray.getJSONObject(position).isNull("user")
                        && jsonArray.getJSONObject(position).getJSONObject("user") != null &&
                        !jsonArray.getJSONObject(position).getJSONObject("user").toString().equals("null")) {
                    userName = jsonArray.getJSONObject(position).getJSONObject("user").getString("firstname")
                            + " " + jsonArray.getJSONObject(position).getJSONObject("user").getString("lastname");
                }

                holder.dateTxt.setText(ddMMYYYFormat(jsonArray.getJSONObject(position).getString("created_at")));
                if (!jsonArray.getJSONObject(position).isNull("user")
                        && jsonArray.getJSONObject(position).getJSONObject("user") != null &&
                        !jsonArray.getJSONObject(position).getJSONObject("user").toString().equals("null")) {
                    String name = jsonArray.getJSONObject(position).getJSONObject("user").getString("firstname")
                            + " " + jsonArray.getJSONObject(position).getJSONObject("user").getString("lastname");
                    holder.nameTxt.setText(name);
                    holder.phoneNo.setText(jsonArray.getJSONObject(position).getJSONObject("user").getString("mobile"));

                }
                if (jsonArray.getJSONObject(position).getJSONObject("user_flat").length() != 0
                        && !jsonArray.getJSONObject(position).isNull("user_flat")) {
                    holder.flateNo.setText(jsonArray.getJSONObject(position).getJSONObject("user_flat").getString("flat"));
                } else {
                    holder.flateNo.setText("No flat no");
                }
                final String id = jsonArray.getJSONObject(position).getString("id");
                if (!myName.equals(userName)) {
                    holder.disable.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {

                            JSONObject jsonObjectMain = new JSONObject();
                            JSONObject jsonObjectSub = new JSONObject();
                            try {
                                jsonObjectSub.put("approve", 0);
                                jsonObjectMain.put("user_society", jsonObjectSub);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                            removePosition = position;
                            getRequest("http://lockated.com/user_societies/" + id + ".json?token=", jsonObjectMain);
                        }
                    });
                } else {
                    holder.mainLinearView.setVisibility(View.GONE);
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }

        }

        @Override
        public int getItemCount() {
            return jsonArray.length();
        }

        @Override
        public void onAttachedToRecyclerView(RecyclerView recyclerView) {
            super.onAttachedToRecyclerView(recyclerView);
        }

        public class ApprovedUserViewHolder extends RecyclerView.ViewHolder {

            TextView nameTxt, dateTxt, phoneNo, flateNo, disable, decline;
            LinearLayout mainLinearView;

            public ApprovedUserViewHolder(View itemView) {
                super(itemView);

                nameTxt = (TextView) itemView.findViewById(R.id.nameValue);
                dateTxt = (TextView) itemView.findViewById(R.id.mTextDate);
                disable = (TextView) itemView.findViewById(R.id.disable);
                decline = (TextView) itemView.findViewById(R.id.mContactAuthor);
                phoneNo = (TextView) itemView.findViewById(R.id.phoneValue);
                flateNo = (TextView) itemView.findViewById(R.id.flatValue);
                mainLinearView = (LinearLayout) itemView.findViewById(R.id.mainView);

            }

        }

        public void getRequest(String url, JSONObject jsonObject) {

            if (!mLockatedPreferences.getSocietyId().equals("blank")) {
                if (ConnectionDetector.isConnectedToInternet(contextMain)) {
                    String mainUrl = url + mLockatedPreferences.getLockatedToken();
                    LockatedVolleyRequestQueue lockatedVolleyRequestQueue = LockatedVolleyRequestQueue.getInstance(contextMain);
                    lockatedVolleyRequestQueue.sendRequest(REQUEST_TAG, Request.Method.PUT, mainUrl, jsonObject, this, this);
                    /*mQueue = LockatedVolleyRequestQueue.getInstance(contextMain).getRequestQueue();
                    LockatedJSONObjectRequest lockatedJSONObjectRequest = new LockatedJSONObjectRequest(Request.Method.PUT,
                            url + mLockatedPreferences.getLockatedToken(), jsonObject, this, this);
                    *//*Log.e("remove url", "" + url + mLockatedPreferences.getLockatedToken());
                    Log.e("remove url", " jsonObject " + jsonObject);*//*
                    lockatedJSONObjectRequest.setTag(REQUEST_TAG);
                    mQueue.add(lockatedJSONObjectRequest);*/
                } else {
                    Utilities.showToastMessage(contextMain, contextMain.getResources().getString(R.string.internet_connection_error));
                }
            } else {
                Toast.makeText(contextMain, "You are not associated with any society", Toast.LENGTH_LONG).show();
            }
        }

        @Override
        public void onErrorResponse(VolleyError error) {
            if (contextMain != null) {
                LockatedRequestError.onRequestError(contextMain, error);
            }
        }

        @Override
        public void onResponse(JSONObject response) {
            if (contextMain != null) {
                if (response != null && response.length() > 0) {
                    try {
                        //Log.e("AppUsr response", ""+response);
                        Intent intent = new Intent(contextMain, AdminUserManageActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        intent.putExtra("AdminUserPosition", 1);
                        contextMain.startActivity(intent);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            } else {

            }
        }

        public String ddMMYYYFormat(String dateStr) {
            if (dateStr != null && !dateStr.equals("null")) {
                DateFormat format = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);
                Date date;
                try {
                    date = format.parse(dateStr);
                    dateStr = new SimpleDateFormat("yyyy-MM-dd").format(date);
                } catch (ParseException e) {
                    e.printStackTrace();
                    dateStr = "No Date";
                }
            } else {
                dateStr = "No Date";
            }
            return dateStr;
        }

    }

}
