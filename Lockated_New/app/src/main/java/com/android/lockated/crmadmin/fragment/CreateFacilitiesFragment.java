package com.android.lockated.crmadmin.fragment;


import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.lockated.R;
import com.android.lockated.crmadmin.activity.AdminFacilitiesActivity;
import com.android.lockated.information.AccountController;
import com.android.lockated.model.AccountData;
import com.android.lockated.network.ConnectionDetector;
import com.android.lockated.preferences.LockatedPreferences;
import com.android.lockated.request.LockatedRequestError;
import com.android.lockated.request.LockatedVolleyRequestQueue;
import com.android.lockated.utils.ApplicationURL;
import com.android.lockated.utils.Utilities;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class CreateFacilitiesFragment extends Fragment implements View.OnClickListener,
        Response.Listener<JSONObject>, Response.ErrorListener {

    private View mFunctionCreateView;
    FragmentManager fragmentManager;
    private EditText amenitiesName;
    private EditText amenitiesCost;
    private EditText amenitiesCostType;
    private EditText amenitiesDescription;
    private TextView amenitiesSubmit;
    private RequestQueue mQueue;
    private ProgressBar mProgressBarView;
    private LockatedPreferences mLockatedPreferences;
    AccountController accountController;
    ArrayList<AccountData> accountDataArrayList;
    int societyId;
    public static final String CREATE_FACILITIES = "CreateFacilitiesFragment";

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    public FragmentManager setFragmentManager(FragmentManager fragmentManager) {
        this.fragmentManager = fragmentManager;
        return fragmentManager;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mFunctionCreateView = inflater.inflate(R.layout.fragment_create_facilities, container, false);
        init();
        return mFunctionCreateView;
    }

    private void init() {
        mLockatedPreferences = new LockatedPreferences(getActivity());
        mProgressBarView = (ProgressBar) mFunctionCreateView.findViewById(R.id.mProgressBarView);
        amenitiesName = (EditText) mFunctionCreateView.findViewById(R.id.amenityNameValue);
        amenitiesCost = (EditText) mFunctionCreateView.findViewById(R.id.amenityCostValue);
        amenitiesCostType = (EditText) mFunctionCreateView.findViewById(R.id.amenityCostTypeValue);
        amenitiesDescription = (EditText) mFunctionCreateView.findViewById(R.id.amenityDescriptionValue);
        amenitiesSubmit = (TextView) mFunctionCreateView.findViewById(R.id.amenitySubmitText);
        amenitiesSubmit.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.amenitySubmitText:
                if (amenitiesName.getText().toString().length() > 0
                        && amenitiesCost.getText().toString().length() > 0
                        && amenitiesDescription.getText().toString().length() > 0) {
                    createFacilitiesApi(amenitiesName.getText().toString(), amenitiesCost.getText().toString(),
                            amenitiesDescription.getText().toString(), amenitiesCostType.getText().toString());
                } else {
                    Toast.makeText(getActivity(), "Please fill all fields", Toast.LENGTH_LONG).show();
                }
                break;
        }
    }

    private void createFacilitiesApi(String name, String cost, String description, String costType) {
        if (ConnectionDetector.isConnectedToInternet(getActivity())) {
            mProgressBarView.setVisibility(View.VISIBLE);
            String url = ApplicationURL.createFacilitiesUrl + mLockatedPreferences.getLockatedToken()
                    + ApplicationURL.societyId + mLockatedPreferences.getSocietyId();
            LockatedVolleyRequestQueue mLockatedVolleyRequestQueue = LockatedVolleyRequestQueue.getInstance(getActivity());
            JSONObject jsonObject = new JSONObject();
            JSONObject jsonObjectMain = new JSONObject();
            try {
                jsonObject.put("society_id", mLockatedPreferences.getSocietyId());
                jsonObject.put("name", name);
                jsonObject.put("description", description);
                jsonObject.put("user_society_id", "" + mLockatedPreferences.getUserSocietyId());
                jsonObject.put("cost", cost);
                jsonObject.put("cost_type", costType);
                jsonObjectMain.put("amenity", jsonObject);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            mLockatedVolleyRequestQueue.sendRequest(CREATE_FACILITIES, Request.Method.POST, url, jsonObjectMain, this, this);
        } else {
            Utilities.showToastMessage(getActivity(), getActivity().getResources().getString(R.string.internet_connection_error));
        }
    }

    @Override
    public void onErrorResponse(VolleyError error) {
        mProgressBarView.setVisibility(View.GONE);
        if (getActivity() != null) {
            LockatedRequestError.onRequestError(getActivity(), error);
        }
    }

    @Override
    public void onResponse(JSONObject response) {
        mProgressBarView.setVisibility(View.GONE);
        if (response.has("id")) {
            Intent intent = new Intent(getActivity(), AdminFacilitiesActivity.class);
            intent.putExtra("AdminFacilitiesActivity", 1);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            getActivity().startActivity(intent);
        } else {
            Toast.makeText(getActivity(), "Something went wrong", Toast.LENGTH_LONG).show();
        }

    }

}