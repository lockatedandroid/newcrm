package com.android.lockated.account.activity;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.LinearLayout;

import com.android.lockated.R;
import com.android.lockated.account.fragment.SocietyDetailFragment;

public class AddNewSocietyActivity extends AppCompatActivity {

    String className = "AddNewSocietyActivity";
    LinearLayout societyContainer;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_society);

        setToolBar("Add Society");
        init();
        callSocietyFragment();
    }

    private void setToolBar(String name) {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(true);
        getSupportActionBar().setTitle(name);
    }

    private void init() {
        societyContainer = (LinearLayout) findViewById(R.id.societyContainer);
    }

    private void callSocietyFragment() {
        SocietyDetailFragment societyDetailFragment = new SocietyDetailFragment();
        societyDetailFragment.setBottomBar(0, className);
        getSupportFragmentManager().beginTransaction().add(R.id.societyContainer, societyDetailFragment).commit();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home: {
                super.onBackPressed();
                return true;
            }
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onBackPressed() {
        //super.onBackPressed();
        AddNewSocietyActivity.this.finish();
    }
}
