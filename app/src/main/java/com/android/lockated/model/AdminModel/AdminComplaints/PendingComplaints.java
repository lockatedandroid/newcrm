
package com.android.lockated.model.AdminModel.AdminComplaints;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class PendingComplaints {

    @SerializedName("complaints")
    @Expose
    private ArrayList<Complaint> complaints = new ArrayList<Complaint>();
    @SerializedName("issue_status")
    @Expose
    private ArrayList<String> issueStatus = new ArrayList<String>();
    @SerializedName("issue_type")
    @Expose
    private ArrayList<String> issueType = new ArrayList<String>();

    /**
     * 
     * @return
     *     The complaints
     */
    public ArrayList<Complaint> getComplaints() {
        return complaints;
    }

    /**
     * 
     * @param complaints
     *     The complaints
     */
    public void setComplaints(ArrayList<Complaint> complaints) {
        this.complaints = complaints;
    }

    /**
     * 
     * @return
     *     The issueStatus
     */
    public ArrayList<String> getIssueStatus() {
        return issueStatus;
    }

    /**
     * 
     * @param issueStatus
     *     The issue_status
     */
    public void setIssueStatus(ArrayList<String> issueStatus) {
        this.issueStatus = issueStatus;
    }

    /**
     * 
     * @return
     *     The issueType
     */
    public ArrayList<String> getIssueType() {
        return issueType;
    }

    /**
     * 
     * @param issueType
     *     The issue_type
     */
    public void setIssueType(ArrayList<String> issueType) {
        this.issueType = issueType;
    }

}
