package com.android.lockated.crm.fragment.polls;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.android.lockated.LockatedApplication;
import com.android.lockated.R;
import com.android.lockated.crm.activity.CreateNoticeListPolls;
import com.android.lockated.crm.adapters.PollsAdapter;
import com.android.lockated.information.AccountController;
import com.android.lockated.model.AccountData;
import com.android.lockated.model.usermodel.Polls.AllPolls.AllPoll;
import com.android.lockated.network.ConnectionDetector;
import com.android.lockated.preferences.LockatedPreferences;
import com.android.lockated.request.LockatedJSONObjectRequest;
import com.android.lockated.request.LockatedRequestError;
import com.android.lockated.request.LockatedVolleyRequestQueue;
import com.android.lockated.utils.ApplicationURL;
import com.android.lockated.utils.Utilities;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class AllPollFragment extends Fragment implements View.OnClickListener, Response.ErrorListener, Response.Listener<JSONObject> {
    TextView errorMsg;
    String societyid;
    ProgressBar progressBar;
    RecyclerView pollsList;
    FloatingActionButton fab;
    JSONObject noticeJsonObj;
    PollsAdapter pollsAdapter;
    private RequestQueue mQueue;
    AccountController accountController;
    ArrayList<AccountData> accountDataArrayList;
    ArrayList<AllPoll> allPolls;
    private LockatedPreferences mLockatedPreferences;
    String SHOW, CREATE, INDEX, UPDATE, EDIT;
    public static final String REQUEST_TAG = "AllPollFragment";

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View pollView = inflater.inflate(R.layout.fragment_poll, container, false);
        Utilities.ladooIntegration(getActivity(), "All Polls");
        init(pollView);
        return pollView;

    }

    @Override
    public void onResume() {
        super.onResume();
        LockatedApplication.getInstance().trackScreenView(getString(R.string.all_polls));
        Utilities.lockatedGoogleAnalytics(getString(R.string.all_polls),
                getString(R.string.visited), getString(R.string.all_polls));
        allPolls.clear();
        //getAllPolls();
    }

    private void init(View view) {

        allPolls = new ArrayList<>();
        mLockatedPreferences = new LockatedPreferences(getActivity());
        pollsList = (RecyclerView) view.findViewById(R.id.noticeList);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        pollsList.setLayoutManager(linearLayoutManager);
        errorMsg = (TextView) view.findViewById(R.id.noNotices);
        progressBar = (ProgressBar) view.findViewById(R.id.mProgressBarView);

        fab = (FloatingActionButton) view.findViewById(R.id.fab);
        fab.setOnClickListener(this);
        pollsAdapter = new PollsAdapter(getActivity(), allPolls, getChildFragmentManager());
        pollsList.setAdapter(pollsAdapter);
        getPollsRoll();
        checkPermission();

        /*try {

            noticeJsonObj = new JSONObject(mLockatedPreferences.getRolesJson());
            userType = noticeJsonObj.getString("name");
            if (!userType.equals("admin") || !userType.equals("society_admin")) {
                show = noticeJsonObj.getJSONArray("permissions").getJSONObject(8).getJSONObject("permission").getString("show");
                create = noticeJsonObj.getJSONArray("permissions").getJSONObject(8).getJSONObject("permission").getString("create");
                update = noticeJsonObj.getJSONArray("permissions").getJSONObject(8).getJSONObject("permission").getString("update");
                destroy = noticeJsonObj.getJSONArray("permissions").getJSONObject(8).getJSONObject("permission").getString("destroy");
            }
        } catch (JSONException e) {
            e.printStackTrace();
            create = "false";
            update = "false";
            show = "false";
            destroy = "false";
        }*/
    }

    private void checkPermission() {
        if (CREATE!=null&&CREATE.equals("true")) {
            fab.setVisibility(View.VISIBLE);
        } else {
            fab.setVisibility(View.GONE);
        }
        if (SHOW!=null&&SHOW.equals("true")) {
            callApi();
        } else {
            pollsList.setVisibility(View.GONE);
            errorMsg.setVisibility(View.VISIBLE);
            errorMsg.setText(R.string.no_permission_error);
        }
    }

    public void callApi() {
       /* accountController = AccountController.getInstance();
        accountDataArrayList = accountController.getmAccountDataList();
        societyid = accountDataArrayList.get(0).getmResidenceDataList().get(0).getSociety_id();*/
        societyid = mLockatedPreferences.getSocietyId();
        if (ConnectionDetector.isConnectedToInternet(getActivity())) {
            progressBar.setVisibility(View.VISIBLE);
            mQueue = LockatedVolleyRequestQueue.getInstance(getActivity()).getRequestQueue();
            String url = ApplicationURL.getPollsUrl + societyid + "&token=" + mLockatedPreferences.getLockatedToken();
            LockatedJSONObjectRequest lockatedJSONObjectRequest = new LockatedJSONObjectRequest(Request.Method.GET,
                    url, null, this, this);
            lockatedJSONObjectRequest.setTag(REQUEST_TAG);
            mQueue.add(lockatedJSONObjectRequest);
        } else {
            Utilities.showToastMessage(getActivity(), getActivity().getResources().getString(R.string.internet_connection_error));
        }
    }

    @Override
    public void onErrorResponse(VolleyError error) {
        if (getActivity() != null) {
            progressBar.setVisibility(View.GONE);
            LockatedRequestError.onRequestError(getActivity(), error);
        }
    }

    @Override
    public void onResponse(JSONObject response) {
        if (getActivity() != null) {
            progressBar.setVisibility(View.GONE);
            try {
                if (response != null && response.has("polls") && response.getJSONArray("polls").length() > 0) {
                    JSONArray jsonArray = response.getJSONArray("polls");
                    Gson gson = new Gson();
                    for (int i = 0; i < jsonArray.length(); i++) {
                        AllPoll myPoll = gson.fromJson(jsonArray.getJSONObject(i).toString(), AllPoll.class);
                        allPolls.add(myPoll);
                    }
                    pollsAdapter.notifyDataSetChanged();
                } else {
                    pollsList.setVisibility(View.GONE);
                    errorMsg.setVisibility(View.VISIBLE);
                    errorMsg.setText(R.string.no_data_error);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            pollsList.setVisibility(View.GONE);
            errorMsg.setVisibility(View.VISIBLE);
            errorMsg.setText(R.string.no_data_error);
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.fab:
                Intent intent = new Intent(getActivity(), CreateNoticeListPolls.class);
                startActivity(intent);
                break;
        }
    }

    public String getPollsRoll() {
        String mySocietyRoles = getActivity().getResources().getString(R.string.blank_value);
        if (mLockatedPreferences.getRolesJson() != null && !mLockatedPreferences.getRolesJson().equals("blank")) {
            try {
                JSONObject noticeJsonObj = new JSONObject(mLockatedPreferences.getRolesJson());
                for (int i = 0; i < noticeJsonObj.getJSONArray(getActivity().getResources().getString(R.string.permissions_value)).length(); i++) {
                    JSONObject jsonObject1 = noticeJsonObj.getJSONArray(getActivity().getResources().getString(R.string.permissions_value)).getJSONObject(i);
                    if (jsonObject1.has(getActivity().getResources().getString(R.string.section_value))
                            && jsonObject1.getString(getActivity().getResources().getString(R.string.section_value)).equals("spree_polls")) {
                        if (jsonObject1.getJSONObject(getActivity().getResources().getString(R.string.permission_value)).has(getActivity().getResources().getString(R.string.index_value))) {
                            mySocietyRoles = jsonObject1.getJSONObject(getActivity().getResources().getString(R.string.permission_value)).getString(getActivity().getResources().getString(R.string.index_value));
                            CREATE = jsonObject1.getJSONObject(getActivity().getResources().getString(R.string.permission_value)).getString("create");
                            INDEX = jsonObject1.getJSONObject(getActivity().getResources().getString(R.string.permission_value)).getString("index");
                            UPDATE = jsonObject1.getJSONObject(getActivity().getResources().getString(R.string.permission_value)).getString("update");
                            EDIT = jsonObject1.getJSONObject(getActivity().getResources().getString(R.string.permission_value)).getString("edit");
                            SHOW = jsonObject1.getJSONObject(getActivity().getResources().getString(R.string.permission_value)).getString("show");
                        }
                    }
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return mySocietyRoles;
    }
}
