package com.android.lockated.categories.medicine.fragment;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.lockated.LockatedApplication;
import com.android.lockated.R;
import com.android.lockated.account.fragment.MyAddressFragment;
import com.android.lockated.categories.medicine.MedicineActivity;
import com.android.lockated.information.ServiceController;
import com.android.lockated.model.MyAddressData;
import com.android.lockated.network.ConnectionDetector;
import com.android.lockated.preferences.LockatedPreferences;
import com.android.lockated.request.LockatedRequestError;
import com.android.lockated.request.LockatedVolleyRequestQueue;
import com.android.lockated.utils.ApplicationURL;
import com.android.lockated.utils.DatePickerFragment;
import com.android.lockated.utils.TimePickerFragment;
import com.android.lockated.utils.Utilities;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;

import org.json.JSONException;
import org.json.JSONObject;

public class MedicineDetailFragment extends Fragment implements View.OnClickListener, Response.Listener, Response.ErrorListener {
    private EditText mEditTextServiceTitle;
    private EditText mEditTextServiceDescription;

    private TextView mTextViewServiceDeliveryAddress;
    private TextView mTextViewServiceDeliveryAddressChange;

    private TextView mTextViewServiceDetailSubmit;
    private TextView mTextViewServicePreferredDeliveryTime;
    private TextView mTextViewServicePreferredDeliveryDate;

    private ImageView mImageViewServiceDeliveryTime;
    private ImageView mImageViewServiceDeliveryDate;

    private ProgressDialog mProgressDialog;

    private MyAddressData myAddressData;
    private ServiceController mServiceController;
    private LockatedPreferences mLockatedPreferences;

    private String selectedDate = "";
    private String selectedTime = "";

    private static final String REQUEST_TAG = "ServiceDetailFragment";
    String screenName = "Medicine Detail Screen";

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View serviceDetailView = inflater.inflate(R.layout.fragment_service_detail, container, false);
        Utilities.ladooIntegration(getActivity(), screenName);
        Utilities.lockatedGoogleAnalytics(screenName, getString(R.string.visited), screenName);
        init(serviceDetailView);
        refreshValues();
        return serviceDetailView;
    }

    private void init(View serviceDetailView) {
        mServiceController = ServiceController.getInstance();
        mLockatedPreferences = new LockatedPreferences(getActivity());

        mEditTextServiceTitle = (EditText) serviceDetailView.findViewById(R.id.mEditTextServiceTitle);
        mEditTextServiceDescription = (EditText) serviceDetailView.findViewById(R.id.mEditTextServiceDescription);

        mTextViewServiceDeliveryAddress = (TextView) serviceDetailView.findViewById(R.id.mTextViewServiceDeliveryAddress);
        mTextViewServiceDeliveryAddressChange = (TextView) serviceDetailView.findViewById(R.id.mTextViewServiceDeliveryAddressChange);

        mTextViewServiceDetailSubmit = (TextView) serviceDetailView.findViewById(R.id.mTextViewServiceDetailSubmit);
        mTextViewServicePreferredDeliveryTime = (TextView) serviceDetailView.findViewById(R.id.mTextViewServicePreferredDeliveryTime);
        mTextViewServicePreferredDeliveryDate = (TextView) serviceDetailView.findViewById(R.id.mTextViewServicePreferredDeliveryDate);

        mImageViewServiceDeliveryTime = (ImageView) serviceDetailView.findViewById(R.id.mImageViewServiceDeliveryTime);
        mImageViewServiceDeliveryDate = (ImageView) serviceDetailView.findViewById(R.id.mImageViewServiceDeliveryDate);

        mTextViewServiceDetailSubmit.setOnClickListener(this);
        mImageViewServiceDeliveryTime.setOnClickListener(this);
        mImageViewServiceDeliveryDate.setOnClickListener(this);
        mTextViewServiceDeliveryAddress.setOnClickListener(this);
        mTextViewServiceDeliveryAddressChange.setOnClickListener(this);
    }

    @Override
    public void onPause() {
        super.onPause();
        mServiceController.setServiceTitle(mEditTextServiceTitle.getText().toString());
        mServiceController.setServiceDescription(mEditTextServiceDescription.getText().toString());
    }

    private void refreshValues() {
        setInputDetails();
        setAddressDetails();
    }

    private void setInputDetails() {
        mEditTextServiceTitle.setText(mServiceController.getServiceTitle());
        mEditTextServiceDescription.setText(mServiceController.getServiceDescription());
    }

    private void setAddressDetails() {
        myAddressData = ((LockatedApplication) getActivity().getApplicationContext()).getMyAddressData();
        selectedDate = ((LockatedApplication) getActivity().getApplicationContext()).getmDeliveryDate();
        selectedTime = ((LockatedApplication) getActivity().getApplicationContext()).getmDeliveryTime();
        if (myAddressData != null) {
            mTextViewServiceDeliveryAddressChange.setVisibility(View.VISIBLE);
            mTextViewServiceDeliveryAddress.setText(myAddressData.getFirstName() + " " + myAddressData.getLastName() + "\n\n" + myAddressData.getAddress1() + " " + myAddressData.getAddress2() + " " + myAddressData.getCity() + " " + myAddressData.getState() + " " + myAddressData.getZipcode());
        }

        mTextViewServicePreferredDeliveryDate.setText(selectedDate);
        mTextViewServicePreferredDeliveryTime.setText(selectedTime);
    }

    private void onSelectAddressClicked() {
        myAddressData = ((LockatedApplication) getActivity().getApplicationContext()).getMyAddressData();
        if (myAddressData == null) {
            MyAddressFragment myAddressFragment = new MyAddressFragment();
            myAddressFragment.setViewId(1);
            getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.mVendorContainer, myAddressFragment).addToBackStack("Checkout").commit();
        } else {
            mTextViewServiceDeliveryAddressChange.setVisibility(View.VISIBLE);
            mTextViewServiceDeliveryAddress.setText(myAddressData.getFirstName() + " " + myAddressData.getLastName() + "\n\n" + myAddressData.getAddress1() + " " + myAddressData.getAddress2() + " " + myAddressData.getCity() + " " + myAddressData.getState() + " " + myAddressData.getZipcode());
        }
    }

    private void onAddressChangeClicked() {
        MyAddressFragment myAddressFragment = new MyAddressFragment();
        myAddressFragment.setViewId(1);
        getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.mVendorContainer, myAddressFragment).addToBackStack("Checkout").commit();
    }

    private void openDatePicker() {
        DatePickerFragment datePickerFragment = new DatePickerFragment();
        datePickerFragment.setDatePickerView(mTextViewServicePreferredDeliveryDate);
        DialogFragment newFragment = datePickerFragment;
        newFragment.show(getActivity().getSupportFragmentManager(), getActivity().getString(R.string.date_picker));
    }

    private void onDeliveryTimeClicked() {
        TimePickerFragment timePickerFragment = new TimePickerFragment();
        timePickerFragment.setTimePickerView(mTextViewServicePreferredDeliveryTime);
        DialogFragment newFragment = timePickerFragment;
        newFragment.show(getActivity().getSupportFragmentManager(), getActivity().getString(R.string.time_picker));
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.mImageViewServiceDeliveryTime:
                onDeliveryTimeClicked();
                break;

            case R.id.mImageViewServiceDeliveryDate:
                openDatePicker();
                break;

            case R.id.mTextViewServiceDeliveryAddress:
                onSelectAddressClicked();
                break;

            case R.id.mTextViewServiceDeliveryAddressChange:
                onAddressChangeClicked();
                break;

            case R.id.mTextViewServiceDetailSubmit:
                onServiceDetailSubmit();
                break;

        }
    }

    private void onServiceDetailSubmit() {
        String strPreferredDate = mTextViewServicePreferredDeliveryDate.getText().toString();
        String strPreferredTime = mTextViewServicePreferredDeliveryTime.getText().toString();

        String strMessage = "";
        boolean serviceCancel = false;

        if (TextUtils.isEmpty(strPreferredDate)) {
            serviceCancel = true;
            strMessage = "Please select preferred date";
        }

        if (TextUtils.isEmpty(strPreferredTime)) {
            serviceCancel = true;
            strMessage = "Please select preferred time";
        }

        if (serviceCancel) {
            Utilities.showToastMessage(getActivity(), strMessage);
        } else {
            if (ConnectionDetector.isConnectedToInternet(getActivity())) {
                if (myAddressData != null) {
                    mProgressDialog = ProgressDialog.show(getActivity(), "", "Please wait...", false);

                    int categoryId = mServiceController.getCategoryId();
                    int subCategoryId = mServiceController.getSubCategoryId();

                    JSONObject jsonObject = new JSONObject();

                    try {
                        jsonObject.put("category_id", categoryId);
                        jsonObject.put("sub_category_id", subCategoryId);
                        jsonObject.put("pdate", mTextViewServicePreferredDeliveryDate.getText().toString());
                        jsonObject.put("ptime", mTextViewServicePreferredDeliveryTime.getText().toString());
                        jsonObject.put("ship_address_id", myAddressData.getAddressId());
                        jsonObject.put("bill_address_id", myAddressData.getAddressId());
                        jsonObject.put("prescription", mServiceController.getPrescriptionEncodedImage());
                        jsonObject.put("title", "");
                        jsonObject.put("description", mEditTextServiceDescription.getText().toString());
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    String url = ApplicationURL.submitServiceDataUrl + mLockatedPreferences.getLockatedToken();
                    LockatedVolleyRequestQueue lockatedVolleyRequestQueue = LockatedVolleyRequestQueue.getInstance(getActivity());
                    lockatedVolleyRequestQueue.sendRequest(REQUEST_TAG, Request.Method.POST,
                            url, jsonObject, this, this);
                } else {
                    Utilities.showToastMessage(getActivity(), "Please select address to continue.");
                }
            } else {
                Utilities.showToastMessage(getActivity(), getActivity().getResources().getString(R.string.internet_connection_error));
            }
        }
    }

    @Override
    public void onErrorResponse(VolleyError error) {
        if (mProgressDialog != null && mProgressDialog.isShowing()) {
            mProgressDialog.dismiss();
        }
        if (getActivity() != null) {
            LockatedRequestError.onRequestError(getActivity(), error);
        }
    }

    @Override
    public void onResponse(Object response) {

        if (mProgressDialog != null && mProgressDialog.isShowing()) {
            mProgressDialog.dismiss();
        }

        JSONObject jsonObject = (JSONObject) response;

        try {
            int code = jsonObject.optInt("code");
            if (code == 200) {
                mServiceController.resetData();
                ((LockatedApplication) getActivity().getApplicationContext()).setMyAddressData(null);
                ((LockatedApplication) getActivity().getApplicationContext()).setmDeliveryTime("");
                ((LockatedApplication) getActivity().getApplicationContext()).setmDeliveryDate("");
                ((MedicineActivity) getActivity()).addCongratulationScreen(jsonObject.optInt("id") + "");
            } else {
                Utilities.showToastMessage(getActivity(), jsonObject.getString("error"));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
