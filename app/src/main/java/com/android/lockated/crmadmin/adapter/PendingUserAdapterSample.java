package com.android.lockated.crmadmin.adapter;


import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.android.lockated.R;
import com.android.lockated.network.ConnectionDetector;
import com.android.lockated.preferences.LockatedPreferences;
import com.android.lockated.request.LockatedJSONObjectRequest;
import com.android.lockated.request.LockatedRequestError;
import com.android.lockated.request.LockatedVolleyRequestQueue;
import com.android.lockated.utils.ApplicationURL;
import com.android.lockated.utils.Utilities;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class PendingUserAdapterSample extends RecyclerView.Adapter<PendingUserAdapterSample.PendingUserAdapterViewHolder> implements Response.ErrorListener, Response.Listener<JSONObject> {

    Context contextMain;
    JSONArray jsonArray;
    RequestQueue mQueue;
    LockatedPreferences mLockatedPreferences;
    String REQUEST_TAG = "PendingUserAdapterSample";

    public PendingUserAdapterSample(Context context, JSONArray jsonArray) {
        this.contextMain = context;
        this.jsonArray = jsonArray;
        mLockatedPreferences = new LockatedPreferences(contextMain);
    }

    @Override
    public PendingUserAdapterViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(contextMain).inflate(R.layout.adapter_pending_user, parent, false);
        PendingUserAdapterViewHolder pvh = new PendingUserAdapterViewHolder(v);
        return pvh;
    }

    @Override
    public void onBindViewHolder(PendingUserAdapterViewHolder holder, int position) {

        try {

            holder.dateTxt.setText(Utilities.ddMMYYYFormat(jsonArray.getJSONObject(position).getString("created_at")));
            String name = jsonArray.getJSONObject(position).getJSONObject("user").getString("firstname")
                    + " " + jsonArray.getJSONObject(position).getJSONObject("user").getString("lastname");
            holder.nameTxt.setText(name);

            final String id = jsonArray.getJSONObject(position).getString("id");

            holder.accept.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    JSONObject jsonObjectMain = new JSONObject();
                    JSONObject jsonObjectSub = new JSONObject();
                    try {
                        jsonObjectSub.put("approve", "1");
                        jsonObjectSub.put("admin_id", "");
                        jsonObjectMain.put("user_society", jsonObjectSub);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                    //getRequest("http://lockated.com/user_societies/" + id + ".json?token=", jsonObjectMain);
                    getRequest(ApplicationURL.approveDisapproveUser + id + ".json?token=", jsonObjectMain);
                }
            });

            holder.decline.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    JSONObject jsonObjectMain = new JSONObject();
                    JSONObject jsonObjectSub = new JSONObject();
                    try {
                        jsonObjectSub.put("approve", "0");
                        jsonObjectMain.put("user_society", jsonObjectSub);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                    //getRequest("http://lockated.com/user_societies/" + id + ".json?token=", jsonObjectMain);
                    getRequest(ApplicationURL.approveDisapproveUser + id + ".json?token=", jsonObjectMain);
                }
            });

        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    public void getRequest(String url, JSONObject jsonObject) {
        if (!mLockatedPreferences.getSocietyId().equals("blank")) {
            if (ConnectionDetector.isConnectedToInternet(contextMain)) {
                mQueue = LockatedVolleyRequestQueue.getInstance(contextMain).getRequestQueue();
                LockatedJSONObjectRequest lockatedJSONObjectRequest = new LockatedJSONObjectRequest(Request.Method.PUT,
                        url + mLockatedPreferences.getLockatedToken()
                                + "&id_society=" + mLockatedPreferences.getSocietyId(), jsonObject, this, this);
                lockatedJSONObjectRequest.setTag(REQUEST_TAG);
                mQueue.add(lockatedJSONObjectRequest);
            } else {
                Utilities.showToastMessage(contextMain, contextMain.getResources().getString(R.string.internet_connection_error));
            }
        } else {
            Toast.makeText(contextMain, "You are not associated with any society", Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public int getItemCount() {
        return jsonArray.length();
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }

    @Override
    public void onErrorResponse(VolleyError error) {
        if (contextMain != null) {
            LockatedRequestError.onRequestError(contextMain, error);
        }
    }

    @Override
    public void onResponse(JSONObject response) {
        if (contextMain != null) {
            try {
                notifyDataSetChanged();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public class PendingUserAdapterViewHolder extends RecyclerView.ViewHolder {

        TextView nameTxt, dateTxt, accept, decline;

        public PendingUserAdapterViewHolder(View itemView) {
            super(itemView);

            nameTxt = (TextView) itemView.findViewById(R.id.nameValue);
            dateTxt = (TextView) itemView.findViewById(R.id.mTextDate);
            accept = (TextView) itemView.findViewById(R.id.mViewDetails);
            decline = (TextView) itemView.findViewById(R.id.mContactAuthor);

        }

    }

}
