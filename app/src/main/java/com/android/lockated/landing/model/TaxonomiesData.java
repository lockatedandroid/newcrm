package com.android.lockated.landing.model;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class TaxonomiesData
{
    public int id;
    public String name;
    public String taxonomy_id;

    ArrayList<TaxonData> mTaxonDatas = new ArrayList<>();

    public TaxonomiesData(JSONObject jsonObject)
    {
        try
        {
            id = jsonObject.optInt("id");
            name = jsonObject.optString("name");

            JSONObject root = jsonObject.getJSONObject("root");
            taxonomy_id = root.getString("taxonomy_id");

            JSONArray jsonArray = root.getJSONArray("taxons");
            for (int i = 0; i < jsonArray.length(); i++)
            {
                JSONObject taxonObject = jsonArray.getJSONObject(i);
                TaxonData taxonData = new TaxonData(taxonObject);
                mTaxonDatas.add(taxonData);
            }

        }
        catch (JSONException e)
        {
            e.printStackTrace();
        }
    }

    public int getId()
    {
        return id;
    }

    public String getName()
    {
        return name;
    }

    public ArrayList<TaxonData> getmTaxonDatas()
    {
        return mTaxonDatas;
    }
}
