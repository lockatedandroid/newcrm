
package com.android.lockated.model.userSocieties;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class UserSocietiesData {

    @SerializedName("user_societies")
    @Expose
    private ArrayList<UserSociety> userSocieties = new ArrayList<UserSociety>();

    /**
     * @return The userSocieties
     */
    public List<UserSociety> getUserSocieties() {
        return userSocieties;
    }

    /**
     * @param userSocieties The user_societies
     */
    public void setUserSocieties(ArrayList<UserSociety> userSocieties) {
        this.userSocieties = userSocieties;
    }

}
