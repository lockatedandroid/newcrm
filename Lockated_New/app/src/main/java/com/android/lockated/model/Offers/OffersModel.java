package com.android.lockated.model.Offers;

import org.json.JSONObject;

public class OffersModel {

    private String id;
    private String oimage;
    private String title;
    private String description;
    private String otype;
    private int otype_id;
    private int taxon_id;
    private String rediurl;
    private String expiry;
    private String active;
    private String medium;
    private String thumb;
    private String original;
    private String url;

    /*public OffersModel(JSONObject jsonObject) {
        setId(jsonObject.optString("id"));
        setOimage(jsonObject.optString("oimage"));
        setTitle(jsonObject.optString("title"));
        setDescription(jsonObject.optString("description"));
        setOtype(jsonObject.optString("otype"));
        setOtype_id(jsonObject.optInt("otype_id"));
        setTaxon_id(jsonObject.optInt("taxon_id"));
        setRediurl(jsonObject.optString("rediurl"));
        setExpiry(jsonObject.optString("expiry"));
        setActive(jsonObject.optString("active"));
        setMedium(jsonObject.optString("medium"));
        setThumb(jsonObject.optString("thumb"));
        setOriginal(jsonObject.optString("original"));
        setUrl(jsonObject.optString("urls"));
    }*/

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getOimage() {
        return oimage;
    }

    public void setOimage(String oimage) {
        this.oimage = oimage;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getOtype() {
        return otype;
    }

    public void setOtype(String otype) {
        this.otype = otype;
    }

    public int getOtype_id() {
        return otype_id;
    }

    public void setOtype_id(int otype_id) {
        this.otype_id = otype_id;
    }

    public int getTaxon_id() {
        return taxon_id;
    }

    public void setTaxon_id(int taxon_id) {
        this.taxon_id = taxon_id;
    }

    public String getRediurl() {
        return rediurl;
    }

    public void setRediurl(String rediurl) {
        this.rediurl = rediurl;
    }

    public String getExpiry() {
        return expiry;
    }

    public void setExpiry(String expiry) {
        this.expiry = expiry;
    }

    public String getActive() {
        return active;
    }

    public void setActive(String active) {
        this.active = active;
    }

    public String getMedium() {
        return medium;
    }

    public void setMedium(String medium) {
        this.medium = medium;
    }

    public String getThumb() {
        return thumb;
    }

    public void setThumb(String thumb) {
        this.thumb = thumb;
    }

    public String getOriginal() {
        return original;
    }

    public void setOriginal(String original) {
        this.original = original;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}
