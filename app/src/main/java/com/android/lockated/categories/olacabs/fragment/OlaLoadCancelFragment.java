package com.android.lockated.categories.olacabs.fragment;


import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.ProgressBar;

import com.android.lockated.R;
import com.android.lockated.model.OlaCab.OlaAutoBookResponse.AutoBookResponse;
import com.android.lockated.model.OlaCab.OlaCabBookResponse;
import com.android.lockated.network.ConnectionDetector;
import com.android.lockated.preferences.LockatedPreferences;
import com.android.lockated.request.LockatedRequestError;
import com.android.lockated.request.LockatedVolleyRequestQueue;
import com.android.lockated.utils.Utilities;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

public class OlaLoadCancelFragment extends DialogFragment implements Response.ErrorListener, Response.Listener<JSONObject> {

    private Window window;
    private boolean cab_booking;


    //booking_category for cabs= 1, auto =2;
    private int booking_category = 0;

    private float pickup_latitude, pickup_longitude;
    private float drop_latitude, drop_longitude;
    private static final int REQUEST_LOCATION = 10;
    private static final String REQUEST_TAG = "OLA CABS CONFIRM";
    private JSONObject response_json;
    private OlaCabBookResponse olaCabBookResponse;
    private AutoBookResponse olaAutoBookResponse;
    private LockatedPreferences mLockatedPreferences;

    private String category, ola_token;
    ProgressBar progressBar;
    ImageView img_cab;

    public OlaLoadCancelFragment() {
        // Required empty public constructor
    }

    /*@Override
    public void onStart() {
        super.onStart();
        mLockatedPreferences = new LockatedPreferences(getActivity());
        window = getDialog().getWindow();
        WindowManager.LayoutParams windowParams = window.getAttributes();
        windowParams.dimAmount = 0.75f;
        windowParams.flags |= WindowManager.LayoutParams.FLAG_DIM_BEHIND;
        window.setAttributes(windowParams);
        window.setBackgroundDrawableResource(android.R.color.white);

    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

    }
*/

    @Override
    public void onStart() {
        super.onStart();
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(getDialog().getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.WRAP_CONTENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        getDialog().getWindow().setAttributes(lp);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(STYLE_NO_TITLE, 0);
        mLockatedPreferences = new LockatedPreferences(getActivity());
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

    }


    /*@NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        View connectToView = getActivity().getLayoutInflater().inflate(R.layout.fragment_ola_load_cancel, new LinearLayout(getActivity()), false);

        progressBar = (ProgressBar) connectToView.findViewById(R.id.mProgressBarView);
        progressBar.setVisibility(View.VISIBLE);
        Bundle bundle = getArguments();

        if (bundle != null) {
            pickup_latitude = bundle.getFloat("pickup_latitude");
            pickup_longitude = bundle.getFloat("pickup_longitude");
            drop_latitude = bundle.getFloat("drop_latitude");
            drop_longitude = bundle.getFloat("drop_longitude");
            category = bundle.getString("category");
            ola_token = bundle.getString("ola_token");
        }
        //olaCabBookResponses = new ArrayList<>();
        assert bundle != null;


        Log.e("pickup_latitude : ", String.valueOf(pickup_latitude));
        Log.e("pickup_longitude : ", String.valueOf(pickup_longitude));
        Log.e("drop_latitude : ", String.valueOf(drop_latitude));
        Log.e("drop_longitude : ", String.valueOf(drop_longitude));
        Log.e("category ::", category);

        Dialog builder = new Dialog(getActivity());
        builder.requestWindowFeature(Window.FEATURE_NO_TITLE);
        builder.getWindow().setBackgroundDrawable(new ColorDrawable(Color.WHITE));

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(builder.getWindow().getAttributes());

        *//*lp.width = WindowManager.LayoutParams.WRAP_CONTENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;*//*

        Point size = new Point();

        // Store dimensions of the screen in `size`
        Display display = getActivity().getWindowManager().getDefaultDisplay();
        display.getSize(size);

        // Set the width of the dialog proportional to 75% of the screen width
        //lp.width = 400;
        //lp.height = 300;

        lp.width = (int) (size.x *0.5 );
        lp.height = (int) (size.y * 0.7);


        builder.getWindow().setAttributes(lp);
        builder.setContentView(connectToView);
        builder.show();
        sendConfirmation();
        return builder;
    }*/

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        getDialog().setCanceledOnTouchOutside(false);
        View view = inflater.inflate(R.layout.fragment_ola_load_cancel, container);
        progressBar = (ProgressBar) view.findViewById(R.id.mProgressBarView);
        progressBar.setVisibility(View.VISIBLE);
        img_cab = (ImageView)view.findViewById(R.id.img_cab);
        ola_token = mLockatedPreferences.getOlaToken();
        Bundle bundle = getArguments();

        if (bundle != null) {
            //-----------------29-7-2016---------------------

           /* pickup_latitude = bundle.getFloat("pickup_latitude");
            pickup_longitude = bundle.getFloat("pickup_longitude");
            drop_latitude = bundle.getFloat("drop_latitude");
            drop_longitude = bundle.getFloat("drop_longitude");*/

            //------------------------------------------------
            category = bundle.getString("category");

            //---------changes done on 5-8-2016 3.00 pm ----------------------

                //ola_token = bundle.getString("ola_token");
                //Log.e("ola_token",ola_token);

            //----------------------------------------
        }
        //olaCabBookResponses = new ArrayList<>();
        assert bundle != null;

        if (!category.equals("auto"))
        {
            img_cab.setImageResource(R.drawable.cab_new);
        }
        else {
            img_cab.setImageResource(R.drawable.rickshaw_new);
        }

        //-----------------29-7-2016---------------------

       /* Log.e("pickup_latitude : ", String.valueOf(pickup_latitude));
        Log.e("pickup_longitude : ", String.valueOf(pickup_longitude));
        Log.e("drop_latitude : ", String.valueOf(drop_latitude));
        Log.e("drop_longitude : ", String.valueOf(drop_longitude));*/

        //------------------------------------------------

        Log.e("category ::", category);

        sendConfirmation();
        return view;
    }


    public void sendConfirmation() {
        try {
            Log.e("sendConfirmation", "m inside");
            if (ConnectionDetector.isConnectedToInternet(getActivity())) {

                //String url = "http://sandbox-t.olacabs.com/v1/bookings/create";
                String url = "https://devapi.olacabs.com/v1/bookings/create";

                //-----------------29-7-2016---------------------

                //Log.e("pickup_latitude " + pickup_latitude, "pickup_longitude " + pickup_longitude);
                //Log.e("drop_latitude " + drop_latitude, "drop_longitude " + drop_longitude);

                //------------------------------------------------


                Log.e("ola token",ola_token);
                final JSONObject jsonObject;
                if (!category.equals("auto")) {
                    jsonObject = new JSONObject();


                    //?pickup_lat=12.9491416&pickup_lng=77.64298"

                    //pickup_latitude = (float) 12.9491416;
                    //pickup_longitude = (float) 77.64298;

                    pickup_latitude = mLockatedPreferences.getSrcLat();
                    pickup_longitude = mLockatedPreferences.getSrcLng();
                    drop_latitude =  mLockatedPreferences.getDstLat();
                    drop_longitude = mLockatedPreferences.getDstLng();

                    jsonObject.put("pickup_lat", pickup_latitude);
                    jsonObject.put("pickup_lng", pickup_longitude);

                    if(drop_latitude != 0 && drop_longitude != 0)
                    {
                        jsonObject.put("drop_lat", drop_latitude);
                        jsonObject.put("drop_lng", drop_longitude);
                    }

                    //-----------------29-7-2016---------------------

                    /*jsonObject.put("pickup_lat", 12.950121f);
                    jsonObject.put("pickup_lng", 77.642917f);*/

                    //------------------------------------------------

                    jsonObject.put("pickup_mode", "NOW");
                    jsonObject.put("category", category);
                } else {
                    jsonObject = new JSONObject();

                    pickup_latitude = (float) 12.950121;
                    pickup_longitude = (float) 77.642917;
                    drop_latitude = (float) 12.951074;
                    drop_longitude = (float) 77.644598;


                    jsonObject.put("pickup_lat", pickup_latitude);
                    jsonObject.put("pickup_lng", pickup_longitude);
                    jsonObject.put("drop_lat", drop_latitude);
                    jsonObject.put("drop_lng", drop_longitude);

                    //-----------------29-7-2016---------------------

                    /*jsonObject.put("pickup_lat", 12.950121f);
                    jsonObject.put("pickup_lng", 77.642917f);
                    jsonObject.put("drop_lat", 12.951074f);
                    jsonObject.put("drop_lng", 77.644598f);*/

                    //------------------------------------------------

                    jsonObject.put("pickup_mode", "NOW");
                    jsonObject.put("category", category);

                }

                Log.e("Request JsonObject", "" + jsonObject.toString());

                LockatedVolleyRequestQueue mLockatedVolleyRequestQueue = LockatedVolleyRequestQueue.getInstance(getActivity());
                mLockatedVolleyRequestQueue.sendPostRequestOlaCabsConfirmEx(REQUEST_TAG, Request.Method.POST, url, ola_token, jsonObject, this, this);

            } else {
                Utilities.showToastMessage(getActivity(), getActivity().getResources().getString(R.string.internet_connection_error));
            }

        } catch (Exception e) {
            e.getMessage();
        }

    }

    private void bookingResponse(boolean cab_booking, int booking_category) {
        if (cab_booking) {
            //sendBookResponseToServer(response_json);

            if (booking_category == 1) {
                OlaCabCancelFragment olaCabCancelFragment = new OlaCabCancelFragment();
                Bundle bundle_new = new Bundle();
                bundle_new.putString("booking_id", olaCabBookResponse.getBookingId());
                bundle_new.putString("cab_number", olaCabBookResponse.getCabNumber());
                bundle_new.putString("cab_type", olaCabBookResponse.getCabType());
                bundle_new.putString("car_model", olaCabBookResponse.getCarModel());
                bundle_new.putString("crn", olaCabBookResponse.getCrn());
                bundle_new.putString("driver_name", olaCabBookResponse.getDriverName());
                bundle_new.putString("driver_number", olaCabBookResponse.getDriverNumber());
                bundle_new.putFloat("driver_lat", olaCabBookResponse.getDriverLat());
                bundle_new.putFloat("driver_lng", olaCabBookResponse.getDriverLng());
                bundle_new.putFloat("user_lat",pickup_latitude);
                bundle_new.putFloat("user_lng",pickup_longitude);
                bundle_new.putString("share_ride_url", olaCabBookResponse.getShareRideUrl());
                bundle_new.putInt("eta", olaCabBookResponse.getEta());

                // ----------------Changes done on 5-8-2016  2.59 pm ---------------

                    //bundle_new.putString("AccessToken", ola_token);

                //------------------------------------------------------------------

                if (olaCabBookResponse.getSurchargeValue() != null) {
                    bundle_new.putString("surcharge_value", "" + olaCabBookResponse.getSurchargeValue());
                }

                //OlaMainActivity_Latest.setDriverLocation(pickup_latitude,pickup_longitude,olaCabBookResponse.getDriverLat(), olaCabBookResponse.getDriverLng());
                olaCabCancelFragment.setArguments(bundle_new);

                FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
                fragmentTransaction.replace(R.id.mFrameContainer, olaCabCancelFragment);
                fragmentTransaction.addToBackStack(null);
                fragmentTransaction.commit();
                dismiss();
                Log.e("cab_booking ", "Response true" + "m here");
            }
            else if (booking_category == 2) {

                OlaAutoCancelFragment olaAutoCancelFragment = new OlaAutoCancelFragment();
                Bundle bundle_new = new Bundle();
                bundle_new.putString("booking_id", olaAutoBookResponse.getBookingId());
                bundle_new.putString("status", olaAutoBookResponse.getStatus());
                bundle_new.putString("message", olaAutoBookResponse.getMessage());
                bundle_new.putFloat("user_lat",pickup_latitude);
                bundle_new.putFloat("user_lng",pickup_longitude);
                bundle_new.putString("AccessToken", ola_token);


                //OlaMainActivity_Latest.setDriverLocation(olaCabBookResponse.getDriverLat(), olaCabBookResponse.getDriverLng());
                olaAutoCancelFragment.setArguments(bundle_new);

                FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
                fragmentTransaction.replace(R.id.mFrameContainer, olaAutoCancelFragment);
                fragmentTransaction.addToBackStack(null);
                fragmentTransaction.commit();
                dismiss();
                Log.e("cab_booking ", "Response true" + "m here");
            }
        } else {
            dismiss();
            Log.e("cab_booking ", "Response false" + "m here");
        }
    }

    @SuppressLint("LongLogTag")
    private void sendBookResponseToServer(JSONObject jsonObject) {
        Log.e(" Inside:", "" + "sendBookResponseToServer");
        if (getActivity() != null) {
            if (ConnectionDetector.isConnectedToInternet(getActivity())) {
                String url = "https://www.lockated.com/add_ola_orders.json?token=" + mLockatedPreferences.getLockatedToken();
                Log.e("url of send]BookResponseToServer", url);
                LockatedVolleyRequestQueue lockatedVolleyRequestQueue = LockatedVolleyRequestQueue.getInstance(getActivity());
                lockatedVolleyRequestQueue.sendPostRequestOlaCabsConfirmEx(REQUEST_TAG, Request.Method.POST, url, ola_token, jsonObject, this, this);
            } else {
                Utilities.showToastMessage(getActivity(), getActivity().getResources().getString(R.string.internet_connection_error));
            }
        }
    }


    @Override
    public void onResponse(JSONObject response) {
        Log.e("Response JsonObject", "" + response);
        if (response != null && response.length() > 0) {
            try {
                if (response.has("message") && response.has("code")) {
                    //bookingResponse(cab_booking,booking_category);
                    Log.e("message",response.getString("message"));
                    dismiss();
                    Utilities.showToastMessage(getActivity(), response.getString("message"));
                } else {
                    if (category.equals("auto")) {
                        cab_booking = true;
                        booking_category = 2;

                        response_json = response;
                        Gson gson = new Gson();
                        //OlaCabBookResponse olaCabBookResponse = gson.fromJson(response.toString(), OlaCabBookResponse.class);
                        //olaCabBookResponses.add(olaCabBookResponse);
                        olaAutoBookResponse = gson.fromJson(response.toString(), AutoBookResponse.class);

                        Log.e("BookingId ", "" + olaAutoBookResponse.getBookingId());
                        Log.e("Message ", "" + olaAutoBookResponse.getMessage());
                        Log.e("Status ", "" + olaAutoBookResponse.getStatus());

                        //olaCabBookResponses.add(olaCabCreateResponse);
                        bookingResponse(cab_booking, booking_category);
                        Utilities.showToastMessage(getActivity(), "Your Auto is  Booked Successfully");
                    } else {
                        cab_booking = true;
                        booking_category = 1;
                        response_json = response;
                        Gson gson = new Gson();
                        //OlaCabBookResponse olaCabBookResponse = gson.fromJson(response.toString(), OlaCabBookResponse.class);
                        //olaCabBookResponses.add(olaCabBookResponse);
                        olaCabBookResponse = gson.fromJson(response.toString(), OlaCabBookResponse.class);

                        Log.e("BookingId ", "" + olaCabBookResponse.getBookingId());
                        Log.e("CabNumber ", "" + olaCabBookResponse.getCabNumber());

                        //olaCabBookResponses.add(olaCabCreateResponse);
                        bookingResponse(cab_booking, booking_category);
                        Utilities.showToastMessage(getActivity(), "Your Cab is  Booked Successfully");
                    }
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }

        }
    }

    @Override
    public void onErrorResponse(VolleyError error) {
        Log.e("VolleyError", "" + error);
        if (getActivity() != null) {
            LockatedRequestError.onRequestError(getActivity(), error);
        }
    }
}
