package com.android.lockated.account.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.LinearLayout;

import com.android.lockated.HomeActivity;
import com.android.lockated.R;
import com.android.lockated.account.fragment.EditPersonalInformationFragment;
import com.android.lockated.preferences.LockatedPreferences;
import com.android.lockated.register.OTPGeneratorFragment;

public class EditPersonalDetailActivity extends AppCompatActivity {
    LinearLayout personalInfoEditContainer;
    LockatedPreferences mlockatedPreferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_personal_detail);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(true);
        getSupportActionBar().setTitle(R.string.edit_personal_details);
        init();
    }

    private void init() {
        mlockatedPreferences=new LockatedPreferences(getApplicationContext());
        FragmentManager fragmentManager = getSupportFragmentManager();
        EditPersonalInformationFragment editPersonalInformationFragment = new EditPersonalInformationFragment();
        editPersonalInformationFragment.setFragmentManager(fragmentManager);
        getSupportFragmentManager().beginTransaction().add(R.id.personalInfoEditContainer, editPersonalInformationFragment).commit();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                callIntent();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        callIntent();
    }

    private void callIntent() {
        Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.personalInfoEditContainer);
        if (!(fragment instanceof EditPersonalInformationFragment)) {
            onOTPBackPressed();
        } else if (fragment instanceof OTPGeneratorFragment) {
            onOTPBackPressed();
        } else {
            personalInformation();
        }
    }

    private void onOTPBackPressed() {
        OTPGeneratorFragment otpGeneratorFragment = new OTPGeneratorFragment();
        Bundle bundle = new Bundle();
        bundle.putString("mobileNumber",mlockatedPreferences.getContactNumber() );
        bundle.putString("EditPersonalInformationFragment", "EditPersonalInformationFragment");
        otpGeneratorFragment.setArguments(bundle);
        getSupportFragmentManager().beginTransaction().replace(R.id.personalInfoEditContainer, otpGeneratorFragment).commitAllowingStateLoss();

    }

    private void personalInformation() {
        Intent intent = new Intent(getApplicationContext(), HomeActivity.class);
        intent.putExtra("EditPersonalInformationFragment", "EditPersonalInformationFragment");
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
        finish();
    }

}
