package com.android.lockated.preferences;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;

import com.android.lockated.model.AccountData;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;


public class LockatedPreferences {

    private static final String USER_PREFS = "LOCKATED_PREFS";
    private SharedPreferences appSharedPrefs;
    private SharedPreferences.Editor prefsEditor;

    //-----------------Ola Changes-------------------------

    private String isCabBooked = "IsCabBooked";
    private String isAutoBooked = "isAutoBooked";
    private String tpDetail = "TpDetail";
    private String olaToken = "Ola_Token";
    private String olaCategory = "Ola_Category";

    private String srcLat = "srcLat";
    private String srcLng = "srcLng";
    private String dstLat = "dstLat";
    private String dstLng = "dstLng";

    //--------------------------------------------------------

    //------------------Zomato Changes-----------------//
    private String zomato_token = "zomato_token";
    //--------------------------------------------------//
    private String lockatedToken = "Lockated_Token";
    private String contactNumber = "Contact_Number";
    private String personalAvatar = "personal_avatar";
    private String lockatedUserId = "Lockated_User_Id";
    private String personalName = "FirstName";
    private String lastName = "Last_Name";
    private String isLoggedOut = "Logout";
    private String emailId = "Email";
    private String isOnboardComplete = "Onboard_Complete";
    private String notyValue = "notyValue";
    private String rolesJsonResp = "RolesJsonResp";
    private String societyId = "SocietyId";
    private String userSocietyId = "UserSocietyId";
    private String isUserApprovedInSociety = "IsUserApprovedInSociety";
    private String gcmRegId = "GCMID";
    private String AccountDataString = "AccountData";
    private static final String KEY_NOTIFICATIONS = "notifications";
    private String TaxonomyData = "TaxonomyData";
    private String offerTaxonId = "offerTaxonId";
    private String groupList = "groupList";
    private String gateKeeper_id = "gateKeeper_id";
    private String isOtpGenerated = "isOtpGenerated";
    private String event_id = "event_id";
    private String notice_id = "notice_id";


    public LockatedPreferences(Context context) {
        this.appSharedPrefs = context.getSharedPreferences(USER_PREFS, Activity.MODE_PRIVATE);
        this.prefsEditor = appSharedPrefs.edit();
    }

    public void setOnBoardComplete(boolean onBoardComplete) {
        prefsEditor.putBoolean(isOnboardComplete, onBoardComplete).commit();
    }

    public boolean getOnBoardComplete() {
        return appSharedPrefs.getBoolean(isOnboardComplete, false);
    }

    public void setLockatedUserId(String id) {
        prefsEditor.putString(lockatedUserId, id).commit();
    }

    public String getLockatedUserId() {
        return appSharedPrefs.getString(lockatedUserId, "");
    }

    public void setLogout(boolean logout) {
        prefsEditor.putBoolean(isLoggedOut, logout).commit();
    }

    public boolean getLogout() {
        return appSharedPrefs.getBoolean(isLoggedOut, true);
    }

    public void setLockatedToken(String token) {
        prefsEditor.putString(lockatedToken, token).commit();
    }

    public String getLockatedToken() {
        return appSharedPrefs.getString(lockatedToken, "");
    }

    public void setContactNumber(String number) {
        prefsEditor.putString(contactNumber, number).commit();
    }

    public String getContactNumber() {
        return appSharedPrefs.getString(contactNumber, "");
    }

    public void addNotification(String notification) {

        // get old notifications
        String oldNotifications = getNotifications();

        if (oldNotifications != null) {
            oldNotifications += "|" + notification;
        } else {
            oldNotifications = notification;
        }

        prefsEditor.putString(KEY_NOTIFICATIONS, oldNotifications);
        prefsEditor.commit();
    }

    public String getNotifications() {
        return appSharedPrefs.getString(KEY_NOTIFICATIONS, null);
    }

    public void setRolesJson(String jsonResp) {
        prefsEditor.putString(rolesJsonResp, jsonResp).commit();
    }

    public String getRolesJson() {
        return appSharedPrefs.getString(rolesJsonResp, "blank");
    }

    public void setSocietyId(String idSociety) {
        prefsEditor.putString(societyId, idSociety).commit();
    }

    public String getSocietyId() {
        return appSharedPrefs.getString(societyId, "blank");
    }

    public void setUserSocietyId(int idSociety) {
        prefsEditor.putInt(userSocietyId, idSociety).commit();
    }

    public int getUserSocietyId() {
        return appSharedPrefs.getInt(userSocietyId, 0);
    }

    public void setIsUserApprovedInSociety(Boolean IsUserApprovedInSociety) {
        prefsEditor.putBoolean(isUserApprovedInSociety, IsUserApprovedInSociety).commit();
    }

    public boolean getIsUserApprovedInSociety() {
        return appSharedPrefs.getBoolean(isUserApprovedInSociety, false);
    }

    public void setGcmId(String gcmId) {
        prefsEditor.putString(gcmRegId, gcmId).commit();
    }

    public String getGcmId() {
        return appSharedPrefs.getString(gcmRegId, "blank");
    }

    public void setAccountData(AccountData accountData) {
        Gson gson = new Gson();
        String jsonAccountData = gson.toJson(accountData);
        prefsEditor.putString(AccountDataString, jsonAccountData).commit();
    }

    public AccountData getAccountData() {
        AccountData accountData = new Gson().fromJson(appSharedPrefs.getString(AccountDataString, "blank"),
                AccountData.class);
        return accountData;
    }

    public void setTaxonomyData(JSONArray jsonArray) {
        String jsonTaxonomyData = jsonArray.toString();
        prefsEditor.putString(TaxonomyData, jsonTaxonomyData).commit();
    }

    public JSONArray getTaxonomyData() {
        JSONArray newJArray = null;
        try {
            newJArray = new JSONArray(appSharedPrefs.getString(TaxonomyData, "0"));
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return newJArray;
    }

    public int getOfferTaxonName() {
        return appSharedPrefs.getInt(offerTaxonId, 0);
    }

    public void setOfferTaxonId(int offerTaxonNameValue) {
        prefsEditor.putInt(offerTaxonId, offerTaxonNameValue).commit();
    }

    public void setGroupList(JSONObject jsonObject) {
        prefsEditor.putString(groupList, jsonObject.toString()).commit();
    }

    public String getGroupList() {
        return appSharedPrefs.getString(groupList, "NoData");
    }


    public void setImageAvatar(String avatar) {
        prefsEditor.putString(personalAvatar, avatar).commit();
    }

    public String getImageAvatar() {
        return appSharedPrefs.getString(personalAvatar, "");
    }

    public void setPersonalName(String name) {
        prefsEditor.putString(personalName, name).commit();

    }

    public String getPersonalName() {
        return appSharedPrefs.getString(personalName, "");
    }

    public void setEmailAddress(String emailAddress) {
        prefsEditor.putString(emailId, emailAddress).commit();
    }

    public String getEmailAddress() {
        return appSharedPrefs.getString(emailId, "");
    }

    public void setLastName(String lName) {
        prefsEditor.putString(lastName, lName).commit();
    }

    public String getLastName() {
        return appSharedPrefs.getString(lastName, "");
    }

    public void setIsOtpGenerated(boolean lName) {
        prefsEditor.putBoolean(isOtpGenerated, lName).commit();
    }

    public void setNotificationValue(boolean nValue) {
        prefsEditor.putBoolean(notyValue, nValue).commit();
    }

    public boolean getNotificationValue() {
        return appSharedPrefs.getBoolean(notyValue, false);
    }

    public boolean getIsOtpGenerated() {
        return appSharedPrefs.getBoolean(isOtpGenerated, false);
    }

    public void setEvent_id(String Name) {
        prefsEditor.putString(event_id, Name).commit();
    }

    public String getEvent_id() {
        return appSharedPrefs.getString(event_id, "blank");
    }

    public void setNotice_id(String Notice) {
        prefsEditor.putString(notice_id, Notice).commit();
    }

    public String getNotice_id() {
        return appSharedPrefs.getString(notice_id, "blank");
    }

    public void clear() {
        prefsEditor.clear();
        prefsEditor.commit();
    }

    public void setGateKeeper_id(String gateKeeper) {
        prefsEditor.putString(gateKeeper_id, gateKeeper).commit();
    }

    public String getGateKeeper_id() {
        return appSharedPrefs.getString(gateKeeper_id, "blank");
    }


    //-------------------------Ola Changes----------------------

    //---------Set Method-------------

    public void setIsCabBooked(boolean cabBooked) {
        prefsEditor.putBoolean(isCabBooked, cabBooked).commit();
    }

    public void setIsAutoBooked(boolean autoBooked) {
        prefsEditor.putBoolean(isAutoBooked, autoBooked).commit();
    }

    public void setTpdetail(String tpdetail) {
        prefsEditor.putString(tpDetail, tpdetail).commit();
    }

    public void setSrcLat(float srcLat) {
        prefsEditor.putFloat(this.srcLat, srcLat).commit();
    }

    public void setSrcLng(float srcLng) {
        prefsEditor.putFloat(this.srcLng, srcLng).commit();
    }

    public void setDstLat(float dstLat) {
        prefsEditor.putFloat(this.dstLat, dstLat).commit();
    }

    public void setDstLng(float dstLng) {
        prefsEditor.putFloat(this.dstLng, dstLng).commit();
    }

    public void setOlaToken(String token) {
        prefsEditor.putString(olaToken, token).commit();
    }

    //--------------Get Method-------------

    public boolean getIsCabBooked() {
        return appSharedPrefs.getBoolean(isCabBooked, false);
    }

    public boolean getIsAutoBooked() {
        return appSharedPrefs.getBoolean(isAutoBooked, false);
    }

    public String getTpDetail() {
        return appSharedPrefs.getString(tpDetail, "blank");
    }

    public float getSrcLat() {
        return appSharedPrefs.getFloat(this.srcLat, 0);
    }

    public float getSrcLng() {
        return appSharedPrefs.getFloat(this.srcLng, 0);
    }

    public float getDstLat() {
        return appSharedPrefs.getFloat(this.dstLat, 0);
    }

    public float getDstLng() {
        return appSharedPrefs.getFloat(this.dstLng, 0);
    }

    public String getOlaToken() {
        return appSharedPrefs.getString(olaToken, "null");
    }


    public void setOlaCategory(String s) {
        prefsEditor.putString(olaCategory, s).commit();
    }

    public String getOlaCategory() {
        return appSharedPrefs.getString(olaCategory, "null");
    }

    ///-------------------------Zomato Changes ------------------------------------//

    public void setZomatoToken(String s) {
        prefsEditor.putString(zomato_token, s).commit();
    }

    public String getZomatoToken() {
        return appSharedPrefs.getString(zomato_token, "null");
    }

    //--
}
