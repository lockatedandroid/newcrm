package com.android.lockated.categories.reminder.fragment;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.provider.CalendarContract;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.android.lockated.Interfaces.IRecyclerItemClickListener;
import com.android.lockated.Interfaces.IReminderAdded;
import com.android.lockated.R;
import com.android.lockated.categories.reminder.adapter.ReminderRowAdapter;
import com.android.lockated.categories.reminder.model.ReminderData;
import com.android.lockated.network.ConnectionDetector;
import com.android.lockated.preferences.LockatedPreferences;
import com.android.lockated.request.LockatedRequestError;
import com.android.lockated.request.LockatedVolleyRequestQueue;
import com.android.lockated.utils.ApplicationURL;
import com.android.lockated.utils.Utilities;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;

public class ReminderFragment extends Fragment implements View.OnClickListener, IRecyclerItemClickListener, Response.Listener, Response.ErrorListener, IReminderAdded {
    private static final String GET_REQUEST_TAG = "ReminderFragment";
    private static final String DELETE_REQUEST_TAG = "DeleteReminderFragment";

    private ArrayList<ReminderData> mReminderDataList;

    private ProgressBar mProgressBarView;
    private TextView mTextViewNoReminderFound;

    private int mPosition;

    private ReminderRowAdapter mReminderRowAdapter;

    private LockatedPreferences mLockatedPreferences;
    private LockatedVolleyRequestQueue mLockatedVolleyRequestQueue;
    String screenName = "Reminder Screen";

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View reminderView = inflater.inflate(R.layout.fragment_reminder, container, false);
        init(reminderView);
        Utilities.ladooIntegration(getActivity(), screenName);
        Utilities.lockatedGoogleAnalytics(screenName, getString(R.string.visited), screenName);
        getReminderData();
        return reminderView;
    }

    private void init(View reminderView) {
        mReminderDataList = new ArrayList<>();
        mLockatedPreferences = new LockatedPreferences(getActivity());

        mProgressBarView = (ProgressBar) reminderView.findViewById(R.id.mProgressBarView);
        RecyclerView mRecyclerView = (RecyclerView) reminderView.findViewById(R.id.mRecyclerView);
        mTextViewNoReminderFound = (TextView) reminderView.findViewById(R.id.mTextViewNoReminderFound);
        FloatingActionButton mFloatingActionButton = (FloatingActionButton) reminderView.findViewById(R.id.mFloatingActionButton);

        LinearLayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        mRecyclerView.setLayoutManager(mLayoutManager);
        mRecyclerView.setHasFixedSize(true);

        mTextViewNoReminderFound.setVisibility(View.GONE);

        mFloatingActionButton.setOnClickListener(this);

        mReminderRowAdapter = new ReminderRowAdapter(mReminderDataList, this);
        mRecyclerView.setAdapter(mReminderRowAdapter);
    }

    private void getReminderData() {
        if (ConnectionDetector.isConnectedToInternet(getActivity())) {
            mProgressBarView.setVisibility(View.VISIBLE);

            mLockatedVolleyRequestQueue = LockatedVolleyRequestQueue.getInstance(getActivity());
            mLockatedVolleyRequestQueue.sendRequest(GET_REQUEST_TAG, Request.Method.GET, ApplicationURL.getReminderDataUrl + mLockatedPreferences.getLockatedToken(), null, this, this);
        } else {
            Utilities.showToastMessage(getActivity(), getActivity().getResources().getString(R.string.internet_connection_error));
        }
    }

    private void onReminderDeleteClicked(int id) {
        if (ConnectionDetector.isConnectedToInternet(getActivity())) {
            mProgressBarView.setVisibility(View.VISIBLE);

            mLockatedVolleyRequestQueue = LockatedVolleyRequestQueue.getInstance(getActivity());
            mLockatedVolleyRequestQueue.sendRequest(DELETE_REQUEST_TAG, Request.Method.DELETE, ApplicationURL.deleteReminderDataUrl + id + ".json?token=" + mLockatedPreferences.getLockatedToken(), null, this, this);
        } else {
            Utilities.showToastMessage(getActivity(), getActivity().getResources().getString(R.string.internet_connection_error));
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.mFloatingActionButton:
                onAddReminderClicked();
                break;
        }
    }

    private void onAddReminderClicked() {
        FragmentManager fragmentManager = getChildFragmentManager();
        AddReminderFragment addReminderFragment = new AddReminderFragment();
        addReminderFragment.setListener(this);
        addReminderFragment.setReminderDataOnEdit(0, null, 0);
        addReminderFragment.show(fragmentManager, "ReminderFragment");
    }

    private void onEditReminderClicked() {
        FragmentManager fragmentManager = getChildFragmentManager();
        AddReminderFragment addReminderFragment = new AddReminderFragment();
        addReminderFragment.setListener(this);
        addReminderFragment.setReminderDataOnEdit(1, mReminderDataList.get(mPosition), mPosition);
        addReminderFragment.show(fragmentManager, "ReminderFragment");
    }

    @Override
    public void onRecyclerItemClick(View view, int position) {
        mPosition = position;
        switch (view.getId()) {
            case R.id.mButtonReminderDelete:
                onReminderDeleteClicked(mReminderDataList.get(position).getId());
                break;

            case R.id.mButtonReminderEdit:
                onEditReminderClicked();
                break;

        }
    }

    @Override
    public void onErrorResponse(VolleyError error) {
        if (getActivity() != null) {
            mProgressBarView.setVisibility(View.INVISIBLE);
            LockatedRequestError.onRequestError(getActivity(), error);
        }
    }

    @Override
    public void onResponse(Object response) {
        if (getActivity() != null) {
            mProgressBarView.setVisibility(View.INVISIBLE);

            JSONObject jsonObject = (JSONObject) response;
            try {
                if (mLockatedVolleyRequestQueue.getLockatedJSONObjectRequest().getTag().toString().equals(GET_REQUEST_TAG)) {
                    JSONArray jsonArray = jsonObject.getJSONArray("reminders");
                    if (jsonArray.length() > 0) {
                        mReminderDataList.clear();

                        for (int i = 0; i < jsonArray.length(); i++) {
                            JSONObject reminderObject = jsonArray.getJSONObject(i);
                            ReminderData reminderData = new ReminderData(reminderObject);
                            mReminderDataList.add(reminderData);
                        }
                    } else {
                        mTextViewNoReminderFound.setVisibility(View.VISIBLE);
                    }
                } else if (mLockatedVolleyRequestQueue.getLockatedJSONObjectRequest().getTag().toString().equals(DELETE_REQUEST_TAG)) {
                    mReminderDataList.remove(mPosition);
                    showHideMessage();
                }

                mReminderRowAdapter.notifyDataSetChanged();
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void onReminderUpdated(ReminderData reminderData, int requestId, int position) {
        if (requestId == 1) {
            mReminderDataList.remove(position);
        } else {
            addReminderToCalender("Do you wish to add this reminder to calender?", reminderData);
        }
        mReminderDataList.add(reminderData);
        showHideMessage();
        mReminderRowAdapter.notifyDataSetChanged();
    }

    private void showHideMessage() {
        if (mReminderDataList.size() == 0) {
            mTextViewNoReminderFound.setVisibility(View.VISIBLE);
        } else {
            mTextViewNoReminderFound.setVisibility(View.GONE);
        }
    }

    public void addReminderToCalender(String message, final ReminderData reminderData) {
        final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setMessage(message)
                .setCancelable(false)
                .setPositiveButton("Add to Calender", new DialogInterface.OnClickListener() {
                    public void onClick(final DialogInterface dialog, final int id) {
                        dialog.cancel();
                        String date = reminderData.getDate();
                        String time = reminderData.getTime();

                        long startTime = startTimeInMilis(date + "=" + time);
                        Intent intent = new Intent(Intent.ACTION_INSERT);
                        intent.setData(CalendarContract.Events.CONTENT_URI);
                        intent.putExtra(CalendarContract.EXTRA_EVENT_BEGIN_TIME, startTime);
                        intent.putExtra(CalendarContract.EXTRA_EVENT_END_TIME, startTime + 1000 * 60 * 60);
                        intent.putExtra(CalendarContract.Events.HAS_ALARM, true);
                        intent.putExtra(CalendarContract.Events.ALL_DAY, false);
                        intent.putExtra(CalendarContract.Events.TITLE, reminderData.getSubject());
                        intent.putExtra(CalendarContract.Events.DESCRIPTION, reminderData.getDescription());
                        intent.putExtra(CalendarContract.Events.ALLOWED_REMINDERS, "METHOD_DEFAULT");
                        intent.putExtra(CalendarContract.Reminders.EVENT_ID, reminderData.getId());
                        intent.putExtra(CalendarContract.Reminders.MINUTES, 5);
                        intent.putExtra(CalendarContract.Reminders.METHOD, CalendarContract.Reminders.METHOD_ALERT);
                        startActivity(intent);
                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    public void onClick(final DialogInterface dialog, final int id) {
                        dialog.cancel();
                    }
                });
        final AlertDialog alert = builder.create();
        alert.show();
    }

    private long startTimeInMilis(String startTime) {
        long timeInMilliseconds = 0;
        try {
            String[] dateTime = startTime.split("=");
            String date = dateTime[0];
            String time = dateTime[1];

            String[] dateArr = date.split("-");
            String[] timeArr = time.split(":");

            String year = dateArr[0];
            String month = dateArr[1];
            String day = dateArr[2];

            String hour = timeArr[0];
            String min = dateArr[1];

            Calendar calendar = Calendar.getInstance();
            calendar.set(Integer.valueOf(year), Integer.valueOf(month) - 1, Integer.valueOf(day), Integer.valueOf(hour), Integer.valueOf(min));
            timeInMilliseconds = calendar.getTimeInMillis();
        } catch (Exception e) {
            Calendar calendar = Calendar.getInstance();
            timeInMilliseconds = calendar.getTimeInMillis();
        }
        return timeInMilliseconds;
    }
}
