package com.android.lockated.categories.olacabs.fragment;

import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.SlidingPaneLayout;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import com.android.lockated.LockatedApplication;
import com.android.lockated.R;
import com.android.lockated.categories.olacabs.activity.OlaMainActivity_Latest;
import com.android.lockated.categories.olacabs.adapter.OlaReasonAdapter;
import com.android.lockated.information.AccountController;
import com.android.lockated.model.AccountData;
import com.android.lockated.model.OlaCab.CancelResponse;
import com.android.lockated.model.OlaCab.OlaAutoCompleteResponse.OlaAutoCompleteResponse;
import com.android.lockated.model.OlaCab.OlaAutoTrackResponse.OlaAutoTrackReponse;
import com.android.lockated.model.OlaCab.OlaCabCancelReason.CancelReasons;
import com.android.lockated.network.ConnectionDetector;
import com.android.lockated.preferences.LockatedPreferences;
import com.android.lockated.request.LockatedVolleyRequestQueue;
import com.android.lockated.utils.Utilities;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

/**
 * Created by HAVEN INFOLINE on 7/20/2016.
 */
public class OlaAutoConfirmCancel extends Fragment implements View.OnClickListener, Response.ErrorListener, Response.Listener<JSONObject> {

    private static final String REQUEST_TAG = "OLA AUTO CANCEL";

    private TextView txt_ola_auto_driver_name,txt_ola_auto_cancel,txt_ola_auto_number,txt_ola_auto_model,txt_ola_auto_eta;
    private Button btn_call_ola_auto_driver;
    private String access_token = null;

    public CancelReasons cancelReasons;
    public CancelResponse cancelResponse;

    private AccountController accountController;
    private ArrayList<AccountData> accountDataArrayList;

    private float DriverLat, DriverLng,UserLat,UserLng;

    private String DriverNumber,DriverName,VehicleNumber,VehicleType,Eta;


    public OlaAutoTrackReponse trackResponse;
    public OlaAutoCompleteResponse completeResponse;

    private LockatedPreferences mLockatedPreferences;
    String BookingId,BookingMessage,BookingStatus,AccessToken,BookingDateTime;

    String reason;
    private static final int REQUEST_CALL = 11;
    private SlidingPaneLayout mLayout;

    private OlaReasonAdapter adapter;
    JSONObject confirm_jsonObject;
    public OlaAutoConfirmCancel() {
        // Required empty public constructor
    }


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        accountController = AccountController.getInstance();
        accountDataArrayList = accountController.getmAccountDataList();

        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        mLockatedPreferences = new LockatedPreferences(getActivity());
        if (mLockatedPreferences.getIsAutoBooked()) {
            //getCurrentCabStatus();

            Log.e("Ola Token", "" + accountController.getmAccountDataList().get(0).getOlaToken());
            AccessToken = accountController.getmAccountDataList().get(0).getOlaToken();

            Log.e("I am","getIsBooked");
            String jsonString = mLockatedPreferences.getTpDetail();
            JSONObject jsonObject;
            try {

                jsonObject = new JSONObject(jsonString);
                BookingId = jsonObject.getString("booking_id");
                VehicleNumber = jsonObject.getString("vehicle_number");
                VehicleType = jsonObject.getString("vehicle_type");
                DriverName = jsonObject.getString("driver_name");
                DriverNumber = jsonObject.getString("driver_number");
                DriverLat = (float) jsonObject.getDouble("driver_lat");
                DriverLng = (float) jsonObject.getDouble("driver_lng");
                UserLat = (float) jsonObject.getDouble("user_lat");
                UserLng = (float) jsonObject.getDouble("user_lng");
                BookingStatus = jsonObject.getString("booking_status");
                Eta = jsonObject.getString("eta");
                //OlaMainActivity_Latest.setDriverLocation(DriverLat,DriverLng);
            } catch (JSONException e) {
                e.printStackTrace();
            }


        }
        else if(!mLockatedPreferences.getIsAutoBooked()) {
            if (savedInstanceState == null) {
                Bundle bundle = getArguments();
                Log.e("Bundle Arguments", "" + getArguments());
                mLockatedPreferences = new LockatedPreferences(getActivity());
                if (bundle != null) {
                    String booking_string = bundle.getString("Response");
                    JSONObject jsonObject;
                    try {
                        jsonObject = new JSONObject(booking_string);
                        BookingId = jsonObject.getString("booking_id");
                        DriverNumber = jsonObject.getString("driver_number");
                        DriverName = jsonObject.getString("driver_name");
                        VehicleNumber = jsonObject.getString("vehicle_number");
                        VehicleType = jsonObject.getString("vehicle_type");
                        DriverLat = (float) jsonObject.getDouble("driver_lat");
                        DriverLng = (float) jsonObject.getDouble("driver_lng");
                        JSONObject jsonObject1 = jsonObject.getJSONObject("duration");
                        Eta = jsonObject1.getString("value");
                        BookingStatus =  jsonObject.getString("booking_status");

                        Log.e("BookingStatus",BookingStatus);

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }
            }

            confirm_jsonObject = new JSONObject();
            try {
                confirm_jsonObject.put("booking_id", BookingId);
                confirm_jsonObject.put("cab_number", VehicleNumber);
                confirm_jsonObject.put("cab_type", VehicleType);
                confirm_jsonObject.put("car_model", null);
                confirm_jsonObject.put("crn", null);
                confirm_jsonObject.put("driver_name", DriverName);
                confirm_jsonObject.put("driver_number", DriverNumber);
                confirm_jsonObject.put("driver_lat", DriverLat);
                confirm_jsonObject.put("driver_lng", DriverLng);
                confirm_jsonObject.put("user_lat", UserLat);
                confirm_jsonObject.put("user_lng", UserLng);
                confirm_jsonObject.put("eta", Eta);
                confirm_jsonObject.put("booking_status","Booked");
                confirm_jsonObject.put("current_status",true);

                mLockatedPreferences.setIsAutoBooked(true);
                sendBookResponseToServer(confirm_jsonObject);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        setHasOptionsMenu(true);
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view =  inflater.inflate(R.layout.ola_auto_confirm_cancel, container, false);
        OlaMainActivity_Latest.mLayout.setEnabled(false);
        //OlaMainActivity_Latest.mLayout.setPanelHeight(200);
        OlaMainActivity_Latest.mLayout.setCoveredFadeColor(Color.WHITE);
        OlaMainActivity_Latest.edt_olaSourceLocation.setOnClickListener(null);
        OlaMainActivity_Latest.edt_olaSourceLocation.setClickable(false);

        getActivity().setTitle("Cancellation");

        final ScheduledExecutorService executor = Executors.newScheduledThreadPool(1);
        executor.scheduleAtFixedRate(new Runnable() {
            @Override
            public void run() {
                Log.e(" Inside:", "" + "Runnable Task");
                if (getActivity() != null) {
                    if (ConnectionDetector.isConnectedToInternet(getActivity())) {
                        String url = "http://sandbox-t.olacabs.com/v1/bookings/track_ride?booking_id="+ BookingId;
                        Log.e("url of sendBookResponseToServer", "" + url);
                        LockatedVolleyRequestQueue lockatedVolleyRequestQueue = LockatedVolleyRequestQueue.getInstance(getActivity());
                        lockatedVolleyRequestQueue.sendPostRequestOlaCabsConfirmEx(REQUEST_TAG, Request.Method.GET, url, AccessToken, null, new Response.Listener<JSONObject>() {
                            @Override
                            public void onResponse(JSONObject response) {
                                Log.e("Response", response.toString());
                                Gson gson = new Gson();
                                try {
                                    if (response.has("message") && response.has("code")) {
                                        Log.e("Response",response.toString());
                                        Utilities.showToastMessage(getActivity(), response.getString("message"));
                                    } else if (response.has("booking_status")) {

                                        if (response.getString("booking_status").equals("PENDING"))
                                        {
                                            Log.e("Response",response.toString());
                                            //trackResponse = gson.fromJson(response.toString(), TrackResponse.class);
                                            //OlaMainActivity_Latest.setDriverLocation(0,0,trackResponse.getDriverLat(),trackResponse.getDriverLng());
                                        }

                                        else if (response.getString("booking_status").equals("ALLOTMENT_RETRYING"))
                                        {
                                            Log.e("Response",response.toString());
                                            //trackResponse = gson.fromJson(response.toString(), TrackResponse.class);
                                            //OlaMainActivity_Latest.setDriverLocation(0,0,trackResponse.getDriverLat(),trackResponse.getDriverLng());
                                        }

                                        else if (response.getString("booking_status").equals("ALLOTMENT_FAILED"))
                                        {
                                            Log.e("Response",response.toString());
                                            Utilities.showToastMessage(getActivity(), response.getString("Allotment Failed Please Cancel ride and Try Again"));
                                            executor.shutdown();
                                            //trackResponse = gson.fromJson(response.toString(), TrackResponse.class);
                                            //OlaMainActivity_Latest.setDriverLocation(0,0,trackResponse.getDriverLat(),trackResponse.getDriverLng());
                                        }

                                        else if (response.getString("booking_status").equals("CALL_DRIVER"))
                                        {
                                            Log.e("Response",response.toString());
                                            trackResponse = gson.fromJson(response.toString(), OlaAutoTrackReponse.class);
                                            OlaMainActivity_Latest.setDriverLocation(UserLat,UserLng,trackResponse.getDriverLat(),trackResponse.getDriverLng(),true);
                                        }
                                        else if (response.getString("booking_status").equals("IN_PROGRESS"))
                                        {
                                            Log.e("Response",response.toString());
                                            //trackResponse = gson.fromJson(response.toString(), TrackResponse.class);
                                            //OlaMainActivity_Latest.setDriverLocation(0,0,trackResponse.getDriverLat(),trackResponse.getDriverLng());
                                        }
                                        else if (response.getString("booking_status").equals("INVOICE"))
                                        {
                                            Log.e("Response",response.toString());
                                            //trackResponse = gson.fromJson(response.toString(), TrackResponse.class);
                                            //OlaMainActivity_Latest.setDriverLocation(0,0,trackResponse.getDriverLat(),trackResponse.getDriverLng());
                                        }
                                        else if (response.getString("booking_status").equals("COMPLETED"))
                                        {
                                            Log.e("Response",response.toString());
                                            completeResponse = gson.fromJson(response.toString(), OlaAutoCompleteResponse.class);
                                            //OlaMainActivity_Latest.setDriverLocation(0,0,completeResponse.getDriverLat(),trackResponse.getDriverLng());
                                            sendTrackResponseToServer(response);
                                        }
                                        else if (response.getString("booking_status").equals("BOOKING_CANCELLED"))
                                        {
                                            Log.e("Response",response.toString());
                                            //trackResponse = gson.fromJson(response.toString(), TrackResponse.class);
                                            ///OlaMainActivity_Latest.setDriverLocation(trackResponse.getDriverLat(),trackResponse.getDriverLng());
                                            executor.shutdown();
                                        }
                                        else if (response.getString("reason").equals("BOOKING_ALREADY_CANCELLED"))
                                        {
                                            Log.e("Response",response.toString());
                                            //trackResponse = gson.fromJson(response.toString(), TrackResponse.class);
                                            //OlaMainActivity_Latest.setDriverLocation(trackResponse.getDriverLat(),trackResponse.getDriverLng());
                                        }

                                    }
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }
                        }, new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                Log.e("Error Response",error.toString());
                            }
                        });
                    } else {
                        Utilities.showToastMessage(getActivity(), getActivity().getResources().getString(R.string.internet_connection_error));
                    }
                }
            }
        }, 0, 60 * 1000, TimeUnit.MILLISECONDS);
        init(view);
        getCancelReason(AccessToken);
        return view;
    }

    public void init(View view) {
        btn_call_ola_auto_driver = (Button)view.findViewById(R.id.btn_call_ola_auto_driver);
        txt_ola_auto_eta = (TextView) view.findViewById(R.id.txt_ola_auto_eta);
        txt_ola_auto_driver_name = (TextView) view.findViewById(R.id.txt_ola_auto_driver_name);
        txt_ola_auto_cancel = (TextView) view.findViewById(R.id.txt_ola_auto_cancel);
        txt_ola_auto_model = (TextView) view.findViewById(R.id.txt_ola_auto_model);
        txt_ola_auto_number = (TextView) view.findViewById(R.id.txt_ola_auto_number);


        Log.e("DriverName","" + DriverName);
        Log.e("DriverNumber","" + DriverNumber);
        Log.e("VehicleNumber","" + VehicleNumber);
        Log.e("VehicleType","" + VehicleType);

        txt_ola_auto_driver_name.setText(DriverName);
        txt_ola_auto_model.setText(VehicleNumber);
        txt_ola_auto_number.setText(VehicleType);

        txt_ola_auto_cancel.setOnClickListener(this);
        btn_call_ola_auto_driver.setOnClickListener(this);

    }


    public void sendCabCancel() {
        try {
            Log.e("sendCabCancel", "m inside");
            Log.e("reason", reason);
            if (ConnectionDetector.isConnectedToInternet(getActivity())) {

                String url = "http://sandbox-t.olacabs.com/v1/bookings/cancel";

                JSONObject jsonObject = new JSONObject();
                jsonObject.put("booking_id", BookingId);
                jsonObject.put("reason", reason);

                Log.e("Request JsonObject", "" + jsonObject.toString());

                LockatedVolleyRequestQueue mLockatedVolleyRequestQueue = LockatedVolleyRequestQueue.getInstance(getActivity());
                mLockatedVolleyRequestQueue.sendPostRequestOlaCabsConfirmEx(REQUEST_TAG, Request.Method.POST, url, AccessToken, jsonObject, this,this);

            } else {
                Utilities.showToastMessage(getActivity(), getActivity().getResources().getString(R.string.internet_connection_error));
            }

        } catch (Exception e) {
            e.getMessage();
        }

    }

    @Override
    public void onResume()
    {
        super.onResume();
        if (((LockatedApplication) getActivity().getApplicationContext()).isGPSEnabled()) {
            ((LockatedApplication) getActivity().getApplicationContext()).setIsGPSEnabled(false);
        }
    }


    public void getCancelReason(String access_token) {
        if (getActivity() != null) {
            if (ConnectionDetector.isConnectedToInternet(getActivity())) {
                String url = "http://sandbox-t.olacabs.com/v1/bookings/cancel/reasons";
                Log.e("getCancelReason", "" + url);

                LockatedVolleyRequestQueue mLockatedVolleyRequestQueue = LockatedVolleyRequestQueue.getInstance(getActivity());
                mLockatedVolleyRequestQueue.sendPostRequestOlaCabsConfirmEx(REQUEST_TAG, Request.Method.GET, url, access_token,null,new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.e("Cancel Response", "" + response);
                        if (response != null && response.length() > 0) {
                            try {
                                if (response.has("message") && response.has("code")) {
                                    Utilities.showToastMessage(getActivity(), response.getString("message"));
                                }
                                else if(response.has("cancel_reasons"))
                                {
                                    Gson gson = new Gson();
                                    cancelReasons = gson.fromJson(response.get("cancel_reasons").toString(), CancelReasons.class);

                                    for ( int i =0 ; i<cancelReasons.getAuto().size();i++) {
                                        Log.e("Cancel Reason", ""+cancelReasons.getAuto().get(i));
                                    }
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    }
                }, this);
            } else {
                Utilities.showToastMessage(getActivity(), getActivity().getResources().getString(R.string.internet_connection_error));
            }
        }
    }


    @Override
    public void onClick(View v) {
        switch (v.getId())
        {
            case R.id.btn_cancel_auto:
            {
                getReasonAlert("auto");
                break;
            }

        }

    }

    @Override
    public void onErrorResponse(VolleyError error) {
        Log.e("Error Response",error.toString());
    }

    @Override
    public void onResponse(JSONObject response) {
        Log.e("Response",""+response);
        if (response != null && response.length() > 0) {
            try {
                if (response.has("message") && response.has("code")) {
                    Utilities.showToastMessage(getActivity(), response.getString("message"));
                    Log.e("Response",response.toString());
                }
                else if (response.has("request_type") && response.has("status"))
                {
                    Gson gson = new Gson();
                    cancelResponse = gson.fromJson(response.toString(), CancelResponse.class);

                    Log.e("request_type ", "" + cancelResponse.getRequestType());
                    Log.e("status ", "" + cancelResponse.getStatus());
                    Log.e("text ", "" + cancelResponse.getText());
                    Log.e("header ", "" + cancelResponse.getHeader());

                    response.put("booking_status","CANCELLED");
                    response.put("booking_id",BookingId);
                    Log.e("Response",response.toString());
                    sendBookResponseToServer(response);
                    Utilities.showToastMessage(getActivity(),""+cancelResponse.getText());
                }
                else if(response.has("thirdparty"))
                {
                    if (response.getJSONObject("tpdetails").getString("booking_status").equals("CANCELLED")) {
                        AvailMainFragment availMainFragment = new AvailMainFragment();
                        FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
                        fragmentTransaction.add(R.id.mFrameContainer, availMainFragment);
                        fragmentTransaction.commit();
                    }
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    private void getReasonAlert(String cabType) {

        Type type = Type.valueOf(cabType); // surround with try/catch
        ArrayList<String> strings ;;
        switch(type) {
            case mini:
                strings = cancelReasons.getMini();
                Log.e("strings",""+strings);
                adapter = new OlaReasonAdapter(getActivity(),R.layout.layout_each_reason,cancelReasons.getMini());
                break;
            case micro:
                strings = cancelReasons.getMicro();
                Log.e("strings",""+strings);
                adapter = new OlaReasonAdapter(getActivity(), R.layout.layout_each_reason, cancelReasons.getMicro());
                break;
            case prime:
                strings = cancelReasons.getPrime();
                Log.e("strings",""+strings);
                adapter = new OlaReasonAdapter(getActivity(),R.layout.layout_each_reason,cancelReasons.getPrime());
                break;
            case sedan:
                strings = cancelReasons.getSedan();
                Log.e("strings",""+strings);
                adapter = new OlaReasonAdapter(getActivity(),R.layout.layout_each_reason,cancelReasons.getSedan());
                break;
            case auto:
                strings = cancelReasons.getAuto();
                Log.e("strings",""+strings);
                adapter = new OlaReasonAdapter(getActivity(),R.layout.layout_each_reason,cancelReasons.getAuto());
                break;
        }
        final AlertDialog.Builder dialog = new AlertDialog.Builder(getActivity());
        View view = getActivity().getLayoutInflater().inflate( R.layout.reason_layout, null );
        dialog.setTitle("Select Reason of Cancel");
        dialog.setView(view);
        final AlertDialog alertDialog = dialog.show();

        ListView listView = (ListView)view.findViewById(R.id.listView_reason);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                reason = parent.getItemAtPosition(position).toString();
                Log.e("Reason",""+reason);
                sendCabCancel();
                alertDialog.dismiss();
            }
        });
        listView.setAdapter(adapter);

    }

    private void sendBookResponseToServer(JSONObject jsonObject)
    {
        Log.e(" Inside:",""+"sendBookResponseToServer");
        if (getActivity() != null) {
            if (ConnectionDetector.isConnectedToInternet(getActivity())) {
                String url = "https://www.lockated.com/add_ola_orders.json?token="+mLockatedPreferences.getLockatedToken();
                Log.e("url of sendBookResponseToServer",""+url);
                LockatedVolleyRequestQueue lockatedVolleyRequestQueue = LockatedVolleyRequestQueue.getInstance(getActivity());
                lockatedVolleyRequestQueue.sendPostRequestLocated(REQUEST_TAG, Request.Method.POST, url,jsonObject, this, this);
            } else {
                Utilities.showToastMessage(getActivity(), getActivity().getResources().getString(R.string.internet_connection_error));
            }
        }
    }

    private void sendTrackResponseToServer(JSONObject jsonObject)
    {
        Log.e(" Inside:",""+"sendTrackResponseToServer");
        if (getActivity() != null) {
            if (ConnectionDetector.isConnectedToInternet(getActivity())) {
                String url = "http://sandbox-t.olacabs.com/v1/bookings/track_ride?booking_id="+BookingId;
                Log.e("url of sendTrackResponseToServer",""+url);
                LockatedVolleyRequestQueue lockatedVolleyRequestQueue = LockatedVolleyRequestQueue.getInstance(getActivity());
                lockatedVolleyRequestQueue.sendPostRequestOlaCabsConfirmEx(REQUEST_TAG, Request.Method.POST, url,AccessToken,jsonObject, this, this);
            } else {
                Utilities.showToastMessage(getActivity(), getActivity().getResources().getString(R.string.internet_connection_error));
            }
        }
    }

    private  enum Type
    {
        mini,micro,sedan,prime,auto
    }
}