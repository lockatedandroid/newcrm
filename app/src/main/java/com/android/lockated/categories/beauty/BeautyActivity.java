package com.android.lockated.categories.beauty;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;

import com.android.lockated.LockatedApplication;
import com.android.lockated.R;
import com.android.lockated.categories.beauty.fragment.BeautyFragment;

public class BeautyActivity extends AppCompatActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_vendor);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(true);
        getSupportActionBar().setTitle(((LockatedApplication) getApplicationContext()).getHeaderName());

        ((LockatedApplication) getApplicationContext()).setMyAddressData(null);
        ((LockatedApplication) getApplicationContext()).setmDeliveryTime("");
        ((LockatedApplication) getApplicationContext()).setmDeliveryDate("");

        if (savedInstanceState == null) {
            BeautyFragment beautyFragment = new BeautyFragment();
            getSupportFragmentManager().beginTransaction().add(R.id.mVendorContainer, beautyFragment).commit();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home: {
                super.onBackPressed();
                return true;
            }
        }
        return super.onOptionsItemSelected(item);
    }
}
