package com.android.lockated.categories.olacabs.adapter;

import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.lockated.R;
import com.android.lockated.utils.Utilities;

import java.util.ArrayList;

/**
 * Created by HAVEN INFOLINE on 7/2/2016.
 */
public class CouponAdapter  extends RecyclerView.Adapter<CouponAdapter.CouponViewHolder> {

    Context context;
    FragmentManager fragmentManager;
    ArrayList<String> coupon_code;
    ArrayList<String> coupon_head;
    ArrayList<String> coupon_sub_head_1;
    ArrayList<String> coupon_sub_head_2;

    public CouponAdapter(Context context,ArrayList<String> coupon_code,ArrayList<String> coupon_head,ArrayList<String> coupon_sub_head_1,ArrayList<String> coupon_sub_head_2, FragmentManager fragmentManager) {
        this.context = context;
        this.coupon_code = coupon_code;
        this.coupon_head = coupon_head;
        this.coupon_sub_head_1 = coupon_sub_head_1;
        this.coupon_sub_head_2 = coupon_sub_head_2;
        this.fragmentManager = fragmentManager;
    }

    @Override
    public CouponViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View layout = LayoutInflater.from(context).inflate(R.layout.layout_each_coupon, parent, false);
        return new CouponViewHolder(layout);
    }

    @Override
    public void onBindViewHolder(CouponViewHolder holder, final int position)
    {
            holder.txt_coupon_head.setText(coupon_head.get(position));
            holder.txt_coupon_code.setText(coupon_code.get(position));
            holder.txt_coupon_sub_head_1.setText(coupon_sub_head_1.get(position));
            holder.txt_coupon_sub_head_2.setText(coupon_sub_head_2.get(position));

            holder.coupon_main.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    // Gets a handle to the clipboard service.
                    ClipboardManager clipboard = (ClipboardManager)
                            context.getSystemService(Context.CLIPBOARD_SERVICE);
                    ClipData clip = ClipData.newPlainText("coupon_code",coupon_code.get(position));

                    Utilities.showToastMessage(context,"Coupon code "+coupon_code.get(position)+" copied to your clipboard");

            }});
    }

    @Override
    public int getItemCount() {
        return coupon_code.size();
    }

    public class CouponViewHolder extends RecyclerView.ViewHolder {

        TextView txt_coupon_head,txt_coupon_code,txt_coupon_sub_head_1,txt_coupon_sub_head_2;
        LinearLayout coupon_main;
        public CouponViewHolder(final View itemView) {
            super(itemView);
            coupon_main = (LinearLayout)itemView.findViewById(R.id.coupon_main);

            txt_coupon_head = (TextView) itemView.findViewById(R.id.txt_coupon_head);
            txt_coupon_code = (TextView) itemView.findViewById(R.id.txt_coupon_code);
            txt_coupon_sub_head_1 = (TextView) itemView.findViewById(R.id.txt_coupon_sub_head_1);
            txt_coupon_sub_head_2 = (TextView) itemView.findViewById(R.id.txt_coupon_sub_head_2);
        }
    }

}
