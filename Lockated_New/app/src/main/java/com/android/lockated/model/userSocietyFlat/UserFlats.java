
package com.android.lockated.model.userSocietyFlat;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class UserFlats {

    @SerializedName("user_flats")
    @Expose
    private ArrayList<UserFlat> userFlats = new ArrayList<UserFlat>();

    /**
     * 
     * @return
     *     The userFlats
     */
    public ArrayList<UserFlat> getUserFlats() {
        return userFlats;
    }

    /**
     * 
     * @param userFlats
     *     The user_flats
     */
    public void setUserFlats(ArrayList<UserFlat> userFlats) {
        this.userFlats = userFlats;
    }

}
