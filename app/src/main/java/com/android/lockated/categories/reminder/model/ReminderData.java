package com.android.lockated.categories.reminder.model;

import com.android.lockated.utils.Utilities;

import org.json.JSONObject;

public class ReminderData
{
    private int id;
    private int user_id;
    private String subject;
    private String description;
    private String date;
    private String time;

    public ReminderData(JSONObject jsonObject)
    {
        id = jsonObject.optInt("id");
        user_id = jsonObject.optInt("user_id");
        subject = jsonObject.optString("subject");
        description = jsonObject.optString("description");
        date = jsonObject.optString("date");
        time = jsonObject.optString("time");
        if (time != null)
        {
            time = Utilities.dateConvertToTime(time);
        }
    }

    public int getId()
    {
        return id;
    }

    public int getUser_id()
    {
        return user_id;
    }

    public String getSubject()
    {
        return subject;
    }

    public String getDescription()
    {
        return description;
    }

    public String getDate()
    {
        return date;
    }

    public String getTime()
    {
        return time;
    }
}
