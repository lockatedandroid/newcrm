package com.android.lockated.categories.beauty.fragment;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.android.lockated.Interfaces.IRecyclerItemClickListener;
import com.android.lockated.Interfaces.IServiceViewListener;
import com.android.lockated.LockatedApplication;
import com.android.lockated.R;
import com.android.lockated.categories.adapter.ServiceRowAdapter;
import com.android.lockated.categories.beauty.BeautyServiceActivity;
import com.android.lockated.categories.services.model.ServiceData;
import com.android.lockated.categories.services.model.ServiceRowData;
import com.android.lockated.information.ServiceController;
import com.android.lockated.network.ConnectionDetector;
import com.android.lockated.preferences.LockatedPreferences;
import com.android.lockated.request.LockatedRequestError;
import com.android.lockated.request.LockatedVolleyRequestQueue;
import com.android.lockated.utils.ApplicationURL;
import com.android.lockated.utils.Utilities;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class BeautyServiceFragment extends Fragment implements Response.Listener, Response.ErrorListener, IServiceViewListener, View.OnClickListener, IRecyclerItemClickListener
{
    private ServiceController mServiceController;

    private ProgressBar mProgressBarView;
    private TextView mTextViewServiceContinue;
    private FloatingActionButton mFloatingActionButton;

    private ServiceRowAdapter mServiceAdapter;
    private int mSectionId;
    private int mSupplierId;
    private int mServicePosition = -1;
    private int mSubServicePosition = -1;

    private ArrayList<String> mServiceNameList;
    private ArrayList<String> mSubServiceNameList;
    private ArrayList<ServiceData> mServiceDataList;

    private static final String REQUEST_TAG = "BeautyServiceFragment";
    private LockatedPreferences mLockatedPreferences;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        View serviceView = inflater.inflate(R.layout.fragment_service, container, false);
        init(serviceView);
        getServiceData();
        return serviceView;
    }

    private void init(View serviceView)
    {
        mServiceDataList = new ArrayList<>();
        mServiceNameList = new ArrayList<>();
        mSubServiceNameList = new ArrayList<>();
        mServiceController = ServiceController.getInstance();
        mLockatedPreferences = new LockatedPreferences(getActivity());
        mSectionId = ((LockatedApplication) getActivity().getApplicationContext()).getmSectionId();
        mSupplierId = ((LockatedApplication) getActivity().getApplicationContext()).getCategoryId();

        mServiceDataList.clear();
        mServiceController.resetData();

        mProgressBarView = (ProgressBar) serviceView.findViewById(R.id.mProgressBarView);
        RecyclerView mRecyclerView = (RecyclerView) serviceView.findViewById(R.id.mRecyclerView);
        mTextViewServiceContinue = (TextView) serviceView.findViewById(R.id.mTextViewServiceContinue);
        mFloatingActionButton = (FloatingActionButton) serviceView.findViewById(R.id.mFloatingActionButton);

        LinearLayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        mRecyclerView.setLayoutManager(mLayoutManager);
        mRecyclerView.setHasFixedSize(true);

        mFloatingActionButton.setOnClickListener(this);
        mTextViewServiceContinue.setOnClickListener(this);

        mServiceAdapter = new ServiceRowAdapter(mServiceController.getServiceRowDataList(), this, this);
        mRecyclerView.setAdapter(mServiceAdapter);
    }

    private void getServiceData()
    {
        if (ConnectionDetector.isConnectedToInternet(getActivity()))
        {
            mProgressBarView.setVisibility(View.VISIBLE);

            LockatedVolleyRequestQueue lockatedVolleyRequestQueue = LockatedVolleyRequestQueue.getInstance(getActivity());
            lockatedVolleyRequestQueue.sendRequest(REQUEST_TAG, Request.Method.GET, ApplicationURL.getServiceDataUrl + mSectionId +"&supplier_id="+mSupplierId+"&token=" + mLockatedPreferences.getLockatedToken(), null, this, this);
        }
        else
        {
            Utilities.showToastMessage(getActivity(), getActivity().getResources().getString(R.string.internet_connection_error));
        }
    }

    private void addNewRowToView()
    {
        mServiceController.mServiceRowDataList.add(new ServiceRowData());
        mServiceAdapter.notifyDataSetChanged();
    }

    private void onServiceClicked(final int position)
    {
        CharSequence[] cs = mServiceNameList.toArray(new CharSequence[mServiceNameList.size()]);
        new AlertDialog.Builder(getActivity()).setSingleChoiceItems(cs, -1, new DialogInterface.OnClickListener()
        {
            public void onClick(DialogInterface dialog, int whichButton)
            {
                dialog.dismiss();
                mServicePosition = whichButton;
                ServiceRowData serviceRowData = mServiceController.getServiceRowDataList().get(position);
                serviceRowData.setServiceName(mServiceDataList.get(mServicePosition).getService_name());
                serviceRowData.setId(mServiceDataList.get(mServicePosition).getId());
                serviceRowData.setCategoryId(mServiceDataList.get(mServicePosition).getCategory_id());
                serviceRowData.setSubCategoryId(mServiceDataList.get(mServicePosition).getSub_category_id());

                if (mServiceDataList.get(mServicePosition).getServiceAttributeDatas().size() > 0)
                {
                    serviceRowData.setAttributeId(mServiceDataList.get(mServicePosition).getServiceAttributeDatas().get(0).getId());
                    serviceRowData.setSubServiceName(mServiceDataList.get(mServicePosition).getServiceAttributeDatas().get(0).getName());
                    serviceRowData.setServiceCharges(mServiceDataList.get(mServicePosition).getServiceAttributeDatas().get(0).getApprox_charge());
                }
                mServiceAdapter.notifyDataSetChanged();
            }
        }).show();
    }

    private void onSubServiceClicked(final int position)
    {
        if (mServiceDataList.size() > 0)
        {
            mSubServiceNameList = mServiceDataList.get(mServicePosition).getSubServiceNameList();
            if (mSubServiceNameList.size() > 0)
            {
                CharSequence[] cs = mSubServiceNameList.toArray(new CharSequence[mSubServiceNameList.size()]);
                new AlertDialog.Builder(getActivity()).setSingleChoiceItems(cs, -1, new DialogInterface.OnClickListener()
                {
                    public void onClick(DialogInterface dialog, int whichButton)
                    {
                        dialog.dismiss();
                        mSubServicePosition = whichButton;
                        mServiceController.getServiceRowDataList().get(position).setAttributeId(mServiceDataList.get(mServicePosition).getServiceAttributeDatas().get(mSubServicePosition).getId());
                        mServiceController.getServiceRowDataList().get(position).setSubServiceName(mServiceDataList.get(mServicePosition).getServiceAttributeDatas().get(mSubServicePosition).getName());
                        mServiceController.getServiceRowDataList().get(position).setServiceCharges(mServiceDataList.get(mServicePosition).getServiceAttributeDatas().get(mSubServicePosition).getApprox_charge());
                        mServiceAdapter.notifyDataSetChanged();
                    }
                }).show();
            }
        }
    }

    @Override
    public void onErrorResponse(VolleyError error)
    {
        if (getActivity() != null)
        {
            mProgressBarView.setVisibility(View.INVISIBLE);
            LockatedRequestError.onRequestError(getActivity(), error);
        }
    }

    @Override
    public void onResponse(Object response)
    {
        if (getActivity() != null)
        {
            mProgressBarView.setVisibility(View.INVISIBLE);

            JSONObject jsonObject = (JSONObject) response;
            try
            {
                JSONArray jsonArray = jsonObject.getJSONArray("request_metadata");
                for (int i = 0; i < jsonArray.length(); i++)
                {
                    JSONObject serviceMetadataObject = jsonArray.getJSONObject(i);
                    ServiceData serviceData = new ServiceData(serviceMetadataObject);
                    mServiceDataList.add(serviceData);
                    mServiceNameList.add(serviceMetadataObject.optString("service_name"));
                }

                addNewRowToView();
            }
            catch (JSONException e)
            {
                e.printStackTrace();
            }
        }
    }

    private void onServiceClick(int position)
    {
        onServiceClicked(position);
    }

    private void onSubServiceClick(int position)
    {
        if (mServiceController.getServiceRowDataList().get(position).getId() != -1)
        {
            onSubServiceClicked(position);
        } else
        {
            Utilities.showToastMessage(getActivity(), getActivity().getString(R.string.service_selection_error));
        }
    }

    @Override
    public void onClick(View v)
    {
        switch (v.getId())
        {
            case R.id.mFloatingActionButton:
                addNewRowToView();
                break;

            case R.id.mTextViewServiceContinue:
                onContinueClicked();
                break;
        }
    }

    private void onContinueClicked()
    {
        if (!isUnselectedRowAvailable())
        {
            ((BeautyServiceActivity)getActivity()).addNewFragmentToStack();
        }
        else
        {
            Utilities.showToastMessage(getActivity(), getActivity().getString(R.string.continue_error));
        }
    }

    private boolean isUnselectedRowAvailable()
    {
        boolean isExist = false;
        ArrayList<ServiceRowData> rowData = mServiceController.getServiceRowDataList();
        if (rowData.size() > 0)
        {
            for (int i = 0; i <rowData.size(); i++)
            {
                if (mServiceController.getServiceRowDataList().get(i).getId() == -1)
                {
                    isExist = true;
                }
            }
        }

        return isExist;
    }

    private void onServiceRowLongClick(int position)
    {
        if (mServiceController.getServiceRowDataList().size() > 1)
        {
            alertServiceDelete(position);
        }
        else
        {
            Utilities.showToastMessage(getActivity(), getActivity().getString(R.string.service_remove_error));
        }
    }

    @Override
    public void onServiceRowBrand(String brand, int position)
    {
        mServiceController.getServiceRowDataList().get(position).setServiceBrand(brand);
    }

    @Override
    public void onServiceRowModel(String model, int position)
    {
        mServiceController.getServiceRowDataList().get(position).setServiceModel(model);
    }

    public void alertServiceDelete(final int position)
    {
        final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setMessage(R.string.service_remove_message)
                .setCancelable(false)
                .setPositiveButton("Yes", new DialogInterface.OnClickListener()
                {
                    public void onClick(final DialogInterface dialog, final int id)
                    {
                        mServiceController.getServiceRowDataList().remove(position);
                        mServiceAdapter.notifyDataSetChanged();
                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener()
                {
                    public void onClick(final DialogInterface dialog, final int id)
                    {
                        dialog.cancel();
                    }
                });
        final AlertDialog alert = builder.create();
        alert.show();
    }

    @Override
    public void onRecyclerItemClick(View view, int position)
    {
        switch (view.getId())
        {
            case R.id.mLinearLayoutService:
                onServiceClick(position);
                break;
            case R.id.mLinearLayoutSubService:
                onSubServiceClick(position);
                break;
            case R.id.mLinearLayoutServiceRow:
                onServiceRowLongClick(position);
                break;
        }
    }
}
