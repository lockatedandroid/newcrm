package com.android.lockated.crmadmin.activity;


import android.Manifest;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.design.widget.TabLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.android.lockated.HomeActivity;
import com.android.lockated.IndexActivity;
import com.android.lockated.LockatedApplication;
import com.android.lockated.R;
import com.android.lockated.cart.ecommerce.EcommerceCartActivity;
import com.android.lockated.crmadmin.adapter.AdminNoticeBoardPagerAdapter;
import com.android.lockated.drawer.PoliciesActivity;
import com.android.lockated.information.AccountController;
import com.android.lockated.model.AccountData;
import com.android.lockated.model.CartDetail;
import com.android.lockated.network.ConnectionDetector;
import com.android.lockated.preferences.LockatedPreferences;
import com.android.lockated.utils.Utilities;

import java.util.ArrayList;

public class AdminNoticeBoardActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener,
        View.OnClickListener {

    ViewPager noticeBoardPager;
    TabLayout tabLayout;
    View lineColor;

    private ImageView mImageViewHome;
    private ImageView mImageViewChat;
    private ImageView mImageViewAccount;
    private ImageView mImageViewLocation;
    private ImageView mImageViewOffers;

    private LockatedPreferences mLockatedPreferences;
    private AccountController mAccountController;

    private static final int REQUEST_CALL = 11;
    LinearLayout linearLayoutContainer;
    String[] nameList = {"Pending Notices", "Published Notices"};
    int position;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notice_board_admin);

        linearLayoutContainer = (LinearLayout) findViewById(R.id.mLandingContainer);
        mAccountController = AccountController.getInstance();
        mLockatedPreferences = new LockatedPreferences(this);
        position = getIntent().getExtras().getInt("AdminNotice");
        String id=getIntent().getExtras().getString("id");
        setTabLayout();
        actionBar("Notice");
        //setBottomTabView();

    }

    public void actionBar(String name) {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(true);
        getSupportActionBar().setTitle(name);
    }

    public void setTabLayout() {
        lineColor = findViewById(R.id.colorView);
        tabLayout = (TabLayout) findViewById(R.id.tab_layout);
        for (String s : nameList) {
            tabLayout.addTab(tabLayout.newTab().setText(s));
        }
        tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);
        noticeBoardPager = (ViewPager) findViewById(R.id.noticeBoardPager);
        noticeBoardPager.setAdapter(new AdminNoticeBoardPagerAdapter(getSupportFragmentManager(),
                tabLayout.getTabCount(), nameList));
        noticeBoardPager.setCurrentItem(position);
        noticeBoardPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
        tabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                noticeBoardPager.setCurrentItem(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
    }

    public void setToolbar() {

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setLogo(R.drawable.ic_header_logo);
        toolbar.setTitle("");
        setSupportActionBar(toolbar);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, drawer, toolbar,
                R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        //navigationView.getMenu().findItem(R.id.crmScreen).setVisible(true);
        navigationView.setNavigationItemSelectedListener(this);

    }

    private void setBottomTabView() {
        LinearLayout mLinearLayoutHome = (LinearLayout) findViewById(R.id.mLinearLayoutHome);
        LinearLayout mLinearLayoutSupport = (LinearLayout) findViewById(R.id.mLinearLayoutSupport);
        LinearLayout mLinearLayoutAccount = (LinearLayout) findViewById(R.id.mLinearLayoutAccount);
        LinearLayout mLinearLayoutLocation = (LinearLayout) findViewById(R.id.mLinearLayoutLocation);
        LinearLayout mLinearLayoutOffers = (LinearLayout) findViewById(R.id.mLinearLayoutOffers);

        mImageViewHome = (ImageView) findViewById(R.id.mImageViewHome);
        mImageViewChat = (ImageView) findViewById(R.id.mImageViewChat);
        mImageViewAccount = (ImageView) findViewById(R.id.mImageViewAccount);
        mImageViewLocation = (ImageView) findViewById(R.id.mImageViewLocation);
        mImageViewOffers = (ImageView) findViewById(R.id.mImageViewOffers);

        mLinearLayoutHome.setOnClickListener(this);
        mLinearLayoutSupport.setOnClickListener(this);
        mLinearLayoutAccount.setOnClickListener(this);
        mLinearLayoutLocation.setOnClickListener(this);
        mLinearLayoutOffers.setOnClickListener(this);

    }

    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.about_us) {
            onPoliciesClicked(getString(R.string.about_us), getString(R.string.about_us_link));
            LockatedApplication.getInstance().trackScreenView(getString(R.string.about_us));
            Utilities.lockatedGoogleAnalytics(getString(R.string.about_us), getString(R.string.visited), getString(R.string.about_us));
        } else if (id == R.id.rating) {
            onRatingClicked();
            Utilities.lockatedGoogleAnalytics(getString(R.string.rating), getString(R.string.visited), getString(R.string.rating));
        } else if (id == R.id.feedback) {
            onFeedbackClicked();
            Utilities.lockatedGoogleAnalytics(getString(R.string.feedback), "Click", getString(R.string.feedback));
        } else if (id == R.id.invite_friends) {
            onShareClicked();
            Utilities.lockatedGoogleAnalytics(getString(R.string.invite_friends), "Invite", getString(R.string.invite_friends));
        } else if (id == R.id.contact_us) {
            onContactUsClicked();
            Utilities.lockatedGoogleAnalytics(getString(R.string.contact_us), "Contact", getString(R.string.contact_us));
        } else if (id == R.id.call_us) {
            onCallUsClicked();
            Utilities.lockatedGoogleAnalytics(getString(R.string.call_us), "Call Us", getString(R.string.call_us));
        } else if (id == R.id.refund_policy) {
            onPoliciesClicked(getString(R.string.refund_policy), getString(R.string.refund_link));
            LockatedApplication.getInstance().trackScreenView(getString(R.string.refund_policy));
            Utilities.lockatedGoogleAnalytics(getString(R.string.refund_policy), getString(R.string.visited), getString(R.string.refund_policy));
        } else if (id == R.id.privacy_policy) {
            onPoliciesClicked(getString(R.string.privacy_policy), getString(R.string.privacy_link));
            LockatedApplication.getInstance().trackScreenView(getString(R.string.privacy_policy));
            Utilities.lockatedGoogleAnalytics(getString(R.string.privacy_policy), getString(R.string.visited), getString(R.string.privacy_policy));
        } else if (id == R.id.terms_conditions) {
            onPoliciesClicked(getString(R.string.terms_conditions), getString(R.string.terms_condition_link));
            LockatedApplication.getInstance().trackScreenView(getString(R.string.terms_conditions));
            Utilities.lockatedGoogleAnalytics(getString(R.string.terms_conditions), "Visited", getString(R.string.terms_conditions));
        } else if (id == R.id.logout) {
            showAlertDialog(this);
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    private void onCallUsClicked() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                requestPermissions(new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, REQUEST_CALL);
            } else {
                callingIntent();
            }
        } else {
            callingIntent();
        }
    }

    private void callingIntent() {
        Intent intent = new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + getString(R.string.calling_number)));
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        startActivity(intent);
    }

    private void onRatingClicked() {
        Uri uri = Uri.parse("market://details?id=" + getPackageName());
        Intent myAppLinkToMarket = new Intent(Intent.ACTION_VIEW, uri);
        try {
            startActivity(myAppLinkToMarket);
        } catch (ActivityNotFoundException e) {
            Utilities.showToastMessage(this, "Unable to find application on Play store.");
        }
    }

    private void onShareClicked() {
        Intent sendIntent = new Intent();
        sendIntent.setAction(Intent.ACTION_SEND);
        sendIntent.putExtra(Intent.EXTRA_TEXT, getString(R.string.sharing_url));
        sendIntent.setType("text/plain");
        startActivity(Intent.createChooser(sendIntent, getResources().getText(R.string.share_message)));
    }

    private void onPoliciesClicked(String name, String url) {
        if (ConnectionDetector.isConnectedToInternet(this)) {
            Intent detailIntent = new Intent(this, PoliciesActivity.class);
            ((LockatedApplication) getApplicationContext()).setSectionName(name);
            ((LockatedApplication) getApplicationContext()).setSectionURL(url);
            startActivity(detailIntent);
        } else {
            Utilities.showToastMessage(this, getString(R.string.internet_connection_error));
        }
    }

    private void onFeedbackClicked() {
        Intent intent = new Intent(Intent.ACTION_SEND);
        intent.setType("message/rfc822");
        intent.putExtra(Intent.EXTRA_EMAIL, new String[]{getString(R.string.feedback_email)});
        intent.putExtra(Intent.EXTRA_SUBJECT, getString(R.string.feedback_subject));
        startActivity(Intent.createChooser(intent, getString(R.string.contact_us_message)));
    }

    private void onContactUsClicked() {
        Intent intent = new Intent(Intent.ACTION_SEND);
        intent.setType("message/rfc822");
        intent.putExtra(Intent.EXTRA_EMAIL, new String[]{getString(R.string.contact_us_email)});
        intent.putExtra(Intent.EXTRA_SUBJECT, getString(R.string.contact_us_subject));
        startActivity(Intent.createChooser(intent, getString(R.string.contact_us_message)));
    }

    public void showAlertDialog(Context context) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setMessage(R.string.logout_dialog_message);

        // Add the buttons
        builder.setPositiveButton(R.string.logout, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.dismiss();
                onLogout();
            }
        });

        builder.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.dismiss();
            }
        });

        AlertDialog dialog = builder.create();
        dialog.show();
    }

    private void onLogout() {
        mAccountController.getmAccountDataList().clear();
        mLockatedPreferences.setLockatedToken("");
        mLockatedPreferences.setLogout(true);

        Utilities.lockatedGoogleAnalytics(getString(R.string.logout), "Logout", getString(R.string.logout));

        Intent intent = new Intent(this, IndexActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.mLinearLayoutHome:
                bottomTabClicked(getApplicationContext().getResources().getString(R.string.home_clicked));
                //onHomeClicked();
                break;
            case R.id.mLinearLayoutSupport:
                bottomTabClicked(getApplicationContext().getResources().getString(R.string.chat_support_clicked));
                //onSupportClicked();
                break;
            case R.id.mLinearLayoutAccount:
                bottomTabClicked(getApplicationContext().getResources().getString(R.string.account_clicked));
                //onAccountClicked(0);
                break;
            case R.id.mLinearLayoutLocation:
                bottomTabClicked(getApplicationContext().getResources().getString(R.string.location_clicked));
                //onLocationClicked();
                break;
            case R.id.mLinearLayoutOffers:
                bottomTabClicked(getApplicationContext().getResources().getString(R.string.offer_clicked));
                //onOfferClicked();
                break;
        }
    }

    private void bottomTabClicked(String name) {
        //Log.e("bottomTabClicked", "name " + name);
        Intent intent = new Intent(this, HomeActivity.class);
        intent.putExtra("HomeActivityCalled", name);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
        finish();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.home, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_cart:
                onCartClicked();
                return false;
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void onCartClicked() {
        ArrayList<AccountData> accountDatas = AccountController.getInstance().getmAccountDataList();
        if (accountDatas.size() > 0) {
            ArrayList<CartDetail> cartDetails = accountDatas.get(0).getCartDetailList();
            if (cartDetails.size() > 0) {
                if (cartDetails.get(0).getmLineItemData().size() > 0) {
                    Intent intent = new Intent(this, EcommerceCartActivity.class);
                    startActivity(intent);
                } else {
                    Utilities.showToastMessage(this, getString(R.string.empty_cart));
                }
            } else {
                Utilities.showToastMessage(this, getString(R.string.empty_cart));
            }
        }
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(this, AdminActivity.class);
        intent.putExtra("AdminActivityPosition",1);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
        finish();
    }

}
