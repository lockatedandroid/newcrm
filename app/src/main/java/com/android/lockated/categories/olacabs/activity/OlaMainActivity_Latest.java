package com.android.lockated.categories.olacabs.activity;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.location.Location;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.ResultReceiver;
import android.provider.Settings;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.Toolbar;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.TypedValue;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.android.lockated.LockatedApplication;
import com.android.lockated.R;
import com.android.lockated.categories.olacabs.adapter.CouponAdapter;
import com.android.lockated.categories.olacabs.adapter.OlaCabsAdapter;
import com.android.lockated.categories.olacabs.fragment.AvailMainFragment;
import com.android.lockated.categories.olacabs.fragment.CheapestOlaRideFragment;
import com.android.lockated.categories.olacabs.fragment.ConfirmOlaBookingFragment;
import com.android.lockated.categories.olacabs.fragment.OlaCabCancelFragment;
import com.android.lockated.model.OlaCab.Category;
import com.android.lockated.model.OlaCab.OlaCabCategoryDetail;
import com.android.lockated.model.OlaCab.RideEstimate;
import com.android.lockated.network.ConnectionDetector;
import com.android.lockated.preferences.LockatedPreferences;
import com.android.lockated.request.LockatedRequestError;
import com.android.lockated.request.LockatedVolleyRequestQueue;
import com.android.lockated.utils.Utilities;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.GoogleMapOptions;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.gson.Gson;
import com.sothree.slidinguppanel.SlidingUpPanelLayout;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;


public class OlaMainActivity_Latest extends AppCompatActivity implements GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener, LocationListener, SlidingUpPanelLayout.PanelSlideListener, Response.Listener<String>, Response.ErrorListener, View.OnClickListener {

    private static Context mContext;
    private String screenName = "OlaMainActivity_Latest";
    public MapFragment mapFragment;
    public static GoogleMap gmap;
    private int action_bar_height, status_bar_height, panel_height, window_height;

    private LockatedPreferences mLockatedPreferences;
    public static MarkerOptions user_marker_option;
    public static MarkerOptions driver_marker_option;

    public static Marker user_marker, driver_marker;
    private static final int REQUEST_LOCATION = 10;
    private static final int CHECK_LOCATION_PERMISSION = 11;

    public static GoogleApiClient mGoogleApiClient;
    private LocationRequest mLocationRequest;
    private long UPDATE_INTERVAL = 20000;//5 * 60 * 1000; //5 minutes //60000;  /* 60 secs */
    private long FASTEST_INTERVAL = 30000;// 2 * 60 * 1000;//2 minutes //5000; /* 5 secs */

    private static final String REQUEST_TAG = "OLA CABS";
    private final static int CONNECTION_FAILURE_RESOLUTION_REQUEST = 9000;

    //public static AutoCompleteTextView edt_olaSourceLocation, edt_olaDestinationLocation;
    //public static LinearLayout location_button;

    public static SlidingUpPanelLayout mLayout;
    public static EditText edt_olaSourceLocation, edt_olaDestinationLocation;
    public static ImageView img_olaShowDestinationField, img_ola_cancel;
    public static LinearLayout mOlaSourceLayout, mOlaDestinationLayout;
    public static CardView mOlaSourceCard, mOlaDestinationCard;
    public static LinearLayout show_destination, layout_marker, layout_ola_cancel;
    public static LinearLayout current_location_zoom; //--------change on 19-06-2016
    //public static ImageButton location_button_new;//--------change on 19-06-2016
    public static ImageView imageMarker;
    private View view;
    private Toolbar mToolbar;

    private ArrayList<Category> olaCabCategorys;
    private ArrayList<RideEstimate> olaCabRideEstimates;
    private ArrayList<OlaCabCategoryDetail> olaCabCategoryDetails;

    public static AddressResultReceiver mResultReceiver;
    private OlaCabsAdapter olaCabsAdapter;
    private CouponAdapter couponAdapter;
    private LatLng mCenterLatLong;
    public static double srcLat, srcLng;
    public static double destLat, destLng;
    public static String mAddressOutput;

    private static Response.ErrorListener errorListener;
    private static Response.Listener<String> listener;

    public OlaMainActivity_Latest() {
    }

    @SuppressLint("LongLogTag")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Utilities.ladooIntegration(this, screenName);
        Utilities.lockatedGoogleAnalytics(screenName, getString(R.string.visited), screenName);

        setContentView(R.layout.activity_ola_main);
        setTitle();
        mContext = OlaMainActivity_Latest.this;
        listener = this;
        errorListener = this;

        mLayout = (SlidingUpPanelLayout) findViewById(R.id.sliding_layout);
        setPanelHeight();

        // Calculate ActionBar height
        TypedValue tv = new TypedValue();
        if (getTheme().resolveAttribute(android.R.attr.actionBarSize, tv, true)) {
            action_bar_height = TypedValue.complexToDimensionPixelSize(tv.data, getResources().getDisplayMetrics());
        }

        status_bar_height = getStatusBarHeight();
        layout_marker = (LinearLayout) findViewById(R.id.layout_marker);

        FrameLayout.LayoutParams lp = new FrameLayout.LayoutParams(FrameLayout.LayoutParams.MATCH_PARENT,
                window_height - status_bar_height - panel_height - action_bar_height); // or set height to any fixed value you want

        Log.e("window_height", "" + window_height);
        Log.e("status_bar_height", "" + status_bar_height);
        Log.e("panel_height", "" + panel_height);
        Log.e("action_bar_height", "" + action_bar_height);
        int layout_marker_width = window_height - status_bar_height - panel_height - action_bar_height;

        Log.e("window_height-status_bar_height-panel_height-action_bar_height", "" + layout_marker_width);
        layout_marker.setLayoutParams(lp);

        init(savedInstanceState);
        checkLocationPermission();
    }

    public void setTitle() {
        mToolbar = (Toolbar) findViewById(R.id.custom_toolbar);
        setSupportActionBar(mToolbar);
        getSupportActionBar().setTitle("Ola cabs");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(true);
    }

    @SuppressLint("LongLogTag")
    public void init(Bundle savedInstanceState) {
        mLockatedPreferences = new LockatedPreferences(this);

        mLockatedPreferences.setSrcLat(0);
        mLockatedPreferences.setSrcLng(0);

        mLockatedPreferences.setDstLat(0);
        mLockatedPreferences.setDstLng(0);

        //olaCabCategoryDetails = new ArrayList<Category>();

        olaCabCategoryDetails = new ArrayList<OlaCabCategoryDetail>();
        olaCabCategorys = new ArrayList<Category>();
        olaCabRideEstimates = new ArrayList<RideEstimate>();


        mapFragment = ((MapFragment) getFragmentManager().findFragmentById(R.id.map));
        edt_olaSourceLocation = (EditText) findViewById(R.id.edt_olaSourceLocation);
        edt_olaDestinationLocation = (EditText) findViewById(R.id.edt_olaDestinationLocation);
        mOlaSourceLayout = (LinearLayout) findViewById(R.id.mOlaSourceLayout);
        mOlaDestinationLayout = (LinearLayout) findViewById(R.id.mOlaDestinationLayout);
        mOlaSourceCard = (CardView) findViewById(R.id.mOlaSourceCard);
        mOlaDestinationCard = (CardView) findViewById(R.id.mOlaDestinationCard);
        imageMarker = (ImageView) findViewById(R.id.imageMarker);
        //location_button_new = (ImageButton) findViewById(R.id.location_button_new);//--------change on 19-06-2016
        current_location_zoom = (LinearLayout) findViewById(R.id.current_location_zoom);//---------change on 16-9-2016
        show_destination = (LinearLayout) findViewById(R.id.show_destination);
        img_olaShowDestinationField = (ImageView) findViewById(R.id.img_olaShowDestinationField);
        img_ola_cancel = (ImageView) findViewById(R.id.img_ola_cancel);
        layout_ola_cancel = (LinearLayout) findViewById(R.id.layout_ola_cancel);
        view = findViewById(R.id.divider);
        mResultReceiver = new AddressResultReceiver(new Handler());


        mLayout.setScrollableViewHelper(new NestedScrollableViewHelper());
        //edt_olaSourceLocation.setHorizontallyScrolling(true);
        //edt_olaSourceLocation.setMovementMethod(new ScrollingMovementMethod());
        //edt_olaDestinationLocation.setHorizontallyScrolling(true);
        //edt_olaDestinationLocation.setMovementMethod(new ScrollingMovementMethod());
        imageMarker.setVisibility(View.VISIBLE);

        /*edt_olaSourceLocation.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {

                Log.e("edt_olaSourceLocation", "Touch");
                Intent intent_1 = new Intent(OlaMainActivity_Latest.this, FindGooglePlaces.class);
                intent_1.putExtra("AutoComplete", 1);
                startActivity(intent_1);
                return false;
            }
        });

*/
       /* edt_olaDestinationLocation.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {

                Log.e("edt_olaSourceLocation", "Touch");
                Intent intent_1 = new Intent(OlaMainActivity_Latest.this, FindGooglePlaces.class);
                intent_1.putExtra("AutoComplete", 2);
                startActivity(intent_1);
                return false;
            }
        });*/
        edt_olaSourceLocation.setOnClickListener(this);
        edt_olaDestinationLocation.setOnClickListener(this);
        //location_button_new.setOnClickListener(this);//--------change on 19-06-2016
        current_location_zoom.setOnClickListener(this);//------change on 16-09-2016
        show_destination.setOnClickListener(this);
        //img_ola_cancel.setOnClickListener(this);
        layout_ola_cancel.setOnClickListener(this);

        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addApi(LocationServices.API)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this).build();

        if (mapFragment != null) {
            mapFragment.getMapAsync(new OnMapReadyCallback() {
                @Override
                public void onMapReady(GoogleMap map) {
                    loadMap(map);
                }
            });
        } else {
            Toast.makeText(this, "Error - Map Fragment was null!!", Toast.LENGTH_SHORT).show();
        }

        if (savedInstanceState == null) {
            /*if (mLockatedPreferences.getIsCabBooked()&& !mLockatedPreferences.getIsAutoBooked()) {
                OlaCabCancelFragment olaCabCancelFragment = new OlaCabCancelFragment();
                FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
                fragmentTransaction.add(R.id.mFrameContainer, olaCabCancelFragment);
                fragmentTransaction.commit();

            }
            else if (!mLockatedPreferences.getIsCabBooked()&& mLockatedPreferences.getIsAutoBooked())
            {
                String jsonString = mLockatedPreferences.getTpDetail();
                try {
                    JSONObject jsonObject = new JSONObject(jsonString);
                    if (jsonObject.has("message"))
                    {
                        OlaAutoCancelFragment olaAutoCancelFragment = new OlaAutoCancelFragment();
                        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
                        fragmentTransaction.add(R.id.mFrameContainer,olaAutoCancelFragment);
                        fragmentTransaction.commit();
                    }
                    else
                    {
                        OlaAutoConfirmCancel olaAutoConfirmCancel = new OlaAutoConfirmCancel();
                        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
                        fragmentTransaction.add(R.id.mFrameContainer,olaAutoConfirmCancel);
                        fragmentTransaction.commit();
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
            else {

                AvailMainFragment availMainFragment = new AvailMainFragment();
                FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
                fragmentTransaction.add(R.id.mFrameContainer, availMainFragment);
                fragmentTransaction.commit();
            }
        }*/

            Log.e("mLockatedPreferences.getIsCabBooked()", "" + mLockatedPreferences.getIsCabBooked());
            if (mLockatedPreferences.getIsCabBooked()) {
                Log.e("IsCabBooked()", "true");
                Log.e("Activity has ", "OlaCabCancelFragment");
                OlaCabCancelFragment olaCabCancelFragment = new OlaCabCancelFragment();
                FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
                fragmentTransaction.add(R.id.mFrameContainer, olaCabCancelFragment);
                fragmentTransaction.commit();
            } else {
                Log.e("IsCabBooked()", "false");
                Log.e("Activity has ", "AvailMainFragment");
                AvailMainFragment availMainFragment = new AvailMainFragment();
                FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
                fragmentTransaction.add(R.id.mFrameContainer, availMainFragment);
                fragmentTransaction.commit();
            }
        }
    }

    //------------------------------------------------------------------------------------

    @SuppressLint("LongLogTag")
    private void checkLocationPermission() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                Log.e(" Request Permission", "PromptDialog");
                requestPermissions(new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, REQUEST_LOCATION);
               /* if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.ACCESS_FINE_LOCATION)) {
                    //Asked permission before ,choosen dont ask me again option
                    Log.e(" checkLocationPermission shouldShowRequestPermissionRationale", "true");
                    //alertGPSConnectionError();
                    initializeLocationUpdater();
                } else {
                    //Asked permission before , not choosen dont ask me again option
                    Log.e(" checkLocationPermission shouldShowRequestPermissionRationale", "false");
                    requestPermissions(new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, REQUEST_LOCATION);
                }*/
            } else {
                Log.e(" checkLocationPermission Permission", "Granted");
                initializeLocationUpdater();
            }

        } else {
            Log.e(" checkLocationPermission Request permission", "Not Known");
            initializeLocationUpdater();
        }
    }

    @SuppressLint("LongLogTag")
    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        Log.e("onRequestPermissionsResult", "Yes");
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == REQUEST_LOCATION) {
            if (grantResults.length == 1 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                Log.e("Request permission", "Granted");
                if (getGoogleApiClient() != null) {
                    edt_olaSourceLocation.setText("Fetching location...");
                    initializeLocationUpdater();
                }
            } else {
                finish();

                /*// showRationale = false if user clicks Never Ask Again, otherwise true
                Log.e("Request permission", "Denied");
                boolean showRationale = shouldShowRequestPermissionRationale(Manifest.permission.ACCESS_FINE_LOCATION);
                if (!showRationale) {
                    Log.e("showRationale", String.valueOf(false));
                    //alertGPSConnectionError();
                    initializeLocationUpdater();
                    //alertForMarshMellow();
                    // user denied flagging NEVER ASK AGAIN
                    // you can either enable some fall back,
                    // disable features of your app
                    // or open another dialog explaining
                    // again the permission and directing to
                    // the app setting
                } else {
                    //alertForMarshMellow();
                    //alertGPSConnectionError();
                    initializeLocationUpdater();
                    Log.e("showRationale", String.valueOf(true));
                    // user denied WITHOUT never ask again
                    // this is a good place to explain the user
                    // why you need the permission and ask if he want
                    // to accept it (the rationale)
                }*/
            }
        }
    }

    @SuppressLint("LongLogTag")
    private void initializeLocationUpdater() {
        Log.e("InitializeLocationUpdater", "Yes");
        LocationManager mLocationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        if (mLocationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
            Log.e("Location Provider Enabled", "Yes");
            if (getGoogleApiClient() != null) {
                Log.e("getGoogleApiClient ", "is not null");
                if (Utilities.isPlayServiceAvailable(this)) {
                    if (mGoogleApiClient.isConnected()) {
                        Log.e("mGoogleApiClient", "is already connected");
                        buildGoogleApiClient();//added on 24-8-2016 at 4.22 pm
                    } else {
                        Log.e("mGoogleApiClient", "is  not connected");
                        buildGoogleApiClient();
                    }
                } else {
                    Utilities.serviceUpdateError(this, "Google Play Service is not Updated. Would you like to update?");
                }
            } else {
                Log.e("getGoogleApiClient ", "is null");
            }
        } else {
            Log.e("Location Provider Enabled", "No");
            alertGPSConnectionError();
        }
    }

    public GoogleApiClient getGoogleApiClient() {
        return mGoogleApiClient;
    }

    //-------------------------------------------------------------------------------

    private void buildGoogleApiClient() {
        createLocationRequest();
        connectClient();
    }

    protected void createLocationRequest() {
        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(UPDATE_INTERVAL);
        mLocationRequest.setFastestInterval(FASTEST_INTERVAL);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
    }

    protected void connectClient() {
        if (isGooglePlayServicesAvailable() && mGoogleApiClient != null) {
            mGoogleApiClient.connect();
        }
    }

    //----------------------------- Loading Map--------------------------------------

    public void loadMap(GoogleMap googleMap) {
        gmap = googleMap;
        if (gmap != null) {
            // Map is ready
            //Toast.makeText(this, "Map Fragment was loaded properly!", Toast.LENGTH_SHORT).show();
            Log.e("Map", "Map Fragment was loaded properly");
            getMyLocation();
        } else {
            //Toast.makeText(this, "Error - Map was null!!", Toast.LENGTH_SHORT).show();
            Log.e("Map", "Error - Map was null!!");
        }
    }

    public void getMyLocation() {
        if (gmap != null) {
            //checkPermission();
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                return;
            }
            gmap.setMyLocationEnabled(true);
            GoogleMapOptions options = new GoogleMapOptions();
            options.mapType(GoogleMap.MAP_TYPE_NORMAL)
                    .compassEnabled(false)
                    .rotateGesturesEnabled(false)
                    .tiltGesturesEnabled(false);
            MapFragment.newInstance(options);

            MapsInitializer.initialize(this);


            //gmap.setMyLocationEnabled(true);
            gmap.getUiSettings().setMyLocationButtonEnabled(false);
            gmap.getUiSettings().setScrollGesturesEnabled(true);
            ///------------------------------------------------

            DisplayMetrics dm = new DisplayMetrics();
            getWindowManager().getDefaultDisplay().getMetrics(dm);
            int height = dm.heightPixels;

            ///------------------------------------------------
            gmap.setPadding(0, 0, 0, (int) (height / 2.5));
            gmap.setOnCameraChangeListener(new GoogleMap.OnCameraChangeListener() {
                @Override
                public void onCameraChange(CameraPosition cameraPosition) {
                    if (mLockatedPreferences.getIsCabBooked() || mLockatedPreferences.getIsAutoBooked()) {
                        Log.e("OlaMainActivity_Latest", "" + "The Cab is Booked u cannot alter camera change");
                    } else {
                        Log.d("Camera postion change" + "", cameraPosition + "");
                        mCenterLatLong = cameraPosition.target;

                        srcLat = mCenterLatLong.latitude;
                        srcLng = mCenterLatLong.longitude;

                        Log.e("srcLat", "" + srcLat);
                        Log.e("srcLng", "" + srcLng);

                        mLockatedPreferences.setSrcLat((float) srcLat);
                        mLockatedPreferences.setSrcLng((float) srcLng);

                        //ConfirmOlaBookingFragment.pickup_latitude = mCenterLatLong.latitude;
                        //ConfirmOlaBookingFragment.pickup_longitude = mCenterLatLong.longitude;

                        try {

                            Location mLocation = new Location("");
                            mLocation.setLatitude(mCenterLatLong.latitude);
                            mLocation.setLongitude(mCenterLatLong.longitude);

                            startIntentService(mLocation);

                            if (mLockatedPreferences.getDstLat() != 0 && mLockatedPreferences.getDstLng() != 0) {
                                onLocationChangeListenerNew(srcLat, srcLng, mLockatedPreferences.getDstLat(), mLockatedPreferences.getDstLng());
                            } else {
                                onLocationChangeListener(srcLat, srcLng);
                            }
                            //onLocationChangeListener(srcLat, srcLng);

                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }
            });

        }
    }

    //-----------------------------------------------------------------------------------
    @Override
    protected void onStart() {
        super.onStart();
    }

    //------------------------------------------------------------------------------------
    @Override
    public void onResume() {
        super.onResume();
        Log.e("Activity Resumed", "In" + "onResume" + "Yes");
        if (((LockatedApplication) getApplicationContext()).isGPSEnabled()) {
            Log.e("Activity Resumed", "GPSEnabled");
            ((LockatedApplication) getApplicationContext()).setIsGPSEnabled(false);
            checkLocationPermission();
        } else {
            Log.e("Activity Resumed", "GPS is not Enabled");
            //checkLocationPermission();
        }
    }
    //------------------------------------------------------------------------------------

    @SuppressLint("LongLogTag")
    @Override
    protected void onPause() {
        super.onPause();
        Log.e("mGoogleApiClient.isConnected()", "" + mGoogleApiClient.isConnected());
        Log.e("Activity Paused", "In" + "onPause" + "Yes");
        Log.e("mGoogleApiClient.isConnected()", "" + mGoogleApiClient.isConnected());
        //super.onPause();
        //stopLocationUpdates();
        //Log.e("mGoogleApiClient.isConnected()", ""+mGoogleApiClient.isConnected());
    }

    protected void stopLocationUpdates() {
        if (mGoogleApiClient.isConnected()) {
            LocationServices.FusedLocationApi.removeLocationUpdates(
                    mGoogleApiClient, this);
            mGoogleApiClient.disconnect();
        }
    }

    //------------------------------------------------------------------------------------
    @Override
    protected void onStop() {
        // Disconnecting the client invalidates it.
        if (mGoogleApiClient != null) {
            mGoogleApiClient.disconnect();
        }
        super.onStop();
    }

    //------------------------------------------------------------------------------------
    @Override
    protected void onSaveInstanceState(Bundle outState) {
        //super.onSaveInstanceState(outState);
        Log.e("In", "onSaveInstanceState");
    }

    @SuppressLint("LongLogTag")
    @Override
    public void onClick(View v) {
        switch (v.getId()) {
           /* case R.id.location_button_new://--------change on 19-06-2016
                getCurrentLocation();
                break;*/

            case R.id.current_location_zoom:  //------change on 16-09-2016
                getCurrentLocation();
                break;

            case R.id.edt_olaSourceLocation:

                Log.e("edt_olaSourceLocation", "Click");
                hideSoftKeyboard();
                //-----------Changes on 10-8-2016-------------
                //edt_olaSourceLocation.setText("");
                //-------------------------------------------
                Intent intent_1 = new Intent(OlaMainActivity_Latest.this, FindGooglePlaces.class);
                intent_1.putExtra("AutoComplete", 1);
                startActivity(intent_1);
                break;

            case R.id.edt_olaDestinationLocation:
                hideSoftKeyboard();

                Log.e("edt_olaDestinationLocation", "Click");
                //-----------Changes on 10-8-2016-------------
                //edt_olaDestinationLocation.setText("");
                //-------------------------------------------
                OlaMainActivity_Latest.edt_olaSourceLocation.setTextColor(Color.BLACK);
                Intent intent_2 = new Intent(OlaMainActivity_Latest.this, FindGooglePlaces.class);
                intent_2.putExtra("AutoComplete", 2);
                startActivity(intent_2);
                break;

            case R.id.layout_ola_cancel:
                Log.e("img_ola_cancel", "Click");
                destLat = 0;
                destLng = 0;
                mLockatedPreferences.setDstLat(0);
                mLockatedPreferences.setDstLng(0);
                edt_olaDestinationLocation.setText("");
                Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.mFrameContainer);
                if (fragment instanceof ConfirmOlaBookingFragment) {
                    findEstmateForKms();
                } else {
                    onLocationChangeListener(srcLat, srcLng);
                }
                break;

            case R.id.show_destination:

                Log.e("show_destination", "Click");
                /*if (mOlaDestinationLayout.getVisibility() == View.GONE) {
                    view.setVisibility(View.VISIBLE);
                    mOlaDestinationLayout.setVisibility(View.VISIBLE);
                } else {
                    view.setVisibility(View.VISIBLE);
                    mOlaDestinationLayout.setVisibility(View.GONE);
                }*/
                break;

        }
    }
    //------------------------------------------------------------------------------------

    /**
     * Receiver for data sent from FetchAddressIntentService.
     */
    @SuppressLint("ParcelCreator")
    class AddressResultReceiver extends ResultReceiver {
        public AddressResultReceiver(Handler handler) {
            super(handler);
        }

        /**
         * Receives data sent from FetchAddressIntentService and updates the UI in MainActivity.
         */
        @Override
        protected void onReceiveResult(int resultCode, Bundle resultData) {

            // Display the address string or an error message sent from the intent service.
            mAddressOutput = resultData.getString(Constants.RESULT_DATA_KEY);
            Log.e("mAddressOutput", " " + mAddressOutput);

            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.append(mAddressOutput);

            edt_olaSourceLocation.setText(stringBuilder);
            if (resultCode == Constants.SUCCESS_RESULT) {
                Log.e("ReceiveResult", "Address Found");
            }
        }
    }
    //------------------------------------------------------------------------------------

    /**
     * Creates an intent, adds location data to it as an extra, and starts the intent service for
     * fetching an address.
     */
    protected void startIntentService(Location mLocation) {
        // Create an intent for passing to the intent service responsible for fetching the address.
        Intent intent = new Intent(this, FetchAddressIntentService.class);

        // Pass the result receiver as an extra to the service.
        intent.putExtra(Constants.RECEIVER, mResultReceiver);

        // Pass the location data as an extra to the service.
        intent.putExtra(Constants.LOCATION_DATA_EXTRA, mLocation);

        // Start the service. If the service isn't already running, it is instantiated and started
        // (creating a process for it if needed); if it is running then it remains running. The
        // service kills itself automatically once all intents are processed.
        startService(intent);
    }

    //------------------------------------------------------------------------------------
    //Handle results returned to the FragmentActivity by Google Play services
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        // Decide what to do based on the original request code
        switch (requestCode) {

            case CONNECTION_FAILURE_RESOLUTION_REQUEST:
            /*
             * If the result code is Activity.RESULT_OK, try to connect again
			 */
                switch (resultCode) {
                    case Activity.RESULT_OK:
                        mGoogleApiClient.connect();
                        break;
                }

        }
    }

    //------------------------------------------------------------------------------------
    private boolean isGooglePlayServicesAvailable() {
        // Check that Google Play services is available
        int resultCode = GooglePlayServicesUtil.isGooglePlayServicesAvailable(this);
        // If Google Play services is available
        if (ConnectionResult.SUCCESS == resultCode) {
            // In debug mode, log the status
            Log.d("Location Updates", "Google Play services is available.");
            return true;
        } else {

            Log.d("Location Updates", "Google Play services is not available.");
            // Get the error dialog from Google Play services
            Dialog errorDialog = GooglePlayServicesUtil.getErrorDialog(resultCode, this,
                    CONNECTION_FAILURE_RESOLUTION_REQUEST);

            // If Google Play services can provide an error dialog
            if (errorDialog != null) {
                // Create a new DialogFragment for the error dialog
                ErrorDialogFragment errorFragment = new ErrorDialogFragment();
                errorFragment.setDialog(errorDialog);
                errorFragment.show(getSupportFragmentManager(), "Location Updates");
            }

            return false;
        }
    }

    //------------------------------------------------------------------------------------
    @SuppressLint("LongLogTag")
    @Override
    public void onConnected(Bundle dataBundle) {
        Log.e("Im inside onConnected", "" + "OlaMainActivity_Latest");
        Log.e("mLockatedPreferences.getIsCabBooked() in onConnected", String.valueOf(mLockatedPreferences.getIsCabBooked()));
        Log.e("mLockatedPreferences.getIsAutoBooked() in onConnected", String.valueOf(mLockatedPreferences.getIsAutoBooked()));

        if (mLockatedPreferences.getIsCabBooked() || mLockatedPreferences.getIsAutoBooked()) {
            Log.e("The Cab Is Already Booked", "Yes");

        } else {
            Log.e("The Cab Is Already Booked", "No");

            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                return;
            }
            Location location = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
            Log.e("mLockatedPreferences.getSrcLat", "FusedLocationApi Lat" + mLockatedPreferences.getSrcLat());
            Log.e("mLockatedPreferences.getSrcLng", "FusedLocationApi Lng" + mLockatedPreferences.getSrcLng());
            if (location != null) {
                Log.e("location", "" + location);
                //Toast.makeText(this, "GPS location was found!", Toast.LENGTH_SHORT).show();
                handleNewLocation(location);

            } else {
                //Toast.makeText(this, "Current location was null, enable GPS on emulator!", Toast.LENGTH_SHORT).show();
                Log.e("location", "" + location);
            }
            startLocationUpdates();
        }
    }

    public void startLocationUpdates() {

        Log.e("Locaton Update Started", "Yes");
        mLocationRequest = new LocationRequest();
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        mLocationRequest.setInterval(UPDATE_INTERVAL);
        mLocationRequest.setFastestInterval(FASTEST_INTERVAL);
        //checkPermission();
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this);
    }

    //------------------------------------------------------------------------------------
    public void onLocationChanged(Location location) {
        String msg = "Updated Location: " + Double.toString(location.getLatitude()) + "," + Double.toString(location.getLongitude());
        //Toast.makeText(this, msg, Toast.LENGTH_SHORT).show();
        handleNewLocation(location);
    }

    //------------------------------------------------------------------------------------
    @Override
    public void onConnectionSuspended(int i) {
        if (i == CAUSE_SERVICE_DISCONNECTED) {
            Toast.makeText(this, "Disconnected. Please Wait re-connecting.", Toast.LENGTH_SHORT).show();
        } else if (i == CAUSE_NETWORK_LOST) {
            Toast.makeText(this, "Network lost. Please Wait re-connecting.", Toast.LENGTH_SHORT).show();
        }
        mGoogleApiClient.connect();
    }

    //------------------------------------------------------------------------------------
    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
        if (connectionResult.hasResolution()) {
            try {
                connectionResult.startResolutionForResult(this,
                        CONNECTION_FAILURE_RESOLUTION_REQUEST);
            } catch (IntentSender.SendIntentException e) {
                e.printStackTrace();
                mGoogleApiClient.connect();
            }
        } else {
            Toast.makeText(getApplicationContext(),
                    "Sorry. Location services not available to you", Toast.LENGTH_LONG).show();
        }
    }

    //---------------------------AndroidSlidingPanelUp--------------------------------------------
    @Override
    public void onPanelSlide(View panel, float slideOffset) {

    }

    @Override
    public void onPanelStateChanged(View panel, SlidingUpPanelLayout.PanelState previousState, SlidingUpPanelLayout.PanelState newState) {
        //mLayout.setPanelState(newState);
    }

    //------------------------------------------------------------------------------------
    // Define a DialogFragment that displays the error dialog
    public static class ErrorDialogFragment extends DialogFragment {

        // Global field to contain the error dialog
        private Dialog mDialog;

        // Default constructor. Sets the dialog field to null
        public ErrorDialogFragment() {
            super();
            mDialog = null;
        }

        // Set the dialog to display
        public void setDialog(Dialog dialog) {
            mDialog = dialog;
        }

        // Return a Dialog to the DialogFragment.
        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            return mDialog;
        }
    }

    public static void setDriverLocation(float user_lat, float user_lng, float driver_lat, float driver_lng, boolean driver_available) {

        LatLng user_latLng = new LatLng(user_lat, user_lng);
        LatLng driver_latLng = new LatLng(driver_lat, driver_lng);

        //------User Location Address on Edt_SourceLocation---------------
        Location location = new Location("");
        location.setLatitude(user_lat);
        location.setLongitude(user_lng);
        //----- User Marker------------------------------------------------
        user_marker_option = new MarkerOptions().title("User Location").position(user_latLng).icon(BitmapDescriptorFactory.fromResource(R.drawable.small_marker)).draggable(false);
        gmap.moveCamera(CameraUpdateFactory.newLatLngZoom(user_latLng, 15));
        user_marker = gmap.addMarker(user_marker_option);

        Log.e("User Latalng = ", "" + user_lat + ":" + user_lng);
        Log.e("User_marker", "" + "is added");

        //----- Driver Marker------------------------------------------------

        if (driver_available) {
            gmap.moveCamera(CameraUpdateFactory.newLatLngZoom(driver_latLng, 15));
            driver_marker_option = new MarkerOptions().title("Cab Location").position(driver_latLng).icon(BitmapDescriptorFactory.fromResource(R.drawable.new_cab_pin)).draggable(false);

            driver_marker = gmap.addMarker(driver_marker_option);

            Log.e("Driver Latalng = ", "" + driver_lat + ":" + driver_lng);
            Log.e("Driver_marker", "" + "is added");
        }
    }

    //------------------------------------------------------------------------------------
    private void onLocationChangeListener(double latitude, double longitude) {

        if (ConnectionDetector.isConnectedToInternet(OlaMainActivity_Latest.this)) {
            Log.e("is cab booked", String.valueOf(mLockatedPreferences.getIsCabBooked()));
            CheapestOlaRideFragment.progressBar.setVisibility(View.VISIBLE);
            CheapestOlaRideFragment.cheap_view.setVisibility(View.GONE);
            String url = "https://devapi.olacabs.com/v1/products?pickup_lat=" + latitude + "&pickup_lng=" + longitude;
            Log.e("onLocationPointListener", "" + url);
            LockatedVolleyRequestQueue mLockatedVolleyRequestQueue = LockatedVolleyRequestQueue.getInstance(this);
            mLockatedVolleyRequestQueue.sendGetRequestOlaCabs(REQUEST_TAG, Request.Method.GET, url, this, this);
        } else {
            Utilities.showToastMessage(this, this.getResources().getString(R.string.internet_connection_error));
        }
    }

    @SuppressLint("LongLogTag")
    private void onLocationChangeListenerNew(double src_latitude, double src_longitude, double dst_latitude, double dst_longitude) {

        if (ConnectionDetector.isConnectedToInternet(OlaMainActivity_Latest.this)) {
            Log.e("is cab booked", String.valueOf(mLockatedPreferences.getIsCabBooked()));
            CheapestOlaRideFragment.progressBar.setVisibility(View.VISIBLE);
            CheapestOlaRideFragment.cheap_view.setVisibility(View.GONE);
            String url = "https://devapi.olacabs.com/v1/products?pickup_lat=" + src_latitude + "&pickup_lng=" + src_longitude + "&drop_lat=" + dst_latitude + "&drop_lng=" + dst_longitude;
            Log.e("onLocationChangeListenerNew", "" + url);
            LockatedVolleyRequestQueue mLockatedVolleyRequestQueue = LockatedVolleyRequestQueue.getInstance(this);
            mLockatedVolleyRequestQueue.sendGetRequestOlaCabs(REQUEST_TAG, Request.Method.GET, url, this, this);
        } else {
            Utilities.showToastMessage(this, this.getResources().getString(R.string.internet_connection_error));
        }
    }

    //------------------------------------------------------------------------------------
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.ola_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_bookings: {
                Intent intent = new Intent(this, OlaListActivity.class);
                startActivity(intent);
                return true;
            }
            case android.R.id.home: {
                onBackPressed();
                return true;
            }


            /* case R.id.action_coupon: {
                final AlertDialog.Builder dialog = new AlertDialog.Builder(this);
                View view = getLayoutInflater().inflate(R.layout.coupon_layout, null);
                dialog.setView(view);
                final AlertDialog alertDialog = dialog.show();

                //ArrayList<String> coupon_code= getActivity().getResources().getStringArray(R.array.coupon);
                ArrayList<String> coupon_code = new ArrayList<>();
                ArrayList<String> coupon_sub_head_1 = new ArrayList<>();
                ArrayList<String> coupon_sub_head_2 = new ArrayList<>();
                ArrayList<String> coupon_head = new ArrayList<>();


                coupon_code.add("AUTO10");
                coupon_code.add("FIRST25");

                coupon_head.add("Get 10% Cashback on any 5 auto rides booked via Lockated.com");
                coupon_head.add("25% CB on 1st Ride.Apply to get 75% CB on 2nd Ride");

                coupon_sub_head_1.add("Minimum Transaction Rs. 20.0");
                coupon_sub_head_1.add("Minimum Transaction Rs. 80.0");

                coupon_sub_head_2.add("Maximum Cashback of Rs. 25.0");
                coupon_sub_head_2.add("Maximum Cashback of Rs. 40.0");
                couponAdapter = new CouponAdapter(this, coupon_code, coupon_head, coupon_sub_head_1, coupon_sub_head_2, null);

                RecyclerView recyclerView = (RecyclerView) view.findViewById(R.id.mRecyclerView);
                recyclerView.addItemDecoration(new DividerItemDecoration(this, LinearLayoutManager.VERTICAL));
                RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(this);
                recyclerView.setLayoutManager(mLayoutManager);
                recyclerView.setHasFixedSize(true);

                recyclerView.setAdapter(couponAdapter);
            }

        }*/
        }
        return super.onOptionsItemSelected(item);
    }

    //------------------------------------------------------------------------------------
    @Override
    public void onErrorResponse(VolleyError error) {
        CheapestOlaRideFragment.progressBar.setVisibility(View.GONE);
        LockatedRequestError.onRequestError(this, error);
    }

    @SuppressLint("LongLogTag")
    @Override
    public void onResponse(String response) {
        Log.e("RESPONSE", "" + response);
        if (response != null && response.length() > 0) {

            CheapestOlaRideFragment.progressBar.setVisibility(View.GONE);
            JSONObject jsonObject = null;
            Gson gson = new Gson();
            JSONArray categoryArray = null;
            JSONArray rideEstimateArray = null;

            Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.mFrameContainer);
            if (fragment instanceof ConfirmOlaBookingFragment) {
                try {
                    int minFare = 0;
                    int maxFare = 0;
                    jsonObject = new JSONObject(response);
                    olaCabCategoryDetails.clear();
                    olaCabCategorys.clear();
                    olaCabRideEstimates.clear();

                    if (jsonObject.has("categories")) {
                        categoryArray = jsonObject.getJSONArray("categories");
                        Category molaCabCategory = gson.fromJson(categoryArray.get(0).toString(), Category.class);
                        olaCabCategorys.add(molaCabCategory);

                        minFare = fareCalculater(olaCabCategorys.get(0).getFareBreakup().get(0).getBaseFare(), olaCabCategorys.get(0).getFareBreakup().get(0).getCostPerDistance(), olaCabCategorys.get(0).getFareBreakup().get(0).getRideCostPerMinute());
                        maxFare = minFare + 15;

                        ConfirmOlaBookingFragment.progressBar.setVisibility(View.GONE);
                        ConfirmOlaBookingFragment.txt_ola_cab_cost.setVisibility(View.VISIBLE);
                        ConfirmOlaBookingFragment.txt_ola_cab_cost.setText(getString(R.string.rupees_symbol) + "" + minFare + " - " + maxFare);
                        ConfirmOlaBookingFragment.txt_ola_cab_cost.setTextColor(Color.parseColor("#000000"));
                        ConfirmOlaBookingFragment.txt_fare_cal.setText("For 5 Kms");
                    }

                    if (jsonObject.has("ride_estimate") && jsonObject.getJSONArray("ride_estimate").length() > 0) {
                        rideEstimateArray = jsonObject.getJSONArray("ride_estimate");
                        for (int i = 0; i < rideEstimateArray.length(); i++)
                        {
                            if (rideEstimateArray.getJSONObject(i).getString("category").equals(mLockatedPreferences.getOlaCategory())) {
                                RideEstimate mcabCategoryRideEstimate = gson.fromJson(rideEstimateArray.getJSONObject(i).toString(), RideEstimate.class);
                                olaCabRideEstimates.add(mcabCategoryRideEstimate);
                            }
                        }
                        //RideEstimate mcabCategoryRideEstimate = gson.fromJson(rideEstimateArray.getJSONObject(0).toString(), RideEstimate.class);
                        //olaCabRideEstimates.add(mcabCategoryRideEstimate);
                        Log.e("Destination Selected", " " + "Im Here");
                        ConfirmOlaBookingFragment.progressBar.setVisibility(View.GONE);
                        ConfirmOlaBookingFragment.txt_ola_cab_cost.setVisibility(View.VISIBLE);
                        ConfirmOlaBookingFragment.txt_ola_cab_cost.setText(getString(R.string.rupees_symbol) + "" + olaCabRideEstimates.get(0).getAmountMin() + " - " + olaCabRideEstimates.get(0).getAmountMax());
                        ConfirmOlaBookingFragment.txt_ola_cab_cost.setTextColor(Color.parseColor("#5CB3FF"));
                        ConfirmOlaBookingFragment.txt_fare_cal.setText("For destination");
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }

            } else {
                try {
                    jsonObject = new JSONObject(response);
                    olaCabCategoryDetails.clear();
                    olaCabCategorys.clear();
                    olaCabRideEstimates.clear();

                    if (jsonObject.has("message") && jsonObject.has("code")) {
                        if (jsonObject.getString("code").equals("DIFFERENT_DROP_CITY")) {
                            CheapestOlaRideFragment.mTextViewNoCFound.setText("" + jsonObject.getString("message"));
                        } else if (jsonObject.getString("code").equals("INVALID_CITY")) {
                            CheapestOlaRideFragment.mTextViewNoCFound.setText(getString(R.string.no_cab));
                        }
                        CheapestOlaRideFragment.cheap_view.setVisibility(View.GONE);
                        CheapestOlaRideFragment.mTextViewNoCFound.setVisibility(View.VISIBLE);
                    } else if (jsonObject.has("categories") && jsonObject.has("ride_estimate")) {
                        CheapestOlaRideFragment.cheap_view.setVisibility(View.VISIBLE);
                        CheapestOlaRideFragment.mTextViewNoCFound.setVisibility(View.GONE);
                        CheapestOlaRideFragment.cheap_fareText.setText(getResources().getString(R.string.text_fare_kms));
                        OlaCabCategoryDetail olaCabCategoryDetail = new OlaCabCategoryDetail();

                        if (jsonObject.getJSONArray("categories").length() > 0) {
                            categoryArray = jsonObject.getJSONArray("categories");
                            Log.e("categoryArray.length ", "" + categoryArray.length());
                            for (int i = 0; i < categoryArray.length(); i++) {
                                if (categoryArray.getJSONObject(i).getInt("eta") != -1 && !(categoryArray.getJSONObject(i).getString("id").equals("auto") || categoryArray.getJSONObject(i).getString("id").equals("rental") || categoryArray.getJSONObject(i).getString("id").equals("share"))) {
                                    Category molaCabCategory = gson.fromJson(categoryArray.getJSONObject(i).toString(), Category.class);
                                    olaCabCategorys.add(molaCabCategory);
                                    Log.e("olaCabCategoryDetail ", "" + olaCabCategoryDetail.getCategories());
                                    try {
                                        if (jsonObject.getJSONArray("ride_estimate").length() > 0) {
                                            CheapestOlaRideFragment.cheap_fareText.setText(getResources().getString(R.string.text_fare_dest));
                                            rideEstimateArray = jsonObject.getJSONArray("ride_estimate");
                                            Log.e("rideEstimateArray.length ", "" + rideEstimateArray.length());
                                            for (int j = 0; j < rideEstimateArray.length(); j++) {
                                                if (categoryArray.getJSONObject(i).getInt("eta") != -1 && categoryArray.getJSONObject(i).get("id").equals(rideEstimateArray.getJSONObject(j).get("category")) && !(rideEstimateArray.getJSONObject(i).getString("category").equals("auto") || rideEstimateArray.getJSONObject(i).getString("category").equals("rental") || rideEstimateArray.getJSONObject(i).getString("category").equals("share"))) {
                                                    RideEstimate mcabCategoryRideEstimate = gson.fromJson(rideEstimateArray.getJSONObject(j).toString(), RideEstimate.class);
                                                    olaCabRideEstimates.add(mcabCategoryRideEstimate);
                                                }
                                            }
                                        }
                                    } catch (JSONException e) {
                                        Log.e("JSONException", "" + e);
                                    }

                                    olaCabCategoryDetail.setCategories(olaCabCategorys);
                                    olaCabCategoryDetail.setRideEstimate(olaCabRideEstimates);
                                    olaCabCategoryDetails.add(olaCabCategoryDetail);

                                }
                            }
                        }
                        olaCabsAdapter = new OlaCabsAdapter(this, olaCabCategoryDetails, getSupportFragmentManager());
                        olaCabsAdapter.notifyDataSetChanged();
                        if (olaCabCategoryDetails.size() > 0) {
                            CheapestOlaRideFragment.mRecyclerView.setAdapter(olaCabsAdapter);
                            CheapestOlaRideFragment.mTextViewNoCFound.setVisibility(View.GONE);
                        } else {
                            CheapestOlaRideFragment.cheap_view.setVisibility(View.GONE);
                            CheapestOlaRideFragment.mTextViewNoCFound.setText(getString(R.string.no_cab));
                            CheapestOlaRideFragment.mTextViewNoCFound.setVisibility(View.VISIBLE);
                        }
                    }

                    /*olaCabsAdapter = new OlaCabsAdapter(this, olaCabCategoryDetails, getSupportFragmentManager());
                    olaCabsAdapter.notifyDataSetChanged();
                    if (olaCabCategoryDetails.size() > 0) {
                        CheapestOlaRideFragment.mRecyclerView.setAdapter(olaCabsAdapter);
                        CheapestOlaRideFragment.mTextViewNoCFound.setVisibility(View.GONE);
                    } else {
                        CheapestOlaRideFragment.cheap_view.setVisibility(View.GONE);
                        //CheapestOlaRideFragment.mTextViewNoCFound.setText(getString(R.string.no_cab));
                        CheapestOlaRideFragment.mTextViewNoCFound.setVisibility(View.VISIBLE);
                    }*/
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    //------------------------------------------------------------------------------------
    public static void SourceLocation(LatLng latLng, int autocompletefield) {
        if (autocompletefield == 1) {
            srcLat = latLng.latitude;
            srcLng = latLng.longitude;

            Log.e("srcLat", "" + srcLat);
            Log.e("srcLng", "" + srcLng);
            CameraPosition cameraPosition = new CameraPosition.Builder().target(latLng).zoom(13f).build();
            gmap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));

            setSourcelatLng(latLng);

        } else if (autocompletefield == 2) {
            destLat = latLng.latitude;
            destLng = latLng.longitude;

            Log.e("destLat", "" + destLat);
            Log.e("destLng", "" + destLng);

            setDstlatLng(latLng);
        }
    }

    private static void setSourcelatLng(LatLng latLng) {
        if (destLat != 0 && destLng != 0) {
            onChangeLocation(mContext, latLng, destLat, destLng);
        } else {
            onChangeLocation(mContext, latLng, destLat, destLng);
        }
    }

    private static void setDstlatLng(LatLng latLng) {
        onChangeLocationNew(mContext, latLng);
    }

    //------------------------------------------------------------------------------------
    private void getCurrentLocation() {
        checkLocationPermission();
        Location location = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
        if (location != null) {
            LatLng latLng = new LatLng(location.getLatitude(), location.getLongitude());
            mLockatedPreferences.setSrcLat((float) srcLat);
            mLockatedPreferences.setSrcLng((float) srcLng);
            CameraPosition cameraPosition = new CameraPosition.Builder().target(latLng).zoom(13f).build();
            gmap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));

            if (mLockatedPreferences.getDstLat() != 0 && mLockatedPreferences.getDstLng() != 0) {
                onLocationChangeListenerNew(location.getLatitude(), location.getLongitude(), mLockatedPreferences.getDstLat(), mLockatedPreferences.getDstLng());
            } else {
                onLocationChangeListener(location.getLatitude(), location.getLongitude());
            }
        }

    }

    //------------------------------------------------------------------------------------
    public void hideSoftKeyboard() {
        if (getCurrentFocus() != null) {
            InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
            inputMethodManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
        }
    }

    //------------------------------------------------------------------------------------
    public static void removeUserCurrentLocation() {
        if (user_marker != null) {
            user_marker.remove();
        }
        if (driver_marker != null) {
            driver_marker.remove();
        }
    }

    //------------------------------------------------------------------------------------
    private void handleNewLocation(Location location) {
        Log.e("New Location", location.toString());
        LatLng latLng = new LatLng(location.getLatitude(), location.getLongitude());
        mLockatedPreferences.setSrcLat((float) srcLat);
        mLockatedPreferences.setSrcLng((float) srcLng);
        CameraPosition cameraPosition = new CameraPosition.Builder().target(latLng).zoom(13f).build();
        gmap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
        startIntentService(location);
        if (mLockatedPreferences.getDstLat() != 0 && mLockatedPreferences.getDstLng() != 0) {
            onLocationChangeListenerNew(location.getLatitude(), location.getLongitude(), mLockatedPreferences.getDstLat(), mLockatedPreferences.getDstLng());
        } else {
            onLocationChangeListener(location.getLatitude(), location.getLongitude());
        }
    }
    //------------------------------------------------------------------------------------

    @Override
    public void onBackPressed() {
        Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.mFrameContainer);
        if (fragment instanceof ConfirmOlaBookingFragment) {
            if (mLayout != null && (mLayout.getPanelState() == SlidingUpPanelLayout.PanelState.EXPANDED || mLayout.getPanelState() == SlidingUpPanelLayout.PanelState.ANCHORED)) {
                mLayout.setPanelState(SlidingUpPanelLayout.PanelState.COLLAPSED);
            }
            getSupportFragmentManager().popBackStack();
            mLockatedPreferences.setDstLat(0);
            mLockatedPreferences.setDstLng(0);
            mLockatedPreferences.setOlaCategory(null);
            edt_olaDestinationLocation.getText().clear();
        } else {
            mLockatedPreferences.setDstLat(0);
            mLockatedPreferences.setDstLng(0);
            edt_olaDestinationLocation.getText().clear();
            finish();
        }
    }

    public void setPanelHeight() {
        DisplayMetrics dm = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(dm);
        window_height = dm.heightPixels;
        mLayout.setPanelHeight((int) (window_height / 2.5));
        panel_height = (int) (window_height / 2.5);
    }

    public static void setNewDriverLocation(float driver_lat, float driver_lng) {

        LatLng driver_latLng = new LatLng(driver_lat, driver_lng);
        gmap.moveCamera(CameraUpdateFactory.newLatLngZoom(driver_latLng, 17));

        driver_marker_option = new MarkerOptions().title("Cab Location").position(driver_latLng).icon(BitmapDescriptorFactory.fromResource(R.drawable.new_cab_pin)).draggable(false);
        driver_marker = gmap.addMarker(driver_marker_option);

        Log.e("Driver Latalng = ", "" + driver_lat + ":" + driver_lng);
        Log.e("Driver_marker", "" + "is added");
    }

    public void alertGPSConnectionError() {
        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage(getString(R.string.gps_disabled))
                .setCancelable(false)
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    public void onClick(final DialogInterface dialog, final int id) {
                        ((LockatedApplication) getApplicationContext()).setIsGPSEnabled(true);
                        dialog.cancel();
                        startActivity(new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS));
                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    public void onClick(final DialogInterface dialog, final int id) {
                        dialog.cancel();
                        finish();
                    }
                });
        final AlertDialog alert = builder.create();
        alert.show();
    }

    private int getStatusBarHeight() {
        int result = 0;
        int resourceId = getResources().getIdentifier("status_bar_height", "dimen", "android");
        if (resourceId > 0) {
            result = getResources().getDimensionPixelSize(resourceId);
        }
        return result;
    }

    private static void onChangeLocation(Context context, LatLng latLng, double destLat, double destLng) {

        if (ConnectionDetector.isConnectedToInternet(context)) {

            CheapestOlaRideFragment.progressBar.setVisibility(View.VISIBLE);
            CheapestOlaRideFragment.cheap_view.setVisibility(View.GONE);
            String url = null;
            if (destLat != 0 && destLng != 0) {
                url = "https://devapi.olacabs.com/v1/products?pickup_lat=" + latLng.latitude + "&pickup_lng=" + latLng.longitude + "&drop_lat=" + destLat + "&drop_lng=" + destLng;
            } else {
                url = "https://devapi.olacabs.com/v1/products?pickup_lat=" + latLng.latitude + "&pickup_lng=" + latLng.longitude;
            }
            Log.e("onChangeLocation", "" + url);
            LockatedVolleyRequestQueue mLockatedVolleyRequestQueue = LockatedVolleyRequestQueue.getInstance(context);
            mLockatedVolleyRequestQueue.sendGetRequestOlaCabs(REQUEST_TAG, Request.Method.GET, url, listener, errorListener);
        } else {
            Utilities.showToastMessage(context, context.getResources().getString(R.string.internet_connection_error));
        }
    }

    private static void onChangeLocationNew(Context context, LatLng latLng) {

        if (ConnectionDetector.isConnectedToInternet(context)) {

            CheapestOlaRideFragment.progressBar.setVisibility(View.VISIBLE);
            CheapestOlaRideFragment.cheap_view.setVisibility(View.GONE);
            String url = "https://devapi.olacabs.com/v1/products?pickup_lat=" + srcLat + "&pickup_lng=" + srcLng + "&drop_lat=" + latLng.latitude + "&drop_lng=" + latLng.longitude;

            Log.e("onChangeLocation", "" + url);
            LockatedVolleyRequestQueue mLockatedVolleyRequestQueue = LockatedVolleyRequestQueue.getInstance(context);
            mLockatedVolleyRequestQueue.sendGetRequestOlaCabs(REQUEST_TAG, Request.Method.GET, url, listener, errorListener);
        } else {
            Utilities.showToastMessage(context, context.getResources().getString(R.string.internet_connection_error));
        }
    }

    private void findEstmateForKms() {

        if (ConnectionDetector.isConnectedToInternet(OlaMainActivity_Latest.this)) {

            ConfirmOlaBookingFragment.progressBar.setVisibility(View.VISIBLE);
            ConfirmOlaBookingFragment.txt_ola_cab_cost.setVisibility(View.GONE);
            String url = "https://devapi.olacabs.com/v1/products?pickup_lat=" + mLockatedPreferences.getSrcLat() + "&pickup_lng=" + mLockatedPreferences.getSrcLng() + "&category=" + mLockatedPreferences.getOlaCategory();
            Log.e("url", "" + url);
            LockatedVolleyRequestQueue mLockatedVolleyRequestQueue = LockatedVolleyRequestQueue.getInstance(this);
            mLockatedVolleyRequestQueue.sendGetRequestOlaCabs(REQUEST_TAG, Request.Method.GET, url, this, this);
        } else {
            Utilities.showToastMessage(this, this.getResources().getString(R.string.internet_connection_error));
        }
    }

    public int fareCalculater(String basefare, String costPerDistance, String costPerMinute) {

        Log.e("basefare", "" + basefare);
        Log.e("costPerDistance", "" + costPerDistance);
        Log.e("costPerMinute", "" + costPerMinute);

        // it is the fareCalculation for 5 KMs . Base Fare is always for 4 KMs. and Adding Cost for 1 KMs .
        int minTime = 15;
        int basefare_new;
        int costPerDistance_new;
        int costPerMinute_new;

        try {
            basefare_new = Integer.parseInt(basefare);
        } catch (NumberFormatException e) {
            double basefare_new_new = Double.parseDouble(basefare);
            basefare_new = (int) basefare_new_new;
        }

        try {
            costPerDistance_new = Integer.parseInt(costPerDistance);
        } catch (NumberFormatException e) {
            double costPerDistance_new_new = Double.parseDouble(costPerDistance);
            costPerDistance_new = (int) costPerDistance_new_new;
        }

        try {
            costPerMinute_new = Integer.parseInt(costPerMinute);
        } catch (NumberFormatException e) {
            double costPerMinute_new_new = Double.parseDouble(costPerMinute);
            costPerMinute_new = (int) costPerMinute_new_new;
        }


        return basefare_new + (costPerDistance_new * costPerMinute_new) + (minTime * costPerMinute_new);
    }
}
