
package com.android.lockated.model.AdminModel.AdminDirectoty.MembersDirectory;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class UserFlat {

    @SerializedName("id")
    @Expose
    private int id;
    @SerializedName("flat")
    @Expose
    private String flat;
    @SerializedName("block")
    @Expose
    private String block;

    /**
     * 
     * @return
     *     The id
     */
    public int getId() {
        return id;
    }

    /**
     * 
     * @param id
     *     The id
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * 
     * @return
     *     The flat
     */
    public String getFlat() {
        return flat;
    }

    /**
     * 
     * @param flat
     *     The flat
     */
    public void setFlat(String flat) {
        this.flat = flat;
    }

    /**
     * 
     * @return
     *     The block
     */
    public String getBlock() {
        return block;
    }

    /**
     * 
     * @param block
     *     The block
     */
    public void setBlock(String block) {
        this.block = block;
    }

}
