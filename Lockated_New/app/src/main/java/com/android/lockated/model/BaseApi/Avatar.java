
package com.android.lockated.model.BaseApi;

import org.json.JSONException;
import org.json.JSONObject;

public class Avatar {
    public String url;
    public String medium;
    public String thumb;


    public Avatar(JSONObject jsonObjectSociety1) {
        try {
            url = jsonObjectSociety1.getString("url");
            medium = jsonObjectSociety1.getString("medium");
            thumb = jsonObjectSociety1.getString("thumb");

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }


    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getMedium() {
        return medium;
    }

    public void setMedium(String medium) {
        this.medium = medium;
    }

}