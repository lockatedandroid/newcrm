package com.android.lockated.location;

import android.os.Bundle;
import android.os.Handler;
import android.os.Message;

import com.android.lockated.Interfaces.IGeocoderAddressUpdateListener;

public class GeocoderHandler extends Handler
{
    private IGeocoderAddressUpdateListener mIGeocoderAddressUpdateListener;

    public GeocoderHandler(IGeocoderAddressUpdateListener iGeocoderAddressUpdateListener)
    {
        mIGeocoderAddressUpdateListener = iGeocoderAddressUpdateListener;
    }

    @Override
    public void handleMessage(Message message)
    {
        String locationAddress;
        switch (message.what)
        {
            case 1:
                Bundle bundle = message.getData();
                locationAddress = bundle.getString("address");
                mIGeocoderAddressUpdateListener.onAddressUpdate(locationAddress);
                break;
            default:
                locationAddress = null;
                mIGeocoderAddressUpdateListener.onAddressUpdate(locationAddress);
                break;
        }
    }
}
