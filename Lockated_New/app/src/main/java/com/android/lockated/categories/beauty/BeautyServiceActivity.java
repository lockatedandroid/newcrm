package com.android.lockated.categories.beauty;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;

import com.android.lockated.LockatedApplication;
import com.android.lockated.R;
import com.android.lockated.categories.beauty.fragment.BeautyServiceDetailFragment;
import com.android.lockated.categories.beauty.fragment.BeautyServiceFragment;
import com.android.lockated.checkout.ServiceCongratulationFragment;

public class BeautyServiceActivity extends AppCompatActivity
{
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_vendor);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(true);
        getSupportActionBar().setTitle(((LockatedApplication) getApplicationContext()).getHeaderName());

        ((LockatedApplication) getApplicationContext()).setMyAddressData(null);
        ((LockatedApplication) getApplicationContext()).setmDeliveryTime("");
        ((LockatedApplication) getApplicationContext()).setmDeliveryDate("");

        if (savedInstanceState == null)
        {
            BeautyServiceFragment beautyServiceFragment = new BeautyServiceFragment();
            getSupportFragmentManager().beginTransaction().add(R.id.mVendorContainer, beautyServiceFragment).commit();
        }
    }

    public void addNewFragmentToStack()
    {
        BeautyServiceDetailFragment beautyServiceDetailFragment = new BeautyServiceDetailFragment();
        getSupportFragmentManager().beginTransaction().replace(R.id.mVendorContainer, beautyServiceDetailFragment).addToBackStack("BeautyServiceDetailFragment").commit();
    }

    public void addCongratulationScreen(String mOrderNumber)
    {
        ServiceCongratulationFragment congratulationFragment = new ServiceCongratulationFragment();
        congratulationFragment.setOrderId(mOrderNumber);
        getSupportFragmentManager().beginTransaction().replace(R.id.mVendorContainer, congratulationFragment).commit();
    }

    @Override
    public void onBackPressed()
    {
        Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.mVendorContainer);
        if (fragment instanceof ServiceCongratulationFragment)
        {
            ((ServiceCongratulationFragment) fragment).getScreenDetail();
        }
        else
        {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        switch (item.getItemId())
        {
            case android.R.id.home:
            {
                super.onBackPressed();
                return true;
            }
        }
        return super.onOptionsItemSelected(item);
    }
}