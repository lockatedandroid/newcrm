
package com.android.lockated.model.AdminModel.AdminGatekeeper;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class GatekeeperAdminModel {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("id_user")
    @Expose
    private Integer idUser;
    @SerializedName("id_society")
    @Expose
    private Integer idSociety;
    @SerializedName("guest_name")
    @Expose
    private String guestName;
    @SerializedName("guest_entry_time")
    @Expose
    private String guestEntryTime;
    @SerializedName("guest_exit_time")
    @Expose
    private String guestExitTime;
    @SerializedName("IsDelete")
    @Expose
    private Object IsDelete;
    @SerializedName("guest_number")
    @Expose
    private String guestNumber;
    @SerializedName("guest_vehicle_number")
    @Expose
    private String guestVehicleNumber;
    @SerializedName("visit_purpose")
    @Expose
    private String visitPurpose;
    @SerializedName("visit_to")
    @Expose
    private String visitTo;
    @SerializedName("plus_person")
    @Expose
    private Integer plusPerson;
    @SerializedName("expected_at")
    @Expose
    private String expectedAt;
    @SerializedName("approve")
    @Expose
    private int approve;
    @SerializedName("accompany")
    @Expose
    private String accompany;
    @SerializedName("url")
    @Expose
    private String url;
    @SerializedName("flat")
    @Expose
    private String flat;
    @SerializedName("documents")
    @Expose
    private ArrayList<GateKeeperDocument> documents = new ArrayList<GateKeeperDocument>();

    /**
     * @return The id
     */
    public Integer getId() {
        return id;
    }

    /**
     * @param id The id
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * @return The idUser
     */
    public Integer getIdUser() {
        return idUser;
    }

    /**
     * @param idUser The id_user
     */
    public void setIdUser(Integer idUser) {
        this.idUser = idUser;
    }

    /**
     * @return The idSociety
     */
    public Integer getIdSociety() {
        return idSociety;
    }

    /**
     * @param idSociety The id_society
     */
    public void setIdSociety(Integer idSociety) {
        this.idSociety = idSociety;
    }

    /**
     * @return The guestName
     */
    public String getGuestName() {
        return guestName;
    }

    /**
     * @param guestName The guest_name
     */
    public void setGuestName(String guestName) {
        this.guestName = guestName;
    }

    /**
     * @return The guestEntryTime
     */
    public String getGuestEntryTime() {
        return guestEntryTime;
    }

    /**
     * @param guestEntryTime The guest_entry_time
     */
    public void setGuestEntryTime(String guestEntryTime) {
        this.guestEntryTime = guestEntryTime;
    }

    /**
     * @return The guestExitTime
     */
    public String getGuestExitTime() {
        return guestExitTime;
    }

    /**
     * @param guestExitTime The guest_exit_time
     */
    public void setGuestExitTime(String guestExitTime) {
        this.guestExitTime = guestExitTime;
    }

    /**
     * @return The IsDelete
     */
    public Object getIsDelete() {
        return IsDelete;
    }

    /**
     * @param IsDelete The IsDelete
     */
    public void setIsDelete(Object IsDelete) {
        this.IsDelete = IsDelete;
    }

    /**
     * @return The guestNumber
     */
    public String getGuestNumber() {
        return guestNumber;
    }

    /**
     * @param guestNumber The guest_number
     */
    public void setGuestNumber(String guestNumber) {
        this.guestNumber = guestNumber;
    }

    /**
     * @return The guestVehicleNumber
     */
    public String getGuestVehicleNumber() {
        return guestVehicleNumber;
    }

    /**
     * @param guestVehicleNumber The guest_vehicle_number
     */
    public void setGuestVehicleNumber(String guestVehicleNumber) {
        this.guestVehicleNumber = guestVehicleNumber;
    }

    /**
     * @return The visitPurpose
     */
    public String getVisitPurpose() {
        return visitPurpose;
    }

    /**
     * @param visitPurpose The visit_purpose
     */
    public void setVisitPurpose(String visitPurpose) {
        this.visitPurpose = visitPurpose;
    }

    /**
     * @return The visitTo
     */
    public String getVisitTo() {
        return visitTo;
    }

    /**
     * @param visitTo The visit_to
     */
    public void setVisitTo(String visitTo) {
        this.visitTo = visitTo;
    }

    /**
     * @return The plusPerson
     */
    public Integer getPlusPerson() {
        return plusPerson;
    }

    /**
     * @param plusPerson The plus_person
     */
    public void setPlusPerson(Integer plusPerson) {
        this.plusPerson = plusPerson;
    }

    /**
     * @return The expectedAt
     */
    public String getExpectedAt() {
        return expectedAt;
    }

    /**
     * @param expectedAt The expected_at
     */
    public void setExpectedAt(String expectedAt) {
        this.expectedAt = expectedAt;
    }

    public int getApprove() {
        return approve;
    }

    public void setApprove(int approve) {
        this.approve = approve;
    }

    public String getAccompany() {
        return accompany;
    }

    public void setAccompany(String accompany) {
        this.accompany = accompany;
    }

    public String getUrl() {
        return url;
    }

    /**
     * @param url The url
     */
    public void setUrl(String url) {
        this.url = url;
    }

    /**
     * @return The flat
     */
    public String getFlat() {
        return flat;
    }

    /**
     * @param flat The flat
     */
    public void setFlat(String flat) {
        this.flat = flat;
    }

    public ArrayList<GateKeeperDocument> getDocuments() {
        return documents;
    }

    /**
     * @param documents The documents
     */
    public void setDocuments(ArrayList<GateKeeperDocument> documents) {
        this.documents = documents;
    }
}