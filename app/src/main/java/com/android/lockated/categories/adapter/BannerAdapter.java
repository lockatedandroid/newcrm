package com.android.lockated.categories.adapter;


import android.content.Context;
import android.content.Intent;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.android.lockated.HomeActivity;
import com.android.lockated.R;
import com.android.lockated.model.Banner.Banner;
import com.android.lockated.utils.Utilities;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class BannerAdapter extends PagerAdapter {

    private final String name;
    Context context;
    boolean clickable;
    LayoutInflater mLayoutInflater;
    ArrayList<Banner> bannerArrayList;

    public BannerAdapter(Context context, ArrayList<Banner> bannerArrayList, boolean clickable, String name) {
        this.context = context;
        this.name = name;
        this.clickable = clickable;
        this.bannerArrayList = bannerArrayList;
        mLayoutInflater = LayoutInflater.from(context);
    }

    @Override
    public int getCount() {
        return bannerArrayList.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == ((LinearLayout) object);
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {

        View itemView = mLayoutInflater.inflate(R.layout.banner_item, container, false);
        ImageView imageView = (ImageView) itemView.findViewById(R.id.imageView);
        Picasso.with(context).load(bannerArrayList.get(position).getOriginal())
                .placeholder(R.drawable.loading).fit().into(imageView);
        container.addView(itemView);
        if (!name.equals("crm")) {
            if (clickable) {
                imageView.setTag(position);
                imageView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        int position = (int) v.getTag();
                        Intent intent = new Intent(context, HomeActivity.class);
                        intent.putExtra("offersOtypeId", Utilities.getIdPosition(bannerArrayList.get(position).getBtypeId()));
                        intent.putExtra("otype_id", bannerArrayList.get(position).getBtypeId());
                        intent.putExtra("taxon_id", bannerArrayList.get(position).getTaxonId());
                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        context.startActivity(intent);
                    }
                });
            }
        }
        return itemView;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((LinearLayout) object);
    }

}
