package com.android.lockated.categories.olacabs.activity;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.location.Address;
import android.location.Geocoder;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.method.ScrollingMovementMethod;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AutoCompleteTextView;
import android.widget.ImageView;

import com.android.lockated.R;
import com.android.lockated.categories.olacabs.adapter.OlaCabsAdapter;
import com.android.lockated.categories.olacabs.fragment.CheapestOlaRideFragment;
import com.android.lockated.categories.vendors.adapters.GooglePlacesAutocompleteAdapter;
import com.android.lockated.location.LocationUpdateManager;
import com.android.lockated.model.OlaCab.OlaCabCategoryDetail;
import com.android.lockated.network.ConnectionDetector;
import com.android.lockated.preferences.LockatedPreferences;
import com.android.lockated.request.LockatedVolleyRequestQueue;
import com.android.lockated.utils.Utilities;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.google.android.gms.maps.model.LatLng;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class FindGooglePlaces extends AppCompatActivity implements Response.ErrorListener, Response.Listener<String> {

    private FindGooglePlaces activity_fing_google_places;

    private static final String REQUEST_TAG = "OLA CABS";
    private AutoCompleteTextView mEnterLocationQuery;
    private ImageView mCurrentLocation;
    private LocationUpdateManager mLocationUpdateManager;
    private double mLatitude;
    private double mLongitude;
    int autoComplete_number;

    private OlaCabsAdapter olaCabsAdapter;
    private LockatedPreferences mLockatedPreferences;
    //ArrayList<Category> olaCabCategoryDetails;
    ArrayList<OlaCabCategoryDetail> olaCabCategoryDetails;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fing_google_places);
        Intent intent = getIntent();
        autoComplete_number = intent.getIntExtra("AutoComplete",0);
        init();
    }

    public void init()
    {
        mLockatedPreferences = new LockatedPreferences(this);
        olaCabCategoryDetails = new ArrayList<OlaCabCategoryDetail>();
        //---------------Changes on 17-8-2016---------------------

        //mCurrentLocation = (ImageView)findViewById(R.id.mCurrentLocation);
       /* mCurrentLocation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });*/

        //---------------------------------------------------------

        mEnterLocationQuery = (AutoCompleteTextView)findViewById(R.id.mEnterLocationQuery);
        mEnterLocationQuery.setHorizontallyScrolling(true);
        mEnterLocationQuery.setMovementMethod(new ScrollingMovementMethod());
        mEnterLocationQuery.setAdapter(new GooglePlacesAutocompleteAdapter(this));
        mEnterLocationQuery.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @SuppressLint("LongLogTag")
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                String str = (String) parent.getItemAtPosition(position);
                LatLng latLng = getLocationFromAddress(FindGooglePlaces.this, str);

                //onLocationChangeListener(latLng.latitude,latLng.longitude);

                if (autoComplete_number == 1) {
                    OlaMainActivity_Latest.edt_olaSourceLocation.setText(str);
                    OlaMainActivity_Latest.edt_olaSourceLocation.setTextColor(Color.BLACK);

                    mLockatedPreferences.setSrcLat((float) latLng.latitude);
                    mLockatedPreferences.setSrcLng((float) latLng.longitude);

                    //OlaMainActivity_Latest olaMainActivity_latest = new OlaMainActivity_Latest();
                    //olaMainActivity_latest.findSourceCab((float) latLng.latitude,(float) latLng.longitude);
                } else if(autoComplete_number == 2){
                    //Log.e("mLockatedPreferences.getDstLat","Before set "+mLockatedPreferences.getDstLat());
                    //Log.e("mLockatedPreferences.getDstLng","Before set "+mLockatedPreferences.getDstLng());

                    mLockatedPreferences.setDstLat((float) latLng.latitude);
                    mLockatedPreferences.setDstLng((float) latLng.longitude);

                    //Log.e("mLockatedPreferences.getDstLat","After set "+mLockatedPreferences.getDstLat());
                    //Log.e("mLockatedPreferences.getDstLng","After set "+mLockatedPreferences.getDstLng());

                    OlaMainActivity_Latest.edt_olaDestinationLocation.setText(str);
                    OlaMainActivity_Latest.edt_olaDestinationLocation.setTextColor(Color.BLACK);

                }
                //Log.e("Address", "" + str);
                OlaMainActivity_Latest.SourceLocation(latLng,autoComplete_number);

                //OlaMainActivity.latlng_from_find = latLng;

                //OlaMainActivity_Latest OlaMainActivity_Latest = new OlaMainActivity_Latest();

                finish();

            } });
    }

    public LatLng getLocationFromAddress(Context context, String strAddress) {

        Geocoder coder = new Geocoder(context);
        List<Address> address;
        LatLng p1;

        try {
            address = coder.getFromLocationName(strAddress, 5);
            if (address == null) {
                return null;
            }
            Address location = address.get(0);
            location.getLatitude();
            location.getLongitude();
            p1 = new LatLng(location.getLatitude(), location.getLongitude());
            return p1;
        } catch (Exception e) {
            p1 = new LatLng(0.0, 0.0);
            e.printStackTrace();
        }
        //Log.e("getLocationFromAddress", "" + p1);
        return p1;
    }

    private void onLocationChangeListener(double latitude, double longitude) {
        if (this != null) {
            if (ConnectionDetector.isConnectedToInternet(this)) {
                CheapestOlaRideFragment.progressBar.setVisibility(View.VISIBLE);
                CheapestOlaRideFragment.cheap_view.setVisibility(View.GONE);
                //String url = "http://sandbox-t.olacabs.com/v1/products?pickup_lat=" + latitude + "&pickup_lng=" + longitude;
                String url = "http://sandbox-t.olacabs.com/v1/products?pickup_lat=12.9491416&pickup_lng=77.64298";
                //Log.e("onLocationPointListener", "" + url);
                LockatedVolleyRequestQueue mLockatedVolleyRequestQueue = LockatedVolleyRequestQueue.getInstance(this);
                mLockatedVolleyRequestQueue.sendGetRequestOlaCabs(REQUEST_TAG, Request.Method.GET, url, this, this);
            } else {
                Utilities.showToastMessage(this, this.getResources().getString(R.string.internet_connection_error));
            }
        }
    }

    @Override
    public void onErrorResponse(VolleyError error) {
    }

    @Override
    public void onResponse(String response) {
        //Log.e("RESPONSE", "" + response);
        if (this != null) {
            if (response != null && response.length() > 0) {
                CheapestOlaRideFragment.progressBar.setVisibility(View.GONE);

                JSONObject jsonObject = null;
                try {
                    jsonObject = new JSONObject(response);
                    if (jsonObject.has("categories") && jsonObject.getJSONArray("categories").length() > 0) {
                        CheapestOlaRideFragment.cheap_view.setVisibility(View.VISIBLE);
                        JSONArray jsonArray = jsonObject.getJSONArray("categories");
                        //Log.e("jsonArray.length", "" + jsonArray.length());
                        Gson gson = new Gson();
                        for (int i = 0; i < jsonArray.length(); i++) {

                            if (jsonArray.getJSONObject(i).getInt("eta") != -1) {
                                //OlaCabCategoryDetail mOlaCabCategoryDetail = gson.fromJson(jsonArray.getJSONObject(i).toString(), OlaCabCategoryDetail.class);
                                //olaCabCategoryDetails.clear();
                                //olaCabCategoryDetails.isEmpty();
                                OlaCabCategoryDetail mOlaCabCategoryDetail = gson.fromJson(jsonArray.getJSONObject(i).toString(), OlaCabCategoryDetail.class);
                                olaCabCategoryDetails.add(mOlaCabCategoryDetail);
                            }

                        }
                        olaCabsAdapter = new OlaCabsAdapter(this, olaCabCategoryDetails, getSupportFragmentManager());
                        CheapestOlaRideFragment.mRecyclerView.setAdapter(olaCabsAdapter);
                    } else {
                        if (olaCabCategoryDetails.size() > 0) {
                            olaCabCategoryDetails.clear();
                        }
                        CheapestOlaRideFragment.mTextViewNoCFound.setVisibility(View.VISIBLE);
                    }
                    olaCabsAdapter.notifyDataSetChanged();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}
