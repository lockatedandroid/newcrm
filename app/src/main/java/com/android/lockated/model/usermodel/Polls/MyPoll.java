
package com.android.lockated.model.usermodel.Polls;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class MyPoll {

    @SerializedName("id")
    @Expose
    private int id;
    @SerializedName("society_id")
    @Expose
    private int societyId;
    @SerializedName("subject")
    @Expose
    private Object subject;
    @SerializedName("user_id")
    @Expose
    private int userId;
    @SerializedName("start")
    @Expose
    private String start;
    @SerializedName("end")
    @Expose
    private String end;
    @SerializedName("active")
    @Expose
    private Object active;
    @SerializedName("created_at")
    @Expose
    private String createdAt;
    @SerializedName("publish")
    @Expose
    private int publish;
    @SerializedName("canceled_by")
    @Expose
    private String canceledBy;
    @SerializedName("canceler_id")
    @Expose
    private int cancelerId;
    @SerializedName("comment")
    @Expose
    private Object comment;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("total_votes")
    @Expose
    private int totalVotes;
    @SerializedName("flat")
    @Expose
    private String flat;
    @SerializedName("poll_options")
    @Expose
    private ArrayList<PollOption> pollOptions = new ArrayList<PollOption>();
    @SerializedName("user")
    @Expose
    private PollUser pollUser;

    /**
     * @return The id
     */
    public int getId() {
        return id;
    }

    /**
     * @param id The id
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * @return The societyId
     */
    public int getSocietyId() {
        return societyId;
    }

    /**
     * @param societyId The society_id
     */
    public void setSocietyId(int societyId) {
        this.societyId = societyId;
    }

    /**
     * @return The subject
     */
    public Object getSubject() {
        return subject;
    }

    /**
     * @param subject The subject
     */
    public void setSubject(Object subject) {
        this.subject = subject;
    }

    /**
     * @return The userId
     */
    public int getUserId() {
        return userId;
    }

    /**
     * @param userId The user_id
     */
    public void setUserId(int userId) {
        this.userId = userId;
    }

    /**
     * @return The start
     */
    public String getStart() {
        return start;
    }

    /**
     * @param start The start
     */
    public void setStart(String start) {
        this.start = start;
    }

    /**
     * @return The end
     */
    public String getEnd() {
        return end;
    }

    /**
     * @param end The end
     */
    public void setEnd(String end) {
        this.end = end;
    }

    /**
     * @return The active
     */
    public Object getActive() {
        return active;
    }

    /**
     * @param active The active
     */
    public void setActive(Object active) {
        this.active = active;
    }

    /**
     * @return The createdAt
     */
    public String getCreatedAt() {
        return createdAt;
    }

    /**
     * @param createdAt The created_at
     */
    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    /**
     * @return The publish
     */
    public int getPublish() {
        return publish;
    }

    /**
     * @param publish The publish
     */
    public void setPublish(int publish) {
        this.publish = publish;
    }

    /**
     * @return The canceledBy
     */
    public String getCanceledBy() {
        return canceledBy;
    }

    /**
     * @param canceledBy The canceled_by
     */
    public void setCanceledBy(String canceledBy) {
        this.canceledBy = canceledBy;
    }

    /**
     * @return The cancelerId
     */
    public int getCancelerId() {
        return cancelerId;
    }

    /**
     * @param cancelerId The canceler_id
     */
    public void setCancelerId(int cancelerId) {
        this.cancelerId = cancelerId;
    }

    /**
     * @return The comment
     */
    public Object getComment() {
        return comment;
    }

    /**
     * @param comment The comment
     */
    public void setComment(Object comment) {
        this.comment = comment;
    }

    /**
     * @return The status
     */
    public String getStatus() {
        return status;
    }

    /**
     * @param status The status
     */
    public void setStatus(String status) {
        this.status = status;
    }

    /**
     * @return The totalVotes
     */
    public int getTotalVotes() {
        return totalVotes;
    }

    /**
     * @param totalVotes The total_votes
     */
    public void setTotalVotes(int totalVotes) {
        this.totalVotes = totalVotes;
    }

    /**
     * @return The flat
     */
    public String getFlat() {
        return flat;
    }

    /**
     * @param flat The flat
     */
    public void setFlat(String flat) {
        this.flat = flat;
    }

    /**
     * @return The pollOptions
     */
    public ArrayList<PollOption> getPollOptions() {
        return pollOptions;
    }

    /**
     * @param pollOptions The poll_options
     */
    public void setPollOptions(ArrayList<PollOption> pollOptions) {
        this.pollOptions = pollOptions;
    }

    /**
     * @return The pollUser
     */
    public PollUser getPollUser() {
        return pollUser;
    }

    /**
     * @param pollUser The poll_user
     */
    public void setPollUser(PollUser pollUser) {
        this.pollUser = pollUser;
    }

}
