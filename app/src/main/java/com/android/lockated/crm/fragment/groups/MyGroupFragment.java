package com.android.lockated.crm.fragment.groups;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.android.lockated.Interfaces.IRecyclerItemClickListener;
import com.android.lockated.R;
import com.android.lockated.crm.activity.CreateGroupActivity;
import com.android.lockated.crm.adapters.GroupAdapter;
import com.android.lockated.crm.adapters.GroupMemberListAdapter;
import com.android.lockated.model.userGroups.Groupmember;
import com.android.lockated.model.userGroups.UserGroup;
import com.android.lockated.network.ConnectionDetector;
import com.android.lockated.preferences.LockatedPreferences;
import com.android.lockated.request.LockatedRequestError;
import com.android.lockated.request.LockatedVolleyRequestQueue;
import com.android.lockated.utils.ApplicationURL;
import com.android.lockated.utils.Utilities;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class MyGroupFragment extends Fragment implements IRecyclerItemClickListener, Response.ErrorListener, Response.Listener<JSONObject>, View.OnClickListener {

    TextView noData;
    ProgressBar progressBar;
    String REQUEST_TAG = "MyGroupFragment";
    String DELETE_TAG = "DeleteGroup";
    ArrayList<UserGroup> userGroupArrayList;
    ArrayList<Groupmember> groupmemberArrayList;
    LockatedPreferences mLockatedPreferences;
    FloatingActionButton addGroupFab;
    GroupAdapter groupAdapter;
    GroupMemberListAdapter groupMemberListAdapter;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View myGroupView = inflater.inflate(R.layout.fragment_my_group, container, false);
        Utilities.lockatedGoogleAnalytics(getString(R.string.my_groups),
                getString(R.string.visited), getString(R.string.my_groups));
        init(myGroupView);
        return myGroupView;
    }

    @Override
    public void onResume() {
        super.onResume();
        userGroupArrayList.clear();
        callGroupApi();
    }

    private void init(View myGroupView) {
        userGroupArrayList = new ArrayList<>();
        groupmemberArrayList = new ArrayList<>();
        mLockatedPreferences = new LockatedPreferences(getActivity());
        RecyclerView groupContainer = (RecyclerView) myGroupView.findViewById(R.id.groupContainer);
        progressBar = (ProgressBar) myGroupView.findViewById(R.id.mProgressBarView);
        noData = (TextView) myGroupView.findViewById(R.id.noData);
        addGroupFab = (FloatingActionButton) myGroupView.findViewById(R.id.addGroupFab);
        addGroupFab.setOnClickListener(this);
        LinearLayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        groupContainer.setLayoutManager(mLayoutManager);
        groupContainer.setHasFixedSize(true);
        groupAdapter = new GroupAdapter(getActivity(), userGroupArrayList, this, true);
        groupContainer.setAdapter(groupAdapter);
        groupMemberListAdapter = new GroupMemberListAdapter(getActivity(), groupmemberArrayList);
    }

    public void callGroupApi() {
        if (getActivity() != null) {
            if (ConnectionDetector.isConnectedToInternet(getActivity())) {
                progressBar.setVisibility(View.VISIBLE);
                String url = ApplicationURL.getMyGroups + mLockatedPreferences.getLockatedToken();
                LockatedVolleyRequestQueue lockatedVolleyRequestQueue = LockatedVolleyRequestQueue.getInstance(getActivity());
                lockatedVolleyRequestQueue.sendRequest(REQUEST_TAG, Request.Method.GET, url, null, this, this);
            } else {
                Utilities.showToastMessage(getActivity(), getActivity().getResources()
                        .getString(R.string.internet_connection_error));
            }
        }
    }

    private void groupMemberListAlertDialog(Context context, String groupName) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle(groupName);
        builder.setAdapter(groupMemberListAdapter, null);
        AlertDialog dialog = builder.create();
        dialog.show();
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = 450;
        dialog.getWindow().setAttributes(lp);

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.addGroupFab:
                Intent intent = new Intent(getActivity(), CreateGroupActivity.class);
                getActivity().startActivity(intent);
                break;
        }
    }

    @Override
    public void onErrorResponse(VolleyError error) {
        if (getActivity() != null) {
            progressBar.setVisibility(View.GONE);
            LockatedRequestError.onRequestError(getActivity(), error);
        }
    }

    @Override
    public void onResponse(JSONObject response) {
        if (getActivity() != null) {
            progressBar.setVisibility(View.GONE);
            try {
                if (response.has("usergroups")) {
                    if (response.getJSONArray("usergroups").length() > 0) {
                        mLockatedPreferences.setGroupList(response);
                        JSONArray jsonArray = response.getJSONArray("usergroups");
                        for (int i = 0; i < jsonArray.length(); i++) {
                            Gson gson = new Gson();
                            UserGroup userGroup = gson.fromJson(jsonArray.getJSONObject(i).toString(),
                                    UserGroup.class);
                            userGroupArrayList.add(userGroup);
                        }
                        if (noData.getVisibility() == View.VISIBLE) {
                            noData.setVisibility(View.GONE);
                        }
                        groupAdapter.notifyDataSetChanged();
                    } else {
                        noData.setVisibility(View.VISIBLE);
                    }

                } else if (response.has("message")) {
                    Utilities.showToastMessage(getActivity(), response.getString("message"));
                    userGroupArrayList.clear();
                    callGroupApi();
                } else {
                    noData.setVisibility(View.VISIBLE);
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void onRecyclerItemClick(View view, int position) {
        switch (view.getId()) {
            case R.id.groupDelete:
                deleteGroup(position);
                break;

            case R.id.groupName:
                viewGroupMembers(position);
                break;

            case R.id.groupEdit:
                editGroup(position);
                break;
        }
    }

    private void deleteGroup(int position) {
        if (getActivity() != null) {
            if (ConnectionDetector.isConnectedToInternet(getActivity())) {
                String url = ApplicationURL.deleteGroup + userGroupArrayList.get(position).getId()
                        + ".json?token=" + mLockatedPreferences.getLockatedToken();
                LockatedVolleyRequestQueue lockatedVolleyRequestQueue = LockatedVolleyRequestQueue.getInstance(getActivity());
                lockatedVolleyRequestQueue.sendRequest(DELETE_TAG, Request.Method.DELETE, url, null, this, this);
            } else {
                Utilities.showToastMessage(getActivity(), getActivity().getResources()
                        .getString(R.string.internet_connection_error));
            }
        }
    }

    private void viewGroupMembers(int position) {
        groupmemberArrayList.clear();
        groupmemberArrayList.addAll(userGroupArrayList.get(position).getGroupmembers());
        groupAdapter.notifyDataSetChanged();
        groupMemberListAlertDialog(getActivity(), userGroupArrayList.get(position).getName());
    }

    private void editGroup(int position) {
        ArrayList<Integer> userId = new ArrayList<>();
        userId.clear();
        for (int i = 0; i < userGroupArrayList.get(position).getGroupmembers().size(); i++) {
            userId.add(userGroupArrayList.get(position).getGroupmembers().get(i).getUserId());
        }
        groupmemberArrayList.clear();
        groupmemberArrayList.addAll(userGroupArrayList.get(position).getGroupmembers());
        Intent intent = new Intent(getActivity(), CreateGroupActivity.class);
        intent.putExtra("groupName", userGroupArrayList.get(position).getName());
        intent.putExtra("updateGroup", getString(R.string.update_group));
        intent.putExtra("groupId", "" + userGroupArrayList.get(position).getId());
        intent.putIntegerArrayListExtra("userId", userId);
        intent.putParcelableArrayListExtra("groupmemberArrayList", groupmemberArrayList);
        getActivity().startActivity(intent);
    }

}
