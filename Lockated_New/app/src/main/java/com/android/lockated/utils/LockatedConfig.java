package com.android.lockated.utils;

public class LockatedConfig {

    //==================================GridLayoutManager===========================================
    public static final int SPAN_COUNT = 3;
    //==================================PayTm=======================================================
    //====staging====
    /*public static  String requestType = "DEFAULT";
    public static  String midValue = "HAVENI27654766472012";
    public static  String channelId = "WAP";
    public static  String intustryTypeId = " Retail";
    public static  String website = "havenwap";
    public static  String merchantKey = " tMAbQs0gnegTj2c_";
    public static  String theme = "merchant";*/
    //====Live====
    public static String requestType = "DEFAULT";
    public static String midValue = "lockat86249336134829";
    public static String channelId = "WAP";
    public static String intustryTypeId = "Retail108";
    public static String website = "lockatedwap";

    public static String merchantKey = "nB2PMswIUVV@byQl";
    public static String theme = "merchant";

    //==================================Push Notifications==========================================
    // flag to identify whether to show single line
    // or multi line text in push notification tray
    public static boolean appendNotificationMessages = false;

    // global topic to receive app wide push notifications
    public static final String TOPIC_GLOBAL = "global";

    // broadcast receiver intent filters
    public static final String SENT_TOKEN_TO_SERVER = "sentTokenToServer";
    public static final String REGISTRATION_COMPLETE = "registrationComplete";
    public static final String PUSH_NOTIFICATION = "pushNotification";

    // type of push messages
    public static final int PUSH_TYPE_CHATROOM = 1;
    public static final int PUSH_TYPE_USER = 2;

    // id to handle the notification in the notification try
    public static final int NOTIFICATION_ID = 100;
    public static final int NOTIFICATION_ID_BIG_IMAGE = 101;

    //=====================================Camera Image Response Codes==============================
    public static final int REQUEST_CAMERA_PHOTO = 3;
    public static final int REQUEST_TAKE_PHOTO = 100;
    public static final int CHOOSE_IMAGE_REQUEST = 101;
    public static final int REQUEST_CAMERA = 102;
    public static final int REQUEST_STORAGE = 103;

}
