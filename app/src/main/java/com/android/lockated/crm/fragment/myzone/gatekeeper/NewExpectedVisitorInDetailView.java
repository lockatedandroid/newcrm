package com.android.lockated.crm.fragment.myzone.gatekeeper;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.android.lockated.R;
import com.android.lockated.crm.activity.CrmVisitorActivity;
import com.android.lockated.model.expectedVisitor.Document;
import com.android.lockated.model.expectedVisitor.GatekeeperExpected;
import com.android.lockated.network.ConnectionDetector;
import com.android.lockated.preferences.LockatedPreferences;
import com.android.lockated.request.LockatedJSONObjectRequest;
import com.android.lockated.request.LockatedVolleyRequestQueue;
import com.android.lockated.utils.ApplicationURL;
import com.android.lockated.utils.DatePickerFragment;
import com.android.lockated.utils.ShowImage;
import com.android.lockated.utils.TimePickerFragment;
import com.android.lockated.utils.Utilities;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.google.gson.Gson;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

public class NewExpectedVisitorInDetailView extends AppCompatActivity implements ViewPager.OnPageChangeListener, View.OnClickListener, Response.ErrorListener, Response.Listener<JSONObject> {
    private ImageView mImageView;

    private EditText mEditFlatNumber, mEditVisitorName, mEditAdditionalMember, mEditTextMobileNumber, mEditEnterOtp,
            mEditVehicleNumber, mEditNotes, mEditNumberofItem, mEditNumberofItemList, mEditMemberName;
    LinearLayout mOwnerStatusLayout, mSuperVisiorStatusLayout, mMainSuperVisiorStatusLayout, sendOtpButton, mLayoutSubmitOTP, reSendOtpButton;
    LinearLayout mUploadDocLayout;
    TextView mTextOwnerStatus, mTextSupervisorStatus, mTextMainSupervisorStatus, mUpdate;
    private TextView mEditOtp, mTextExpectedDate, mTextExpectedTime, mStatus;
    TextView mSpinnerCategory;
    CheckBox checkBoxInOut;
    Spinner mSpinnerPurpose;
    ImageView mImageDate, mImageExpectedTime;
    LinearLayout moveInOutLayout;
    ViewPager mImageViewCategoryItem;
    private LinearLayout pager_indicator;
    private RelativeLayout pagerIndicatoRelative;
    private ImageView[] dots;
    String strImage;
    ArrayAdapter<String> stringArrayadapter;
    int oTypeItemId, dotsCount;
    private ProgressDialog mProgressDialog;
    private RequestQueue mQueue;
    private String REQUEST_TAG = "PostData";
    private String REQUEST_VISITOR_DATA = "Visitor Data";
    private String SEND_OTP_TAG = "SendOtp";
    private LockatedPreferences mLockatedPreferences;
    ArrayList<String> purposesArrays;
    private String id;
    private String CREATE_TAG = "Get";
    private String approve;
    private String status;
    private TextView mApprove;
    private TextView mReject;
    private TextView mAccompany;
    private String spid;

    private GatekeeperExpected gatekeeperVisiterData;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_visitor_expected_detail_view);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(true);
        getSupportActionBar().setTitle(R.string.visitorDetail);
        init();
    }

    private void init() {
        purposesArrays = new ArrayList<>();
        mLockatedPreferences = new LockatedPreferences(this);
        mImageView = (ImageView) findViewById(R.id.mImageView);
        mImageViewCategoryItem = (ViewPager) findViewById(R.id.mImageViewCategoryItem);
        pager_indicator = (LinearLayout) findViewById(R.id.viewPagerCountDots);
        mOwnerStatusLayout = (LinearLayout) findViewById(R.id.mOwnerStatusLayout);
        /*sendOtpButton = (LinearLayout) view.findViewById(R.id.sendOtpButton);*/
        reSendOtpButton = (LinearLayout) findViewById(R.id.reSendOtpButton);
        mLayoutSubmitOTP = (LinearLayout) findViewById(R.id.mLayoutSubmitOTP);
        mSuperVisiorStatusLayout = (LinearLayout) findViewById(R.id.mSuperVisiorStatusLayout);
        mMainSuperVisiorStatusLayout = (LinearLayout) findViewById(R.id.mMainSuperVisiorStatusLayout);
        pagerIndicatoRelative = (RelativeLayout) findViewById(R.id.pagerIndicatoRelative);
        mImageDate = (ImageView) findViewById(R.id.mImageDate);
        mImageExpectedTime = (ImageView) findViewById(R.id.mImageExpectedTime);
        mSpinnerPurpose = (Spinner) findViewById(R.id.mSpinnerPurpose);

        mUploadDocLayout = (LinearLayout) findViewById(R.id.mUploadDocLayout);

        mEditFlatNumber = (EditText) findViewById(R.id.mEditFlatNumber);
        mEditVisitorName = (EditText) findViewById(R.id.mEditVisitorName);
        mEditAdditionalMember = (EditText) findViewById(R.id.mEditAdditionalMember);
        mEditTextMobileNumber = (EditText) findViewById(R.id.mEditTextMobileNumber);
        mEditVehicleNumber = (EditText) findViewById(R.id.mEditVehicleNumber);
        mEditNotes = (EditText) findViewById(R.id.mEditNotes);
        mEditNumberofItemList = (EditText) findViewById(R.id.mEditNumberofItemList);
        mEditMemberName = (EditText) findViewById(R.id.mEditMemberName);
        mEditNumberofItem = (EditText) findViewById(R.id.mEditNumberofItem);
        mEditEnterOtp = (EditText) findViewById(R.id.mEditEnterOtp);
        mUpdate = (TextView) findViewById(R.id.mUpdate);
        mApprove = (TextView) findViewById(R.id.mApprove);
        mReject = (TextView) findViewById(R.id.mReject);
        mAccompany = (TextView) findViewById(R.id.mAccompany);
        mTextExpectedDate = (TextView) findViewById(R.id.mTextExpectedDate);
        mTextExpectedTime = (TextView) findViewById(R.id.mTextExpectedTime);
        mTextOwnerStatus = (TextView) findViewById(R.id.mTextOwnerStatus);
        mTextSupervisorStatus = (TextView) findViewById(R.id.mTextSupervisorStatus);
        mTextMainSupervisorStatus = (TextView) findViewById(R.id.mTextMainSupervisorStatus);
        checkBoxInOut = (CheckBox) findViewById(R.id.checkBoxInOut);
        mSpinnerCategory = (TextView) findViewById(R.id.mSpinnerCategory);
        moveInOutLayout = (LinearLayout) findViewById(R.id.moveInOutLayout);
        mStatus = (TextView) findViewById(R.id.status);
        /*mEditOtp = (TextView) view.findViewById(R.id.mEditOtp);*/
/*
        if (spid != null) {
            mUpdate.setVisibility(View.VISIBLE);
            mApprove.setVisibility(View.GONE);
            mReject.setVisibility(View.GONE);
            mAccompany.setVisibility(View.GONE);
         *//* *//**//*  visitto.setVisibility(View.VISIBLE);
            mVisitTo.setVisibility(View.VISIBLE);*//**//*
            mVisitPurpose.setEnabled(true);
            mVehiclrNumber.setEnabled(true);
            mVisitorName.setEnabled(true);
            mAdditionalMember.setEnabled(true);
            mContactNumber.setEnabled(true);*//*
            //  getSetEditData();
        } else {

            mApprove.setVisibility(View.VISIBLE);
            mAccompany.setVisibility(View.VISIBLE);
            mReject.setVisibility(View.VISIBLE);
            mUpdate.setVisibility(View.GONE);
        }*/

        pagerIndicatoRelative.setVisibility(View.GONE);
        mImageView.setOnClickListener(this);
        mUpdate.setOnClickListener(this);
        mApprove.setOnClickListener(this);
        mAccompany.setOnClickListener(this);
        mReject.setOnClickListener(this);
        reSendOtpButton.setOnClickListener(this);
        mLayoutSubmitOTP.setOnClickListener(this);
        //mImageExpectedTime.setOnClickListener(this);   // changes by Bhavesh
        //mImageDate.setOnClickListener(this);  // changes by Bhavesh
        mImageViewCategoryItem.setOnPageChangeListener(this);

        getGateKeeperVisitorData(mLockatedPreferences.getGateKeeper_id());
        //getDetailViewData();
        //getPurposes();
    }

    private void getPurposes() {
        String url = ApplicationURL.getVisitorAllPurposes + mLockatedPreferences.getSocietyId()
                + "&token=" + mLockatedPreferences.getLockatedToken();
        requestGetApi(url);
    }

    private void setUiPageViewController(int count) {
        dotsCount = count;
        dots = new ImageView[dotsCount];
        for (int i = 0; i < dotsCount; i++) {
            dots[i] = new ImageView(this);
            dots[i].setImageDrawable(getResources().getDrawable(R.drawable.circle_128));
            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                    15, LinearLayout.LayoutParams.WRAP_CONTENT
            );
            params.setMargins(1, 0, 1, 0);
            pager_indicator.addView(dots[i], params);
        }
        dots[0].setImageDrawable(getResources().getDrawable(R.drawable.bullet));
    }

    private void getGateKeeperVisitorData(String gateKeeper_id) {
        mProgressDialog = ProgressDialog.show(this, "", "Please Wait...", false);
        if (ConnectionDetector.isConnectedToInternet(getApplicationContext())) {
            mQueue = LockatedVolleyRequestQueue.getInstance(getApplicationContext()).getRequestQueue();
            String url = "https://www.lockated.com/gatekeepers/" + gateKeeper_id + ".json?token=" + mLockatedPreferences.getLockatedToken();
            LockatedJSONObjectRequest lockatedJSONObjectRequest = new LockatedJSONObjectRequest(Request.Method.GET, url, null, this, this);
            lockatedJSONObjectRequest.setTag(REQUEST_TAG);
            mQueue.add(lockatedJSONObjectRequest);
        } else {
            Utilities.showToastMessage(getApplicationContext(), getApplicationContext().getResources().getString(R.string.internet_connection_error));
        }
    }

    private void getDetailViewData() {
            mEditVisitorName.setText(gatekeeperVisiterData.getGuestName());
            mEditVisitorName.setClickable(false);
            mEditVisitorName.setEnabled(false);
            mEditTextMobileNumber.setText(gatekeeperVisiterData.getGuestNumber());
            mEditTextMobileNumber.setClickable(false);
            mEditTextMobileNumber.setEnabled(false);

            if (gatekeeperVisiterData.getGuestVehicleNumber() != null) {
                mEditVehicleNumber.setText(gatekeeperVisiterData.getGuestVehicleNumber());
            } else {
                mEditVehicleNumber.setText("Not Available");
            }
            if (gatekeeperVisiterData.getNotes() != null) {
                mEditNotes.setText(gatekeeperVisiterData.getNotes());
            } else {
                mEditNotes.setText("Not Available");
            }

            mEditNotes.setClickable(false);
            mEditNotes.setEnabled(false);

            if (gatekeeperVisiterData.getFlat() != null && !gatekeeperVisiterData.getFlat().equals("")) {
                mEditFlatNumber.setText(gatekeeperVisiterData.getFlat());
            } else {
                mEditFlatNumber.setText("Not Available");
            }
            approve = String.valueOf(gatekeeperVisiterData.getApprove());
            mStatus.setText(gatekeeperVisiterData.getVstatus());
            if (approve.equals("0")) {
                mApprove.setVisibility(View.VISIBLE);
                mReject.setVisibility(View.VISIBLE);
                mAccompany.setVisibility(View.VISIBLE);
            } else {
                mApprove.setVisibility(View.GONE);
                mReject.setVisibility(View.GONE);
                mAccompany.setVisibility(View.GONE);
            }

            mEditAdditionalMember.setText(gatekeeperVisiterData.getPlusPerson() + "");

            if (gatekeeperVisiterData.getCreatedBy() != null) {
                mEditMemberName.setText(gatekeeperVisiterData.getCreatedBy() + "");
            } else {
                mEditMemberName.setText("Not Available");
            }

            if (gatekeeperVisiterData.getExpectedAt() != null) {
                mTextExpectedDate.setText(Utilities.ddMMYYYFormat(gatekeeperVisiterData.getExpectedAt()));
                mTextExpectedTime.setText(Utilities.m24TimeFormat(gatekeeperVisiterData.getExpectedAt()));
            } else {
                mTextExpectedDate.setText("Not Available");
                mTextExpectedTime.setText("Not Available");
            }
            mSpinnerCategory.setText(gatekeeperVisiterData.getVisitPurpose());
            id = String.valueOf(gatekeeperVisiterData.getId());

            if (gatekeeperVisiterData.getGatekeeperMimos() != null &&
                    gatekeeperVisiterData.getGatekeeperMimos().size() > 0) {
                if (gatekeeperVisiterData.getGatekeeperMimos().get(0).getMimoType().equals("move_in")) {
                    checkBoxInOut.setText("Move In");
                    checkBoxInOut.setChecked(true);
                    checkBoxInOut.setEnabled(false);
                    moveInOutLayout.setVisibility(View.VISIBLE);
                } else if (gatekeeperVisiterData.getGatekeeperMimos().get(0).getMimoType().equals("move_out")) {
                    checkBoxInOut.setText("Move Out");
                    checkBoxInOut.setChecked(true);
                    checkBoxInOut.setEnabled(false);
                    moveInOutLayout.setVisibility(View.VISIBLE);
                }

                mEditNumberofItem.setText(gatekeeperVisiterData.getGatekeeperMimos().get(0).getItemNumber() + "");
                if (gatekeeperVisiterData.getGatekeeperMimos().get(0).getDetails() != null && !gatekeeperVisiterData.getGatekeeperMimos().get(0).getDetails().equals("")) {
                    mEditNumberofItemList.setText(gatekeeperVisiterData.getGatekeeperMimos().get(0).getDetails());
                } else {
                    mEditNumberofItemList.setText("Not Available");
                }

                if (gatekeeperVisiterData.getGatekeeperMimos().get(0).getDocuments() != null &&
                        gatekeeperVisiterData.getGatekeeperMimos().get(0).getDocuments().size() > 0) {
                    //Log.e("size", "" + gatekeeperArrayList.get(position).getGatekeeperMimos().get(0).getDocuments().size());
                    mUploadDocLayout.setVisibility(View.VISIBLE);
                    ArrayList<Document> documents = gatekeeperVisiterData.getGatekeeperMimos().get(0).getDocuments();
                    pagerIndicatoRelative.setVisibility(View.VISIBLE);
                    ImageAdapterDetail imageAdapter = new ImageAdapterDetail(this, documents, true);
                    mImageViewCategoryItem.setAdapter(imageAdapter);
                    imageAdapter.notifyDataSetChanged();
                    pager_indicator.removeAllViews();
                    setUiPageViewController(documents.size());
                } else {
                    mUploadDocLayout.setVisibility(View.GONE);
                }
            } else {
                checkBoxInOut.setText("Not Available");
                mEditNumberofItemList.setText("Not Available");
                mEditNumberofItem.setText("0");
            }

            if (gatekeeperVisiterData.getImage() != null && !gatekeeperVisiterData.getImage().equals("")) {
                Picasso.with(this).load(gatekeeperVisiterData.getImage()).fit().into(mImageView);
                strImage = gatekeeperVisiterData.getImage();
            } else {
                strImage = "no";
            }
        }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }

    @Override
    public void onPageSelected(int position) {
        if (this != null && dotsCount != 0) {
            for (int i = 0; i < dotsCount; i++) {
                dots[i].setImageDrawable(getResources().getDrawable(R.drawable.circle_128));
            }
            dots[position].setImageDrawable(getResources().getDrawable(R.drawable.bullet));
        }
    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.mImageView:
                if (!strImage.equals("no")) {
                    Intent intent = new Intent(this, ShowImage.class);
                    intent.putExtra("imageUrlString", strImage);
                    startActivity(intent);
                }
                break;
            case R.id.mUpdate:
                updateRequestedVisitor(id);
                break;
            case R.id.mApprove:
                onApproveClicked(id);
                break;
            case R.id.mAccompany:
                onAccompanyClicked(id);
                break;
            case R.id.mReject:
                onRejectClicked(id);
                break;
            case R.id.mImageDate:
                datepicker(mTextExpectedDate);
                break;
            case R.id.mImageExpectedTime:
                selectTime(mTextExpectedTime);
                break;
          /*  case R.id.reSendOtpButton:
                sendOtp();
                break;
            case R.id.mLayoutSubmitOTP:
                verifyOtp();
                break;*/
        }
    }

    private void onRejectClicked(String approve_id) {
        mProgressDialog = ProgressDialog.show(this, "", "Please Wait...", false);
        JSONObject jsonObjectMain = new JSONObject();
        try {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("approve", "2");
            jsonObjectMain.put("gatekeeper", jsonObject);

        } catch (JSONException e) {
            e.printStackTrace();
        }
        mQueue = LockatedVolleyRequestQueue.getInstance(this).getRequestQueue();
        String url = ApplicationURL.UpdateGateKeeper + approve_id + ".json?token="
                + mLockatedPreferences.getLockatedToken();
        LockatedJSONObjectRequest lockatedJSONObjectRequest = new LockatedJSONObjectRequest(Request.Method.PUT,
                url,
                jsonObjectMain, this, this);
        lockatedJSONObjectRequest.setTag(REQUEST_TAG);
        mQueue.add(lockatedJSONObjectRequest);
    }

    private void onAccompanyClicked(String approve_id) {
        mProgressDialog = ProgressDialog.show(this, "", "Please Wait...", false);
        JSONObject jsonObjectMain = new JSONObject();
        try {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("approve", "1");
            jsonObject.put("accompany", "1");
            jsonObjectMain.put("gatekeeper", jsonObject);

        } catch (JSONException e) {
            e.printStackTrace();
        }
        mQueue = LockatedVolleyRequestQueue.getInstance(this).getRequestQueue();
        String url = ApplicationURL.UpdateGateKeeper + approve_id + ".json?token="
                + mLockatedPreferences.getLockatedToken();
        LockatedJSONObjectRequest lockatedJSONObjectRequest = new LockatedJSONObjectRequest(Request.Method.PUT,
                url,
                jsonObjectMain, this, this);
        lockatedJSONObjectRequest.setTag(REQUEST_TAG);
        mQueue.add(lockatedJSONObjectRequest);
    }

    private void onApproveClicked(String approve_id) {
        mProgressDialog = ProgressDialog.show(this, "", "Please Wait...", false);
        JSONObject jsonObjectMain = new JSONObject();
        try {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("approve", "1");
            jsonObjectMain.put("gatekeeper", jsonObject);

        } catch (JSONException e) {
            e.printStackTrace();
        }
        mQueue = LockatedVolleyRequestQueue.getInstance(this).getRequestQueue();
        String url = ApplicationURL.UpdateGateKeeper + approve_id + ".json?token="
                + mLockatedPreferences.getLockatedToken();
        LockatedJSONObjectRequest lockatedJSONObjectRequest = new LockatedJSONObjectRequest(Request.Method.PUT,
                url,
                jsonObjectMain, this, this);
        lockatedJSONObjectRequest.setTag(REQUEST_TAG);
        mQueue.add(lockatedJSONObjectRequest);
    }

    private void datepicker(TextView mImageDate) {
        DatePickerFragment datePickerFragment = new DatePickerFragment();
        datePickerFragment.setDatePickerView(mImageDate);
        DialogFragment newFragment = datePickerFragment;
        newFragment.show(this.getSupportFragmentManager(), "DatePicker");
    }

    private void selectTime(TextView mTextExpectedTime) {
        TimePickerFragment timePickerFragment = new TimePickerFragment();
        timePickerFragment.setTimePickerView(mTextExpectedTime);
        DialogFragment newFragment = timePickerFragment;
        newFragment.show(this.getSupportFragmentManager(), "TimePicker");
    }

    private void updateRequestedVisitor(String id) {
        String strNotes = mEditNotes.getText().toString();
        String striVehicleNumber = mEditVehicleNumber.getText().toString();
        String striMember = mEditAdditionalMember.getText().toString();
        String striName = mEditVisitorName.getText().toString();
        String strExpDate = "";
        String strEntryTime = "";

        String striContactNumber = mEditTextMobileNumber.getText().toString();
        JSONObject jsonObjectMain = new JSONObject();
        JSONObject jsonObject = new JSONObject();
        if (strExpDate.equals("Not Available")) {
            strExpDate = "";
        } else {
            strExpDate = mTextExpectedDate.getText().toString();
        }
        if (strEntryTime.equals("Not Available")) {
            strEntryTime = "";
        } else {
            strEntryTime = mTextExpectedTime.getText().toString();
        }
        if ((TextUtils.isEmpty(striName))) {
            Utilities.showToastMessage(this, "Please Fill Name");
        } else if (TextUtils.isEmpty(striContactNumber)) {
            Utilities.showToastMessage(this, "Please Fill Contact Number");
        } else {
            try {
                jsonObject.put("guest_name", striName);
                jsonObject.put("guest_number", striContactNumber);
                jsonObject.put("guest_vehicle_number", striVehicleNumber);
                jsonObject.put("plus_person", striMember);
                jsonObject.put("expected_at", strExpDate + "T" + strEntryTime);
                jsonObject.put("notes", strNotes);
                jsonObjectMain.put("gatekeeper", jsonObject);
                Log.e("jsonObjectMain", "" + jsonObjectMain);
            } catch (JSONException e) {
                e.printStackTrace();
            }

            String url = ApplicationURL.UpdateGateKeeper + id + ".json?token=" + mLockatedPreferences.getLockatedToken();
            request(jsonObjectMain, url);

        }
    }

/*
    private void verifyOtp() {
        String otpText = mEditEnterOtp.getText().toString();
        if (TextUtils.isEmpty(otpText)) {
            Utilities.showToastMessage(getActivity(), "Please Enter Otp");
        } else {
            String url = ApplicationURL.verifyGateOtpUrl + id + "&otp=" + otpText + "&token=" + mLockatedPreferences.getLockatedToken();
            Log.e("verify", "url" + url);
            requestOtpUrl(url);
        }
    }*/

    private void requestOtpUrl(String url) {
        if (ConnectionDetector.isConnectedToInternet(this)) {
            try {
                mQueue = LockatedVolleyRequestQueue.getInstance(this).getRequestQueue();
                LockatedJSONObjectRequest lockatedJSONObjectRequest = new LockatedJSONObjectRequest(Request.Method.GET,
                        url, null, this, this);
                lockatedJSONObjectRequest.setTag(SEND_OTP_TAG);
                mQueue.add(lockatedJSONObjectRequest);
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            Utilities.showToastMessage(this, this.getResources().getString(R.string.internet_connection_error));
        }
    }

    /* private void sendOtp() {
         String url = ApplicationURL.sendOtp + id + "&token=" + mLockatedPreferences.getLockatedToken();
         requestOtpUrl(url);
     }
 */
    @Override
    public void onErrorResponse(VolleyError error) {
        Log.e("error", "" + error);

    }

    @Override
    public void onResponse(JSONObject response) {
        Log.e("response", "" + response);
        mProgressDialog.dismiss();
        if (response.length() > 0 && response != null) {
            if (response.has("id") && response.has("guest_entry_time")) {
                Gson gson = new Gson();
                gatekeeperVisiterData = gson.fromJson(response.toString(), GatekeeperExpected.class);
                getDetailViewData();
            }
        }
    }

    private void updateEntryTime() {
        JSONObject jsonObjectMain = new JSONObject();
        JSONObject jsonObject = new JSONObject();
        Calendar c = Calendar.getInstance();
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
        String formattedDate = df.format(c.getTime());
        Log.e("formattedDate", "" + formattedDate);
        try {
            jsonObject.put("guest_entry_time", formattedDate);
            jsonObjectMain.put("gatekeeper", jsonObject);
            Log.e("jsonObjectMain", "" + jsonObjectMain);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        String url = ApplicationURL.UpdateGateKeeper + id + ".json?token=" + mLockatedPreferences.getLockatedToken();
        request(jsonObjectMain, url);
    }

    private void requestGetApi(String url) {
       /* mProgressDialog = ProgressDialog.show(getActivity(), "", "Please Wait...", false);
        mProgressDialog.show();
        Log.e("mProgressDialog","start");*/
        if (ConnectionDetector.isConnectedToInternet(this)) {
            LockatedVolleyRequestQueue lockatedVolleyRequestQueue = LockatedVolleyRequestQueue.getInstance(this);
            lockatedVolleyRequestQueue.sendRequest(CREATE_TAG, Request.Method.GET, url, null, this, this);
        } else {
            Utilities.showToastMessage(this, this.getResources().getString(R.string.internet_connection_error));
        }
    }

    private void request(JSONObject jsonObjectMain, String url) {
        if (!this.isFinishing()) {
            if (ConnectionDetector.isConnectedToInternet(this)) {
                mProgressDialog = ProgressDialog.show(this, "", "Please Wait...", false);

                //mProgressDialog.show();
                try {
                    mQueue = LockatedVolleyRequestQueue.getInstance(this).getRequestQueue();
                    LockatedJSONObjectRequest lockatedJSONObjectRequest = new LockatedJSONObjectRequest(Request.Method.PUT,
                            url, jsonObjectMain, this, this);
                    lockatedJSONObjectRequest.setTag(REQUEST_TAG);
                    mQueue.add(lockatedJSONObjectRequest);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else {
                Utilities.showToastMessage(this, this.getResources().getString(R.string.internet_connection_error));
            }
        }
    }

    private class ImageAdapterDetail extends PagerAdapter {
        private final ArrayList<Document> documents;
        private final Context context;
        private final LayoutInflater mLayoutInflater;

        public ImageAdapterDetail(Context context, ArrayList<Document> documents, boolean b) {
            this.documents = documents;
            this.context = context;
            mLayoutInflater = LayoutInflater.from(context);
        }

        @Override
        public int getCount() {
            return documents.size();
        }

        @Override
        public boolean isViewFromObject(View view, Object object) {
            return view == object;
        }

        @Override
        public Object instantiateItem(ViewGroup container, final int position) {
            View itemView = mLayoutInflater.inflate(R.layout.multiple_image, container, false);
            ImageView imageView = (ImageView) itemView.findViewById(R.id.mImageEdit);
            String strImage = documents.get(position).getDocument();
            Picasso.with(context).load(strImage).fit().placeholder(R.drawable.loading).into(imageView);

            container.addView(itemView);

            return itemView;
        }

        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            container.removeView((LinearLayout) object);
        }

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home: {
                onBackPressed();
                return true;
            }
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(this, CrmVisitorActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
        finish();
    }
}
