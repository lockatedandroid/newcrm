package com.android.lockated.crm.fragment.myzone;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;

import com.android.lockated.R;
import com.android.lockated.crm.activity.MySocietyActivity;
import com.android.lockated.information.AccountController;
import com.android.lockated.model.AccountData;
import com.android.lockated.network.ConnectionDetector;
import com.android.lockated.preferences.LockatedPreferences;
import com.android.lockated.request.LockatedJSONObjectRequest;
import com.android.lockated.request.LockatedRequestError;
import com.android.lockated.request.LockatedVolleyRequestQueue;
import com.android.lockated.utils.ApplicationURL;
import com.android.lockated.utils.LockatedConfig;
import com.android.lockated.utils.MarshMallowPermission;
import com.android.lockated.utils.ShowImage;
import com.android.lockated.utils.Utilities;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

public class CreateComplaints extends Fragment implements AdapterView.OnItemSelectedListener, View.OnClickListener, CompoundButton.OnCheckedChangeListener,
        RadioGroup.OnCheckedChangeListener, Response.Listener, Response.ErrorListener {

    private View CreateHelpDeskView;
    private RadioGroup mRadioGroup;
    private RadioButton mPresonalRadioButton;
    private RadioButton mSocityRadioButton;
    private Spinner mSpinnerCtegory;
    private EditText mEditTitle;
    private EditText mDescription;
    private TextView mSubmit;
    private CheckBox mCheckUrgent;
    int societyId;
    int strCheckedBox = 0;
    private String strTitle;
    private String strDescription;
    private String strIssueType;
    private ImageView mComplaintImageView;
    private ImageView mImageAttachment;
    boolean imageSet = false;
    private String mCurrentPhotoPath, encodedImage;
    private int strCatrgory;
    private String strCreatedBy;
    ArrayAdapter<String> arrayadapter;
    JSONArray statusArray;
    JSONObject SpinnerJsonObject;
    private RequestQueue mQueue;
    ProgressDialog mProgressDialog;
    /*private ProgressBar mProgressBarView;*/
    private LockatedPreferences mLockatedPreferences;
    AccountController accountController;
    ArrayList<AccountData> accountDataArrayList;
    MarshMallowPermission marshMallowPermission;
    public static final String REQUEST_TAG = "CreateComplaints";
    public static final String CREATE = "CreateComplaints";

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        CreateHelpDeskView = inflater.inflate(R.layout.fragment_create_help_desk, container, false);
        init();
        getSpinnerCategory();
        return CreateHelpDeskView;
    }

    private void getSpinnerCategory() {

        mLockatedPreferences = new LockatedPreferences(getActivity());
        mProgressDialog = ProgressDialog.show(getActivity(), "", "Please Wait...", false);
        mProgressDialog.show();
        if (ConnectionDetector.isConnectedToInternet(getActivity())) {
            mQueue = LockatedVolleyRequestQueue.getInstance(getActivity()).getRequestQueue();
            String url = ApplicationURL.getHelpDeskCatecory + mLockatedPreferences.getLockatedToken();
            LockatedVolleyRequestQueue lockatedVolleyRequestQueue = LockatedVolleyRequestQueue.getInstance(getActivity());
            lockatedVolleyRequestQueue.sendRequest(CREATE, Request.Method.GET, url,
                    null, new Response.Listener<String>() {

                        @Override
                        public void onResponse(String response) {
                            mProgressDialog.dismiss();
                            JSONObject jsonObject = null;
                            try {
                                jsonObject = new JSONObject(response);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                            if (jsonObject != null && jsonObject.length() > 0) {
                                try {
                                    SpinnerJsonObject = jsonObject.getJSONObject("complaint_constant");
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                                if (SpinnerJsonObject.has("category_type")) {
                                    try {
                                        statusArray = SpinnerJsonObject.getJSONArray("category_type");
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }
                                    arrayadapter = new ArrayAdapter<>(getContext(), R.layout.support_simple_spinner_dropdown_item);
                                    try {
                                        for (int i = 0; i < SpinnerJsonObject.getJSONArray("category_type").length(); i++) {
                                            arrayadapter.add(statusArray.getString(i));
                                        }
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }
                                    mSpinnerCtegory.setAdapter(arrayadapter);
                                }
                            } else {
                                Utilities.showToastMessage(getActivity(), "Nothing TO Display");
                            }
                        }
                    }, new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            mProgressDialog.dismiss();
                            if (getActivity() != null) {
                                LockatedRequestError.onRequestError(getActivity(), error);
                            }
                        }
                    }, null);

        } else {
            Utilities.showToastMessage(getActivity(), getActivity().getResources().getString(R.string.internet_connection_error));
        }
    }

    private void init() {
        mLockatedPreferences = new LockatedPreferences(getActivity());
        marshMallowPermission = new MarshMallowPermission(getActivity());
        /*mProgressBarView = (ProgressBar) CreateHelpDeskView.findViewById(R.id.mProgressBarView);*/
        mRadioGroup = (RadioGroup) CreateHelpDeskView.findViewById(R.id.mRadioGroup);
        mPresonalRadioButton = (RadioButton) CreateHelpDeskView.findViewById(R.id.mPresonalRadioButton);
        mSocityRadioButton = (RadioButton) CreateHelpDeskView.findViewById(R.id.mSocityRadioButton);
        mSpinnerCtegory = (Spinner) CreateHelpDeskView.findViewById(R.id.mSpinnerCtegory);
        mEditTitle = (EditText) CreateHelpDeskView.findViewById(R.id.mEditTitle);
        mDescription = (EditText) CreateHelpDeskView.findViewById(R.id.mDescription);
        mSubmit = (TextView) CreateHelpDeskView.findViewById(R.id.mSubmit);
        mComplaintImageView = (ImageView) CreateHelpDeskView.findViewById(R.id.mComplaintImageView);
        mImageAttachment = (ImageView) CreateHelpDeskView.findViewById(R.id.mImageAttachment);
        mCheckUrgent = (CheckBox) CreateHelpDeskView.findViewById(R.id.mCheckUrgent);
        //  mSpinnerCtegory.setAdapter(arrayadapter);
        mSpinnerCtegory.setOnItemSelectedListener(this);
        mSubmit.setOnClickListener(this);
        mCheckUrgent.setOnCheckedChangeListener(this);
        mRadioGroup.setOnCheckedChangeListener(this);
        mImageAttachment.setOnClickListener(this);
        mComplaintImageView.setVisibility(View.GONE);
        mComplaintImageView.setOnClickListener(this);
        mComplaintImageView.setDrawingCacheEnabled(true);
        mComplaintImageView.buildDrawingCache();
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        strCatrgory = parent.getSelectedItemPosition();
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.mSubmit:
                SubmiButtonClicked();
                break;
            case R.id.mComplaintImageView:
                Intent displayImage = new Intent(getActivity(), ShowImage.class);
                displayImage.putExtra("imagePathString", mCurrentPhotoPath);
                getActivity().startActivity(displayImage);
                break;
            case R.id.mImageAttachment:
                if (!imageSet) {
                    selectImage();
                } else {
                    selectImageAction();
                }
                break;
        }
    }

    private void selectImage() {
        final CharSequence[] options = {"Camera", "Gallery"};
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle("Add Attachment!");
        builder.setItems(options, new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int item) {
                if (options[item].equals("Camera")) {
                    checkCameraPermission();
                } else if (options[item].equals("Gallery")) {
                    checkStoragePermission();
                }
            }

        });

        builder.show();

    }

    private void selectImageAction() {
        final CharSequence[] options = {"View Image", "Change Image", "Remove Image"};
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setItems(options, new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int item) {
                if (options[item].equals("View Image")) {
                    Intent displayImage = new Intent(getActivity(), ShowImage.class);
                    displayImage.putExtra("imagePathString", mCurrentPhotoPath);
                    getActivity().startActivity(displayImage);
                } else if (options[item].equals("Change Image")) {
                    selectImage();
                } else {
                    imageViewGone();
                    mComplaintImageView.setImageBitmap(null);
                    encodedImage = null;
                    imageSet = false;
                }
            }

        });

        builder.show();

    }

    private void checkCameraPermission() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
                requestPermissions(new String[]{Manifest.permission.CAMERA}, LockatedConfig.REQUEST_CAMERA);
            } else {
                onCameraClicked();
            }
        } else {
            onCameraClicked();
        }
    }

    private void checkStoragePermission() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.WRITE_EXTERNAL_STORAGE)
                    != PackageManager.PERMISSION_GRANTED) {
                requestPermissions(new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, LockatedConfig.REQUEST_STORAGE);
            } else {
                onGalleryClicked();
            }
        } else {
            onGalleryClicked();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        if (requestCode == LockatedConfig.REQUEST_CAMERA) {
            if (grantResults.length == 1 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                onCameraClicked();
            }
        } else if (requestCode == LockatedConfig.REQUEST_STORAGE) {
            if (grantResults.length == 1 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                onGalleryClicked();
            }
        }
    }

    private void onGalleryClicked() {
        Intent i = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(i, LockatedConfig.CHOOSE_IMAGE_REQUEST);
        imageViewVisible();
    }

    private File createImageFile() throws IOException {
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss", Locale.ENGLISH).format(new Date());
        String imageFileName = "JPEG_" + timeStamp + "_";
        File storageDir = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES);
        File image = File.createTempFile(imageFileName, ".jpg", storageDir);
        mCurrentPhotoPath = image.getAbsolutePath();
        return image;
    }

    private void onCameraClicked() {
        imageViewVisible();
        if (!marshMallowPermission.checkPermissionForCamera()) {
            marshMallowPermission.requestPermissionForCamera();
        } else {
            if (!marshMallowPermission.checkPermissionForExternalStorage()) {
                marshMallowPermission.requestPermissionForExternalStorage();
            } else {
                Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                if (takePictureIntent.resolveActivity(getActivity().getPackageManager()) != null) {
                    File photoFile = null;
                    try {
                        photoFile = createImageFile();
                    } catch (IOException ex) {
                        ex.printStackTrace();
                    }

                    if (photoFile != null) {
                        takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(photoFile));
                        startActivityForResult(takePictureIntent, LockatedConfig.REQUEST_TAKE_PHOTO);
                    }
                }
            }
        }
        /*imageViewVisible();*/

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode != getActivity().RESULT_CANCELED) {
            if (requestCode == LockatedConfig.REQUEST_TAKE_PHOTO && resultCode == getActivity().RESULT_OK) {
                setPic();
            } else if (requestCode == LockatedConfig.CHOOSE_IMAGE_REQUEST && resultCode == getActivity().RESULT_OK) {
                Uri selectedImageURI = data.getData();
                mCurrentPhotoPath = getPath(selectedImageURI);
                setPic();
            } else if (requestCode == LockatedConfig.REQUEST_CAMERA_PHOTO && resultCode == getActivity().RESULT_OK) {
                Bitmap thumbnail = (Bitmap) data.getExtras().get("data");
                if (thumbnail != null) {
                    ByteArrayOutputStream bytes = new ByteArrayOutputStream();
                    File destination = new File(Environment.getExternalStorageDirectory(), System.currentTimeMillis() + ".jpg");
                    FileOutputStream fo;
                    try {
                        destination.createNewFile();
                        fo = new FileOutputStream(destination);
                        fo.write(bytes.toByteArray());
                        fo.close();
                    } catch (FileNotFoundException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    imageViewVisible();
                    setCameraPic(thumbnail);
                }
            } else {
                imageViewGone();
            }
        }
    }

    private void setCameraPic(Bitmap bitmap) {
        if (bitmap != null) {
            encodedImage = Utilities.encodeTobase64(bitmap);
            mComplaintImageView.setImageBitmap(bitmap);
            imageSet = true;
        }
    }

    private void setPic() {
        int targetW = mComplaintImageView.getWidth();
        int targetH = mComplaintImageView.getHeight();
        BitmapFactory.Options bmOptions = new BitmapFactory.Options();
        bmOptions.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(mCurrentPhotoPath, bmOptions);
        int photoW = bmOptions.outWidth;
        int photoH = bmOptions.outHeight;
        int scaleFactor = Math.min(photoW / targetW, photoH / targetH);

        bmOptions.inJustDecodeBounds = false;
        bmOptions.inSampleSize = scaleFactor;
        bmOptions.inPurgeable = true;

        Bitmap bitmap = BitmapFactory.decodeFile(mCurrentPhotoPath, bmOptions);
        if (bitmap != null) {
            encodedImage = Utilities.encodeTobase64(bitmap);
            Log.w("encodedImage",""+encodedImage);
            mComplaintImageView.setImageBitmap(bitmap);
            imageSet = true;
        }
    }

    private String getPath(Uri uri) {
        if (uri == null) {
            return null;
        }
        String[] projection = {MediaStore.Images.Media.DATA};
        Cursor cursor = getActivity().getContentResolver().query(uri, projection, null, null, null);
        if (cursor != null) {
            int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
            cursor.moveToFirst();
            return cursor.getString(column_index);
        }
        return uri.getPath();
    }

    private void SubmiButtonClicked() {
        strTitle = mEditTitle.getText().toString();
        strDescription = mDescription.getText().toString();
        accountController = AccountController.getInstance();
        accountDataArrayList = accountController.getmAccountDataList();
        strCreatedBy = mLockatedPreferences.getAccountData().getFirstname() + " " +
                mLockatedPreferences.getAccountData().getLastname();

        societyId = Integer.parseInt(mLockatedPreferences.getSocietyId());
        String flatId = accountDataArrayList.get(0).getmResidenceDataList().get(0).getFlat();

        Calendar c = Calendar.getInstance();
        System.out.println("Current time => " + c.getTime());
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSz", Locale.FRANCE);
        String formattedDate = df.format(c.getTime());
        if ((TextUtils.isEmpty(strTitle))) {
            Utilities.showToastMessage(getActivity(), "Please Fill Tile");

        } else if ((TextUtils.isEmpty(strDescription))) {
            Utilities.showToastMessage(getActivity(), "Please Fill Description");
        } else if (mRadioGroup.getCheckedRadioButtonId() == -1) {
            Utilities.showToastMessage(getActivity(), "Please Fill Issue Type");
        } else if (mSpinnerCtegory.getSelectedItemPosition() == 0) {
            Utilities.showToastMessage(getActivity(), "Please Select Category");
        } else {
            if (ConnectionDetector.isConnectedToInternet(getActivity())) {
                mProgressDialog = ProgressDialog.show(getActivity(), "", "Please Wait...", false);
                mProgressDialog.show();
                JSONObject jsonObject = new JSONObject();
                JSONObject jsonObjectMain = new JSONObject();
                JSONArray jsonArray = new JSONArray();
                try {
                    jsonObject.put("id_society", societyId);
                    jsonObject.put("category_type_id", strCatrgory + 1);
                    jsonObject.put("heading", strTitle);
                    jsonObject.put("text", strDescription);
                    jsonObject.put("issue_type", strIssueType);
                    jsonObject.put("user_society_id", "" + mLockatedPreferences.getUserSocietyId());
                    jsonObject.put("flat_number", flatId);
                    jsonObject.put("posted_by", strCreatedBy);
                    jsonObject.put("is_urgent", strCheckedBox);
                    jsonArray.put(encodedImage);
                    jsonObject.put("documents", jsonArray);
                    jsonObjectMain.put("complaint", jsonObject);
                    Utilities.writeToFile(jsonObjectMain.toString(),"sagar");
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                try {
                    mQueue = LockatedVolleyRequestQueue.getInstance(getActivity()).getRequestQueue();
                    String url = ApplicationURL.addHelpdeskComplain + mLockatedPreferences.getLockatedToken();
                    LockatedJSONObjectRequest lockatedJSONObjectRequest = new LockatedJSONObjectRequest(Request.Method.POST,
                            url, jsonObjectMain, this, this);
                    lockatedJSONObjectRequest.setTag(REQUEST_TAG);
                    lockatedJSONObjectRequest.setRetryPolicy(new DefaultRetryPolicy(0,DefaultRetryPolicy.DEFAULT_MAX_RETRIES,DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                    mQueue.add(lockatedJSONObjectRequest);
                } catch (Exception e) {
                    e.printStackTrace();
                }

            } else {
                Utilities.showToastMessage(getActivity(), getActivity().getResources().getString(R.string.internet_connection_error));
            }

        }
    }

    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        if (isChecked) {
            strCheckedBox = 1;
        }
    }

    @Override
    public void onCheckedChanged(RadioGroup group, int checkedId) {
        if (checkedId == R.id.mPresonalRadioButton) {
            strIssueType = mPresonalRadioButton.getText().toString();
        } else {
            strIssueType = mSocityRadioButton.getText().toString();
        }
    }

    @Override
    public void onResponse(Object response) {
        mProgressDialog.dismiss();
        if (response != null) {
            try {
                JSONObject jsonObject = new JSONObject(response.toString());
                if (jsonObject.has("id")) {
                    Intent backIntent = new Intent(getActivity(), MySocietyActivity.class);
                    backIntent.putExtra("crmItemPosition", 3);
                    backIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(backIntent);
                    getActivity().finish();
                } else {
                    Utilities.showToastMessage(getContext(), "Something Went Wrong");
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void onErrorResponse(VolleyError error) {
        mProgressDialog.dismiss();
        if (getActivity() != null) {
            LockatedRequestError.onRequestError(getActivity(), error);
        }
    }

    private void imageViewVisible() {
        if (mComplaintImageView.getVisibility() == View.GONE) {
            mComplaintImageView.setVisibility(View.VISIBLE);
        }
    }

    private void imageViewGone() {
        if (mComplaintImageView.getVisibility() == View.VISIBLE) {
            mComplaintImageView.setVisibility(View.GONE);
        }
    }
}
