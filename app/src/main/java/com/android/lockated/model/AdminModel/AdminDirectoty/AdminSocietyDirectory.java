
package com.android.lockated.model.AdminModel.AdminDirectoty;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class AdminSocietyDirectory {

    @SerializedName("code")
    @Expose
    private int code;
    @SerializedName("public_directories")
    @Expose
    private List<PublicDirectory> publicDirectories = new ArrayList<PublicDirectory>();

    /**
     * 
     * @return
     *     The code
     */
    public int getCode() {
        return code;
    }

    /**
     * 
     * @param code
     *     The code
     */
    public void setCode(int code) {
        this.code = code;
    }

    /**
     * 
     * @return
     *     The publicDirectories
     */
    public List<PublicDirectory> getPublicDirectories() {
        return publicDirectories;
    }

    /**
     * 
     * @param publicDirectories
     *     The public_directories
     */
    public void setPublicDirectories(List<PublicDirectory> publicDirectories) {
        this.publicDirectories = publicDirectories;
    }

}
