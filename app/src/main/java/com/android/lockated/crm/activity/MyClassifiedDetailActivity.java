package com.android.lockated.crm.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.lockated.R;
import com.android.lockated.utils.ShowImage;
import com.squareup.picasso.Picasso;

public class MyClassifiedDetailActivity extends AppCompatActivity implements View.OnClickListener {
    String strImage;
    private ImageView mImageView;
    private TextView mThingType, mThingto, mRate, mExpiryDate, mDescription, mStatus, categoryValue, mClose, mMobileNumber, mEmailId, postedBy;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.my_classifieds_detail);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(true);
        getSupportActionBar().setTitle(R.string.myClassifieds);
        init();
        getDetails();
    }

    private void init() {
        mThingType = (TextView) findViewById(R.id.mThingType);
        mThingto = (TextView) findViewById(R.id.mThingto);
        mRate = (TextView) findViewById(R.id.mRate);
        mExpiryDate = (TextView) findViewById(R.id.mExpiryDate);
        mDescription = (TextView) findViewById(R.id.mDescription);
        mStatus = (TextView) findViewById(R.id.mStatus);
        mMobileNumber = (TextView) findViewById(R.id.mMobileNumber);
        mEmailId = (TextView) findViewById(R.id.mEmailId);
        postedBy = (TextView) findViewById(R.id.postedBy);
        mImageView = (ImageView) findViewById(R.id.mImageView);
        mImageView.setOnClickListener(this);
        categoryValue = (TextView) findViewById(R.id.categoryValue);
        mClose = (TextView) findViewById(R.id.mClose);
        mClose.setOnClickListener(this);
    }

    private void getDetails() {
        if (getIntent().getExtras() != null) {
            String spid = getIntent().getExtras().getString("pid");
            mThingType.setText(getIntent().getExtras().getString("strThing"));
            mThingto.setText(getIntent().getExtras().getString("strThingTO"));
            mRate.setText(getIntent().getExtras().getString("strRate"));
            mExpiryDate.setText((getIntent().getExtras().getString("strDate")));
            mDescription.setText(getIntent().getExtras().getString("strDescription"));
            categoryValue.setText(getIntent().getExtras().getString("categoryValue"));
            String status = (getIntent().getExtras().getString("Status"));
            if (status == null) {
                mStatus.setText("Pending");
            } else {
                mStatus.setText(getIntent().getExtras().getString("Status"));
            }
            strImage = getIntent().getExtras().getString("ImagePicaso");
            Picasso.with(this).load(strImage).fit().into(mImageView);

            postedBy.setText(getIntent().getExtras().getString("name"));
            mMobileNumber.setText(getIntent().getExtras().getString("mobile"));
            mMobileNumber.setText(getIntent().getExtras().getString("mobile"));
            mEmailId.setText(getIntent().getExtras().getString("email"));
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();

        if (id == android.R.id.home) {
            finish();
            return true;
        } else {
            return false;
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.mClose:
                finish();
                break;
            case R.id.mImageView:
                Intent intent = new Intent(getApplicationContext(), ShowImage.class);
                intent.putExtra("imageUrlString", strImage);
                startActivity(intent);
                break;
        }
    }
}
