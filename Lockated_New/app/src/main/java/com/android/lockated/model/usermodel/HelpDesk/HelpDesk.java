
package com.android.lockated.model.usermodel.HelpDesk;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class HelpDesk {

    @SerializedName("complaints")
    @Expose
    private ArrayList<Complaint> complaints = new ArrayList<Complaint>();

    /**
     * 
     * @return
     *     The complaints
     */
    public ArrayList<Complaint> getComplaints() {
        return complaints;
    }

    /**
     * 
     * @param complaints
     *     The complaints
     */
    public void setComplaints(ArrayList<Complaint> complaints) {
        this.complaints = complaints;
    }

}
