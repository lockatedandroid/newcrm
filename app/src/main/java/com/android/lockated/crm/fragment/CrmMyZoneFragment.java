package com.android.lockated.crm.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import com.android.lockated.R;
import com.android.lockated.crm.activity.MyZoneActivity;
import com.android.lockated.crm.adapters.CommanListViewAdapter;
import com.android.lockated.utils.Utilities;


public class CrmMyZoneFragment extends Fragment implements AdapterView.OnItemClickListener {
    ListView myZoneList;
    String[] nameList = {/*"Visitor Corner", */"Facilities", "Directory", /*"Classifieds"*/};
    CommanListViewAdapter commanListViewAdapter;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View myZoneView = inflater.inflate(R.layout.comman_listview, container, false);
        Utilities.ladooIntegration(getActivity(), "My Zone List");
        myZoneList = (ListView) myZoneView.findViewById(R.id.noticeList);
        commanListViewAdapter = new CommanListViewAdapter(getActivity(), nameList);
        myZoneList.setAdapter(commanListViewAdapter);
        myZoneList.setOnItemClickListener(this);

        return myZoneView;
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        Intent intent = new Intent(getActivity(), MyZoneActivity.class);
        intent.putExtra("MyZonePosition", position);
        startActivity(intent);

    }
}