
package com.android.lockated.model.usermodel.Facilities;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Amenity {

    @SerializedName("id")
    @Expose
    private int id;
    @SerializedName("society_id")
    @Expose
    private int societyId;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("description")
    @Expose
    private String description;
    @SerializedName("chargeable")
    @Expose
    private int chargeable;
    @SerializedName("cost")
    @Expose
    private int cost;
    @SerializedName("cost_type")
    @Expose
    private String costType;
    @SerializedName("active")
    @Expose
    private int active;
    @SerializedName("url")
    @Expose
    private String url;

    /**
     * 
     * @return
     *     The id
     */
    public int getId() {
        return id;
    }

    /**
     * 
     * @param id
     *     The id
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * 
     * @return
     *     The societyId
     */
    public int getSocietyId() {
        return societyId;
    }

    /**
     * 
     * @param societyId
     *     The society_id
     */
    public void setSocietyId(int societyId) {
        this.societyId = societyId;
    }

    /**
     * 
     * @return
     *     The name
     */
    public String getName() {
        return name;
    }

    /**
     * 
     * @param name
     *     The name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * 
     * @return
     *     The description
     */
    public String getDescription() {
        return description;
    }

    /**
     * 
     * @param description
     *     The description
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * 
     * @return
     *     The chargeable
     */
    public int getChargeable() {
        return chargeable;
    }

    /**
     * 
     * @param chargeable
     *     The chargeable
     */
    public void setChargeable(int chargeable) {
        this.chargeable = chargeable;
    }

    /**
     * 
     * @return
     *     The cost
     */
    public int getCost() {
        return cost;
    }

    /**
     * 
     * @param cost
     *     The cost
     */
    public void setCost(int cost) {
        this.cost = cost;
    }

    /**
     * 
     * @return
     *     The costType
     */
    public String getCostType() {
        return costType;
    }

    /**
     * 
     * @param costType
     *     The cost_type
     */
    public void setCostType(String costType) {
        this.costType = costType;
    }

    /**
     * 
     * @return
     *     The active
     */
    public int getActive() {
        return active;
    }

    /**
     * 
     * @param active
     *     The active
     */
    public void setActive(int active) {
        this.active = active;
    }

    /**
     * 
     * @return
     *     The url
     */
    public String getUrl() {
        return url;
    }

    /**
     * 
     * @param url
     *     The url
     */
    public void setUrl(String url) {
        this.url = url;
    }

}
