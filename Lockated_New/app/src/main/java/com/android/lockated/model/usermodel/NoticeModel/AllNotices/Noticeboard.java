
package com.android.lockated.model.usermodel.NoticeModel.AllNotices;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class Noticeboard {

    @SerializedName("date")
    @Expose
    private String date;
    @SerializedName("notices")
    @Expose
    private ArrayList<Notice> notices = new ArrayList<Notice>();

    /**
     * 
     * @return
     *     The date
     */
    public String getDate() {
        return date;
    }

    /**
     * 
     * @param date
     *     The date
     */
    public void setDate(String date) {
        this.date = date;
    }

    /**
     * 
     * @return
     *     The notices
     */
    public ArrayList<Notice> getNotices() {
        return notices;
    }

    /**
     * 
     * @param notices
     *     The notices
     */
    public void setNotices(ArrayList<Notice> notices) {
        this.notices = notices;
    }

}
