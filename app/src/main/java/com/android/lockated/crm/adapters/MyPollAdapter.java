package com.android.lockated.crm.adapters;

import android.content.Context;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.lockated.R;
import com.android.lockated.information.AccountController;
import com.android.lockated.model.AccountData;
import com.android.lockated.model.usermodel.Polls.MyPoll;
import com.android.lockated.utils.Utilities;

import org.json.JSONArray;

import java.util.ArrayList;

public class MyPollAdapter extends RecyclerView.Adapter<MyPollAdapter.MyPollsViewHolder> {
    public static final String REQUEST_TAG = "MyPollAdapter";
    private final ArrayList<MyPoll> myPolls;
    Context contextMain;
    JSONArray jsonArray;
    String strStatus;
    FragmentManager childFragmentManager;
    AccountController accountController;
    ArrayList<AccountData> datas;

    public MyPollAdapter(Context context, ArrayList<MyPoll> myPolls, FragmentManager childFragmentManager) {
        this.contextMain = context;
        this.myPolls = myPolls;
        this.childFragmentManager = childFragmentManager;
        accountController = AccountController.getInstance();
        datas = accountController.getmAccountDataList();
    }

    @Override
    public MyPollsViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(contextMain).inflate(R.layout.polls_my_parent_view, parent, false);
        MyPollsViewHolder pvh = new MyPollsViewHolder(v);
        return pvh;
    }

    @Override
    public void onBindViewHolder(MyPollsViewHolder holder, int position) {

        if (myPolls.get(position).getSubject() != null) {
            holder.subject.setText(myPolls.get(position).getSubject().toString());
        } else {
            holder.subject.setText("No Subject");
        }
        if ((myPolls.get(position).getPollUser().getFirstname() != null) && (myPolls.get(position).getPollUser().getLastname() != null)) {
            String fname = myPolls.get(position).getPollUser().getFirstname() + "" + myPolls.get(position).getPollUser().getLastname();
            holder.mPostedBy.setText(fname);

        } else {
            holder.mPostedBy.setText("No Name");
        }
       /* if (myPolls.get(position).getFlat() != null) {

            holder.mFlatNo.setText(myPolls.get(position).getFlat());
        } else {
            holder.mFlatNo.setText("No Flat");
        }*/
        if (myPolls.get(position).getCreatedAt() != null) {

            holder.mCreatedAt.setText(Utilities.ddMonTimeFormat(myPolls.get(position).getCreatedAt()));
        } else {
            holder.mCreatedAt.setText("No Date");
        }
        if (myPolls.get(position).getStart() != null) {

            holder.mStartDateTime.setText(Utilities.ddMonTimeFormat(myPolls.get(position).getStart()));
        } else {
            holder.mStartDateTime.setText("No Date");
        }
        if (myPolls.get(position).getEnd() != null) {
            holder.mEndDateTime.setText(Utilities.ddMonTimeFormat(myPolls.get(position).getEnd()));
        } else {
            holder.mEndDateTime.setText("No Date");
        }
        if (myPolls.get(position).getStatus() != null) {
            holder.mStatus.setText((myPolls.get(position).getStatus()));
        } else {
            holder.mStatus.setText("No Status");
        }
        if (myPolls.get(position).getPollOptions().size() == 1) {
            holder.secondChoice.setVisibility(View.GONE);
            holder.thirdChoice.setVisibility(View.GONE);
            holder.fourthChoice.setVisibility(View.GONE);
            // holder.secondOption.setVisibility(View.GONE);
            holder.firstChoice.setText(choiceNumber(contextMain, 1, myPolls.get(position).getPollOptions().
                    get(0).getName()));
        } else if (myPolls.get(position).getPollOptions().size() == 2) {
            holder.thirdChoice.setVisibility(View.GONE);
            holder.fourthChoice.setVisibility(View.GONE);
            holder.firstChoice.setText(choiceNumber(contextMain, 1, myPolls.get(position).getPollOptions().
                    get(0).getName()));
            holder.secondChoice.setText(choiceNumber(contextMain, 2, myPolls.get(position).getPollOptions().
                    get(1).getName()));
        } else if (myPolls.get(position).getPollOptions().size() == 3) {
            holder.fourthChoice.setVisibility(View.GONE);
            holder.firstChoice.setText(choiceNumber(contextMain, 1, myPolls.get(position).getPollOptions().
                    get(0).getName()));
            holder.secondChoice.setText(choiceNumber(contextMain, 2, myPolls.get(position).getPollOptions().
                    get(1).getName()));
            holder.thirdChoice.setText(choiceNumber(contextMain, 3, myPolls.get(position).getPollOptions().
                    get(2).getName()));
        } else if (myPolls.get(position).getPollOptions().size() == 4) {

            holder.firstChoice.setText(choiceNumber(contextMain, 1, myPolls.get(position).getPollOptions().
                    get(0).getName()));
            holder.secondChoice.setText(choiceNumber(contextMain, 2, myPolls.get(position).getPollOptions().
                    get(1).getName()));
            holder.thirdChoice.setText(choiceNumber(contextMain, 3, myPolls.get(position).getPollOptions().
                    get(2).getName()));
            holder.fourthChoice.setText(choiceNumber(contextMain, 4, myPolls.get(position).getPollOptions().
                    get(3).getName()));

        } else if (myPolls.get(position).getPollOptions().size() == 0) {

        }
    }

    public String choiceNumber(Context context, int number, String name) {
        if (number == 1)
            return context.getResources().getString(R.string.one) + name;
        else if (number == 2)
            return context.getResources().getString(R.string.two) + name;
        else if (number == 3)
            return context.getResources().getString(R.string.three) + name;
        else if (number == 4)
            return context.getResources().getString(R.string.four) + name;
        else
            return context.getResources().getString(R.string.one) + name;
    }

    @Override
    public int getItemCount() {
        return myPolls.size();
        //jsonArray.length();
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }

    public class MyPollsViewHolder extends RecyclerView.ViewHolder {

        TextView subject, firstChoice, secondChoice, thirdChoice, fourthChoice, firstOption, mStartDateTime, mEndDateTime,
                secondOption, alreadyVotedText;
        TextView mPostedBy, mFlatNo, mCreatedAt, mStatus;
        LinearLayout options, voted;

        public MyPollsViewHolder(View itemView) {
            super(itemView);
            mPostedBy = (TextView) itemView.findViewById(R.id.mPostedBy);
            // mFlatNo = (TextView) itemView.findViewById(R.id.mFlatNo);
            mCreatedAt = (TextView) itemView.findViewById(R.id.mCreatedAt);
            mStatus = (TextView) itemView.findViewById(R.id.mStatus);
            subject = (TextView) itemView.findViewById(R.id.subjectText);
            mStartDateTime = (TextView) itemView.findViewById(R.id.mStartDateTime);
            mEndDateTime = (TextView) itemView.findViewById(R.id.mEndDateTime);
            firstChoice = (TextView) itemView.findViewById(R.id.firstChoice);
            secondChoice = (TextView) itemView.findViewById(R.id.secondChoice);
            thirdChoice = (TextView) itemView.findViewById(R.id.thirdChoice);
            fourthChoice = (TextView) itemView.findViewById(R.id.fourthChoice);
            firstOption = (TextView) itemView.findViewById(R.id.firstOption);
            secondOption = (TextView) itemView.findViewById(R.id.secondOption);
            // alreadyVotedText = (TextView) itemView.findViewById(R.id.alreadyVoted);
            options = (LinearLayout) itemView.findViewById(R.id.optionsLinear);
            // voted = (LinearLayout) itemView.findViewById(R.id.alreadyVotedLinear);
        }
    }

}