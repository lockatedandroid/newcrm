package com.android.lockated.categories.medicine;

import android.support.v7.app.AppCompatActivity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;

import android.util.Log;

import android.view.MenuItem;

import com.android.lockated.LockatedApplication;
import com.android.lockated.R;
import com.android.lockated.categories.medicine.fragment.MedicineDetailFragment;
import com.android.lockated.categories.medicine.fragment.MedicineFragment;
import com.android.lockated.checkout.ServiceCongratulationFragment;
import com.android.lockated.information.ServiceController;

public class MedicineActivity extends AppCompatActivity {

    private ServiceController mServiceController;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_vendor);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        mServiceController = ServiceController.getInstance();
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(true);
        getSupportActionBar().setTitle(((LockatedApplication) getApplicationContext()).getHeaderName());

        try {
            //Intent intent = getIntent();
            final Intent intent = getIntent();
            final String myScheme = intent.getScheme();
            final Bundle myBundle = intent.getExtras();
            final boolean inContestKey;
            /*if (myBundle != null) {
                inContestKey = myBundle.containsKey("inContest");
            }*/
            final Uri myURI = intent.getData();
            /*final String value;
            if (myURI != null) {
                value = myURI.getQueryParameter("inContest");
            }*/
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (savedInstanceState == null) {
            MedicineFragment medicineFragment = new MedicineFragment();
            getSupportFragmentManager().beginTransaction().add(R.id.mVendorContainer, medicineFragment).commit();
        }
    }

    public void addNewFragmentToStack() {
        MedicineDetailFragment medicineDetailFragment = new MedicineDetailFragment();
        getSupportFragmentManager().beginTransaction().replace(R.id.mVendorContainer, medicineDetailFragment).addToBackStack("ServiceDetailFragment").commit();
    }

    public void addCongratulationScreen(String mOrderNumber) {
        ServiceCongratulationFragment congratulationFragment = new ServiceCongratulationFragment();
        congratulationFragment.setOrderId(mOrderNumber);
        getSupportFragmentManager().beginTransaction().replace(R.id.mVendorContainer, congratulationFragment).commit();
    }

    @Override
    public void onBackPressed() {
        Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.mVendorContainer);
        if (fragment instanceof ServiceCongratulationFragment) {
            ((ServiceCongratulationFragment) fragment).getScreenDetail();
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home: {
                super.onBackPressed();
                return true;
            }
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putInt("setCategoryId", ((LockatedApplication) this.getApplicationContext()).getCategoryId());
        outState.putInt("getCategoryIdTwo", ((LockatedApplication) this.getApplicationContext()).getCategoryIdTwo());
    }

    @Override
    public void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        if (savedInstanceState != null) {
            ((LockatedApplication) this.getApplicationContext()).setCategoryId(savedInstanceState.getInt("setCategoryId"));
            ((LockatedApplication) this.getApplicationContext()).setCategoryIdTwo(savedInstanceState.getInt("getCategoryIdTwo"));
            mServiceController.setCategoryId(((LockatedApplication) this.getApplicationContext()).getCategoryIdTwo());
            mServiceController.setSubCategoryId(((LockatedApplication) this.getApplicationContext()).getCategoryId());
        }
    }
}