package com.android.lockated.crm.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.lockated.Interfaces.IRecyclerItemClickListener;
import com.android.lockated.R;
import com.android.lockated.holder.RecyclerViewHolder;
import com.android.lockated.model.userGroups.UserGroup;
import com.android.lockated.network.ConnectionDetector;
import com.android.lockated.preferences.LockatedPreferences;
import com.android.lockated.request.LockatedJSONObjectRequest;
import com.android.lockated.request.LockatedRequestError;
import com.android.lockated.request.LockatedVolleyRequestQueue;
import com.android.lockated.utils.ApplicationURL;
import com.android.lockated.utils.Utilities;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;

import org.json.JSONObject;

import java.util.ArrayList;

public class GroupAdapter extends RecyclerView.Adapter<RecyclerViewHolder> {

    Context context;
    boolean showControlFunctions;
    ArrayList<UserGroup> userGroupArrayList;
    private IRecyclerItemClickListener mIRecyclerItemClickListener;

    public GroupAdapter(Context context, ArrayList<UserGroup> userGroupArrayList,
                        IRecyclerItemClickListener mIRecyclerItemClickListener, boolean showControlFunctions) {
        this.context = context;
        this.showControlFunctions = showControlFunctions;
        this.userGroupArrayList = userGroupArrayList;
        this.mIRecyclerItemClickListener = mIRecyclerItemClickListener;
    }

    @Override
    public RecyclerViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(context).inflate(R.layout.group_child_row, parent, false);
        return new RecyclerViewHolder(v, mIRecyclerItemClickListener);
    }

    @Override
    public void onBindViewHolder(RecyclerViewHolder holder, int position) {
        holder.onGroupViewHolder(userGroupArrayList.get(position), showControlFunctions);
    }

    @Override
    public int getItemCount() {
        return userGroupArrayList.size();
    }

}
