package com.android.lockated.crm.fragment.notices;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.RelativeLayout;

import com.android.lockated.R;
import com.android.lockated.crm.activity.MySocietyActivity;
import com.android.lockated.crm.adapters.CrmNoticeBoardListAdapter;
import com.android.lockated.preferences.LockatedPreferences;


public class CrmNoticeBoardFragment extends Fragment implements AdapterView.OnItemClickListener {

    ListView noticeBoardList;
    RelativeLayout relativeLayout;
    private Boolean isFabOpen = false;
    CrmNoticeBoardListAdapter crmNoticeBoardListAdapter;
    LockatedPreferences mLockatedPreference;
    private Animation fab_open, fab_close, rotate_forward, rotate_backward;
    String[] nameList = {"Notice", "Events", "Polls", "Directory"};

    public CrmNoticeBoardFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View noticeBoardView = inflater.inflate(R.layout.fragment_notice_board, container, false);
        mLockatedPreference = new LockatedPreferences(getActivity());
        relativeLayout = (RelativeLayout) noticeBoardView.findViewById(R.id.relativeMain);
        noticeBoardList = (ListView) noticeBoardView.findViewById(R.id.boardList);
        fab_open = AnimationUtils.loadAnimation(getActivity(), R.anim.fab_open);
        fab_close = AnimationUtils.loadAnimation(getActivity(), R.anim.fab_close);
        rotate_forward = AnimationUtils.loadAnimation(getActivity(), R.anim.rotate_forward);
        rotate_backward = AnimationUtils.loadAnimation(getActivity(), R.anim.rotate_backward);

        crmNoticeBoardListAdapter = new CrmNoticeBoardListAdapter(getActivity(), nameList);
        noticeBoardList.setAdapter(crmNoticeBoardListAdapter);
        noticeBoardList.setOnItemClickListener(this);

        return noticeBoardView;
    }

    @Override
    public void onResume() {
        super.onResume();
        isFabOpen = true;
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        Intent intent = new Intent(getActivity(), MySocietyActivity.class);
        intent.putExtra("crmItemPosition", position);
        startActivity(intent);
    }

}