package com.android.lockated.crm.adapters;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.android.lockated.R;
import com.android.lockated.crm.activity.FacilitiesActivity;
import com.android.lockated.crm.detailView.FacilitiesDetailView;
import com.android.lockated.model.usermodel.Facilities.FacilityHistory.Booked;
import com.android.lockated.network.ConnectionDetector;
import com.android.lockated.preferences.LockatedPreferences;
import com.android.lockated.request.LockatedRequestError;
import com.android.lockated.request.LockatedVolleyRequestQueue;
import com.android.lockated.utils.ApplicationURL;
import com.android.lockated.utils.Utilities;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;

import org.json.JSONObject;

import java.util.ArrayList;


public class FacilitiesHistoryAdapter extends RecyclerView.Adapter<FacilitiesHistoryAdapter.FacilitiesHistoryViewHolder>
        implements View.OnClickListener, Response.Listener<JSONObject>, Response.ErrorListener {

    private final ArrayList<Booked> booked;
    LayoutInflater layoutInflater;
    Context context;
    FragmentManager childFragmentManager;

    public FacilitiesHistoryAdapter(Context context, FragmentManager childFragmentManager, ArrayList<Booked> booked) {
        this.context = context;
        layoutInflater = LayoutInflater.from(context);
        this.booked = booked;
        this.childFragmentManager = childFragmentManager;
    }

    @Override
    public FacilitiesHistoryViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(context).inflate(R.layout.facilities_history_adapter_view, parent, false);
        FacilitiesHistoryViewHolder facilitiesViewHolder = new FacilitiesHistoryViewHolder(v);
        return facilitiesViewHolder;
    }

    @Override
    public void onBindViewHolder(FacilitiesHistoryViewHolder holder, final int position) {
        String facilityStatus = null;
        if (booked.get(position).getAmenity() != null) {
            holder.venueName.setText(booked.get(position).getAmenity().getName());
            String cost = String.valueOf(booked.get(position).getAmenity().getCost());
            holder.costValue.setText(cost);
        } else {
            holder.venueName.setText("No Data");
            holder.costValue.setText("No Data");
        }
        // holder.bookDuration.setText(jsonArray.getJSONObject(position).getString("book_duration"));
        holder.bookPurpose.setText(booked.get(position).getBookPurpose());
        holder.createdDate.setText(Utilities.ddMonYYYYFormat(booked.get(position).getCreatedAt()));
        holder.numberPerson.setText(String.valueOf(booked.get(position).getPersonNo()));
        /*if (booked.get(position).getApprove() == null) {
            facilityStatus = "PENDING";
            holder.mEdit.setVisibility(View.VISIBLE);
            holder.cancelRequest.setVisibility(View.VISIBLE);
            holder.status.setTextColor(context.getResources().getColor(R.color.primary));
        } else */
        if (booked.get(position).getStatus().equals("Pending")) {
            facilityStatus = "PENDING";
            holder.mEdit.setVisibility(View.VISIBLE);
            holder.cancelRequest.setVisibility(View.VISIBLE);
            holder.status.setTextColor(context.getResources().getColor(R.color.primary));
        } else if (booked.get(position).getStatus().equals("Approved")) {
            facilityStatus = "APPROVED";
            holder.mEdit.setVisibility(View.INVISIBLE);
            holder.cancelRequest.setVisibility(View.VISIBLE);
            holder.status.setTextColor(context.getResources().getColor(R.color.primary));
        } else if (booked.get(position).getStatus().equals("Rejected")) {
            facilityStatus = "REJECTED";
            holder.cancelRequest.setVisibility(View.GONE);
            holder.mEdit.setVisibility(View.GONE);
            holder.status.setTextColor(context.getResources().getColor(R.color.red));
        } else {
            facilityStatus = "PENDING";
            holder.mEdit.setVisibility(View.VISIBLE);
            holder.cancelRequest.setVisibility(View.VISIBLE);
            holder.status.setTextColor(context.getResources().getColor(R.color.primary));
        }
        holder.status.setText(facilityStatus);
        holder.fromDate.setText(Utilities.ddMonYYYYFormat(booked.get(position).getStartdate()));
        holder.toDate.setText(Utilities.ddMonYYYYFormat(booked.get(position).getEnddate()));
        if (facilityStatus.equalsIgnoreCase("Cancelled by User")) {
            holder.cancelRequest.setVisibility(View.GONE);
        } else {
            holder.cancelRequest.setTag(position);
            holder.cancelRequest.setOnClickListener(this);

        }

        holder.mEdit.setTag(position);
        holder.mEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int position = (int) v.getTag();
                EditDetails(position, booked.get(position));
            }
        });
    }

    @Override
    public int getItemCount() {
        return booked.size();
        //return jsonArray.length();
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }

    @Override
    public void onClick(View v) {
        int position;
        position = (int) v.getTag();
        switch (v.getId()) {
            case R.id.cancelTextView:
                cancelRequestApi(context, position);
                break;
           /* case R.id.mEdit:
                Log.e("edit clicked", "");
                EditDetails(position);
                break;
                */
        }
    }

    private void EditDetails(int position, Booked booked) {
        //jsonArray.getJSONObject(position)
        DialogFragment dialog = new FacilitiesDetailView();
        Bundle args = new Bundle();
        //Log.e("purpose purpose", "" + jsonObject.getString("book_purpose"));
        args.putString("id", "" + booked.getId());
        args.putString("facility_id", "" + booked.getFacilityId());
        args.putString("valueId", "1");
        if (booked.getAmenity() != null) {
            args.putString("cost", String.valueOf(booked.getAmenity().getCost()));
            args.putString("name", booked.getAmenity().getName());
        } else {
            args.putString("cost", "No Data");
            args.putString("name", "No Data");
        }
        //args.putString("cost_type", jsonObject.getString("cost_type"));
        args.putString("book_purpose", booked.getBookPurpose());
        args.putString("person_no", "" + booked.getPersonNo());
        args.putString("startdate", booked.getStartdate());
        args.putString("enddate", booked.getEnddate());
        //Log.e("Bundle arguments", "" + args.toString());
        dialog.setArguments(args);
        dialog.show(childFragmentManager, "dialog");
    }

    private void cancelRequestApi(Context context, int position) {
        if (ConnectionDetector.isConnectedToInternet(context)) {
            String REQUEST_FACILITIES = "CancelFacilities";
            LockatedPreferences mLockatedPreferences = new LockatedPreferences(context);
            String url = ApplicationURL.cancelFacilitiesUrl + "" + booked.get(position).getId()
                    + "&approve=0&comment=not%20available&token=" + mLockatedPreferences.getLockatedToken();
            LockatedVolleyRequestQueue mLockatedVolleyRequestQueue = LockatedVolleyRequestQueue.getInstance(context);
            mLockatedVolleyRequestQueue.sendRequest(REQUEST_FACILITIES, Request.Method.GET, url, null, this, this);
        } else {
            Utilities.showToastMessage(context, context.getResources().getString(R.string.internet_connection_error));
        }
    }

    public class FacilitiesHistoryViewHolder extends RecyclerView.ViewHolder {

        TextView venueName, costValue, status, bookDuration, bookPurpose, createdDate, numberPerson,
                cancelRequest, fromDate, toDate;
        TextView mEdit;

        public FacilitiesHistoryViewHolder(View itemView) {
            super(itemView);
            venueName = (TextView) itemView.findViewById(R.id.venuTxt);
            costValue = (TextView) itemView.findViewById(R.id.costValue);
            status = (TextView) itemView.findViewById(R.id.statusValue);
            bookDuration = (TextView) itemView.findViewById(R.id.bookDuration);
            bookPurpose = (TextView) itemView.findViewById(R.id.bookPurpose);
            createdDate = (TextView) itemView.findViewById(R.id.createdDate);
            numberPerson = (TextView) itemView.findViewById(R.id.numberPerson);
            fromDate = (TextView) itemView.findViewById(R.id.fromDate);
            toDate = (TextView) itemView.findViewById(R.id.toDate);
            cancelRequest = (TextView) itemView.findViewById(R.id.cancelTextView);
            mEdit = (TextView) itemView.findViewById(R.id.mEdit);

        }
    }

    @Override
    public void onResponse(JSONObject response) {
        if (response.has("id")) {
            Intent intent = new Intent(context, FacilitiesActivity.class);
            intent.putExtra("FacilitiesActivity", 1);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            context.startActivity(intent);
        }
    }

    @Override
    public void onErrorResponse(VolleyError error) {
        if (context != null) {
            LockatedRequestError.onRequestError(context, error);
        }
    }

}