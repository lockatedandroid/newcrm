package com.android.lockated.categories.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.android.lockated.categories.vendors.fragment.VendorMenuProductFragment;
import com.android.lockated.categories.vendors.model.VendorProductData;

import java.util.ArrayList;

public class VendorMenuTabAdapter extends FragmentPagerAdapter
{
    private ArrayList<VendorProductData> mVendorProductData;

    public VendorMenuTabAdapter(FragmentManager fm, ArrayList<VendorProductData> tabs)
    {
        super(fm);
        mVendorProductData = tabs;
    }

    @Override
    public Fragment getItem(int position)
    {
        VendorMenuProductFragment vendorMenuProductFragment = new VendorMenuProductFragment();
        vendorMenuProductFragment.setVendorMenuProduct(mVendorProductData.get(position).getProductData());
        return vendorMenuProductFragment;
    }

    @Override
    public int getItemPosition(Object object)
    {
        return super.getItemPosition(object);
    }

    @Override
    public CharSequence getPageTitle(int position)
    {
        return mVendorProductData.get(position).getCatName();
    }

    @Override
    public long getItemId(int position)
    {
        return super.getItemId(position);
    }

    @Override
    public int getCount()
    {
        return mVendorProductData.size();
    }
}
