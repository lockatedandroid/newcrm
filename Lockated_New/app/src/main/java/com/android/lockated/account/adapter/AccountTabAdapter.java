package com.android.lockated.account.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.android.lockated.account.fragment.FamilyDetailFragment;
import com.android.lockated.account.fragment.MyAddressFragment;
import com.android.lockated.account.fragment.MyOrdersFragment;
import com.android.lockated.account.fragment.MyServicesFragment;
import com.android.lockated.account.fragment.MySocietyListFragment;
import com.android.lockated.account.fragment.PersonalInfoFragment;
import com.android.lockated.account.fragment.SocietyDetailFragment;

import java.util.ArrayList;

public class AccountTabAdapter extends FragmentPagerAdapter {
    private ArrayList<String> mAccountTabList;
    String name = "AccountTabAdapter";

    public AccountTabAdapter(FragmentManager fm, ArrayList<String> tabs) {
        super(fm);
        mAccountTabList = tabs;
    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                PersonalInfoFragment personalInfoFragment = new PersonalInfoFragment();
                return personalInfoFragment;
            case 1:
                SocietyDetailFragment societyDetailFragment = new SocietyDetailFragment();
                societyDetailFragment.setBottomBar(1,name);
                return societyDetailFragment;
               /* MySocietyListFragment mySocietyListFragment = new MySocietyListFragment();
                return mySocietyListFragment;*/
            case 2:
                FamilyDetailFragment familyDetailFragment = new FamilyDetailFragment();
                familyDetailFragment.setFamilyBottomView(1);
                return familyDetailFragment;
            case 3:
                MyAddressFragment myAddressFragment = new MyAddressFragment();
                return myAddressFragment;
            case 4:
                MyOrdersFragment myOrdersFragment = new MyOrdersFragment();
                return myOrdersFragment;
            case 5:
                MyServicesFragment myServicesFragment = new MyServicesFragment();
                return myServicesFragment;
            default:
                PersonalInfoFragment personalInfoFragment1 = new PersonalInfoFragment();
                return personalInfoFragment1;
        }
        //return null;
    }

    @Override
    public int getItemPosition(Object object) {
        return super.getItemPosition(object);
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return mAccountTabList.get(position).toString();
    }

    @Override
    public long getItemId(int position) {
        return super.getItemId(position);
    }

    @Override
    public int getCount() {
        return mAccountTabList.size();
    }
}
