package com.android.lockated.utils;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;

import com.android.lockated.R;
import com.android.lockated.request.LockatedVolleyRequestQueue;
import com.android.volley.toolbox.ImageLoader;
import com.squareup.picasso.Picasso;


public class ShowImage extends AppCompatActivity {

    Bundle bundle;
    ZoomableImageView displayimage;
    String imagePath;
    ImageLoader imageLoader;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_show_image);

        init();
        setToolBarTitle("");

        if (getIntent().getExtras() != null) {
            bundle = getIntent().getExtras();
            if (bundle.containsKey("imagePathString")) {
                imagePath = bundle.getString("imagePathString");
                Bitmap bmp = BitmapFactory.decodeFile(imagePath);
                displayimage.setImageBitmap(bmp);
            } else if (bundle.containsKey("imageUrlString")) {
                imagePath = bundle.getString("imageUrlString");
                displayimage.setDefaultImageResId(R.drawable.loading);
                displayimage.setImageUrl(imagePath, imageLoader);
                //Picasso.with(this).load(imagePath).placeholder(R.drawable.loading).into(displayimage);
            }
        }

    }

    private void init() {
        displayimage = (ZoomableImageView) findViewById(R.id.displayimage);
        imageLoader = LockatedVolleyRequestQueue.getInstance(this).getImageLoader();
    }

    public void setToolBarTitle(String titleName) {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(true);
        getSupportActionBar().setTitle(titleName);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home: {
                super.onBackPressed();
                return true;
            }
            default:
                return super.onOptionsItemSelected(item);
        }
    }

}