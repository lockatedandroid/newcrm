package com.android.lockated.model.usermodel.EventModel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;


public class Events {

    @SerializedName("date")
    @Expose
    private String date;
    @SerializedName("events")
    @Expose
    private ArrayList<EventSub> events = new ArrayList<EventSub>();

    /**
     * @return The date
     */
    public String getDate() {
        return date;
    }

    /**
     * @param date The date
     */
    public void setDate(String date) {
        this.date = date;
    }

    /**
     * @return The events
     */
    public ArrayList<EventSub> getEvents() {
        return events;
    }

    /**
     * @param events The events
     */
    public void setEvents(ArrayList<EventSub> events) {
        this.events = events;
    }

}