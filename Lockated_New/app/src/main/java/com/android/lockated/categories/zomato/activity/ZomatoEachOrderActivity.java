package com.android.lockated.categories.zomato.activity;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.android.lockated.R;
import com.android.lockated.categories.zomato.adapter.ZomatoItemAdapter;
import com.android.lockated.model.zomato.ThirdpartyTransaction;

import java.util.ArrayList;

public class ZomatoEachOrderActivity extends AppCompatActivity {
    int position;
    ImageView img_place_order, img_order_confirm, img_pending_order, img_order_delivered, img_calender, img_callRestaurant;
    TextView txt_orderDate, txt_restaurant_name, txt_order_id, txt_restaurant_address, item_details, txt_item_name, txt_item_quantity, txt_item_cost, txt_subtotal, txt_taxes, txt_extra_charge, txt_discount_amount, txt_total_amount_payable, txt_amount_paid_via,txt_transaction_id,txt_delivery_address,txt_delivery_message;
    LinearLayout mLayoutContainer;
    public static ArrayList<ThirdpartyTransaction> thirdpartyTransactions;
    private ProgressBar progressBar;
    private ListView listView;
    ZomatoItemAdapter zomatoItemAdapter;
    private static final int REQUEST_CALL_PERMISSION = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_zomato_each_order);
        Toolbar toolbar = (Toolbar) findViewById(R.id.custom_toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setTitle("Order Details");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(true);
        progressBar = (ProgressBar) findViewById(R.id.mProgressBarView);
        mLayoutContainer = (LinearLayout) findViewById(R.id.mLayoutContainer);
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            position = extras.getInt("Position");
        }
        init();
        setData(position);
    }

    public void init() {
        mLayoutContainer.setVisibility(View.GONE);
        progressBar.setVisibility(View.VISIBLE);

        img_place_order = (ImageView) findViewById(R.id.img_place_order);
        img_order_confirm = (ImageView) findViewById(R.id.img_order_confirm);
        img_pending_order = (ImageView) findViewById(R.id.img_pending_order);
        img_order_delivered = (ImageView) findViewById(R.id.img_order_delivered);
        img_calender = (ImageView) findViewById(R.id.img_calender);
        img_callRestaurant = (ImageView) findViewById(R.id.img_callRestaurant);


        txt_orderDate = (TextView) findViewById(R.id.txt_orderDate);
        txt_restaurant_name = (TextView) findViewById(R.id.txt_restaurant_name);
        txt_order_id = (TextView) findViewById(R.id.txt_order_id);
        txt_restaurant_address = (TextView) findViewById(R.id.txt_restaurant_address);
        txt_item_name = (TextView) findViewById(R.id.txt_item_name);
        txt_item_quantity = (TextView) findViewById(R.id.txt_item_quantity);
        txt_item_cost = (TextView) findViewById(R.id.txt_item_cost);
        txt_subtotal = (TextView) findViewById(R.id.txt_subtotal);
        txt_taxes = (TextView) findViewById(R.id.txt_taxes);
        txt_extra_charge = (TextView) findViewById(R.id.txt_extra_charge);
        txt_discount_amount = (TextView) findViewById(R.id.txt_discount_amount);
        txt_total_amount_payable = (TextView) findViewById(R.id.txt_total_amount_payable);
        txt_amount_paid_via = (TextView) findViewById(R.id.txt_amount_paid_via);
        //txt_transaction_id =  (TextView) findViewById(R.id.txt_transaction_id);
        txt_delivery_address = (TextView) findViewById(R.id.txt_delivery_address);
        txt_delivery_message = (TextView) findViewById(R.id.txt_delivery_message);


        img_callRestaurant.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onCallClicked();
            }
        });


        listView = (ListView) findViewById(R.id.listView);
        setListViewHeightBasedOnChildren(listView);
        listView.setOnTouchListener(new View.OnTouchListener() {
            // Setting on Touch Listener for handling the touch inside ScrollView
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                // Disallow the touch request for parent scroll on touch of child view
                v.getParent().requestDisallowInterceptTouchEvent(true);
                return false;
            }
        });

    }


    @SuppressLint("LongLogTag")
    public void setData(int position) {
        progressBar.setVisibility(View.GONE);
        mLayoutContainer.setVisibility(View.VISIBLE);
        txt_orderDate.setText("" + thirdpartyTransactions.get(position).getTpdetails().getCreatedTimestampStr());
        txt_restaurant_name.setText("" + thirdpartyTransactions.get(position).getTpdetails().getRestaurantName());
        //txt_transaction_id.setText("" + thirdpartyTransactions.get(position).getTptransactionid());
        txt_order_id.setText(""+thirdpartyTransactions.get(position).getTpdetails().getId());
        txt_restaurant_address.setText("" + thirdpartyTransactions.get(position).getTpdetails().getRestaurantAddress());
        txt_amount_paid_via.setText(" " + thirdpartyTransactions.get(position).getPaymentmethod());
        txt_delivery_address.setText(thirdpartyTransactions.get(position).getTpdetails().getDeliveryAddressString());
        txt_delivery_message.setText(thirdpartyTransactions.get(position).getTpdetails().getDeliveryMessage());


        Log.e("order_status ",""+ thirdpartyTransactions.get(position).getTpdetails().getOrderStatus());
        switch (thirdpartyTransactions.get(position).getTpdetails().getOrderStatus()) {
            case 1:
                Log.e("ORDER_STATUS_PENDING", "Order Placed (pending confirmation)");
                Log.e("Message  ",thirdpartyTransactions.get(position).getTpdetails().getDeliveryMessage());
                img_place_order.setImageDrawable(getResources().getDrawable(R.drawable.round_check));
                break;
            case 2:
                Log.e("ORDER_STATUS_CONFIRMED", "Order Confirmed");
                Log.e("Message  ",thirdpartyTransactions.get(position).getTpdetails().getDeliveryMessage());
                img_place_order.setImageDrawable(getResources().getDrawable(R.drawable.round_check));
                img_order_confirm.setImageDrawable(getResources().getDrawable(R.drawable.round_check));
                break;

            case 3:
                Log.e("ORDER_DELIVERY_STATUS_ENROUTE", "Order Enroute");
                Log.e("Message  ",thirdpartyTransactions.get(position).getTpdetails().getDeliveryMessage());
                img_place_order.setImageDrawable(getResources().getDrawable(R.drawable.round_check));
                img_order_confirm.setImageDrawable(getResources().getDrawable(R.drawable.round_check));
                img_pending_order.setImageDrawable(getResources().getDrawable(R.drawable.round_check));
                break;

            case  6:
                Log.e("ORDER_STATUS_DELIVERED", "Order Delivered");
                Log.e("Message  ",thirdpartyTransactions.get(position).getTpdetails().getDeliveryMessage());
                img_place_order.setImageDrawable(getResources().getDrawable(R.drawable.round_check));
                img_order_confirm.setImageDrawable(getResources().getDrawable(R.drawable.round_check));
                img_pending_order.setImageDrawable(getResources().getDrawable(R.drawable.round_check));
                img_order_delivered.setImageDrawable(getResources().getDrawable(R.drawable.round_check));
                break;
            case  7:
                Log.e("ORDER_STATUS_TIMED_OUT", "Timed Out due to no action taken by Restaurant");
                Log.e("Message  ",thirdpartyTransactions.get(position).getTpdetails().getDeliveryMessage());
                img_place_order.setImageDrawable(getResources().getDrawable(R.drawable.order_placed));
                img_order_confirm.setImageDrawable(getResources().getDrawable(R.drawable.order_confirm));
                img_pending_order.setImageDrawable(getResources().getDrawable(R.drawable.order_enroute));
                img_order_delivered.setImageDrawable(getResources().getDrawable(R.drawable.order_delivery));
                break;

            case  8:
                Log.e("ORDER_STATUS_REJECTED", "Rejected by Restaurant");
                Log.e("Message  ",thirdpartyTransactions.get(position).getTpdetails().getDeliveryMessage());
                img_place_order.setImageDrawable(getResources().getDrawable(R.drawable.order_placed));
                img_order_confirm.setImageDrawable(getResources().getDrawable(R.drawable.order_confirm));
                img_pending_order.setImageDrawable(getResources().getDrawable(R.drawable.order_enroute));
                img_order_delivered.setImageDrawable(getResources().getDrawable(R.drawable.order_delivery));
                break;
        }
        if (thirdpartyTransactions.get(position).getTpdetails().getOrderDetails().get(0).getDishes() != null) {
            int c = thirdpartyTransactions.get(position).getTpdetails().getOrderDetails().get(0).getDishes().size();
            for (int j = 0; j < c; j++) {
                Log.e("ItemName","" + thirdpartyTransactions.get(position).getTpdetails().getOrderDetails().get(0).getDishes().get(j).getItemName());
                Log.e("Quantity","" + thirdpartyTransactions.get(position).getTpdetails().getOrderDetails().get(0).getDishes().get(j).getQuantity());
                Log.e("Unit Cost","" + thirdpartyTransactions.get(position).getTpdetails().getOrderDetails().get(0).getDishes().get(j).getUnitCost());
                Log.e("Total Cost","" + thirdpartyTransactions.get(position).getTpdetails().getOrderDetails().get(0).getDishes().get(j).getTotalCost());

            }
        }

        if (thirdpartyTransactions.get(position).getTpdetails().getOrderDetails().get(0).getDishes() != null) {
            zomatoItemAdapter = new ZomatoItemAdapter(this, R.layout.zomato_item,thirdpartyTransactions.get(position).getTpdetails().getOrderDetails().get(0).getDishes());
            listView.setAdapter(zomatoItemAdapter);
        }

        if (thirdpartyTransactions.get(position).getTpdetails().getOrderDetails() != null) {
            int a = thirdpartyTransactions.get(position).getTpdetails().getOrderDetails().size();
            for (int i = 0; i < a; i++) {

                if (thirdpartyTransactions.get(position).getTpdetails().getOrderDetails().get(i).getSaltDiscounts() != null) {
                    int c = thirdpartyTransactions.get(position).getTpdetails().getOrderDetails().get(i).getSaltDiscounts().size();
                    for (int j = 0; j < c; j++) {
                        txt_discount_amount.setText("" + thirdpartyTransactions.get(position).getTpdetails().getCurrency() + " " + thirdpartyTransactions.get(position).getTpdetails().getOrderDetails().get(i).getSaltDiscounts().get(j).getTotalCost());
                    }
                }

                if (thirdpartyTransactions.get(position).getTpdetails().getOrderDetails().get(i).getCharges() != null) {
                    int d = thirdpartyTransactions.get(position).getTpdetails().getOrderDetails().get(i).getCharges().size();
                    for (int j = 0; j < d; j++) {
                        txt_extra_charge.setText("" + thirdpartyTransactions.get(position).getTpdetails().getCurrency() + " " + thirdpartyTransactions.get(position).getTpdetails().getOrderDetails().get(i).getCharges().get(j).getTotalCost());
                    }
                }

                if (thirdpartyTransactions.get(position).getTpdetails().getOrderDetails().get(i).getSubtotal2() != null) {
                    int f = thirdpartyTransactions.get(position).getTpdetails().getOrderDetails().get(i).getSubtotal2().size();
                    for (int j = 0; j < f; j++) {
                        txt_subtotal.setText("" + thirdpartyTransactions.get(position).getTpdetails().getCurrency() + " " + thirdpartyTransactions.get(position).getTpdetails().getOrderDetails().get(i).getSubtotal2().get(j).getTotalCost());
                    }
                }
                if (thirdpartyTransactions.get(position).getTpdetails().getOrderDetails().get(i).getTotal() != null) {
                    int g = thirdpartyTransactions.get(position).getTpdetails().getOrderDetails().get(i).getTotal().size();
                    for (int j = 0; j < g; j++) {
                        txt_total_amount_payable.setText("" + thirdpartyTransactions.get(position).getTpdetails().getCurrency() + " " + thirdpartyTransactions.get(position).getTpdetails().getOrderDetails().get(i).getTotal().get(j).getTotalCost());
                    }
                }
                if (thirdpartyTransactions.get(position).getTpdetails().getOrderDetails().get(i).getTaxes() != null) {
                    int h = thirdpartyTransactions.get(position).getTpdetails().getOrderDetails().get(i).getTaxes().size();
                    for (int j = 0; j < h; j++) {
                        txt_taxes.setText("" + thirdpartyTransactions.get(position).getTpdetails().getCurrency() + " " + thirdpartyTransactions.get(position).getTpdetails().getOrderDetails().get(i).getTaxes().get(j).getTotalCost());
                    }
                }
                if (thirdpartyTransactions.get(position).getTpdetails().getOrderDetails().get(i).getVoucherDiscounts() != null) {
                    int k = thirdpartyTransactions.get(position).getTpdetails().getOrderDetails().get(i).getVoucherDiscounts().size();
                    for (int j = 0; j < k; j++) {
                    }
                }
            }
        }
    }



    private void onCallClicked() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                requestPermissions(new String[]{Manifest.permission.CALL_PHONE}, REQUEST_CALL_PERMISSION );
            } else {
                callingIntent();
            }
        } else {
            callingIntent();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == REQUEST_CALL_PERMISSION) {
            if (grantResults.length == 1 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                callingIntent();
            }
        }
    }

    private void callingIntent() {
        Intent intent = new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + thirdpartyTransactions.get(position).getTpdetails().getRestaurantPhone()));
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        startActivity(intent);
    }

    /**** Method for Setting the Height of the ListView dynamically.
     **** Hack to fix the issue of not showing all the items of the ListView
     **** when placed inside a ScrollView  ****/
    public static void setListViewHeightBasedOnChildren(ListView listView) {
        ListAdapter listAdapter = listView.getAdapter();
        if (listAdapter == null)
            return;
//-----------------------------------------------------------------------------
        /*int desiredWidth = View.MeasureSpec.makeMeasureSpec(listView.getWidth(), View.MeasureSpec.AT_MOST);
        int totalHeight = 0;
        View view = null;
        for (int i = 0; i < listAdapter.getCount(); i++) {
            view = listAdapter.getView(i, view, listView);
            if (i == 0)
                view.setLayoutParams(new ViewGroup.LayoutParams(desiredWidth, LinearLayout.LayoutParams.MATCH_PARENT));

            view.measure(desiredWidth, View.MeasureSpec.UNSPECIFIED);
            totalHeight += view.getMeasuredHeight();
        }
        ViewGroup.LayoutParams params = listView.getLayoutParams();
        params.height = totalHeight + (listView.getDividerHeight() * (listAdapter.getCount() - 1));
        listView.setLayoutParams(params);*/
//-------------------------------------------------------------------------------------------------
        int numberOfItems = listAdapter.getCount();

        // Get total height of all items.
        int totalItemsHeight = 0;
        for (int itemPos = 0; itemPos < numberOfItems; itemPos++) {
            View item = listAdapter.getView(itemPos, null, listView);

            float px = 300 * (listView.getResources().getDisplayMetrics().density);
            item.measure(View.MeasureSpec.makeMeasureSpec((int)px, View.MeasureSpec.AT_MOST), View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED));
            //item.measure(View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED), View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED));
            totalItemsHeight += item.getMeasuredHeight();
        }

        // Get total height of all item dividers.
        int totalDividersHeight = listView.getDividerHeight() *
                (numberOfItems - 1);
        // Get padding
        int totalPadding = listView.getPaddingTop() + listView.getPaddingBottom();

        // Set list height.
        ViewGroup.LayoutParams params = listView.getLayoutParams();
        params.height = totalItemsHeight + totalDividersHeight + totalPadding;
        listView.setLayoutParams(params);
        listView.requestLayout();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                callIntent();
                break;
        }
        return super.onOptionsItemSelected(item);
    }


    @Override
    public void onBackPressed() {
        callIntent();
    }

    private void callIntent() {
        Intent intent = new Intent(this, ZomatoOrderItemListActivity.class);//------------Change on 16-06-2016
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
        finish();
    }
}
