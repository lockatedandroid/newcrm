package com.android.lockated.crm.adapters;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.lockated.R;
import com.android.lockated.crm.activity.MyClassifiedDetailActivity;
import com.android.lockated.model.usermodel.Classified.AllClassified.AllClassifiedList;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;

import java.util.ArrayList;

public class SellAdapter extends BaseAdapter {

    private final ArrayList<AllClassifiedList> allClassifieds;
    Context contextMain;
    JSONArray jsonArray;
    LayoutInflater layoutInflater;

    public SellAdapter(Context context, ArrayList<AllClassifiedList> allClassifieds) {
        this.contextMain = context;
        this.allClassifieds = allClassifieds;
        layoutInflater = LayoutInflater.from(context);
    }

    @Override
    public int getCount() {
        return allClassifieds.size();
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    private class ViewHolder {
        TextView mThingToRent, mTextTO, mTextRate, mTextName, mTextDate;
        TextView mViewDetails, mContactAuthor, categoryValue;
        ImageView msellImage;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        ViewHolder holder = null;
        if (convertView == null) {
            convertView = layoutInflater.inflate(R.layout.sub_classified, parent, false);
            holder = new ViewHolder();
            convertView.setTag(holder);

        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        holder.mThingToRent = (TextView) convertView.findViewById(R.id.mThingToRent);
        holder.msellImage = (ImageView) convertView.findViewById(R.id.msellImage);
        holder.mTextTO = (TextView) convertView.findViewById(R.id.mTextTO);
        holder.mTextRate = (TextView) convertView.findViewById(R.id.mTextRate);
        holder.mTextName = (TextView) convertView.findViewById(R.id.mTextName);
        holder.mTextDate = (TextView) convertView.findViewById(R.id.mTextDate);
        holder.mViewDetails = (TextView) convertView.findViewById(R.id.mViewDetails);
        holder.mContactAuthor = (TextView) convertView.findViewById(R.id.mContactAuthor);
        holder.categoryValue = (TextView) convertView.findViewById(R.id.categoryValue);
        try {
            holder.mThingToRent.setText(allClassifieds.get(position).getTitle());
            holder.mTextTO.setText(allClassifieds.get(position).getPurpose());
            holder.mTextRate.setText(allClassifieds.get(position).getPrice());
            holder.mTextDate.setText(allClassifieds.get(position).getExpire());
            holder.categoryValue.setText(allClassifieds.get(position).getClassifiedCategory());
            Picasso.with(contextMain).load("" + allClassifieds.get(position).getMedium()).fit()
                    .into(holder.msellImage);
        } catch (Exception e) {
            e.printStackTrace();
        }
        holder.mViewDetails.setTag(position);
        holder.mViewDetails.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int position = (int) v.getTag();
                Intent intent = new Intent(contextMain, MyClassifiedDetailActivity.class);
                try {
                    intent.putExtra("strThing", allClassifieds.get(position).getTitle());
                    intent.putExtra("strThingTO", allClassifieds.get(position).getPurpose());
                    intent.putExtra("strRate", allClassifieds.get(position).getPrice());
                    intent.putExtra("strDescription", allClassifieds.get(position).getDescription());
                    intent.putExtra("strDate", allClassifieds.get(position).getExpire());
                    intent.putExtra("ImagePicaso", allClassifieds.get(position).getOriginal());
                    intent.putExtra("categoryValue", allClassifieds.get(position).getClassifiedCategory());
                    intent.putExtra("pid", "" + allClassifieds.get(position).getId());

                    if (allClassifieds.get(position).getUser() != null) {
                        intent.putExtra("name", allClassifieds.get(position).getUser().getFirstname() + allClassifieds.get(position).getUser().getLastname());
                        intent.putExtra("mobile", allClassifieds.get(position).getUser().getMobile());
                        intent.putExtra("email", allClassifieds.get(position).getUser().getEmail());
                    } else {
                        intent.putExtra("name", "Not Available");
                        intent.putExtra("mobile", "Not Available");
                        intent.putExtra("email", "Not Available");
                    }
                    contextMain.startActivity(intent);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
        return convertView;
    }

}