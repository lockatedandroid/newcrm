package com.android.lockated.crm.fragment.myzone.directory;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.android.lockated.LockatedApplication;
import com.android.lockated.R;
import com.android.lockated.crm.activity.MyZoneDirectoryActivity;
import com.android.lockated.model.usermodel.Directory.Directory;
import com.android.lockated.network.ConnectionDetector;
import com.android.lockated.preferences.LockatedPreferences;
import com.android.lockated.request.LockatedJSONObjectRequest;
import com.android.lockated.request.LockatedRequestError;
import com.android.lockated.request.LockatedVolleyRequestQueue;
import com.android.lockated.utils.ApplicationURL;
import com.android.lockated.utils.Utilities;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class PersonalDirectoryFragment extends Fragment implements View.OnClickListener, Response.ErrorListener, Response.Listener<JSONObject> {
    FragmentManager fragmentManager;
    private FloatingActionButton fab;
    private RequestQueue mQueue;
    ListView listView;
    String pid;
    String userType;
    TextView errorMsg;
    ProgressBar progressBar;
    ArrayList<Directory> personalDirectoryList;
    private LockatedPreferences mLockatedPreferences;
    public static final String REQUEST_TAG = "PersonalDirectoryFragment";
    PersonalDirectoryAdapter personalDirectoryAdapter;
    String SHOW, CREATE, INDEX, UPDATE, EDIT, DESTROY;

    public FragmentManager setFragmentManager(FragmentManager fragmentManager) {
        this.fragmentManager = fragmentManager;
        return fragmentManager;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View PersonalDirectoryFragmentView = inflater.inflate(R.layout.fragment_personal_directory, container, false);
        init(PersonalDirectoryFragmentView);
        Utilities.ladooIntegration(getActivity(), "Personal Directory");
        return PersonalDirectoryFragmentView;
    }

    @Override
    public void onResume() {
        super.onResume();
        LockatedApplication.getInstance().trackScreenView(getString(R.string.personalDirectory));
        LockatedApplication.getInstance().trackEvent(getString(R.string.personalDirectory),
                getString(R.string.visited), getString(R.string.personalDirectory));
        personalDirectoryList.clear();
        personalDirectoryAdapter.notifyDataSetChanged();

    }

    private void init(View personalDirectoryFragmentView) {
        personalDirectoryList = new ArrayList<>();
        mLockatedPreferences = new LockatedPreferences(getActivity());
        progressBar = (ProgressBar) personalDirectoryFragmentView.findViewById(R.id.mProgressBarView);
        listView = (ListView) personalDirectoryFragmentView.findViewById(R.id.listView);
        errorMsg = (TextView) personalDirectoryFragmentView.findViewById(R.id.errorMsg);
        fab = (FloatingActionButton) personalDirectoryFragmentView.findViewById(R.id.fab);
        fab.setOnClickListener(this);
        getDirectoryRole();
        checkPermission();
        personalDirectoryAdapter = new PersonalDirectoryAdapter(getActivity(), personalDirectoryList, EDIT, DESTROY);
        listView.setAdapter(personalDirectoryAdapter);
        /*  try {
            userType = getMySocietyRoles();
        } catch (Exception e) {
            e.printStackTrace();
        }*/
    }

    private void checkPermission() {
        if (CREATE!=null&&CREATE.equals("true")) {
            fab.setVisibility(View.VISIBLE);
        } else {
            fab.setVisibility(View.GONE);
        }
        if (SHOW!=null&&SHOW.equals("true")) {
            getDirectory();
        } else {
            listView.setVisibility(View.GONE);
            errorMsg.setVisibility(View.VISIBLE);
            errorMsg.setText(R.string.no_permission_error);
        }
    }

    private void getDirectory() {
        if (getActivity() != null) {
            if (!mLockatedPreferences.getSocietyId().equals("blank")) {
                if (ConnectionDetector.isConnectedToInternet(getActivity())) {
                    mLockatedPreferences = new LockatedPreferences(getActivity());
                    mQueue = LockatedVolleyRequestQueue.getInstance(getActivity()).getRequestQueue();
                    String url = ApplicationURL.getDirectory + mLockatedPreferences.getLockatedToken();
                    LockatedJSONObjectRequest lockatedJSONObjectRequest = new LockatedJSONObjectRequest(Request.Method.GET,
                            url, null, this, this);
                    lockatedJSONObjectRequest.setTag(REQUEST_TAG);
                    mQueue.add(lockatedJSONObjectRequest);
                } else {
                    Utilities.showToastMessage(getActivity(), getActivity().getResources().getString(R.string.internet_connection_error));
                }
            } else {
                listView.setVisibility(View.GONE);
                errorMsg.setVisibility(View.VISIBLE);
                errorMsg.setText(R.string.not_in_society);
            }

        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.fab:
                String pid = null;
                AddToDirectory mAddToDirectory = new AddToDirectory();
                Bundle bundle = new Bundle();
                bundle.putString("pid", pid);
                mAddToDirectory.setArguments(bundle);
                mAddToDirectory.show(getActivity().getSupportFragmentManager(), "PreviewDialog");
                break;
        }
    }

    @Override
    public void onErrorResponse(VolleyError error) {
        progressBar.setVisibility(View.GONE);
        if (getActivity() != null) {
            LockatedRequestError.onRequestError(getActivity(), error);
        }
    }

    @Override
    public void onResponse(JSONObject response) {
        progressBar.setVisibility(View.GONE);

        if (getActivity() != null) {

            try {
                if (response != null && response.length() > 0) {
                    if (response.has("code") && response.getJSONArray("directories").length() > 0) {
                        JSONArray jsonArray = response.getJSONArray("directories");
                        Gson gson = new Gson();
                        for (int i = 0; i < jsonArray.length(); i++) {
                            Directory directory = gson.fromJson(jsonArray.getJSONObject(i).toString(), Directory.class);
                            personalDirectoryList.add(directory);
                        }
                        personalDirectoryAdapter.notifyDataSetChanged();
                     /*  personalDirectoryAdapter = new PersonalDirectoryAdapter(getActivity(), response.getJSONArray("directories"));
                        listView.setAdapter(personalDirectoryAdapter);*/
                    } else {
                        listView.setVisibility(View.GONE);
                        errorMsg.setVisibility(View.VISIBLE);
                        errorMsg.setText(R.string.no_data_error);
                    }
                } else {
                    listView.setVisibility(View.GONE);
                    errorMsg.setVisibility(View.VISIBLE);
                    errorMsg.setText(R.string.no_data_error);
                }

            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private class PersonalDirectoryAdapter extends BaseAdapter {
        private final ArrayList<Directory> directory;
        private final String EDIT;
        private final String DESTROY;
        Context contextMain;
        JSONArray jsonArray;
        LayoutInflater layoutInflater;

        public PersonalDirectoryAdapter(Context context, ArrayList<Directory> directory, String EDIT, String DESTROY) {
            this.contextMain = context;
            this.EDIT = EDIT;
            this.DESTROY = DESTROY;
            this.directory = directory;
            layoutInflater = LayoutInflater.from(context);
        }


        @Override
        public int getCount() {
            return personalDirectoryList.size();
        }

        @Override
        public Object getItem(int position) {
            return position;
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        private class ViewHolder {
            ImageView mImageDelete;
            ImageView mImageEdit;
            TextView mTextNumber;
            TextView mEmail_Id;
            TextView mTextDirectoryName;
            TextView mServiceType;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
            ViewHolder holder = null;
            if (convertView == null) {

                convertView = layoutInflater.inflate(R.layout.directorychild, parent, false);
                holder = new ViewHolder();
                convertView.setTag(holder);
            } else {
                holder = (ViewHolder) convertView.getTag();
            }

            holder.mImageEdit = (ImageView) convertView.findViewById(R.id.mImageEdit);
            holder.mImageDelete = (ImageView) convertView.findViewById(R.id.mImageDelete);
            holder.mTextDirectoryName = (TextView) convertView.findViewById(R.id.mTextDirectoryName);
            holder.mTextNumber = (TextView) convertView.findViewById(R.id.mTextNumber);
            holder.mEmail_Id = (TextView) convertView.findViewById(R.id.mEmail_Id);
            if (EDIT != null && EDIT.equals("true")) {
                holder.mImageEdit.setVisibility(View.VISIBLE);
            } else {
                holder.mImageEdit.setVisibility(View.INVISIBLE);
            }
            if (DESTROY != null && DESTROY.equals("true")) {
                holder.mImageDelete.setVisibility(View.VISIBLE);
            } else {
                holder.mImageDelete.setVisibility(View.INVISIBLE);
            }

            try {
                String name = directory.get(position).getFirstname()
                        + " " + directory.get(position).getLastname();
                holder.mTextDirectoryName.setText(name);
                holder.mTextNumber.setText(directory.get(position).getMobile());
                if (directory.get(position).getEmail() != null) {
                    holder.mEmail_Id.setText(directory.get(position).getEmail());
                } else {
                    holder.mEmail_Id.setText("Not Available");
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            holder.mImageEdit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    pid = "" + directory.get(position).getId();
                    AddToDirectory mAddToDirectory = new AddToDirectory();
                    Bundle bundle = new Bundle();
                    bundle.putString("pid", pid);
                    bundle.putString("mobile", directory.get(position).getMobile());
                    if (directory.get(position).getEmail() != null) {
                        bundle.putString("email", directory.get(position).getEmail());
                    } else {
                        bundle.putString("email", "Not Available");
                    }
                    bundle.putString("firstname", directory.get(position).getFirstname());
                    bundle.putString("lastname", directory.get(position).getLastname());

                    mAddToDirectory.setArguments(bundle);
                    mAddToDirectory.show(getActivity().getSupportFragmentManager(), "PreviewDialog");
                }
            });
            holder.mImageDelete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    pid = "" + directory.get(position).getId();
                    onDirectoryDelete(pid);
                }
            });
            return convertView;
        }

        private void onDirectoryDelete(String pid) {
            mLockatedPreferences = new LockatedPreferences(getActivity());
            progressBar.setVisibility(View.VISIBLE);
            if (getActivity() != null) {
                if (!mLockatedPreferences.getSocietyId().equals("blank")) {

                    if (pid != null) {
                        try {
                            mQueue = LockatedVolleyRequestQueue.getInstance(getActivity()).getRequestQueue();
                            LockatedJSONObjectRequest lockatedJSONObjectRequest = new LockatedJSONObjectRequest
                                    (Request.Method.DELETE, ApplicationURL.deleteDirectory + pid + ".json?token="
                                            + mLockatedPreferences.getLockatedToken(), null, new Response.Listener<JSONObject>() {
                                        @Override
                                        public void onResponse(JSONObject response) {
                                            progressBar.setVisibility(View.GONE);
                                            if (response != null && response.length() > 0) {

                                                if (response.has("code") && response.has("message")) {
                                                    Intent intent = new Intent(getActivity(), MyZoneDirectoryActivity.class);
                                                    intent.putExtra("Listposition", 0);
                                                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                                    getActivity().startActivity(intent);
                                                }
                                            } else {
                                                listView.setVisibility(View.GONE);
                                                errorMsg.setVisibility(View.VISIBLE);
                                                errorMsg.setText(R.string.no_data_error);
                                            }
                                        }
                                    }, new Response.ErrorListener() {
                                        @Override
                                        public void onErrorResponse(VolleyError error) {
                                            progressBar.setVisibility(View.GONE);
                                            if (getActivity() != null) {
                                                LockatedRequestError.onRequestError(getActivity(), error);
                                            }
                                        }
                                    });
                            lockatedJSONObjectRequest.setTag(REQUEST_TAG);
                            mQueue.add(lockatedJSONObjectRequest);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                } else {
                    listView.setVisibility(View.GONE);
                    errorMsg.setVisibility(View.VISIBLE);
                    errorMsg.setText(R.string.not_in_society);
                }
            }
        }
    }

    public String getDirectoryRole() {
        String mySocietyRoles = getActivity().getResources().getString(R.string.blank_value);
        if (mLockatedPreferences.getRolesJson() != null && !mLockatedPreferences.getRolesJson().equals("blank")) {
            try {
                JSONObject noticeJsonObj = new JSONObject(mLockatedPreferences.getRolesJson());
                for (int i = 0; i < noticeJsonObj.getJSONArray(getActivity().getResources().getString(R.string.permissions_value)).length(); i++) {
                    JSONObject jsonObject1 = noticeJsonObj.getJSONArray(getActivity().getResources().getString(R.string.permissions_value)).getJSONObject(i);
                    if (jsonObject1.has(getActivity().getResources().getString(R.string.section_value))
                            && jsonObject1.getString(getActivity().getResources().getString(R.string.section_value)).equals("spree_directories")) {
                        if (jsonObject1.getJSONObject(getActivity().getResources().getString(R.string.permission_value)).has(getActivity().getResources().getString(R.string.index_value))) {
                            mySocietyRoles = jsonObject1.getJSONObject(getActivity().getResources().getString(R.string.permission_value)).getString(getActivity().getResources().getString(R.string.index_value));
                            CREATE = jsonObject1.getJSONObject(getActivity().getResources().getString(R.string.permission_value)).getString("create");
                            INDEX = jsonObject1.getJSONObject(getActivity().getResources().getString(R.string.permission_value)).getString("index");
                            UPDATE = jsonObject1.getJSONObject(getActivity().getResources().getString(R.string.permission_value)).getString("update");
                            EDIT = jsonObject1.getJSONObject(getActivity().getResources().getString(R.string.permission_value)).getString("edit");
                            SHOW = jsonObject1.getJSONObject(getActivity().getResources().getString(R.string.permission_value)).getString("show");
                            DESTROY = jsonObject1.getJSONObject(getActivity().getResources().getString(R.string.permission_value)).getString("destroy");
                        }
                    }
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return mySocietyRoles;
    }
}
