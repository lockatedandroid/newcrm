package com.android.lockated.model;

import org.json.JSONException;
import org.json.JSONObject;

public class MyAddressData
{
    private int addressId;
    private String firstName;
    private String lastName;
    private String address1;
    private String address2;
    private String city;
    private String zipcode;
    private String phoneNumber;
    private String state;
    private int stateId;
    private String country;

    public MyAddressData(JSONObject jsonObject)
    {
        try
        {
            addressId = jsonObject.optInt("id");
            firstName = jsonObject.optString("firstname");
            lastName = jsonObject.optString("lastname");
            address1 = jsonObject.optString("address1");
            address2 = jsonObject.optString("address2");
            city = jsonObject.optString("city");
            zipcode = jsonObject.optString("zipcode");
            phoneNumber = jsonObject.optString("phone");

            JSONObject stateObject = jsonObject.getJSONObject("state");
            JSONObject countryObject = jsonObject.getJSONObject("country");

            state = stateObject.optString("name");
            stateId = stateObject.optInt("id");
            country = countryObject.optString("iso_name");
        }
        catch (JSONException e)
        {
            e.printStackTrace();
        }
    }

    public int getAddressId()
    {
        return addressId;
    }

    public String getFirstName()
    {
        return firstName;
    }

    public String getLastName()
    {
        return lastName;
    }

    public String getAddress1()
    {
        return address1;
    }

    public String getAddress2()
    {
        return address2;
    }

    public String getCity()
    {
        return city;
    }

    public String getZipcode()
    {
        return zipcode;
    }

    public String getPhoneNumber()
    {
        return phoneNumber;
    }

    public String getState()
    {
        return state;
    }

    public String getCountry()
    {
        return country;
    }

    public int getStateId()
    {
        return stateId;
    }
}
