package com.android.lockated.crm.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.lockated.R;
import com.android.lockated.utils.ShowImage;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

public class NoticeDetailActivity extends AppCompatActivity implements View.OnClickListener {
    private TextView mDescription;
    private TextView mClose;
    private TextView mEventType;
    private TextView mEventAt;
    private TextView mSubject;
    private TextView mCreatedBY;
    private TextView mEndDateTime;
    private TextView mTextAttachment;
    private ImageView mImageAttachment;
    String strImage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.notice_detail_activity);
        init();
        setToolBarTitle("Notice");
        displayData();
    }

    private void init() {
        mEventType = (TextView) findViewById(R.id.mEventType);
        mEventAt = (TextView) findViewById(R.id.mEventAt);
        mSubject = (TextView) findViewById(R.id.mSubject);
        mCreatedBY = (TextView) findViewById(R.id.mCreatedBY);
        mEndDateTime = (TextView) findViewById(R.id.mEndDateTime);
        mDescription = (TextView) findViewById(R.id.mDescription);
        mTextAttachment = (TextView) findViewById(R.id.mTextAttachment);
        mImageAttachment = (ImageView) findViewById(R.id.mImageAttachment);
        mImageAttachment.setOnClickListener(this);
        mClose = (TextView) findViewById(R.id.mClose);
        mClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    public void setToolBarTitle(String titleName) {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(true);
        getSupportActionBar().setTitle(titleName);
    }

    private void displayData() {
        if (getIntent().getExtras() != null) {
            try {
                JSONObject jsonObjectDetail = new JSONObject(getIntent().getExtras().getString("NoticeDetailViewData"));
                if (jsonObjectDetail.getString("username").equals("null")) {
                    mCreatedBY.setText("No Data");
                } else {
                    mCreatedBY.setText(jsonObjectDetail.getString("username"));
                }
                if (jsonObjectDetail.getString("notice_heading").equals("null")) {
                    mSubject.setText("No Data");
                } else {
                    mSubject.setText(jsonObjectDetail.getString("notice_heading"));
                }
                if (jsonObjectDetail.getString("description").equals("null")) {
                    mDescription.setText("No data");
                } else {
                    mDescription.setText(jsonObjectDetail.getString("description"));
                }
                mEndDateTime.setText(jsonObjectDetail.getString("expire_time"));
                if (!jsonObjectDetail.getString("document").equals("No Document")) {
                    strImage=jsonObjectDetail.getString("document");
                    Picasso.with(this).load("" + strImage).fit().placeholder(R.drawable.loading).into(mImageAttachment);
                } else {
                    mTextAttachment.setVisibility(View.VISIBLE);
                    mImageAttachment.setVisibility(View.GONE);
                }

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            finish();
            return true;
        } else {
            return false;
        }
    }

    @Override
    public void onClick(View v) {
        Intent intent = new Intent(getApplicationContext(), ShowImage.class);
        intent.putExtra("imageUrlString", strImage);
        startActivity(intent);
    }
}
