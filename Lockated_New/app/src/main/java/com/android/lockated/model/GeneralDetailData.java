package com.android.lockated.model;

import org.json.JSONObject;

public class GeneralDetailData
{
    private int id;
    private int pets;
    private int id_user;
    private String doc_name;
    private String doc_phone;
    private String emergency_contact_person;
    private String ecp_phone;
    private int vehicle;
    private String veh_reg_number;

    public GeneralDetailData(JSONObject jsonObject)
    {
        id = jsonObject.optInt("id");
        pets = jsonObject.optInt("pets");
        id_user = jsonObject.optInt("id_user");
        doc_name = jsonObject.optString("doc_name");
        doc_phone = jsonObject.optString("doc_phone");
        emergency_contact_person = jsonObject.optString("emergency_contact_person");
        ecp_phone = jsonObject.optString("ecp_phone");
        vehicle = jsonObject.optInt("vehicle");
        veh_reg_number = jsonObject.optString("veh_reg_number");
    }

    public int getId()
    {
        return id;
    }

    public int getPets()
    {
        return pets;
    }

    public int getId_user()
    {
        return id_user;
    }

    public String getDoc_name()
    {
        return doc_name;
    }

    public String getDoc_phone()
    {
        return doc_phone;
    }

    public String getEmergency_contact_person()
    {
        return emergency_contact_person;
    }

    public String getEcp_phone()
    {
        return ecp_phone;
    }

    public int getVehicle()
    {
        return vehicle;
    }

    public String getVeh_reg_number()
    {
        return veh_reg_number;
    }

    public void setId(int id)
    {
        this.id = id;
    }

    public void setPets(int pets)
    {
        this.pets = pets;
    }

    public void setId_user(int id_user)
    {
        this.id_user = id_user;
    }

    public void setDoc_name(String doc_name)
    {
        this.doc_name = doc_name;
    }

    public void setDoc_phone(String doc_phone)
    {
        this.doc_phone = doc_phone;
    }

    public void setEmergency_contact_person(String emergency_contact_person)
    {
        this.emergency_contact_person = emergency_contact_person;
    }

    public void setEcp_phone(String ecp_phone)
    {
        this.ecp_phone = ecp_phone;
    }

    public void setVehicle(int vehicle)
    {
        this.vehicle = vehicle;
    }

    public void setVeh_reg_number(String veh_reg_number)
    {
        this.veh_reg_number = veh_reg_number;
    }
}