package com.android.lockated.account.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import com.android.lockated.HomeActivity;
import com.android.lockated.R;
import com.android.lockated.information.AccountController;
import com.android.lockated.model.AccountData;
import com.android.lockated.network.ConnectionDetector;
import com.android.lockated.preferences.LockatedPreferences;
import com.android.lockated.register.OTPGeneratorFragment;
import com.android.lockated.request.LockatedRequestError;
import com.android.lockated.request.LockatedVolleyRequestQueue;
import com.android.lockated.utils.ApplicationURL;
import com.android.lockated.utils.MarshMallowPermission;
import com.android.lockated.utils.Utilities;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class EditPersonalInformationFragment extends Fragment implements View.OnClickListener, Response.ErrorListener, Response.Listener<JSONObject> {
    private FragmentManager fragmentManager;
    private EditText editTextFirstName;
    private EditText editTextLastName;
    private EditText editTextEmailAddress;
    private EditText editTextMobileNumber;
    private EditText editTextOTP;
    private TextView textSendOtp;
    private TextView textViewSubmit;
    String strFname;
    String strLname;
    String strEmailId;
    String strOTP;
    String strMobileNumber;
    ArrayList<AccountData> accountDataArrayList;
    AccountController accountController;
    private LockatedPreferences mLockatedPreferences;
    private LockatedVolleyRequestQueue mLockatedVolleyRequestQueue;
    public static final String REQUEST_OTP = "EditInformation";
    public static final String REQUEST_CHANGE_INFO = "ChangeInformation";
    public static final String REQUEST_TAG = "PersonalInfoFragment";
    MarshMallowPermission marshMallowPermission;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View editPersonalInformationView = inflater.inflate(R.layout.fragment_edit_personal_information, container, false);
        init(editPersonalInformationView);
        return editPersonalInformationView;
    }


    private void init(View view) {
        mLockatedPreferences = new LockatedPreferences(getActivity());
        marshMallowPermission = new MarshMallowPermission(getActivity());
        editTextFirstName = (EditText) view.findViewById(R.id.editTextFirstName);
        editTextLastName = (EditText) view.findViewById(R.id.editTextLastName);
        editTextEmailAddress = (EditText) view.findViewById(R.id.editTextEmailAddress);
        editTextMobileNumber = (EditText) view.findViewById(R.id.editTextMobileNumber);
        editTextOTP = (EditText) view.findViewById(R.id.editTextOTP);
        textSendOtp = (TextView) view.findViewById(R.id.textSendOtp);
        textViewSubmit = (TextView) view.findViewById(R.id.textViewSubmit);
        accountController = AccountController.getInstance();
        accountDataArrayList = accountController.getmAccountDataList();

       /* editTextMobileNumber.setText(accountDataArrayList.get(0).getMobile());
        editTextFirstName.setText(accountDataArrayList.get(0).getFirstname());
        editTextLastName.setText(accountDataArrayList.get(0).getLastname());
        editTextEmailAddress.setText(accountDataArrayList.get(0).getEmail());*/
        editTextMobileNumber.setText(mLockatedPreferences.getContactNumber());
        editTextFirstName.setText(mLockatedPreferences.getPersonalName());
        editTextLastName.setText(mLockatedPreferences.getLastName());
        editTextEmailAddress.setText(mLockatedPreferences.getEmailAddress());
        textViewSubmit.setOnClickListener(this);
        textSendOtp.setOnClickListener(this);
        // disableButton();
    }

   /* private void disableButton() {
        textViewSubmit.setEnabled(false);
        textViewSubmit.setTextColor(ContextCompat.getColor(getActivity(), R.color.secondary_text));
    }

    private void enableButton() {
        textViewSubmit.setEnabled(true);
        textViewSubmit.setTextColor(ContextCompat.getColor(getActivity(), R.color.primary_text));
    }*/


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.textViewSubmit:
                onSubmitButtonClicked();
                break;
        }
    }

    private void onSubmitButtonClicked() {
        strFname = editTextFirstName.getText().toString();
        strLname = editTextLastName.getText().toString();
        strEmailId = editTextEmailAddress.getText().toString();
        strMobileNumber = editTextMobileNumber.getText().toString();
        if (TextUtils.isEmpty(strMobileNumber)) {
            Utilities.showToastMessage(getActivity(), "Please Enter Valid Mobile Number");
        } else if (TextUtils.isEmpty(strFname)) {
            Utilities.showToastMessage(getActivity(), "Please Enter  First Name");
        } else if (TextUtils.isEmpty(strLname)) {
            Utilities.showToastMessage(getActivity(), "Please Enter  Last Name");
        } else if (TextUtils.isEmpty(strEmailId)) {
            Utilities.showToastMessage(getActivity(), "Please Enter  Emai");
        } else if (strMobileNumber.length() < 10) {
            Utilities.showToastMessage(getActivity(), "Please Enter Valid Mobile Number");

        } else {
            if (ConnectionDetector.isConnectedToInternet(getActivity())) {
               /* mProgressDialog = ProgressDialog.show(getActivity(), "", "Please Wait...", false);
                mProgressDialog.show();*/
                JSONObject jsonObjectFields = new JSONObject();
                JSONObject jsonObjectMain = new JSONObject();
                try {
                    jsonObjectFields.put("email", strEmailId);
                    jsonObjectFields.put("mobile", strMobileNumber);
                    jsonObjectFields.put("firstname", strFname);
                    jsonObjectFields.put("lastname", strLname);
                    jsonObjectMain.put("user", jsonObjectFields);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                String url = ApplicationURL.updatePersonalInformatio + mLockatedPreferences.getLockatedToken();
                mLockatedVolleyRequestQueue = LockatedVolleyRequestQueue.getInstance(getActivity());
                mLockatedVolleyRequestQueue.sendRequest(REQUEST_CHANGE_INFO, Request.Method.PUT, url, jsonObjectMain, this, this);
            } else {
                Utilities.showToastMessage(getActivity(), getActivity().getResources().getString(R.string.internet_connection_error));
            }
        }
    }

    @Override
    public void onErrorResponse(VolleyError error) {
        if (getActivity() != null) {
            LockatedRequestError.onRequestError(getActivity(), error);
        }
    }

    @Override
    public void onResponse(JSONObject response) {
        Log.w("Update PInfo response", "" + response);
        JSONObject jsonObject = (JSONObject) response;
        try {
            if (jsonObject.has("id") && jsonObject.length() > 0) {
                Intent intent = new Intent(getActivity(), HomeActivity.class);
                intent.putExtra("EditPersonalInformationFragment", "EditPersonalInformationFragment");
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                //onPrefrenceDataStore(jsonObject);
                startActivity(intent);
                getActivity().finish();
            } else if (jsonObject.has("message") && jsonObject.has("otp")) {
                // Utilities.showToastMessage(getActivity(), jsonObject.getString("message"));
                OTPGeneratorFragment otpGeneratorFragment = new OTPGeneratorFragment();
                Bundle bundle = new Bundle();
                bundle.putString("mobileNumber", editTextMobileNumber.getText().toString());
                bundle.putString("EditPersonalInformationFragment", "EditPersonalInformationFragment");
                otpGeneratorFragment.setArguments(bundle);
                getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.personalInfoEditContainer, otpGeneratorFragment).commitAllowingStateLoss();

            } else if (jsonObject.has("error") && jsonObject.has("code")) {
                Utilities.showToastMessage(getActivity(), jsonObject.getString("error"));

            }
        } catch (JSONException e1) {
            e1.printStackTrace();
        }
    }

    private void onPrefrenceDataStore(JSONObject jsonObject) {
        try {
            mLockatedPreferences.setPersonalName(jsonObject.getString("firstname"));
            mLockatedPreferences.setLastName(jsonObject.getString("lastname"));
            mLockatedPreferences.setEmailAddress(jsonObject.getString("email"));
            mLockatedPreferences.setContactNumber(jsonObject.getString("mobile"));


        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public FragmentManager setFragmentManager(FragmentManager fragmentManager) {
        this.fragmentManager = fragmentManager;
        return fragmentManager;
    }
}
