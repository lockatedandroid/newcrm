
package com.android.lockated.model.AdminModel.AdminClassified;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class PublishClassified {

    @SerializedName("classifieds")
    @Expose
    private ArrayList<Classifieds> classifieds = new ArrayList<Classifieds>();

    /**
     * 
     * @return
     *     The classifieds
     */
    public ArrayList<Classifieds> getClassifieds() {
        return classifieds;
    }

    /**
     * 
     * @param classifieds
     *     The classifieds
     */
    public void setClassifieds(ArrayList<Classifieds> classifieds) {
        this.classifieds = classifieds;
    }

}
