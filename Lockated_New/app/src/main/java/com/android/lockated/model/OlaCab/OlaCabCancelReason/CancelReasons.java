
package com.android.lockated.model.OlaCab.OlaCabCancelReason;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class CancelReasons {

    @SerializedName("micro")
    @Expose
    private ArrayList<String> micro = new ArrayList<String>();
    @SerializedName("mini")
    @Expose
    private ArrayList<String> mini = new ArrayList<String>();
    @SerializedName("sedan")
    @Expose
    private ArrayList<String> sedan = new ArrayList<String>();
    @SerializedName("prime")
    @Expose
    private ArrayList<String> prime = new ArrayList<String>();
    @SerializedName("auto")
    @Expose
    private ArrayList<String> auto = new ArrayList<String>();

    /**
     * 
     * @return
     *     The micro
     */
    public ArrayList<String> getMicro() {
        return micro;
    }

    /**
     * 
     * @param micro
     *     The micro
     */
    public void setMicro(ArrayList<String> micro) {
        this.micro = micro;
    }

    /**
     * 
     * @return
     *     The mini
     */
    public ArrayList<String> getMini() {
        return mini;
    }

    /**
     * 
     * @param mini
     *     The mini
     */
    public void setMini(ArrayList<String> mini) {
        this.mini = mini;
    }

    /**
     * 
     * @return
     *     The sedan
     */
    public ArrayList<String> getSedan() {
        return sedan;
    }

    /**
     * 
     * @param sedan
     *     The sedan
     */
    public void setSedan(ArrayList<String> sedan) {
        this.sedan = sedan;
    }

    /**
     * 
     * @return
     *     The prime
     */
    public ArrayList<String> getPrime() {
        return prime;
    }

    /**
     * 
     * @param prime
     *     The prime
     */
    public void setPrime(ArrayList<String> prime) {
        this.prime = prime;
    }

    /**
     * 
     * @return
     *     The auto
     */
    public ArrayList<String> getAuto() {
        return auto;
    }

    /**
     * 
     * @param auto
     *     The auto
     */
    public void setAuto(ArrayList<String> auto) {
        this.auto = auto;
    }

}
