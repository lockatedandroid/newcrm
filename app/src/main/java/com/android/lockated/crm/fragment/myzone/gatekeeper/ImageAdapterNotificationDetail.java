package com.android.lockated.crm.fragment.myzone.gatekeeper;

import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.android.lockated.R;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class ImageAdapterNotificationDetail extends PagerAdapter {
    private final ArrayList<String> documents;
    private final Context context;
    private final LayoutInflater mLayoutInflater;

    public ImageAdapterNotificationDetail(Context context, ArrayList<String> documents, boolean b) {
        this.documents = documents;
        this.context = context;
        mLayoutInflater = LayoutInflater.from(context);
    }

    @Override
    public int getCount() {
        return documents.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }

    @Override
    public Object instantiateItem(ViewGroup container, final int position) {
        View itemView = mLayoutInflater.inflate(R.layout.multiple_image, container, false);
        ImageView imageView = (ImageView) itemView.findViewById(R.id.mImageEdit);
        String strImage = documents.get(position);
        Picasso.with(context).load(strImage).fit().placeholder(R.drawable.loading).into(imageView);

        container.addView(itemView);

        return itemView;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((LinearLayout) object);
    }

}