package com.android.lockated.crm.activity;

import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.android.lockated.R;
import com.android.lockated.model.Banner.Society.SocietyBanner_all;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

/**
 * Created by Haven on 7/16/2016.
 */
public class SocietyBannerAdapter extends PagerAdapter {

    private final String name;
    Context context;
    boolean clickable;
    LayoutInflater mLayoutInflater;
    ArrayList<SocietyBanner_all> bannerArrayList;

    public SocietyBannerAdapter(Context context, ArrayList<SocietyBanner_all> bannerArrayList, boolean clickable, String name) {
        this.context = context;
        this.name = name;
        this.clickable = clickable;
        this.bannerArrayList = bannerArrayList;
        mLayoutInflater = LayoutInflater.from(context);
    }

    @Override
    public int getCount() {
        return bannerArrayList.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == ((LinearLayout) object);
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {

        View itemView = mLayoutInflater.inflate(R.layout.banner_item, container, false);
        ImageView imageView = (ImageView) itemView.findViewById(R.id.imageView);
        Picasso.with(context).load(bannerArrayList.get(position).getMedium())
                .placeholder(R.drawable.loading).fit().into(imageView);
        container.addView(itemView);
       /* if (!name.equals("crm")) {
            if (clickable) {
                imageView.setTag(position);
                imageView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        int position = (int) v.getTag();
                        Intent intent = new Intent(context, HomeActivity.class);
                        intent.putExtra("offersOtypeId", Utilities.getIdPosition(bannerArrayList.get(position).getBtypeId()));
                        intent.putExtra("otype_id", bannerArrayList.get(position).getBtypeId());
                        intent.putExtra("taxon_id", bannerArrayList.get(position).getTaxonId());
                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        context.startActivity(intent);
                    }
                });
            }
        }*/
        return itemView;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((LinearLayout) object);
    }

}
