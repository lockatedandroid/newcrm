package com.android.lockated.categories.olacabs.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.lockated.R;
import com.android.lockated.categories.olacabs.activity.OlaEachActivity;
import com.android.lockated.model.OlaCab.OlaDetailModel.ThirdpartyTransaction;
import com.android.lockated.utils.Utilities;

import java.util.ArrayList;

public class OlaBookAdapter extends RecyclerView.Adapter<OlaBookAdapter.OlaViewHolder> {

    Context context;
    ArrayList<ThirdpartyTransaction> thirdpartyTransactions;

    public OlaBookAdapter(Context context, ArrayList<ThirdpartyTransaction> thirdpartyTransactions)
    {
        this.context = context;
        this.thirdpartyTransactions = thirdpartyTransactions;
    }
    @Override
    public OlaBookAdapter.OlaViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.ola_each_recycler, parent, false);
        return new OlaViewHolder(itemView);
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(final OlaBookAdapter.OlaViewHolder holder, final int position) {

        OlaEachActivity.thirdpartyTransactions = thirdpartyTransactions;
        //holder.txt_ola_cab.setText(""+thirdpartyTransactions.get(position).getTpdetails().getCabType());
        //holder.txt_ola_status.setText(""+thirdpartyTransactions.get(position).getTpdetails().getBookingStatus());

        holder.txt_ola_cab.setText("Ola");
        holder.txt_ola_status.setText(Utilities.dateTime_AMPM_Format(thirdpartyTransactions.get(position).getUpdatedAt()));

        if(thirdpartyTransactions.get(position).getTpdetails().getBookingStatus() != null)
        {
            if (thirdpartyTransactions.get(position).getDeliverylabel().equals("BOOKING_CANCELLED")) {
                Log.e("txt_ola_book","setText before");
                holder.txt_ola_book.setText("Cancelled");
                Log.e("txt_ola_book","setText after");
                //holder.txt_ola_book.setTextColor(Color.RED);
            } else if (thirdpartyTransactions.get(position).getDeliverylabel().equals("Booked")) {
                holder.txt_ola_book.setText("Booked");
                //holder.txt_ola_book.setTextColor(Color.BLACK);
            } else if (thirdpartyTransactions.get(position).getDeliverylabel().equals("COMPLETED")) {
                holder.txt_ola_book.setText("Completed");
                //holder.txt_ola_book.setTextColor(Color.BLACK);
            }
        }
        else
        {
            holder.txt_ola_book.setText("Cancelled");
            //holder.txt_ola_book.setTextColor(Color.RED);
        }
        holder.mZRecyclerLLayout.setOnClickListener(new View.OnClickListener() {


            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context,OlaEachActivity.class);
                intent.putExtra("Position",position);
                context.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return thirdpartyTransactions.size();
    }


    public class OlaViewHolder extends RecyclerView.ViewHolder {

        TextView txt_ola_cab,txt_ola_status,txt_ola_book;
        LinearLayout mZRecyclerLLayout;
        public OlaViewHolder(View itemView) {
            super(itemView);
            txt_ola_cab = (TextView) itemView.findViewById(R.id.txt_ola_cab);
            txt_ola_status = (TextView) itemView.findViewById(R.id.txt_ola_status);
            txt_ola_book = (TextView) itemView.findViewById(R.id.txt_ola_book);

            mZRecyclerLLayout = (LinearLayout) itemView.findViewById(R.id.mZRecyclerLLayout);
        }
    }
}
