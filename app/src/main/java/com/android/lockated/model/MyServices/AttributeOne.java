
package com.android.lockated.model.MyServices;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class AttributeOne {

    @SerializedName("id")
    @Expose
    private int id;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("service_metadata_id")
    @Expose
    private int serviceMetadataId;
    @SerializedName("active")
    @Expose
    private int active;
    @SerializedName("created_at")
    @Expose
    private String createdAt;
    @SerializedName("updated_at")
    @Expose
    private String updatedAt;
    @SerializedName("approx_charge")
    @Expose
    private String approxCharge;

    /**
     * @return The id
     */
    public int getId() {
        return id;
    }

    /**
     * @param id The id
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * @return The name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name The name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return The serviceMetadataId
     */
    public int getServiceMetadataId() {
        return serviceMetadataId;
    }

    /**
     * @param serviceMetadataId The service_metadata_id
     */
    public void setServiceMetadataId(int serviceMetadataId) {
        this.serviceMetadataId = serviceMetadataId;
    }

    /**
     * @return The active
     */
    public int getActive() {
        return active;
    }

    /**
     * @param active The active
     */
    public void setActive(int active) {
        this.active = active;
    }

    /**
     * @return The createdAt
     */
    public String getCreatedAt() {
        return createdAt;
    }

    /**
     * @param createdAt The created_at
     */
    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    /**
     * @return The updatedAt
     */
    public String getUpdatedAt() {
        return updatedAt;
    }

    /**
     * @param updatedAt The updated_at
     */
    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    /**
     * @return The approxCharge
     */
    public String getApproxCharge() {
        return approxCharge;
    }

    /**
     * @param approxCharge The approx_charge
     */
    public void setApproxCharge(String approxCharge) {
        this.approxCharge = approxCharge;
    }

}
