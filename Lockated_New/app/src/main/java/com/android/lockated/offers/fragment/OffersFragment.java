package com.android.lockated.offers.fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.android.lockated.LockatedApplication;
import com.android.lockated.R;
import com.android.lockated.network.ConnectionDetector;
import com.android.lockated.offers.adapter.OffersAdapter;
import com.android.lockated.model.Offers.OffersModel;
import com.android.lockated.preferences.LockatedPreferences;
import com.android.lockated.request.LockatedRequestError;
import com.android.lockated.request.LockatedVolleyRequestQueue;
import com.android.lockated.utils.ApplicationURL;
import com.android.lockated.utils.Utilities;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;


public class OffersFragment extends Fragment implements Response.ErrorListener, Response.Listener<JSONObject> {

    ProgressBar progressBar;
    private TextView errorMsg;
    OffersAdapter offersAdapter;
    private LockatedPreferences mLockatedPreferences;
    private ArrayList<OffersModel> offersModelArrayList;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mLockatedPreferences = new LockatedPreferences(getActivity());
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View offerFragmentView = inflater.inflate(R.layout.fragment_offers, container, false);
        init(offerFragmentView);
        return offerFragmentView;
    }

    @Override
    public void onResume() {
        super.onResume();
        Utilities.ladooIntegration(getActivity(), getString(R.string.offers_list_screen));
        LockatedApplication.getInstance().trackScreenView(getString(R.string.offers_list_screen));
        Utilities.lockatedGoogleAnalytics(getString(R.string.offers_list_screen),
                getString(R.string.visited), getString(R.string.offers_list_screen));
    }

    public void init(View offerFragmentView) {

        offersModelArrayList = new ArrayList<>();
        errorMsg = (TextView) offerFragmentView.findViewById(R.id.noNotices);
        progressBar = (ProgressBar) offerFragmentView.findViewById(R.id.mProgressBarView);
        RecyclerView landingContainer = (RecyclerView) offerFragmentView.findViewById(R.id.mLandingContainer);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        landingContainer.setLayoutManager(linearLayoutManager);
        landingContainer.setHasFixedSize(true);
        offersAdapter = new OffersAdapter(getActivity(), offersModelArrayList);
        landingContainer.setAdapter(offersAdapter);
        getOffers();
    }

    public void getOffers() {
        if (getActivity() != null) {
            if (ConnectionDetector.isConnectedToInternet(getActivity())) {
                progressBar.setVisibility(View.VISIBLE);
                String REQUEST_TAG = "OffersActivity";
                String url = ApplicationURL.getOffersUrl + mLockatedPreferences.getLockatedToken();
                LockatedVolleyRequestQueue lockatedVolleyRequestQueue = LockatedVolleyRequestQueue.getInstance(getActivity());
                lockatedVolleyRequestQueue.sendRequest(REQUEST_TAG, Request.Method.GET, url, null, this, this);
            } else {
                Utilities.showToastMessage(getActivity(), this.getResources().getString(R.string.internet_connection_error));
            }
        }
    }

    @Override
    public void onErrorResponse(VolleyError error) {
        progressBar.setVisibility(View.GONE);
        if (getActivity() != null) {
            LockatedRequestError.onRequestError(getActivity(), error);
        }
    }

    @Override
    public void onResponse(JSONObject response) {
        progressBar.setVisibility(View.GONE);
        if (getActivity() != null) {
            try {
                if (response.has("code") && response.getInt("code") == 200) {
                    if (response.has("offers")) {
                        if (response.getJSONArray("offers").length() > 0) {
                            JSONArray jsonArray = response.getJSONArray("offers");
                            Gson gson = new Gson();
                            for (int i = 0; i < jsonArray.length(); i++) {
                                OffersModel offersModel = gson.fromJson(jsonArray.getJSONObject(i).toString(), OffersModel.class);
                                offersModelArrayList.add(offersModel);
                            }
                            offersAdapter.notifyDataSetChanged();
                        }
                    } else {
                        errorMsg.setVisibility(View.VISIBLE);
                        errorMsg.setText("No offers available currently");
                    }
                } else {
                    errorMsg.setVisibility(View.VISIBLE);
                    errorMsg.setText("No offers available currently");
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

}
