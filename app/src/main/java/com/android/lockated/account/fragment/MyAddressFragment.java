package com.android.lockated.account.fragment;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.android.lockated.Interfaces.INewAddressAdded;
import com.android.lockated.Interfaces.IRecyclerItemClickListener;
import com.android.lockated.LockatedApplication;
import com.android.lockated.R;
import com.android.lockated.account.adapter.MyAddressAdapter;
import com.android.lockated.information.AccountController;
import com.android.lockated.model.MyAddressData;
import com.android.lockated.network.ConnectionDetector;
import com.android.lockated.preferences.LockatedPreferences;
import com.android.lockated.request.LockatedJSONObjectRequest;
import com.android.lockated.request.LockatedRequestError;
import com.android.lockated.request.LockatedVolleyRequestQueue;
import com.android.lockated.utils.ApplicationURL;
import com.android.lockated.utils.Utilities;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class MyAddressFragment extends Fragment implements Response.Listener, Response.ErrorListener, View.OnClickListener, INewAddressAdded, IRecyclerItemClickListener {

    private View mMyAddressView;
    private int viewId;

    private ProgressDialog mProgressDialog;
    private ProgressBar mProgressBarView;

    private MyAddressAdapter mMyAddressAdapter;
    private RequestQueue mQueue;
    private AccountController mAccountController;
    private LockatedPreferences mLockatedPreferences;
    private LockatedJSONObjectRequest mLockatedJSONObjectRequest;

    private ArrayList<MyAddressData> mMyAddressDatas;

    public static final String GET_REQUEST_TAG = "GetMyAddresses";
    public static final String REQUEST_TAG = "MyAddressFragment";

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mMyAddressView = inflater.inflate(R.layout.fragment_my_address, container, false);
        init();
        Utilities.ladooIntegration(getActivity(), getActivity().getString(R.string.my_addresses_screen));
        return mMyAddressView;
    }

    public void setViewId(int i) {
        viewId = i;
    }

    @Override
    public void onResume() {
        super.onResume();
        LockatedApplication.getInstance().trackScreenView(getActivity().getString(R.string.my_addresses_screen));
    }

    private void init() {
        mMyAddressDatas = new ArrayList<>();
        mAccountController = AccountController.getInstance();
        mLockatedPreferences = new LockatedPreferences(getActivity());

        mProgressBarView = (ProgressBar) mMyAddressView.findViewById(R.id.mProgressBarView);
        RecyclerView mRecyclerView = (RecyclerView) mMyAddressView.findViewById(R.id.mRecyclerView);
        TextView mButtonAddNewAddress = (TextView) mMyAddressView.findViewById(R.id.mButtonAddNewAddress);
        mButtonAddNewAddress.setOnClickListener(this);

        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        mRecyclerView.setLayoutManager(mLayoutManager);
        mRecyclerView.setHasFixedSize(true);
        mMyAddressAdapter = new MyAddressAdapter(mMyAddressDatas, this);
        mRecyclerView.setAdapter(mMyAddressAdapter);

        getMyAddressData();
    }

    private void getMyAddressData() {
        if (ConnectionDetector.isConnectedToInternet(getActivity())) {
            mProgressBarView.setVisibility(View.VISIBLE);
            LockatedVolleyRequestQueue lockatedVolleyRequestQueue = LockatedVolleyRequestQueue.getInstance(getActivity());
            lockatedVolleyRequestQueue.sendRequest(GET_REQUEST_TAG, Request.Method.GET, ApplicationURL.getMyAddressesUrl + mLockatedPreferences.getLockatedToken(), null, this, this);
        } else {
            Utilities.showToastMessage(getActivity(), getActivity().getResources().getString(R.string.internet_connection_error));
        }
    }

    private void onAddNewAddressClicked() {
        FragmentManager fragmentManager = getChildFragmentManager();
        AddNewAddressFragment addNewAddressFragment = new AddNewAddressFragment();
        addNewAddressFragment.setListener(this);
        addNewAddressFragment.setAddressDataOnEdit(0, null);
        addNewAddressFragment.show(fragmentManager, "AddNewAddressFragment");
    }

    private void onRemoveClicked(int position) {
        if (ConnectionDetector.isConnectedToInternet(getActivity())) {
            mProgressDialog = ProgressDialog.show(getActivity(), "", "Please Wait...", false);
            mProgressDialog.show();
            LockatedVolleyRequestQueue lockatedVolleyRequestQueue = LockatedVolleyRequestQueue.getInstance(getActivity());
            lockatedVolleyRequestQueue.sendRequest(REQUEST_TAG, Request.Method.DELETE,
                    ApplicationURL.deleteAddressUrl + "?address_id=" + mMyAddressDatas.get(position).getAddressId() + "&token=" + mLockatedPreferences.getLockatedToken(), null, this, this);
        } else {
            Utilities.showToastMessage(getActivity(), getActivity().getResources().getString(R.string.internet_connection_error));
        }
    }

    private void onEditClicked(int position) {
        FragmentManager fragmentManager = getChildFragmentManager();
        AddNewAddressFragment addNewAddressFragment = new AddNewAddressFragment();
        addNewAddressFragment.setListener(this);
        addNewAddressFragment.setAddressDataOnEdit(1, mMyAddressDatas.get(position));
        addNewAddressFragment.show(fragmentManager, "AddNewAddressFragment");
    }

    @Override
    public void onErrorResponse(VolleyError error) {
        if (mProgressDialog != null) {
            mProgressDialog.dismiss();
        }
        mProgressBarView.setVisibility(View.GONE);
        if (getActivity() != null) {
            LockatedRequestError.onRequestError(getActivity(), error);
        }
    }

    @Override
    public void onResponse(Object response) {
        if (mProgressDialog != null) {
            mProgressDialog.dismiss();
        }
        mProgressBarView.setVisibility(View.GONE);
        if (getActivity() != null) {
            JSONObject jsonObject = (JSONObject) response;
            try {
                mMyAddressDatas.clear();
                JSONArray jsonArray = jsonObject.getJSONArray("addresses");
                for (int i = 0; i < jsonArray.length(); i++) {
                    JSONObject addressObject = jsonArray.getJSONObject(i);
                    MyAddressData myAddressData = new MyAddressData(addressObject);
                    mMyAddressDatas.add(myAddressData);
                }

                mMyAddressAdapter.notifyDataSetChanged();
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.mButtonAddNewAddress:
                onAddNewAddressClicked();
                break;
        }
    }

    @Override
    public void onRecyclerItemClick(View view, int position) {
        switch (view.getId()) {
            case R.id.mButtonMySavedAddressRemove:
                onRemoveClicked(position);
                break;

            case R.id.mButtonMySavedAddressEdit:
                onEditClicked(position);
                break;

            case R.id.mLinearLayoutAddresses:
                onRowClicked(position);
                break;

        }
    }

    private void onRowClicked(int position) {
        if (viewId == 1) {
            if (getActivity() != null) {
                ((LockatedApplication) getActivity().getApplicationContext()).setMyAddressData(mMyAddressDatas.get(position));
                getActivity().getSupportFragmentManager().popBackStack();
            }
        }
    }

    @Override
    public void onNewAddressAdded(ArrayList<MyAddressData> addressDatas) {
        mMyAddressDatas.clear();
        mAccountController.getmAccountDataList().get(0).getmMyAddressDataList().clear();

        mAccountController.getmAccountDataList().get(0).setmMyAddressDataList(addressDatas);

        for (int i = 0; i < addressDatas.size(); i++) {
            mMyAddressDatas.add(addressDatas.get(i));
        }

        mMyAddressAdapter.notifyDataSetChanged();
    }
}