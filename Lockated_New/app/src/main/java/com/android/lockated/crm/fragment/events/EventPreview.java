package com.android.lockated.crm.fragment.events;

import android.app.Dialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.lockated.R;

public class EventPreview extends DialogFragment implements View.OnClickListener {

    private View EventNoticePreviewView;
    private TextView mSubject;
    private TextView mDescription;
    private TextView mShareWith;
    private TextView mStartDate;

    private TextView mStartTime;
    private TextView mEndtDate;
    private TextView mEndTime;
    private TextView mEdit;
    Window window;

    private TextView mRSVP;

    @Override
    public void onStart() {
        super.onStart();
        window = getDialog().getWindow();
        WindowManager.LayoutParams windowParams = window.getAttributes();
        windowParams.dimAmount = 0.75f;
        windowParams.flags |= WindowManager.LayoutParams.FLAG_DIM_BEHIND;
        window.setAttributes(windowParams);
        window.setBackgroundDrawableResource(android.R.color.white);
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        EventNoticePreviewView = getActivity().getLayoutInflater().inflate(R.layout.fragment_event_notice_preview,
                new LinearLayout(getActivity()), false);

        init(EventNoticePreviewView);
        Dialog builder = new Dialog(getActivity());
        builder.requestWindowFeature(Window.FEATURE_NO_TITLE);
        builder.getWindow().setBackgroundDrawable(new ColorDrawable(Color.WHITE));

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(builder.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.WRAP_CONTENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;

        builder.show();
        builder.getWindow().setAttributes(lp);
        builder.setContentView(EventNoticePreviewView);
        return builder;

    }

    private void init(View EventNoticePreviewView) {

        mSubject = (TextView) EventNoticePreviewView.findViewById(R.id.mSubject);
        mDescription = (TextView) EventNoticePreviewView.findViewById(R.id.mDescription);
        mShareWith = (TextView) EventNoticePreviewView.findViewById(R.id.mShareWith);
        mStartDate = (TextView) EventNoticePreviewView.findViewById(R.id.mStartDate);
        mEndtDate = (TextView) EventNoticePreviewView.findViewById(R.id.mEndDate);
        mStartTime = (TextView) EventNoticePreviewView.findViewById(R.id.mStartTime);
        mEndTime = (TextView) EventNoticePreviewView.findViewById(R.id.mEndTime);
        mEdit = (TextView) EventNoticePreviewView.findViewById(R.id.mEdit);
        mRSVP = (TextView) EventNoticePreviewView.findViewById(R.id.mRSVP);

        mSubject.setText(getArguments().getString("subject"));
        mDescription.setText(getArguments().getString("description"));
        mShareWith.setText(getArguments().getString("Location"));
        mStartDate.setText(getArguments().getString("startdate"));
        mEndtDate.setText(getArguments().getString("enddate"));
        mStartTime.setText(getArguments().getString("starttime"));
        mEndTime.setText(getArguments().getString("endtime"));
        mEdit.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.mEdit:
                dismiss();
                break;
        }
    }

}
