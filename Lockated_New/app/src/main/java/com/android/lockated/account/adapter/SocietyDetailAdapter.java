package com.android.lockated.account.adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Filter;
import android.widget.TextView;

import com.android.lockated.R;
import com.android.lockated.model.SocietyData;

import java.util.ArrayList;

public class SocietyDetailAdapter extends ArrayAdapter<SocietyData>
{
    private int mLayoutResourceId;
    private ArrayList<SocietyData> mSocietyDatas;

    public SocietyDetailAdapter(Context context, int resource, ArrayList<SocietyData> data)
    {
        super(context, resource, data);
        mSocietyDatas = data;
        mLayoutResourceId = resource;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent)
    {
        final SocietyData societyData = mSocietyDatas.get(position);

        ItemHolder itemHolder;
        if (convertView == null)
        {
            LayoutInflater mLayoutInflater = ((Activity) super.getContext()).getLayoutInflater();
            convertView = mLayoutInflater.inflate(mLayoutResourceId, parent, false);

            itemHolder = new ItemHolder();

            itemHolder.mTextSocietyName = (TextView) convertView.findViewById(R.id.mTextSocietyName);

            convertView.setTag(itemHolder);
        } else
        {
            itemHolder = (ItemHolder) convertView.getTag();
        }

        itemHolder.mTextSocietyName.setText(societyData.getBuilding_name());

        return convertView;
    }

    @Override
    public Filter getFilter()
    {
        return super.getFilter();
    }

    class ItemHolder
    {
        TextView mTextSocietyName;
    }
}
