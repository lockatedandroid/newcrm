package com.android.lockated.account.fragment;

import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v4.content.ContextCompat;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import com.android.lockated.Interfaces.IFamilyMemberAdded;
import com.android.lockated.R;
import com.android.lockated.model.FamilyData;
import com.android.lockated.network.ConnectionDetector;
import com.android.lockated.preferences.LockatedPreferences;
import com.android.lockated.request.LockatedRequestError;
import com.android.lockated.request.LockatedVolleyRequestQueue;
import com.android.lockated.utils.ApplicationURL;
import com.android.lockated.utils.Utilities;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;

public class AddFamilyDetailFragment extends DialogFragment implements View.OnClickListener, AdapterView.OnItemSelectedListener, Response.Listener, Response.ErrorListener
{
    private View mAddFamilyDetailView;

    private TextView mTextViewFamilyMemberName;
    private TextView mTextViewFamilyMemberRelationship;
    private TextView mTextViewFamilyMemberMobile;
    private TextView mTextViewFamilyMemberDob;
    private TextView mTextViewAnniversary;
    private TextView mTextViewFamilyMemberAnniversary;
    private TextView mButtonAddAddress;

    private Spinner mSpinnerFamilyMemberGender;
    private Spinner mSpinnerFamilyMemberMaritalStatus;

    private EditText mEditTextFamilyMemberName;
    private EditText mEditTextFamilyMemberRelationship;
    private EditText mEditTextFamilyMemberNumber;

    private int mGenderId;
    private int requestId;
    private int mMaritalStatusId;

    private RequestQueue mQueue;
    private ProgressDialog mProgressDialog;
    private LockatedPreferences mLockatedPreferences;
    private LockatedVolleyRequestQueue mLockatedVolleyRequestQueue;

    private IFamilyMemberAdded mINewAddressAdded;
    private FamilyData mFamilyDataRow;
    private ArrayList<FamilyData> mFamilyDatas;
    private int mPosition;

    public static final String REQUEST_TAG = "AddNewAddressFragment";

    public AddFamilyDetailFragment()
    {

    }

    public void setFamilyDataOnEdit(int id, FamilyData data, int position)
    {
        requestId = id;
        mFamilyDataRow = data;
        mPosition = position;
    }

    public void setListener(IFamilyMemberAdded listener)
    {
        mINewAddressAdded = listener;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setStyle(STYLE_NO_TITLE, 0);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        mAddFamilyDetailView = inflater.inflate(R.layout.fragment_add_new_family, container);
        init();
        return mAddFamilyDetailView;
    }

    @Override
    public void onStart()
    {
        super.onStart();
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(getDialog().getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        getDialog().getWindow().setAttributes(lp);
    }

    private void init()
    {
        mFamilyDatas = new ArrayList<>();
        mLockatedPreferences = new LockatedPreferences(getActivity());

        mTextViewFamilyMemberName = (TextView) mAddFamilyDetailView.findViewById(R.id.mTextViewFamilyMemberName);
        mTextViewFamilyMemberRelationship = (TextView) mAddFamilyDetailView.findViewById(R.id.mTextViewFamilyMemberRelationship);
        mTextViewFamilyMemberMobile = (TextView) mAddFamilyDetailView.findViewById(R.id.mTextViewFamilyMemberMobile);
        mTextViewFamilyMemberDob = (TextView) mAddFamilyDetailView.findViewById(R.id.mTextViewFamilyMemberDob);
        mTextViewAnniversary = (TextView) mAddFamilyDetailView.findViewById(R.id.mTextViewAnniversary);
        mTextViewFamilyMemberAnniversary = (TextView) mAddFamilyDetailView.findViewById(R.id.mTextViewFamilyMemberAnniversary);

        TextView mButtonAddAddressCancel = (TextView) mAddFamilyDetailView.findViewById(R.id.mButtonAddAddressCancel);
        mButtonAddAddress = (TextView) mAddFamilyDetailView.findViewById(R.id.mButtonAddAddress);

        mSpinnerFamilyMemberGender = (Spinner) mAddFamilyDetailView.findViewById(R.id.mSpinnerFamilyMemberGender);
        mSpinnerFamilyMemberMaritalStatus = (Spinner) mAddFamilyDetailView.findViewById(R.id.mSpinnerFamilyMemberMaritalStatus);

        mEditTextFamilyMemberName = (EditText) mAddFamilyDetailView.findViewById(R.id.mEditTextFamilyMemberName);
        mEditTextFamilyMemberRelationship = (EditText) mAddFamilyDetailView.findViewById(R.id.mEditTextFamilyMemberRelationship);
        mEditTextFamilyMemberNumber = (EditText) mAddFamilyDetailView.findViewById(R.id.mEditTextFamilyMemberNumber);

        if (requestId == 0)
        {
            mButtonAddAddress.setText(R.string.add);
        }
        else
        {
            mButtonAddAddress.setText(R.string.update);
        }

        mSpinnerFamilyMemberGender.setOnItemSelectedListener(this);
        mSpinnerFamilyMemberMaritalStatus.setOnItemSelectedListener(this);
        mTextViewFamilyMemberDob.setOnClickListener(this);
        mTextViewFamilyMemberAnniversary.setOnClickListener(this);
        mButtonAddAddress.setOnClickListener(this);
        mButtonAddAddressCancel.setOnClickListener(this);

        hideName();
        hideRelationship();
        hideMobileNumber();
        hideAnniversaryField();
        setEditTextListener();
        disableAddButton();
        setEditedData();
    }

    private void setEditedData()
    {
        if (requestId == 1)
        {
            if (mFamilyDataRow.getGender().equalsIgnoreCase("M"))
            {
                mSpinnerFamilyMemberGender.setSelection(1);
            }
            else if (mFamilyDataRow.getGender().equalsIgnoreCase("F"))
            {
                mSpinnerFamilyMemberGender.setSelection(2);
            }
            else
            {
                mSpinnerFamilyMemberGender.setSelection(0);
            }

            if (mFamilyDataRow.getMarried().equalsIgnoreCase("Y"))
            {
                mSpinnerFamilyMemberMaritalStatus.setSelection(1);
                showAnniversaryField();
            }
            else if(mFamilyDataRow.getMarried().equalsIgnoreCase("N"))
            {
                mSpinnerFamilyMemberMaritalStatus.setSelection(2);
                hideAnniversaryField();
            }
            else
            {
                mSpinnerFamilyMemberMaritalStatus.setSelection(0);
            }

            if (mFamilyDataRow.getDob() == "null")
            {
                mTextViewFamilyMemberDob.setText(R.string.dob);
            }
            else
            {
                mTextViewFamilyMemberDob.setText(mFamilyDataRow.getDob());
            }

            mEditTextFamilyMemberName.setText(mFamilyDataRow.getName());
            mEditTextFamilyMemberRelationship.setText(mFamilyDataRow.getRelationship());
            mEditTextFamilyMemberNumber.setText(mFamilyDataRow.getMobile());
        }
    }

    private void hideAnniversaryField()
    {
        mTextViewFamilyMemberAnniversary.setText(R.string.anniversary);
        mTextViewAnniversary.setVisibility(View.GONE);
        mTextViewFamilyMemberAnniversary.setVisibility(View.GONE);
    }

    private void showAnniversaryField()
    {
        mTextViewFamilyMemberAnniversary.setText(R.string.anniversary);
        mTextViewAnniversary.setVisibility(View.VISIBLE);
        mTextViewFamilyMemberAnniversary.setVisibility(View.VISIBLE);
    }

    private void disableAddButton()
    {
        mButtonAddAddress.setEnabled(false);
        mButtonAddAddress.setTextColor(ContextCompat.getColor(getActivity(), R.color.secondary_text));
    }

    private void enableAddButton()
    {
        mButtonAddAddress.setEnabled(true);
        mButtonAddAddress.setTextColor(ContextCompat.getColor(getActivity(), R.color.primary));
    }

    private void hideName()
    {
        mTextViewFamilyMemberName.setVisibility(View.INVISIBLE);
    }

    private void hideRelationship()
    {
        mTextViewFamilyMemberRelationship.setVisibility(View.INVISIBLE);
    }

    private void hideMobileNumber()
    {
        mTextViewFamilyMemberMobile.setVisibility(View.INVISIBLE);
    }

    private void showName()
    {
        mTextViewFamilyMemberName.setVisibility(View.VISIBLE);
    }

    private void showRelationship()
    {
        mTextViewFamilyMemberRelationship.setVisibility(View.VISIBLE);
    }

    private void showMobileNumber()
    {
        mTextViewFamilyMemberMobile.setVisibility(View.VISIBLE);
    }

    private void setEditTextListener()
    {
        mEditTextFamilyMemberName.addTextChangedListener(new TextWatcher()
        {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after)
            {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count)
            {
                if (s.length() > 0)
                {
                    showName();

                    if (mEditTextFamilyMemberRelationship.getText().toString().length() > 0 && mEditTextFamilyMemberNumber.getText().toString().length() > 0)
                    {
                        enableAddButton();
                    }
                } else
                {
                    disableAddButton();
                    hideName();
                }
            }

            @Override
            public void afterTextChanged(Editable s)
            {

            }
        });

        mEditTextFamilyMemberRelationship.addTextChangedListener(new TextWatcher()
        {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after)
            {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count)
            {
                if (s.length() > 0)
                {
                    showRelationship();

                    if (mEditTextFamilyMemberName.getText().toString().length() > 0 && mEditTextFamilyMemberNumber.getText().toString().length() > 0)
                    {
                        enableAddButton();
                    }
                } else
                {
                    disableAddButton();
                    hideRelationship();
                }
            }

            @Override
            public void afterTextChanged(Editable s)
            {

            }
        });

        mEditTextFamilyMemberNumber.addTextChangedListener(new TextWatcher()
        {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after)
            {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count)
            {
                if (s.length() > 0)
                {
                    showMobileNumber();

                    if (mEditTextFamilyMemberName.getText().toString().length() > 0 && mEditTextFamilyMemberRelationship.getText().toString().length() > 0)
                    {
                        enableAddButton();
                    }
                } else
                {
                    disableAddButton();
                    hideMobileNumber();
                }
            }

            @Override
            public void afterTextChanged(Editable s)
            {

            }
        });
    }

    private void openDatePicker(final TextView textView)
    {
        final Calendar c = Calendar.getInstance();
        int year = c.get(Calendar.YEAR);
        int month = c.get(Calendar.MONTH);
        int day = c.get(Calendar.DAY_OF_MONTH);

        DatePickerDialog datePickerDialog = new DatePickerDialog(getActivity(),
                new DatePickerDialog.OnDateSetListener()
                {
                    @Override
                    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth)
                    {
                        textView.setText(year + "-" + (monthOfYear + 1) + "-" + dayOfMonth);
                    }
                }, year, month, day);
        datePickerDialog.show();
    }

    private void onAddMemberButtonCicked()
    {
        String strFamilyMemberName = mEditTextFamilyMemberName.getText().toString();
        String strFamilyMemberRelationship = mEditTextFamilyMemberRelationship.getText().toString();
        String strFamilyMemberNumber = mEditTextFamilyMemberNumber.getText().toString();
        String strFamilyMemberDob = mTextViewFamilyMemberDob.getText().toString();
        String strFamilyMemberAnniversary = mTextViewFamilyMemberAnniversary.getText().toString();
        String strSelectedGender = "";
        String strSelectedMaritalStatus = "";

        boolean addAddressCancel = false;
        boolean addFamilyDetailCancel = false;
        View addAddressFocusView = null;
        String message = getActivity().getResources().getString(R.string.signup_blank_field_error);

        if (TextUtils.isEmpty(strFamilyMemberName))
        {
            addAddressFocusView = mEditTextFamilyMemberName;
            addAddressCancel = true;
            message = getActivity().getResources().getString(R.string.signup_blank_field_error);
        }

        if (TextUtils.isEmpty(strFamilyMemberRelationship))
        {
            addAddressFocusView = mEditTextFamilyMemberRelationship;
            addAddressCancel = true;
            message = getActivity().getResources().getString(R.string.signup_blank_field_error);
        }

        // Validation for Number field, it should not be blank
        if (TextUtils.isEmpty(strFamilyMemberNumber))
        {
            addAddressFocusView = mEditTextFamilyMemberNumber;
            addAddressCancel = true;
            message = getActivity().getResources().getString(R.string.signup_blank_field_error);
        }

        if (strFamilyMemberNumber.length() != 10)
        {
            addAddressFocusView = mEditTextFamilyMemberNumber;
            addAddressCancel = true;
            message = getActivity().getResources().getString(R.string.mobile_number_error);
        }

        if (mGenderId == 0)
        {
            addFamilyDetailCancel = true;
            message = getActivity().getResources().getString(R.string.gender_message_error);
        } else
        {
            if (mGenderId == 1)
            {
                strSelectedGender = "M";
            } else
            {
                strSelectedGender = "F";
            }
        }

        if (mMaritalStatusId == 0)
        {
            strSelectedMaritalStatus = "";
            addFamilyDetailCancel = true;
            message = getActivity().getResources().getString(R.string.matrial_message_error);
        } else if (mMaritalStatusId == 1)
        {
            strSelectedMaritalStatus = "Y";
            strFamilyMemberAnniversary = "";
        } else
        {
            strSelectedMaritalStatus = "N";
            strFamilyMemberAnniversary = "";
        }

//        if (strFamilyMemberDob.equalsIgnoreCase(getString(R.string.dob)))
//        {
//            addFamilyDetailCancel = true;
//            message = getActivity().getResources().getString(R.string.dob_message_error);
//        }

        if (addAddressCancel)
        {
            addAddressFocusView.requestFocus();
            Utilities.showToastMessage(getActivity(), message);
        } else if (addFamilyDetailCancel)
        {
            Utilities.showToastMessage(getActivity(), message);
        } else
        {
            if (ConnectionDetector.isConnectedToInternet(getActivity()))
            {
                mProgressDialog = ProgressDialog.show(getActivity(), "", "Please Wait...", false);
                mProgressDialog.show();

                if (requestId == 0)
                {
                    JSONObject familyObject = new JSONObject();
                    JSONObject jsonObject = new JSONObject();
                    JSONArray jsonArray = new JSONArray();

                    try
                    {
                        jsonObject.put("name", strFamilyMemberName);
                        jsonObject.put("gender", strSelectedGender);
                        jsonObject.put("relationship", strFamilyMemberRelationship);
                        jsonObject.put("mobile", strFamilyMemberNumber);
                        jsonObject.put("dob", strFamilyMemberDob);
                        jsonObject.put("married", strSelectedMaritalStatus);
                        jsonObject.put("anniversary", strFamilyMemberAnniversary);

                        jsonArray.put(0, jsonObject);

                        familyObject.put("user_family", jsonArray);
                    }
                    catch (JSONException e)
                    {
                        e.printStackTrace();
                    }

//                    int socketTimeout = Utilities.REQUEST_TIME_OUT;
//                    mQueue = LockatedVolleyRequestQueue.getInstance(getActivity()).getRequestQueue();
//                    LockatedJSONObjectRequest lockatedJSONObjectRequest;
//                    lockatedJSONObjectRequest = new LockatedJSONObjectRequest(Request.Method.POST, ApplicationURL.addFamilyDetailUrl + "?token=" + mLockatedPreferences.getLockatedToken(), familyObject, this, this);
//                    lockatedJSONObjectRequest.setTag(REQUEST_TAG);
//                    RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
//                    lockatedJSONObjectRequest.setRetryPolicy(policy);
//                    mQueue.add(lockatedJSONObjectRequest);

                    mLockatedVolleyRequestQueue = LockatedVolleyRequestQueue.getInstance(getActivity());
                    mLockatedVolleyRequestQueue.sendRequest(REQUEST_TAG, Request.Method.POST, ApplicationURL.addFamilyDetailUrl + "?token=" + mLockatedPreferences.getLockatedToken(), familyObject, this, this);
                }
                else
                {
                    JSONObject jsonObject = new JSONObject();
                    JSONObject familyObject = new JSONObject();
                    try
                    {
                        jsonObject.put("name", strFamilyMemberName);
                        jsonObject.put("gender", strSelectedGender);
                        jsonObject.put("relationship", strFamilyMemberRelationship);
                        jsonObject.put("mobile", strFamilyMemberNumber);
                        jsonObject.put("dob", strFamilyMemberDob);
                        jsonObject.put("married", strSelectedMaritalStatus);
                        jsonObject.put("anniversary", strFamilyMemberAnniversary);

                        familyObject.put("user_family", jsonObject);
                    }
                    catch (JSONException ex)
                    {
                        ex.printStackTrace();
                    }

//                    int socketTimeout = Utilities.REQUEST_TIME_OUT;
//                    mQueue = LockatedVolleyRequestQueue.getInstance(getActivity()).getRequestQueue();
//                    LockatedJSONObjectRequest lockatedJSONObjectRequest;
//                    lockatedJSONObjectRequest = new LockatedJSONObjectRequest(Request.Method.PUT, ApplicationURL.updateFamilyDetailUrl + mFamilyDataRow.getId()+".json?token=" + mLockatedPreferences.getLockatedToken(), familyObject, this, this);
//                    lockatedJSONObjectRequest.setTag(REQUEST_TAG);
//                    RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
//                    lockatedJSONObjectRequest.setRetryPolicy(policy);
//                    mQueue.add(lockatedJSONObjectRequest);

                    mLockatedVolleyRequestQueue = LockatedVolleyRequestQueue.getInstance(getActivity());
                    mLockatedVolleyRequestQueue.sendRequest(REQUEST_TAG, Request.Method.PUT, ApplicationURL.updateFamilyDetailUrl + mFamilyDataRow.getId()+".json?token=" + mLockatedPreferences.getLockatedToken(), familyObject, this, this);
                }
            } else
            {
                Utilities.showToastMessage(getActivity(), getActivity().getResources().getString(R.string.internet_connection_error));
            }
        }
    }

    @Override
    public void onClick(View v)
    {
        switch (v.getId())
        {
            case R.id.mButtonAddAddress:
                if (getDialog() != null)
                {
                    onAddMemberButtonCicked();
                }
                break;
            case R.id.mButtonAddAddressCancel:
                if (getDialog() != null)
                {
                    getDialog().dismiss();
                }
                break;
            case R.id.mTextViewFamilyMemberDob:
                openDatePicker(mTextViewFamilyMemberDob);
                break;
            case R.id.mTextViewFamilyMemberAnniversary:
                openDatePicker(mTextViewFamilyMemberAnniversary);
                break;
        }
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id)
    {
        switch (parent.getId())
        {
            case R.id.mSpinnerFamilyMemberGender:
                mGenderId = position;
                break;
            case R.id.mSpinnerFamilyMemberMaritalStatus:
                mMaritalStatusId = position;
                onMaritalStatusChange();
                break;
        }
    }

    private void onMaritalStatusChange()
    {
        if (requestId == 1)
        {
            if (mMaritalStatusId == 1)
            {
                showAnniversaryField();
                if (mFamilyDataRow.getAnniversary().equals("null"))
                {
                    mTextViewFamilyMemberAnniversary.setText(R.string.anniversary);
                }
                else
                {
                    mTextViewFamilyMemberAnniversary.setText(mFamilyDataRow.getAnniversary());
                }
            }
            else
            {
                hideAnniversaryField();
            }
        }
        else
        {
            if (mMaritalStatusId == 1)
            {
                showAnniversaryField();
            } else
            {
                hideAnniversaryField();
            }
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent)
    {

    }

    @Override
    public void onErrorResponse(VolleyError error)
    {
        mProgressDialog.dismiss();
        if (getActivity() != null)
        {
            LockatedRequestError.onRequestError(getActivity(), error);
        }
    }

    @Override
    public void onResponse(Object response)
    {
        mProgressDialog.dismiss();
        if (getActivity() != null)
        {
            if (getDialog() != null)
            {
                getDialog().dismiss();
                JSONObject jsonObject = (JSONObject) response;

                if (requestId == 0)
                {
                    try
                    {
                        JSONArray jsonArray = jsonObject.getJSONArray("user_family");
                        for (int i = 0; i < jsonArray.length(); i++)
                        {
                            JSONObject familyObject = jsonArray.getJSONObject(i);
                            FamilyData familyData = new FamilyData(familyObject);
                            mFamilyDatas.add(familyData);
                        }

                        mINewAddressAdded.onNewMemberAdded(mFamilyDatas, requestId);
                    }
                    catch (JSONException e)
                    {
                        e.printStackTrace();
                    }
                }
                else
                {
                    try
                    {
                        mFamilyDataRow = new FamilyData(jsonObject);
                        mINewAddressAdded.onNewMemberUpdated(mFamilyDataRow, requestId, mPosition);
                    }
                    catch (Exception e)
                    {
                        e.printStackTrace();
                    }
                }
            }
        }
    }
}