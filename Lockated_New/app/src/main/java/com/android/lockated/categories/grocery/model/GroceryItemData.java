package com.android.lockated.categories.grocery.model;

import com.android.lockated.utils.ApplicationURL;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class GroceryItemData
{
    private int id;
    private String name;
    private String description;
    private String price;
    private String cost_price;
    private String display_price;
    private String available_on;
    private String shipping_category_id;
    private String weight;
    private String imageUrl;
    private int quantity = 0;
    private int total_on_hand;
    private int masterId;

    public GroceryItemData(JSONObject jsonObject)
    {
        try
        {
            id = jsonObject.optInt("id");
            name = jsonObject.optString("name");
            description = jsonObject.optString("description");
            price = jsonObject.optString("price");
            display_price = jsonObject.optString("display_price");
            available_on = jsonObject.optString("available_on");
            shipping_category_id = jsonObject.optString("shipping_category_id");
            total_on_hand = jsonObject.optInt("total_on_hand");

            JSONObject masterObject = new JSONObject(jsonObject.optString("master"));
            weight = masterObject.optString("weight");
            masterId = masterObject.optInt("id");
            cost_price = masterObject.optString("cost_price");
            JSONArray imagesJsonArray = masterObject.getJSONArray("images");
            for (int i = 0; i < imagesJsonArray.length(); i++)
            {
                JSONObject imageObject = imagesJsonArray.getJSONObject(i);
                imageUrl = ApplicationURL.ImageBaseURL+imageObject.optString("small_url");
            }
        }
        catch (JSONException e)
        {
            e.printStackTrace();
        }
    }

    public int getMasterId()
    {
        return masterId;
    }

    public int getId()
    {
        return id;
    }

    public String getName()
    {
        return name;
    }

    public String getDescription()
    {
        return description;
    }

    public String getPrice()
    {
        return price;
    }

    public String getDisplay_price()
    {
        return display_price;
    }

    public String getAvailable_on()
    {
        return available_on;
    }

    public String getShipping_category_id()
    {
        return shipping_category_id;
    }

    public String getWeight()
    {
        return weight;
    }

    public String getImageUrl()
    {
        return imageUrl;
    }

    public int getQuantity()
    {
        return quantity;
    }

    public void setQuantity(int quantity)
    {
        this.quantity = quantity;
    }

    public String getCostPrice()
    {
        return cost_price;
    }

    public int getTotalItems()
    {
        return total_on_hand;
    }
}
