package com.android.lockated.crm.fragment.myzone.directory;

import android.content.Context;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.android.lockated.LockatedApplication;
import com.android.lockated.R;
import com.android.lockated.information.AccountController;
import com.android.lockated.model.AccountData;
import com.android.lockated.model.usermodel.Directory.SocietyDirctory.PublicDirectory;
import com.android.lockated.network.ConnectionDetector;
import com.android.lockated.preferences.LockatedPreferences;
import com.android.lockated.request.LockatedJSONObjectRequest;
import com.android.lockated.request.LockatedRequestError;
import com.android.lockated.request.LockatedVolleyRequestQueue;
import com.android.lockated.utils.ApplicationURL;
import com.android.lockated.utils.Utilities;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class SocietyDirectoryFragment extends Fragment implements Response.ErrorListener, Response.Listener<JSONObject> {

    private FloatingActionButton fab;
    private RequestQueue mQueue;
    ListView listView;
    String pid;
    String userType;
    TextView errorMsg;
    ProgressBar progressBar;
    int societyid;
    AccountController accountController;
    ArrayList<AccountData> accountDataArrayList;
    ArrayList<PublicDirectory> publicDirectories;
    private LockatedPreferences mLockatedPreferences;
    public static final String REQUEST_TAG = "SocietyDirectoryFragment";
    SocityDirectoryAdapter socityDirectoryAdapter;
    String SHOW, CREATE, INDEX, UPDATE, EDIT, DESTROY;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View SocietyDirectoryView = inflater.inflate(R.layout.fragment_socity_directory, container, false);
        init(SocietyDirectoryView);
        return SocietyDirectoryView;
    }

    @Override
    public void onResume() {
        super.onResume();
        LockatedApplication.getInstance().trackScreenView(getString(R.string.societyDirectory));
        LockatedApplication.getInstance().trackEvent(getString(R.string.societyDirectory),
                getString(R.string.visited), getString(R.string.societyDirectory));
        publicDirectories.clear();

    }

    private void init(View societyDirectoryView) {
        publicDirectories = new ArrayList<>();
        progressBar = (ProgressBar) societyDirectoryView.findViewById(R.id.mProgressBarView);
        mLockatedPreferences = new LockatedPreferences(getActivity());
        errorMsg = (TextView) societyDirectoryView.findViewById(R.id.errorMsg);
        listView = (ListView) societyDirectoryView.findViewById(R.id.listView);
        /*accountController = AccountController.getInstance();
        accountDataArrayList = accountController.getmAccountDataList();*/
        societyid = Integer.parseInt(mLockatedPreferences.getSocietyId());
        getDirectoryRole();
        checkPermission();
        socityDirectoryAdapter = new SocityDirectoryAdapter(getActivity(), publicDirectories);
        listView.setAdapter(socityDirectoryAdapter);
    }

    private void checkPermission() {
        if (SHOW!=null&&SHOW.equals("true")) {
            getDirectory();
        } else {
            listView.setVisibility(View.GONE);
            errorMsg.setVisibility(View.VISIBLE);
            errorMsg.setText(R.string.no_permission_error);
        }
    }

    private void getDirectory() {
        if (getActivity() != null) {
            if (!mLockatedPreferences.getSocietyId().equals("blank")) {
                if (ConnectionDetector.isConnectedToInternet(getActivity())) {
                    mLockatedPreferences = new LockatedPreferences(getActivity());
                    mQueue = LockatedVolleyRequestQueue.getInstance(getActivity()).getRequestQueue();
                    LockatedJSONObjectRequest lockatedJSONObjectRequest = new LockatedJSONObjectRequest(Request.Method.GET,
                            ApplicationURL.getPublicDirectory + "society_id=" + societyid + "&token=" + mLockatedPreferences.getLockatedToken(), null, this, this);
                    lockatedJSONObjectRequest.setTag(REQUEST_TAG);
                    mQueue.add(lockatedJSONObjectRequest);
                } else {
                    Utilities.showToastMessage(getActivity(), getActivity().getResources().getString(R.string.internet_connection_error));
                }
            } else {
                listView.setVisibility(View.GONE);
                errorMsg.setVisibility(View.VISIBLE);
                errorMsg.setText(R.string.not_in_society);

            }
        }
    }

    public String getMySocietyRoles() {
        String mySocietyRoles = "blank";
        if (mLockatedPreferences.getRolesJson() != null && !mLockatedPreferences.getRolesJson().equals("blank")) {
            try {
                JSONObject noticeJsonObj = new JSONObject(mLockatedPreferences.getRolesJson());
                for (int i = 0; i < noticeJsonObj.getJSONArray("permissions").length(); i++) {
                    JSONObject jsonObject1 = noticeJsonObj.getJSONArray("permissions").getJSONObject(i);
                    if (jsonObject1.has("section") && jsonObject1.getString("section").equals("spree_mysociety")) {
                        if (jsonObject1.getJSONObject("permission").has("index")) {
                            mySocietyRoles = jsonObject1.getJSONObject("permission").getString("index");
                        }
                    }
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return mySocietyRoles;
    }

    @Override
    public void onErrorResponse(VolleyError error) {
        progressBar.setVisibility(View.GONE);
        if (getActivity() != null) {
            LockatedRequestError.onRequestError(getActivity(), error);
        }
    }

    @Override
    public void onResponse(JSONObject response) {
        if (getActivity() != null) {
            try {
                if (response != null && response.length() > 0) {
                    if (response.has("code") && response.has("public_directories") &&
                            response.getJSONArray("public_directories").length() > 0) {
                        JSONArray jsonArray = response.getJSONArray("public_directories");
                        Gson gson = new Gson();
                        for (int i = 0; i < jsonArray.length(); i++) {
                            PublicDirectory directory = gson.fromJson(jsonArray.getJSONObject(i).toString(), PublicDirectory.class);
                            publicDirectories.add(directory);
                        }
                        socityDirectoryAdapter.notifyDataSetChanged();
                    } else {
                        listView.setVisibility(View.GONE);
                        errorMsg.setVisibility(View.VISIBLE);
                        errorMsg.setText(R.string.no_data_error);
                    }
                } else {
                    listView.setVisibility(View.GONE);
                    errorMsg.setVisibility(View.VISIBLE);
                    errorMsg.setText(R.string.no_data_error);
                }

            } catch (Exception e) {
                e.printStackTrace();
            }
        }

    }

    private class SocityDirectoryAdapter extends BaseAdapter {
        private final ArrayList<PublicDirectory> publicDirectories;
        Context contextMain;
        LayoutInflater layoutInflater;

        public SocityDirectoryAdapter(Context context, ArrayList<PublicDirectory> publicDirectories) {
            this.contextMain = context;
            this.publicDirectories = publicDirectories;
            layoutInflater = LayoutInflater.from(context);
        }

        @Override
        public int getCount() {
            //return jsonArray.length();
            return publicDirectories.size();
        }

        @Override
        public Object getItem(int position) {
            return position;
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        private class ViewHolder {
            TextView mTextNumber;
            TextView mEmail_Id;
            TextView mTextDirectoryName;
            TextView mServiceType;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {

            ViewHolder holder = null;
            if (convertView == null) {

                convertView = layoutInflater.inflate(R.layout.socitychild, parent, false);
                holder = new ViewHolder();
                convertView.setTag(holder);
            } else {
                holder = (ViewHolder) convertView.getTag();
            }
            holder.mTextDirectoryName = (TextView) convertView.findViewById(R.id.mTextDirectoryName);
            holder.mTextNumber = (TextView) convertView.findViewById(R.id.mTextNumber);
            holder.mEmail_Id = (TextView) convertView.findViewById(R.id.mEmail_Id);
            holder.mServiceType = (TextView) convertView.findViewById(R.id.mServiceType);
            try {
                holder.mTextDirectoryName.setText(publicDirectories.get(position).getFirstname() + " " + (publicDirectories.get(position).getLastname()));
                holder.mTextNumber.setText(publicDirectories.get(position).getMobile());
                if (publicDirectories.get(position).getEmail() != null) {
                    holder.mEmail_Id.setText(publicDirectories.get(position).getEmail());
                } else {
                    holder.mEmail_Id.setText("Not Available");
                }
                holder.mServiceType.setText(publicDirectories.get(position).getNature());
            } catch (Exception e) {
                e.printStackTrace();
            }
            return convertView;
        }
    }

    public String getDirectoryRole() {
        String mySocietyRoles = getActivity().getResources().getString(R.string.blank_value);
        if (mLockatedPreferences.getRolesJson() != null && !mLockatedPreferences.getRolesJson().equals("blank")) {
            try {
                JSONObject noticeJsonObj = new JSONObject(mLockatedPreferences.getRolesJson());
                for (int i = 0; i < noticeJsonObj.getJSONArray(getActivity().getResources().getString(R.string.permissions_value)).length(); i++) {
                    JSONObject jsonObject1 = noticeJsonObj.getJSONArray(getActivity().getResources().getString(R.string.permissions_value)).getJSONObject(i);
                    if (jsonObject1.has(getActivity().getResources().getString(R.string.section_value))
                            && jsonObject1.getString(getActivity().getResources().getString(R.string.section_value)).equals("spree_directories")) {
                        if (jsonObject1.getJSONObject(getActivity().getResources().getString(R.string.permission_value)).has(getActivity().getResources().getString(R.string.index_value))) {
                            mySocietyRoles = jsonObject1.getJSONObject(getActivity().getResources().getString(R.string.permission_value)).getString(getActivity().getResources().getString(R.string.index_value));
                            CREATE = jsonObject1.getJSONObject(getActivity().getResources().getString(R.string.permission_value)).getString("create");
                            INDEX = jsonObject1.getJSONObject(getActivity().getResources().getString(R.string.permission_value)).getString("index");
                            UPDATE = jsonObject1.getJSONObject(getActivity().getResources().getString(R.string.permission_value)).getString("update");
                            EDIT = jsonObject1.getJSONObject(getActivity().getResources().getString(R.string.permission_value)).getString("edit");
                            SHOW = jsonObject1.getJSONObject(getActivity().getResources().getString(R.string.permission_value)).getString("show");
                            DESTROY = jsonObject1.getJSONObject(getActivity().getResources().getString(R.string.permission_value)).getString("destroy");
                        }
                    }
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return mySocietyRoles;
    }

}
