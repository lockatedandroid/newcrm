
package com.android.lockated.model.AdminModel.AdminPolls.PendingPolls;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class PollOption {

    @SerializedName("id")
    @Expose
    private int id;
    @SerializedName("poll_id")
    @Expose
    private int pollId;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("active")
    @Expose
    private Object active;
    @SerializedName("is_deleted")
    @Expose
    private Object isDeleted;
    @SerializedName("created_at")
    @Expose
    private String createdAt;
    @SerializedName("updated_at")
    @Expose
    private String updatedAt;
    @SerializedName("has_voted")
    @Expose
    private String hasVoted;
    @SerializedName("vote_share")
    @Expose
    private int voteShare;
    @SerializedName("can_vote")
    @Expose
    private String canVote;

    /**
     * 
     * @return
     *     The id
     */
    public int getId() {
        return id;
    }

    /**
     * 
     * @param id
     *     The id
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * 
     * @return
     *     The pollId
     */
    public int getPollId() {
        return pollId;
    }

    /**
     * 
     * @param pollId
     *     The poll_id
     */
    public void setPollId(int pollId) {
        this.pollId = pollId;
    }

    /**
     * 
     * @return
     *     The name
     */
    public String getName() {
        return name;
    }

    /**
     * 
     * @param name
     *     The name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * 
     * @return
     *     The active
     */
    public Object getActive() {
        return active;
    }

    /**
     * 
     * @param active
     *     The active
     */
    public void setActive(Object active) {
        this.active = active;
    }

    /**
     * 
     * @return
     *     The isDeleted
     */
    public Object getIsDeleted() {
        return isDeleted;
    }

    /**
     * 
     * @param isDeleted
     *     The is_deleted
     */
    public void setIsDeleted(Object isDeleted) {
        this.isDeleted = isDeleted;
    }

    /**
     * 
     * @return
     *     The createdAt
     */
    public String getCreatedAt() {
        return createdAt;
    }

    /**
     * 
     * @param createdAt
     *     The created_at
     */
    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    /**
     * 
     * @return
     *     The updatedAt
     */
    public String getUpdatedAt() {
        return updatedAt;
    }

    /**
     * 
     * @param updatedAt
     *     The updated_at
     */
    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    /**
     * 
     * @return
     *     The hasVoted
     */
    public String getHasVoted() {
        return hasVoted;
    }

    /**
     * 
     * @param hasVoted
     *     The has_voted
     */
    public void setHasVoted(String hasVoted) {
        this.hasVoted = hasVoted;
    }

    /**
     * 
     * @return
     *     The voteShare
     */
    public int getVoteShare() {
        return voteShare;
    }

    /**
     * 
     * @param voteShare
     *     The vote_share
     */
    public void setVoteShare(int voteShare) {
        this.voteShare = voteShare;
    }

    /**
     * 
     * @return
     *     The canVote
     */
    public String getCanVote() {
        return canVote;
    }

    /**
     * 
     * @param canVote
     *     The can_vote
     */
    public void setCanVote(String canVote) {
        this.canVote = canVote;
    }

}
