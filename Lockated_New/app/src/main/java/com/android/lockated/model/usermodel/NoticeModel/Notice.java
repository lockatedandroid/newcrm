package com.android.lockated.model.usermodel.NoticeModel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class Notice {

    @SerializedName("id")
    @Expose
    private int id;
    @SerializedName("id_society")
    @Expose
    private int idSociety;
    @SerializedName("notice_heading")
    @Expose
    private String noticeHeading;
    @SerializedName("notice_text")
    @Expose
    private String noticeText;
    @SerializedName("active")
    @Expose
    private Object active;
    @SerializedName("IsDelete")
    @Expose
    private Object IsDelete;
    @SerializedName("expire_time")
    @Expose
    private String expireTime;
    @SerializedName("created_at")
    @Expose
    private String createdAt;
    @SerializedName("updated_at")
    @Expose
    private String updatedAt;
    @SerializedName("id_user")
    @Expose
    private int idUser;
    @SerializedName("publish")
    @Expose
    private int publish;
    @SerializedName("notice_type")
    @Expose
    private Object noticeType;
    @SerializedName("deny")
    @Expose
    private int deny;
    @SerializedName("flag_expire")
    @Expose
    private Object flagExpire;
    @SerializedName("canceled_by")
    @Expose
    private Object canceledBy;
    @SerializedName("canceler_id")
    @Expose
    private Object cancelerId;
    @SerializedName("comment")
    @Expose
    private Object comment;
    @SerializedName("shared")
    @Expose
    private int shared;
    @SerializedName("documents")
    @Expose
    private ArrayList<Document> documents = new ArrayList<Document>();
    @SerializedName("username")
    @Expose
    private String username;
    @SerializedName("flat")
    @Expose
    private String flat;

    public Notice(JSONObject jsonObject) {
        id = jsonObject.optInt("id");
        idSociety = jsonObject.optInt("id_society");
        noticeHeading = jsonObject.optString("notice_heading");
        noticeText = jsonObject.optString("notice_text");
        active = jsonObject.opt("active");
        IsDelete = jsonObject.opt("IsDelete");
        expireTime = jsonObject.optString("expire_time");
        createdAt = jsonObject.optString("created_at");
        updatedAt = jsonObject.optString("updated_at");
        idUser = jsonObject.optInt("id_user");
        publish = jsonObject.optInt("publish");
        noticeType = jsonObject.opt("notice_type");
        deny = jsonObject.optInt("deny");
        flagExpire = jsonObject.opt("flag_expire");
        canceledBy = jsonObject.opt("canceled_by");
        cancelerId = jsonObject.opt("canceler_id");
        comment = jsonObject.opt("comment");
        shared = jsonObject.optInt("shared");
        username = jsonObject.optString("username");
        flat = jsonObject.optString("flat");
        /*try {
            for (int i = 0; i < jsonObject.optJSONArray("documents").length(); i++) {
                documents.add(jsonObject.optJSONArray("documents").getJSONObject(i).toString());
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }*/
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getIdSociety() {
        return idSociety;
    }

    public void setIdSociety(int idSociety) {
        this.idSociety = idSociety;
    }

    public String getNoticeHeading() {
        return noticeHeading;
    }

    public void setNoticeHeading(String noticeHeading) {
        this.noticeHeading = noticeHeading;
    }

    public String getNoticeText() {
        return noticeText;
    }

    public void setNoticeText(String noticeText) {
        this.noticeText = noticeText;
    }

    public Object getActive() {
        return active;
    }

    public void setActive(Object active) {
        this.active = active;
    }

    public Object getIsDelete() {
        return IsDelete;
    }

    public void setIsDelete(Object IsDelete) {
        this.IsDelete = IsDelete;
    }

    public String getExpireTime() {
        return expireTime;
    }

    public void setExpireTime(String expireTime) {
        this.expireTime = expireTime;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public int getIdUser() {
        return idUser;
    }

    public void setIdUser(int idUser) {
        this.idUser = idUser;
    }

    public int getPublish() {
        return publish;
    }

    public void setPublish(int publish) {
        this.publish = publish;
    }

    public Object getNoticeType() {
        return noticeType;
    }

    public void setNoticeType(Object noticeType) {
        this.noticeType = noticeType;
    }

    public int getDeny() {
        return deny;
    }

    public void setDeny(int deny) {
        this.deny = deny;
    }

    public Object getFlagExpire() {
        return flagExpire;
    }

    public void setFlagExpire(Object flagExpire) {
        this.flagExpire = flagExpire;
    }

    public Object getCanceledBy() {
        return canceledBy;
    }

    public void setCanceledBy(Object canceledBy) {
        this.canceledBy = canceledBy;
    }

    public Object getCancelerId() {
        return cancelerId;
    }

    public void setCancelerId(Object cancelerId) {
        this.cancelerId = cancelerId;
    }

    public Object getComment() {
        return comment;
    }

    public void setComment(Object comment) {
        this.comment = comment;
    }

    public int getShared() {
        return shared;
    }

    public void setShared(int shared) {
        this.shared = shared;
    }

    public String getUsername() {
        return username;
    }

    public ArrayList<Document> getDocuments() {
        return documents;
    }

    public void setDocuments(ArrayList<Document> documents) {
        this.documents = documents;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getFlat() {
        return flat;
    }

    public void setFlat(String flat) {
        this.flat = flat;
    }

}