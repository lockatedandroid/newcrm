
package com.android.lockated.model.usermodel.Directory;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class PersonalDirectory {

    @SerializedName("code")
    @Expose
    private int code;
    @SerializedName("directories")
    @Expose
    private ArrayList<Directory> directories = new ArrayList<Directory>();

    /**
     * 
     * @return
     *     The code
     */
    public int getCode() {
        return code;
    }

    /**
     * 
     * @param code
     *     The code
     */
    public void setCode(int code) {
        this.code = code;
    }

    /**
     *
     * @param directories
     *     The directories
     */
    public void setDirectories(ArrayList<Directory> directories) {
        this.directories = directories;
    }

    /**
     *
     * @return
     *     The directories
     */
    public ArrayList<Directory> getDirectories() {
        return directories;
    }

}
