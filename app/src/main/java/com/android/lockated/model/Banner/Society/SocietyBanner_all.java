
package com.android.lockated.model.Banner.Society;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.json.JSONObject;

public class SocietyBanner_all {

    @SerializedName("id")
    @Expose
    public int id;
    @SerializedName("active")
    @Expose
    public int active;
    @SerializedName("created_at")
    @Expose
    public String createdAt;
    @SerializedName("updated_at")
    @Expose
    public String updatedAt;
    @SerializedName("url")
    @Expose
    public String url;
    @SerializedName("medium")
    @Expose
    public String medium;
    @SerializedName("thumb")
    @Expose
    public String thumb;

    public SocietyBanner_all(JSONObject jsonObject) {
        id = jsonObject.optInt("id");
        active = jsonObject.optInt("active");
        url = jsonObject.optString("url");
        medium = jsonObject.optString("medium");
        thumb = jsonObject.optString("thumb");
    }
    public int getId() {
        return id;
    }

    public String getUrl() {
        return url;
    }

    public String getMedium() {
        return medium;
    }

    public String getThumb() {
        return thumb;
    }


}
