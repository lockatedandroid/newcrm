package com.android.lockated.model;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class MyOrderData {
    public String number;
    public String completed_at;
    public String total;
    public String state;
    public boolean canCancel;

    private ArrayList<OrderDetailData> mOrderDetailDataList = new ArrayList<>();

    public MyOrderData(JSONObject jsonObject) {
        try {
            number = jsonObject.optString("number");
            completed_at = jsonObject.optString("completed_at");
            total = jsonObject.optString("total");
            //state = jsonObject.optString("shipment_state");
            state = jsonObject.optString("state");
            canCancel = jsonObject.optBoolean("can_cancel");

            JSONArray jsonArray = jsonObject.getJSONArray("order_details");

            for (int i = 0; i < jsonArray.length(); i++) {
                JSONObject orderDetailObject = jsonArray.getJSONObject(i);
                OrderDetailData orderDetailData = new OrderDetailData(orderDetailObject);
                mOrderDetailDataList.add(orderDetailData);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public String getNumber() {
        return number;
    }

    public String getCompleted_at() {
        return completed_at;
    }

    public String getTotal() {
        return total;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public ArrayList<OrderDetailData> getmOrderDetailDataList() {
        return mOrderDetailDataList;
    }

    public boolean getCanCancel() {
        return canCancel;
    }

    public void setCanCancel(boolean canCancel) {
        this.canCancel = canCancel;
    }
}
