package com.android.lockated.crmadmin.fragment;


import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.TextView;

import com.android.lockated.LockatedApplication;
import com.android.lockated.R;
import com.android.lockated.crmadmin.activity.AdminActivity;
import com.android.lockated.crmadmin.detailView.PendingComplaintsDetailViewActivity;
import com.android.lockated.information.AccountController;
import com.android.lockated.model.AccountData;
import com.android.lockated.model.AdminModel.AdminComplaints.Complaint;
import com.android.lockated.network.ConnectionDetector;
import com.android.lockated.preferences.LockatedPreferences;
import com.android.lockated.request.LockatedJSONObjectRequest;
import com.android.lockated.request.LockatedRequestError;
import com.android.lockated.request.LockatedVolleyRequestQueue;
import com.android.lockated.utils.ApplicationURL;
import com.android.lockated.utils.Utilities;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Locale;


public class PendingComplaintsFragment extends Fragment implements Response.ErrorListener, Response.Listener<JSONObject> {

    public static final String REQUEST_TAG = "PendingComplaintsFragment";
    RecyclerView recyclerView;
    String spid;
    int pos;
    /*ProgressBar progressBar;*/
    ProgressDialog mProgressDialog;
    ArrayAdapter<String> arrayadapter;
    ArrayList<Complaint> complaintArrayList;
    PendingHelpDeskAdapter pendinghelpdeskAdapter;
    JSONArray statusArray;
    String SHOW, CREATE, INDEX, UPDATE, EDIT;
    private RequestQueue mQueue;
    private LockatedPreferences mLockatedPreferences;
    private TextView errorMsg;
    private String strStatus;
    private String pid;
    private String formattedDate;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View pendingClassified = inflater.inflate(R.layout.fragment_pending_help_desk, container, false);
        init(pendingClassified);
        Utilities.ladooIntegration(getActivity(), "Admin Pending Complaints");
        return pendingClassified;
    }

    @Override
    public void onResume() {
        super.onResume();
        LockatedApplication.getInstance().trackScreenView(getString(R.string.adminPendingHelpDesk));
        Utilities.lockatedGoogleAnalytics(getString(R.string.adminPendingHelpDesk),
                getString(R.string.visited), getString(R.string.adminPendingHelpDesk));
        // complaintArrayList.clear();
        //getPendingHelpdesk();
    }

    private void init(View view) {
        complaintArrayList = new ArrayList<>();
        mLockatedPreferences = new LockatedPreferences(getActivity());
        /*progressBar = (ProgressBar) view.findViewById(R.id.mProgressBarView);*/
        recyclerView = (RecyclerView) view.findViewById(R.id.recyclerView);
        errorMsg = (TextView) view.findViewById(R.id.noText);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity(),
                LinearLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(linearLayoutManager);
        getHelpDeskRole();
        checkPermission();
        pendinghelpdeskAdapter = new PendingHelpDeskAdapter(getActivity(), complaintArrayList);
        recyclerView.setAdapter(pendinghelpdeskAdapter);
    }

    private void checkPermission() {
        if (SHOW!=null&&SHOW.equals("true")) {
            getPendingHelpdesk();
        } else {
            recyclerView.setVisibility(View.GONE);
            errorMsg.setVisibility(View.VISIBLE);
            errorMsg.setText(R.string.no_permission_error);
        }
    }

    public void getPendingHelpdesk() {
        if (getActivity() != null) {
            if (!mLockatedPreferences.getSocietyId().equals("blank")) {
                if (ConnectionDetector.isConnectedToInternet(getActivity())) {
                    /*progressBar.setVisibility(View.VISIBLE);*/
                    mProgressDialog = ProgressDialog.show(getActivity(), "", "Please Wait...", false);
                    mProgressDialog.show();
                    mQueue = LockatedVolleyRequestQueue.getInstance(getActivity()).getRequestQueue();
                    String url = ApplicationURL.getAdminPendingHelpDeskdUrl + mLockatedPreferences.getLockatedToken();
                    LockatedJSONObjectRequest lockatedJSONObjectRequest = new LockatedJSONObjectRequest(Request.Method.GET,
                            url, null, this, this);
                    lockatedJSONObjectRequest.setTag(REQUEST_TAG);
                    mQueue.add(lockatedJSONObjectRequest);
                } else {
                    Utilities.showToastMessage(getActivity(), getActivity().getResources().getString(R.string.internet_connection_error));
                }
            } else {
                errorMsg.setVisibility(View.VISIBLE);
                errorMsg.setText(R.string.not_in_society);
            }
        }
    }

    @Override
    public void onResponse(JSONObject response) {
        /*progressBar.setVisibility(View.GONE);*/
        mProgressDialog.dismiss();
        if (getActivity() != null) {
            try {
                if (response != null && response.has("complaints") && response.getJSONArray("complaints").length() > 0) {
                    JSONArray jsonArray = response.getJSONArray("complaints");
                    Gson gson = new Gson();
                    if (response.has("issue_status")) {
                        statusArray = response.getJSONArray("issue_status");
                        arrayadapter = new ArrayAdapter<>(getContext(), R.layout.support_simple_spinner_dropdown_item);
                        for (int i = 0; i < response.getJSONArray("issue_status").length(); i++) {
                            arrayadapter.add(statusArray.getString(i));
                        }
                    }
                    for (int i = 0; i < jsonArray.length(); i++) {
                        Complaint complaint = gson.fromJson(jsonArray.getJSONObject(i).toString(), Complaint.class);
                        complaintArrayList.add(complaint);
                    }
                    pendinghelpdeskAdapter.notifyDataSetChanged();
                } else {
                    recyclerView.setVisibility(View.GONE);
                    errorMsg.setVisibility(View.VISIBLE);
                    errorMsg.setText(R.string.no_data_error);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void onErrorResponse(VolleyError error) {
        if (getActivity() != null) {
            mProgressDialog.dismiss();
            LockatedRequestError.onRequestError(getActivity(), error);
        }
    }

    public String getHelpDeskRole() {
        String mySocietyRoles = getActivity().getResources().getString(R.string.blank_value);
        if (mLockatedPreferences.getRolesJson() != null && !mLockatedPreferences.getRolesJson().equals("blank")) {
            try {
                JSONObject noticeJsonObj = new JSONObject(mLockatedPreferences.getRolesJson());
                for (int i = 0; i < noticeJsonObj.getJSONArray(getActivity().getResources().getString(R.string.permissions_value)).length(); i++) {
                    JSONObject jsonObject1 = noticeJsonObj.getJSONArray(getActivity().getResources().getString(R.string.permissions_value)).getJSONObject(i);
                    if (jsonObject1.has(getActivity().getResources().getString(R.string.section_value))
                            && jsonObject1.getString(getActivity().getResources().getString(R.string.section_value)).equals("spree_complaints")) {
                        if (jsonObject1.getJSONObject(getActivity().getResources().getString(R.string.permission_value)).has(getActivity().getResources().getString(R.string.index_value))) {
                            mySocietyRoles = jsonObject1.getJSONObject(getActivity().getResources().getString(R.string.permission_value)).getString(getActivity().getResources().getString(R.string.index_value));
                            CREATE = jsonObject1.getJSONObject(getActivity().getResources().getString(R.string.permission_value)).getString("create");
                            INDEX = jsonObject1.getJSONObject(getActivity().getResources().getString(R.string.permission_value)).getString("index");
                            UPDATE = jsonObject1.getJSONObject(getActivity().getResources().getString(R.string.permission_value)).getString("update");
                            EDIT = jsonObject1.getJSONObject(getActivity().getResources().getString(R.string.permission_value)).getString("edit");
                            SHOW = jsonObject1.getJSONObject(getActivity().getResources().getString(R.string.permission_value)).getString("show");
                        }
                    }
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return mySocietyRoles;
    }

    private class PendingHelpDeskAdapter extends RecyclerView.Adapter<PendingHelpDeskAdapter.PendingHelpDeskViewHolder>
            implements Response.ErrorListener, Response.Listener<JSONObject>, AdapterView.OnItemSelectedListener {
        public static final String REQUEST_TAG = "PendingHelpDeskAdapter";
        private final ArrayList<Complaint> arrayList;
        Context context;
        ArrayList<AccountData> accountDataArrayList;
        int societyId;
        private RequestQueue mQueue;
        private LockatedPreferences mLockatedPreferences;
        private AccountController accountController;
        private String Creatername;

        public PendingHelpDeskAdapter(Context context, ArrayList<Complaint> arrayList) {
            this.context = context;
            this.arrayList = arrayList;
        }

        @Override
        public PendingHelpDeskViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View v = LayoutInflater.from(context).inflate(R.layout.admin_pending_helpdesk_child, parent, false);
            PendingHelpDeskViewHolder pvh = new PendingHelpDeskViewHolder(v);
            return pvh;
        }

        @Override
        public void onBindViewHolder(PendingHelpDeskViewHolder holder, int position) {
            accountController = AccountController.getInstance();
            accountDataArrayList = accountController.getmAccountDataList();
          /*  String strFlatNumber = accountDataArrayList.get(0).getmResidenceDataList().get(0).getFlat();*/
            //     societyId = accountDataArrayList.get(0).getmResidenceDataList().get(0).getSociety_id();
            Calendar c = Calendar.getInstance();
            SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSz", Locale.FRANCE);
            formattedDate = df.format(c.getTime());
            pid = String.valueOf(arrayList.get(position).getId());
            holder.mIssueType.setText(arrayList.get(position).getIssueType());
            holder.mIssueDescription.setText(arrayList.get(position).getText());
            holder.mIssueCategory.setText(arrayList.get(position).getHeading());
            holder.mPostedOn.setText(Utilities.ddMMYYYFormat(arrayList.get(position).getCreatedAt()));
            holder.mTextCategory.setText(arrayList.get(position).getCategoryType());
            holder.mpostedBy.setText(arrayList.get(position).getPostedBy());
            String flatno = String.valueOf(arrayList.get(position).getFlatNumber());
            if (!flatno.equals("null")) {
                holder.mFlatNumber.setText(flatno);
            } else {
                holder.mFlatNumber.setText("No Flat");
            }
            holder.mUpdatedOn.setText(Utilities.ddMMYYYFormat(arrayList.get(position).getUpdatedAt()));
            String strCurrentStatus = arrayList.get(position).getIssueStatus();
            if (strCurrentStatus != null) {
                holder.mCurrentStatus.setText(arrayList.get(position).getIssueStatus());
            } else {
                holder.mCurrentStatus.setText("Pending");
            }
            holder.mSpinnerStatus.setAdapter(arrayadapter);
            holder.mDetails.setTag(position);
            holder.mDetails.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int position = (int) v.getTag();
                    Intent intent = new Intent(getActivity(), PendingComplaintsDetailViewActivity.class);
                    try {
                        intent.putExtra("pid", String.valueOf(arrayList.get(position).getId()));
                        intent.putExtra("strIssueType", arrayList.get(position).getIssueType());
                        intent.putExtra("strDescription", arrayList.get(position).getText());
                        intent.putExtra("strIssueCategory", arrayList.get(position).getHeading());
                        intent.putExtra("strPostedOn", Utilities.ddMMYYYFormat(arrayList.get(position).getCreatedAt()));
                        intent.putExtra("strmTextCategory", arrayList.get(position).getCategoryType());
                        intent.putExtra("strpostedBy", arrayList.get(position).getPostedBy());
                        intent.putExtra("FlatNumber", "" + arrayList.get(position).getFlatNumber());
                        intent.putExtra("strmUpdatedOn", Utilities.ddMMYYYFormat(arrayList.get(position).getUpdatedAt()));
                        intent.putExtra("CurrentStatus", arrayList.get(position).getIssueStatus());
                        if (arrayList.get(position).getDocuments() != null && arrayList.get(position).getDocuments().size() > 0) {
                            intent.putExtra("attachmentImage", arrayList.get(position).getDocuments().get(0).getDocument());
                        } else {
                            intent.putExtra("attachmentImage", "No Document");
                        }
                        startActivity(intent);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            });
            holder.mSpinnerStatus.setTag(position);
            holder.mSpinnerStatus.setOnItemSelectedListener(this);
        }

        private void putComplaintsData(JSONObject jsonObjectMain, String spid) {
            mProgressDialog = ProgressDialog.show(getActivity(), "", "Please Wait...", false);
            mProgressDialog.show();
            try {
                mQueue = LockatedVolleyRequestQueue.getInstance(getActivity()).getRequestQueue();
                LockatedJSONObjectRequest lockatedJSONObjectRequest = new LockatedJSONObjectRequest(Request.Method.PUT,
                        ApplicationURL.updatePendingComplaint + spid + ".json?token="
                                + mLockatedPreferences.getLockatedToken(),
                        jsonObjectMain, this, this);
                lockatedJSONObjectRequest.setTag(REQUEST_TAG);
                mQueue.add(lockatedJSONObjectRequest);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        @Override
        public void onErrorResponse(VolleyError error) {
            mProgressDialog.dismiss();
            if (context != null) {
                LockatedRequestError.onRequestError(context, error);
            }
        }

        @Override
        public int getItemCount() {
            return arrayList.size();
        }

        @Override
        public void onResponse(JSONObject response) {
            mProgressDialog.dismiss();
            if (context != null) {
                try {
                    if (response != null && response.length() > 0) {
                        Intent intent = new Intent(context, AdminActivity.class);
                        intent.putExtra("AdminActivityPosition", 4);
                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        context.startActivity(intent);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }

        @Override
        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
            int pos = (int) parent.getTag();
            spid = String.valueOf(arrayList.get(pos).getId());
            strStatus = parent.getSelectedItem().toString();
            mLockatedPreferences = new LockatedPreferences(getActivity());
            if (parent.getSelectedItemPosition() != 0) {
                if (spid != null) {
                    JSONObject jsonObjectMain = new JSONObject();
                    JSONObject jsonObject = new JSONObject();
                    try {
                        jsonObject.put("issue_status", strStatus);
                        jsonObject.put("updated_at", formattedDate);
                        jsonObjectMain.put("complaint", jsonObject);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    putComplaintsData(jsonObjectMain, String.valueOf(arrayList.get(pos).getId()));
                } else {
                    Utilities.showToastMessage(getActivity(), "Your View id is null");
                }
            }
        }

        @Override
        public void onNothingSelected(AdapterView<?> parent) {

        }

        public class PendingHelpDeskViewHolder extends RecyclerView.ViewHolder {
            private TextView mIssueType, mIssueDescription, mIssueCategory, mpostedBy, mPostedOn,
                    mUpdatedOn, mTextCategory, mFlatNumber, mDetails, mCurrentStatus;
            private Spinner mSpinnerStatus;

            public PendingHelpDeskViewHolder(View itemView) {
                super(itemView);
                mDetails = (TextView) itemView.findViewById(R.id.mDetails);
                mFlatNumber = (TextView) itemView.findViewById(R.id.mFlatNumber);
                mTextCategory = (TextView) itemView.findViewById(R.id.mTextCategory);
                mIssueType = (TextView) itemView.findViewById(R.id.mIssueType);//personal/socity//issue_type
                mIssueDescription = (TextView) itemView.findViewById(R.id.mIssueDescription);//text
                mIssueCategory = (TextView) itemView.findViewById(R.id.mIssueCategory);//heading
                mSpinnerStatus = (Spinner) itemView.findViewById(R.id.mSpinnerStatus);
                mpostedBy = (TextView) itemView.findViewById(R.id.mpostedBy);//Created_By
                mPostedOn = (TextView) itemView.findViewById(R.id.mPostedOn);//create date
                mUpdatedOn = (TextView) itemView.findViewById(R.id.mUpdatedOn);
                mCurrentStatus = (TextView) itemView.findViewById(R.id.mCurrentStatus);
            }
        }
    }

}