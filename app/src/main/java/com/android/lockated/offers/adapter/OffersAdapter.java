package com.android.lockated.offers.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.lockated.HomeActivity;
import com.android.lockated.R;
import com.android.lockated.model.Offers.OffersModel;
import com.android.lockated.preferences.LockatedPreferences;
import com.android.lockated.utils.Utilities;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;


public class OffersAdapter extends RecyclerView.Adapter<OffersAdapter.OffersAdapterViewHolder> {

    Context contextSub;
    ArrayList<OffersModel> offersModelArrayList;
    LockatedPreferences lockatedPreferences;

    public OffersAdapter(Context context, ArrayList<OffersModel> offersModelArrayList) {
        this.contextSub = context;
        this.offersModelArrayList = offersModelArrayList;
        lockatedPreferences = new LockatedPreferences(context);
    }

    @Override
    public OffersAdapterViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(contextSub).inflate(R.layout.offers_adapter_view, parent, false);
        OffersAdapterViewHolder offersAdapterViewHolder = new OffersAdapterViewHolder(v);
        return offersAdapterViewHolder;
    }

    @Override
    public void onBindViewHolder(OffersAdapterViewHolder holder, int position) {

        holder.date.setText(Utilities.ddMonYYYYFormatWithoutTime(offersModelArrayList.get(position).getExpiry()));
        holder.type.setText(offersModelArrayList.get(position).getTitle());
        holder.description.setText(offersModelArrayList.get(position).getDescription());
        Picasso.with(contextSub).load(offersModelArrayList.get(position).getOriginal())
                .placeholder(R.drawable.loading).fit().into(holder.offerImg);
        holder.offersCard.setTag(position);
        holder.offersCard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int pos = (int) v.getTag();
                Intent intent = new Intent(contextSub, HomeActivity.class);
                intent.putExtra("offersOtypeId", Utilities.getIdPosition(offersModelArrayList.get(pos).getOtype_id()));
                intent.putExtra("otype_id", offersModelArrayList.get(pos).getOtype_id());
                intent.putExtra("taxon_id", offersModelArrayList.get(pos).getTaxon_id());
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                contextSub.startActivity(intent);
            }
        });

    }

    @Override
    public int getItemCount() {
        return offersModelArrayList.size();
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }

    public class OffersAdapterViewHolder extends RecyclerView.ViewHolder {

        CardView offersCard;
        TextView date, type, description;
        ImageView offerImg;

        public OffersAdapterViewHolder(View itemView) {
            super(itemView);

            offersCard = (CardView) itemView.findViewById(R.id.offersCardView);
            date = (TextView) itemView.findViewById(R.id.dateValue);
            type = (TextView) itemView.findViewById(R.id.typeValue);
            description = (TextView) itemView.findViewById(R.id.descriptionValue);
            offerImg = (ImageView) itemView.findViewById(R.id.offerImage);

        }

    }

}