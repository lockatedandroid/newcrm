
package com.android.lockated.model.usermodel.Directory.MembersDirectory;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class User {

    @SerializedName("id")
    @Expose
    private int id;
    @SerializedName("firstname")
    @Expose
    private String firstname;
    @SerializedName("lastname")
    @Expose
    private String lastname;
    @SerializedName("mobile")
    @Expose
    private String mobile;
    @SerializedName("email")
    @Expose
    private String email;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    /**
     * @return The firstname
     * <<<<<<< HEAD
     * >>>>>>> origin/master
     * =======
     * >>>>>>> lockated_new/master
     */
    public String getFirstname() {
        return firstname;
    }

    /**
     * <<<<<<< HEAD
     * <<<<<<< HEAD
     *
     * @param firstname The firstname
     *                  =======
     * @param firstname The firstname
     *                  >>>>>>> origin/master
     *                  =======
     * @param firstname The firstname
     *                  >>>>>>> lockated_new/master
     */
    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    /**
     * <<<<<<< HEAD
     * <<<<<<< HEAD
     *
     * @return The lastname
     * >>>>>>> lockated_new/master
     */
    public String getLastname() {
        return lastname;
    }

    /**
     * <<<<<<< HEAD
     * <<<<<<< HEAD
     *
     * @param lastname The lastname
     *                 =======
     * @param lastname The lastname
     *                 >>>>>>> origin/master
     *                 =======
     * @param lastname The lastname
     *                 >>>>>>> lockated_new/master
     */
    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    /**
     * <<<<<<< HEAD
     * <<<<<<< HEAD
     *
     * @return The mobile
     * >>>>>>> lockated_new/master
     */
    public String getMobile() {
        return mobile;
    }

    /**
     * <<<<<<< HEAD
     * <<<<<<< HEAD
     *
     * @param mobile The mobile
     *               =======
     * @param mobile The mobile
     *               >>>>>>> origin/master
     *               =======
     * @param mobile The mobile
     *               >>>>>>> lockated_new/master
     */
    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    /**
     * <<<<<<< HEAD
     * <<<<<<< HEAD
     *
     * @return The email
     * >>>>>>> lockated_new/master
     */
    public String getEmail() {
        return email;
    }

    /**
     * <<<<<<< HEAD
     * <<<<<<< HEAD
     *
     * @param email The email
     *              =======
     * @param email The email
     *              >>>>>>> origin/master
     *              =======
     * @param email The email
     *              >>>>>>> lockated_new/master
     */
    public void setEmail(String email) {
        this.email = email;
    }

}
