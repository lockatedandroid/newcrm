package com.android.lockated.model.OlaCab;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;


public class OlaCabCategoryDetail {

    @SerializedName("categories")
    @Expose
    private ArrayList<Category> categories = new ArrayList<Category>();
    @SerializedName("ride_estimate")
    @Expose
    private ArrayList<RideEstimate> rideEstimate = new ArrayList<RideEstimate>();

    /**
     * @return The categories
     */
    public ArrayList<Category> getCategories() {
        return categories;
    }

    /**
     * @param categories The categories
     */
    public void setCategories(ArrayList<Category> categories) {
        this.categories = categories;
    }

    /**
     * @return The rideEstimate
     */
    public ArrayList<RideEstimate> getRideEstimate() {
        return rideEstimate;
    }

    /**
     * @param rideEstimate The ride_estimate
     */
    public void setRideEstimate(ArrayList<RideEstimate> rideEstimate) {
        this.rideEstimate = rideEstimate;
    }

}
