package com.android.lockated.location.model;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class LocationData
{
    public String state;
    public String locality;
    public String countryName;
    public String address = "";

    public LocationData(JSONObject jsonObject)
    {
        try
        {
            state = jsonObject.optString("state");
            locality = jsonObject.optString("locality");
            countryName = jsonObject.optString("countryName");

            JSONArray jsonArray = jsonObject.getJSONArray("address");

            for (int i = 0; i < jsonArray.length(); i++)
            {
                JSONObject addressObject = jsonArray.getJSONObject(i);
                address = address+" "+addressObject.optString("address");
            }
        }
        catch (JSONException e)
        {
            e.printStackTrace();
        }
    }

    public String getState()
    {
        return state;
    }

    public String getLocality()
    {
        return locality;
    }

    public String getCountryName()
    {
        return countryName;
    }

    public String getAddress()
    {
        return address;
    }
}
