package com.android.lockated.model.MyServices;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class MyServices {

    @SerializedName("code")
    @Expose
    private int code;
    @SerializedName("service_requests")
    @Expose
    private ArrayList<ServiceRequest> serviceRequests = new ArrayList<ServiceRequest>();

    /**
     * @return The code
     */
    public int getCode() {
        return code;
    }

    /**
     * @param code The code
     */
    public void setCode(int code) {
        this.code = code;
    }

    /**
     * @return The serviceRequests
     */
    public ArrayList<ServiceRequest> getServiceRequests() {
        return serviceRequests;
    }

    /**
     * @param serviceRequests The service_requests
     */
    public void setServiceRequests(ArrayList<ServiceRequest> serviceRequests) {
        this.serviceRequests = serviceRequests;
    }

}
