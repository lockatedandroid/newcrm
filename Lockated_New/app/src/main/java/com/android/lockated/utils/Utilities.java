package com.android.lockated.utils;

import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Environment;
import android.provider.Settings;
import android.support.v7.app.AlertDialog;
import android.util.Base64;
import android.util.Log;
import android.widget.Toast;

import com.android.lockated.LockatedApplication;
import com.android.lockated.R;
import com.android.lockated.model.UserRoles;
import com.android.lockated.preferences.LockatedPreferences;
import com.android.lockated.relaunchApp.DefaultExceptionHandler;
import com.genie.Genie;
import com.google.android.gms.analytics.ecommerce.Product;
import com.google.android.gms.analytics.ecommerce.ProductAction;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedWriter;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.StringTokenizer;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Utilities {
    public static int REQUEST_TIME_OUT = 1000 * 2 * 60;
    //public static int REQUEST_TIME_OUT = 0;

    public static void showToastMessage(Context context, String message) {
        Toast toast = Toast.makeText(context, message, Toast.LENGTH_SHORT);
        toast.show();
    }

    public static boolean isPlayServiceAvailable(Context context) {
        int statusCode = GoogleApiAvailability.getInstance().isGooglePlayServicesAvailable(context);
        switch (statusCode) {
            case ConnectionResult.SERVICE_VERSION_UPDATE_REQUIRED:
                return false;
            case ConnectionResult.SUCCESS:
                return true;
            default:
                return true;
        }
    }

    public static void alertGPSConnectionError(final Context context) {
        final AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setMessage(context.getString(R.string.gps_disabled))
                .setCancelable(false)
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    public void onClick(final DialogInterface dialog, final int id) {
                        ((LockatedApplication) context.getApplicationContext()).setIsGPSEnabled(true);
                        dialog.cancel();
                        context.startActivity(new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS));
                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    public void onClick(final DialogInterface dialog, final int id) {
                        dialog.cancel();
                    }
                });
        final AlertDialog alert = builder.create();
        alert.show();
    }

    public static void serviceUpdateError(final Context context, String message) {
        final AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setMessage(message)
                .setCancelable(false)
                .setPositiveButton("Update", new DialogInterface.OnClickListener() {
                    public void onClick(final DialogInterface dialog, final int id) {
                        dialog.cancel();
                        Uri uri = Uri.parse("market://details?id=com.google.android.gms");
                        Intent myAppLinkToMarket = new Intent(Intent.ACTION_VIEW, uri);
                        try {
                            context.startActivity(myAppLinkToMarket);
                        } catch (ActivityNotFoundException e) {
                            Utilities.showToastMessage(context, "Unable to find application on Play store.");
                        }
                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    public void onClick(final DialogInterface dialog, final int id) {
                        dialog.cancel();
                    }
                });
        final AlertDialog alert = builder.create();
        alert.show();
    }

    public static int dpToPx(int dp) {
        return (int) (dp * Resources.getSystem().getDisplayMetrics().density);
    }

    public static int getScreenWidth() {
        return Resources.getSystem().getDisplayMetrics().widthPixels;
    }

    public static int getScreenHeight() {
        return Resources.getSystem().getDisplayMetrics().heightPixels;
    }

    public static String encodeTobase64(Bitmap image) {
        String imageEncoded = null;
        if (image != null) {
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            image.compress(Bitmap.CompressFormat.JPEG, 100, baos);
            byte[] b = baos.toByteArray();
            imageEncoded = Base64.encodeToString(b, Base64.DEFAULT);
            return imageEncoded;
        }
        return imageEncoded;
    }

    public static Bitmap decodeBase64(String input) {
        byte[] decodedByte = Base64.decode(input, 0);
        return BitmapFactory.decodeByteArray(decodedByte, 0, decodedByte.length);
    }

    public static boolean isValidEmail(String email) {
        Pattern pattern;
        Matcher matcher;
        final String EMAIL_PATTERN = "^[_A-Za-z0-9-]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
        pattern = Pattern.compile(EMAIL_PATTERN);
        matcher = pattern.matcher(email);
        return matcher.matches();
    }

    public static String dateConvertToDayMonthYear(String date) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSz", Locale.ENGLISH);
        SimpleDateFormat output = new SimpleDateFormat("dd MMM", Locale.ENGLISH);
        Date d = null;
        try {
            d = sdf.parse(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return output.format(d);
    }

    public static String dateConvertToTime(String date) {
        if (date != null && !date.equals("null") || !date.equalsIgnoreCase("blank")) {
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSz", Locale.ENGLISH);
            SimpleDateFormat output = new SimpleDateFormat("HH:mm", Locale.ENGLISH);
            Date d = null;
            try {
                d = sdf.parse(date);
            } catch (ParseException e) {
                e.printStackTrace();
            }
            return output.format(d);
        } else {
            return "";
        }
    }

    public static String dateConvertToDayMonth(String date) {

        if (date != null && !date.equals("null")) {
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSz", Locale.ENGLISH);
            SimpleDateFormat output = new SimpleDateFormat("dd MMM", Locale.ENGLISH);
            Date d = null;
            try {
                d = sdf.parse(date);
            } catch (ParseException e) {
                e.printStackTrace();
            }
            date = output.format(d);
        } else {
            date = "No Date";
        }
        return date;
    }

    public static String dateConvertMinify(String date) {
        if (date != null && !date.equals("null")) {
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);
            SimpleDateFormat output = new SimpleDateFormat("dd MMM yyyy", Locale.ENGLISH);
            Date d = null;
            try {
                d = sdf.parse(date);
            } catch (Exception e) {
                e.printStackTrace();
            }
            date = output.format(d);
        } else {
            date = "No Date";
        }
        return date;
    }

    public static String ddMMYYYFormat(String dateStr) {
        //Only date
        if (dateStr != null && !dateStr.equals("null")) {
            DateFormat format = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);
            Date date = null;
            try {
                date = format.parse(dateStr);
            } catch (ParseException e) {
                e.printStackTrace();
            }
            //dateStr = new SimpleDateFormat("dd-MM-yyyy").format(date);
            dateStr = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH).format(date);
        } else {
            dateStr = "No Date";
        }
        return dateStr;
    }

    public static String m24TimeFormat(String dateStr) {
        //Only time 24
        if (!dateStr.equals("null")) {
            String[] parts = dateStr.split("T");
            String part2 = parts[1];
            dateStr = part2.substring(0, 5);
        } else {
            dateStr = "No Time";
        }
        return dateStr;
    }

    public static String ddMonYYYYFormat(String dateStr) {
        //date  1 "Jun" 2016 time
        if (!dateStr.equals("null")) {
            String[] parts = dateStr.split("T");
            String part1 = parts[0];
            String part2 = parts[1];
            DateFormat format = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);
            Date date = null;
            try {
                date = format.parse(part1);
                part2 = part2.substring(0, 5);
            } catch (ParseException e) {
                e.printStackTrace();
            }
            dateStr = new SimpleDateFormat("dd-MMM-yyyy", Locale.ENGLISH).format(date) + " " + part2;
        } else {
            dateStr = "No Date";
        }
        return dateStr;
    }

    public static String ddMonYYYYFormatWithoutTime(String dateStr) {

        if (dateStr != null && !dateStr.equals("null")) {
            String[] parts = dateStr.split("T");
            String part1 = parts[0];
            String part2 = parts[1];
            DateFormat format = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);
            Date date = null;
            try {
                date = format.parse(part1);
                part2 = part2.substring(0, 5);
            } catch (ParseException e) {
                e.printStackTrace();
            }
            dateStr = new SimpleDateFormat("dd-MMM-yyyy", Locale.ENGLISH).format(date) /*+ " " + part2*/;
        } else {
            dateStr = "No Date";
        }
        return dateStr;
    }

    public static void writeToFile(String imageEncoded, String fileName) {
        try {
            File filePath;
            if (Environment.getExternalStorageState() == null) {
                filePath = Environment.getDataDirectory();
            } else {
                filePath = Environment.getExternalStorageDirectory();
            }
            String finalString1 = imageEncoded;
            File file = new File(filePath.getAbsolutePath(), "/" + fileName);
            File file1;

            if (!file.exists()) {
                file.mkdir();
                file1 = new File(filePath.getAbsolutePath() + "/" + fileName, fileName + ".txt");
                if (!file1.exists()) {
                    file1.createNewFile();
                }
                FileWriter fw = new FileWriter(file1.getAbsoluteFile());
                BufferedWriter bw = new BufferedWriter(fw);
                bw.write(finalString1);
                bw.close();
            } else {
                file1 = new File(filePath.getAbsolutePath() + "/" + fileName, fileName + ".txt");
                if (!file1.exists()) {
                    file1.createNewFile();
                }
                FileWriter fw = new FileWriter(file1.getAbsoluteFile());
                BufferedWriter bw = new BufferedWriter(fw);
                bw.write(finalString1);
                bw.close();
            }

            //Log.e(fileName + "", "file done");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static String dateMonFormat(String dateString) {
// 1 jan
        String format = "No Data";
        if (!dateString.equals("No Date")) {
            try {
                String trimDate;
                if (dateString.contains("T")) {
                    trimDate = dateString.substring(0, dateString.indexOf("T"));
                } else {
                    trimDate = dateString;
                }
                String str[] = trimDate.split("-");
                int selectedYear = Integer.parseInt(str[0]);
                int selectedMonth = Integer.parseInt(str[1]);
                int selectedDay = Integer.parseInt(str[2]);
                Calendar cal = Calendar.getInstance();
                cal.set(Calendar.YEAR, selectedYear);
                cal.set(Calendar.DAY_OF_MONTH, selectedDay);
                cal.set(Calendar.MONTH, selectedMonth - 1);
                format = new SimpleDateFormat("d MMM ", Locale.ENGLISH).format(cal.getTime());
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return format;

    }

    public static String dateTrim(String dateString) {
        String trimDate = "No data";
        if (!dateString.equals("null")) {
            try {
                trimDate = dateString.substring(0, dateString.indexOf("T"));
                DateFormat format = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);
                Date date = format.parse(trimDate);
                trimDate = new SimpleDateFormat("dd-MM-yyyy", Locale.ENGLISH).format(date);
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            trimDate = "No Date";
        }
        return trimDate;

    }

    public static String getUserRoles(Context context) {
        String indexVal = "blank";
        LockatedPreferences mLockatedPreferences = new LockatedPreferences(context);
        if (mLockatedPreferences.getRolesJson() != null && !mLockatedPreferences.getRolesJson().equals("blank")) {
            try {
                JSONObject noticeJsonObj = new JSONObject(mLockatedPreferences.getRolesJson());
                for (int i = 0; i < noticeJsonObj.getJSONArray("permissions").length(); i++) {
                    JSONObject jsonObject1 = noticeJsonObj.getJSONArray("permissions").getJSONObject(i);
                    if (jsonObject1.has("section") && jsonObject1.getString("section").equals("spree_society_admin")) {
                        indexVal = jsonObject1.getJSONObject("permission").getString("index");
                    }
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return indexVal;
    }

    public static String ddMonTimeFormat(String fullDate) {
// 1 jan 20:16
        String date1, fulldate;
        if (!fullDate.equals("null")) {
            date1 = dateConvertMinify(fullDate);
            String trimTime = timeAmPm(fullDate);
            fulldate = (date1 + "  " + trimTime);
        } else {
            fulldate = "No Date";
        }
        return fulldate;
    }

    public static int getIdPosition(int id) {
        switch (id) {
            case 1:
                return 3;
            case 2:
                return 0;
            case 6:
                return 1;
            case 8:
                return 4;
            case 10:
                return 5;
            case 18:
                return 2;
            case 28:
                return 6;
            case 31:
                return 7;
            case 33:
                return 8;
            default:
                return 666;
        }
    }

    public static int setListImage(String word) {
        String alphabet = String.valueOf(word.charAt(0));
        if (alphabet.equalsIgnoreCase("a")) {
            return R.drawable.a52;
        } else if (alphabet.equalsIgnoreCase("b")) {
            return R.drawable.b52;
        } else if (alphabet.equalsIgnoreCase("c")) {
            return R.drawable.c52;
        } else if (alphabet.equalsIgnoreCase("d")) {
            return R.drawable.d52;
        } else if (alphabet.equalsIgnoreCase("e")) {
            return R.drawable.e52;
        } else if (alphabet.equalsIgnoreCase("f")) {
            return R.drawable.f52;
        } else if (alphabet.equalsIgnoreCase("g")) {
            return R.drawable.g52;
        } else if (alphabet.equalsIgnoreCase("h")) {
            return R.drawable.h52;
        } else if (alphabet.equalsIgnoreCase("i")) {
            return R.drawable.i52;
        } else if (alphabet.equalsIgnoreCase("j")) {
            return R.drawable.j52;
        } else if (alphabet.equalsIgnoreCase("k")) {
            return R.drawable.k52;
        } else if (alphabet.equalsIgnoreCase("l")) {
            return R.drawable.l52;
        } else if (alphabet.equalsIgnoreCase("m")) {
            return R.drawable.m52;
        } else if (alphabet.equalsIgnoreCase("n")) {
            return R.drawable.n52;
        } else if (alphabet.equalsIgnoreCase("o")) {
            return R.drawable.o52;
        } else if (alphabet.equalsIgnoreCase("p")) {
            return R.drawable.p52;
        } else if (alphabet.equalsIgnoreCase("q")) {
            return R.drawable.q52;
        } else if (alphabet.equalsIgnoreCase("r")) {
            return R.drawable.r52;
        } else if (alphabet.equalsIgnoreCase("s")) {
            return R.drawable.s52;
        } else if (alphabet.equalsIgnoreCase("t")) {
            return R.drawable.t52;
        } else if (alphabet.equalsIgnoreCase("u")) {
            return R.drawable.u52;
        } else if (alphabet.equalsIgnoreCase("v")) {
            return R.drawable.v52;
        } else if (alphabet.equalsIgnoreCase("w")) {
            return R.drawable.w52;
        } else if (alphabet.equalsIgnoreCase("x")) {
            return R.drawable.x52;
        } else if (alphabet.equalsIgnoreCase("y")) {
            return R.drawable.y52;
        } else {
            return R.drawable.z52;
        }
    }

    public static void lockatedGoogleAnalytics(String category, String action, String label) {
        LockatedApplication.getInstance().trackEvent(category, action, label);
    }

    public static void ladooIntegration(Context context, String screenName) {
        Genie.getInstance().trackEvent(context, screenName);
    }

    /////////Used Time Format
    public static String dateTime_AMPM_Format(String fullDate) {
        //11 May 3:22PM
        String fulldate;
        if (!fullDate.equals("null")) {
            String date1;
            date1 = dateMonFormat(fullDate);
            String trimTime = timeAmPm(fullDate);
            fulldate = (date1 + "  " + trimTime);
        } else {
            fulldate = "No Date";
        }
        return fulldate;
    }

    public static String timeAmPm(String fullDate) {
        String trimTime = fullDate.substring(fullDate.indexOf("T") + 1, fullDate.length());
        StringTokenizer tk = new StringTokenizer(trimTime);
        String time = tk.nextToken();
        SimpleDateFormat sdf = new SimpleDateFormat("hh:mm:ss", Locale.ENGLISH);
        SimpleDateFormat sdfs = new SimpleDateFormat("hh:mm a", Locale.ENGLISH);
        Date dt;
        try {
            dt = sdf.parse(time);
            trimTime = sdfs.format(dt);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return (trimTime);
    }

    public static String ddMonYYYYTimeFormat(String dateStr) {
        //24hr time
        if (dateStr != null && !dateStr.equals("null")) {
            String[] parts = dateStr.split("T");
            String part1 = parts[0];
            String part2 = parts[1];
            DateFormat format = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);
            Date date = null;
            try {
                date = format.parse(part1);
                part2 = part2.substring(0, 5);
            } catch (ParseException e) {
                e.printStackTrace();
            }
            dateStr = new SimpleDateFormat("dd-MMM-yyyy", Locale.ENGLISH).format(date) + " " + part2;
        } else {
            dateStr = "No Date";
        }
        return dateStr;
    }

    public static ArrayList<UserRoles> getUserRole(Context context, String spreeName) {
        String CREATE, INDEX, UPDATE, EDIT, SHOW;
        ArrayList<UserRoles> userRolesArrayList = new ArrayList<>();
        String mySocietyRoles = context.getString(R.string.blank_value);
        LockatedPreferences mLockatedPreferences = new LockatedPreferences(context);
        if (mLockatedPreferences.getRolesJson() != null && !mLockatedPreferences.getRolesJson().equals("blank")) {
            try {
                JSONObject noticeJsonObj = new JSONObject(mLockatedPreferences.getRolesJson());
                for (int i = 0; i < noticeJsonObj.getJSONArray(context.getString(R.string.permissions_value)).length(); i++) {
                    JSONObject jsonObject1 = noticeJsonObj.getJSONArray(context.getString(R.string.permissions_value)).getJSONObject(i);
                    if (jsonObject1.has(context.getString(R.string.section_value))
                            && jsonObject1.getString(context.getString(R.string.section_value)).equals(spreeName)) {
                        if (jsonObject1.getJSONObject(context.getString(R.string.permission_value)).has(context.getString(R.string.index_value))) {
                            mySocietyRoles = jsonObject1.getJSONObject(context.getString(R.string.permission_value)).getString(context.getString(R.string.index_value));
                            if (jsonObject1.getJSONObject(context.getString(R.string.permission_value)).has("create")) {
                                userRolesArrayList.get(0).setCREATE(jsonObject1.getJSONObject(context.getString(R.string.permission_value)).getString("create"));
                            }
                            if (jsonObject1.getJSONObject(context.getString(R.string.permission_value)).has("index")) {
                                userRolesArrayList.get(0).setINDEX(jsonObject1.getJSONObject(context.getString(R.string.permission_value)).getString("index"));
                            }
                            if (jsonObject1.getJSONObject(context.getString(R.string.permission_value)).has("update")) {
                                userRolesArrayList.get(0).setUPDATE(jsonObject1.getJSONObject(context.getString(R.string.permission_value)).getString("update"));
                            }
                            if (jsonObject1.getJSONObject(context.getString(R.string.permission_value)).has("edit")) {
                                userRolesArrayList.get(0).setEDIT(jsonObject1.getJSONObject(context.getString(R.string.permission_value)).getString("edit"));
                            }
                            if (jsonObject1.getJSONObject(context.getString(R.string.permission_value)).has("show")) {
                                userRolesArrayList.get(0).setSHOW(jsonObject1.getJSONObject(context.getString(R.string.permission_value)).getString("show"));
                            }
                        }
                    }
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return userRolesArrayList;
    }

    public static void ecommerceMeasureTransaction(JSONObject productJsonObject, JSONObject productActionJsonObject,
                                                   String screenName, String email) {
        try {
            Product productObject = new Product();
            productObject.setId(productJsonObject.optString("setId"))
                    .setName(productJsonObject.optString("setName"))
                    .setCategory(productJsonObject.optString("setCategory"))
                    .setBrand(productJsonObject.optString("setBrand"))
                    .setVariant(productJsonObject.optString("setVariant"))
                    .setPrice(Double.valueOf(productJsonObject.optString("setPrice")))
                    .setQuantity(productJsonObject.optInt("setQuantity"));
            Log.e("productObject", "" + productObject.toString());
            ProductAction productAction = new ProductAction(ProductAction.ACTION_PURCHASE)
                    .setTransactionId(productActionJsonObject.optString("setTransactionId"))
                    .setTransactionAffiliation(productActionJsonObject.optString("setTransactionAffiliation"))
                    .setTransactionRevenue(Double.valueOf(productActionJsonObject.optString("setTransactionRevenue")))
                    .setTransactionTax(Double.valueOf(productActionJsonObject.optString("setTransactionTax")))
                    .setTransactionShipping(Double.valueOf(productActionJsonObject.optString("setTransactionShipping")))
                    .setTransactionCouponCode(productActionJsonObject.optString("setTransactionCouponCode"));
            Log.e("productAction", "" + productAction.toString());
            LockatedApplication.gaMeasureTransaction(productObject, productAction, screenName, email);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void relaunchApp(Activity activity) {
        Thread.setDefaultUncaughtExceptionHandler(new DefaultExceptionHandler(activity));
    }

}