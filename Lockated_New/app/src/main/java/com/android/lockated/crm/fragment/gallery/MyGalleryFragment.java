package com.android.lockated.crm.fragment.gallery;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Environment;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridView;

import com.android.lockated.R;
import com.android.lockated.crm.activity.CreateGalleryActivity;
import com.android.lockated.utils.Utilities;

import java.io.File;
import java.util.ArrayList;

public class MyGalleryFragment extends Fragment implements View.OnClickListener {

    GridView galleryGridView;
    FloatingActionButton createGallery;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View galleryView = inflater.inflate(R.layout.fragment_gallery, container, false);
        Utilities.ladooIntegration(getActivity(), getString(R.string.my_gallery));
        Utilities.lockatedGoogleAnalytics(getString(R.string.my_gallery), getString(R.string.visited), getString(R.string.my_gallery));
        if (getActivity() != null) {
            checkFileStorage(getActivity());
        }
        init(galleryView);
        return galleryView;
    }

    private void init(View galleryView) {
        galleryGridView = (GridView) galleryView.findViewById(R.id.galleryGridView);
        createGallery = (FloatingActionButton) galleryView.findViewById(R.id.createGallery);
        createGallery.setOnClickListener(this);
    }

    private void checkFileStorage(Context context) {
        ArrayList<String> fileArrayList = new ArrayList<>();
        File filePath;
        if (context.getFilesDir().exists()) {
            filePath = context.getDir("LockatedGallery", Context.MODE_PRIVATE);
        } else {
            filePath = Environment.getExternalStorageDirectory();
        }
        File myFile = new File(filePath, "/");
        myFile.mkdirs();
        for (int i = 0; i < 3; i++) {
            String ab = "file_" + i;
            File file = new File(myFile.getAbsoluteFile() + File.separator + ab);
            file.mkdir();
        }
        //File newFile = new File(filePath + "/", "");
        /*Log.e("myFile", "" + myFile.getAbsolutePath());
        Log.e("myFile 1", "" + myFile.length());*/
        /*if (myFile.listFiles().length > 0) {
            Log.e("myFile 2", "" + myFile.listFiles().length);
            for (int j = 0; j < myFile.listFiles().length; j++) {
                fileArrayList.add(myFile.getAbsoluteFile().toString());
            }
        }
        for (int j = 0; j < fileArrayList.size(); j++) {
            Log.e("fileArrayList", "" + fileArrayList.get(j));
        }*/

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.createGallery:
                Intent intent = new Intent(getActivity(), CreateGalleryActivity.class);
                getActivity().startActivity(intent);
                break;
        }
    }
}
