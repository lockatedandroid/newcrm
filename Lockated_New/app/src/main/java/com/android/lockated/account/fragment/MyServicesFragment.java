package com.android.lockated.account.fragment;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.android.lockated.Interfaces.IRecyclerItemClickListener;
import com.android.lockated.LockatedApplication;
import com.android.lockated.R;
import com.android.lockated.account.adapter.MyServicesAdapter;
import com.android.lockated.information.AccountController;
import com.android.lockated.model.MyServices.ServiceRequest;
import com.android.lockated.network.ConnectionDetector;
import com.android.lockated.preferences.LockatedPreferences;
import com.android.lockated.request.LockatedRequestError;
import com.android.lockated.request.LockatedVolleyRequestQueue;
import com.android.lockated.utils.ApplicationURL;
import com.android.lockated.utils.Utilities;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class MyServicesFragment extends Fragment implements Response.Listener, Response.ErrorListener, IRecyclerItemClickListener {

    private View mMyServicesView;

    private ProgressDialog mProgressDialog;
    private RecyclerView mRecyclerView;
    private RecyclerView.LayoutManager mLayoutManager;

    private MyServicesAdapter myServicesAdapter;
    private LockatedPreferences mLockatedPreferences;

    private ArrayList<ServiceRequest> serviceRequestArrayList;
    private AccountController mAccountController;

    private int mPosition;

    public static final String REQUEST_TAG = "MyServicesFragment";
    public static final String REQUEST_CANCEL = "MyServicesFragment";

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mMyServicesView = inflater.inflate(R.layout.fragment_recycler, container, false);
        init();
        Utilities.ladooIntegration(getActivity(), getActivity().getString(R.string.my_services_screen));
        return mMyServicesView;
    }

    @Override
    public void onResume() {
        super.onResume();
        LockatedApplication.getInstance().trackScreenView(getActivity().getString(R.string.my_services_screen));
    }

    private void init() {
        serviceRequestArrayList = new ArrayList<>();
        mAccountController = AccountController.getInstance();
        mLockatedPreferences = new LockatedPreferences(getActivity());

        mRecyclerView = (RecyclerView) mMyServicesView.findViewById(R.id.mRecyclerView);

        mLayoutManager = new LinearLayoutManager(getActivity());
        mRecyclerView.setLayoutManager(mLayoutManager);
        mRecyclerView.setHasFixedSize(true);
        myServicesAdapter = new MyServicesAdapter(serviceRequestArrayList, this);
        mRecyclerView.setAdapter(myServicesAdapter);
        getMyServiceRequests();
    }

    private void getMyServiceRequests() {
        if (ConnectionDetector.isConnectedToInternet(getActivity())) {
            mProgressDialog = ProgressDialog.show(getActivity(), "", "Please Wait...", false);
            mProgressDialog.show();
            String url = ApplicationURL.getMyServiceRequests + mLockatedPreferences.getLockatedToken();
            LockatedVolleyRequestQueue lockatedVolleyRequestQueue = LockatedVolleyRequestQueue.getInstance(getActivity());
            lockatedVolleyRequestQueue.sendRequest(REQUEST_TAG, Request.Method.GET, url, null, this, this);
        } else {
            Utilities.showToastMessage(getActivity(), getActivity().getResources().getString(R.string.internet_connection_error));
        }
    }

    private void onServiceCancel(int position) {
        if (ConnectionDetector.isConnectedToInternet(getActivity())) {
            mProgressDialog = ProgressDialog.show(getActivity(), "", "Please Wait...", false);
            mProgressDialog.show();
            String url = ApplicationURL.cancelMyServiceRequests + serviceRequestArrayList.get(position).getId()
                    + "&token=" + mLockatedPreferences.getLockatedToken();
            LockatedVolleyRequestQueue lockatedVolleyRequestQueue = LockatedVolleyRequestQueue.getInstance(getActivity());
            lockatedVolleyRequestQueue.sendRequest(REQUEST_CANCEL, Request.Method.GET, url, null, this, this);
        } else {
            Utilities.showToastMessage(getActivity(), getActivity().getResources().getString(R.string.internet_connection_error));
        }
    }

    @Override
    public void onErrorResponse(VolleyError error) {
        mProgressDialog.dismiss();
        if (getActivity() != null) {
            LockatedRequestError.onRequestError(getActivity(), error);
        }
    }

    @Override
    public void onResponse(Object response) {
        mProgressDialog.dismiss();
        if (getActivity() != null) {
            try {
                JSONObject jsonObject = (JSONObject) response;
                if (jsonObject.has("service_requests")) {
                    if (jsonObject.getJSONArray("service_requests").length() > 0) {
                        JSONArray jsonArray = jsonObject.getJSONArray("service_requests");
                        for (int i = 0; i < jsonArray.length(); i++) {
                            ServiceRequest serviceRequest = new Gson().fromJson(jsonArray.getJSONObject(i).toString(),
                                    ServiceRequest.class);
                            serviceRequestArrayList.add(serviceRequest);
                        }
                        myServicesAdapter.notifyDataSetChanged();
                    } else {

                    }
                } else if (jsonObject.has("id")) {
                    String state = jsonObject.getString("state");
                    boolean canCancel = jsonObject.getBoolean("can_cancel");
                    serviceRequestArrayList.get(mPosition).setState(state);
                    serviceRequestArrayList.get(mPosition).setCanCancel(canCancel);
                    myServicesAdapter.notifyDataSetChanged();
                } else {
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void onRecyclerItemClick(View view, int position) {
        switch (view.getId()) {
            case R.id.mTextViewOrderCancel:
                mPosition = position;
                if (serviceRequestArrayList.get(mPosition).getState().equalsIgnoreCase("canceled")) {
                    Utilities.showToastMessage(getActivity(), "Service is already canceled");
                } else {
                    onServiceCancel(position);
                }
                break;
        }
    }

}