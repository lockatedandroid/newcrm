package com.android.lockated.checkout;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.android.lockated.Interfaces.IRecyclerItemClickListener;
import com.android.lockated.LockatedApplication;
import com.android.lockated.R;
import com.android.lockated.information.AccountController;
import com.android.lockated.model.AccountData;
import com.android.lockated.model.CartDetail;
import com.android.lockated.network.ConnectionDetector;
import com.android.lockated.preferences.LockatedPreferences;
import com.android.lockated.request.LockatedJSONObjectRequest;
import com.android.lockated.request.LockatedRequestError;
import com.android.lockated.request.LockatedVolleyRequestQueue;
import com.android.lockated.utils.ApplicationURL;
import com.android.lockated.utils.LockatedConfig;
import com.android.lockated.utils.Utilities;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.google.android.gms.analytics.ecommerce.Product;
import com.google.android.gms.analytics.ecommerce.ProductAction;
import com.paytm.pgsdk.PaytmMerchant;
import com.paytm.pgsdk.PaytmOrder;
import com.paytm.pgsdk.PaytmPGService;
import com.paytm.pgsdk.PaytmPaymentTransactionCallback;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;

public class PaymentFragment extends Fragment implements Response.ErrorListener, IRecyclerItemClickListener,
        Response.Listener<JSONObject> {

    private View mPaymentFragmentView;

    private ProgressDialog mProgressDialog;
    private RecyclerView mRecyclerView;
    private RecyclerView.LayoutManager mLayoutManager;

    private PaymentOptionAdapter mPaymentOptionAdapter;

    private RequestQueue mQueue;
    private LockatedPreferences mLockatedPreferences;

    private ArrayList<String> mPaymentOptions;
    private ArrayList<Integer> mPaymentIcons;
    private String mOrderNumber;
    private TextView mPaybleAmount;
    public static final String REQUEST_TAG = "PaymentFragment";
    public static final String PAYMENT_SUCCESS = "PayTmPaymentFragment";
    public static final String GET_PROMO_CODE_TAG = "PayTmPromoCode";
    public static final String ScreenName = "Payment Screen";
    String totalAmount;
    private int randomInt = 0;
    private PaytmPGService Service = null;
    JSONArray paytmPromoCodeJsonArray;
    static boolean foundCode = false;

    @org.jetbrains.annotations.Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mPaymentFragmentView = inflater.inflate(R.layout.fragment_select_payment, container, false);
        init();
        Utilities.ladooIntegration(getActivity(), ScreenName);
        LockatedApplication.getInstance().trackScreenView(getString(R.string.payment_screen));
        Utilities.lockatedGoogleAnalytics(getString(R.string.payment_screen), getString(R.string.payment_screen),
                getString(R.string.payment_screen));
        return mPaymentFragmentView;
    }

    private void init() {
        mPaymentOptions = new ArrayList<>();
        mPaymentIcons = new ArrayList<>();
        mPaymentIcons.add(R.drawable.cod);
        mPaymentIcons.add(R.drawable.online);
        mPaymentOptions.add("Cash On Delivery");
        mPaymentOptions.add("Pay Online");
        mPaybleAmount = (TextView) mPaymentFragmentView.findViewById(R.id.mPaybleAmount);
        mLockatedPreferences = new LockatedPreferences(getActivity());
        ArrayList<AccountData> mAccountData = AccountController.getInstance().getmAccountDataList();
        if (mAccountData.size() > 0 && mAccountData.get(0).getCartDetailList().size() > 0 &&
                mAccountData.get(0).getCartDetailList().get(0).getTotal() != null) {
            totalAmount = mAccountData.get(0).getCartDetailList().get(0).getTotal();
            mPaybleAmount.setText(totalAmount);
        }
        mRecyclerView = (RecyclerView) mPaymentFragmentView.findViewById(R.id.mRecyclerView);
        mProgressDialog = ProgressDialog.show(getActivity(), "", "Please Wait...", false);

        mLayoutManager = new LinearLayoutManager(getActivity());
        mRecyclerView.setLayoutManager(mLayoutManager);
        mRecyclerView.setHasFixedSize(true);

        mPaymentOptionAdapter = new PaymentOptionAdapter(mPaymentOptions, mPaymentIcons, this);
        mRecyclerView.setAdapter(mPaymentOptionAdapter);
        getPayTmPromoCode();
    }

    public void getPayTmPromoCode() {
        if (getActivity() != null) {
            if (ConnectionDetector.isConnectedToInternet(getActivity())) {
                mProgressDialog.show();
                String url = ApplicationURL.getPaytmPromoCodeUrl;
                LockatedVolleyRequestQueue lockatedVolleyRequestQueue = LockatedVolleyRequestQueue.getInstance(getActivity());
                lockatedVolleyRequestQueue.sendRequest(GET_PROMO_CODE_TAG, Request.Method.GET, url, null, this, this);
                /*mQueue = LockatedVolleyRequestQueue.getInstance(getActivity()).getRequestQueue();
                String url = ApplicationURL.getPaytmPromoCodeUrl;
                LockatedJSONObjectRequest lockatedJSONObjectRequest = new LockatedJSONObjectRequest(Request.Method.GET,
                        url, null, this, this);
                lockatedJSONObjectRequest.setTag(GET_PROMO_CODE_TAG);
                mQueue.add(lockatedJSONObjectRequest);*/
            } else {
                Utilities.showToastMessage(getActivity(), getActivity().getResources().getString(R.string.internet_connection_error));
            }
        }
    }

    private void onPaymentOptionSelected(int pos) {

        if (ConnectionDetector.isConnectedToInternet(getActivity())) {
            ArrayList<AccountData> mAccountData = AccountController.getInstance().getmAccountDataList();
            ArrayList<CartDetail> mCartDetail = null;
            if (mAccountData.size() > 0) {
                mCartDetail = mAccountData.get(0).getCartDetailList();
                if (mCartDetail.size() > 0) {
                    mOrderNumber = mCartDetail.get(0).getNumber();
                    JSONObject jsonObject = new JSONObject();
                    JSONObject orderObject = new JSONObject();
                    JSONArray jsonArray = new JSONArray();
                    try {
                        jsonObject.put("order", orderObject);
                        orderObject.put("payments_attributes", jsonArray);
                        for (int i = 0; i < 1; i++) {
                            JSONObject itemObject = new JSONObject();
                            itemObject.put("payment_method_id", "2");
                            jsonArray.put(i, itemObject);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    if (pos == 0) {

                        /*mProgressDialog = ProgressDialog.show(getActivity(), "", "Please Wait...", false);*/
                        mProgressDialog.show();
                        String url = ApplicationURL.proceedToPaymentUrl + mCartDetail.get(0).getNumber() +
                                ".json?token=" + mLockatedPreferences.getLockatedToken();
                        LockatedVolleyRequestQueue lockatedVolleyRequestQueue = LockatedVolleyRequestQueue.getInstance(getActivity());
                        lockatedVolleyRequestQueue.sendRequest(REQUEST_TAG, Request.Method.PUT, url, jsonObject, this, this);
                        /*mQueue = LockatedVolleyRequestQueue.getInstance(getActivity()).getRequestQueue();
                        String url = ApplicationURL.proceedToPaymentUrl + mCartDetail.get(0).getNumber() +
                                ".json?token=" + mLockatedPreferences.getLockatedToken();
                        LockatedJSONObjectRequest lockatedJSONObjectRequest = new LockatedJSONObjectRequest(Request.Method.PUT,
                                url, jsonObject, this, this);
                        lockatedJSONObjectRequest.setTag(REQUEST_TAG);
                        mQueue.add(lockatedJSONObjectRequest);*/

                    } else if (pos == 1) {

                        Random randomGenerator = new Random();
                        randomInt = randomGenerator.nextInt(1000);

                        //Service = PaytmPGService.getStagingService();
                        Service = PaytmPGService.getProductionService();

                        String orderIdAppend = mCartDetail.get(0).getId() + "-lock-" + randomInt;
                        Map<String, String> paramMap = new HashMap<>();
                        String GenerateCheckSumUrl;
                        if (verifyPromoCode(mCartDetail.get(0).getPromoCode())) {
                            paramMap.put("ORDER_ID", "" + orderIdAppend);
                            paramMap.put("MID", LockatedConfig.midValue);
                            paramMap.put("CUST_ID", "" + mCartDetail.get(0).getUserId());
                            paramMap.put("CHANNEL_ID", LockatedConfig.channelId);
                            paramMap.put("INDUSTRY_TYPE_ID", LockatedConfig.intustryTypeId);
                            paramMap.put("WEBSITE", LockatedConfig.website);
                            paramMap.put("TXN_AMOUNT", mCartDetail.get(0).getTotal());
                            paramMap.put("REQUEST_TYPE", LockatedConfig.requestType);
                            paramMap.put("THEME", LockatedConfig.theme);
                            paramMap.put("MOBILE_NO", mAccountData.get(0).getMobile());
                            paramMap.put("EMAIL", mAccountData.get(0).getEmail());
                            paramMap.put("PROMO_CAMP_ID", mCartDetail.get(0).getPromoCode());

                            GenerateCheckSumUrl = ApplicationURL.paytmGenerateCheckSumUrl
                                    + "ORDER_ID=" + orderIdAppend
                                    + "&MID=" + LockatedConfig.midValue
                                    + "&CUST_ID=" + mCartDetail.get(0).getUserId()
                                    + "&CHANNEL_ID=" + LockatedConfig.channelId
                                    + "&INDUSTRY_TYPE_ID=" + LockatedConfig.intustryTypeId
                                    + "&WEBSITE=" + LockatedConfig.website
                                    + "&TXN_AMOUNT=" + mCartDetail.get(0).getTotal()
                                    + "&REQUEST_TYPE=" + LockatedConfig.requestType
                                    + "&THEME=" + LockatedConfig.theme
                                    + "&MOBILE_NO=" + mAccountData.get(0).getMobile()
                                    + "&EMAIL=" + mAccountData.get(0).getEmail()
                                    + "&token=" + mLockatedPreferences.getLockatedToken()
                                    + "&PROMO_CAMP_ID=" + mCartDetail.get(0).getPromoCode();

                        } else {
                            paramMap.put("ORDER_ID", "" + orderIdAppend);
                            paramMap.put("MID", LockatedConfig.midValue);
                            paramMap.put("CUST_ID", "" + mCartDetail.get(0).getUserId());
                            paramMap.put("CHANNEL_ID", LockatedConfig.channelId);
                            paramMap.put("INDUSTRY_TYPE_ID", LockatedConfig.intustryTypeId);
                            paramMap.put("WEBSITE", LockatedConfig.website);
                            paramMap.put("TXN_AMOUNT", mCartDetail.get(0).getTotal());
                            paramMap.put("REQUEST_TYPE", LockatedConfig.requestType);
                            paramMap.put("THEME", LockatedConfig.theme);
                            paramMap.put("MOBILE_NO", mAccountData.get(0).getMobile());
                            paramMap.put("EMAIL", mAccountData.get(0).getEmail());

                            GenerateCheckSumUrl = ApplicationURL.paytmGenerateCheckSumUrl
                                    + "ORDER_ID=" + orderIdAppend
                                    + "&MID=" + LockatedConfig.midValue
                                    + "&CUST_ID=" + mCartDetail.get(0).getUserId()
                                    + "&CHANNEL_ID=" + LockatedConfig.channelId
                                    + "&INDUSTRY_TYPE_ID=" + LockatedConfig.intustryTypeId
                                    + "&WEBSITE=" + LockatedConfig.website
                                    + "&TXN_AMOUNT=" + mCartDetail.get(0).getTotal()
                                    + "&REQUEST_TYPE=" + LockatedConfig.requestType
                                    + "&THEME=" + LockatedConfig.theme
                                    + "&MOBILE_NO=" + mAccountData.get(0).getMobile()
                                    + "&EMAIL=" + mAccountData.get(0).getEmail()
                                    + "&token=" + mLockatedPreferences.getLockatedToken()
                                    /*+ "&PROMO_CAMP_ID=" + mCartDetail.get(0).getPromoCode()*/;
                        }
                        PaytmOrder Order = new PaytmOrder(paramMap);

                        String VerifyCheckSumUrl = ApplicationURL.paytmVerifyCheckSumUrl;
                        /*Log.e("GenerateCheckSumUrl", "" + GenerateCheckSumUrl);
                        Log.e("paytmVerifyCheckSumUrl", "" + VerifyCheckSumUrl);*/

                        PaytmMerchant Merchant = new PaytmMerchant(GenerateCheckSumUrl, VerifyCheckSumUrl);

                        Service.initialize(Order, Merchant, null);
                        Service.startPaymentTransaction(getActivity(), false, false, new PaytmPaymentTransactionCallback() {
                            @Override
                            public void onTransactionSuccess(Bundle bundle) {
                                dismissProgress();
                                //Log.e("onTransactionSuccess", "called");
                                JSONObject jsonObjectTxnSuccessful = new JSONObject();
                                try {
                                    jsonObjectTxnSuccessful.put("STATUS", bundle.getString("STATUS"));
                                    jsonObjectTxnSuccessful.put("BANKNAME", bundle.getString("BANKNAME"));
                                    jsonObjectTxnSuccessful.put("ORDERID", bundle.getString("ORDERID"));
                                    jsonObjectTxnSuccessful.put("TXNAMOUNT", bundle.getString("TXNAMOUNT"));
                                    jsonObjectTxnSuccessful.put("TXNDATE", bundle.getString("TXNDATE"));
                                    jsonObjectTxnSuccessful.put("MID", bundle.getString("MID"));
                                    jsonObjectTxnSuccessful.put("TXNID", bundle.getString("TXNID"));
                                    jsonObjectTxnSuccessful.put("RESPCODE", bundle.getString("RESPCODE"));
                                    jsonObjectTxnSuccessful.put("PAYMENTMODE", bundle.getString("PAYMENTMODE"));
                                    jsonObjectTxnSuccessful.put("BANKTXNID", bundle.getString("BANKTXNID"));
                                    jsonObjectTxnSuccessful.put("CURRENCY", bundle.getString("CURRENCY"));
                                    jsonObjectTxnSuccessful.put("GATEWAYNAME", bundle.getString("GATEWAYNAME"));
                                    jsonObjectTxnSuccessful.put("IS_CHECKSUM_VALID", bundle.getString("IS_CHECKSUM_VALID"));
                                    jsonObjectTxnSuccessful.put("RESPMSG", bundle.getString("RESPMSG"));
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                                successfulTransaction(jsonObjectTxnSuccessful);
                            }

                            @Override
                            public void onTransactionFailure(String s, Bundle bundle) {
                                dismissProgress();
                                //Log.e("onTransactionFailure", "called");
                                Utilities.showToastMessage(getActivity(), bundle.getString("RESPMSG"));
                            }

                            @Override
                            public void networkNotAvailable() {
                                dismissProgress();
                                //Log.e("networkNotAvailable", "called");
                                Utilities.showToastMessage(getActivity(), getActivity().getResources().getString(R.string.connection_error));
                            }

                            @Override
                            public void clientAuthenticationFailed(String s) {
                                dismissProgress();
                                //Log.e("clientAuthenticationFailed", "called " + s);
                                Utilities.showToastMessage(getActivity(), s);
                            }

                            @Override
                            public void someUIErrorOccurred(String s) {
                                dismissProgress();
                                //Log.e("someUIErrorOccurred", "called " + s);
                                Utilities.showToastMessage(getActivity(), s);
                            }

                            @Override
                            public void onErrorLoadingWebPage(int i, String s, String s2) {
                                dismissProgress();
                                //Log.e("onErrorLoadingWebPage", "called " + s);
                                Utilities.showToastMessage(getActivity(), s);
                            }

                            @Override
                            public void onBackPressedCancelTransaction() {
                                dismissProgress();
                            }
                        });
                    }
                }
            }
        } else {
            Utilities.showToastMessage(getActivity(), getActivity().getResources().getString(R.string.internet_connection_error));
        }
    }

    @Override
    public void onErrorResponse(VolleyError error) {
        if (mProgressDialog != null && mProgressDialog.isShowing()) {
            mProgressDialog.dismiss();
        }
        if (getActivity() != null) {
            LockatedRequestError.onRequestError(getActivity(), error);
        }
    }

    @Override
    public void onResponse(JSONObject response) {
        if (mProgressDialog != null && mProgressDialog.isShowing()) {
            mProgressDialog.dismiss();
        }
        if (getActivity() != null) {
            if (response != null && response.has("paytm_promo_code")) {
                //Log.e("paytm_promo_code", "" + response);
                try {
                    paytmPromoCodeJsonArray = response.getJSONArray("paytm_promo_code");
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else if (response != null && response.has("message")) {
                //Log.e("PayTm response", "" + response);
                try {
                    if (response.getInt("code") == 200) {
                        ArrayList<AccountData> accountData = AccountController.getInstance().getmAccountDataList();
                        JSONObject productJsonObject = new JSONObject();
                        JSONObject productActionJsonObject = new JSONObject();
                        try {
                            productJsonObject.put("setId", accountData.get(0).getCartDetailList().get(0)
                                    .getNumber());
                            productJsonObject.put("setName", accountData.get(0).getCartDetailList().get(0)
                                    .getmLineItemData().get(0).getVariantName());
                            productJsonObject.put("setCategory", accountData.get(0).getCartDetailList().get(0)
                                    .getLabel());
                            productJsonObject.put("setBrand", accountData.get(0).getCartDetailList().get(0)
                                    .getmLineItemData().get(0).getVariantName());
                            productJsonObject.put("setVariant", accountData.get(0).getCartDetailList().get(0)
                                    .getLabel());
                            productJsonObject.put("setPrice", accountData.get(0).getCartDetailList().get(0)
                                    .getTotal());
                            productJsonObject.put("setQuantity", accountData.get(0).getCartDetailList().size());

                            productActionJsonObject.put("setTransactionId", mOrderNumber);
                            productActionJsonObject.put("User Email", accountData.get(0).getEmail());
                            productActionJsonObject.put("setTransactionAffiliation", "Lockated");
                            productActionJsonObject.put("setTransactionRevenue", accountData.get(0)
                                    .getCartDetailList().get(0).getTotal());
                            productActionJsonObject.put("setTransactionTax", "0.00");
                            productActionJsonObject.put("setTransactionShipping", "0.00");
                            productActionJsonObject.put("setTransactionCouponCode", accountData.get(0)
                                    .getCartDetailList().get(0).getPromoCode());
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                        Utilities.ecommerceMeasureTransaction(productJsonObject, productActionJsonObject,
                                ScreenName, productActionJsonObject.getString("User Email"));

                        double totalVal = Double.valueOf(accountData.get(0).getCartDetailList().get(0).getTotal());
                        //Log.e("sale_gt_500", "totalVal " + totalVal);
                        if (totalVal == 500.0 || totalVal > 500.0) {
                            Utilities.ladooIntegration(getActivity(), "sale_gt_500");
                        }
                        if (accountData.get(0).getCartDetailList().size() > 0) {
                            accountData.get(0).getCartDetailList().clear();
                        }

                        callCongratulationFragment();
                    } else {
                        Utilities.showToastMessage(getActivity(), response.getString("message"));
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            } else {
                //Log.e("COD response", "" + response);
                ArrayList<AccountData> accountData = AccountController.getInstance().getmAccountDataList();

                JSONObject productJsonObject = new JSONObject();
                JSONObject productActionJsonObject = new JSONObject();
                try {
                    productJsonObject.put("setId", accountData.get(0).getCartDetailList().get(0).getNumber());
                    productJsonObject.put("setName", accountData.get(0).getCartDetailList().get(0)
                            .getmLineItemData().get(0).getVariantName());
                    productJsonObject.put("setCategory", accountData.get(0).getCartDetailList().get(0)
                            .getLabel());
                    productJsonObject.put("setBrand", accountData.get(0).getCartDetailList().get(0)
                            .getmLineItemData().get(0).getVariantName());
                    productJsonObject.put("setVariant", accountData.get(0).getCartDetailList().get(0)
                            .getLabel());
                    productJsonObject.put("setPrice", accountData.get(0).getCartDetailList().get(0)
                            .getTotal());
                    productJsonObject.put("setQuantity", accountData.get(0).getCartDetailList().size());


                    productActionJsonObject.put("setTransactionId", mOrderNumber);
                    productActionJsonObject.put("User Email", accountData.get(0).getEmail());
                    productActionJsonObject.put("setTransactionAffiliation", "Lockated");
                    productActionJsonObject.put("setTransactionRevenue", accountData.get(0)
                            .getCartDetailList().get(0).getTotal());
                    productActionJsonObject.put("setTransactionTax", "0.00");
                    productActionJsonObject.put("setTransactionShipping", "0.00");
                    productActionJsonObject.put("setTransactionCouponCode", accountData.get(0)
                            .getCartDetailList().get(0).getPromoCode());
                    Utilities.ecommerceMeasureTransaction(productJsonObject, productActionJsonObject,
                            ScreenName, productActionJsonObject.getString("User Email"));
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                //ArrayList<AccountData> accountData = AccountController.getInstance().getmAccountDataList();
                /*double totalVal = Double.valueOf(accountData.get(0).getCartDetailList().get(0).getTotal());
                Log.e("totalVal", "" + totalVal);
                if (totalVal == 500.0 || totalVal > 500.0) {
                    Utilities.ladooIntegration(getActivity(), "sale_gt_500");
                }*/
                if (accountData.get(0).getCartDetailList().size() > 0) {
                    accountData.get(0).getCartDetailList().clear();
                }

                callCongratulationFragment();

            }
        }
    }

    private void callCongratulationFragment() {
        CongratulationFragment congratulationFragment = new CongratulationFragment();
        congratulationFragment.setOrderId(mOrderNumber);
        getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.mCheckoutContainer, congratulationFragment).commit();
    }

    @Override
    public void onRecyclerItemClick(View view, int position) {
        switch (view.getId()) {
            case R.id.mTextViewPaymentOption:
                onPaymentOptionSelected(position);
                break;
        }
    }

    public void successfulTransaction(JSONObject jsonObjectTxnSuccessful) {
        mProgressDialog.show();
        String paytmSuccessUrl = ApplicationURL.getPaytmTrxSuccessUrl + "?token=" + mLockatedPreferences.getLockatedToken();
        LockatedVolleyRequestQueue lockatedVolleyRequestQueue = LockatedVolleyRequestQueue.getInstance(getActivity());
        lockatedVolleyRequestQueue.sendRequest(PAYMENT_SUCCESS, Request.Method.POST, paytmSuccessUrl, jsonObjectTxnSuccessful, this, this);
    }

    private boolean verifyPromoCode(String enteredPromoCode) {
        if (paytmPromoCodeJsonArray != null && enteredPromoCode != null) {
            if (!foundCode) {
                for (int i = 0; i < paytmPromoCodeJsonArray.length(); i++) {
                    if (!foundCode) {
                        if (enteredPromoCode.equalsIgnoreCase(paytmPromoCodeJsonArray.optString(i))) {
                            foundCode = true;
                        } else {
                            foundCode = false;
                        }
                    }
                }
            }
        } else {
            foundCode = false;
        }
        return foundCode;
    }

    public void dismissProgress() {
        if (mProgressDialog != null && mProgressDialog.isShowing()) {
            mProgressDialog.dismiss();
        }
    }

}