
package com.android.lockated.model.AdminModel.Notice;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class PendingNotice {

    @SerializedName("noticeboards")
    @Expose
    private ArrayList<Noticeboard> noticeboards = new ArrayList<Noticeboard>();

    /**
     * 
     * @return
     *     The noticeboards
     */
    public ArrayList<Noticeboard> getNoticeboards() {
        return noticeboards;
    }

    /**
     * 
     * @param noticeboards
     *     The noticeboards
     */
    public void setNoticeboards(ArrayList<Noticeboard> noticeboards) {
        this.noticeboards = noticeboards;
    }

}
