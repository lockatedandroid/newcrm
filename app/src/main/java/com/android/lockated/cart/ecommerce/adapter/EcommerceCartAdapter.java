package com.android.lockated.cart.ecommerce.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.android.lockated.Interfaces.IRecyclerItemClickListener;
import com.android.lockated.R;
import com.android.lockated.holder.RecyclerViewHolder;
import com.android.lockated.model.LineItemData;

import java.util.ArrayList;

public class EcommerceCartAdapter extends RecyclerView.Adapter<RecyclerViewHolder>
{
    private ArrayList<LineItemData> mEcommerceDataList;
    private IRecyclerItemClickListener mIRecyclerItemClickListener;

    public EcommerceCartAdapter(ArrayList<LineItemData> data, IRecyclerItemClickListener listener)
    {
        mEcommerceDataList = data;
        mIRecyclerItemClickListener = listener;
    }

    @Override
    public RecyclerViewHolder onCreateViewHolder(ViewGroup parent, int viewType)
    {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.fragment_ecommerce_cart_item_row, parent, false);
        return new RecyclerViewHolder(itemView, mIRecyclerItemClickListener);
    }

    @Override
    public void onBindViewHolder(RecyclerViewHolder holder, int position)
    {
        holder.onEcommerceViewHolder(mEcommerceDataList.get(position));
    }

    @Override
    public int getItemCount()
    {
        return mEcommerceDataList.size();
    }
}