
package com.android.lockated.model.AdminModel.AdminClassified.PendingClassified;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class PendingClassified {

    @SerializedName("classifieds")
    @Expose
    private ArrayList<AdminClassified> classifieds = new ArrayList<AdminClassified>();

    /**
     * @return The classifieds
     */
    public ArrayList<AdminClassified> getClassifieds() {
        return classifieds;
    }

    /**
     * @param classifieds The classifieds
     */
    public void setClassifieds(ArrayList<AdminClassified> classifieds) {
        this.classifieds = classifieds;
    }

}
