package com.android.lockated.register;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.text.method.HideReturnsTransformationMethod;
import android.text.method.PasswordTransformationMethod;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.lockated.LockatedApplication;
import com.android.lockated.R;
import com.android.lockated.login.LoginFragment;
import com.android.lockated.network.ConnectionDetector;
import com.android.lockated.preferences.LockatedPreferences;
import com.android.lockated.request.LockatedRequestError;
import com.android.lockated.request.LockatedVolleyRequestQueue;
import com.android.lockated.utils.ApplicationURL;
import com.android.lockated.utils.Utilities;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;

import org.json.JSONException;
import org.json.JSONObject;

public class SignUpFragment extends Fragment implements View.OnClickListener, Response.Listener, Response.ErrorListener {
    private View mSignUpView;

    private TextView mButtonSignUp;
    private TextView mTextViewSignUp;
    private TextView mTextViewSignIn;
    private TextView mTextSignUpBar;
    private TextView mTextSignInBar;

    private TextView mTextViewEnterFirstName;
    private TextView mTextViewEnterLastName;
    private TextView mTextViewEnterEmail;
    private TextView mTextViewEnterPassword;
    private TextView mTextViewAlreadySignIn;
    private TextView mTextViewEnterMobileNumber;

    private EditText mEditTextEnterFirstName;
    private EditText mEditTextEnterLastName;
    private EditText mEditTextEnterEmail;
    private EditText mEditTextEnterPassword;
    private EditText mEditTextEnterMobileNumber;

    String strSignUpFirstName;
    String strSignUpLastName;
    String strSignUpEmail;
    String strSignUpPassword;
    String strSignUpNumber;

    private ImageView mImageViewEnterShowPassword;

    private boolean isPasswordVisible;

    private RequestQueue mQueue;
    private ProgressDialog mProgressDialog;
    private LockatedPreferences mLockatedPreferences;

    public static final String REQUEST_TAG = "SignUpFragment";

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mSignUpView = inflater.inflate(R.layout.fragment_signup, container, false);

        init();
        setSignUpTabBar();
        hideEditTextFirstNameHint();
        hideEditTextLastNameHint();
        hideEditTextEmailHint();
        hideEditTextNumberHint();
        hideEditTextPasswordHint();

        return mSignUpView;
    }

    @Override
    public void onResume() {
        super.onResume();
        LockatedApplication.getInstance().trackScreenView(getActivity().getString(R.string.signup_screen));
    }

    private void init() {
        mLockatedPreferences = new LockatedPreferences(getActivity());
        mTextViewSignUp = (TextView) getActivity().findViewById(R.id.mTextViewSignUp);
        mTextViewSignIn = (TextView) getActivity().findViewById(R.id.mTextViewSignIn);
        mTextSignUpBar = (TextView) getActivity().findViewById(R.id.mTextSignUpBar);
        mTextSignInBar = (TextView) getActivity().findViewById(R.id.mTextSignInBar);

        mTextViewEnterFirstName = (TextView) mSignUpView.findViewById(R.id.mTextViewEnterFirstName);
        mTextViewEnterLastName = (TextView) mSignUpView.findViewById(R.id.mTextViewEnterLastName);
        mTextViewEnterEmail = (TextView) mSignUpView.findViewById(R.id.mTextViewEnterEmail);
        mTextViewEnterPassword = (TextView) mSignUpView.findViewById(R.id.mTextViewEnterPassword);
        mTextViewAlreadySignIn = (TextView) mSignUpView.findViewById(R.id.mTextViewAlreadySignIn);
        mTextViewEnterMobileNumber = (TextView) mSignUpView.findViewById(R.id.mTextViewEnterMobileNumber);

        mButtonSignUp = (TextView) mSignUpView.findViewById(R.id.mButtonSignUp);
        mEditTextEnterFirstName = (EditText) mSignUpView.findViewById(R.id.mEditTextEnterFirstName);
        mEditTextEnterLastName = (EditText) mSignUpView.findViewById(R.id.mEditTextEnterLastName);
        mEditTextEnterEmail = (EditText) mSignUpView.findViewById(R.id.mEditTextEnterEmail);
        mEditTextEnterPassword = (EditText) mSignUpView.findViewById(R.id.mEditTextEnterPassword);
        mEditTextEnterMobileNumber = (EditText) mSignUpView.findViewById(R.id.mEditTextEnterMobileNumber);

        mImageViewEnterShowPassword = (ImageView) mSignUpView.findViewById(R.id.mImageViewEnterShowPassword);

        mButtonSignUp.setOnClickListener(this);
        mTextViewAlreadySignIn.setOnClickListener(this);
        mImageViewEnterShowPassword.setOnClickListener(this);

        setEditTextListener();
        hideShowPassword();
        disableSignUpButton();
    }

    private void setSignUpTabBar() {
        mTextViewSignIn.setTextColor(ContextCompat.getColor(getActivity(), R.color.primary_text));
        mTextViewSignUp.setTextColor(ContextCompat.getColor(getActivity(), R.color.primary));
        mTextSignUpBar.setBackgroundColor(ContextCompat.getColor(getActivity(), R.color.primary));
        mTextSignInBar.setBackgroundColor(ContextCompat.getColor(getActivity(), R.color.colorThemeAccent));
    }

    private void disableSignUpButton() {
        mButtonSignUp.setEnabled(false);
        mButtonSignUp.setTextColor(ContextCompat.getColor(getActivity(), R.color.secondary_text));
    }

    private void enableSignUpButton() {
        mButtonSignUp.setEnabled(true);
        mButtonSignUp.setTextColor(ContextCompat.getColor(getActivity(), R.color.primary));
    }

    private void hideShowPassword() {
        mImageViewEnterShowPassword.setVisibility(View.GONE);
    }

    private void unnHideShowPassword() {
        mImageViewEnterShowPassword.setVisibility(View.VISIBLE);
    }

    private void showEditTextFirstNameHint() {
        mTextViewEnterFirstName.setVisibility(View.VISIBLE);
    }

    private void showEditTextLastNameHint() {
        mTextViewEnterLastName.setVisibility(View.VISIBLE);
    }

    private void showEditTextEmailHint() {
        mTextViewEnterEmail.setVisibility(View.VISIBLE);
    }

    private void showEditTextNumberHint() {
        mTextViewEnterMobileNumber.setVisibility(View.VISIBLE);
    }

    private void showEditTextPasswordHint() {
        mTextViewEnterPassword.setVisibility(View.VISIBLE);
    }

    private void hideEditTextFirstNameHint() {
        mTextViewEnterFirstName.setVisibility(View.INVISIBLE);
    }

    private void hideEditTextLastNameHint() {
        mTextViewEnterLastName.setVisibility(View.INVISIBLE);
    }

    private void hideEditTextEmailHint() {
        mTextViewEnterEmail.setVisibility(View.INVISIBLE);
    }

    private void hideEditTextNumberHint() {
        mTextViewEnterMobileNumber.setVisibility(View.INVISIBLE);
    }

    private void hideEditTextPasswordHint() {
        mTextViewEnterPassword.setVisibility(View.INVISIBLE);
    }

    private void onSignInButtonClicked() {
        if (!getActivity().isFinishing()) {
            getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.container, new LoginFragment()).commitAllowingStateLoss();
        }
    }

    private void onShowPasswordClicked() {
        if (isPasswordVisible) {
            isPasswordVisible = false;
            mEditTextEnterPassword.setTransformationMethod(PasswordTransformationMethod.getInstance());
            mEditTextEnterPassword.setSelection(mEditTextEnterPassword.getText().length());
        } else {
            isPasswordVisible = true;
            mEditTextEnterPassword.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
            mEditTextEnterPassword.setSelection(mEditTextEnterPassword.getText().length());
        }
    }

    private void setEditTextListener() {
        mEditTextEnterFirstName.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.length() > 0) {
                    showEditTextFirstNameHint();

                    if (mEditTextEnterLastName.getText().toString().length() > 0 && mEditTextEnterEmail.getText().toString().length() > 0 && mEditTextEnterMobileNumber.getText().toString().length() > 0 && mEditTextEnterPassword.getText().toString().length() > 0) {
                        enableSignUpButton();
                    }
                } else {
                    disableSignUpButton();
                    hideEditTextFirstNameHint();
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        mEditTextEnterLastName.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.length() > 0) {
                    showEditTextLastNameHint();
                    if (mEditTextEnterFirstName.getText().toString().length() > 0 && mEditTextEnterEmail.getText().toString().length() > 0 && mEditTextEnterMobileNumber.getText().toString().length() > 0 && mEditTextEnterPassword.getText().toString().length() > 0) {
                        enableSignUpButton();
                    }

                } else {
                    disableSignUpButton();
                    hideEditTextLastNameHint();
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        mEditTextEnterEmail.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.length() > 0) {
                    showEditTextEmailHint();

                    if (mEditTextEnterFirstName.getText().toString().length() > 0 && mEditTextEnterLastName.getText().toString().length() > 0 && mEditTextEnterMobileNumber.getText().toString().length() > 0 && mEditTextEnterPassword.getText().toString().length() > 0) {
                        enableSignUpButton();
                    }
                } else {
                    disableSignUpButton();
                    hideEditTextEmailHint();
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        mEditTextEnterPassword.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.length() > 0) {
                    showEditTextPasswordHint();
                    unnHideShowPassword();
                    if (mEditTextEnterFirstName.getText().toString().length() > 0 && mEditTextEnterLastName.getText().toString().length() > 0 && mEditTextEnterMobileNumber.getText().toString().length() > 0 && mEditTextEnterEmail.getText().toString().length() > 0) {
                        enableSignUpButton();
                    }
                } else {
                    disableSignUpButton();
                    hideEditTextPasswordHint();
                    hideShowPassword();
                    if (isPasswordVisible) {
                        isPasswordVisible = false;
                        mEditTextEnterPassword.setTransformationMethod(PasswordTransformationMethod.getInstance());
                        mEditTextEnterPassword.setSelection(mEditTextEnterPassword.getText().length());
                    }
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        mEditTextEnterMobileNumber.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.length() > 0) {
                    showEditTextNumberHint();
                    if (mEditTextEnterFirstName.getText().toString().length() > 0 && mEditTextEnterLastName.getText().toString().length() > 0 && mEditTextEnterPassword.getText().toString().length() > 0 && mEditTextEnterEmail.getText().toString().length() > 0) {
                        enableSignUpButton();
                    }
                } else {
                    disableSignUpButton();
                    hideEditTextNumberHint();
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }

    private void onSignUpButtonClicked() {
        strSignUpFirstName = mEditTextEnterFirstName.getText().toString();
        strSignUpLastName = mEditTextEnterLastName.getText().toString();
        strSignUpEmail = mEditTextEnterEmail.getText().toString();
        strSignUpPassword = mEditTextEnterPassword.getText().toString();
        strSignUpNumber = mEditTextEnterMobileNumber.getText().toString();

        boolean signUpCancel = false;
        View signUpFocusView = null;
        String message = getActivity().getResources().getString(R.string.signup_blank_field_error);

        // Validation for Name field, it should not be blank
        if (TextUtils.isEmpty(strSignUpFirstName)) {
            signUpFocusView = mEditTextEnterFirstName;
            signUpCancel = true;
            message = getActivity().getResources().getString(R.string.signup_blank_field_error);
        }

        if (TextUtils.isEmpty(strSignUpLastName)) {
            signUpFocusView = mEditTextEnterLastName;
            signUpCancel = true;
            message = getActivity().getResources().getString(R.string.signup_blank_field_error);
        }

        // Validation for Email field, it should not be blank
        if (TextUtils.isEmpty(strSignUpEmail)) {
            signUpFocusView = mEditTextEnterEmail;
            signUpCancel = true;
            message = getActivity().getResources().getString(R.string.signup_blank_field_error);
        }

        if (!Utilities.isValidEmail(strSignUpEmail)) {
            signUpFocusView = mEditTextEnterEmail;
            signUpCancel = true;
            message = getActivity().getResources().getString(R.string.email_blank_field_error);
        }

        // Validation for Number field, it should not be blank
        if (TextUtils.isEmpty(strSignUpNumber)) {
            signUpFocusView = mEditTextEnterMobileNumber;
            signUpCancel = true;
            message = getActivity().getResources().getString(R.string.signup_blank_field_error);
        }

        if (strSignUpNumber.length() != 10) {
            signUpFocusView = mEditTextEnterMobileNumber;
            signUpCancel = true;
            message = getActivity().getResources().getString(R.string.mobile_number_error);
        }

        // Validation for Password field, it should not be blank
        if (TextUtils.isEmpty(strSignUpPassword)) {
            signUpFocusView = mEditTextEnterPassword;
            signUpCancel = true;
            message = getActivity().getResources().getString(R.string.signup_blank_field_error);
        }

        if (signUpCancel) {
            signUpFocusView.requestFocus();
            Utilities.showToastMessage(getActivity(), message);
        } else {
            if (ConnectionDetector.isConnectedToInternet(getActivity())) {
                mProgressDialog = ProgressDialog.show(getActivity(), "", "Please Wait...", false);
                mProgressDialog.show();

                JSONObject jsonObject = new JSONObject();
                JSONObject jsonObjectFields = new JSONObject();
                try {
                    jsonObjectFields.put("email", strSignUpEmail);
                    jsonObjectFields.put("password", strSignUpPassword);
                    jsonObjectFields.put("firstname", strSignUpFirstName);
                    jsonObjectFields.put("lastname", strSignUpLastName);
                    jsonObjectFields.put("mobile", strSignUpNumber);

                    jsonObject.put("user", jsonObjectFields);
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                LockatedVolleyRequestQueue lockatedVolleyRequestQueue = LockatedVolleyRequestQueue.getInstance(getActivity());
                lockatedVolleyRequestQueue.sendRequest(REQUEST_TAG, Request.Method.POST, ApplicationURL.signUpUrl, jsonObject, this, this);

                Utilities.lockatedGoogleAnalytics(getString(R.string.sign_up), getString(R.string.sign_up), getString(R.string.sign_up));
            } else {
                Utilities.showToastMessage(getActivity(), getActivity().getResources().getString(R.string.internet_connection_error));
            }
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.mButtonSignUp:
                onSignUpButtonClicked();
                break;

            case R.id.mTextViewAlreadySignIn:
                onSignInButtonClicked();
                break;

            case R.id.mImageViewEnterShowPassword:
                onShowPasswordClicked();
                break;
        }
    }

    @Override
    public void onErrorResponse(VolleyError error) {
        mProgressDialog.dismiss();
        if (getActivity() != null) {
            LockatedRequestError.onRequestError(getActivity(), error);
        }
    }

    @Override
    public void onResponse(Object response) {
        mProgressDialog.dismiss();
        if (getActivity() != null) {
            JSONObject jsonObject = (JSONObject) response;
            try {
                int code = jsonObject.optInt("code");
                if (code == 200) {
                    String token = jsonObject.getString("spree_api_key");
                    String number = jsonObject.getString("mobile");
                    String userId = jsonObject.getString("id");
                    mLockatedPreferences.setLockatedUserId(userId);
                    mLockatedPreferences.setLockatedToken(token);
                    mLockatedPreferences.setContactNumber(number);

                    if (getActivity() != null) {
                        getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.container, new OTPGeneratorFragment()).commitAllowingStateLoss();
                    }
                } else {
                    Utilities.showToastMessage(getActivity(), jsonObject.getString("error"));
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }
}
