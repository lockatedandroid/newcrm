package com.android.lockated.crmadmin.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.android.lockated.R;
import com.android.lockated.network.ConnectionDetector;
import com.android.lockated.preferences.LockatedPreferences;
import com.android.lockated.request.LockatedJSONObjectRequest;
import com.android.lockated.request.LockatedRequestError;
import com.android.lockated.request.LockatedVolleyRequestQueue;
import com.android.lockated.utils.ApplicationURL;
import com.android.lockated.utils.ShowImage;
import com.android.lockated.utils.Utilities;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

public class AdminNoticeDetailActivity extends AppCompatActivity implements View.OnClickListener, Response.ErrorListener, Response.Listener<JSONObject> {

    String noticeid;
    String strId;
    TextView flat;
    String visibilityValue;
    private TextView mDescription;
    private TextView mClose;
    private TextView mPublish;
    private TextView mDeny;
    private TextView mEventType;
    private TextView mSubject;
    private TextView mCreatedBY;
    private TextView mEndDateTime;
    private TextView mTextAttachment;
    private ImageView mImageAttachment;
    String strdocument;
    ProgressBar mProgressBarView;
    private RequestQueue mQueue;
    private LockatedPreferences mLockatedPreferences;
    public static final String REQUEST_TAG = "AdminNoticeDetailActivity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_admin_notice_detail);

        init();
        setToolBarTitle();
        displayData();

    }

    private void init() {
        mLockatedPreferences = new LockatedPreferences(getApplicationContext());
        mProgressBarView = (ProgressBar) findViewById(R.id.mProgressBarView);
        mEventType = (TextView) findViewById(R.id.mEventType);
        mSubject = (TextView) findViewById(R.id.mSubject);
        flat = (TextView) findViewById(R.id.mflat);
        mCreatedBY = (TextView) findViewById(R.id.mCreatedBY);
        mEndDateTime = (TextView) findViewById(R.id.mEndDateTime);
        mDescription = (TextView) findViewById(R.id.mDescription);
        mClose = (TextView) findViewById(R.id.mClose);
        mTextAttachment = (TextView) findViewById(R.id.mTextAttachment);
        mPublish = (TextView) findViewById(R.id.mPublish);
        mDeny = (TextView) findViewById(R.id.mDeny);
        mImageAttachment = (ImageView) findViewById(R.id.mImageAttachment);
        mPublish.setOnClickListener(this);
        mDeny.setOnClickListener(this);
        mClose.setOnClickListener(this);
        mImageAttachment.setOnClickListener(this);
    }

    public void setToolBarTitle() {

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(true);
        getSupportActionBar().setTitle(R.string.notice);
    }

    private void displayData() {
        if (mLockatedPreferences.getEvent_id() != null) {
            if (!mLockatedPreferences.getNotice_id().equals("blank")) {
                getPendingNotices(mLockatedPreferences.getNotice_id());
            } else {
                try {
                    if (getIntent().getExtras() != null) {
                        JSONObject jsonObjectDetail = new JSONObject(getIntent().getExtras().getString("NoticeDetailViewData"));
                        //    mEventType.setText(jsonObjectDetail.getString("event_type"));
                        mDescription.setText(jsonObjectDetail.getString("notice_text"));
                        mCreatedBY.setText(jsonObjectDetail.getString("username"));
                        mSubject.setText(jsonObjectDetail.getString("notice_heading"));
                        noticeid = (jsonObjectDetail.getString("id"));
                        flat.setText((jsonObjectDetail.getString("flat")));
                        mEndDateTime.setText(jsonObjectDetail.getString("expire_time"));
                        String VisiblityValue = jsonObjectDetail.getString("VisiblityValue");
                        if (VisiblityValue.equals("1")) {
                            mPublish.setVisibility(View.GONE);
                        } else {
                            mPublish.setVisibility(View.VISIBLE);
                        }
                        if (!jsonObjectDetail.getString("document").equals("No Document")) {
                            strdocument = jsonObjectDetail.getString("document");
                            Picasso.with(this).load("" + strdocument).fit().placeholder(R.drawable.loading).into(mImageAttachment);
                        } else {
                            mTextAttachment.setVisibility(View.VISIBLE);
                            mImageAttachment.setVisibility(View.GONE);
                        }
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.mClose:
                onCallIntent();
                break;
            case R.id.mPublish:
                onPublishClicked();
                break;
            case R.id.mDeny:
                onDenyClicked();
                break;
            case R.id.mImageAttachment:
                Intent intent = new Intent(getApplicationContext(), ShowImage.class);
                intent.putExtra("imageUrlString", strdocument);
                startActivity(intent);
                break;
        }
    }

    public void getPendingNotices(String pid) {
        if (ConnectionDetector.isConnectedToInternet(getApplicationContext())) {
            mProgressBarView.setVisibility(View.VISIBLE);
            mQueue = LockatedVolleyRequestQueue.getInstance(getApplicationContext()).getRequestQueue();
            String url = "https://www.lockated.com/noticeboards/" + pid + ".json?token=" + mLockatedPreferences.getLockatedToken()
                    + "&id_society=" + mLockatedPreferences.getSocietyId();
            LockatedJSONObjectRequest lockatedJSONObjectRequest = new LockatedJSONObjectRequest(Request.Method.GET,
                    url, null, this, this);
            lockatedJSONObjectRequest.setTag(REQUEST_TAG);
            mQueue.add(lockatedJSONObjectRequest);
        } else {
            Utilities.showToastMessage(getApplicationContext(), getApplicationContext().getResources().getString(R.string.internet_connection_error));
        }

    }

    private void onDenyClicked() {
        request(null, ApplicationURL.getRejectNoticeUrl + noticeid);
    }

    private void onPublishClicked() {
        request(null, ApplicationURL.getPublishNoticeUrl + noticeid);
    }

    public void request(JSONObject jsonObject, String url) {
        mProgressBarView.setVisibility(View.VISIBLE);
        if (ConnectionDetector.isConnectedToInternet(this)) {
            /*mLockatedPreferences = new LockatedPreferences(this);*/
            mQueue = LockatedVolleyRequestQueue.getInstance(this).getRequestQueue();
            LockatedJSONObjectRequest lockatedJSONObjectRequest = new LockatedJSONObjectRequest(Request.Method.GET,
                    url + "&token=" + mLockatedPreferences.getLockatedToken()
                            + "&id_society=" + mLockatedPreferences.getSocietyId(), jsonObject, this, this);
            lockatedJSONObjectRequest.setTag(REQUEST_TAG);
            mQueue.add(lockatedJSONObjectRequest);
        } else {
            Utilities.showToastMessage(this, this.getResources().getString(R.string.internet_connection_error));
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();
        if (id == android.R.id.home) {
            finish();
            return true;
        } else {
            return false;
        }
    }

    @Override
    public void onErrorResponse(VolleyError error) {
        mProgressBarView.setVisibility(View.GONE);
        LockatedRequestError.onRequestError(this, error);
    }

    @Override
    public void onResponse(JSONObject response) {
        mProgressBarView.setVisibility(View.GONE);
        if (response != null && response.length() > 0) {
            try {
                JSONObject jsonObjectDetail = response;
                if (response.has("id") && !mLockatedPreferences.getNotice_id().equals("blank")) {
                    mDescription.setText(jsonObjectDetail.getString("notice_text"));
                    mSubject.setText(jsonObjectDetail.getString("notice_heading"));
                    mEndDateTime.setText(jsonObjectDetail.getString("expire_time"));
                    mCreatedBY.setText(jsonObjectDetail.getJSONObject("user").getString("firstname") + " " + jsonObjectDetail.getJSONObject("user").getString("firstname"));
                    noticeid = (jsonObjectDetail.getString("id"));
                    flat.setText((jsonObjectDetail.getJSONObject("user_flat").getString("flat")));
                    //String VisiblityValue = jsonObjectDetail.getString("VisiblityValue");
                   /* if (visibilityValue != null && visibilityValue.equals("1")) {
                        mPublish.setVisibility(View.GONE);
                    } else {
                        mPublish.setVisibility(View.VISIBLE);
                    }*/
                    if (jsonObjectDetail.getJSONArray("documents").length() > 0) {
                        strdocument = jsonObjectDetail.getJSONArray("documents").getJSONObject(0).getString("document");
                        Picasso.with(this).load("" + strdocument).fit().placeholder(R.drawable.loading).into(mImageAttachment);
                    } else {
                        mTextAttachment.setVisibility(View.VISIBLE);
                        mImageAttachment.setVisibility(View.GONE);
                    }
                    mLockatedPreferences.setNotice_id("blank");
                } else if (response.has("id") && mLockatedPreferences.getNotice_id().equals("blank")) {
                    onCallIntent();
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

    }

    private void onCallIntent() {
        Intent intent = new Intent(getApplicationContext(), AdminNoticeBoardActivity.class);
        intent.putExtra("AdminNotice", 0);
        startActivity(intent);
        finish();
    }
}
