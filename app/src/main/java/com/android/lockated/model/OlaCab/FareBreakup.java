
package com.android.lockated.model.OlaCab;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class FareBreakup {

    @SerializedName("type")
    @Expose
    private String type;
    @SerializedName("minimum_distance")
    @Expose
    private String minimumDistance;
    @SerializedName("minimum_time")
    @Expose
    private String minimumTime;
    @SerializedName("base_fare")
    @Expose
    private String baseFare;
    @SerializedName("minimum_fare")
    @Expose
    private String minimumFare;
    @SerializedName("cost_per_distance")
    @Expose
    private String costPerDistance;
    @SerializedName("waiting_cost_per_minute")
    @Expose
    private String waitingCostPerMinute;
    @SerializedName("ride_cost_per_minute")
    @Expose
    private String rideCostPerMinute;
    @SerializedName("surcharge")
    @Expose
    private ArrayList<Surcharge> surcharge = new ArrayList<Surcharge>();

    @SerializedName("convenience_charge")
    @Expose
    private String convenienceCharge;
    @SerializedName("night_time_charges")
    @Expose
    private String nightTimeCharges;
    @SerializedName("night_time_duration")
    @Expose
    private String nightTimeDuration;

    /**
     *
     * @return
     *     The convenienceCharge
     */
    public String getConvenienceCharge() {
        return convenienceCharge;
    }

    /**
     *
     * @param convenienceCharge
     *     The convenience_charge
     */
    public void setConvenienceCharge(String convenienceCharge) {
        this.convenienceCharge = convenienceCharge;
    }

    /**
     *
     * @return
     *     The nightTimeCharges
     */
    public String getNightTimeCharges() {
        return nightTimeCharges;
    }

    /**
     *
     * @param nightTimeCharges
     *     The night_time_charges
     */
    public void setNightTimeCharges(String nightTimeCharges) {
        this.nightTimeCharges = nightTimeCharges;
    }

    /**
     *
     * @return
     *     The nightTimeDuration
     */
    public String getNightTimeDuration() {
        return nightTimeDuration;
    }

    /**
     *
     * @param nightTimeDuration
     *     The night_time_duration
     */
    public void setNightTimeDuration(String nightTimeDuration) {
        this.nightTimeDuration = nightTimeDuration;
    }

    /**
     * 
     * @return
     *     The type
     */
    public String getType() {
        return type;
    }

    /**
     * 
     * @param type
     *     The type
     */
    public void setType(String type) {
        this.type = type;
    }

    /**
     * 
     * @return
     *     The minimumDistance
     */
    public String getMinimumDistance() {
        return minimumDistance;
    }

    /**
     * 
     * @param minimumDistance
     *     The minimum_distance
     */
    public void setMinimumDistance(String minimumDistance) {
        this.minimumDistance = minimumDistance;
    }

    /**
     * 
     * @return
     *     The minimumTime
     */
    public String getMinimumTime() {
        return minimumTime;
    }

    /**
     * 
     * @param minimumTime
     *     The minimum_time
     */
    public void setMinimumTime(String minimumTime) {
        this.minimumTime = minimumTime;
    }

    /**
     * 
     * @return
     *     The baseFare
     */
    public String getBaseFare() {
        return baseFare;
    }

    /**
     * 
     * @param baseFare
     *     The base_fare
     */
    public void setBaseFare(String baseFare) {
        this.baseFare = baseFare;
    }

    /**
     * 
     * @return
     *     The minimumFare
     */
    public String getMinimumFare() {
        return minimumFare;
    }

    /**
     * 
     * @param minimumFare
     *     The minimum_fare
     */
    public void setMinimumFare(String minimumFare) {
        this.minimumFare = minimumFare;
    }

    /**
     * 
     * @return
     *     The costPerDistance
     */
    public String getCostPerDistance() {
        return costPerDistance;
    }

    /**
     * 
     * @param costPerDistance
     *     The cost_per_distance
     */
    public void setCostPerDistance(String costPerDistance) {
        this.costPerDistance = costPerDistance;
    }

    /**
     * 
     * @return
     *     The waitingCostPerMinute
     */
    public String getWaitingCostPerMinute() {
        return waitingCostPerMinute;
    }

    /**
     * 
     * @param waitingCostPerMinute
     *     The waiting_cost_per_minute
     */
    public void setWaitingCostPerMinute(String waitingCostPerMinute) {
        this.waitingCostPerMinute = waitingCostPerMinute;
    }

    /**
     * 
     * @return
     *     The rideCostPerMinute
     */
    public String getRideCostPerMinute() {
        return rideCostPerMinute;
    }

    /**
     * 
     * @param rideCostPerMinute
     *     The ride_cost_per_minute
     */
    public void setRideCostPerMinute(String rideCostPerMinute) {
        this.rideCostPerMinute = rideCostPerMinute;
    }

    /**
     * 
     * @return
     *     The surcharge
     */
    public ArrayList<Surcharge> getSurcharge() {
        return surcharge;
    }

    /**
     * 
     * @param surcharge
     *     The surcharge
     */
    public void setSurcharge(ArrayList<Surcharge> surcharge) {
        this.surcharge = surcharge;
    }

}
