package com.android.lockated.model;

import org.json.JSONObject;

public class OrderDetailData
{
    public int id;
    public int order_id;
    public String preferred_date;
    public String preferred_time;

    public OrderDetailData(JSONObject jsonObject)
    {
        id = jsonObject.optInt("id");
        order_id = jsonObject.optInt("order_id");
        preferred_date = jsonObject.optString("preferred_date");
        preferred_time = jsonObject.optString("preferred_time");
    }

    public int getId()
    {
        return id;
    }

    public int getOrder_id()
    {
        return order_id;
    }

    public String getPreferred_date()
    {
        return preferred_date;
    }

    public String getPreferred_time()
    {
        return preferred_time;
    }
}
