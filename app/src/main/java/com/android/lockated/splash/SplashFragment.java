package com.android.lockated.splash;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.android.lockated.HomeActivity;
import com.android.lockated.IndexActivity;
import com.android.lockated.LockatedApplication;
import com.android.lockated.R;
import com.android.lockated.information.AccountController;
import com.android.lockated.login.LoginFragment;
import com.android.lockated.model.BaseApi.BaseAccountApi;
import com.android.lockated.network.ConnectionDetector;
import com.android.lockated.preferences.LockatedPreferences;
import com.android.lockated.register.OTPGeneratorFragment;
import com.android.lockated.request.LockatedRequestError;
import com.android.lockated.request.LockatedVolleyRequestQueue;
import com.android.lockated.utils.ApplicationURL;
import com.android.lockated.utils.Utilities;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;

import org.json.JSONException;
import org.json.JSONObject;

public class SplashFragment extends Fragment implements Response.Listener, Response.ErrorListener, View.OnClickListener {
    private TextView mButtonHomeRetry;
    private AccountController mAccountController;

    private LockatedPreferences mLockatedPreferences;

    private static final int SPLASH_DISPLAY_LENGTH = 1000;
    public static final String REQUEST_TAG = "SplashFragment";

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View splashView = inflater.inflate(R.layout.fragment_splash, container, false);
        init(splashView);

        return splashView;
    }

    @Override
    public void onResume() {
        super.onResume();
        LockatedApplication.getInstance().trackScreenView(getActivity().getString(R.string.splash_screen));
    }

    private void init(View splashView) {
        mAccountController = AccountController.getInstance();
        mLockatedPreferences = new LockatedPreferences(getActivity());

        mButtonHomeRetry = (TextView) splashView.findViewById(R.id.mButtonHomeRetry);
        mButtonHomeRetry.setVisibility(View.GONE);

        mButtonHomeRetry.setOnClickListener(this);

        Log.e("Lockated Token", "" + mLockatedPreferences.getLockatedToken());
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                if (mLockatedPreferences.getLockatedToken().equals("")) {
                    IndexActivity.mIndex_layout.setVisibility(View.VISIBLE);
                    openLoginFragment();
                } else {
                    Log.e("GetScreenDetail", "getting");
                    // getScreenDetail();
                    getScreenDetailBaseApi();
                }

            }
        }, SPLASH_DISPLAY_LENGTH);
    }

    private void getScreenDetailBaseApi() {
        if (getActivity() != null) {
            if (ConnectionDetector.isConnectedToInternet(getActivity())) {
                //getScreenUrl
                String url = ApplicationURL.getBaseAccountApi + mLockatedPreferences.getLockatedToken();
                Log.e("url", "" + url);
                LockatedVolleyRequestQueue lockatedVolleyRequestQueue = LockatedVolleyRequestQueue.getInstance(getActivity());
                lockatedVolleyRequestQueue.sendRequest(REQUEST_TAG, Request.Method.GET, url, null, this, this);
            } else {
                Utilities.showToastMessage(getActivity(), getActivity().getResources().getString(R.string.internet_connection_error));
                mButtonHomeRetry.setVisibility(View.VISIBLE);
            }
        }
    }

    private void openLoginFragment() {
        if (getActivity() != null) {
            if (mLockatedPreferences.getLockatedToken().equals("")) {
                getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.container, new LoginFragment()).commitAllowingStateLoss();
            } else {
                openHomeActivity();
            }
        }
    }

    private void openOTPFragment() {
        if (getActivity() != null) {
            getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.container, new OTPGeneratorFragment()).commitAllowingStateLoss();
        }
    }

    private void openHomeActivity() {
        Intent intent = new Intent(getActivity(), HomeActivity.class);
        startActivity(intent);
        getActivity().finish();
    }

    @Override
    public void onErrorResponse(VolleyError error) {
        if (getActivity() != null) {
            mButtonHomeRetry.setVisibility(View.VISIBLE);
            LockatedRequestError.onRequestError(getActivity(), error);
        }
    }

    @Override
    public void onResponse(Object response) {
        if (getActivity() != null) {
            mButtonHomeRetry.setVisibility(View.GONE);
            JSONObject jsonObject = (JSONObject) response;
            if (jsonObject.has("number_verified")) {
                Log.e("splash", "Login" + response);
                String token = null;
                try {
                    token = jsonObject.getString("spree_api_key");
                    String number = jsonObject.getString("mobile");
                    int numberVerified = jsonObject.getInt("number_verified");
                    mLockatedPreferences.setLockatedToken(token);
                    mLockatedPreferences.setSocietyId(jsonObject.getJSONObject("society").getString("id"));
                    mLockatedPreferences.setUserSocietyId(jsonObject.getInt("selected_user_society"));
                    mLockatedPreferences.setContactNumber(jsonObject.getString("mobile"));
                    mLockatedPreferences.setImageAvatar(jsonObject.getJSONObject("avatar").getString("medium"));

                    BaseAccountApi basedata = new BaseAccountApi(jsonObject);
                    mAccountController = AccountController.getInstance();
                    mAccountController.mBaseAccountsApi.add(basedata);
                    /*mAccountController.getBaseAccountsData().get(0).getMobile();*/

                    //------------------Ola Changes-------------------------
                    Log.e("Ola Token", "Before set " + mLockatedPreferences.getOlaToken());
                    mLockatedPreferences.setOlaToken(mAccountController.mBaseAccountsApi.get(0).getOlatoken());
                    Log.e("Ola Token", "After set " + mLockatedPreferences.getOlaToken());
                    //-----------------------------------------------------------

                    //------------------Zomato Changes -----------------------------
                    Log.e("Zomato Token", "Before set " + mLockatedPreferences.getZomatoToken());
                    mLockatedPreferences.setZomatoToken(mAccountController.mBaseAccountsApi.get(0).getZomatotoken());
                    Log.e("Zomato Token", "After set " + mLockatedPreferences.getZomatoToken());
                    //-----------------------------------------------------------------

                    mLockatedPreferences.setNotificationValue(false);
                    Log.e("numberVerified resp", "" + numberVerified);
                    if (numberVerified == 0) {
                        openOTPFragment();
                    } else {
                        Log.e("openfragment", "openLoginFragment");
                        openLoginFragment();
                    }


                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.mButtonHomeRetry:
                onRetryClicked();
                break;
        }
    }

    private void onRetryClicked() {
        mButtonHomeRetry.setVisibility(View.GONE);
        //getScreenDetail();
        getScreenDetailBaseApi();
    }

}
