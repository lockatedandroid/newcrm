package com.android.lockated.crm.fragment.polls;

import android.app.Dialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.android.lockated.R;
import com.android.lockated.preferences.LockatedPreferences;
import com.android.lockated.utils.Utilities;
import com.android.volley.RequestQueue;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class PollsNoticePreview extends DialogFragment implements View.OnClickListener {
    private View PollsNoticePreviewView;
    private TextView mSubject;
    private TextView mOPtion1, mOPtion2, mOPtion3, mOPtion4;
    private TextView mStartDate;
    private TextView mPollingOptions;
    private TextView mEndtDate;
    private RequestQueue mQueue;
    private TextView mProceed;
    private TextView mEdit;
    Window window;
    private ProgressBar mProgressBar;
    private LockatedPreferences mLockatedPreferences;
    public static final String REQUEST_TAG = "PollsNoticePreview";

    @Override
    public void onStart() {
        super.onStart();
        window = getDialog().getWindow();
        WindowManager.LayoutParams windowParams = window.getAttributes();
        windowParams.dimAmount = 0.75f;
        windowParams.flags |= WindowManager.LayoutParams.FLAG_DIM_BEHIND;
        window.setAttributes(windowParams);
        window.setBackgroundDrawableResource(android.R.color.white);
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        PollsNoticePreviewView = getActivity().getLayoutInflater().inflate(R.layout.fragment_polls_notice_preview,
                new LinearLayout(getActivity()), false);

        init(PollsNoticePreviewView);
        Dialog builder = new Dialog(getActivity());
        builder.requestWindowFeature(Window.FEATURE_NO_TITLE);
        builder.getWindow().setBackgroundDrawable(new ColorDrawable(Color.WHITE));
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(builder.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.WRAP_CONTENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        builder.show();
        builder.getWindow().setAttributes(lp);
        builder.setContentView(PollsNoticePreviewView);
        return builder;
    }

    private void init(View PollsNoticePreviewView) {

        mLockatedPreferences = new LockatedPreferences(getActivity());
        mProgressBar = (ProgressBar) PollsNoticePreviewView.findViewById(R.id.mProgressBarSmallView);
        mSubject = (TextView) PollsNoticePreviewView.findViewById(R.id.mSubject);
        mOPtion1 = (TextView) PollsNoticePreviewView.findViewById(R.id.mOption1);
        mOPtion2 = (TextView) PollsNoticePreviewView.findViewById(R.id.mOption2);
        mOPtion3 = (TextView) PollsNoticePreviewView.findViewById(R.id.mOption3);
        mOPtion4 = (TextView) PollsNoticePreviewView.findViewById(R.id.mOption4);
        mPollingOptions = (TextView) PollsNoticePreviewView.findViewById(R.id.mPollingOptions);
        mStartDate = (TextView) PollsNoticePreviewView.findViewById(R.id.mStartDate);
        mEndtDate = (TextView) PollsNoticePreviewView.findViewById(R.id.mEndDate);
        mEdit = (TextView) PollsNoticePreviewView.findViewById(R.id.mEdit);
        //mProceed = (TextView) PollsNoticePreviewView.findViewById(R.id.mProceed);
        if (getArguments() != null) {
            if (getArguments().getString("duration") != null && getArguments().getString("duration").equals("0")) {
                mPollingOptions.setText("No");
            } else {
                mPollingOptions.setText("Yes");
            }
            mSubject.setText(getArguments().getString("subject"));
            mOPtion1.setText("1." + getArguments().getString("option1"));
            mOPtion2.setText("2." + getArguments().getString("option2"));

            if (!getArguments().getString("option3").equals("blank") && !getArguments().getString("option4").equals("blank")) {
                mOPtion3.setText("3." + getArguments().getString("option3"));
                mOPtion4.setText("4." + getArguments().getString("option4"));
            } else if (!getArguments().getString("option3").equals("blank") && getArguments().getString("option4").equals("blank")) {
                mOPtion3.setText("3." + getArguments().getString("option3"));
                mOPtion4.setVisibility(View.GONE);
            } else if (getArguments().getString("option3").equals("blank") && !getArguments().getString("option4").equals("blank")) {
                mOPtion4.setText("3." + getArguments().getString("option4"));
                mOPtion3.setVisibility(View.GONE);
            } else {
                mOPtion4.setVisibility(View.GONE);
                mOPtion3.setVisibility(View.GONE);
            }
            //mPollingOptions.setText(getArguments().getString("duration"));
            mStartDate.setText(checkIfToday(getArguments().getString("startdate")));
            mEndtDate.setText(checkIfToday(getArguments().getString("enddate")));
        }
        //mProceed.setOnClickListener(this);
        mEdit.setOnClickListener(this);

    }

    public String checkIfToday(String todayOrNot) {

        if (todayOrNot != null) {
            try {
                Calendar c = Calendar.getInstance();
                SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
                String formattedDate = df.format(c.getTime());
                Date currentDate = df.parse(formattedDate);
                Date respDate = df.parse(todayOrNot);

                /*if (currentDate.equals(respDate)) {
                    todayOrNot = todayOrNot.substring(todayOrNot.indexOf("T") + 1, todayOrNot.length());
                    Log.e("if", ""+todayOrNot);
                } else {*/
                todayOrNot = Utilities.ddMonYYYYFormat(todayOrNot);
                //}
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }

        return todayOrNot;

    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            /*case R.id.mProceed:
            this.dismiss();
                break;*/
            case R.id.mEdit:
                this.dismiss();
                break;
        }
    }
}
