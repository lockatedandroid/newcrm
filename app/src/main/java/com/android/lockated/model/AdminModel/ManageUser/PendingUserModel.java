package com.android.lockated.model.AdminModel.ManageUser;




import java.util.ArrayList;
import java.util.List;

public class PendingUserModel {


    private List<UserSociety> userSocieties = new ArrayList<UserSociety>();

    /**
     *
     * @return
     * The userSocieties
     */
    public List<UserSociety> getUserSocieties() {
        return userSocieties;
    }

    /**
     *
     * @param userSocieties
     * The user_societies
     */
    public void setUserSocieties(List<UserSociety> userSocieties) {
        this.userSocieties = userSocieties;
    }

}
