package com.android.lockated.crmadmin.fragment;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.android.lockated.LockatedApplication;
import com.android.lockated.R;
import com.android.lockated.crmadmin.activity.AdminActivity;
import com.android.lockated.crmadmin.detailView.PublishedHelpDeskActivity;
import com.android.lockated.information.AccountController;
import com.android.lockated.model.AccountData;
import com.android.lockated.network.ConnectionDetector;
import com.android.lockated.preferences.LockatedPreferences;
import com.android.lockated.request.LockatedJSONObjectRequest;
import com.android.lockated.request.LockatedRequestError;
import com.android.lockated.request.LockatedVolleyRequestQueue;
import com.android.lockated.utils.ApplicationURL;
import com.android.lockated.utils.Utilities;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Locale;

public class ApprovedComplaintsFragment extends Fragment implements Response.ErrorListener, Response.Listener<JSONObject> {

    public static final String REQUEST_TAG = "ApprovedComplaintsFragment";
    RecyclerView recyclerView;
    ProgressBar progressBar;
    ProgressDialog mProgressDialog;
    PublihedHelpDeskAdapter publihedHelpDeskAdapter;
    private TextView errorMsg;
    private String pid;
    private String formattedDate;
    private RequestQueue mQueue;
    private LockatedPreferences mLockatedPreferences;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View pendingClassified = inflater.inflate(R.layout.fragment_published_help_desk, container, false);
        init(pendingClassified);
        Utilities.ladooIntegration(getActivity(), "Admin Published Complaints");
        return pendingClassified;
    }

    @Override
    public void onResume() {
        super.onResume();
        LockatedApplication.getInstance().trackScreenView(getString(R.string.adminPublishedHelpDesk));
        Utilities.lockatedGoogleAnalytics(getString(R.string.adminPublishedHelpDesk),
                getString(R.string.visited), getString(R.string.adminPublishedHelpDesk));
        getPublishedHelpDesk();
    }

    private void init(View view) {
        mLockatedPreferences = new LockatedPreferences(getActivity());
        progressBar = (ProgressBar) view.findViewById(R.id.mProgressBarView);
        recyclerView = (RecyclerView) view.findViewById(R.id.recyclerView);
        errorMsg = (TextView) view.findViewById(R.id.noNotices);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity(),
                LinearLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(linearLayoutManager);
    }

    public void getPublishedHelpDesk() {
        mProgressDialog = ProgressDialog.show(getActivity(), "", "Please Wait...", false);
        mProgressDialog.show();
        if (getActivity() != null) {
            if (!mLockatedPreferences.getSocietyId().equals("blank")) {
                if (ConnectionDetector.isConnectedToInternet(getActivity())) {
                    progressBar.setVisibility(View.VISIBLE);
                    mQueue = LockatedVolleyRequestQueue.getInstance(getActivity()).getRequestQueue();
                    LockatedJSONObjectRequest lockatedJSONObjectRequest = new LockatedJSONObjectRequest(Request.Method.GET,
                            ApplicationURL.getAdminPendingHelpDeskdUrl + mLockatedPreferences.getLockatedToken(), null, this, this);
                    lockatedJSONObjectRequest.setTag(REQUEST_TAG);
                    mQueue.add(lockatedJSONObjectRequest);
                } else {
                    Utilities.showToastMessage(getActivity(), getActivity().getResources().getString(R.string.internet_connection_error));
                }
            } else {
                errorMsg.setVisibility(View.VISIBLE);
                errorMsg.setText("You are not associated with any society");
            }
        }
    }

    @Override
    public void onErrorResponse(VolleyError error) {
        mProgressDialog.dismiss();
        if (getActivity() != null) {
            /*progressBar.setVisibility(View.GONE);*/
            LockatedRequestError.onRequestError(getActivity(), error);
        }
    }

    @Override
    public void onResponse(JSONObject response) {
        mProgressDialog.dismiss();
        if (getActivity() != null) {
            /*progressBar.setVisibility(View.GONE);*/
            try {
                if (response != null && response.has("complaints") && response.getJSONArray("complaints").length() > 0) {
                    JSONArray jsonArray = response.getJSONArray("complaints");
                    publihedHelpDeskAdapter = new PublihedHelpDeskAdapter(getActivity(), jsonArray);
                    recyclerView.setAdapter(publihedHelpDeskAdapter);
                } else {
                    recyclerView.setVisibility(View.GONE);
                    errorMsg.setVisibility(View.VISIBLE);
                    errorMsg.setText("No list to display");
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private class PublihedHelpDeskAdapter extends RecyclerView.Adapter<PublihedHelpDeskAdapter.PublishedHelpDeskViewHolder>
            implements Response.ErrorListener, Response.Listener<JSONObject> {

        public static final String REQUEST_TAG = "PendingHelpDeskAdapter";
        Context context;
        JSONArray jsonArray;
        ArrayList<AccountData> accountDataArrayList;
        int societyId;
        private RequestQueue mQueue;
        private LockatedPreferences mLockatedPreferences;
        private AccountController accountController;
        private String Creatername;

        public PublihedHelpDeskAdapter(Context context, JSONArray jsonArray) {

            this.context = context;
            this.jsonArray = jsonArray;

        }

        @Override
        public PublishedHelpDeskViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View v = LayoutInflater.from(context).inflate(R.layout.published_help_desk_child, parent, false);
            PublishedHelpDeskViewHolder pvh = new PublishedHelpDeskViewHolder(v);
            return pvh;
        }

        @Override
        public void onBindViewHolder(PublishedHelpDeskViewHolder holder, final int position) {
            accountController = AccountController.getInstance();
            Creatername = accountController.getmAccountDataList().get(0).getFirstname() + " " +
                    accountController.getmAccountDataList().get(0).getLastname();
            accountDataArrayList = accountController.getmAccountDataList();
            societyId = accountDataArrayList.get(0).getmResidenceDataList().get(0).getSociety_id();
            Calendar c = Calendar.getInstance();
            SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSz", Locale.ENGLISH);
            formattedDate = df.format(c.getTime());
            try {
                pid = jsonArray.getJSONObject(position).getString("id");
                holder.mIssueType.setText(jsonArray.getJSONObject(position).getString("issue_type"));
                holder.mIssueDescription.setText(jsonArray.getJSONObject(position).getString("text"));
                holder.mIssueCategory.setText(jsonArray.getJSONObject(position).getString("heading"));
                holder.mPostedOn.setText(Utilities.ddMMYYYFormat(jsonArray.getJSONObject(position).getString("created_at")));
                holder.mTextCategory.setText(jsonArray.getJSONObject(position).getString("category_type"));
                holder.mTextStatus.setText(jsonArray.getJSONObject(position).getString("issue_status"));
                holder.mpostedBy.setText(jsonArray.getJSONObject(position).getString("posted_by"));
                holder.mFlatNumber.setText(jsonArray.getJSONObject(position).getString("flat_number"));
                holder.mUpdatedOn.setText(Utilities.ddMMYYYFormat(jsonArray.getJSONObject(position).getString("updated_at")));

                holder.mDetails.setTag(position);
                holder.mDetails.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        Intent intent = new Intent(getActivity(), PublishedHelpDeskActivity.class);
                        try {
                            intent.putExtra("strIssueType", jsonArray.getJSONObject(position).getString("issue_type"));
                            intent.putExtra("strStatus", jsonArray.getJSONObject(position).getString("issue_status"));
                            intent.putExtra("strDescription", jsonArray.getJSONObject(position).getString("text"));
                            intent.putExtra("strIssueCategory", jsonArray.getJSONObject(position).getString("heading"));
                            intent.putExtra("strPostedOn", Utilities.ddMMYYYFormat(jsonArray.getJSONObject(position).getString("created_at")));
                            intent.putExtra("strmTextCategory", jsonArray.getJSONObject(position).getString("category_type"));
                            intent.putExtra("strpostedBy", jsonArray.getJSONObject(position).getString("posted_by"));
                            intent.putExtra("FlatNumber", jsonArray.getJSONObject(position).getString("flat_number"));
                            intent.putExtra("strmUpdatedOn", Utilities.ddMMYYYFormat(jsonArray.getJSONObject(position).getString("updated_at")));
                            startActivity(intent);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                });

            } catch (JSONException e) {
                e.printStackTrace();
            }

        }

        @Override
        public int getItemCount() {
            return jsonArray.length();
        }

        @Override
        public void onErrorResponse(VolleyError error) {
            mProgressDialog.dismiss();
            if (context != null) {
                LockatedRequestError.onRequestError(context, error);
            }
        }

        @Override
        public void onResponse(JSONObject response) {
            mProgressDialog.dismiss();
            if (context != null) {
                try {
                    if (response != null && response.length() > 0) {
                        Intent intent = new Intent(context, AdminActivity.class);
                        intent.putExtra("AdminNotice", 2);
                        context.startActivity(intent);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

        }

        public class PublishedHelpDeskViewHolder extends RecyclerView.ViewHolder {
            TextView mIssueType, mIssueDescription, mIssueCategory, mpostedBy, mPostedOn,
                    mUpdatedOn, mTextCategory, mFlatNumber, mPostedBy, mTextStatus, mDetails;

            public PublishedHelpDeskViewHolder(View itemView) {
                super(itemView);
                mTextStatus = (TextView) itemView.findViewById(R.id.mTextStatus);
                mPostedBy = (TextView) itemView.findViewById(R.id.mPostedBy);
                mFlatNumber = (TextView) itemView.findViewById(R.id.mFlatNumber);
                mTextCategory = (TextView) itemView.findViewById(R.id.mTextCategory);
                mIssueType = (TextView) itemView.findViewById(R.id.mIssueType);//personal/socity//issue_type
                mIssueDescription = (TextView) itemView.findViewById(R.id.mIssueDescription);//text
                mIssueCategory = (TextView) itemView.findViewById(R.id.mIssueCategory);//heading
                mDetails = (TextView) itemView.findViewById(R.id.mDetails);
                mpostedBy = (TextView) itemView.findViewById(R.id.mpostedBy);//Created_By
                mPostedOn = (TextView) itemView.findViewById(R.id.mPostedOn);//create date
                mUpdatedOn = (TextView) itemView.findViewById(R.id.mUpdatedOn);
            }

        }
    }

}