
package com.android.lockated.model.AdminModel.AdminFacilities.Booked;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class BookedFacilityList {

    @SerializedName("booked")
    @Expose
    private List<Booked> booked = new ArrayList<Booked>();

    /**
     * 
     * @return
     *     The booked
     */
    public List<Booked> getBooked() {
        return booked;
    }

    /**
     * 
     * @param booked
     *     The booked
     */
    public void setBooked(List<Booked> booked) {
        this.booked = booked;
    }

}
