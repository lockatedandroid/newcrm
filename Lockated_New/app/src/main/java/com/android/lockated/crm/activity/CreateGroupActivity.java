package com.android.lockated.crm.activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.SparseBooleanArray;
import android.view.ActionMode;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.android.lockated.R;
import com.android.lockated.model.userGroups.Groupmember;
import com.android.lockated.model.usermodel.myGroups.UserSociety;
import com.android.lockated.network.ConnectionDetector;
import com.android.lockated.preferences.LockatedPreferences;
import com.android.lockated.request.LockatedRequestError;
import com.android.lockated.request.LockatedVolleyRequestQueue;
import com.android.lockated.utils.ApplicationURL;
import com.android.lockated.utils.Utilities;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class CreateGroupActivity extends AppCompatActivity implements Response.ErrorListener,
        Response.Listener<JSONObject> {

    EditText groupName;
    ListView memberList;
    ListViewAdapter listViewAdapter;
    String REQUEST_TAG = "CreateGroupActivity";
    String CREATE_TAG = "CreateGroup";
    RequestQueue mQueue;
    ProgressDialog mProgressDialog;
    int checkedCount, groupId;
    String updateGroup;
    String groupNameText;
    ArrayList<Integer> userId;
    ArrayList<String> userIdArrayList;
    ArrayList<UserSociety> userSocietyArrayList;
    ArrayList<Groupmember> groupmemberArrayList;
    LockatedPreferences mLockatedPreferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_group);

        this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        setToolBar(getString(R.string.create_group));
        init();

    }

    @Override
    protected void onResume() {
        super.onResume();
        userSocietyArrayList.clear();
        getAllMemberApi();
    }

    private void setToolBar(String name) {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(true);
        getSupportActionBar().setTitle(name);
    }

    private void init() {

        mLockatedPreferences = new LockatedPreferences(this);
        userSocietyArrayList = new ArrayList<>();
        userIdArrayList = new ArrayList<>();

        groupName = (EditText) findViewById(R.id.groupName);
        memberList = (ListView) findViewById(R.id.memberList);
        mProgressDialog = ProgressDialog.show(this, "", "Please Wait...", false);
        checkIfUpdateOrCreate();
        listViewAdapter = new ListViewAdapter(this, R.layout.listview_item, userSocietyArrayList,
                userId, updateGroup, groupmemberArrayList);
        memberList.setAdapter(listViewAdapter);
        memberList.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE_MODAL);
        memberList.setMultiChoiceModeListener(new ListView.MultiChoiceModeListener() {

            @Override
            public void onItemCheckedStateChanged(ActionMode mode, int position, long id, boolean checked) {
                checkedCount = memberList.getCheckedItemCount();
                mode.setTitle(checkedCount + " Selected");
                listViewAdapter.toggleSelection(position);
            }

            @Override
            public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.select:
                        forCreateAndUpdate(mode);
                        return true;
                    case R.id.update:
                        forCreateAndUpdate(mode);
                        return true;
                    default:
                        return false;
                }
            }

            @Override
            public boolean onCreateActionMode(ActionMode mode, Menu menu) {
                mode.getMenuInflater().inflate(R.menu.group_select_member, menu);
                MenuItem select = menu.findItem(R.id.select);
                MenuItem update = menu.findItem(R.id.update);
                if (updateGroup != null && updateGroup.equals(getString(R.string.update_group))) {
                    update.setVisible(true);
                } else {
                    select.setVisible(true);
                }
                return true;
            }

            @Override
            public void onDestroyActionMode(ActionMode mode) {
                listViewAdapter.removeSelection();
            }

            @Override
            public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
                return false;
            }

        });

    }

    private void forCreateAndUpdate(ActionMode mode) {
        SparseBooleanArray selected = listViewAdapter.getSelectedIds();
        for (int i = (selected.size() - 1); i >= 0; i--) {
            if (selected.valueAt(i)) {
                UserSociety selecteditem = listViewAdapter.getItem(selected.keyAt(i));
                if (selecteditem.getUser() != null) {
                    userIdArrayList.add(String.valueOf(selecteditem.getUser().getId()));
                }
                listViewAdapter.add(selecteditem);
            }
        }
        onSubmitButtonClicked();
        mode.finish();
    }

    private void checkIfUpdateOrCreate() {
        if (getIntent().getIntegerArrayListExtra("userId") != null) {
            userId = getIntent().getIntegerArrayListExtra("userId");
            groupNameText = getIntent().getStringExtra("groupName");
            updateGroup = getIntent().getStringExtra("updateGroup");
            groupmemberArrayList = getIntent().getParcelableArrayListExtra("groupmemberArrayList");
            groupId = Integer.valueOf(getIntent().getStringExtra("groupId"));
            setToolBar(updateGroup);
            groupName.setText(groupNameText);
        }
    }

    private void getAllMemberApi() {
        if (ConnectionDetector.isConnectedToInternet(this)) {
            mProgressDialog.show();
            String url = ApplicationURL.getAllMemberList + mLockatedPreferences.getSocietyId()
                    + "&token=" + mLockatedPreferences.getLockatedToken();
            LockatedVolleyRequestQueue lockatedVolleyRequestQueue = LockatedVolleyRequestQueue.getInstance(this);
            lockatedVolleyRequestQueue.sendRequest(CREATE_TAG, Request.Method.GET, url, null, this, this);
            /*mQueue = LockatedVolleyRequestQueue.getInstance(this).getRequestQueue();
            String url = ApplicationURL.getAllMemberList + mLockatedPreferences.getSocietyId()
                    + "&token=" + mLockatedPreferences.getLockatedToken();
            LockatedJSONObjectRequest lockatedJSONObjectRequest = new LockatedJSONObjectRequest(Request.Method.GET,
                    url, null, this, this);
            lockatedJSONObjectRequest.setTag(REQUEST_TAG);
            mQueue.add(lockatedJSONObjectRequest);*/
        } else {
            Utilities.showToastMessage(this, this.getResources().getString(R.string.internet_connection_error));
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.group_select_member, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                super.onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void onSubmitButtonClicked() {

        String name = groupName.getText().toString();

        if (name.length() == 0) {
            Utilities.showToastMessage(this, "Please enter the group name");
            userIdArrayList.clear();
        } else if (checkedCount == 0) {
            Utilities.showToastMessage(this, "Please select atleast one member");
            userIdArrayList.clear();
        } else if (userIdArrayList.size() == 0) {
            Utilities.showToastMessage(this, "Please select atleast one member");
            userIdArrayList.clear();
        } else {
            if (updateGroup != null && updateGroup.equals(getString(R.string.update_group))) {
                callUpdateGroupApi(name, userIdArrayList);
            } else {
                callCreateGroupApi(name, userIdArrayList);
            }
        }

    }

    private void callCreateGroupApi(String name, ArrayList<String> userIdArrayList) {
        if (ConnectionDetector.isConnectedToInternet(this)) {
            mProgressDialog.dismiss();
            JSONObject jsonObjectMain = new JSONObject();
            JSONObject jsonObject = new JSONObject();
            JSONArray jsonArray = new JSONArray(userIdArrayList);
            try {
                jsonObject.put("name", name);
                jsonObject.put("user_society_id", mLockatedPreferences.getUserSocietyId());
                jsonObject.put("swusers", jsonArray);
                jsonObjectMain.put("usergroup", jsonObject);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            try {
                String url = ApplicationURL.postMyGroups + mLockatedPreferences.getLockatedToken();
                LockatedVolleyRequestQueue lockatedVolleyRequestQueue = LockatedVolleyRequestQueue.getInstance(this);
                lockatedVolleyRequestQueue.sendRequest(CREATE_TAG, Request.Method.POST, url, jsonObjectMain, this, this);
                /*mQueue = LockatedVolleyRequestQueue.getInstance(this).getRequestQueue();
                String url = ApplicationURL.postMyGroups + mLockatedPreferences.getLockatedToken();
                LockatedJSONObjectRequest lockatedJSONObjectRequest = new LockatedJSONObjectRequest(Request.Method.POST,
                        url, jsonObjectMain, this, this);
                lockatedJSONObjectRequest.setTag(CREATE_TAG);
                mQueue.add(lockatedJSONObjectRequest);*/
            } catch (Exception e) {
                e.printStackTrace();
            }

        } else {
            Utilities.showToastMessage(this, this.getResources().getString(R.string.internet_connection_error));
        }
    }

    private void callUpdateGroupApi(String name, ArrayList<String> userIdArrayList) {
        if (ConnectionDetector.isConnectedToInternet(this)) {
            mProgressDialog.dismiss();
            JSONObject jsonObjectMain = new JSONObject();
            JSONObject jsonObject = new JSONObject();
            JSONArray jsonArray = new JSONArray(userIdArrayList);
            try {
                jsonObject.put("name", name);
                jsonObject.put("swusers", jsonArray);
                jsonObjectMain.put("usergroup", jsonObject);
            } catch (JSONException e) {
                e.printStackTrace();
            }

            try {
                String url = ApplicationURL.updateCurrentGroup + groupId + ".json?token=" + mLockatedPreferences.getLockatedToken();
                LockatedVolleyRequestQueue lockatedVolleyRequestQueue = LockatedVolleyRequestQueue.getInstance(this);
                lockatedVolleyRequestQueue.sendRequest(CREATE_TAG, Request.Method.PUT, url, jsonObjectMain, this, this);
                /*mQueue = LockatedVolleyRequestQueue.getInstance(this).getRequestQueue();
                String url = ApplicationURL.updateCurrentGroup + groupId + ".json?token=" + mLockatedPreferences.getLockatedToken();
                LockatedJSONObjectRequest lockatedJSONObjectRequest = new LockatedJSONObjectRequest(Request.Method.PUT,
                        url, jsonObjectMain, this, this);
                lockatedJSONObjectRequest.setTag(CREATE_TAG);
                mQueue.add(lockatedJSONObjectRequest);*/
            } catch (Exception e) {
                e.printStackTrace();
            }

        } else {
            Utilities.showToastMessage(this, this.getResources().getString(R.string.internet_connection_error));
        }
    }

    @Override
    public void onErrorResponse(VolleyError error) {
        mProgressDialog.dismiss();
        LockatedRequestError.onRequestError(this, error);
    }

    @Override
    public void onResponse(JSONObject response) {
        mProgressDialog.dismiss();
        if (response.has("user_societies")) {
            try {
                if (response.getJSONArray("user_societies").length() > 0) {
                    JSONArray jsonArray = response.getJSONArray("user_societies");
                    Gson gson = new Gson();
                    for (int i = 0; i < jsonArray.length(); i++) {
                        UserSociety userSociety = gson.fromJson(jsonArray.getJSONObject(i).toString(), UserSociety.class);
                        userSocietyArrayList.add(userSociety);
                    }
                    if ((userId != null) && (userId.size() > 0)) {
                        for (int i = 0; i < userSocietyArrayList.size(); i++) {
                            if (userId.contains(userSocietyArrayList.get(i).getUser().getId())) {
                                memberList.setItemChecked(i, true);
                            }
                        }
                    }
                }
                listViewAdapter.notifyDataSetChanged();
            } catch (JSONException e) {
                e.printStackTrace();
            }
        } else if (response.has("id")) {
            CreateGroupActivity.this.finish();
        } else {
        }
    }

    public class ListViewAdapter extends ArrayAdapter<UserSociety> implements View.OnClickListener,
            Response.ErrorListener, Response.Listener<JSONObject> {

        Context context;
        String updateGroup;
        LayoutInflater inflater;
        ArrayList<Integer> userId;
        ArrayList<UserSociety> userSocietyArrayList;
        ArrayList<Groupmember> groupmemberArrayList;
        private SparseBooleanArray mSelectedItemsIds;
        int userIdPosition;

        public ListViewAdapter(Context context, int resource, ArrayList<UserSociety> userSocietyArrayList,
                               ArrayList<Integer> userId, String updateGroup, ArrayList<Groupmember> groupmemberArrayList) {
            super(context, resource, userSocietyArrayList);

            this.context = context;
            inflater = LayoutInflater.from(context);
            mSelectedItemsIds = new SparseBooleanArray();
            this.userSocietyArrayList = userSocietyArrayList;
            this.userId = userId;
            this.updateGroup = updateGroup;
            this.groupmemberArrayList = groupmemberArrayList;
        }

        public View getView(int position, View view, ViewGroup parent) {
            final ViewHolder holder;
            if (view == null) {
                holder = new ViewHolder();
                view = inflater.inflate(R.layout.listview_item, parent, false);
                holder.userNameText = (TextView) view.findViewById(R.id.userNameText);
                holder.societyNameText = (TextView) view.findViewById(R.id.societyNameText);
                holder.flatNoText = (TextView) view.findViewById(R.id.flatNoText);
                holder.removeMember = (TextView) view.findViewById(R.id.removeMember);
                holder.mainLinear = (LinearLayout) view.findViewById(R.id.mainLinear);
                view.setTag(holder);
            } else {
                holder = (ViewHolder) view.getTag();
            }
            if (userSocietyArrayList.get(position).getUser() != null) {
                String name = userSocietyArrayList.get(position).getUser().getFirstname()
                        + " " + userSocietyArrayList.get(position).getUser().getLastname();
                holder.userNameText.setText(name);
            } else {
                holder.userNameText.setText("No Data");
            }
            if (userSocietyArrayList.get(position).getSociety() != null) {
                holder.societyNameText.setText(userSocietyArrayList.get(position).getSociety().getBuildingName());
            } else {
                holder.societyNameText.setText("No Data");
            }
            if (userSocietyArrayList.get(position).getUserFlat() != null) {
                holder.flatNoText.setText(userSocietyArrayList.get(position).getUserFlat().getFlat());
            } else {
                holder.flatNoText.setText("No Data");
            }
            if (updateGroup != null && updateGroup.equals(getString(R.string.update_group)) &&
                    userId.contains(userSocietyArrayList.get(position).getUser().getId())) {
            }

            if (updateGroup != null && updateGroup.equals(getString(R.string.update_group))
                    && userId.contains(userSocietyArrayList.get(position).getUser().getId())) {
                holder.removeMember.setVisibility(View.VISIBLE);
                holder.removeMember.setTag(position);
                holder.removeMember.setOnClickListener(this);
            }

            return view;
        }

        private class ViewHolder {
            TextView userNameText;
            TextView societyNameText;
            TextView flatNoText;
            TextView removeMember;
            LinearLayout mainLinear;
        }

        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.removeMember:
                    boolean continueLoop = true;
                    int position = (int) v.getTag();
                    userIdPosition = position;
                    for (int i = 0; i < groupmemberArrayList.size(); i++) {
                        if (continueLoop) {
                            if (groupmemberArrayList.get(i).getUserId() == userSocietyArrayList.get(position).getUser().getId()) {
                                continueLoop = false;
                                deleteUserFromGroup(context, groupmemberArrayList.get(i).getId());
                            }
                        }
                    }
                    break;
            }
        }

        private void updateView(int position) {
            View view = memberList.getChildAt(position);
            if (view != null) {
                TextView removeView = (TextView) view.findViewById(R.id.removeMember);
                removeView.setText("");
                removeView.setBackgroundColor(0);
                removeView.setEnabled(false);
            }
        }

        private void deleteUserFromGroup(Context context, int userId) {
            if (ConnectionDetector.isConnectedToInternet(context)) {
                mProgressDialog.show();
                String url = ApplicationURL.removeUserFromGroup + userId + ".json?token=" + mLockatedPreferences.getLockatedToken();
                LockatedVolleyRequestQueue lockatedVolleyRequestQueue = LockatedVolleyRequestQueue.getInstance(context);
                lockatedVolleyRequestQueue.sendRequest(CREATE_TAG, Request.Method.DELETE, url, null, this, this);
                /*mQueue = LockatedVolleyRequestQueue.getInstance(context).getRequestQueue();
                String url = ApplicationURL.removeUserFromGroup + userId + ".json?token=" + mLockatedPreferences.getLockatedToken();
                LockatedJSONObjectRequest lockatedJSONObjectRequest = new LockatedJSONObjectRequest(Request.Method.DELETE,
                        url, null, this, this);
                lockatedJSONObjectRequest.setTag(REQUEST_TAG);
                mQueue.add(lockatedJSONObjectRequest);*/
            } else {
                Utilities.showToastMessage(context, context.getResources().getString(R.string.internet_connection_error));
            }
        }

        @Override
        public void onErrorResponse(VolleyError error) {
            mProgressDialog.dismiss();
            if (context != null) {
                LockatedRequestError.onRequestError(context, error);
            }
        }

        @Override
        public void onResponse(JSONObject response) {
            mProgressDialog.dismiss();
            if (context != null) {
                try {
                    if (response.has("code")) {
                        if (response.getString("code").equals("200")) {
                            memberList.setItemChecked(userIdPosition, false);
                            updateView(userIdPosition);
                        }
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }

        @Override
        public void add(UserSociety object) {
            //super.add(object);
            //userSocietyArrayList.add(object);
            //notifyDataSetChanged();
        }

        public ArrayList<UserSociety> getWorldPopulation() {
            return userSocietyArrayList;
        }

        public void toggleSelection(int position) {
            selectView(position, !mSelectedItemsIds.get(position));
        }

        public void removeSelection() {
            mSelectedItemsIds = new SparseBooleanArray();
            notifyDataSetChanged();
        }

        public void selectView(int position, boolean value) {
            if (value) {
                mSelectedItemsIds.put(position, value);
            } else {
                mSelectedItemsIds.delete(position);
            }
            notifyDataSetChanged();
        }

        public int getSelectedCount() {
            return mSelectedItemsIds.size();
        }

        public SparseBooleanArray getSelectedIds() {
            return mSelectedItemsIds;
        }
    }

}