package com.android.lockated.account.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.lockated.R;
import com.android.lockated.utils.Utilities;

import org.json.JSONArray;
import org.json.JSONException;

public class GroupListAdapter extends BaseAdapter {

    Context context;
    JSONArray jsonArray;

    public GroupListAdapter(Context context, JSONArray jsonArray) {
        this.context = context;
        this.jsonArray = jsonArray;
    }

    @Override
    public int getCount() {
        return jsonArray.length();
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        GroupListAdapterViewHolder groupListAdapterViewHolder;
        if (convertView == null) {
            groupListAdapterViewHolder = new GroupListAdapterViewHolder();
            convertView = LayoutInflater.from(context).inflate(R.layout.group_list, parent, false);
            convertView.setTag(groupListAdapterViewHolder);
        } else {
            groupListAdapterViewHolder = (GroupListAdapterViewHolder) convertView.getTag();
        }
        groupListAdapterViewHolder.groupImage = (ImageView) convertView.findViewById(R.id.groupImage);
        groupListAdapterViewHolder.groupName = (TextView) convertView.findViewById(R.id.groupName);
        groupListAdapterViewHolder.groupMemberNumber = (TextView) convertView.findViewById(R.id.groupMemberNumber);

        try {
            groupListAdapterViewHolder.groupName.setText(jsonArray.getJSONObject(position).getString("name"));
            groupListAdapterViewHolder.groupImage.setImageResource(Utilities.setListImage(jsonArray.getJSONObject(position).getString("name")));
            try {
                String grpSize = "" + jsonArray.getJSONObject(position).getJSONArray("groupmembers").length();
                groupListAdapterViewHolder.groupMemberNumber.setText(grpSize);
            } catch (Exception e) {
                e.printStackTrace();
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return convertView;
    }

    class GroupListAdapterViewHolder {
        ImageView groupImage;
        TextView groupName;
        TextView groupMemberNumber;
    }
}
