
package com.android.lockated.model.userSocietyFlat;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class UserSociety implements Parcelable {

    @SerializedName("status")
    @Expose
    private String status;



    /**
     * 
     * @return
     *     The status
     */
    public String getStatus() {
        return status;
    }

    /**
     * 
     * @param status
     *     The status
     */
    public void setStatus(String status) {
        this.status = status;
    }

    @Override
    public int describeContents() {
        return this.hashCode();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(status);
    }
    protected UserSociety(Parcel in) {
        status = in.readString();
    }

    public static final Creator<UserSociety> CREATOR = new Creator<UserSociety>() {
        @Override
        public UserSociety createFromParcel(Parcel in) {
            return new UserSociety(in);
        }

        @Override
        public UserSociety[] newArray(int size) {
            return new UserSociety[size];
        }
    };
}
