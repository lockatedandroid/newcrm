
package com.android.lockated.model.AdminModel.AdminPolls;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class PublishPolls {

    @SerializedName("polls")
    @Expose
    private ArrayList<Poll> polls = new ArrayList<Poll>();

    /**
     * 
     * @return
     *     The polls
     */
    public ArrayList<Poll> getPolls() {
        return polls;
    }

    /**
     * 
     * @param polls
     *     The polls
     */
    public void setPolls(ArrayList<Poll> polls) {
        this.polls = polls;
    }

}
