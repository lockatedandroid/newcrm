package com.android.lockated.crm.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.android.lockated.Interfaces.IRecyclerItemClickListener;
import com.android.lockated.R;
import com.android.lockated.holder.RecyclerViewHolder;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;

public class CrmAdapter extends RecyclerView.Adapter<RecyclerViewHolder> {

    private final Context applicationContext;
    String[] data;
    private IRecyclerItemClickListener mIRecyclerItemClickListener;

    public CrmAdapter(String[] data, IRecyclerItemClickListener listener, Context applicationContext) {
        this.data = data;
        this.applicationContext = applicationContext;
        mIRecyclerItemClickListener = listener;
    }

    @Override
    public RecyclerViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.fragment_home_row, parent, false);
        return new RecyclerViewHolder(itemView, mIRecyclerItemClickListener);
    }

    @Override
    public void onBindViewHolder(RecyclerViewHolder holder, int position) {
       //Crm Screen Icons
        holder.onCrmViewHolder(data[position],position,applicationContext);

    }

    @Override
    public int getItemCount() {
        return data.length;
    }
}
