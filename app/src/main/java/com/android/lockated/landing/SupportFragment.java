package com.android.lockated.landing;

import android.content.Context;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.android.lockated.ChatSupportActivity;
import com.android.lockated.HomeActivity;
import com.android.lockated.LockatedApplication;
import com.android.lockated.R;
import com.android.lockated.information.AccountController;
import com.android.lockated.landing.adapter.SupportAdapter;
import com.android.lockated.landing.model.SupportData;
import com.android.lockated.model.AccountData;
import com.android.lockated.network.ConnectionDetector;
import com.android.lockated.preferences.LockatedPreferences;
import com.android.lockated.request.LockatedRequestError;
import com.android.lockated.request.LockatedVolleyRequestQueue;
import com.android.lockated.utils.ApplicationURL;
import com.android.lockated.utils.Utilities;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

public class SupportFragment extends Fragment implements View.OnClickListener, Response.Listener, Response.ErrorListener, AdapterView.OnItemClickListener {
    private View mSupportFragmentView;
    private View mSupportHeaderPreviousMessages;

    private ListView mSupportList;
    public ListView mSupportListSample;
    private ProgressBar mProgressBarView;

    private EditText mEditTextAddMessage;
    private TextView mTextViewSupportMessageDate;
    private TextView mAverageTime;
    private TextView mTextViewSupportHeaderPreviousMessages;
    private ImageView mImageViewMessageSend;
    public ImageView mImageViewSampleMessage;

    private SupportAdapter mSupportAdapter;

    private RequestQueue mQueue;
    private LockatedPreferences mLockatedPreferences;
    private LockatedVolleyRequestQueue mLockatedVolleyRequestQueue;

    private static int mChatId;
    private int mPageNumber = 1;

    private String mScreenName;

    private CountDownTimer mCountDownTimer;

    private boolean hasScreenFirstView;
    private boolean isSupportScreenPaused;
    public boolean isSampleMessageSelected;

    public static final String REQUEST_TAG = "SupportFragment";
    public static final String PAGE_REQUEST_TAG = "PageSupportFragment";
    public static final String SEND_MESSAGE = "SendSupportFragment";
    public static final String GET_ID_REQUEST = "GetIdSupportFragment";
    public static final String TYPING_REQUEST = "TypingSupportFragment";
    public static final String OPERATOR_TYPING = "OperatorSupportFragment";
    public static final String DEPT_ONLINE = "DeptOnlineSupportFragment";

    ArrayList<AccountData> datas;
    //private AccountController mAccountController;
    String fullName, phoneNum, chatIdResp, userId;
    int deptId;
    ChatSupportActivity chatSupportActivity;
    HomeActivity homeActivity;
    boolean onlineOffline;
    String screenName = "Chat Support Screen";

    public void setChatId(int id, String name) {
        mChatId = id;
        deptId = mChatId;
        mScreenName = name;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mSupportFragmentView = inflater.inflate(R.layout.fragment_support, container, false);
        mLockatedPreferences = new LockatedPreferences(getActivity());
        Utilities.ladooIntegration(getActivity(), screenName);
        Utilities.lockatedGoogleAnalytics(screenName, getString(R.string.visited), screenName);
        init();
        return mSupportFragmentView;
    }

    @Override
    public void onPause() {
        super.onPause();
        if (mCountDownTimer != null) {
            mCountDownTimer.cancel();
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        if (mCountDownTimer != null) {
            mCountDownTimer.start();
        }
        LockatedApplication.getInstance().trackScreenView(mScreenName);
        Utilities.lockatedGoogleAnalytics(mScreenName, getString(R.string.visited), mScreenName);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        hasScreenFirstView = false;
    }

    private void init() {
        AccountController mAccountController = AccountController.getInstance();
        datas = mAccountController.getmAccountDataList();
        /*fullName = datas.get(0).getFirstname() + "%20" + datas.get(0).getLastname();
        phoneNum = datas.get(0).getMobile();
        userId = datas.get(0).getId();*/

        fullName = mLockatedPreferences.getAccountData().getFirstname()
                + "%20" + mLockatedPreferences.getAccountData().getLastname();
        phoneNum = mLockatedPreferences.getAccountData().getMobile();
        userId = mLockatedPreferences.getAccountData().getId();

        mSupportList = (ListView) mSupportFragmentView.findViewById(R.id.mSupportList);
        mSupportListSample = (ListView) mSupportFragmentView.findViewById(R.id.mSupportListSample);

        mProgressBarView = (ProgressBar) mSupportFragmentView.findViewById(R.id.mProgressBarSmallView);

        mTextViewSupportMessageDate = (TextView) mSupportFragmentView.findViewById(R.id.mTextViewSupportMessageDate);
        mAverageTime = (TextView) mSupportFragmentView.findViewById(R.id.averageTime);
        mEditTextAddMessage = (EditText) mSupportFragmentView.findViewById(R.id.mEditTextAddMessage);
        mImageViewMessageSend = (ImageView) mSupportFragmentView.findViewById(R.id.mImageViewMessageSend);
        mImageViewSampleMessage = (ImageView) mSupportFragmentView.findViewById(R.id.mImageViewSampleMessage);

        mProgressBarView.setVisibility(View.GONE);
        mSupportListSample.setVisibility(View.GONE);
        mTextViewSupportMessageDate.setVisibility(View.GONE);

        mSupportListSample.setOnItemClickListener(this);
        mImageViewMessageSend.setOnClickListener(this);
        mImageViewSampleMessage.setOnClickListener(this);

        addHeaderView();

        int sampleListArray = getSampleListArray(mChatId);

        mSupportAdapter = new SupportAdapter(getActivity());
        ArrayAdapter<String> supportMessageAdapter = new ArrayAdapter<>(getActivity(),
                android.R.layout.simple_list_item_1, getResources().getStringArray(sampleListArray));
        mSupportList.setAdapter(mSupportAdapter);
        mSupportListSample.setAdapter(supportMessageAdapter);

        getChatId();
        onEditTextChangeListener();
    }

    private void addHeaderView() {
        LayoutInflater inflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        mSupportHeaderPreviousMessages = inflater.inflate(R.layout.fragment_support_previous_messages, null);

        mTextViewSupportHeaderPreviousMessages = (TextView) mSupportHeaderPreviousMessages.findViewById(R.id.mTextViewSupportPreviousMessages);
        mSupportHeaderPreviousMessages.setVisibility(View.GONE);

        mTextViewSupportHeaderPreviousMessages.setOnClickListener(this);

        mSupportList.addHeaderView(mSupportHeaderPreviousMessages);
    }

    private int getSampleListArray(int mChatId) {

        switch (mChatId) {
            case 4:
                return R.array.beauty_faq_array;
            case 3:
                return R.array.grocery_faq_array;
            case 8:
                return R.array.laundry_faq_array;
            case 1:
                return R.array.food_faq_array;
            case 6:
                return R.array.household_faq_array;
            case 7:
                return R.array.medicine_faq_array;
            case 2:
                return R.array.miscellaneous_faq_array;
            case 9:
                return R.array.reminder_faq_array;
            case 5:
                return R.array.repair_faq_array;
            case 157:
                return R.array.support_faq_array;
            default:
                return R.array.support_faq_array;
        }
    }

    private void onEditTextChangeListener() {
        mEditTextAddMessage.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.length() > 0) {
                    getUserTyping();
                    mImageViewMessageSend.setImageResource(R.drawable.ic_message_send);
                } else {
                    mImageViewMessageSend.setImageResource(R.drawable.ic_message_send_disable);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }

    public void getChatId() {
        if (!isSupportScreenPaused) {
            if (ConnectionDetector.isConnectedToInternet(getActivity())) {
                mProgressBarView.setVisibility(View.VISIBLE);
                String url = ApplicationURL.getChatIdUrl + userId + "&dep_id=" + mChatId + "&token="
                        + mLockatedPreferences.getLockatedToken() + "&user_name="
                        + fullName + "&phone=" + phoneNum;
                mLockatedVolleyRequestQueue = LockatedVolleyRequestQueue.getInstance(getActivity());
                mLockatedVolleyRequestQueue.sendRequest(GET_ID_REQUEST, Request.Method.GET, url, null, this, this);
            } else {
                Utilities.showToastMessage(getActivity(), getActivity().getResources().getString(R.string.internet_connection_error));
            }
        }
    }

    public void getUserTyping() {
        if (!isSupportScreenPaused) {
            if (ConnectionDetector.isConnectedToInternet(getActivity())) {
                mProgressBarView.setVisibility(View.VISIBLE);
                int conversationId = 0;
                try {
                    conversationId = mSupportAdapter.getmData().get(0).getConversation_id();
                } catch (Exception e) {
                    conversationId = mChatId;
                }
                String url = ApplicationURL.getUserTypingUrl + conversationId + "/" +
                        mLockatedPreferences.getLockatedToken() + "/true";
                mLockatedVolleyRequestQueue = LockatedVolleyRequestQueue.getInstance(getActivity());
                mLockatedVolleyRequestQueue.sendRequest(TYPING_REQUEST, Request.Method.POST, url, null, this, this, "user is typing");
                /*Log.e("getUserTypingUrl", "" + url);*/
            } else {
                Utilities.showToastMessage(getActivity(), getActivity().getResources().getString(R.string.internet_connection_error));
            }
        }
    }

    public void getIsOperatorTyping() {
        if (!isSupportScreenPaused) {
            if (ConnectionDetector.isConnectedToInternet(getActivity())) {
                mProgressBarView.setVisibility(View.VISIBLE);
                String url = ApplicationURL.getOperatorTypingUrl + chatIdResp + "&token=" +
                        mLockatedPreferences.getLockatedToken();
                mLockatedVolleyRequestQueue = LockatedVolleyRequestQueue.getInstance(getActivity());
                mLockatedVolleyRequestQueue.sendRequest(OPERATOR_TYPING, Request.Method.GET,
                        url, null, this, this);
            } else {
                Utilities.showToastMessage(getActivity(), getActivity().getResources().getString(R.string.internet_connection_error));
            }
        }
    }

    private void onPreviousMessagesClicked() {
        if (!isSupportScreenPaused) {
            if (mSupportAdapter.getmData().size() > 0) {
                if (ConnectionDetector.isConnectedToInternet(getActivity())) {
                    isSupportScreenPaused = true;
                    mProgressBarView.setVisibility(View.VISIBLE);
                    int conversationId = 0;
                    try {
                        conversationId = mSupportAdapter.getmData().get(0).getConversation_id();
                    } catch (Exception e) {
                        conversationId = mChatId;
                    }
                    mTextViewSupportHeaderPreviousMessages.setVisibility(View.GONE);
                    String url = ApplicationURL.getConversationByIdUrl + conversationId + "?page="
                            + mPageNumber + "&token=" + mLockatedPreferences.getLockatedToken();
                    mLockatedVolleyRequestQueue = LockatedVolleyRequestQueue.getInstance(getActivity());
                    mLockatedVolleyRequestQueue.sendRequest(PAGE_REQUEST_TAG, Request.Method.GET,
                            url, null, this, this);
                } else {
                    Utilities.showToastMessage(getActivity(), getActivity().getResources().getString(R.string.internet_connection_error));
                }
            }
        }
    }

    private void getConversationById() {
        if (!isSupportScreenPaused) {
            if (ConnectionDetector.isConnectedToInternet(getActivity())) {
                mProgressBarView.setVisibility(View.VISIBLE);
                int conversationId = 0;
                try {
                    conversationId = mSupportAdapter.getmData().get(mSupportAdapter.getmData().size() - 1).getId();
                } catch (Exception e) {
                    conversationId = 0;
                }
                String url = ApplicationURL.getConversationByIdUrl + chatIdResp + "&last_id="
                        + conversationId + "&token=" + mLockatedPreferences.getLockatedToken();
                mLockatedVolleyRequestQueue = LockatedVolleyRequestQueue.getInstance(getActivity());
                mLockatedVolleyRequestQueue.sendRequest(REQUEST_TAG, Request.Method.GET,
                        url, null, this, this);

            } else {
                Utilities.showToastMessage(getActivity(), getActivity().getResources().getString(R.string.internet_connection_error));
            }
        }
    }

    private void checkIfDeptOnline() {
        if (!isSupportScreenPaused) {
            if (ConnectionDetector.isConnectedToInternet(getActivity())) {
                mProgressBarView.setVisibility(View.VISIBLE);
                String url = ApplicationURL.getDeptOnlineUrl + deptId;
                mLockatedVolleyRequestQueue = LockatedVolleyRequestQueue.getInstance(getActivity());
                mLockatedVolleyRequestQueue.sendRequest(DEPT_ONLINE, Request.Method.GET,
                        url, null, this, this);
            } else {
                Utilities.showToastMessage(getActivity(), getActivity().getResources().getString(R.string.internet_connection_error));
            }
        }
    }

    private void onSampleMessageSendClicked() {
        String strSupportMessage = mEditTextAddMessage.getText().toString();

        boolean supportSendCancel = false;
        View supportSendFocusView = null;

        if (TextUtils.isEmpty(strSupportMessage)) {
            supportSendFocusView = mEditTextAddMessage;
            supportSendCancel = true;
        }

        if (supportSendCancel) {
            supportSendFocusView.requestFocus();
            Utilities.showToastMessage(getActivity(), getActivity().getResources().getString(R.string.send_query_error));
        } else {
            if (ConnectionDetector.isConnectedToInternet(getActivity())) {
                isSupportScreenPaused = true;
                mProgressBarView.setVisibility(View.VISIBLE);
                try {
                    String SendUrl = ApplicationURL.sendConversationUrl + chatIdResp + "/" + mLockatedPreferences.getLockatedToken();
                    mLockatedVolleyRequestQueue = LockatedVolleyRequestQueue.getInstance(getActivity());
                    mLockatedVolleyRequestQueue.sendRequest(SEND_MESSAGE, Request.Method.POST,
                            SendUrl, null, this, this, strSupportMessage);
                    hideSoftKeyboard();
                    mEditTextAddMessage.setText("");
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else {
                Utilities.showToastMessage(getActivity(), getActivity().getResources().getString(R.string.internet_connection_error));
            }
        }
    }

    private void getConversationInLoop() {
        if (mCountDownTimer == null) {
            mCountDownTimer = new CountDownTimer(2000, 2000) {
                @Override
                public void onTick(long millisUntilFinished) {
                    checkIfDeptOnline();
                    getConversationById();
                    getIsOperatorTyping();
                }

                @Override
                public void onFinish() {
                    mCountDownTimer.start();
                }
            }.start();
        }
    }

    @Override
    public void onErrorResponse(VolleyError error) {
        //Log.e("onErrorResponse", "" + error);
        isSupportScreenPaused = false;
        mProgressBarView.setVisibility(View.GONE);
        if (getActivity() != null) {
            LockatedRequestError.onRequestError(getActivity(), error);
            showHideListHeader();
        }
    }

    @Override
    public void onResponse(Object response) {
        mProgressBarView.setVisibility(View.GONE);

        JSONObject jsonObjectMain = null;
        if (response instanceof String) {
            response = response.toString();
        } else {
            jsonObjectMain = (JSONObject) response;
        }
        if (getActivity() != null) {
            if (jsonObjectMain != null && jsonObjectMain.has("code")) {
                if (jsonObjectMain.has("operator_typing")) {
                    isSupportScreenPaused = false;
                    hasScreenFirstView = true;
                    try {
                        if (jsonObjectMain.getBoolean("operator_typing")) {
                            mAverageTime.setText(getActivity().getResources().getString(R.string.agent_typing));
                        } else {
                            mAverageTime.setText(getActivity().getResources().getString(R.string.average_response_time));
                        }
                        if (mSupportAdapter.getmData().size() > 4) {
                            if (mSupportList.getLastVisiblePosition() > mSupportAdapter.getmData().size() - 4) {
                                hasScreenFirstView = true;
                            }
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                } else if (jsonObjectMain.has("get_messages")) {
                    isSupportScreenPaused = false;
                    hasScreenFirstView = true;
                    try {
                        JSONArray jsonArray = jsonObjectMain.getJSONArray("get_messages");
                        if (jsonArray.length() > 0) {
                            for (int i = 0; i < jsonArray.length(); i++) {
                                JSONObject supportObject = jsonArray.getJSONObject(i);
                                SupportData supportData = new SupportData(jsonObjectMain.optInt("count"), supportObject);
                                mSupportAdapter.addItem(i, supportData);
                            }
                            showHideListHeader();
                            if (mSupportAdapter.getmData().size() > 4) {
                                if (mSupportList.getLastVisiblePosition() > mSupportAdapter.getmData().size() - 4) {
                                    hasScreenFirstView = true;
                                }
                            }
                        } else {
                            showHideListHeader();
                            if (mSupportAdapter.getmData().size() > 4) {
                                if (mSupportList.getLastVisiblePosition() > mSupportAdapter.getmData().size() - 4) {
                                    hasScreenFirstView = true;
                                }
                            }
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                } else if (jsonObjectMain.has("provide_messages")) {
                    try {
                        isSupportScreenPaused = false;
                        hasScreenFirstView = false;
                        try {
                            chatSupportActivity = (ChatSupportActivity) getActivity();
                            chatSupportActivity.setToolBarTitle("none");
                        } catch (Exception e) {

                        }
                        JSONArray jsonArray = jsonObjectMain.getJSONArray("provide_messages");
                        if (jsonArray.length() > 0) {
                            for (int i = 0; i < jsonArray.length(); i++) {
                                JSONObject supportObject = jsonArray.getJSONObject(i);
                                SupportData supportData = new SupportData(jsonObjectMain.optInt("count"), supportObject);
                                chatIdResp = supportObject.getString("chat_id");
                                mChatId = Integer.valueOf(supportObject.getString("id"));
                                mSupportAdapter.addItem(mSupportAdapter.getmData().size(), supportData);
                            }
                            showHideListHeader();
                            if (mSupportAdapter.getmData().size() > 4) {
                                if (mSupportList.getLastVisiblePosition() > mSupportAdapter.getmData().size() - 4) {
                                    hasScreenFirstView = false;
                                }
                            }
                        } else {
                            chatIdResp = jsonObjectMain.getString("chat_id");
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                } else {
                    //Log.e("else", "no tag" + response);
                }

            } else if (jsonObjectMain != null && jsonObjectMain.has("isonline")) {
                //Log.e("isonline", "" + jsonObjectMain);
                isSupportScreenPaused = false;
                hasScreenFirstView = true;
                try {
                    if (jsonObjectMain.getBoolean("isonline")) {
                        try {
                            chatSupportActivity = (ChatSupportActivity) getActivity();
                            chatSupportActivity.setToolBarTitle("online");
                        } catch (Exception e) {

                        }

                    } else {
                        try {
                            chatSupportActivity = (ChatSupportActivity) getActivity();
                            chatSupportActivity.setToolBarTitle("offline");
                        } catch (Exception e) {

                        }
                    }
                    if (mSupportAdapter.getmData().size() > 4) {
                        if (mSupportList.getLastVisiblePosition() > mSupportAdapter.getmData().size() - 4) {
                            hasScreenFirstView = true;
                        }
                    }
                } catch (Exception e) {
                    //Log.e("isonline", "error " + e);
                }
            } else if (mLockatedVolleyRequestQueue.getLockatedJSONObjectRequest().getTag().toString().equals(SEND_MESSAGE)) {

                isSupportScreenPaused = false;
                hasScreenFirstView = false;
                JSONObject jsonObject;
                try {
                    if (response instanceof String) {
                        jsonObject = new JSONObject(response.toString());
                    } else {
                        jsonObject = (JSONObject) response;
                    }
                    JSONObject messageObject = jsonObject.getJSONObject("message");
                    SupportData supportData = new SupportData(jsonObject.optInt("count"), messageObject);
                    mSupportAdapter.addItem(mSupportAdapter.getmData().size(), supportData);
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            } else if (mLockatedVolleyRequestQueue.getLockatedJSONObjectRequest().getTag().toString().equals(OPERATOR_TYPING)) {
                isSupportScreenPaused = false;
                hasScreenFirstView = false;
            }

            getConversationInLoop();

            Collections.sort(mSupportAdapter.getmData(), new Comparator<SupportData>() {
                @Override
                public int compare(SupportData lhs, SupportData rhs) {
                    return lhs.getUpdated_at().compareTo(rhs.getUpdated_at());
                }
            });
            if (!hasScreenFirstView) {
                hasScreenFirstView = true;
                mSupportList.setSelection(mSupportAdapter.getmData().size() + 1);
            }
        }
    }

    private void onSampleMessageClicked() {
        if (!isSampleMessageSelected) {
            isSampleMessageSelected = true;
            mSupportListSample.setVisibility(View.VISIBLE);
            mImageViewSampleMessage.setImageResource(R.drawable.ic_sample_message);
            hideSoftKeyboard();
        } else {
            isSampleMessageSelected = false;
            mSupportListSample.setVisibility(View.GONE);
            mImageViewSampleMessage.setImageResource(R.drawable.ic_sample_message_disable);
        }
    }

    public void hideSoftKeyboard() {
        if (getActivity().getCurrentFocus() != null) {
            InputMethodManager inputMethodManager = (InputMethodManager) getActivity().getSystemService(getActivity().INPUT_METHOD_SERVICE);
            inputMethodManager.hideSoftInputFromWindow(getActivity().getCurrentFocus().getWindowToken(), 0);
        }
    }

    private void showHideListHeader() {
        if (mSupportAdapter.getmData().size() > 0) {
            if (mSupportAdapter.getmData().get(0).getMessage_count() > mSupportAdapter.getmData().size()) {
                mPageNumber = ++mPageNumber;
                mTextViewSupportHeaderPreviousMessages.setVisibility(View.VISIBLE);
            } else {
                mTextViewSupportHeaderPreviousMessages.setVisibility(View.GONE);
                mSupportList.removeHeaderView(mSupportHeaderPreviousMessages);
            }
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.mImageViewMessageSend:
                onSampleMessageSendClicked();
                break;
            case R.id.mImageViewSampleMessage:
                onSampleMessageClicked();
                break;
            case R.id.mTextViewSupportPreviousMessages:
                onPreviousMessagesClicked();
                break;
        }
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        String value = (String) parent.getItemAtPosition(position);
        mEditTextAddMessage.setText(value);
        mEditTextAddMessage.setSelection(mEditTextAddMessage.getText().toString().length());
    }

}