package com.android.lockated.crm.fragment.myzone;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.android.lockated.LockatedApplication;
import com.android.lockated.R;
import com.android.lockated.crm.adapters.BookFacilitiesAdapter;
import com.android.lockated.model.usermodel.Facilities.FacilityHistory.Amenity;
import com.android.lockated.network.ConnectionDetector;
import com.android.lockated.preferences.LockatedPreferences;
import com.android.lockated.request.LockatedRequestError;
import com.android.lockated.request.LockatedVolleyRequestQueue;
import com.android.lockated.utils.ApplicationURL;
import com.android.lockated.utils.Utilities;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.google.gson.Gson;

import org.json.JSONArray;

import org.json.JSONException;

import org.json.JSONObject;

import java.util.ArrayList;

public class BookFacilitiesFragment extends Fragment implements Response.Listener<JSONObject>, Response.ErrorListener {
    RecyclerView recyclerView;
    ProgressBar mProgressBarView;
    BookFacilitiesAdapter bookFacilitiesAdapter;
    LockatedPreferences mLockatedPreferences;
    ArrayList<Amenity> amenitiesList;
    TextView errorMsg;
    LockatedVolleyRequestQueue mLockatedVolleyRequestQueue;
    String REQUEST_FACILITIES = "BookFacilitiesFragment";
    String SHOW, CREATE, INDEX, UPDATE, EDIT;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View facilitiesView = inflater.inflate(R.layout.fragment_facilities, container, false);
        init(facilitiesView);
        return facilitiesView;
    }

    @Override
    public void onResume() {
        super.onResume();
        LockatedApplication.getInstance().trackScreenView(getString(R.string.bookFacilities));
        LockatedApplication.getInstance().trackEvent(getString(R.string.bookFacilities),
                getString(R.string.visited), getString(R.string.bookFacilities));
       /* amenitiesList.clear();
        loadView();*/
    }

    private void init(View facilitiesView) {
        amenitiesList = new ArrayList<>();
        mLockatedPreferences = new LockatedPreferences(getActivity());
        errorMsg = (TextView) facilitiesView.findViewById(R.id.errorMsg);
        mProgressBarView = (ProgressBar) facilitiesView.findViewById(R.id.mProgressBarView);
        recyclerView = (RecyclerView) facilitiesView.findViewById(R.id.recyclerViewElement);

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerView.setLayoutManager(linearLayoutManager);
        getFacilitiesRole();
        checkPermission();
        bookFacilitiesAdapter = new BookFacilitiesAdapter(getActivity(), CREATE, getChildFragmentManager(),
                amenitiesList);
        recyclerView.setAdapter(bookFacilitiesAdapter);

    }

    private void checkPermission() {
        if (SHOW!=null&&SHOW.equals("true")) {
            loadView();
        } else {
            recyclerView.setVisibility(View.GONE);
            errorMsg.setVisibility(View.VISIBLE);
            errorMsg.setText(R.string.no_permission_error);
        }
    }

    private void loadView() {
        if (ConnectionDetector.isConnectedToInternet(getActivity())) {
            mProgressBarView.setVisibility(View.VISIBLE);
            String url = ApplicationURL.getAmenitiesUrl + mLockatedPreferences.getSocietyId()
                    + "&token=" + mLockatedPreferences.getLockatedToken();
            mLockatedVolleyRequestQueue = LockatedVolleyRequestQueue.getInstance(getActivity());
            mLockatedVolleyRequestQueue.sendRequest(REQUEST_FACILITIES, Request.Method.GET, url, null, this, this);
        } else {
            Utilities.showToastMessage(getActivity(), getActivity().getResources().getString(R.string.internet_connection_error));
        }
    }

    @Override
    public void onResponse(JSONObject response) {
        mProgressBarView.setVisibility(View.GONE);
        JSONObject jsonObject = (JSONObject) response;
        try {
            if (response.has("amenities") && response.getJSONArray("amenities").length() > 0) {
                JSONArray jsonArray = jsonObject.getJSONArray("amenities");
                Gson gson = new Gson();
                for (int i = 0; i < jsonArray.length(); i++) {
                    Amenity amenity = gson.fromJson(jsonArray.getJSONObject(i).toString(), Amenity.class);
                    amenitiesList.add(amenity);
                }
            } else {
                recyclerView.setVisibility(View.GONE);
                errorMsg.setVisibility(View.VISIBLE);
                errorMsg.setText(R.string.no_data_error);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onErrorResponse(VolleyError error) {
        mProgressBarView.setVisibility(View.GONE);
        if (getActivity() != null) {
            LockatedRequestError.onRequestError(getActivity(), error);
        }
    }

    public String getFacilitiesRole() {
        String mySocietyRoles = getActivity().getResources().getString(R.string.blank_value);
        if (mLockatedPreferences.getRolesJson() != null && !mLockatedPreferences.getRolesJson().equals("blank")) {
            try {
                JSONObject noticeJsonObj = new JSONObject(mLockatedPreferences.getRolesJson());
                for (int i = 0; i < noticeJsonObj.getJSONArray(getActivity().getResources().getString(R.string.permissions_value)).length(); i++) {
                    JSONObject jsonObject1 = noticeJsonObj.getJSONArray(getActivity().getResources().getString(R.string.permissions_value)).getJSONObject(i);
                    if (jsonObject1.has(getActivity().getResources().getString(R.string.section_value))
                            && jsonObject1.getString(getActivity().getResources().getString(R.string.section_value)).equals("spree_facilities")) {
                        if (jsonObject1.getJSONObject(getActivity().getResources().getString(R.string.permission_value)).has(getActivity().getResources().getString(R.string.index_value))) {
                            mySocietyRoles = jsonObject1.getJSONObject(getActivity().getResources().getString(R.string.permission_value)).getString(getActivity().getResources().getString(R.string.index_value));
                            CREATE = jsonObject1.getJSONObject(getActivity().getResources().getString(R.string.permission_value)).getString("create");
                            INDEX = jsonObject1.getJSONObject(getActivity().getResources().getString(R.string.permission_value)).getString("index");
                            UPDATE = jsonObject1.getJSONObject(getActivity().getResources().getString(R.string.permission_value)).getString("update");
                            EDIT = jsonObject1.getJSONObject(getActivity().getResources().getString(R.string.permission_value)).getString("edit");
                            SHOW = jsonObject1.getJSONObject(getActivity().getResources().getString(R.string.permission_value)).getString("show");
                        }
                    }
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return mySocietyRoles;
    }

}
