package com.android.lockated.model;

public class UserRoles {

    String CREATE;
    String INDEX;
    String UPDATE;
    String EDIT;
    String SHOW;

    public String getCREATE() {
        return CREATE;
    }

    public void setCREATE(String CREATE) {
        this.CREATE = CREATE;
    }

    public String getINDEX() {
        return INDEX;
    }

    public void setINDEX(String INDEX) {
        this.INDEX = INDEX;
    }

    public String getUPDATE() {
        return UPDATE;
    }

    public void setUPDATE(String UPDATE) {
        this.UPDATE = UPDATE;
    }

    public String getEDIT() {
        return EDIT;
    }

    public void setEDIT(String EDIT) {
        this.EDIT = EDIT;
    }

    public String getSHOW() {
        return SHOW;
    }

    public void setSHOW(String SHOW) {
        this.SHOW = SHOW;
    }

}
