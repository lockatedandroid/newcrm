package com.android.lockated.crm.fragment.myzone;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import com.android.lockated.R;
import com.android.lockated.crm.activity.MyZoneClassifiedActivity;
import com.android.lockated.crm.adapters.CommanListViewAdapter;


public class ClassifidesFragment extends Fragment implements AdapterView.OnItemClickListener {

    ListView ClassifiedList;
    String[] nameList = {"All Classifieds", "My Classifieds"};
    CommanListViewAdapter commanListViewAdapter;

    public ClassifidesFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View ClassifidesFragmentView = inflater.inflate(R.layout.fragment_classifides, container, false);
        ClassifiedList = (ListView) ClassifidesFragmentView.findViewById(R.id.ClassifiedList);
        commanListViewAdapter = new CommanListViewAdapter(getActivity(), nameList);
        ClassifiedList.setAdapter(commanListViewAdapter);
        ClassifiedList.setOnItemClickListener(this);
        return ClassifidesFragmentView;
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

        Intent intent = new Intent(getContext(), MyZoneClassifiedActivity.class);
        intent.putExtra("Listposition", position);
        startActivity(intent);

    }
}
