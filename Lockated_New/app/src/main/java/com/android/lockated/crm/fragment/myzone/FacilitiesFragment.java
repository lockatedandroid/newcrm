package com.android.lockated.crm.fragment.myzone;


import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import com.android.lockated.R;
import com.android.lockated.crm.activity.FacilitiesActivity;
import com.android.lockated.crm.adapters.CommanListViewAdapter;

public class FacilitiesFragment extends Fragment implements AdapterView.OnItemClickListener {

    ListView listNotice;
    String[] nameList = {"Book Facilities", "View History"};
    CommanListViewAdapter commanListViewAdapter;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View allNoticeView = inflater.inflate(R.layout.comman_listview, container, false);
        init(allNoticeView);
        return allNoticeView;
    }

    private void init(View allNoticeView) {
        listNotice = (ListView) allNoticeView.findViewById(R.id.noticeList);
        commanListViewAdapter = new CommanListViewAdapter(getActivity(), nameList);
        listNotice.setAdapter(commanListViewAdapter);
        listNotice.setOnItemClickListener(this);
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        Intent intent = new Intent(getActivity(), FacilitiesActivity.class);
        intent.putExtra("FacilitiesActivity", position);
        startActivity(intent);
    }

}
