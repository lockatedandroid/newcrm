
package com.android.lockated.model.usermodel.Polls.AllPolls;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class AllPolls {

    @SerializedName("polls")
    @Expose
    private ArrayList<AllPoll> allPolls = new ArrayList<AllPoll>();

    /**
     * 
     * @return
     *     The allPolls
     */
    public ArrayList<AllPoll> getAllPolls() {
        return allPolls;
    }

    /**
     * 
     * @param allPolls
     *     The all_polls
     */
    public void setAllPolls(ArrayList<AllPoll> allPolls) {
        this.allPolls = allPolls;
    }

}
