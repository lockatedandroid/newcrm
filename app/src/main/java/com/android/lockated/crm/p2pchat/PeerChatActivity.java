package com.android.lockated.crm.p2pchat;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;

import com.android.lockated.R;

public class PeerChatActivity extends AppCompatActivity {

    int chatId = 0;
    String userName = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat_support);

        if (getIntent().getExtras() != null) {
            chatId = Integer.valueOf(getIntent().getExtras().getString("userId"));
            userName = getIntent().getExtras().getString("userName");
        }

        actionBar(userName);

        if (savedInstanceState == null) {
            PeerChatFragment peerChatFragment = new PeerChatFragment();
            peerChatFragment.setChatId(chatId, userName);
            getSupportFragmentManager().beginTransaction().add(R.id.mChatContainer, peerChatFragment).commit();
        }
    }

    public void actionBar(String name) {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(true);
        getSupportActionBar().setTitle(name);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();

        if (id == android.R.id.home) {
            onBackPressed();
            return true;
        } else {
            return false;
        }
    }

    @Override
    public void onBackPressed() {
        Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.mChatContainer);
        if (fragment instanceof PeerChatFragment) {
            if (((PeerChatFragment) fragment).isSampleMessageSelected) {
                ((PeerChatFragment) fragment).isSampleMessageSelected = false;
                ((PeerChatFragment) fragment).mImageViewSampleMessage.setImageResource(R.drawable.ic_sample_message_disable);
            } else {
                super.onBackPressed();
            }
        } else {
            super.onBackPressed();
        }
    }
}
