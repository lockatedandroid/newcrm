package com.android.lockated.crm.adapters;

import android.content.Context;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.android.lockated.R;
import com.android.lockated.information.AccountController;
import com.android.lockated.model.AccountData;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.ArrayList;


public class AboutSocietyAmenitiesAdapter extends RecyclerView.Adapter<AboutSocietyAmenitiesAdapter.SocietyViewHolder> {

    Context contextMain;
    JSONArray jsonArray;
    AccountController accountController;
    ArrayList<AccountData> datas;

    public AboutSocietyAmenitiesAdapter(Context context, JSONArray jsonArray) {
        this.contextMain = context;
        this.jsonArray = jsonArray;
        accountController = AccountController.getInstance();
        datas = accountController.getmAccountDataList();
    }

    @Override
    public SocietyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(contextMain).inflate(R.layout.society_amenities, parent, false);
        SocietyViewHolder pvh = new SocietyViewHolder(v);
        return pvh;
    }

    @Override
    public void onBindViewHolder(SocietyViewHolder holder, int position) {

        try {
            holder.amenitiesName.setText(jsonArray.getJSONObject(position).getString("name"));
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    @Override
    public int getItemCount() {
        return jsonArray.length();
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }

    public class SocietyViewHolder extends RecyclerView.ViewHolder {

        TextView amenitiesName;

        public SocietyViewHolder(View itemView) {
            super(itemView);

            amenitiesName = (TextView) itemView.findViewById(R.id.amenitiesName);

        }
    }

}
