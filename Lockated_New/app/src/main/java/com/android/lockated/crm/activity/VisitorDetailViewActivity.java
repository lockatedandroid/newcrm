package com.android.lockated.crm.activity;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.android.lockated.HomeActivity;
import com.android.lockated.R;
import com.android.lockated.model.PurposeArray.PurposesArray;
import com.android.lockated.network.ConnectionDetector;
import com.android.lockated.preferences.LockatedPreferences;
import com.android.lockated.request.LockatedJSONObjectRequest;
import com.android.lockated.request.LockatedRequestError;
import com.android.lockated.request.LockatedVolleyRequestQueue;
import com.android.lockated.utils.ApplicationURL;
import com.android.lockated.utils.DatePickerFragment;
import com.android.lockated.utils.MarshMallowPermission;
import com.android.lockated.utils.ShowImage;
import com.android.lockated.utils.TimePickerFragment;
import com.android.lockated.utils.Utilities;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

public class VisitorDetailViewActivity extends AppCompatActivity implements View.OnClickListener, Response.ErrorListener, Response.Listener<JSONObject> {
    public static final String REQUEST_TAG = "VisitorDetailViewActivity";
    static final int REQUEST_CAMERA_PHOTO = 3;
    static final int REQUEST_TAKE_PHOTO = 100;
    static final int CHOOSE_IMAGE_REQUEST = 101;
    static final int REQUEST_CAMERA = 102;
    static final int REQUEST_STORAGE = 103;
    ProgressDialog mProgressDialog;
    String strExpDate, approve_id;
    String spid, approve;
    String doc_id;
    String strUrl;
    String striName;
    String striPurpose;
    String striVehicleNumber;
    String striMember;
    String strImage;
    String striContactNumber;
    boolean imageSet;
    ProgressBar progressBar;
    String encodedImage, mCurrentPhotoPath;
    MarshMallowPermission marshMallowPermission;
    String SHOW, CREATE, INDEX, UPDATE, EDIT, DESTROY;
    private EditText mVisitorName;
    private EditText mAdditionalMember;
    private EditText mContactNumber;
    private TextView mTextDate;
    private TextView mTextTime;
    private TextView mApprove;
    private TextView textVisitorImage;
    private TextView mAccompany;
    private TextView mReject;
    private ImageView mImageExpectedTime;
    private ImageView mImageExpectedDate;
    private TextView mSubmit;
    private EditText mVisitTo;
    private EditText mVisitPurpose;
    private EditText mVehiclrNumber;
    private ImageView mVisitorImage;
    private RequestQueue mQueue;
    ArrayList<PurposesArray> purposesArrays;
    private LockatedPreferences mLockatedPreferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // overridePendingTransition(R.anim.right_in, R.anim.left_out);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_visitor_detail_view);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(true);
        getSupportActionBar().setTitle("Visitor Corner");
        mLockatedPreferences = new LockatedPreferences(this);
        getGatekeeperRole();
        init();
        getVisitorDetails();
    }

    private void init() {
        spid = (getIntent().getExtras().getString("pid"));
        progressBar = (ProgressBar) findViewById(R.id.mProgressBarView);
        mVisitTo = (EditText) findViewById(R.id.mVisitTo);
        mVisitPurpose = (EditText) findViewById(R.id.mVisitPurpose);
        mVehiclrNumber = (EditText) findViewById(R.id.mVehiclrNumber);
        mVisitorName = (EditText) findViewById(R.id.mVisitorName);
        mAdditionalMember = (EditText) findViewById(R.id.mAdditionalMember);
        mContactNumber = (EditText) findViewById(R.id.mContactNumber);
        mTextDate = (TextView) findViewById(R.id.mTextDate);
        mTextTime = (TextView) findViewById(R.id.mTextTime);
        mApprove = (TextView) findViewById(R.id.mApprove);
        mReject = (TextView) findViewById(R.id.mReject);
        mAccompany = (TextView) findViewById(R.id.mAccompany);
        mImageExpectedDate = (ImageView) findViewById(R.id.mImageExpectedDate);
        mImageExpectedTime = (ImageView) findViewById(R.id.mImageExpectedTime);
        textVisitorImage = (TextView) findViewById(R.id.textVisitorImage);
        mSubmit = (TextView) findViewById(R.id.mSubmit);
        mVisitorImage = (ImageView) findViewById(R.id.mVisitorImage);
        if (spid != null) {
            mSubmit.setVisibility(View.VISIBLE);
            mApprove.setVisibility(View.GONE);
            mReject.setVisibility(View.GONE);
            mAccompany.setVisibility(View.GONE);
          /*  visitto.setVisibility(View.VISIBLE);
            mVisitTo.setVisibility(View.VISIBLE);*/
            mVisitPurpose.setEnabled(true);
            mVehiclrNumber.setEnabled(true);
            mVisitorName.setEnabled(true);
            mAdditionalMember.setEnabled(true);
            mContactNumber.setEnabled(true);
            //  getSetEditData();
        } else {
            mVisitorImage.setEnabled(false);
            mApprove.setVisibility(View.VISIBLE);
            mAccompany.setVisibility(View.VISIBLE);
            mReject.setVisibility(View.VISIBLE);
            mSubmit.setVisibility(View.GONE);
        }
        mVisitorImage.setOnClickListener(this);
        mImageExpectedTime.setOnClickListener(this);
        mImageExpectedDate.setOnClickListener(this);
        mSubmit.setOnClickListener(this);
        mApprove.setOnClickListener(this);
        mReject.setOnClickListener(this);
        mAccompany.setOnClickListener(this);
    }


    private void gatekeeperApi(String gateKeeper_id) {
        mProgressDialog = ProgressDialog.show(this, "", "Please Wait...", false);
        if (ConnectionDetector.isConnectedToInternet(getApplicationContext())) {
            mQueue = LockatedVolleyRequestQueue.getInstance(getApplicationContext()).getRequestQueue();
            String url = "https://www.lockated.com/gatekeepers/" + gateKeeper_id + ".json?token=" + mLockatedPreferences.getLockatedToken()
                    + "&id_society=" + mLockatedPreferences.getSocietyId();
            LockatedJSONObjectRequest lockatedJSONObjectRequest = new LockatedJSONObjectRequest(Request.Method.GET,
                    url, null, this, this);
            lockatedJSONObjectRequest.setTag(REQUEST_TAG);
            mQueue.add(lockatedJSONObjectRequest);
        } else {
            Utilities.showToastMessage(getApplicationContext(), getApplicationContext().getResources().getString(R.string.internet_connection_error));
        }
    }

    /* private void getSetEditData() {
         mVisitorName.setText(getIntent().getExtras().getString("strName"));
         //  mVisitTo.setText(getIntent().getExtras().getString("strVisitTO"));
         mVisitPurpose.setText(getIntent().getExtras().getString("strPurpose"));
         String strVehiclenumber = getIntent().getExtras().getString("strVehicleNumber");
         if (strVehiclenumber != null) {
             mVehiclrNumber.setText(strVehiclenumber);
         } else {
             mVehiclrNumber.setText(" ");
         }
         String strAdditionalMember = getIntent().getExtras().getString("strMember");
         Log.e("strAdditionalMember", "" + strAdditionalMember);
         if (strAdditionalMember != null) {
             mAdditionalMember.setText(strAdditionalMember);
         } else {
             mAdditionalMember.setText("0");
         }
         mContactNumber.setText(getIntent().getExtras().getString("strNumber"));
         String strExpDate = getIntent().getExtras().getString("strExpDate");
       *//*  if (strExpDate.equals("null")) {
            mTextDate.setText("No Date");
        } else {
            mTextDate.setText(strExpDate);
        }*//*
    }
*/
    private void getVisitorDetails() {
        String gateKeeper_id = mLockatedPreferences.getGateKeeper_id();
        if (mLockatedPreferences.getGateKeeper_id() != null && !mLockatedPreferences.getGateKeeper_id().equals("blank")) {
            if (INDEX != null && INDEX.equals("true")) {
                approve_id = mLockatedPreferences.getGateKeeper_id();
                gatekeeperApi(mLockatedPreferences.getGateKeeper_id());
            } else {
                mLockatedPreferences.setGateKeeper_id("blank");
                Intent intent = new Intent(getApplicationContext(), HomeActivity.class);
                startActivity(intent);
                finish();
            }

        } else {
            if (getIntent().getExtras() != null) {
                mVisitorName.setText(getIntent().getExtras().getString("strName"));
                mVisitPurpose.setText(getIntent().getExtras().getString("strPurpose"));
                String strAdditionalMember = getIntent().getExtras().getString("strMember");
                if (strAdditionalMember != null && !strAdditionalMember.equals("null")) {
                    mAdditionalMember.setText(strAdditionalMember);
                } else {
                    mAdditionalMember.setText("0");
                }
                String strVehiclenumber = getIntent().getExtras().getString("strVehicleNumber");
                mVehiclrNumber.setText(strVehiclenumber);
                mContactNumber.setText(getIntent().getExtras().getString("strNumber"));
                String strExpDate = getIntent().getExtras().getString("strExpDate");
                if (strExpDate != null) {
                    mTextDate.setText(Utilities.ddMMYYYFormat(strExpDate));
                    String time = (Utilities.ddMonYYYYTimeFormat(strExpDate));
                    String[] separated = time.split(" ");
                    mTextTime.setText((separated[1]));
                } else {
                    mTextDate.setText("No Date");
                    mTextTime.setText("No Time");
                }
                approve_id = getIntent().getExtras().getString("id");
                approve = getIntent().getExtras().getString("approve");
                if (approve.equals("0")) {
                    mApprove.setVisibility(View.VISIBLE);
                    mReject.setVisibility(View.VISIBLE);
                    mAccompany.setVisibility(View.VISIBLE);
                } else {
                    mApprove.setVisibility(View.GONE);
                    mReject.setVisibility(View.GONE);
                    mAccompany.setVisibility(View.GONE);
                }
                doc_id = getIntent().getExtras().getString("doc_id");
                strUrl = getIntent().getExtras().getString("visitorImage");
                if (!strUrl.equals("No Document") && !doc_id.equals("No Id")) {
                    doc_id = getIntent().getExtras().getString("doc_id");
                    Picasso.with(this).load(strUrl).fit().into(mVisitorImage);
                } else {
                    doc_id = "";
                    encodedImage = "";
                    mVisitorImage.setVisibility(View.GONE);
                    textVisitorImage.setVisibility(View.VISIBLE);
                }
            }
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.mSubmit:
                onUpdateDetails();
                break;
            case R.id.mApprove:
                onApproveClicked(approve_id);
                break;
            case R.id.mAccompany:
                onAccompanyClicked(approve_id);
                break;
            case R.id.mReject:
                onRejectClicked(approve_id);
                break;
            case R.id.mImageExpectedDate:
                datepicker(mTextDate);
                break;
            case R.id.mImageExpectedTime:
                selectTime(mTextTime);
                break;
            case R.id.mVisitorImage:
                if (!imageSet) {
                    selectImage();
                } else {
                    selectImageAction();
                }
                break;
        }
    }

    private void onRejectClicked(String approve_id) {
        mProgressDialog = ProgressDialog.show(this, "", "Please Wait...", false);
        mProgressDialog.show();
        JSONObject jsonObjectMain = new JSONObject();
        try {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("approve", "2");
            jsonObjectMain.put("gatekeeper", jsonObject);

        } catch (JSONException e) {
            e.printStackTrace();
        }
        mQueue = LockatedVolleyRequestQueue.getInstance(this).getRequestQueue();
        String url = ApplicationURL.UpdateGateKeeper + approve_id + ".json?token="
                + mLockatedPreferences.getLockatedToken();
        LockatedJSONObjectRequest lockatedJSONObjectRequest = new LockatedJSONObjectRequest(Request.Method.PUT,
                url,
                jsonObjectMain, this, this);
        lockatedJSONObjectRequest.setTag(REQUEST_TAG);
        mQueue.add(lockatedJSONObjectRequest);
    }

    private void onAccompanyClicked(String approve_id) {
        mProgressDialog = ProgressDialog.show(this, "", "Please Wait...", false);
        mProgressDialog.show();
        JSONObject jsonObjectMain = new JSONObject();
        try {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("approve", "1");
            jsonObject.put("accompany", "1");
            jsonObjectMain.put("gatekeeper", jsonObject);

        } catch (JSONException e) {
            e.printStackTrace();
        }
        mQueue = LockatedVolleyRequestQueue.getInstance(this).getRequestQueue();
        String url = ApplicationURL.UpdateGateKeeper + approve_id + ".json?token="
                + mLockatedPreferences.getLockatedToken();
        LockatedJSONObjectRequest lockatedJSONObjectRequest = new LockatedJSONObjectRequest(Request.Method.PUT,
                url,
                jsonObjectMain, this, this);
        lockatedJSONObjectRequest.setTag(REQUEST_TAG);
        mQueue.add(lockatedJSONObjectRequest);
    }

    private void onApproveClicked(String approve_id) {
        mProgressDialog = ProgressDialog.show(this, "", "Please Wait...", false);
        mProgressDialog.show();
        JSONObject jsonObjectMain = new JSONObject();
        try {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("approve", "1");
            jsonObjectMain.put("gatekeeper", jsonObject);

        } catch (JSONException e) {
            e.printStackTrace();
        }
        mQueue = LockatedVolleyRequestQueue.getInstance(this).getRequestQueue();
        String url = ApplicationURL.UpdateGateKeeper + approve_id + ".json?token="
                + mLockatedPreferences.getLockatedToken();
        LockatedJSONObjectRequest lockatedJSONObjectRequest = new LockatedJSONObjectRequest(Request.Method.PUT,
                url,
                jsonObjectMain, this, this);
        lockatedJSONObjectRequest.setTag(REQUEST_TAG);
        mQueue.add(lockatedJSONObjectRequest);
    }

    private void selectTime(TextView time) {
        TimePickerFragment timePickerFragment = new TimePickerFragment();
        timePickerFragment.setTimePickerView(time);
        DialogFragment newFragment = timePickerFragment;
        newFragment.show(this.getSupportFragmentManager(), "TimePicker");
    }

    private void selectImageAction() {
        final CharSequence[] options = {"View Image", "Change Image", "Remove Image"};
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setItems(options, new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int item) {
                if (options[item].equals("View Image")) {
                    Intent displayImage = new Intent(getApplicationContext(), ShowImage.class);
                    String mImageUrl = mLockatedPreferences.getImageAvatar();
                    displayImage.putExtra("imageUrlString", mImageUrl);
                    startActivity(displayImage);
                } else if (options[item].equals("Change Image")) {
                    selectImage();
                } else {
                    mVisitorImage.setImageResource(R.drawable.ic_account_camera);
                    encodedImage = "";
                    imageSet = false;
                }
            }

        });
        builder.show();

    }

    private void selectImage() {
        final CharSequence[] options = {"Camera", "Gallery"};
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Add Photo!");
        builder.setItems(options, new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int item) {
                if (options[item].equals("Camera")) {
                    checkCameraPermission();
                } else if (options[item].equals("Gallery")) {
                    checkStoragePermission();
                }
            }

        });

        builder.show();
    }

    private void checkStoragePermission() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                requestPermissions(new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, REQUEST_STORAGE);
            } else {
                onGalleryClicked();
            }
        } else {
            onGalleryClicked();
        }
    }

    private void onGalleryClicked() {
        Intent i = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(i, CHOOSE_IMAGE_REQUEST);
    }

    private void checkCameraPermission() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
                requestPermissions(new String[]{Manifest.permission.CAMERA}, REQUEST_CAMERA);
            } else {
                onCameraClicked();
            }
        } else {
            onCameraClicked();
        }
    }

    private void onCameraClicked() {
        if (!marshMallowPermission.checkPermissionForCamera()) {
            marshMallowPermission.requestPermissionForCamera();
        } else {
            if (!marshMallowPermission.checkPermissionForExternalStorage()) {
                marshMallowPermission.requestPermissionForExternalStorage();
            } else {
                Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                if (takePictureIntent.resolveActivity(this.getPackageManager()) != null) {
                    File photoFile = null;
                    try {
                        photoFile = createImageFile();
                    } catch (IOException ex) {
                        ex.printStackTrace();
                    }

                    if (photoFile != null) {
                        takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(photoFile));
                        startActivityForResult(takePictureIntent, REQUEST_TAKE_PHOTO);
                    }
                }
            }
        }
    }

    private File createImageFile() throws IOException {
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss", Locale.ENGLISH).format(new Date());
        String imageFileName = "JPEG_" + timeStamp + "_";
        File storageDir = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES);
        File image = File.createTempFile(imageFileName, ".jpg", storageDir);
        mCurrentPhotoPath = image.getAbsolutePath();
        return image;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode != this.RESULT_CANCELED) {
            if (requestCode == REQUEST_TAKE_PHOTO && resultCode == this.RESULT_OK) {
                setPic();
            } else if (requestCode == CHOOSE_IMAGE_REQUEST && resultCode == this.RESULT_OK) {
                Uri selectedImageURI = data.getData();
                mCurrentPhotoPath = getPath(selectedImageURI);
                setPic();
            } else if (requestCode == REQUEST_CAMERA_PHOTO && resultCode == this.RESULT_OK) {
                Bitmap thumbnail = (Bitmap) data.getExtras().get("data");
                if (thumbnail != null) {
                    ByteArrayOutputStream bytes = new ByteArrayOutputStream();
                    File destination = new File(Environment.getExternalStorageDirectory(), System.currentTimeMillis() + ".jpg");
                    FileOutputStream fo;
                    try {
                        destination.createNewFile();
                        fo = new FileOutputStream(destination);
                        fo.write(bytes.toByteArray());
                        fo.close();
                    } catch (FileNotFoundException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    setCameraPic(thumbnail);
                }
            }
        }
    }

    private String getPath(Uri uri) {
        if (uri == null) {
            return null;
        }
        String[] projection = {MediaStore.Images.Media.DATA};
        Cursor cursor = this.getContentResolver().query(uri, projection, null, null, null);
        if (cursor != null) {
            int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
            cursor.moveToFirst();
            return cursor.getString(column_index);
        }
        return uri.getPath();
    }

    private void setCameraPic(Bitmap bitmap) {
        if (bitmap != null) {
            encodedImage = Utilities.encodeTobase64(bitmap);
            mVisitorImage.setImageBitmap(bitmap);
            imageSet = true;
        }
    }

    private void setPic() {
        int targetW = mVisitorImage.getWidth();
        int targetH = mVisitorImage.getHeight();
        BitmapFactory.Options bmOptions = new BitmapFactory.Options();
        bmOptions.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(mCurrentPhotoPath, bmOptions);
        int photoW = bmOptions.outWidth;
        int photoH = bmOptions.outHeight;
        int scaleFactor = Math.min(photoW / targetW, photoH / targetH);

        bmOptions.inJustDecodeBounds = false;
        bmOptions.inSampleSize = scaleFactor;
        bmOptions.inPurgeable = true;

        Bitmap bitmap = BitmapFactory.decodeFile(mCurrentPhotoPath, bmOptions);
        if (bitmap != null) {
            encodedImage = Utilities.encodeTobase64(bitmap);
            Log.w("setPic", encodedImage);
            mVisitorImage.setImageBitmap(bitmap);
            imageSet = true;
        }
    }

    private void datepicker(TextView mExpectedDate) {
        DatePickerFragment datePickerFragment = new DatePickerFragment();
        datePickerFragment.setDatePickerView(mExpectedDate);
        DialogFragment newFragment = datePickerFragment;
        newFragment.show(this.getSupportFragmentManager(), "DatePicker");
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            callintent();
            return true;
        } else {
            return false;
        }
    }

    private void onUpdateDetails() {
        striName = mVisitorName.getText().toString();
        striPurpose = mVisitPurpose.getText().toString();
        striVehicleNumber = mVehiclrNumber.getText().toString();
        if (striVehicleNumber.equals("Not Available")) {
            striVehicleNumber = "";
        } else {
            striVehicleNumber = mVehiclrNumber.getText().toString();
        }
        striMember = mAdditionalMember.getText().toString();
        String strEntryTime = mTextTime.getText().toString();
        striContactNumber = mContactNumber.getText().toString();
        strExpDate = mTextDate.getText().toString();
        if ((TextUtils.isEmpty(striName))) {
            Utilities.showToastMessage(getApplicationContext(), "Please Fill Name");
        } else if (TextUtils.isEmpty(striPurpose)) {
            Utilities.showToastMessage(getApplicationContext(), "Please Fill Purpose");
        } else if (TextUtils.isEmpty(striContactNumber)) {
            Utilities.showToastMessage(getApplicationContext(), "Please Fill Contact Number");
        } else if (strExpDate.equals("No Date")) {
            Utilities.showToastMessage(getApplicationContext(), "Please select date");
        } else if (strEntryTime.equals("No Time")) {
            Utilities.showToastMessage(getApplicationContext(), "Please select time");
        } else {
            mProgressDialog = ProgressDialog.show(this, "", "Please Wait...", false);
            mProgressDialog.show();
            if (ConnectionDetector.isConnectedToInternet(this)) {
                if (spid != null) {
                    JSONObject jsonObjectMain = new JSONObject();
                    JSONObject jsonObject = new JSONObject();
                    try {
                        jsonObject.put("guest_name", striName);
                        jsonObject.put("guest_number", striContactNumber);
                        jsonObject.put("guest_vehicle_number", striVehicleNumber);
                        jsonObject.put("visit_purpose", striPurpose);
                        jsonObject.put("plus_person", striMember);
                        jsonObject.put("expected_at", strExpDate + "T" + strEntryTime);
                        jsonObject.put("document_id", doc_id);
                        jsonObject.put("document", encodedImage);
                        jsonObjectMain.put("gatekeeper", jsonObject);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    try {
                        mQueue = LockatedVolleyRequestQueue.getInstance(this).getRequestQueue();
                        LockatedJSONObjectRequest lockatedJSONObjectRequest = new LockatedJSONObjectRequest(Request.Method.PUT,
                                ApplicationURL.UpdateGateKeeper + spid + ".json?token="
                                        + mLockatedPreferences.getLockatedToken(),
                                jsonObjectMain, this, this);
                        //Log.e("LogUrl", "" + ApplicationURL.UpdateGateKeeper + spid + ".json?token=" + mLockatedPreferences.getLockatedToken());
                        lockatedJSONObjectRequest.setTag(REQUEST_TAG);
                        mQueue.add(lockatedJSONObjectRequest);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            } else {
                Utilities.showToastMessage(this, this.getResources().getString(R.string.internet_connection_error));
            }
        }

    }

    @Override
    public void onErrorResponse(VolleyError error) {
            /*progressBar.setVisibility(View.GONE);*/
        mProgressDialog.dismiss();
        LockatedRequestError.onRequestError(this, error);
    }

    @Override
    public void onResponse(JSONObject response) {
        /*progressBar.setVisibility(View.GONE);*/
        mProgressDialog.dismiss();
        if (response != null && response.length() > 0) {
            if (response.has("id") && !mLockatedPreferences.getGateKeeper_id().equals("blank")) {
                try {
                    mVisitorName.setText(response.getString("guest_name"));
                    mVisitPurpose.setText(response.getString("visit_purpose"));
                    String strAdditionalMember = response.getString("plus_person");
                    if (strAdditionalMember != null) {
                        mAdditionalMember.setText(strAdditionalMember);
                    } else {
                        mAdditionalMember.setText("0");
                    }
                    String strVehiclenumber = response.getString("guest_vehicle_number");
                    mVehiclrNumber.setText(strVehiclenumber);
                    mContactNumber.setText(response.getString("guest_number"));
                    String strExpDate = response.getString("expected_at");
                    if (strExpDate != null) {
                        mTextDate.setText(Utilities.ddMMYYYFormat(strExpDate));
                        String time = (Utilities.ddMonYYYYTimeFormat(strExpDate));
                        String[] separated = time.split(" ");
                        mTextTime.setText((separated[1]));
                    } else {
                        mTextDate.setText("No Date");
                        mTextTime.setText("No Time");

                    }
                    approve = response.getString("approve");
                    if (approve.equals("0")) {
                        mApprove.setVisibility(View.VISIBLE);
                        mReject.setVisibility(View.VISIBLE);
                        mAccompany.setVisibility(View.VISIBLE);
                    } else {
                        mApprove.setVisibility(View.GONE);
                        mReject.setVisibility(View.GONE);
                        mAccompany.setVisibility(View.GONE);
                    }
                    if (response.getJSONArray("documents") != null && response.getJSONArray("documents").length() > 0) {
                        String strUrl = response.getJSONArray("documents").getJSONObject(0).getString("document");
                        Picasso.with(this).load(strUrl).fit().into(mVisitorImage);
                    } else {
                        encodedImage = "";
                        mVisitorImage.setVisibility(View.GONE);
                        textVisitorImage.setVisibility(View.VISIBLE);
                    }
                    mLockatedPreferences.setGateKeeper_id("blank");
                } catch (Exception e) {
                }
            } else if (response.has("id") && mLockatedPreferences.getGateKeeper_id().equals("blank")) {
                callintent();
            }
        }

    }

    private void callintent() {
        Intent backIntent = new Intent(getApplicationContext(), CrmVisitorActivity.class);
        backIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(backIntent);
        finish();
        //  overridePendingTransition(R.anim.left_in, R.anim.right_out);
    }

    @Override
    public void onBackPressed() {
        callintent();
    }

    public String getGatekeeperRole() {
        String mySocietyRoles = getApplicationContext().getResources().getString(R.string.blank_value);
        if (mLockatedPreferences.getRolesJson() != null && !mLockatedPreferences.getRolesJson().equals("blank")) {
            try {
                JSONObject noticeJsonObj = new JSONObject(mLockatedPreferences.getRolesJson());
                for (int i = 0; i < noticeJsonObj.getJSONArray(getApplicationContext().getResources().getString(R.string.permissions_value)).length(); i++) {
                    JSONObject jsonObject1 = noticeJsonObj.getJSONArray(getApplicationContext().getResources().getString(R.string.permissions_value)).getJSONObject(i);
                    if (jsonObject1.has(getApplicationContext().getResources().getString(R.string.section_value))
                            && jsonObject1.getString(getApplicationContext().getResources().getString(R.string.section_value)).equals("spree_visitors")) {
                        if (jsonObject1.getJSONObject(getApplicationContext().getResources().getString(R.string.permission_value)).has(getApplicationContext().getResources().getString(R.string.index_value))) {
                            mySocietyRoles = jsonObject1.getJSONObject(getApplicationContext().getResources().getString(R.string.permission_value)).getString(getApplicationContext().getResources().getString(R.string.index_value));
                            INDEX = jsonObject1.getJSONObject(getApplicationContext().getResources().getString(R.string.permission_value)).getString("index");
                        }
                    }
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return mySocietyRoles;
    }
}
