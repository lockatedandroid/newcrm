package com.android.lockated.model.usermodel.NoticeModel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class NoticeModel {

    @SerializedName("daily_feed")
    @Expose
    private Object dailyFeed;
    @SerializedName("noticeboards")
    @Expose
    private ArrayList<Noticeboard> noticeboards = new ArrayList<Noticeboard>();
    @SerializedName("shared")
    @Expose
    private ArrayList<Object> shared = new ArrayList<Object>();

    /**
     * @return The dailyFeed
     */
    public Object getDailyFeed() {
        return dailyFeed;
    }

    /**
     * @param dailyFeed The daily_feed
     */
    public void setDailyFeed(Object dailyFeed) {
        this.dailyFeed = dailyFeed;
    }

    /**
     * @return The noticeboards
     */
    public List<Noticeboard> getNoticeboards() {
        return noticeboards;
    }

    /**
     * @param noticeboards The noticeboards
     */
    public void setNoticeboards(ArrayList<Noticeboard> noticeboards) {
        this.noticeboards = noticeboards;
    }

    /**
     * @return The shared
     */
    public List<Object> getShared() {
        return shared;
    }

    /**
     * @param shared The shared
     */
    public void setShared(ArrayList<Object> shared) {
        this.shared = shared;
    }

}