package com.android.lockated.crmadmin.adapter;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.android.lockated.R;
import com.android.lockated.crmadmin.activity.AdminNoticeBoardActivity;
import com.android.lockated.crmadmin.activity.AdminNoticeDetailActivity;
import com.android.lockated.model.AdminModel.Notice.Noticeboard;
import com.android.lockated.network.ConnectionDetector;
import com.android.lockated.preferences.LockatedPreferences;
import com.android.lockated.request.LockatedJSONObjectRequest;
import com.android.lockated.request.LockatedRequestError;
import com.android.lockated.request.LockatedVolleyRequestQueue;
import com.android.lockated.utils.ApplicationURL;
import com.android.lockated.utils.Utilities;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class PendingNoticeAdapter extends RecyclerView.Adapter<PendingNoticeAdapter.PendingNoticeViewHolder> implements Response.ErrorListener, Response.Listener<JSONObject> {
    private final ArrayList<Noticeboard> noticeboardArrayListd;
    Context context;
    JSONArray jsonArray;
    private RequestQueue mQueue;
    FragmentManager childFragmentManager;
    ProgressDialog mProgressDialog;
    private LockatedPreferences mLockatedPreferences;
    public static final String REQUEST_TAG = "PendingNoticeAdapter";

    public PendingNoticeAdapter(Context context, ArrayList<Noticeboard> noticeboardArrayListd, FragmentManager childFragmentManager) {
        this.context = context;
        this.noticeboardArrayListd = noticeboardArrayListd;
        this.childFragmentManager = childFragmentManager;
    }

    @Override
    public PendingNoticeViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(context).inflate(R.layout.admin_pending_notice, parent, false);
        PendingNoticeViewHolder pvh = new PendingNoticeViewHolder(v);
        return pvh;
    }

    @Override
    public void onBindViewHolder(PendingNoticeViewHolder holder, final int position) {
        final Noticeboard noticeboard = noticeboardArrayListd.get(position);
        holder.subject.setText(noticeboard.getNoticeHeading());
        holder.description.setText(noticeboard.getNoticeText());
        holder.submittedOn.setText(Utilities.dateConvertMinify(noticeboard.getCreatedAt()));
        String firstName = noticeboard.getUser().getFirstname();
        String lastName = noticeboard.getUser().getLastname();
        final String id = String.valueOf(noticeboard.getId());
        holder.postedName.setText(firstName + " " + lastName);
        String strFlat = String.valueOf(noticeboard.getUserFlat().getFlat());
        if (strFlat.equals("null")) {
            holder.mFlat.setText("No Flat");
        } else {
            holder.mFlat.setText(strFlat);
        }
        holder.publishNotice.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                request(null, ApplicationURL.getPublishNoticeUrl + id);
            }
        });
        holder.rejectNotice.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                request(null, ApplicationURL.getRejectNoticeUrl + id);
            }
        });
        holder.viewDetail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                viewCardDetail(noticeboard);
            }
        });
    }

    @Override
    public int getItemCount() {
        return noticeboardArrayListd.size();
        //jsonArray.length();
    }

    public class PendingNoticeViewHolder extends RecyclerView.ViewHolder {
        CardView cardView;
        TextView subject, description, submittedOn, postedName, publishNotice, rejectNotice, viewDetail, mFlat;

        public PendingNoticeViewHolder(View itemView) {
            super(itemView);
            subject = (TextView) itemView.findViewById(R.id.subjectName1);
            description = (TextView) itemView.findViewById(R.id.postedName1);
            submittedOn = (TextView) itemView.findViewById(R.id.submissionDate);
            postedName = (TextView) itemView.findViewById(R.id.postedBy);
            publishNotice = (TextView) itemView.findViewById(R.id.publish);
            rejectNotice = (TextView) itemView.findViewById(R.id.reject);
            viewDetail = (TextView) itemView.findViewById(R.id.detailView);
            mFlat = (TextView) itemView.findViewById(R.id.mFlat);
        }
    }

    public void request(JSONObject jsonObject, String url) {
        if (context != null) {
            mProgressDialog = ProgressDialog.show(context, "", "Please Wait...", false);
            mProgressDialog.show();
            if (ConnectionDetector.isConnectedToInternet(context)) {
                mLockatedPreferences = new LockatedPreferences(context);
                String urlMain = url + "&token=" + mLockatedPreferences.getLockatedToken()
                        + "&id_society=" + mLockatedPreferences.getSocietyId();
                LockatedVolleyRequestQueue lockatedVolleyRequestQueue = LockatedVolleyRequestQueue.getInstance(context);
                lockatedVolleyRequestQueue.sendRequest(REQUEST_TAG, Request.Method.GET, urlMain, jsonObject, this, this);
                /*mQueue = LockatedVolleyRequestQueue.getInstance(context).getRequestQueue();
                LockatedJSONObjectRequest lockatedJSONObjectRequest = new LockatedJSONObjectRequest(Request.Method.GET,
                        url + "&token=" + mLockatedPreferences.getLockatedToken()
                                + "&id_society=" + mLockatedPreferences.getSocietyId(), jsonObject, this, this);
                lockatedJSONObjectRequest.setTag(REQUEST_TAG);
                mQueue.add(lockatedJSONObjectRequest);*/
            } else {
                Utilities.showToastMessage(context, context.getResources().getString(R.string.internet_connection_error));
            }
        }
    }

    @Override
    public void onErrorResponse(VolleyError error) {
      mProgressDialog.dismiss();
        if (context != null) {
            LockatedRequestError.onRequestError(context, error);
        }
    }

    @Override
    public void onResponse(JSONObject response) {
        mProgressDialog.dismiss();
        if (context != null) {
            try {
                if (response != null && response.length() > 0) {
                    Intent intent = new Intent(context, AdminNoticeBoardActivity.class);
                    intent.putExtra("AdminNotice", 0);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    context.startActivity(intent);
                } else {
                }

            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public void viewCardDetail(Noticeboard noticeboard) {
        JSONObject jsonObjectMain = new JSONObject();
        try {
            jsonObjectMain.put("event_type", "Notice");
            jsonObjectMain.put("created_by", noticeboard.getUser().getFirstname() + " "
                    + noticeboard.getUser().getLastname());
            jsonObjectMain.put("notice_heading", noticeboard.getNoticeHeading());
            jsonObjectMain.put("notice_text", noticeboard.getNoticeText());
            jsonObjectMain.put("created_at", noticeboard.getCreatedAt());
            jsonObjectMain.put("id", noticeboard.getId());
            jsonObjectMain.put("VisiblityValue", "0");
            String date;
            if (noticeboard.getExpireTime() != null && noticeboard.getExpireTime() != "null") {
                date = Utilities.dateConvertMinify(noticeboard.getExpireTime());
            } else {
                date = "No Date";
            }
            jsonObjectMain.putOpt("expire_time", date);
            /*jsonObjectMain.put("expire_time", noticeboard.getExpireTime());*/
            jsonObjectMain.put("flat", "" + noticeboard.getUserFlat().getFlat());
            jsonObjectMain.put("username", noticeboard.getUser().getFirstname() + " "
                    + noticeboard.getUser().getLastname()/* + " "
                    + jsonObject.getJSONObject("user").getString("mobile")*/);
            if (noticeboard.getDocuments().size() > 0 && noticeboard.getDocuments() != null) {
                jsonObjectMain.putOpt("document", noticeboard.getDocuments().get(0).getDocument());
            } else {
                jsonObjectMain.putOpt("document", "No Document");
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        Intent intent = new Intent(context, AdminNoticeDetailActivity.class);
        intent.putExtra("NoticeDetailViewData", jsonObjectMain.toString());
        context.startActivity(intent);
    }
}
