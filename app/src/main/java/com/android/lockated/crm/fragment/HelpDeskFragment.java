package com.android.lockated.crm.fragment;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.TextView;

import com.android.lockated.LockatedApplication;
import com.android.lockated.R;
import com.android.lockated.crm.activity.HelpDeskActivity;
import com.android.lockated.crm.adapters.HelpDeskAdapter;
import com.android.lockated.model.usermodel.HelpDesk.Complaint;
import com.android.lockated.network.ConnectionDetector;
import com.android.lockated.preferences.LockatedPreferences;
import com.android.lockated.request.LockatedJSONObjectRequest;
import com.android.lockated.request.LockatedRequestError;
import com.android.lockated.request.LockatedVolleyRequestQueue;
import com.android.lockated.utils.ApplicationURL;
import com.android.lockated.utils.Utilities;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class HelpDeskFragment extends Fragment implements View.OnClickListener, Response.ErrorListener, Response.Listener<JSONObject> {
    FloatingActionButton fab;
    ListView listView;
    TextView errorMsg;
    /*ProgressBar progressBar;*/
    ProgressDialog mProgressDialog;
    HelpDeskAdapter helpDeskAdapter;
    private RequestQueue mQueue;
    ArrayList<Complaint> arrayComplaints;
    private LockatedPreferences mLockatedPreferences;
    public static final String REQUEST_TAG = "HelpDeskFragment";
    String SHOW, CREATE, INDEX, UPDATE, EDIT;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View HelpDeskFragmentView = inflater.inflate(R.layout.fragment_help_desk, container, false);
        Utilities.ladooIntegration(getActivity(), getActivity().getResources().getString(R.string.helpdesk));
        init(HelpDeskFragmentView);
        return HelpDeskFragmentView;
    }

    @Override
    public void onResume() {
        super.onResume();
        LockatedApplication.getInstance().trackScreenView(getString(R.string.helpdesk));
        Utilities.lockatedGoogleAnalytics(getString(R.string.helpdesk),
                getString(R.string.visited), getString(R.string.helpdesk));
        /*arrayComplaints.clear();
        getHelpDesk();*/
    }

    private void init(View helpDeskFragmentView) {
        arrayComplaints = new ArrayList<>();
        errorMsg = (TextView) helpDeskFragmentView.findViewById(R.id.noNotices);
        listView = (ListView) helpDeskFragmentView.findViewById(R.id.listView);
        /*progressBar = (ProgressBar) helpDeskFragmentView.findViewById(R.id.mProgressBarView);*/
        mLockatedPreferences = new LockatedPreferences(getActivity());
        fab = (FloatingActionButton) helpDeskFragmentView.findViewById(R.id.fab);
        fab.setOnClickListener(this);
        getHelpDeskRole();
        checkPermission();
        helpDeskAdapter = new HelpDeskAdapter(getActivity(), arrayComplaints, getChildFragmentManager());
        listView.setAdapter(helpDeskAdapter);
    }

    private void checkPermission() {
        if (CREATE!=null&&CREATE.equals("true")) {
            fab.setVisibility(View.VISIBLE);
        } else {
            fab.setVisibility(View.GONE);
        }
        if (SHOW!=null&&SHOW.equals("true")) {
            getHelpDesk();
        } else {
            listView.setVisibility(View.GONE);
            errorMsg.setVisibility(View.VISIBLE);
            errorMsg.setText(R.string.no_permission_error);
        }
    }

    private void getHelpDesk() {
        if (getActivity() != null) {
            if (ConnectionDetector.isConnectedToInternet(getActivity())) {
                mProgressDialog = ProgressDialog.show(getActivity(), "", "Please Wait...", false);
                mProgressDialog.show();
                mQueue = LockatedVolleyRequestQueue.getInstance(getActivity()).getRequestQueue();
                String url = ApplicationURL.getHelpdeskUrl + mLockatedPreferences.getLockatedToken();
                //Log.e("HelpDeskUrl", "" + url);
                LockatedJSONObjectRequest lockatedJSONObjectRequest = new LockatedJSONObjectRequest(Request.Method.GET,
                        url, null, this, this);
                lockatedJSONObjectRequest.setTag(REQUEST_TAG);
                mQueue.add(lockatedJSONObjectRequest);
            } else {
                Utilities.showToastMessage(getActivity(), getActivity().getResources().getString(R.string.internet_connection_error));
            }
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.fab:
                Intent intent = new Intent(getActivity(), HelpDeskActivity.class);
                startActivity(intent);
                break;
        }
    }

    @Override
    public void onResponse(JSONObject response) {
        mProgressDialog.dismiss();
        if (getActivity() != null) {
            try {
                if (response != null && response.length() > 0) {
                    if (response.getJSONArray("complaints").length() > 0) {
                        JSONArray jsonArray = response.getJSONArray("complaints");
                        Gson gson = new Gson();
                        for (int i = 0; i < jsonArray.length(); i++) {
                            Complaint complaint = gson.fromJson(jsonArray.getJSONObject(i).toString(), Complaint.class);
                            arrayComplaints.add(complaint);
                        }
                        helpDeskAdapter.notifyDataSetChanged();
                    } else {
                        listView.setVisibility(View.GONE);
                        errorMsg.setVisibility(View.VISIBLE);
                        errorMsg.setText(R.string.no_data_error);
                    }
                } else {
                    listView.setVisibility(View.GONE);
                    errorMsg.setVisibility(View.VISIBLE);
                    errorMsg.setText(R.string.no_data_error);
                }

            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void onErrorResponse(VolleyError error) {
        /*progressBar.setVisibility(View.GONE);*/
        mProgressDialog.dismiss();
        if (getActivity() != null) {
            LockatedRequestError.onRequestError(getActivity(), error);
        }
    }

    public String getHelpDeskRole() {
        String mySocietyRoles = getActivity().getResources().getString(R.string.blank_value);
        if (mLockatedPreferences.getRolesJson() != null && !mLockatedPreferences.getRolesJson().equals("blank")) {
            try {
                JSONObject noticeJsonObj = new JSONObject(mLockatedPreferences.getRolesJson());
                for (int i = 0; i < noticeJsonObj.getJSONArray(getActivity().getResources().getString(R.string.permissions_value)).length(); i++) {
                    JSONObject jsonObject1 = noticeJsonObj.getJSONArray(getActivity().getResources().getString(R.string.permissions_value)).getJSONObject(i);
                    if (jsonObject1.has(getActivity().getResources().getString(R.string.section_value))
                            && jsonObject1.getString(getActivity().getResources().getString(R.string.section_value)).equals("spree_complaints")) {
                        if (jsonObject1.getJSONObject(getActivity().getResources().getString(R.string.permission_value)).has(getActivity().getResources().getString(R.string.index_value))) {
                            mySocietyRoles = jsonObject1.getJSONObject(getActivity().getResources().getString(R.string.permission_value)).getString(getActivity().getResources().getString(R.string.index_value));
                            CREATE = jsonObject1.getJSONObject(getActivity().getResources().getString(R.string.permission_value)).getString("create");
                            INDEX = jsonObject1.getJSONObject(getActivity().getResources().getString(R.string.permission_value)).getString("index");
                            UPDATE = jsonObject1.getJSONObject(getActivity().getResources().getString(R.string.permission_value)).getString("update");
                            EDIT = jsonObject1.getJSONObject(getActivity().getResources().getString(R.string.permission_value)).getString("edit");
                            SHOW = jsonObject1.getJSONObject(getActivity().getResources().getString(R.string.permission_value)).getString("show");
                        }
                    }
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return mySocietyRoles;
    }

}