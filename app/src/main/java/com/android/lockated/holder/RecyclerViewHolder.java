package com.android.lockated.holder;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Paint;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.lockated.Interfaces.IRecyclerItemClickListener;
import com.android.lockated.Interfaces.IServiceViewListener;
import com.android.lockated.LockatedApplication;
import com.android.lockated.R;
import com.android.lockated.categories.grocery.model.GroceryItemData;
import com.android.lockated.categories.reminder.model.ReminderData;
import com.android.lockated.categories.services.model.ServiceRowData;
import com.android.lockated.categories.vendors.model.ProductData;
import com.android.lockated.categories.vendors.model.VendorData;
import com.android.lockated.categories.vendors.model.VendorProductData;
import com.android.lockated.landing.model.SupportData;
import com.android.lockated.landing.model.TaxonData;
import com.android.lockated.landing.model.TaxonomiesData;
import com.android.lockated.model.LineItemData;
import com.android.lockated.model.MyAddressData;
import com.android.lockated.model.MyOrderData;
import com.android.lockated.model.MyServices.ServiceRequest;
import com.android.lockated.model.userGroups.UserGroup;
import com.android.lockated.model.userSocietyFlat.UserFlat;
import com.android.lockated.request.LockatedVolleyRequestQueue;
import com.android.lockated.utils.Utilities;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.NetworkImageView;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;

public class RecyclerViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener, View.OnLongClickListener {
    private int mSectionId;
    private IServiceViewListener mIServiceViewListener;
    private IRecyclerItemClickListener mIRecyclerItemClickListener;
    private boolean doubleBackToExitPressedOnce;

    public RecyclerViewHolder(View itemView, IRecyclerItemClickListener listener) {
        super(itemView);
        mIRecyclerItemClickListener = listener;
    }

    public RecyclerViewHolder(View itemView, IRecyclerItemClickListener listener, IServiceViewListener viewListener) {
        super(itemView);
        mIServiceViewListener = viewListener;
        mIRecyclerItemClickListener = listener;
        mSectionId = ((LockatedApplication) itemView.getContext().getApplicationContext()).getCategoryIdTwo();
    }

    public void onSupportViewHolder(SupportData supportData) {
        /*Log.w("Lockated", supportData.getBody());*/
    }

    public void paymentOptionViewHolder(String paymentOption, Integer mIcoms) {
        //Button mTextViewPaymentOption = (Button) itemView.findViewById(R.id.mTextViewPaymentOption);
        TextView mTextViewPaymentOption = (TextView) itemView.findViewById(R.id.mTextViewPaymentOption);
        ImageView mPaymenticons = (ImageView) itemView.findViewById(R.id.mPaymenticons);
        mTextViewPaymentOption.setText(paymentOption);
        mPaymenticons.setImageResource(mIcoms);
        mTextViewPaymentOption.setOnClickListener(this);
    }

    public void onHomeScreenViewHolder(TaxonomiesData taxonomiesData, Context context) {
        ////WebView mImageWebView = (WebView) itemView.findViewById(R.id.mImageWebView);
        ImageView mImageViewCategory = (ImageView) itemView.findViewById(R.id.mImageViewCategory);
        TextView mTextViewCategoryName = (TextView) itemView.findViewById(R.id.mTextViewCategoryName);
        LinearLayout mLinearLayoutHomeItem = (LinearLayout) itemView.findViewById(R.id.mLinearLayoutHomeItem);

        mTextViewCategoryName.setText(taxonomiesData.getName());

        switch (taxonomiesData.getId()) {
            case 1:
                mImageViewCategory.setImageResource(R.drawable.ic_laundry);
                break;
            case 2:
                mImageViewCategory.setImageResource(R.drawable.ic_beauty);
                break;
            case 6:
                mImageViewCategory.setImageResource(R.drawable.ic_grocery);
                break;
            case 8:
                mImageViewCategory.setImageResource(R.drawable.ic_medicine);
                break;
            case 10:
                mImageViewCategory.setImageResource(R.drawable.ic_household);
                break;
            case 18:
                mImageViewCategory.setImageResource(R.drawable.ic_food);
                break;
            case 28:
                mImageViewCategory.setImageResource(R.drawable.ic_repair_maint);
                break;
            case 31:
                mImageViewCategory.setImageResource(R.drawable.ic_misc);
                break;
            case 33:
                mImageViewCategory.setImageResource(R.drawable.ic_reminder);
                break;
            case 101:
                mImageViewCategory.setImageResource(R.drawable.mysociety);
                break;
            case 102:
                mImageViewCategory.setImageResource(R.drawable.admin);
                break;
            case 103:
                mImageViewCategory.setImageResource(R.drawable.visitor_icon);//ic_gatekeeper
                break;
            case 104:
                /*mImageViewCategory.setImageResource(R.drawable.book_cab_icon);*/
                Glide.with(context).load(R.raw.cab).asGif().diskCacheStrategy
                        (DiskCacheStrategy.SOURCE).crossFade().into(mImageViewCategory);
                break;

            default:
                mImageViewCategory.setImageResource(R.drawable.ic_grocery);
                break;
        }

        mLinearLayoutHomeItem.setOnClickListener(this);
    }

    public void onCrmViewHolder(String data, int position, Context applicationContext) {
        ImageView mImageViewCategory = (ImageView) itemView.findViewById(R.id.mImageViewCategory);
        TextView mTextViewCategoryName = (TextView) itemView.findViewById(R.id.mTextViewCategoryName);
        LinearLayout mLinearLayoutHomeItem = (LinearLayout) itemView.findViewById(R.id.mLinearLayoutHomeItem);

        mTextViewCategoryName.setText(data);

        switch (position) {
            case 0:
                mImageViewCategory.setImageResource(R.drawable.notices);
                break;
            case 1:
                mImageViewCategory.setImageResource(R.drawable.events);
                break;
            case 2:
                mImageViewCategory.setImageResource(R.drawable.poll);
                break;
            case 3:
                mImageViewCategory.setImageResource(R.drawable.helpdesk);
                break;
            case 4:
                mImageViewCategory.setImageResource(R.drawable.myzone);
                break;
            case 5:
                mImageViewCategory.setImageResource(R.drawable.billspayments);
                break;
            case 6:
                mImageViewCategory.setImageResource(R.drawable.ic_society_launcher);
                break;
            case 7:
                mImageViewCategory.setImageResource(R.drawable.ic_commitee_launcher);
                break;
            case 8:
                mImageViewCategory.setImageResource(R.drawable.group_icon);
                break;
            case 9:
                mImageViewCategory.setImageResource(R.drawable.visitor_icon);
                break;
            case 10:
                mImageViewCategory.setImageResource(R.drawable.classified_icon);
                break;
          /*  case 11:
                mImageViewCategory.setImageResource(R.drawable.ic_home);
                break;*/
            default:
                mImageViewCategory.setImageResource(R.drawable.notices);
                break;
        }

        mLinearLayoutHomeItem.setOnClickListener(this);
    }

    public void onCreateGalleryViewHolder() {
        ImageView mImageViewCategory = (ImageView) itemView.findViewById(R.id.mImageViewCategory);
        mImageViewCategory.setImageResource(R.drawable.ic_account_camera);
        mImageViewCategory.setOnClickListener(this);
    }

    public void onOrderViewHolder(MyOrderData orderData) {
        TextView mTextViewOrderId = (TextView) itemView.findViewById(R.id.mTextViewOrderId);
        TextView mTextViewOrderName = (TextView) itemView.findViewById(R.id.mTextViewOrderName);
        TextView mTextViewOrderStatus = (TextView) itemView.findViewById(R.id.mTextViewOrderStatus);
        TextView mTextViewOrderAmount = (TextView) itemView.findViewById(R.id.mTextViewOrderAmount);
        TextView mTextViewOrderCancel = (TextView) itemView.findViewById(R.id.mTextViewOrderCancel);
        TextView mTextViewOrderPlacedOn = (TextView) itemView.findViewById(R.id.mTextViewOrderPlacedOn);
        TextView mTextViewOrderDeliverTime = (TextView) itemView.findViewById(R.id.mTextViewOrderDeliverTime);
        String completedDate;
        if (orderData.getCompleted_at() != null && !orderData.getCompleted_at().equalsIgnoreCase("null")) {
            completedDate = Utilities.dateConvertToDayMonth(orderData.getCompleted_at());
        } else {
            completedDate = "No Date";
        }
        /*if (orderData.getState().equalsIgnoreCase("canceled")) {
            mTextViewOrderStatus.setEnabled(false);
            mTextViewOrderCancel.setVisibility(View.GONE);
        } else {
            mTextViewOrderStatus.setEnabled(true);
            mTextViewOrderCancel.setVisibility(View.VISIBLE);
        }*/

        if (!orderData.getCanCancel()) {
            mTextViewOrderStatus.setEnabled(false);
            mTextViewOrderCancel.setVisibility(View.GONE);
        } else {
            mTextViewOrderStatus.setEnabled(true);
            mTextViewOrderCancel.setVisibility(View.VISIBLE);
        }

        mTextViewOrderStatus.setText(orderData.getState());
        mTextViewOrderId.setText(orderData.getNumber());
        mTextViewOrderPlacedOn.setText(completedDate);
        mTextViewOrderAmount.setText(orderData.getTotal());

        String strDeliverDate = null;
        String strDeliverTime = null;

        if (orderData.getmOrderDetailDataList().size() > 0) {
            try {
                if (orderData.getmOrderDetailDataList().get(0).getPreferred_date() != null
                        && !orderData.getmOrderDetailDataList().get(0).getPreferred_date().equals("null")) {
                    strDeliverDate = Utilities.dateConvertMinify(orderData.getmOrderDetailDataList().get(0).getPreferred_date());
                } else {
                    strDeliverDate = "";
                }
            } catch (Exception e) {
                strDeliverDate = "";
                e.printStackTrace();
            }
            strDeliverTime = orderData.getmOrderDetailDataList().get(0).getPreferred_time();
        }

        mTextViewOrderDeliverTime.setText(strDeliverDate + " " + strDeliverTime);

        mTextViewOrderCancel.setOnClickListener(this);
    }

    public void onMySocietyListViewHolder(UserFlat userFlat) {
        TextView mTextViewSocietyName = (TextView) itemView.findViewById(R.id.mTextViewOrderName);
        TextView mTextViewSocietyStatus = (TextView) itemView.findViewById(R.id.mTextViewOrderStatus);
        TextView mTextViewSocietyFlatNo = (TextView) itemView.findViewById(R.id.mTextViewOrderAmount);
        TextView mResidentType = (TextView) itemView.findViewById(R.id.mResidentType);
        TextView mTextViewSocietyDetail = (TextView) itemView.findViewById(R.id.mTextViewDetails);
        TextView mAddressOne = (TextView) itemView.findViewById(R.id.mAddressOne);
        TextView mAddressTwo = (TextView) itemView.findViewById(R.id.mAddressTwo);
        LinearLayout mLinearLayoutIndividual = (LinearLayout) itemView.findViewById(R.id.mLinearLayoutIndividual);
        LinearLayout mLinearLayoutSociety = (LinearLayout) itemView.findViewById(R.id.mLinearLayoutSociety);

        if (userFlat.getResidenceType() != null && userFlat.getResidenceType().equalsIgnoreCase("society")) {

            mLinearLayoutIndividual.setVisibility(View.GONE);
            mLinearLayoutSociety.setVisibility(View.VISIBLE);

            if (userFlat.getSociety() != null) {
                mTextViewSocietyName.setText(userFlat.getSociety().getBuildingName());
            } else {
                mTextViewSocietyName.setText("Not Available");
            }

            if (userFlat.getUserSociety() != null) {
                if (userFlat.getUserSociety().getStatus() != null) {
                    mTextViewSocietyStatus.setText(userFlat.getUserSociety().getStatus());
                } else {
                    mTextViewSocietyStatus.setText("Pending");
                }
            } else {
                mTextViewSocietyStatus.setText("Pending");
            }

            if (userFlat.getFlat() != null) {
                mTextViewSocietyFlatNo.setText(userFlat.getFlat());
            } else {
                mTextViewSocietyFlatNo.setText("Not Available");
            }

        } else {

            mLinearLayoutSociety.setVisibility(View.GONE);
            mLinearLayoutIndividual.setVisibility(View.VISIBLE);

            if (userFlat.getResidenceType() != null) {
                mResidentType.setText((userFlat.getResidenceType()));
            } else {
                mResidentType.setText("Not Available");
            }

            if (userFlat.getUserSociety() != null) {
                if (userFlat.getUserSociety().getStatus() != null) {
                    mTextViewSocietyStatus.setText(userFlat.getUserSociety().getStatus());
                } else {
                    mTextViewSocietyStatus.setText("Pending");
                }
            } else {
                mTextViewSocietyStatus.setText("Pending");
            }

            if (userFlat.getAddress().getAddress1() != null) {
                mAddressOne.setText(userFlat.getAddress().getAddress1());
            } else {
                mAddressOne.setText("Not Available");
            }

            if (userFlat.getAddress().getAddress2() != null) {
                mAddressTwo.setText(userFlat.getAddress().getAddress2());
            } else {
                mAddressTwo.setText("Not Available");
            }
        }
        mTextViewSocietyDetail.setOnClickListener(this);
    }

    public void onServiceViewHolder(ServiceRequest serviceRequest) {
        TextView mTextViewOrderId = (TextView) itemView.findViewById(R.id.mTextViewOrderId);
        TextView mTextViewOrderName = (TextView) itemView.findViewById(R.id.mTextViewOrderName);
        TextView mTextViewOrderStatus = (TextView) itemView.findViewById(R.id.mTextViewOrderStatus);
        TextView mTextViewOrderAmount = (TextView) itemView.findViewById(R.id.mTextViewOrderAmount);
        TextView mTextViewOrderCancel = (TextView) itemView.findViewById(R.id.mTextViewOrderCancel);
        TextView mTextViewOrderPlacedOn = (TextView) itemView.findViewById(R.id.mTextViewOrderPlacedOn);
        TextView mTextViewOrderDeliverTime = (TextView) itemView.findViewById(R.id.mTextViewOrderDeliverTime);
        String completedDate;
        //mTextViewOrderCancel
        if (serviceRequest.getCreatedAt() != null && !serviceRequest.getCreatedAt().equalsIgnoreCase("null")) {
            completedDate = Utilities.dateConvertToDayMonth(serviceRequest.getCreatedAt());
        } else {
            completedDate = "No Date";
        }
        if (serviceRequest.getState().equalsIgnoreCase("canceled")) {
            mTextViewOrderStatus.setEnabled(false);
            mTextViewOrderCancel.setVisibility(View.GONE);
        } else {
            mTextViewOrderStatus.setEnabled(true);
            mTextViewOrderCancel.setVisibility(View.VISIBLE);
        }


        if (!serviceRequest.isCanCancel()
                ) {
            mTextViewOrderStatus.setEnabled(false);
            mTextViewOrderCancel.setVisibility(View.GONE);
        } else {
            if (serviceRequest.getState().equalsIgnoreCase("Converted to Order")) {
                mTextViewOrderStatus.setEnabled(false);
                mTextViewOrderCancel.setVisibility(View.GONE);
            } else {
                mTextViewOrderStatus.setEnabled(true);
                mTextViewOrderCancel.setVisibility(View.VISIBLE);
            }
        }

        mTextViewOrderStatus.setText(serviceRequest.getState());
        mTextViewOrderId.setText("" + serviceRequest.getId());
        mTextViewOrderPlacedOn.setText(completedDate);
        if (serviceRequest.getAttributeOne() != null) {
            mTextViewOrderAmount.setText(serviceRequest.getAttributeOne().getApproxCharge());
        } else {
            mTextViewOrderAmount.setText("");
        }
        String strDeliverDate = null;
        String strDeliverTime = null;

        if (serviceRequest.getAttributeOne() != null) {
            try {
                if (serviceRequest.getCreatedAt() != null
                        && !serviceRequest.getCreatedAt().equals("null")) {
                    strDeliverDate = Utilities.dateTime_AMPM_Format(serviceRequest.getCreatedAt());
                } else {
                    strDeliverDate = "";
                }
            } catch (Exception e) {
                strDeliverDate = "";
                e.printStackTrace();
            }
            //strDeliverTime = serviceRequest.getmOrderDetailDataList().get(0).getPreferred_time();
        }

        mTextViewOrderDeliverTime.setText(strDeliverDate/* + " " + strDeliverTime*/);

        mTextViewOrderCancel.setOnClickListener(this);
    }

    public void onCategoryViewHolder(TaxonData taxonData) {
        TextView mTextViewCategoryItem = (TextView) itemView.findViewById(R.id.mTextViewCategoryItem);
        mTextViewCategoryItem.setOnClickListener(this);
        mTextViewCategoryItem.setText(taxonData.getName());
    }

    public void onReminderViewHolder(ReminderData reminderData) {
        TextView mButtonReminderEdit = (TextView) itemView.findViewById(R.id.mButtonReminderEdit);
        TextView mButtonReminderTitle = (TextView) itemView.findViewById(R.id.mButtonReminderTitle);
        TextView mButtonReminderDelete = (TextView) itemView.findViewById(R.id.mButtonReminderDelete);
        TextView mTextReminderDateTime = (TextView) itemView.findViewById(R.id.mTextReminderDateTime);
        TextView mButtonReminderDescription = (TextView) itemView.findViewById(R.id.mButtonReminderDescription);

        mButtonReminderTitle.setText(reminderData.getSubject());
        mButtonReminderDescription.setText(reminderData.getDescription());
        mTextReminderDateTime.setText(reminderData.getDate() + " " + reminderData.getTime());

        mButtonReminderEdit.setOnClickListener(this);
        mButtonReminderDelete.setOnClickListener(this);
    }

    public void serviceRowViewHolder(final ServiceRowData serviceRowData) {
        LinearLayout mLinearLayoutService = (LinearLayout) itemView.findViewById(R.id.mLinearLayoutService);
        LinearLayout mLinearLayoutServiceRow = (LinearLayout) itemView.findViewById(R.id.mLinearLayoutServiceRow);
        LinearLayout mLinearLayoutSubService = (LinearLayout) itemView.findViewById(R.id.mLinearLayoutSubService);
        LinearLayout mLinearLayoutServiceContainer = (LinearLayout) itemView.findViewById(R.id.mLinearLayoutServiceContainer);

        TextView mTextViewServiceName = (TextView) itemView.findViewById(R.id.mTextViewServiceName);
        TextView mTextViewServiceCharge = (TextView) itemView.findViewById(R.id.mTextViewServiceCharge);
        TextView mTextViewSubServiceName = (TextView) itemView.findViewById(R.id.mTextViewSubServiceName);

        EditText mEditTextServiceModel = (EditText) itemView.findViewById(R.id.mEditTextServiceModel);
        EditText mEditTextServiceBrand = (EditText) itemView.findViewById(R.id.mEditTextServiceBrand);

        if (mSectionId == 1) {
            mLinearLayoutServiceContainer.setVisibility(View.VISIBLE);
        } else {
            mLinearLayoutServiceContainer.setVisibility(View.GONE);
        }

        mTextViewServiceName.setText(serviceRowData.getServiceName());
        if (serviceRowData.getServiceCharges().equals("Approximate charges will be showing here.")) {
            mTextViewServiceCharge.setText(serviceRowData.getServiceCharges());
        } else {
            mTextViewServiceCharge.setText("Approx charge " + serviceRowData.getServiceCharges());
        }
        mTextViewSubServiceName.setText(serviceRowData.getSubServiceName());
        mEditTextServiceBrand.setText(serviceRowData.getServiceBrand());
        mEditTextServiceModel.setText(serviceRowData.getServiceModel());

        mEditTextServiceBrand.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                mIServiceViewListener.onServiceRowBrand(s.toString(), getAdapterPosition());
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        mEditTextServiceModel.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                mIServiceViewListener.onServiceRowModel(s.toString(), getAdapterPosition());
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        mLinearLayoutService.setOnClickListener(this);
        mLinearLayoutSubService.setOnClickListener(this);
        mLinearLayoutServiceRow.setOnLongClickListener(this);
    }

    public void onSupplierViewHolder(VendorData vendorData) {
        TextView mTextViewSupplierName = (TextView) itemView.findViewById(R.id.mTextViewSupplierName);
        TextView mTextViewSupplierMinAmount = (TextView) itemView.findViewById(R.id.mTextViewSupplierMinAmount);
        TextView mTextViewSupplierDistance = (TextView) itemView.findViewById(R.id.mTextViewSupplierDistance);
        TextView mTextViewSupplierCod = (TextView) itemView.findViewById(R.id.mTextViewSupplierCod);
        TextView mTextViewSupplierDeliveryCharges = (TextView) itemView.findViewById(R.id.mTextViewSupplierDeliveryCharges);

        LinearLayout mLinearLayoutVendorRow = (LinearLayout) itemView.findViewById(R.id.mLinearLayoutVendorRow);

        mTextViewSupplierName.setText(vendorData.getName());
        mTextViewSupplierMinAmount.setText(vendorData.getMin_amount());
        mTextViewSupplierDistance.setText(vendorData.getDistance() + "");
        mTextViewSupplierDeliveryCharges.setText(vendorData.getDelivery_charges() + "");
        mTextViewSupplierCod.setText(vendorData.getCod());

        mLinearLayoutVendorRow.setOnClickListener(this);
    }

    public void onVendorMenuViewHolder(VendorProductData vendorProductData) {
        TextView mTextViewCategoryItem = (TextView) itemView.findViewById(R.id.mTextViewCategoryItem);
        mTextViewCategoryItem.setText(vendorProductData.getCatName());

        mTextViewCategoryItem.setOnClickListener(this);
    }

    public void onVendorProductViewHolder(ProductData productData) {
        TextView mTextViewProductName = (TextView) itemView.findViewById(R.id.mTextViewProductName);
        TextView mTextViewProductPrice = (TextView) itemView.findViewById(R.id.mTextViewProductPrice);
        TextView mTextViewProductWeight = (TextView) itemView.findViewById(R.id.mTextViewProductWeight);
        TextView mTextViewProductQuantity = (TextView) itemView.findViewById(R.id.mTextViewProductQuantity);
        TextView mTextViewProductDiscountedPrice = (TextView) itemView.findViewById(R.id.mTextViewProductDiscountedPrice);

        ImageView mImageViewProductQuantityAdd = (ImageView) itemView.findViewById(R.id.mImageViewProductQuantityAdd);
        NetworkImageView mImageViewProductImage = (NetworkImageView) itemView.findViewById(R.id.mImageViewProductImage);
        ImageView mImageViewProductQuantityRemove = (ImageView) itemView.findViewById(R.id.mImageViewProductQuantityRemove);

        int quantity = productData.getQuantity();
        int total = productData.getTotalItems();

        if (total > 0) {
            if (quantity > 0) {
                if (quantity == total) {
                    mImageViewProductQuantityAdd.setEnabled(false);
                    mImageViewProductQuantityAdd.setImageResource(R.drawable.ic_item_add_disable);
                } else {
                    mImageViewProductQuantityAdd.setEnabled(true);
                    mImageViewProductQuantityAdd.setImageResource(R.drawable.ic_item_add);
                }

                mImageViewProductQuantityRemove.setEnabled(true);
                mImageViewProductQuantityRemove.setImageResource(R.drawable.ic_item_remove);
            } else {
                mImageViewProductQuantityRemove.setEnabled(false);
                mImageViewProductQuantityRemove.setImageResource(R.drawable.ic_item_remove_disable);
            }
        } else {
            mImageViewProductQuantityRemove.setEnabled(false);
            mImageViewProductQuantityRemove.setImageResource(R.drawable.ic_item_remove_disable);

            mImageViewProductQuantityAdd.setEnabled(false);
            mImageViewProductQuantityAdd.setImageResource(R.drawable.ic_item_add_disable);
        }

        if (productData.getCostPrice().equalsIgnoreCase(productData.getPrice())) {
            mTextViewProductPrice.setVisibility(View.GONE);
            mTextViewProductPrice.setText(productData.getCostPrice());
        } else {
            mTextViewProductPrice.setVisibility(View.VISIBLE);
            mTextViewProductPrice.setText(productData.getCostPrice());
        }

        mTextViewProductName.setText(productData.getName());

        mTextViewProductDiscountedPrice.setText(itemView.getContext().getString(R.string.rupees_symbol) + " " + productData.getPrice());
//        mTextViewProductWeight.setText(productData.getWeight());
        mTextViewProductQuantity.setText(productData.getQuantity() + "");
        mTextViewProductPrice.setPaintFlags(mTextViewProductPrice.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);

        mImageViewProductQuantityAdd.setOnClickListener(this);
        mImageViewProductQuantityRemove.setOnClickListener(this);

        ImageLoader imageLoader = LockatedVolleyRequestQueue.getInstance(itemView.getContext()).getImageLoader();
        mImageViewProductImage.setDefaultImageResId(R.drawable.ic_grocery);
        mImageViewProductImage.setImageUrl(productData.getImageUrl(), imageLoader);
    }

    public void onEcommerceViewHolder(LineItemData lineItemData) {
        TextView mTextViewProductName = (TextView) itemView.findViewById(R.id.mTextViewProductName);
        TextView mTextViewProductPrice = (TextView) itemView.findViewById(R.id.mTextViewProductPrice);
        TextView mTextViewProductWeight = (TextView) itemView.findViewById(R.id.mTextViewProductWeight);
        TextView mTextViewProductQuantity = (TextView) itemView.findViewById(R.id.mTextViewProductQuantity);
        TextView mTextViewProductRemove = (TextView) itemView.findViewById(R.id.mTextViewProductRemove);
        TextView mTextViewProductDiscountedPrice = (TextView) itemView.findViewById(R.id.mTextViewProductDiscountedPrice);

        ImageView mImageViewProductQuantityAdd = (ImageView) itemView.findViewById(R.id.mImageViewProductQuantityAdd);
        NetworkImageView mImageViewProductImage = (NetworkImageView) itemView.findViewById(R.id.mImageViewProductImage);
        ImageView mImageViewProductQuantityRemove = (ImageView) itemView.findViewById(R.id.mImageViewProductQuantityRemove);


        mTextViewProductName.setText(lineItemData.getVariantName());
        mTextViewProductDiscountedPrice.setText(itemView.getContext().getString(R.string.rupees_symbol) + " " + lineItemData.getVariantPrice());
        mTextViewProductWeight.setText(lineItemData.getVariantWeight());
        mTextViewProductQuantity.setText(lineItemData.getQuantity() + "");
        mTextViewProductPrice.setVisibility(View.GONE);

        mTextViewProductRemove.setOnClickListener(this);
        mImageViewProductQuantityAdd.setOnClickListener(this);
        mImageViewProductQuantityRemove.setOnClickListener(this);


        ImageLoader imageLoader = LockatedVolleyRequestQueue.getInstance(itemView.getContext()).getImageLoader();
        mImageViewProductImage.setDefaultImageResId(R.drawable.ic_grocery);
        mImageViewProductImage.setImageUrl(lineItemData.getVariantImageUrl(), imageLoader);
    }

    @SuppressLint("LongLogTag")
    public void onCategoryDetailViewHolder(GroceryItemData groceryItemData) {
        TextView mTextViewProductName = (TextView) itemView.findViewById(R.id.mTextViewProductName);
        TextView mTextViewProductPrice = (TextView) itemView.findViewById(R.id.mTextViewProductPrice);
        TextView mTextViewProductWeight = (TextView) itemView.findViewById(R.id.mTextViewProductWeight);
        TextView mTextViewProductQuantity = (TextView) itemView.findViewById(R.id.mTextViewProductQuantity);
        TextView mTextViewProductDiscountedPrice = (TextView) itemView.findViewById(R.id.mTextViewProductDiscountedPrice);
        TextView mTextProductDescription = (TextView) itemView.findViewById(R.id.mTextProductDescription);
        LinearLayout mLayoutProductDescription = (LinearLayout) itemView.findViewById(R.id.mLayoutProductDescription);

        ImageView mImageViewProductQuantityAdd = (ImageView) itemView.findViewById(R.id.mImageViewProductQuantityAdd);
        NetworkImageView mImageViewProductImage = (NetworkImageView) itemView.findViewById(R.id.mImageViewProductImage);
        ImageView mImageViewProductQuantityRemove = (ImageView) itemView.findViewById(R.id.mImageViewProductQuantityRemove);
        ImageView mImageProductDescription = (ImageView) itemView.findViewById(R.id.mImageProductDescription);

        int quantity = groceryItemData.getQuantity();
        //Log.e("quantity", "" + quantity);
        int total = groceryItemData.getTotalItems();
        Log.e("groceryItemData.getDescription()", "" + groceryItemData.getDescription());
        if (groceryItemData.getDescription() != null && (!groceryItemData.getDescription().equals("")) && (!groceryItemData.getDescription().equals("null"))) {

            //mLayoutProductDescription.setVisibility(View.VISIBLE);
            //mImageProductDescription.setImageResource(R.drawable.new_ic_down);
            mTextProductDescription.setText(groceryItemData.getDescription());
        } else {
            mLayoutProductDescription.setVisibility(View.GONE);
        }
        if (total > 0) {
            if (quantity > 0) {
                if (quantity == total) {
                    mImageViewProductQuantityAdd.setEnabled(false);
                    mImageViewProductQuantityAdd.setImageResource(R.drawable.ic_item_add_disable);
                } else {
                    mImageViewProductQuantityAdd.setEnabled(true);
                    mImageViewProductQuantityAdd.setImageResource(R.drawable.ic_item_add);
                }

                mImageViewProductQuantityRemove.setEnabled(true);
                mImageViewProductQuantityRemove.setImageResource(R.drawable.ic_item_remove);
            } else {
                mImageViewProductQuantityRemove.setEnabled(false);
                mImageViewProductQuantityRemove.setImageResource(R.drawable.ic_item_remove_disable);
            }
        } else {
            mImageViewProductQuantityRemove.setEnabled(false);
            mImageViewProductQuantityRemove.setImageResource(R.drawable.ic_item_remove_disable);

            mImageViewProductQuantityAdd.setEnabled(false);
            mImageViewProductQuantityAdd.setImageResource(R.drawable.ic_item_add_disable);
        }

        if (groceryItemData.getCostPrice().equalsIgnoreCase(groceryItemData.getPrice())) {
            mTextViewProductPrice.setVisibility(View.GONE);
            mTextViewProductPrice.setText(itemView.getContext().getString(R.string.rupees_symbol) + " " + groceryItemData.getCostPrice());
        } else {
            mTextViewProductPrice.setVisibility(View.VISIBLE);
            mTextViewProductPrice.setText(itemView.getContext().getString(R.string.rupees_symbol) + " " + groceryItemData.getCostPrice());
        }

        mTextViewProductName.setText(groceryItemData.getName());
        mTextViewProductDiscountedPrice.setText(itemView.getContext().getString(R.string.rupees_symbol) + " " + groceryItemData.getPrice());
        mTextViewProductWeight.setText(groceryItemData.getWeight());
        mTextViewProductQuantity.setText(groceryItemData.getQuantity() + "");
        mTextViewProductPrice.setPaintFlags(mTextViewProductPrice.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);


        mImageViewProductQuantityAdd.setOnClickListener(this);
        mImageViewProductQuantityRemove.setOnClickListener(this);
        mImageProductDescription.setOnClickListener(this);

        ImageLoader imageLoader = LockatedVolleyRequestQueue.getInstance(itemView.getContext()).getImageLoader();
        mImageViewProductImage.setDefaultImageResId(R.drawable.ic_grocery);
        mImageViewProductImage.setImageUrl(groceryItemData.getImageUrl(), imageLoader);
    }

    public void onAddressViewHolder(MyAddressData myAddressData) {
        TextView mTextMySavedAddress = (TextView) itemView.findViewById(R.id.mTextMySavedAddress);
        TextView mButtonMySavedAddressEdit = (TextView) itemView.findViewById(R.id.mButtonMySavedAddressEdit);
        TextView mButtonMySavedAddressRemove = (TextView) itemView.findViewById(R.id.mButtonMySavedAddressRemove);

        LinearLayout mLinearLayoutAddresses = (LinearLayout) itemView.findViewById(R.id.mLinearLayoutAddresses);

        mLinearLayoutAddresses.setOnClickListener(this);
        mButtonMySavedAddressEdit.setOnClickListener(this);
        mButtonMySavedAddressRemove.setOnClickListener(this);

        mTextMySavedAddress.setText(myAddressData.getFirstName() + " " + myAddressData.getLastName() + "\n\n" + myAddressData.getAddress1() + " " + myAddressData.getAddress2() + " " + myAddressData.getCity() + " " + myAddressData.getState() + " " + myAddressData.getZipcode());
    }

    public void onGroupViewHolder(UserGroup userGroup, boolean showControlFunctions) {

        TextView groupName = (TextView) itemView.findViewById(R.id.groupName);
        TextView groupMemberNumber = (TextView) itemView.findViewById(R.id.groupMemberNumber);
        ImageView groupImage = (ImageView) itemView.findViewById(R.id.groupImage);
        ImageView groupEdit = (ImageView) itemView.findViewById(R.id.groupEdit);
        ImageView groupDelete = (ImageView) itemView.findViewById(R.id.groupDelete);

        groupName.setText(userGroup.getName());
        groupImage.setImageResource(Utilities.setListImage(userGroup.getName()));
        String memberCount = "(" + userGroup.getGroupmembers().size() + ")";
        groupMemberNumber.setText(memberCount);
        if (showControlFunctions) {
            groupDelete.setOnClickListener(this);
            groupName.setOnClickListener(this);
            groupEdit.setOnClickListener(this);

        } else {
            groupEdit.setVisibility(View.INVISIBLE);
            groupDelete.setVisibility(View.INVISIBLE);
        }

    }

    @Override
    public void onClick(View v) {
        onRowItemClicked(v);
    }

    private void onRowItemClicked(View view) {
        mIRecyclerItemClickListener.onRecyclerItemClick(view, getAdapterPosition());
    }

    @Override
    public boolean onLongClick(View view) {
        mIRecyclerItemClickListener.onRecyclerItemClick(view, getAdapterPosition());
        return true;
    }
}