package com.android.lockated;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.lockated.login.LoginFragment;
import com.android.lockated.preferences.LockatedPreferences;
import com.android.lockated.pushnotification.GcmIntentService;
import com.android.lockated.pushnotification.PushNotificationConfig;
import com.android.lockated.register.SignUpFragment;
import com.android.lockated.splash.SplashFragment;
import com.android.lockated.utils.Utilities;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;

public class IndexActivity extends AppCompatActivity implements View.OnClickListener {

    private LockatedPreferences mLockatedPreferences;

    private static final int PLAY_SERVICES_RESOLUTION_REQUEST = 9000;
    private BroadcastReceiver mRegistrationBroadcastReceiver;
    private String TAG = IndexActivity.class.getSimpleName();
    public static LinearLayout mIndex_layout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_index);
        init();
        Log.e("User Logout Status",""+mLockatedPreferences.getLogout());
        if (mLockatedPreferences.getLogout()) {
            mIndex_layout.setVisibility(View.VISIBLE);
            openLoginFragment();
            mLockatedPreferences.setLogout(false);
        } else {
            mIndex_layout.setVisibility(View.GONE);
            getSupportFragmentManager().beginTransaction().add(R.id.container, new SplashFragment()).commit();
        }

        mRegistrationBroadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {

                // checking for type intent filter
                if (intent.getAction().equals(PushNotificationConfig.REGISTRATION_COMPLETE)) {
                    // gcm successfully registered now subscribe to `global` topic to receive app wide notifications
                    String token = intent.getStringExtra("token");
                    //Toast.makeText(getApplicationContext(), "GCM registration token: " + token, Toast.LENGTH_LONG).show();
                } else if (intent.getAction().equals(PushNotificationConfig.SENT_TOKEN_TO_SERVER)) {
                    // gcm registration id is stored in our server's MySQL
                    Toast.makeText(getApplicationContext(), "GCM registration token is stored in server!", Toast.LENGTH_LONG).show();
                } else if (intent.getAction().equals(PushNotificationConfig.PUSH_NOTIFICATION)) {
                    // new push notification is received
                    Toast.makeText(getApplicationContext(), "Push notification is received!", Toast.LENGTH_LONG).show();
                }
            }
        };

        if (checkPlayServices()) {
            registerGCM();
        }

    }

    @Override
    protected void onResume() {
        super.onResume();
        // register GCM registration complete receiver
        LocalBroadcastManager.getInstance(this).registerReceiver(mRegistrationBroadcastReceiver,
                new IntentFilter(PushNotificationConfig.REGISTRATION_COMPLETE));

        // register new push message receiver
        // by doing this, the activity will be notified each time a new message arrives
        LocalBroadcastManager.getInstance(this).registerReceiver(mRegistrationBroadcastReceiver,
                new IntentFilter(PushNotificationConfig.PUSH_NOTIFICATION));
    }

    @Override
    protected void onPause() {
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mRegistrationBroadcastReceiver);
        super.onPause();
    }

    // starting the service to register with GCM
    private void registerGCM() {
        Intent intent = new Intent(this, GcmIntentService.class);
        intent.putExtra("key", "register");
        startService(intent);
    }

    private boolean checkPlayServices() {
        GoogleApiAvailability apiAvailability = GoogleApiAvailability.getInstance();
        int resultCode = apiAvailability.isGooglePlayServicesAvailable(this);
        if (resultCode != ConnectionResult.SUCCESS) {
            if (apiAvailability.isUserResolvableError(resultCode)) {
                apiAvailability.getErrorDialog(this, resultCode, PLAY_SERVICES_RESOLUTION_REQUEST)
                        .show();
            } else {
                Log.i(TAG, "This device is not supported. Google Play Services not installed!");
                Toast.makeText(getApplicationContext(), "This device is not supported. " +
                        "Google Play Services not installed!", Toast.LENGTH_LONG).show();
                finish();
            }
            return false;
        }
        return true;
    }

    private void init() {
        mLockatedPreferences = new LockatedPreferences(IndexActivity.this);
        mIndex_layout = (LinearLayout) findViewById(R.id.mIndex_layout);
        TextView mTextViewSignUp = (TextView) findViewById(R.id.mTextViewSignUp);
        TextView mTextViewSignIn = (TextView) findViewById(R.id.mTextViewSignIn);
        TextView mTextSignUpBar = (TextView) findViewById(R.id.mTextSignUpBar);
        TextView mTextSignInBar = (TextView) findViewById(R.id.mTextSignInBar);

        mTextViewSignUp.setOnClickListener(this);
        mTextViewSignIn.setOnClickListener(this);
        mTextSignUpBar.setOnClickListener(this);
        mTextSignInBar.setOnClickListener(this);
    }

    private void openLoginFragment() {
        if (!isFinishing()) {
            getSupportFragmentManager().beginTransaction().add(R.id.container, new LoginFragment()).commitAllowingStateLoss();
        }
    }

    private void onSignInFragmentClicked() {
        if (!isFinishing()) {
            getSupportFragmentManager().beginTransaction().replace(R.id.container, new LoginFragment()).commitAllowingStateLoss();
        }
    }

    private void onSignUpFragmentClicked() {
        if (!isFinishing()) {
            getSupportFragmentManager().beginTransaction().replace(R.id.container, new SignUpFragment()).commitAllowingStateLoss();
        }
    }

    private void openLoginReplaceFragment() {
        if (!isFinishing()) {
            getSupportFragmentManager().beginTransaction().replace(R.id.container, new LoginFragment()).commitAllowingStateLoss();
        }
    }

    @Override

    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.mTextViewSignUp:
                onSignUpFragmentClicked();
                break;
            case R.id.mTextViewSignIn:
                onSignInFragmentClicked();
                break;
        }
    }

    @Override
    public void onBackPressed() {
        Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.container);
        if (!(fragment instanceof LoginFragment)) {
            openLoginReplaceFragment();
            Utilities.showToastMessage(getApplicationContext(), "Index Back Pressed");
        } else {
            super.onBackPressed();
        }
    }
}
