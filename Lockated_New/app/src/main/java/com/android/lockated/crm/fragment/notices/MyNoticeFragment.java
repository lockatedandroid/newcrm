package com.android.lockated.crm.fragment.notices;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.android.lockated.LockatedApplication;
import com.android.lockated.R;
import com.android.lockated.crm.activity.CreateNoticeActivity;
import com.android.lockated.crm.adapters.MyNoticeAdapter;
import com.android.lockated.model.usermodel.NoticeModel.MyNotices.Noticeboard;
import com.android.lockated.network.ConnectionDetector;
import com.android.lockated.preferences.LockatedPreferences;
import com.android.lockated.request.LockatedJSONObjectRequest;
import com.android.lockated.request.LockatedRequestError;
import com.android.lockated.request.LockatedVolleyRequestQueue;
import com.android.lockated.utils.ApplicationURL;
import com.android.lockated.utils.Utilities;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class MyNoticeFragment extends Fragment implements Response.Listener, Response.ErrorListener, View.OnClickListener {

    RecyclerView noticeList;
    TextView errorMsg;
    MyNoticeAdapter myNoticeAdapter;
    private RequestQueue mQueue;
    private LockatedPreferences mLockatedPreferences;
    public static final String REQUEST_TAG = "MyNoticeFragment";
    ArrayList<Noticeboard> noticeboards;
    ProgressBar progressBar;
    FloatingActionButton fab;
    String SHOW, CREATE, INDEX, UPDATE, EDIT;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View myNoticeView = inflater.inflate(R.layout.fragment_my_notice, container, false);
        mLockatedPreferences = new LockatedPreferences(getActivity());
        Utilities.ladooIntegration(getActivity(), getActivity().getResources().getString(R.string.my_notices));
        init(myNoticeView);
        return myNoticeView;
    }

    @Override
    public void onResume() {
        super.onResume();
        LockatedApplication.getInstance().trackScreenView(getString(R.string.my_notices));
        Utilities.lockatedGoogleAnalytics(getString(R.string.my_notices),
                getString(R.string.visited), getString(R.string.my_notices));
        noticeboards.clear();
        checkPermission();
    }

    private void init(View view) {
        noticeboards = new ArrayList<>();
        progressBar = (ProgressBar) view.findViewById(R.id.mProgressBarView);
        noticeList = (RecyclerView) view.findViewById(R.id.noticeList);
        errorMsg = (TextView) view.findViewById(R.id.noNotices);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        noticeList.setLayoutManager(linearLayoutManager);
        noticeList.setHasFixedSize(true);
        fab = (FloatingActionButton) view.findViewById(R.id.fab);
        fab.setOnClickListener(this);
        getNoticeRole();
        /*checkPermission();*/
        myNoticeAdapter = new MyNoticeAdapter(getActivity(), noticeboards, getChildFragmentManager());
        noticeList.setAdapter(myNoticeAdapter);

    }

    private void checkPermission() {
        if (CREATE!=null&&CREATE.equals("true")) {
            fab.setVisibility(View.VISIBLE);
        } else {
            fab.setVisibility(View.GONE);
        }
        if (SHOW!=null&&SHOW.equals("true")) {
            callApi();
        } else {
            noticeList.setVisibility(View.GONE);
            errorMsg.setVisibility(View.VISIBLE);
            errorMsg.setText(R.string.no_permission_error);
        }
    }

    public void callApi() {
        progressBar.setVisibility(View.VISIBLE);
        if (ConnectionDetector.isConnectedToInternet(getActivity())) {
            mQueue = LockatedVolleyRequestQueue.getInstance(getActivity()).getRequestQueue();
            String url = ApplicationURL.getMyNoticesUrl + mLockatedPreferences.getLockatedToken()
                    + "&id_society=" + mLockatedPreferences.getSocietyId();
            LockatedJSONObjectRequest lockatedJSONObjectRequest = new LockatedJSONObjectRequest(Request.Method.GET,
                    url, null, this, this);
            lockatedJSONObjectRequest.setTag(REQUEST_TAG);
            mQueue.add(lockatedJSONObjectRequest);
        } else {
            Utilities.showToastMessage(getActivity(), getActivity().getResources().getString(R.string.internet_connection_error));
        }
    }

    @Override
    public void onErrorResponse(VolleyError error) {
        progressBar.setVisibility(View.GONE);
        if (getActivity() != null) {
            LockatedRequestError.onRequestError(getActivity(), error);
        }
    }

    @Override
    public void onResponse(Object response) {
        progressBar.setVisibility(View.GONE);
        if (getActivity() != null) {
            try {
                JSONObject jsonObject = (JSONObject) response;
                if (jsonObject != null && jsonObject.length() > 0 && jsonObject.getJSONArray("noticeboards").length() > 0) {
                    if (jsonObject.has("code")) {
                        errorMsg.setVisibility(View.VISIBLE);
                        noticeList.setVisibility(View.GONE);
                    } else {
                        JSONArray jsonArray = jsonObject.getJSONArray("noticeboards");
                        Gson gson = new Gson();
                        for (int i = 0; i < jsonArray.length(); i++) {
                            Noticeboard events = gson.fromJson(jsonArray.getJSONObject(i).toString(), Noticeboard.class);
                            noticeboards.add(events);
                        }
                        myNoticeAdapter.notifyDataSetChanged();
                    }
                } else {
                    noticeList.setVisibility(View.GONE);
                    errorMsg.setVisibility(View.VISIBLE);
                    errorMsg.setText(R.string.no_data_error);
                }

            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.fab:
                Intent intent = new Intent(getActivity(), CreateNoticeActivity.class);
                startActivity(intent);
                break;

        }
    }

    public String getNoticeRole() {
        String mySocietyRoles = getActivity().getResources().getString(R.string.blank_value);
        if (mLockatedPreferences.getRolesJson() != null && !mLockatedPreferences.getRolesJson().equals("blank")) {
            try {
                JSONObject noticeJsonObj = new JSONObject(mLockatedPreferences.getRolesJson());
                for (int i = 0; i < noticeJsonObj.getJSONArray(getActivity().getResources().getString(R.string.permissions_value)).length(); i++) {
                    JSONObject jsonObject1 = noticeJsonObj.getJSONArray(getActivity().getResources().getString(R.string.permissions_value)).getJSONObject(i);
                    if (jsonObject1.has(getActivity().getResources().getString(R.string.section_value))
                            && jsonObject1.getString(getActivity().getResources().getString(R.string.section_value)).equals("spree_noticeboards")) {
                        if (jsonObject1.getJSONObject(getActivity().getResources().getString(R.string.permission_value)).has(getActivity().getResources().getString(R.string.index_value))) {
                            mySocietyRoles = jsonObject1.getJSONObject(getActivity().getResources().getString(R.string.permission_value)).getString(getActivity().getResources().getString(R.string.index_value));
                            CREATE = jsonObject1.getJSONObject(getActivity().getResources().getString(R.string.permission_value)).getString("create");
                            INDEX = jsonObject1.getJSONObject(getActivity().getResources().getString(R.string.permission_value)).getString("index");
                            UPDATE = jsonObject1.getJSONObject(getActivity().getResources().getString(R.string.permission_value)).getString("update");
                            EDIT = jsonObject1.getJSONObject(getActivity().getResources().getString(R.string.permission_value)).getString("edit");
                            SHOW = jsonObject1.getJSONObject(getActivity().getResources().getString(R.string.permission_value)).getString("show");
                        }
                    }
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return mySocietyRoles;
    }

}