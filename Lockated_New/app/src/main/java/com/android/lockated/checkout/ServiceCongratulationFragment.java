package com.android.lockated.checkout;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.android.lockated.HomeActivity;
import com.android.lockated.LockatedApplication;
import com.android.lockated.R;
import com.android.lockated.cart.CartController;
import com.android.lockated.information.AccountController;
import com.android.lockated.model.AccountData;
import com.android.lockated.network.ConnectionDetector;
import com.android.lockated.preferences.LockatedPreferences;
import com.android.lockated.request.LockatedRequestError;
import com.android.lockated.request.LockatedVolleyRequestQueue;
import com.android.lockated.utils.ApplicationURL;
import com.android.lockated.utils.Utilities;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.google.android.gms.analytics.ecommerce.Product;
import com.google.android.gms.analytics.ecommerce.ProductAction;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class ServiceCongratulationFragment extends Fragment implements Response.Listener, Response.ErrorListener, View.OnClickListener {
    private View mCongratsFragment;
    private String mOrderNumber;
    TextView mServiceThank;
    private ProgressDialog mProgressDialog;

    private CartController mCartController;
    private AccountController mAccountController;

    public static final String REQUEST_TAG = "CongratulationFragment";
    private LockatedPreferences mLockatedPreferences;
    String screenName = "Service Congratulation Screen";

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        ((AppCompatActivity) context).getSupportActionBar().setDisplayHomeAsUpEnabled(false);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mCongratsFragment = inflater.inflate(R.layout.fragment_congrats, container, false);
        Utilities.ladooIntegration(getActivity(), screenName);
        Utilities.lockatedGoogleAnalytics(screenName, getString(R.string.visited), screenName);
        init();
        return mCongratsFragment;
    }

    public void setOrderId(String number) {
        mOrderNumber = number;
    }

    private void init() {
        mCartController = CartController.getInstance();
        mAccountController = AccountController.getInstance();
        mLockatedPreferences = new LockatedPreferences(getActivity());
        TextView mTextViewOrderNumber = (TextView) mCongratsFragment.findViewById(R.id.mTextViewOrderNumber);
        TextView mTextViewOrderPlacedDone = (TextView) mCongratsFragment.findViewById(R.id.mTextViewOrderPlacedDone);
        TextView mServiceThank = (TextView) mCongratsFragment.findViewById(R.id.mServiceThank);
        String detalThank = "Thank you for placing service request.\n" +
                "We'll get back to you soon\n" +
                "Your service request id\n";
        mServiceThank.setText(detalThank);
        mTextViewOrderNumber.setText("#"+mOrderNumber);
        mTextViewOrderPlacedDone.setOnClickListener(this);
    }

    private void onOrderPlacedDoneClicked() {
        Intent intent = new Intent(getActivity(), HomeActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
        getActivity().finish();
    }

    public void getScreenDetail() {
        if (ConnectionDetector.isConnectedToInternet(getActivity())) {
            mProgressDialog = ProgressDialog.show(getActivity(), "", "Please Wait...", false);
            LockatedVolleyRequestQueue lockatedVolleyRequestQueue = LockatedVolleyRequestQueue.getInstance(getActivity());
            lockatedVolleyRequestQueue.sendRequest(REQUEST_TAG, Request.Method.GET, ApplicationURL.getScreenUrl
                    + "?token=" + mLockatedPreferences.getLockatedToken(), null, this, this);
        } else {
            Utilities.showToastMessage(getActivity(), getActivity().getResources().getString(R.string.internet_connection_error));
            onOrderPlacedDoneClicked();
        }
    }

    @Override
    public void onErrorResponse(VolleyError error) {
        if (mProgressDialog != null && mProgressDialog.isShowing()) {
            mProgressDialog.dismiss();
        }

        if (getActivity() != null) {
            LockatedRequestError.onRequestError(getActivity(), error);
        }
    }

    @Override
    public void onResponse(Object response) {
        if (mProgressDialog != null && mProgressDialog.isShowing()) {
            mProgressDialog.dismiss();
        }

        if (getActivity() != null) {
            JSONObject jsonObject = (JSONObject) response;
            mCartController.resetItems();
            mAccountController.mAccountDataList.clear();

            AccountData accountData = new AccountData(jsonObject);
            mAccountController.mAccountDataList.add(accountData);
            mLockatedPreferences.setUserSocietyId(accountData.getSelected_user_society());
            onOrderPlacedDoneClicked();
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.mTextViewOrderPlacedDone:
                //onOrderPlacedDoneClicked();
                getScreenDetail();
                break;
        }
    }

}