package com.android.lockated.crmadmin.fragment;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.android.lockated.LockatedApplication;
import com.android.lockated.R;
import com.android.lockated.crmadmin.activity.AdminMyZoneDirectoryActivity;
import com.android.lockated.information.AccountController;
import com.android.lockated.model.AccountData;
import com.android.lockated.model.AdminModel.AdminDirectoty.PublicDirectory;
import com.android.lockated.network.ConnectionDetector;
import com.android.lockated.preferences.LockatedPreferences;
import com.android.lockated.request.LockatedJSONObjectRequest;
import com.android.lockated.request.LockatedRequestError;
import com.android.lockated.request.LockatedVolleyRequestQueue;
import com.android.lockated.utils.ApplicationURL;
import com.android.lockated.utils.Utilities;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class AdminSocietyDirectoryFragment extends Fragment implements View.OnClickListener, Response.ErrorListener, Response.Listener<JSONObject> {
    FragmentManager fragmentManager;
    private FloatingActionButton fab;
    private RequestQueue mQueue;
    ListView listView;
    String pid;
    int societyid;
    String userType;
    TextView errorMsg;
    JSONObject directoryJsonObj;
    ProgressBar progressBar;
    AccountController accountController;
    ArrayList<AccountData> accountDataArrayList;
    ArrayList<PublicDirectory> publicDirectories;
    private LockatedPreferences mLockatedPreferences;
    public static final String REQUEST_TAG = "AdminSocietyDirectoryFragment";
    AdminSocietyDirectoryAdapter adminSocietyDirectoryAdapter;
    String SHOW, CREATE, INDEX, UPDATE, EDIT, DESTROY;

    public FragmentManager setFragmentManager(FragmentManager fragmentManager) {
        this.fragmentManager = fragmentManager;
        return fragmentManager;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View AdminSocityDirectoryFragmentView = inflater.inflate(R.layout.fragment_admin_socity_directory, container, false);
        init(AdminSocityDirectoryFragmentView);
        Utilities.ladooIntegration(getActivity(), getActivity().getResources().getString(R.string.adminSocietyDirectory));
        return AdminSocityDirectoryFragmentView;
    }

    @Override
    public void onResume() {
        super.onResume();
        LockatedApplication.getInstance().trackScreenView(getString(R.string.adminSocietyDirectory));
        Utilities.lockatedGoogleAnalytics(getString(R.string.adminSocietyDirectory),
                getString(R.string.visited), getString(R.string.adminSocietyDirectory));
     /*   publicDirectories.clear();
        getDirectory();*/
    }

    private void init(View AdminSocityDirectoryFragmentView) {
        publicDirectories = new ArrayList<>();
        mLockatedPreferences = new LockatedPreferences(getActivity());
        accountController = AccountController.getInstance();
        accountDataArrayList = accountController.getmAccountDataList();
        societyid = accountDataArrayList.get(0).getmResidenceDataList().get(0).getSociety_id();
        progressBar = (ProgressBar) AdminSocityDirectoryFragmentView.findViewById(R.id.mProgressBarView);
        listView = (ListView) AdminSocityDirectoryFragmentView.findViewById(R.id.listView);
        errorMsg = (TextView) AdminSocityDirectoryFragmentView.findViewById(R.id.errorMsg);
        fab = (FloatingActionButton) AdminSocityDirectoryFragmentView.findViewById(R.id.fab);
        getDirectoryRole();
        checkPermission();
        adminSocietyDirectoryAdapter = new AdminSocietyDirectoryAdapter(getActivity(), publicDirectories, EDIT, DESTROY,
                getChildFragmentManager());
        listView.setAdapter(adminSocietyDirectoryAdapter);
        fab.setOnClickListener(this);

    }

    private void checkPermission() {
        if (CREATE!=null&&CREATE.equals("true")) {
            fab.setVisibility(View.VISIBLE);
        } else {
            fab.setVisibility(View.GONE);
        }
        if (SHOW!=null&&SHOW.equals("true")) {
            getDirectory();
        } else {
            listView.setVisibility(View.GONE);
            errorMsg.setVisibility(View.VISIBLE);
            errorMsg.setText(R.string.no_permission_error);
        }
    }

    private void getDirectory() {
        if (getActivity() != null) {
            if (!mLockatedPreferences.getSocietyId().equals("blank")) {
                if (ConnectionDetector.isConnectedToInternet(getActivity())) {
                    mLockatedPreferences = new LockatedPreferences(getActivity());
                    mQueue = LockatedVolleyRequestQueue.getInstance(getActivity()).getRequestQueue();
                    String url = ApplicationURL.getPublicDirectory + "society_id=" + societyid + "&token="
                            + mLockatedPreferences.getLockatedToken();
                    LockatedJSONObjectRequest lockatedJSONObjectRequest = new LockatedJSONObjectRequest(Request.Method.GET,
                            url, null, this, this);
                    // Log.e("Admin Soc. Dir",""+url);
                    lockatedJSONObjectRequest.setTag(REQUEST_TAG);
                    mQueue.add(lockatedJSONObjectRequest);
                } else {
                    Utilities.showToastMessage(getActivity(), getActivity().getResources().getString(R.string.internet_connection_error));
                }
            } else {
                errorMsg.setVisibility(View.VISIBLE);
                errorMsg.setText(R.string.not_in_society);
            }

        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.fab:
                String pid = null;
                AddToSocietyDirectory mAddToSocityDirectory = new AddToSocietyDirectory();
                Bundle bundle = new Bundle();
                bundle.putString("pid", pid);
                mAddToSocityDirectory.setArguments(bundle);
                mAddToSocityDirectory.show(getActivity().getSupportFragmentManager(), "PreviewDialog");
                break;
        }
    }

    @Override
    public void onErrorResponse(VolleyError error) {
        progressBar.setVisibility(View.GONE);
        if (getActivity() != null) {
            LockatedRequestError.onRequestError(getActivity(), error);
        }
    }

    @Override
    public void onResponse(JSONObject response) {
        progressBar.setVisibility(View.GONE);

        if (getActivity() != null) {
            try {
                if (response != null && response.length() > 0) {
                    if (response.has("code") && response.getJSONArray("public_directories").length() > 0) {
                        JSONArray jsonArray = response.getJSONArray("public_directories");
                        Gson gson = new Gson();
                        for (int i = 0; i < jsonArray.length(); i++) {
                            PublicDirectory publicDirectory = gson.fromJson(jsonArray.getJSONObject(i).toString(), PublicDirectory.class);
                            publicDirectories.add(publicDirectory);
                        }
                        adminSocietyDirectoryAdapter.notifyDataSetChanged();
                        /*               adminSocietyDirectoryAdapter = new AdminSocietyDirectoryAdapter(getActivity(), response.getJSONArray("public_directories"),
                                getChildFragmentManager());
                        listView.setAdapter(adminSocietyDirectoryAdapter);*/
                    } else {
                        listView.setVisibility(View.GONE);
                        errorMsg.setVisibility(View.VISIBLE);
                        errorMsg.setText(R.string.no_data_error);
                    }
                } else {
                    listView.setVisibility(View.GONE);
                    errorMsg.setVisibility(View.VISIBLE);
                    errorMsg.setText(R.string.no_data_error);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private class AdminSocietyDirectoryAdapter extends BaseAdapter {
        private final ArrayList<PublicDirectory> publicDirectories;
        private final String EDIT;
        private final String DESTROY;
        Context contextMain;
        JSONArray jsonArray;
        LayoutInflater layoutInflater;

        public AdminSocietyDirectoryAdapter(Context context, ArrayList<PublicDirectory> publicDirectories, String EDIT, String DESTROY, FragmentManager childFragmentManager) {
            this.contextMain = context;
            this.EDIT = EDIT;
            this.DESTROY = DESTROY;
            this.publicDirectories = publicDirectories;
            layoutInflater = LayoutInflater.from(context);
        }

        @Override
        public int getCount() {
            return publicDirectories.size();
        }

        @Override
        public Object getItem(int position) {
            return position;
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        private class ViewHolder {
            ImageView mImageDelete;
            ImageView mImageEdit;
            TextView mTextNumber;
            TextView mEmail_Id;
            TextView mTextDirectoryName;
            TextView mServiceType;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
            ViewHolder holder = null;
            if (convertView == null) {
                convertView = layoutInflater.inflate(R.layout.admin_society_child, parent, false);
                holder = new ViewHolder();
                convertView.setTag(holder);
            } else {
                holder = (ViewHolder) convertView.getTag();
            }

            holder.mImageEdit = (ImageView) convertView.findViewById(R.id.mImageEdit);
            holder.mImageDelete = (ImageView) convertView.findViewById(R.id.mImageDelete);
            holder.mTextDirectoryName = (TextView) convertView.findViewById(R.id.mTextDirectoryName);
            holder.mTextNumber = (TextView) convertView.findViewById(R.id.mTextNumber);
            holder.mEmail_Id = (TextView) convertView.findViewById(R.id.mEmail_Id);
            holder.mServiceType = (TextView) convertView.findViewById(R.id.mServiceType);
            try {
                String name = publicDirectories.get(position).getFirstname()
                        + " " + publicDirectories.get(position).getLastname();
                holder.mTextDirectoryName.setText(name);
                holder.mTextNumber.setText(publicDirectories.get(position).getMobile());
                String strEmail = publicDirectories.get(position).getEmail();
                if (strEmail.equals("null")) {
                    holder.mEmail_Id.setText("Not Available");
                } else {
                    holder.mEmail_Id.setText(publicDirectories.get(position).getEmail());

                }
                holder.mServiceType.setText(publicDirectories.get(position).getNature());
            } catch (Exception e) {
                e.printStackTrace();
            }

            if (EDIT!=null&&EDIT.equals("true")) {
            } else {
                holder.mImageEdit.setVisibility(View.VISIBLE);
                holder.mImageEdit.setVisibility(View.INVISIBLE);
            }
            holder.mImageEdit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    AddToSocietyDirectory mAddToDirectory = new AddToSocietyDirectory();
                    Bundle bundle = new Bundle();
                    bundle.putString("pid", "" + publicDirectories.get(position).getId());
                    bundle.putString("mobile", publicDirectories.get(position).getMobile());
                    bundle.putString("email", publicDirectories.get(position).getEmail());
                    bundle.putString("firstname", publicDirectories.get(position).getFirstname());
                    bundle.putString("nature", publicDirectories.get(position).getNature());
                    bundle.putString("lastname", publicDirectories.get(position).getLastname());
                    mAddToDirectory.setArguments(bundle);
                    mAddToDirectory.show(getActivity().getSupportFragmentManager(), "PreviewDialog");
                }
            });
            if (DESTROY!=null&&DESTROY.equals("true")) {
                holder.mImageDelete.setVisibility(View.VISIBLE);
            } else {
                holder.mImageDelete.setVisibility(View.INVISIBLE);
            }
            holder.mImageDelete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onDirectoryDelete(String.valueOf(publicDirectories.get(position).getId()));
                }
            });
            return convertView;
        }

        private void onDirectoryDelete(String pid) {
            mLockatedPreferences = new LockatedPreferences(getActivity());
            progressBar.setVisibility(View.VISIBLE);
            if (getActivity() != null) {
                if (!mLockatedPreferences.getSocietyId().equals("blank")) {
                    if (pid != null) {
                        try {
                            mQueue = LockatedVolleyRequestQueue.getInstance(getActivity()).getRequestQueue();
                            String url = ApplicationURL.deletePublicDirectory + pid + ".json?token="
                                    + mLockatedPreferences.getLockatedToken();
                            LockatedJSONObjectRequest lockatedJSONObjectRequest = new LockatedJSONObjectRequest(Request.Method.DELETE,
                                    url, null, new Response.Listener<JSONObject>() {
                                @Override
                                public void onResponse(JSONObject response) {
                                    progressBar.setVisibility(View.GONE);
                                    if (response != null && response.length() > 0) {
                                        if (response.has("code") && response.has("message")) {
                                            Intent intent = new Intent(getActivity(), AdminMyZoneDirectoryActivity.class);
                                            intent.putExtra("Listposition", 1);
                                            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                            getActivity().startActivity(intent);
                                        }
                                    } else {
                                        listView.setVisibility(View.GONE);
                                        errorMsg.setVisibility(View.VISIBLE);
                                        errorMsg.setText(R.string.no_data_error);
                                    }
                                }
                            }, new Response.ErrorListener() {
                                @Override
                                public void onErrorResponse(VolleyError error) {
                                    progressBar.setVisibility(View.GONE);
                                    if (getActivity() != null) {
                                        LockatedRequestError.onRequestError(getActivity(), error);
                                    }
                                }
                            });
                            lockatedJSONObjectRequest.setTag(REQUEST_TAG);
                            mQueue.add(lockatedJSONObjectRequest);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                } else {
                    listView.setVisibility(View.GONE);
                    errorMsg.setVisibility(View.VISIBLE);
                    errorMsg.setText(R.string.not_in_society);
                }
            }
        }
    }

    public String getDirectoryRole() {
        String mySocietyRoles = getActivity().getResources().getString(R.string.blank_value);
        if (mLockatedPreferences.getRolesJson() != null && !mLockatedPreferences.getRolesJson().equals("blank")) {
            try {
                JSONObject noticeJsonObj = new JSONObject(mLockatedPreferences.getRolesJson());
                for (int i = 0; i < noticeJsonObj.getJSONArray(getActivity().getResources().getString(R.string.permissions_value)).length(); i++) {
                    JSONObject jsonObject1 = noticeJsonObj.getJSONArray(getActivity().getResources().getString(R.string.permissions_value)).getJSONObject(i);
                    if (jsonObject1.has(getActivity().getResources().getString(R.string.section_value))
                            && jsonObject1.getString(getActivity().getResources().getString(R.string.section_value)).equals("spree_directories")) {
                        if (jsonObject1.getJSONObject(getActivity().getResources().getString(R.string.permission_value)).has(getActivity().getResources().getString(R.string.index_value))) {
                            mySocietyRoles = jsonObject1.getJSONObject(getActivity().getResources().getString(R.string.permission_value)).getString(getActivity().getResources().getString(R.string.index_value));
                            CREATE = jsonObject1.getJSONObject(getActivity().getResources().getString(R.string.permission_value)).getString("create");
                            INDEX = jsonObject1.getJSONObject(getActivity().getResources().getString(R.string.permission_value)).getString("index");
                            UPDATE = jsonObject1.getJSONObject(getActivity().getResources().getString(R.string.permission_value)).getString("update");
                            EDIT = jsonObject1.getJSONObject(getActivity().getResources().getString(R.string.permission_value)).getString("edit");
                            SHOW = jsonObject1.getJSONObject(getActivity().getResources().getString(R.string.permission_value)).getString("show");
                            DESTROY = jsonObject1.getJSONObject(getActivity().getResources().getString(R.string.permission_value)).getString("destroy");
                        }
                    }
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return mySocietyRoles;
    }
}
