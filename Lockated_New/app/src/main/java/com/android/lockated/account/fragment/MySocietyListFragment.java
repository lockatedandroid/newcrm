package com.android.lockated.account.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.android.lockated.Interfaces.IRecyclerItemClickListener;
import com.android.lockated.LockatedApplication;
import com.android.lockated.R;
import com.android.lockated.account.activity.AddNewSocietyActivity;
import com.android.lockated.account.activity.MySocietyDetailActivity;
import com.android.lockated.account.adapter.MySocietyListAdapter;
import com.android.lockated.model.userSocietyFlat.UserFlat;
import com.android.lockated.network.ConnectionDetector;
import com.android.lockated.preferences.LockatedPreferences;
import com.android.lockated.request.LockatedRequestError;
import com.android.lockated.request.LockatedVolleyRequestQueue;
import com.android.lockated.utils.ApplicationURL;
import com.android.lockated.utils.Utilities;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class MySocietyListFragment extends Fragment implements Response.Listener, Response.ErrorListener, IRecyclerItemClickListener, View.OnClickListener {

    private View mMyOrdersView;

    private ProgressBar mProgressDialog;
    private RecyclerView mRecyclerView;
    private RecyclerView.LayoutManager mLayoutManager;

    private LockatedPreferences mLockatedPreferences;

    public static final String REQUEST_TAG = "MySocietyListFragment";

    ArrayList<UserFlat> userSocietyArrayList;
    MySocietyListAdapter mySocietyListAdapter;
    TextView noText;
    FloatingActionButton societyFab;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mMyOrdersView = inflater.inflate(R.layout.fragment_society_list, container, false);
        init();
        Utilities.ladooIntegration(getActivity(), getActivity().getString(R.string.my_society_list));
        return mMyOrdersView;
    }

    @Override
    public void onResume() {
        super.onResume();
        LockatedApplication.getInstance().trackScreenView(getActivity().getString(R.string.my_society_list));
        userSocietyArrayList.clear();
        mySocietyList();
    }

    private void init() {
        mLockatedPreferences = new LockatedPreferences(getActivity());
        userSocietyArrayList = new ArrayList<>();
        mRecyclerView = (RecyclerView) mMyOrdersView.findViewById(R.id.mRecyclerView);
        noText = (TextView) mMyOrdersView.findViewById(R.id.noText);
        societyFab = (FloatingActionButton) mMyOrdersView.findViewById(R.id.societyFab);
        societyFab.setOnClickListener(this);
        mProgressDialog = (ProgressBar) mMyOrdersView.findViewById(R.id.mProgressBarView);
        mLayoutManager = new LinearLayoutManager(getActivity());
        mRecyclerView.setLayoutManager(mLayoutManager);
        mRecyclerView.setHasFixedSize(true);
        mySocietyListAdapter = new MySocietyListAdapter(getActivity(), userSocietyArrayList, this);
        mRecyclerView.setAdapter(mySocietyListAdapter);
    }

    private void mySocietyList() {
        if (getActivity() != null) {
            if (ConnectionDetector.isConnectedToInternet(getActivity())) {
                mProgressDialog.setVisibility(View.VISIBLE);
                String url = ApplicationURL.getMySocietyList + mLockatedPreferences.getLockatedToken();
                LockatedVolleyRequestQueue lockatedVolleyRequestQueue = LockatedVolleyRequestQueue.getInstance(getActivity());
                lockatedVolleyRequestQueue.sendRequest(REQUEST_TAG, Request.Method.GET, url, null, this, this);
            } else {
                Utilities.showToastMessage(getActivity(), getActivity().getResources().getString(R.string.internet_connection_error));
            }
        }
    }

    @Override
    public void onErrorResponse(VolleyError error) {
        mProgressDialog.setVisibility(View.GONE);
        if (getActivity() != null) {
            LockatedRequestError.onRequestError(getActivity(), error);
        }
    }

    @Override
    public void onResponse(Object response) {
        mProgressDialog.setVisibility(View.GONE);
        if (getActivity() != null) {
            JSONObject jsonObject = (JSONObject) response;
            if (jsonObject.has("user_flats")) {
                try {
                    JSONArray jsonArray = jsonObject.getJSONArray("user_flats");
                    if (jsonArray.length() > 0) {
                        Gson gson = new Gson();
                        for (int i = 0; i < jsonArray.length(); i++) {
                            JSONObject newJsonObj = jsonArray.getJSONObject(i);
                            UserFlat userSocietiesData = gson.fromJson(newJsonObj.toString(), UserFlat.class);
                            userSocietyArrayList.add(userSocietiesData);
                        }
                        mySocietyListAdapter.notifyDataSetChanged();
                    } else {
                        noText.setText(getActivity().getResources().getString(R.string.no_data_error));
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    @Override
    public void onRecyclerItemClick(View view, int position) {
        switch (view.getId()) {
            case R.id.mTextViewDetails:
                Intent intent = new Intent(getActivity(), MySocietyDetailActivity.class);
                intent.putParcelableArrayListExtra("userSocietyArrayList", userSocietyArrayList);
                intent.putExtra("position", position);
                getActivity().startActivity(intent);
                break;
        }
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {

            case R.id.societyFab:
                Intent intent = new Intent(getActivity(), AddNewSocietyActivity.class);
                getActivity().startActivity(intent);
                break;

        }
    }
}
