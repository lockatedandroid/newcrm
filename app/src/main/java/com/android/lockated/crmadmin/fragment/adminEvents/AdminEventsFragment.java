package com.android.lockated.crmadmin.fragment.adminEvents;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.RelativeLayout;

import com.android.lockated.R;
import com.android.lockated.crm.adapters.CommanListViewAdapter;
import com.android.lockated.crmadmin.activity.AdminEventsActivity;
import com.android.lockated.utils.Utilities;


public class AdminEventsFragment extends Fragment implements AdapterView.OnItemClickListener {

    ListView noticeBoardList;
    RelativeLayout relativeLayout;
    CommanListViewAdapter commanListViewAdapter;
    String[] nameList = {"Pending Event", "Published Event"};

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View adminEvents = inflater.inflate(R.layout.fragment_event_admin, container, false);
        Utilities.ladooIntegration(getActivity(), "Admin Events List");
        relativeLayout = (RelativeLayout) adminEvents.findViewById(R.id.relativeMain);
        noticeBoardList = (ListView) adminEvents.findViewById(R.id.boardList);
        commanListViewAdapter = new CommanListViewAdapter(getActivity(), nameList);
        noticeBoardList.setAdapter(commanListViewAdapter);
        noticeBoardList.setOnItemClickListener(this);

        return adminEvents;
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

        Intent intent = new Intent(getActivity(), AdminEventsActivity.class);
        intent.putExtra("AdminEvents", position);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);

    }

}
