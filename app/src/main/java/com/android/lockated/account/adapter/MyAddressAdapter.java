package com.android.lockated.account.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.android.lockated.Interfaces.IRecyclerItemClickListener;
import com.android.lockated.R;
import com.android.lockated.model.MyAddressData;
import com.android.lockated.holder.RecyclerViewHolder;

import java.util.ArrayList;

public class MyAddressAdapter extends RecyclerView.Adapter<RecyclerViewHolder>
{
    private ArrayList<MyAddressData> mMyAddressDatas;
    private IRecyclerItemClickListener mIRecyclerItemClickListener;

    public MyAddressAdapter(ArrayList<MyAddressData> data, IRecyclerItemClickListener listener)
    {
        mMyAddressDatas = data;
        mIRecyclerItemClickListener = listener;
    }

    @Override
    public RecyclerViewHolder onCreateViewHolder(ViewGroup parent, int viewType)
    {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.fragment_my_address_row, parent, false);
        return new RecyclerViewHolder(itemView, mIRecyclerItemClickListener);
    }

    @Override
    public void onBindViewHolder(RecyclerViewHolder holder, int position)
    {
        holder.onAddressViewHolder(mMyAddressDatas.get(position));
    }

    @Override
    public int getItemCount()
    {
        return mMyAddressDatas.size();
    }
}
