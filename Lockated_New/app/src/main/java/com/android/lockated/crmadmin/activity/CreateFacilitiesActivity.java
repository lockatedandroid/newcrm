package com.android.lockated.crmadmin.activity;

import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;

import com.android.lockated.R;
import com.android.lockated.crmadmin.fragment.CreateFacilitiesFragment;


public class CreateFacilitiesActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_facilities);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(true);
        getSupportActionBar().setTitle(R.string.create_facilities);

        init();

    }

    private void init() {

        FragmentManager fragmentManager = getSupportFragmentManager();
        CreateFacilitiesFragment createFacilitiesFragment = new CreateFacilitiesFragment();
        createFacilitiesFragment.setFragmentManager(fragmentManager);
        getSupportFragmentManager().beginTransaction().replace(R.id.ContainerList, createFacilitiesFragment).commit();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home: {
                super.onBackPressed();
                return true;
            }
            default:
                return super.onOptionsItemSelected(item);
        }
    }

}
