package com.android.lockated.crmadmin.adapter;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.android.lockated.R;
import com.android.lockated.crmadmin.activity.AdminNoticeBoardActivity;
import com.android.lockated.crmadmin.activity.AdminNoticeDetailActivity;
import com.android.lockated.information.AccountController;
import com.android.lockated.model.AccountData;
import com.android.lockated.model.AdminModel.Notice.PublishNotice.Notice;
import com.android.lockated.model.AdminModel.Notice.PublishNotice.PublishNoticeboard;
import com.android.lockated.network.ConnectionDetector;
import com.android.lockated.preferences.LockatedPreferences;
import com.android.lockated.request.LockatedJSONObjectRequest;
import com.android.lockated.request.LockatedRequestError;
import com.android.lockated.request.LockatedVolleyRequestQueue;
import com.android.lockated.utils.ApplicationURL;
import com.android.lockated.utils.Utilities;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

public class PublishedNoticeAdapter extends RecyclerView.Adapter<PublishedNoticeAdapter.PublishedNoticeViewHolder> {

    private final ArrayList<PublishNoticeboard> publishNoticeboardArrayList;
    Context contextMain;
    JSONArray jsonArray;
    FragmentManager childFragmentManager;
    AccountController accountController;
    ArrayList<AccountData> datas;
    ProgressDialog mProgressDialog;
    private String REQUEST_TAG = "PublishedNoticeAdapter";
    LockatedPreferences mLockatedPreferences;
    RequestQueue mQueue;

    public PublishedNoticeAdapter(Context context, ArrayList<PublishNoticeboard> publishNoticeboardArrayList) {
        this.contextMain = context;
        this.publishNoticeboardArrayList = publishNoticeboardArrayList;
        accountController = AccountController.getInstance();
        datas = accountController.getmAccountDataList();
    }


    @Override
    public PublishedNoticeViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(contextMain).inflate(R.layout.notice_parent_view, parent, false);
        PublishedNoticeViewHolder pvh = new PublishedNoticeViewHolder(v);
        return pvh;
    }

    @Override
    public void onBindViewHolder(PublishedNoticeViewHolder holder, int position) {

        String dateSep = "(" + publishNoticeboardArrayList.get(position).getDate() + ")";
        holder.dateSeperator.setText(dateSep);
        PublishedNoticeCardAdapter eventGridAdapter = new PublishedNoticeCardAdapter(contextMain,
                publishNoticeboardArrayList.get(position).getNotices());
        holder.recycler.setAdapter(eventGridAdapter);
    }

    @Override
    public int getItemCount() {

        return publishNoticeboardArrayList.size();
        //jsonArray.length();
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }

    public class PublishedNoticeViewHolder extends RecyclerView.ViewHolder {
        RecyclerView recycler;
        TextView dateSeperator;

        public PublishedNoticeViewHolder(View itemView) {
            super(itemView);

            dateSeperator = (TextView) itemView.findViewById(R.id.dateStamp);
            recycler = (RecyclerView) itemView.findViewById(R.id.recyclerViewFirstAdapter);
            LinearLayoutManager linearLayoutManager = new LinearLayoutManager(contextMain,
                    LinearLayoutManager.HORIZONTAL, false);
            recycler.setLayoutManager(linearLayoutManager);

        }
    }

    //======================================================================================================

    public class PublishedNoticeCardAdapter extends RecyclerView.Adapter<PublishedNoticeCardAdapter.NoticeGridViewHolder>
            implements Response.ErrorListener, Response.Listener<JSONObject>, View.OnClickListener {

        private final ArrayList<Notice> notices;
        Context contextSub;
        JSONArray jsonArraySub;

        public PublishedNoticeCardAdapter(Context context, ArrayList<Notice> notices) {
            this.contextSub = context;
            this.notices = notices;
            this.jsonArraySub = jsonArray;
        }

        @Override
        public NoticeGridViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View v = LayoutInflater.from(contextSub).inflate(R.layout.admin_notice_child_view, parent, false);
            NoticeGridViewHolder noticeGridViewHolder = new NoticeGridViewHolder(v);
            return noticeGridViewHolder;
        }

        @Override
        public void onBindViewHolder(NoticeGridViewHolder holder, int position) {
            Calendar c = Calendar.getInstance();
            SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
            String formattedDate = df.format(c.getTime());
            try {
                holder.dateTxt1.setText(Utilities.dateConvertMinify(notices.get(position).getExpireTime()));
                if (notices.get(position).getNoticeHeading() != null) {
                    holder.subNameTxt1.setText(notices.get(position).getNoticeHeading());
                } else {
                    holder.subNameTxt1.setText("No Subject");
                }
                if (notices.get(position).getUsername() != null) {
                    holder.postedNameTxt1.setText(notices.get(position).getUsername());
                } else {
                    holder.postedNameTxt1.setText("No Name");
                }
            } catch (Exception e) {
                //Log.e("NGA error", "" + e);
                e.printStackTrace();
            }

            holder.mDetailView.setTag(position);
            holder.mDetailView.setOnClickListener(this);
            holder.mDisable.setTag(position);
            holder.mDisable.setOnClickListener(this);
        }

        @Override
        public int getItemCount() {
            return notices.size();
        }

        @Override
        public void onAttachedToRecyclerView(RecyclerView recyclerView) {
            super.onAttachedToRecyclerView(recyclerView);
        }

        @Override
        public void onClick(View v) {
            int pos = (int) v.getTag();
            switch (v.getId()) {
                case R.id.mDetailView:
                    viewCardDetail(contextSub, notices.get(pos));
                    break;
                case R.id.mDisable:
                    //  String spid = String.valueOf(publishNoticeboardArrayList.get(pos).getNotices().get(pos).getId());
                    String spid = String.valueOf(notices.get(pos).getId());
                    try {
                        request(null, ApplicationURL.getRejectNoticeUrl
                                + spid);
                    } catch (Exception e) {
                        e.printStackTrace();
                        break;
                    }
            }
        }

        private void viewCardDetail(Context context, Notice notice) {
            JSONObject jsonObjectMain = new JSONObject();
            try {
                jsonObjectMain.put("event_type", "Notice");
                jsonObjectMain.put("username", notice.getUsername());
                jsonObjectMain.put("notice_heading", notice.getNoticeHeading());
                jsonObjectMain.put("notice_text", notice.getNoticeText());
                jsonObjectMain.put("created_at", notice.getCreatedAt());
                jsonObjectMain.put("flat", notice.getFlat());
                jsonObjectMain.put("id", "" + notice.getId());
                jsonObjectMain.put("VisiblityValue", "1");
                String date;
                if (notice.getExpireTime() != null && notice.getExpireTime() != "null") {
                    date = Utilities.dateConvertMinify(notice.getExpireTime());
                } else {
                    date = "No Date";
                }
                jsonObjectMain.putOpt("expire_time", date);
                if (notice.getDocuments().size() > 0 && notice.getDocuments() != null) {
                    jsonObjectMain.putOpt("document", notice.getDocuments().get(0).getDocument());
                } else {
                    jsonObjectMain.putOpt("document", "No Document");
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
            Intent intent = new Intent(context, AdminNoticeDetailActivity.class);
            intent.putExtra("NoticeDetailViewData", jsonObjectMain.toString());
            context.startActivity(intent);
        }

        public class NoticeGridViewHolder extends RecyclerView.ViewHolder {

            TextView dateTxt1, timeTxt1, subNameTxt1, postedNameTxt1, mDisable, mDetailView;
            CardView cardView;


            public NoticeGridViewHolder(View itemView) {
                super(itemView);

                cardView = (CardView) itemView.findViewById(R.id.cardView);
                dateTxt1 = (TextView) itemView.findViewById(R.id.date1);
                subNameTxt1 = (TextView) itemView.findViewById(R.id.subjectName1);
                postedNameTxt1 = (TextView) itemView.findViewById(R.id.postedName1);
                mDisable = (TextView) itemView.findViewById(R.id.mDisable);
                mDetailView = (TextView) itemView.findViewById(R.id.mDetailView);
            }
        }

        public void request(JSONObject jsonObject, String url) {
            mProgressDialog = ProgressDialog.show(contextMain, "", "Please Wait...", false);
            mProgressDialog.show();
            if (contextSub != null) {
                if (ConnectionDetector.isConnectedToInternet(contextSub)) {
                    mLockatedPreferences = new LockatedPreferences(contextSub);
                    String urlMain = url + "&token=" + mLockatedPreferences.getLockatedToken()
                            + "&id_society=" + mLockatedPreferences.getSocietyId();
                    LockatedVolleyRequestQueue lockatedVolleyRequestQueue = LockatedVolleyRequestQueue.getInstance(contextSub);
                    lockatedVolleyRequestQueue.sendRequest(REQUEST_TAG, Request.Method.GET, urlMain, jsonObject, this, this);
                    /*mQueue = LockatedVolleyRequestQueue.getInstance(contextSub).getRequestQueue();
                    LockatedJSONObjectRequest lockatedJSONObjectRequest = new LockatedJSONObjectRequest(Request.Method.GET,
                            url + "&token=" + mLockatedPreferences.getLockatedToken()
                                    + "&id_society=" + mLockatedPreferences.getSocietyId(), jsonObject, this, this);
                    *//*Log.e("request", "" + url + "&token=" + mLockatedPreferences.getLockatedToken()
                            + "&id_society=" + mLockatedPreferences.getSocietyId());*//*
                    lockatedJSONObjectRequest.setTag(REQUEST_TAG);
                    mQueue.add(lockatedJSONObjectRequest);*/
                } else {
                    Utilities.showToastMessage(contextSub, contextSub.getResources().getString(R.string.internet_connection_error));
                }
            }

        }

        @Override
        public void onErrorResponse(VolleyError error) {
            mProgressDialog.dismiss();
            if (contextSub != null) {
                LockatedRequestError.onRequestError(contextSub, error);
            }
        }

        @Override
        public void onResponse(JSONObject response) {
            //Log.e("response", "" + response);
            mProgressDialog.dismiss();
            if (contextSub != null) {
                try {
                    if (response != null && response.length() > 0) {
                        Intent intent = new Intent(contextSub, AdminNoticeBoardActivity.class);
                        intent.putExtra("AdminNotice", 1);
                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        contextSub.startActivity(intent);
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }

    }
}