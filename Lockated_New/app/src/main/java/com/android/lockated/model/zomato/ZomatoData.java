
package com.android.lockated.model.zomato;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class ZomatoData {

    @SerializedName("thirdparty_transactions")
    @Expose
    private ArrayList<ThirdpartyTransaction> thirdpartyTransactions = new ArrayList<ThirdpartyTransaction>();

    /**
     * 
     * @return
     *     The thirdpartyTransactions
     */
    public ArrayList<ThirdpartyTransaction> getThirdpartyTransactions() {
        return thirdpartyTransactions;
    }

    /**
     * 
     * @param thirdpartyTransactions
     *     The thirdparty_transactions
     */
    public void setThirdpartyTransactions(ArrayList<ThirdpartyTransaction> thirdpartyTransactions) {
        this.thirdpartyTransactions = thirdpartyTransactions;
    }

}
