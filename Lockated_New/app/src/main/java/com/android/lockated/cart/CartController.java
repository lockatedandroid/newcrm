package com.android.lockated.cart;

import android.util.Log;

import com.android.lockated.categories.grocery.model.GroceryItemData;
import com.android.lockated.categories.vendors.model.ProductData;

import java.util.ArrayList;

public class CartController {
    private static CartController instance;
    public ArrayList<GroceryItemData> mGroceryCartDataList;
    public ArrayList<ProductData> mVendorProductDataList;

    public CartController() {
        mGroceryCartDataList = new ArrayList<>();
        mVendorProductDataList = new ArrayList<>();
    }

    public static CartController getInstance() {
        if (instance == null) {
            instance = new CartController();
        }
        return instance;
    }

    public ArrayList<GroceryItemData> getGroceryCartDataList() {
        return mGroceryCartDataList;
    }

    public ArrayList<ProductData> getVendorProductDataList() {
        return mVendorProductDataList;
    }

    public void resetItems() {
        mGroceryCartDataList.clear();
        mVendorProductDataList.clear();
    }
}
