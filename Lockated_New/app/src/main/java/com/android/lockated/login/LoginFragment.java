package com.android.lockated.login;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.text.method.HideReturnsTransformationMethod;
import android.text.method.PasswordTransformationMethod;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.lockated.HomeActivity;
import com.android.lockated.LockatedApplication;
import com.android.lockated.R;
import com.android.lockated.information.AccountController;
import com.android.lockated.model.BaseApi.BaseAccountApi;
import com.android.lockated.network.ConnectionDetector;
import com.android.lockated.preferences.LockatedPreferences;
import com.android.lockated.register.OTPGeneratorFragment;
import com.android.lockated.register.ResetPasswordFragment;
import com.android.lockated.request.LockatedRequestError;
import com.android.lockated.request.LockatedVolleyRequestQueue;
import com.android.lockated.utils.ApplicationURL;
import com.android.lockated.utils.Utilities;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;

import org.json.JSONException;
import org.json.JSONObject;

public class LoginFragment extends Fragment implements View.OnClickListener, Response.Listener, Response.ErrorListener {
    private View mLoginView;

    private TextView mTextViewSignUp;
    private TextView mTextViewSignIn;
    private TextView mTextSignUpBar;
    private TextView mTextSignInBar;
    private TextView mButtonSignIn;
    private TextView mTextViewPassword;
    private TextView mTextViewEmailAddress;
    private TextView mTextViewForgotPassword;
    private ImageView mImageViewShowPassword;
    private EditText mEditTextEmail;
    private EditText mEditTextPassword;

    private String mToken;
    private String mNumber;
    private boolean isPasswordVisible;
    private String OTP;
    private RequestQueue mQueue;
    private ProgressDialog mProgressDialog;
    private AccountController mAccountController;
    private LockatedPreferences mLockatedPreferences;
    private LockatedVolleyRequestQueue mLockatedVolleyRequestQueue;

    public static final String REQUEST_TAG = "LoginFragment";
    public static final String REQUEST_OTP = "ForgotPassword";

    public static final String FORGOT_REQUEST_TAG = "ForgotPassword";
    public static final String ACCOUNT_REQUEST_TAG = "AccountDetail";

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mLoginView = inflater.inflate(R.layout.fragment_login, container, false);

        init();
        setSignInTabBar();
        hideEditTextEmailHint();
        hideEditTextPasswordHint();

        return mLoginView;
    }

    @Override
    public void onResume() {
        super.onResume();
        LockatedApplication.getInstance().trackScreenView(getActivity().getString(R.string.login_screen));
    }

    private void init() {
        mAccountController = AccountController.getInstance();
        mLockatedPreferences = new LockatedPreferences(getActivity());
        mTextViewSignUp = (TextView) getActivity().findViewById(R.id.mTextViewSignUp);
        mTextViewSignIn = (TextView) getActivity().findViewById(R.id.mTextViewSignIn);
        mTextSignUpBar = (TextView) getActivity().findViewById(R.id.mTextSignUpBar);
        mTextSignInBar = (TextView) getActivity().findViewById(R.id.mTextSignInBar);
        mButtonSignIn = (TextView) mLoginView.findViewById(R.id.mButtonSignIn);
        mTextViewPassword = (TextView) mLoginView.findViewById(R.id.mTextViewPassword);
        mTextViewEmailAddress = (TextView) mLoginView.findViewById(R.id.mTextViewEmailAddress);
        mTextViewForgotPassword = (TextView) mLoginView.findViewById(R.id.mTextViewForgotPassword);
        mImageViewShowPassword = (ImageView) mLoginView.findViewById(R.id.mImageViewShowPassword);
        mEditTextEmail = (EditText) mLoginView.findViewById(R.id.mEditTextEmail);
        mEditTextPassword = (EditText) mLoginView.findViewById(R.id.mEditTextPassword);

        mButtonSignIn.setOnClickListener(this);
        mImageViewShowPassword.setOnClickListener(this);
        mTextViewForgotPassword.setOnClickListener(this);

        setEditTextListener();
        hideShowPassword();
        disableSignUpButton();
    }

    private void disableSignUpButton() {
        mButtonSignIn.setEnabled(false);
        mButtonSignIn.setTextColor(ContextCompat.getColor(getActivity(), R.color.secondary_text));
    }

    private void enableSignUpButton() {
        mButtonSignIn.setEnabled(true);
        mButtonSignIn.setTextColor(ContextCompat.getColor(getActivity(), R.color.primary));
    }

    private void hideShowPassword() {
        mImageViewShowPassword.setVisibility(View.GONE);
    }

    private void unnHideShowPassword() {
        mImageViewShowPassword.setVisibility(View.VISIBLE);
    }

    private void showEditTextEmailHint() {
        mTextViewEmailAddress.setVisibility(View.VISIBLE);
    }

    private void showEditTextPasswordHint() {
        mTextViewPassword.setVisibility(View.VISIBLE);
    }

    private void hideEditTextEmailHint() {
        mTextViewEmailAddress.setVisibility(View.INVISIBLE);
    }

    private void hideEditTextPasswordHint() {
        mTextViewPassword.setVisibility(View.INVISIBLE);
    }

    private void setSignInTabBar() {
        mTextViewSignUp.setVisibility(View.VISIBLE);
        mTextViewSignIn.setVisibility(View.VISIBLE);
        mTextSignUpBar.setVisibility(View.VISIBLE);
        mTextSignInBar.setVisibility(View.VISIBLE);

        mTextViewSignIn.setTextColor(ContextCompat.getColor(getActivity(), R.color.primary));
        mTextViewSignUp.setTextColor(ContextCompat.getColor(getActivity(), R.color.primary_text));
        mTextSignInBar.setBackgroundColor(ContextCompat.getColor(getActivity(), R.color.primary));
        mTextSignUpBar.setBackgroundColor(ContextCompat.getColor(getActivity(), R.color.colorThemeAccent));
    }

    private void onShowPasswordClicked() {
        if (isPasswordVisible) {
            isPasswordVisible = false;
            mEditTextPassword.setTransformationMethod(PasswordTransformationMethod.getInstance());
            mEditTextPassword.setSelection(mEditTextPassword.getText().length());
        } else {
            isPasswordVisible = true;
            mEditTextPassword.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
            mEditTextPassword.setSelection(mEditTextPassword.getText().length());
        }
    }

    private void setEditTextListener() {
        mEditTextEmail.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.length() > 0) {
                    showEditTextEmailHint();

                    if (mEditTextPassword.getText().length() > 0) {
                        enableSignUpButton();
                    }
                } else {
                    disableSignUpButton();
                    hideEditTextEmailHint();
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        mEditTextPassword.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.length() > 0) {
                    showEditTextPasswordHint();
                    unnHideShowPassword();

                    if (mEditTextEmail.getText().toString().trim().length() > 0) {
                        enableSignUpButton();
                    }
                } else {
                    disableSignUpButton();
                    hideEditTextPasswordHint();
                    hideShowPassword();
                    if (isPasswordVisible) {
                        isPasswordVisible = false;
                        mEditTextPassword.setTransformationMethod(PasswordTransformationMethod.getInstance());
                        mEditTextPassword.setSelection(mEditTextPassword.getText().length());
                    }
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }

    private void onSignInButtonClicked() {
        // get username and password in the String variable
        String strLoginUsername = mEditTextEmail.getText().toString().trim();
        String strLoginPassword = mEditTextPassword.getText().toString();

        boolean loginCancel = false;
        View loginFocusView = null;

        // Validation for Password field, it should not be blank
        if (TextUtils.isEmpty(strLoginPassword)) {
            loginFocusView = mEditTextPassword;
            loginCancel = true;
        }

        // Validation for User name field, it should not be blank
        if (TextUtils.isEmpty(strLoginUsername)) {
            loginFocusView = mEditTextEmail;
            loginCancel = true;
        }

        if (loginCancel) {
            loginFocusView.requestFocus();
            Utilities.showToastMessage(getActivity(), getActivity().getResources().getString(R.string.login_blank_field_error));
        } else {
            if (ConnectionDetector.isConnectedToInternet(getActivity())) {
                mProgressDialog = ProgressDialog.show(getActivity(), "", "Please Wait...", false);
                mProgressDialog.show();

                JSONObject jsonObject = new JSONObject();
                JSONObject jsonObjectFields = new JSONObject();
                try {
                    jsonObjectFields.put("email", strLoginUsername);
                    jsonObjectFields.put("password", strLoginPassword);
                    jsonObject.put("user", jsonObjectFields);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                String url = ApplicationURL.loginUrl;
                mLockatedVolleyRequestQueue = LockatedVolleyRequestQueue.getInstance(getActivity());
                mLockatedVolleyRequestQueue.sendRequest(REQUEST_TAG, Request.Method.POST, url, jsonObject, this, this);

                Utilities.lockatedGoogleAnalytics(getString(R.string.login), getString(R.string.login), getString(R.string.login));
            } else {
                Utilities.showToastMessage(getActivity(), getActivity().getResources().getString(R.string.internet_connection_error));
            }
        }
    }

    private void openResetPasswordFragment() {
        String mobileNumber = mEditTextEmail.getText().toString();
        if (!getActivity().isFinishing()) {
            Bundle bundle = new Bundle();
            bundle.putString("mobileNumber", mobileNumber);
            ResetPasswordFragment resetPasswordFragment = new ResetPasswordFragment();
            resetPasswordFragment.setResetPasswordFragment("LoginFragment");
            resetPasswordFragment.setArguments(bundle);
            getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.container, resetPasswordFragment).commitAllowingStateLoss();
        }
    }

    private void onForgotPasswordClicked() {
        String strLoginUsername = mEditTextEmail.getText().toString();

        boolean loginCancel = false;
        View loginFocusView = null;

        if (TextUtils.isEmpty(strLoginUsername)) {
            loginFocusView = mEditTextEmail;
            loginCancel = true;
        }

        if (loginCancel) {
            loginFocusView.requestFocus();
            Utilities.showToastMessage(getActivity(), getActivity().getResources().getString(R.string.email_blank_field_error));
        } else {
            if (ConnectionDetector.isConnectedToInternet(getActivity())) {
                mProgressDialog = ProgressDialog.show(getActivity(), "", "Please Wait...", false);
                mProgressDialog.show();

                JSONObject jsonObject = new JSONObject();
                JSONObject jsonObjectFields = new JSONObject();
                try {
                    jsonObjectFields.put("email", strLoginUsername);
                    jsonObject.put("user", jsonObjectFields);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                String url = ApplicationURL.forgotPasswordUrl;
                mLockatedVolleyRequestQueue = LockatedVolleyRequestQueue.getInstance(getActivity());
                mLockatedVolleyRequestQueue.sendRequest(FORGOT_REQUEST_TAG, Request.Method.POST, url, jsonObject, this, this);

                Utilities.lockatedGoogleAnalytics(getString(R.string.text_forgot_password), getString(R.string.text_forgot_password), getString(R.string.text_forgot_password));
            } else {
                Utilities.showToastMessage(getActivity(), getActivity().getResources().getString(R.string.internet_connection_error));
            }
        }
    }

    private void getScreenDetail() {
        if (ConnectionDetector.isConnectedToInternet(getActivity())) {
            mProgressDialog = ProgressDialog.show(getActivity(), "", "Please Wait...", false);
            mProgressDialog.show();
            String url = ApplicationURL.getBaseAccountApi +/* "?token=" +*/ mToken;
            //Log.e("getScreenDetail", "" + url);
            mLockatedVolleyRequestQueue = LockatedVolleyRequestQueue.getInstance(getActivity());
            mLockatedVolleyRequestQueue.sendRequest(ACCOUNT_REQUEST_TAG, Request.Method.GET, url, null, this, this);
        } else {
            Utilities.showToastMessage(getActivity(), getActivity().getResources().getString(R.string.internet_connection_error));
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.mButtonSignIn:
                onSignInButtonClicked();
                break;

            case R.id.mImageViewShowPassword:
                onShowPasswordClicked();
                break;
            case R.id.mTextViewForgotPassword:
                // onForgotPasswordClicked();
                onSendOtpClicked();
                // openResetPasswordFragment();
                break;
        }
    }

    private void onSendOtpClicked() {
        String strUserPhone = mEditTextEmail.getText().toString();
        boolean loginCancel = false;
        View loginFocusView = null;

        if (TextUtils.isEmpty(strUserPhone)) {
            loginFocusView = mEditTextEmail;
            loginCancel = true;
        }

        if (loginCancel) {
            loginFocusView.requestFocus();
            Utilities.showToastMessage(getActivity(), getActivity().getResources().getString(R.string.login_blank_field_error));
        } else {
            if (ConnectionDetector.isConnectedToInternet(getActivity())) {
                mProgressDialog = ProgressDialog.show(getActivity(), "", "Please Wait...", false);
                mProgressDialog.show();

                JSONObject jsonObjectFields = new JSONObject();
                try {
                    jsonObjectFields.put("request_otp", "1");
                    jsonObjectFields.put("mobile", strUserPhone);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                String url = ApplicationURL.getForgotPasswordOtp;
                mLockatedVolleyRequestQueue = LockatedVolleyRequestQueue.getInstance(getActivity());
                mLockatedVolleyRequestQueue.sendRequest(REQUEST_OTP, Request.Method.POST, url, jsonObjectFields, this, this);
                Utilities.lockatedGoogleAnalytics(getString(R.string.login), getString(R.string.login), getString(R.string.login));
            } else {
                Utilities.showToastMessage(getActivity(), getActivity().getResources().getString(R.string.internet_connection_error));
            }
        }
    }

    @Override
    public void onErrorResponse(VolleyError error) {
        mProgressDialog.dismiss();
        if (getActivity() != null) {
            LockatedRequestError.onRequestError(getActivity(), error);
        }
    }

    @Override
    public void onResponse(Object response) {
        mProgressDialog.dismiss();
        if (getActivity() != null) {
            JSONObject jsonObject = (JSONObject) response;
            /*try {
                if (jsonObject.has("code") && jsonObject.has("message") && jsonObject.has("otp")) {
                    Utilities.showToastMessage(getActivity(), jsonObject.getString("message"));
                    openResetPasswordFragment();
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }*/
            try {
                if (mLockatedVolleyRequestQueue.getLockatedJSONObjectRequest().getTag().toString().equals(REQUEST_OTP)) {
                    if (jsonObject.has("code") && jsonObject.has("message") && jsonObject.has("otp")) {
                        Utilities.showToastMessage(getActivity(), jsonObject.getString("message"));
                        Toast.makeText(getActivity(), "SMS will be auto detected", 20000).show();
                        openResetPasswordFragment();
                    }
                } else if (mLockatedVolleyRequestQueue.getLockatedJSONObjectRequest().getTag().toString().equals(FORGOT_REQUEST_TAG)) {
                    String message = jsonObject.getString("message");
                    Utilities.showToastMessage(getActivity(), message);
                } else if (mLockatedVolleyRequestQueue.getLockatedJSONObjectRequest().getTag().toString().equals(REQUEST_TAG)) {
                    int code = jsonObject.optInt("code");
                    if (code == 200) {
                        mLockatedPreferences.setLogout(false);
                        mToken = jsonObject.getString("spree_api_key");
                        mLockatedPreferences.setLockatedUserId(jsonObject.getString("id"));
                        getScreenDetail();
                    } else {
                        Utilities.showToastMessage(getActivity(), jsonObject.getString("error"));
                    }
                } else if (mLockatedVolleyRequestQueue.getLockatedJSONObjectRequest().getTag().toString().equals(ACCOUNT_REQUEST_TAG)) {
                  /*  int numberVerified = jsonObject.getInt("number_verified");
                    String token = jsonObject.getString("spree_api_key");
                    String number = jsonObject.getString("mobile");

                    mLockatedPreferences.setLockatedToken(token);
                    mLockatedPreferences.setContactNumber(number);

                    AccountData accountData = new AccountData(jsonObject);
                    //Log.e("accountData.getSelected_user_society()", "" + accountData.getSelected_user_society());
                    mLockatedPreferences.setUserSocietyId(accountData.getSelected_user_society());
                    mLockatedPreferences.setAccountData(accountData);
                    mAccountController.mAccountDataList.add(accountData);

                    //----------------Ola Changes-------------------------------------

                    Log.e("Ola Token","Before set"+ mLockatedPreferences.getOlaToken());
                    mLockatedPreferences.setOlaToken(mAccountController.mAccountDataList.get(0).getOlaToken());
                    Log.e("Ola Token","After set"+ mLockatedPreferences.getOlaToken());

                    //----------------------------------------------------------------------

                    //----------------Zomato Changes-------------------------------------

                    Log.e("Zomato Token","Before set"+ mLockatedPreferences.getZomatoToken());
                    mLockatedPreferences.setZomatoToken(mAccountController.mAccountDataList.get(0).getZomatotoken());
                    Log.e("Zomato Token","After set"+ mLockatedPreferences.getZomatoToken());

                    //----------------------------------------------------------------------

                    Log.e("AccountDataList",""+mAccountController.mAccountDataList);

                    if (numberVerified == 0) {
                        openOTPFragment();
                    } else {
                        openHomeActivity();
                    }*/
                    if (jsonObject.has("number_verified")) {
                        Log.e("number_verified", "Login" + response);
                        String token = jsonObject.getString("spree_api_key");
                        Log.e("token", "token" + token);
                        String number = jsonObject.getString("mobile");
                        Log.e("number", "token" + number);
                        int numberVerified = jsonObject.getInt("number_verified");
                        Log.e("numberVerified", "numberVerified" + numberVerified);
                        mLockatedPreferences.setLockatedToken(token);
                        mLockatedPreferences.setSocietyId(jsonObject.getJSONObject("society").getString("id"));
                        mLockatedPreferences.setUserSocietyId(jsonObject.getInt("selected_user_society"));
                        mLockatedPreferences.setContactNumber(jsonObject.getString("mobile"));
                        mLockatedPreferences.setImageAvatar(jsonObject.getJSONObject("avatar").getString("medium"));

                        BaseAccountApi baseAccountApi = new BaseAccountApi(jsonObject);
                        mAccountController = AccountController.getInstance();
                        mAccountController.mBaseAccountsApi.add(baseAccountApi);
                       /* mAccountController.getBaseAccountsData().get(0).getMobile();*/
                        Log.e("getMobile",""+ mAccountController.getBaseAccountsData().get(0).getMobile());

                        //----------------Ola Changes-------------------------------------

                        Log.e("Ola Token","Before set"+ mLockatedPreferences.getOlaToken());
                        mLockatedPreferences.setOlaToken(mAccountController.mBaseAccountsApi.get(0).getOlatoken());
                        Log.e("Ola Token","After set"+ mLockatedPreferences.getOlaToken());

                        //----------------------------------------------------------------------

                        //----------------Zomato Changes-------------------------------------

                        Log.e("Zomato Token","Before set"+ mLockatedPreferences.getZomatoToken());
                        mLockatedPreferences.setZomatoToken(mAccountController.mBaseAccountsApi.get(0).getZomatotoken());
                        Log.e("Zomato Token","After set"+ mLockatedPreferences.getZomatoToken());

                        //----------------------------------------------------------------------

                        Log.e("AccountDataList",""+mAccountController.mAccountDataList);

                        mLockatedPreferences.setNotificationValue(false);
                       Log.e("Login numberVerified",""+numberVerified);
                        if (numberVerified == 0) {
                            openOTPFragment();
                        } else {
                            Log.e("Login openHomeActivity","openHomeActivity");
                            openHomeActivity();
                            //mLockatedPreferences.setLogout(false);
                        }
                    }
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    private void openOTPFragment() {
        if (getActivity() != null) {
            getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.container, new OTPGeneratorFragment()).commitAllowingStateLoss();
        }
    }

    private void openHomeActivity() {
        if (getActivity() != null) {
            Intent intent = new Intent(getActivity(), HomeActivity.class);
            startActivity(intent);
            getActivity().finish();
        }
    }
}
