package com.android.lockated.crmadmin.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.lockated.R;
import com.android.lockated.crmadmin.activity.AdminFacilitiesActivity;
import com.android.lockated.model.AdminModel.AdminFacilities.Booked.Booked;
import com.android.lockated.network.ConnectionDetector;
import com.android.lockated.preferences.LockatedPreferences;
import com.android.lockated.request.LockatedRequestError;
import com.android.lockated.request.LockatedVolleyRequestQueue;
import com.android.lockated.utils.ApplicationURL;
import com.android.lockated.utils.Utilities;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;


public class AdminBookedFacilitiesAdapter extends RecyclerView.Adapter<AdminBookedFacilitiesAdapter.AdminBookedFacilitiesViewHolder>
        implements Response.ErrorListener, Response.Listener<JSONObject> {

    private final ArrayList<Booked> booked;
    LayoutInflater layoutInflater;
    Context context;
    JSONArray jsonArray;
    FragmentManager childFragmentManager;
    String APPROVE_FACILITIES = "AdminBookedFacilitiesAdapter";
    String commentsValue;

    public AdminBookedFacilitiesAdapter(Context context, FragmentManager childFragmentManager, ArrayList<Booked> booked) {

        this.context = context;
        layoutInflater = LayoutInflater.from(context);
        this.booked = booked;
        this.childFragmentManager = childFragmentManager;
    }

    @Override
    public AdminBookedFacilitiesViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(context).inflate(R.layout.facilities_admin_book_facilities, parent, false);
        AdminBookedFacilitiesViewHolder facilitiesViewHolder = new AdminBookedFacilitiesViewHolder(v);
        return facilitiesViewHolder;
    }

    @Override
    public void onBindViewHolder(AdminBookedFacilitiesViewHolder holder, final int position) {
        if (booked.get(position) != null && booked.get(position).getAmenity() != null) {
            holder.venueName.setText(booked.get(position).getAmenity().getName());
            String cost = String.valueOf(booked.get(position).getAmenity().getCost());
            if (cost.equals("0.0")) {
                holder.costValue.setText("Free");
            } else {
                holder.costValue.setText(cost);
            }
            holder.costTypeValue.setText(booked.get(position).getAmenity().getCostType());
        } else {
            holder.venueName.setText("No Data");
            holder.costValue.setText("No Data");
            holder.costTypeValue.setText("No Data");
        }
        holder.bookPurpose.setText(booked.get(position).getBookPurpose());
        holder.createdDate.setText(Utilities.ddMonYYYYTimeFormat(booked.get(position).getCreatedAt()));
        String personno = String.valueOf(booked.get(position).getPersonNo());
        if (personno != null) {
            holder.numberPerson.setText(personno);
        } else {
            holder.numberPerson.setText("No Data");
        }
        if (booked.get(position).getStatus().equalsIgnoreCase("rejected")) {
            holder.statusValue.setText(booked.get(position).getStatus());
            holder.linearBottom.setVisibility(View.GONE);
        } else if (booked.get(position).getStatus().equalsIgnoreCase("Approved")) {
            holder.statusValue.setText(booked.get(position).getStatus());
            holder.approve.setVisibility(View.GONE);
        } else if (booked.get(position).getStatus().equalsIgnoreCase("Pending")) {
            holder.linearBottom.setVisibility(View.VISIBLE);
            holder.statusValue.setText(booked.get(position).getStatus());
        }

        if (booked.get(position).getUser() != null) {
            if (booked.get(position).getUser().getFirstname() != null) {
                String name = booked.get(position).getUser().getFirstname()
                        + " " + booked.get(position).getUser().getLastname();
                holder.username.setText(name);
            } else {
                holder.username.setText("No Name");
            }
        } else {
            holder.username.setText("No Name");
        }
        if (booked.get(position).getFlat() != null && !booked.get(position).getFlat().equals("")) {
            holder.flatNo.setText(booked.get(position).getFlat());
        } else {
            holder.flatNo.setText("No Flat");
        }

        holder.fromDate.setText(Utilities.ddMonYYYYTimeFormat(booked.get(position).getStartdate()));
        holder.toDate.setText(Utilities.ddMonYYYYTimeFormat(booked.get(position).getEnddate()));
        holder.approve.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String approve = String.valueOf(booked.get(position).getApprove());
                if (!approve.equals("1")) {
                    loadApproveView(String.valueOf("" + booked.get(position).getId()));
                } else {
                    Utilities.showToastMessage(context, "This facility is already approved");
                }
            }
        });

        holder.comments.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                commentsValue = String.valueOf(s);
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });
        holder.reject.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String approve = String.valueOf(booked.get(position).getApprove());
                if (!approve.equals("0")) {
                    loadRejectView(String.valueOf(booked.get(position).getId()), commentsValue);
                } else {
                    Utilities.showToastMessage(context, "This facility is already rejected");
                }
            }
        });


    }

    @Override
    public int getItemCount() {
        return booked.size(); //jsonArray.length();
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }

    public class AdminBookedFacilitiesViewHolder extends RecyclerView.ViewHolder {

        TextView venueName, costValue, approve, reject /*,bookDuration*/, bookPurpose, createdDate,
                numberPerson, fromDate, toDate, username, flatNo, costTypeValue, statusValue;
        EditText comments;
        LinearLayout linearBottom;

        public AdminBookedFacilitiesViewHolder(View itemView) {
            super(itemView);

            venueName = (TextView) itemView.findViewById(R.id.venuTxt);
            costValue = (TextView) itemView.findViewById(R.id.costValue);
            costTypeValue = (TextView) itemView.findViewById(R.id.costTypeValue);
            approve = (TextView) itemView.findViewById(R.id.approveTextView);
            reject = (TextView) itemView.findViewById(R.id.rejectTextView);
            flatNo = (TextView) itemView.findViewById(R.id.userFlatValue);
            username = (TextView) itemView.findViewById(R.id.userValue);
            // bookDuration = (TextView) itemView.findViewById(R.id.bookDuration);

            bookPurpose = (TextView) itemView.findViewById(R.id.bookPurpose);
            createdDate = (TextView) itemView.findViewById(R.id.createdDate);
            numberPerson = (TextView) itemView.findViewById(R.id.numberPerson);
            statusValue = (TextView) itemView.findViewById(R.id.statusValue);
            fromDate = (TextView) itemView.findViewById(R.id.fromDate);
            toDate = (TextView) itemView.findViewById(R.id.toDate);
            comments = (EditText) itemView.findViewById(R.id.commentValue);
            linearBottom = (LinearLayout) itemView.findViewById(R.id.linearBottom);

        }
    }

    private void loadApproveView(String id) {
        if (ConnectionDetector.isConnectedToInternet(context)) {
            LockatedPreferences mLockatedPreferences = new LockatedPreferences(context);
            String url = ApplicationURL.approveBookedFacilities + id + "&approve=1&token=" + mLockatedPreferences.getLockatedToken();
            LockatedVolleyRequestQueue mLockatedVolleyRequestQueue = LockatedVolleyRequestQueue.getInstance(context);
            mLockatedVolleyRequestQueue.sendRequest(APPROVE_FACILITIES, Request.Method.GET, url, null, this, this);
        } else {
            Utilities.showToastMessage(context, context.getResources().getString(R.string.internet_connection_error));
        }
    }

    private void loadRejectView(String id, String comm) {
        if (ConnectionDetector.isConnectedToInternet(context)) {
            LockatedPreferences mLockatedPreferences = new LockatedPreferences(context);
            if (comm != null) {
                if (comm.contains(" ")) {
                    comm = comm.replaceAll(" ", "%20");
                }
            }
            String url = ApplicationURL.cancelFacilitiesUrl + id + "&approve=0&comment=" + comm + "&token="
                    + mLockatedPreferences.getLockatedToken();
            LockatedVolleyRequestQueue mLockatedVolleyRequestQueue = LockatedVolleyRequestQueue.getInstance(context);
            mLockatedVolleyRequestQueue.sendRequest(APPROVE_FACILITIES, Request.Method.GET, url, null, this, this);
        } else {
            Utilities.showToastMessage(context, context.getResources().getString(R.string.internet_connection_error));
        }
    }

    @Override
    public void onResponse(JSONObject response) {
        if (response.has("id")) {
            callIntent();
        } else if (response.has("message")) {
            callIntent();
        }
    }

    @Override

    public void onErrorResponse(VolleyError error) {
        if (context != null) {
            LockatedRequestError.onRequestError(context, error);
        }
    }

    public void callIntent() {
        Intent intent = new Intent(context, AdminFacilitiesActivity.class);
        intent.putExtra("AdminFacilitiesActivity", 0);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        context.startActivity(intent);
    }

}
