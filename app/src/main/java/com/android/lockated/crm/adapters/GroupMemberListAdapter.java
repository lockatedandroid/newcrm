package com.android.lockated.crm.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.android.lockated.R;
import com.android.lockated.model.userGroups.Groupmember;
import com.android.lockated.model.userGroups.UserGroup;

import java.util.ArrayList;

public class GroupMemberListAdapter extends BaseAdapter {

    Context context;
    ArrayList<Groupmember> groupmemberArrayList;

    public GroupMemberListAdapter(Context context, ArrayList<Groupmember> groupmemberArrayList) {
        this.context = context;
        this.groupmemberArrayList = groupmemberArrayList;
    }

    @Override
    public int getCount() {
        return groupmemberArrayList.size();
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        UserSocietyViewHolder userSocietyViewHolder;
        if (convertView == null) {
            userSocietyViewHolder = new UserSocietyViewHolder();
            convertView = LayoutInflater.from(context).inflate(R.layout.group_member_list, parent, false);
            convertView.setTag(userSocietyViewHolder);
        } else {
            userSocietyViewHolder = (UserSocietyViewHolder) convertView.getTag();
        }
        userSocietyViewHolder.memberName = (TextView) convertView.findViewById(R.id.memberName);
        userSocietyViewHolder.memberPhone = (TextView) convertView.findViewById(R.id.memberPhone);

        if (groupmemberArrayList.get(position).getUser() != null) {
            String name = groupmemberArrayList.get(position).getUser().getFirstname()
                    + " " + groupmemberArrayList.get(position).getUser().getLastname();
            String phone = groupmemberArrayList.get(position).getUser().getMobile();
            userSocietyViewHolder.memberName.setText(name);
            userSocietyViewHolder.memberPhone.setText(phone);
        }

        return convertView;
    }

    class UserSocietyViewHolder {
        TextView memberName, memberPhone;
    }

}
