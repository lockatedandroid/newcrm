package com.android.lockated.crm.p2pchat;

import android.content.Context;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;

import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.android.lockated.LockatedApplication;
import com.android.lockated.R;

import com.android.lockated.network.ConnectionDetector;
import com.android.lockated.preferences.LockatedPreferences;
import com.android.lockated.request.LockatedJSONObjectRequest;
import com.android.lockated.request.LockatedRequestError;
import com.android.lockated.request.LockatedVolleyRequestQueue;
import com.android.lockated.utils.ApplicationURL;
import com.android.lockated.utils.Utilities;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.Collections;
import java.util.Comparator;

public class PeerChatFragment extends Fragment implements View.OnClickListener, Response.Listener, Response.ErrorListener, AdapterView.OnItemClickListener {
    private View mSupportFragmentView;
    private View mSupportHeaderPreviousMessages;

    private ListView mSupportList;
    //public ListView mSupportListSample;
    private ProgressBar mProgressBarView;

    private EditText mEditTextAddMessage;
    private TextView mTextViewSupportMessageDate;
    private TextView mTextViewSupportHeaderPreviousMessages;
    private ImageView mImageViewMessageSend;
    public ImageView mImageViewSampleMessage;

    private PeerChatAdapter peerChatAdapter;

    private RequestQueue mQueue;
    private LockatedPreferences mLockatedPreferences;
    private LockatedJSONObjectRequest mLockatedJSONObjectRequest;

    private int mChatId;
    private int mPageNumber = 1;

    private String mScreenName;

    private CountDownTimer mCountDownTimer;

    private boolean hasScreenFirstView;
    private boolean isSupportScreenPaused;
    public boolean isSampleMessageSelected;

    public static final String REQUEST_TAG = "PeerChatFragment";
    public static final String PAGE_REQUEST_TAG = "PageSupportFragment";
    public static final String SEND_REQUEST_TAG = "SendSupportFragment";

    public void setChatId(int id, String name) {
        mChatId = id;
        mScreenName = name;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mSupportFragmentView = inflater.inflate(R.layout.fragment_peer_chat, container, false);
        init();
        return mSupportFragmentView;
    }

    @Override
    public void onPause() {
        super.onPause();
        if (mCountDownTimer != null) {
            mCountDownTimer.cancel();
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        if (mCountDownTimer != null) {
            mCountDownTimer.start();
        }
        LockatedApplication.getInstance().trackScreenView(mScreenName);
        LockatedApplication.getInstance().trackEvent(mScreenName, getString(R.string.visited), mScreenName);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        hasScreenFirstView = false;
    }

    private void init() {
        mLockatedPreferences = new LockatedPreferences(getActivity());

        mSupportList = (ListView) mSupportFragmentView.findViewById(R.id.mSupportList);

        mProgressBarView = (ProgressBar) mSupportFragmentView.findViewById(R.id.mProgressBarSmallView);

        mTextViewSupportMessageDate = (TextView) mSupportFragmentView.findViewById(R.id.mTextViewSupportMessageDate);
        mEditTextAddMessage = (EditText) mSupportFragmentView.findViewById(R.id.mEditTextAddMessage);
        mImageViewMessageSend = (ImageView) mSupportFragmentView.findViewById(R.id.mImageViewMessageSend);
        mImageViewSampleMessage = (ImageView) mSupportFragmentView.findViewById(R.id.mImageViewSampleMessage);

        mProgressBarView.setVisibility(View.GONE);
        mTextViewSupportMessageDate.setVisibility(View.GONE);

        mImageViewMessageSend.setOnClickListener(this);
        mImageViewSampleMessage.setOnClickListener(this);

        addHeaderView();

        peerChatAdapter = new PeerChatAdapter(getActivity());

        mSupportList.setAdapter(peerChatAdapter);

        getConversationBetweenUser();
        onEditTextChangeListener();
    }

    private void addHeaderView() {
        LayoutInflater inflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        mSupportHeaderPreviousMessages = inflater.inflate(R.layout.fragment_support_previous_messages, null);

        mTextViewSupportHeaderPreviousMessages = (TextView) mSupportHeaderPreviousMessages.findViewById(R.id.mTextViewSupportPreviousMessages);
        mSupportHeaderPreviousMessages.setVisibility(View.GONE);

        mTextViewSupportHeaderPreviousMessages.setOnClickListener(this);

        mSupportList.addHeaderView(mSupportHeaderPreviousMessages);
    }

    private void onEditTextChangeListener() {
        mEditTextAddMessage.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.length() > 0) {
                    mImageViewMessageSend.setImageResource(R.drawable.ic_message_send);
                } else {
                    mImageViewMessageSend.setImageResource(R.drawable.ic_message_send_disable);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }

    private void getConversationBetweenUser() {
        if (!isSupportScreenPaused) {
            if (ConnectionDetector.isConnectedToInternet(getActivity())) {
                mProgressBarView.setVisibility(View.VISIBLE);
                JSONObject jsonObject = new JSONObject();
                try {
                    jsonObject.put("recipient_id", mChatId);
                    jsonObject.put("sender_id", mLockatedPreferences.getLockatedUserId());
                    jsonObject.put("token", mLockatedPreferences.getLockatedToken());
                    Log.e("jsonObject", "" + jsonObject);
                    mQueue = LockatedVolleyRequestQueue.getInstance(getActivity()).getRequestQueue();
                    String url = ApplicationURL.getConversationUrl;
                    mLockatedJSONObjectRequest = new LockatedJSONObjectRequest(Request.Method.POST, url, jsonObject, this, this);
                    mLockatedJSONObjectRequest.setTag(REQUEST_TAG);
                    mQueue.add(mLockatedJSONObjectRequest);
                    Log.e("getConversationBetweenUser", "" + url);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                Utilities.showToastMessage(getActivity(), getActivity().getResources().getString(R.string.internet_connection_error));
            }
        }
    }

    private void onPreviousMessagesClicked() {
        if (!isSupportScreenPaused) {
            if (peerChatAdapter.getmData().size() > 0) {
                if (ConnectionDetector.isConnectedToInternet(getActivity())) {
                    isSupportScreenPaused = true;
                    mProgressBarView.setVisibility(View.VISIBLE);
                    mTextViewSupportHeaderPreviousMessages.setVisibility(View.GONE);

                    mQueue = LockatedVolleyRequestQueue.getInstance(getActivity()).getRequestQueue();
                    String url = ApplicationURL.getConversationByIdUrlPP
                            + peerChatAdapter.getmData().get(0).getConversation_id() + "?page="
                            + mPageNumber + "&token=" + mLockatedPreferences.getLockatedToken();
                    Log.e("Chat Url", "" + url);
                    mLockatedJSONObjectRequest = new LockatedJSONObjectRequest(Request.Method.GET, url, null, this, this);
                    mLockatedJSONObjectRequest.setTag(PAGE_REQUEST_TAG);
                    mQueue.add(mLockatedJSONObjectRequest);
                } else {
                    Utilities.showToastMessage(getActivity(), getActivity().getResources().getString(R.string.internet_connection_error));
                }
            }
        }
    }

    private void getConversationById() {
        if (!isSupportScreenPaused) {
            if (peerChatAdapter.getmData().size() > 0) {
                if (ConnectionDetector.isConnectedToInternet(getActivity())) {
                    mProgressBarView.setVisibility(View.VISIBLE);
                    mQueue = LockatedVolleyRequestQueue.getInstance(getActivity()).getRequestQueue();
                    String url = ApplicationURL.getConversationByIdUrlPP
                            + peerChatAdapter.getmData().get(0).getConversation_id() + "?lastid="
                            + peerChatAdapter.getmData().get(peerChatAdapter.getmData().size() - 1).getId()
                            + "&token=" + mLockatedPreferences.getLockatedToken();
                    Log.e("size >0", "" + url);
                    mLockatedJSONObjectRequest = new LockatedJSONObjectRequest(Request.Method.GET, url, null, this, this);
                    mLockatedJSONObjectRequest.setTag(REQUEST_TAG);
                    mQueue.add(mLockatedJSONObjectRequest);
                } else {
                    Utilities.showToastMessage(getActivity(), getActivity().getResources().getString(R.string.internet_connection_error));
                }
            } else {
                if (ConnectionDetector.isConnectedToInternet(getActivity())) {
                    mProgressBarView.setVisibility(View.VISIBLE);
                    mQueue = LockatedVolleyRequestQueue.getInstance(getActivity()).getRequestQueue();
                    String url = ApplicationURL.getConversationByIdUrlPP
                            + mChatId + "?lastid=" + 0 + "&token=" + mLockatedPreferences.getLockatedToken();
                    Log.e("size<0", "" + url);
                    mLockatedJSONObjectRequest = new LockatedJSONObjectRequest(Request.Method.GET, url, null, this, this);
                    mLockatedJSONObjectRequest.setTag(REQUEST_TAG);
                    mQueue.add(mLockatedJSONObjectRequest);
                } else {
                    Utilities.showToastMessage(getActivity(), getActivity().getResources().getString(R.string.internet_connection_error));
                }
            }
        }
    }

    private void onSampleMessageSendClicked() {
        String strSupportMessage = mEditTextAddMessage.getText().toString();

        boolean supportSendCancel = false;
        View supportSendFocusView = null;

        if (TextUtils.isEmpty(strSupportMessage)) {
            supportSendFocusView = mEditTextAddMessage;
            supportSendCancel = true;
        }

        if (supportSendCancel) {
            supportSendFocusView.requestFocus();
            Utilities.showToastMessage(getActivity(), getActivity().getResources().getString(R.string.send_query_error));
        } else {
            if (ConnectionDetector.isConnectedToInternet(getActivity())) {
                isSupportScreenPaused = true;
                mProgressBarView.setVisibility(View.VISIBLE);
                JSONObject jsonObject = new JSONObject();
                try {
                    if (peerChatAdapter.getmData().size() > 0) {
                        String message = URLEncoder.encode(strSupportMessage, "UTF-8");
                        String SendUrl = ApplicationURL.sendConversationUrlPP
                                + peerChatAdapter.getmData().get(0).getConversation_id()
                                + "/messages?message[body]=" + message + "&token="
                                + mLockatedPreferences.getLockatedToken();
                        mQueue = LockatedVolleyRequestQueue.getInstance(getActivity()).getRequestQueue();
                        mLockatedJSONObjectRequest = new LockatedJSONObjectRequest(Request.Method.POST, SendUrl, jsonObject, this, this);
                        mLockatedJSONObjectRequest.setTag(SEND_REQUEST_TAG);
                        mQueue.add(mLockatedJSONObjectRequest);

                        mEditTextAddMessage.setText("");
                    } else {
                        String message = URLEncoder.encode(strSupportMessage, "UTF-8");
                        String SendUrl = ApplicationURL.sendConversationUrlPP
                                + mChatId
                                + "/messages?message[body]=" + message + "&token="
                                + mLockatedPreferences.getLockatedToken();
                        mQueue = LockatedVolleyRequestQueue.getInstance(getActivity()).getRequestQueue();
                        mLockatedJSONObjectRequest = new LockatedJSONObjectRequest(Request.Method.POST, SendUrl, jsonObject, this, this);
                        mLockatedJSONObjectRequest.setTag(SEND_REQUEST_TAG);
                        mQueue.add(mLockatedJSONObjectRequest);

                        mEditTextAddMessage.setText("");
                    }
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }
            } else {
                Utilities.showToastMessage(getActivity(), getActivity().getResources().getString(R.string.internet_connection_error));
            }
        }
    }

    public void hideSoftKeyboard() {
        if (getActivity().getCurrentFocus() != null) {
            InputMethodManager inputMethodManager = (InputMethodManager) getActivity().getSystemService(getActivity().INPUT_METHOD_SERVICE);
            inputMethodManager.hideSoftInputFromWindow(getActivity().getCurrentFocus().getWindowToken(), 0);
        }
    }

    private void showHideListHeader() {
        if (peerChatAdapter.getmData().size() > 0) {
            if (peerChatAdapter.getmData().get(0).getMessage_count() > peerChatAdapter.getmData().size()) {
                mPageNumber = ++mPageNumber;
                mTextViewSupportHeaderPreviousMessages.setVisibility(View.VISIBLE);
            } else {
                mTextViewSupportHeaderPreviousMessages.setVisibility(View.GONE);
                mSupportList.removeHeaderView(mSupportHeaderPreviousMessages);
            }
        }
    }

    private void getConversationInLoop() {
        if (mCountDownTimer == null) {
            mCountDownTimer = new CountDownTimer(2000, 2000) {
                @Override
                public void onTick(long millisUntilFinished) {
                    getConversationById();
                }

                @Override
                public void onFinish() {
                    mCountDownTimer.start();
                }
            }.start();
        }
    }

    @Override
    public void onErrorResponse(VolleyError error) {
        isSupportScreenPaused = false;
        mProgressBarView.setVisibility(View.GONE);
        if (getActivity() != null) {
            LockatedRequestError.onRequestError(getActivity(), error);
            showHideListHeader();
        }
    }

    @Override
    public void onResponse(Object response) {
        mProgressBarView.setVisibility(View.GONE);

        if (getActivity() != null) {
            if (mLockatedJSONObjectRequest.getTag().toString().equals(REQUEST_TAG)) {
                JSONObject jsonObject = (JSONObject) response;
                try {
                    JSONArray jsonArray = jsonObject.getJSONArray("messages");
                    if (jsonArray.length() > 0) {
                        for (int i = 0; i < jsonArray.length(); i++) {
                            JSONObject supportObject = jsonArray.getJSONObject(i);
                            PeerChatData peerChatData = new PeerChatData(jsonObject.optInt("count"), supportObject);
                            peerChatAdapter.addItem(peerChatAdapter.getmData().size(), peerChatData);
                        }

                        showHideListHeader();
                        if (peerChatAdapter.getmData().size() > 4) {
                            if (mSupportList.getLastVisiblePosition() > peerChatAdapter.getmData().size() - 4) {
                                hasScreenFirstView = false;
                            }
                        }
                    } else {
                        if (jsonObject.has("conversation_id")) {
                            mChatId = jsonObject.getInt("conversation_id");
                        }
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else if (mLockatedJSONObjectRequest.getTag().toString().equals(SEND_REQUEST_TAG)) {
                isSupportScreenPaused = false;
                hasScreenFirstView = false;
                JSONObject jsonObject = (JSONObject) response;
                try {
                    JSONObject messageObject = jsonObject.getJSONObject("message");
                    PeerChatData peerChatData = new PeerChatData(jsonObject.optInt("count"), messageObject);
                    peerChatAdapter.addItem(peerChatAdapter.getmData().size(), peerChatData);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else if (mLockatedJSONObjectRequest.getTag().toString().equals(PAGE_REQUEST_TAG)) {
                isSupportScreenPaused = false;
                JSONObject jsonObject = (JSONObject) response;
                try {
                    JSONArray jsonArray = jsonObject.getJSONArray("messages");
                    if (jsonArray.length() > 0) {
                        for (int i = 0; i < jsonArray.length(); i++) {
                            JSONObject supportObject = jsonArray.getJSONObject(i);
                            PeerChatData peerChatData = new PeerChatData(jsonObject.optInt("count"), supportObject);
                            peerChatAdapter.addItem(i, peerChatData);
                        }

                        showHideListHeader();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            getConversationInLoop();

            Collections.sort(peerChatAdapter.getmData(), new Comparator<PeerChatData>() {
                @Override
                public int compare(PeerChatData lhs, PeerChatData rhs) {
                    return lhs.getUpdated_at().compareTo(rhs.getUpdated_at());
                }
            });

            if (!hasScreenFirstView) {
                hasScreenFirstView = true;
                mSupportList.setSelection(peerChatAdapter.getmData().size() + 1);
            }
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.mImageViewMessageSend:
                onSampleMessageSendClicked();
                break;
            case R.id.mImageViewSampleMessage:
                break;
            case R.id.mTextViewSupportPreviousMessages:
                onPreviousMessagesClicked();
                break;
        }
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        String value = (String) parent.getItemAtPosition(position);
        mEditTextAddMessage.setText(value);
        mEditTextAddMessage.setSelection(mEditTextAddMessage.getText().toString().length());
    }
}
