
package com.android.lockated.model.AdminModel.AdminEvents.PublshedEvent;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class Event {

    @SerializedName("date")
    @Expose
    private String date;
    @SerializedName("events")
    @Expose
    private ArrayList<Event_Published> events = new ArrayList<Event_Published>();

    /**
     * 
     * @return
     *     The date
     */
    public String getDate() {
        return date;
    }

    /**
     * 
     * @param date
     *     The date
     */
    public void setDate(String date) {
        this.date = date;
    }

    /**
     * 
     * @return
     *     The events
     */
    public ArrayList<Event_Published> getEvents() {
        return events;
    }

    /**
     * 
     * @param events
     *     The events
     */
    public void setEvents(ArrayList<Event_Published> events) {
        this.events = events;
    }

}
