package com.android.lockated.crm.fragment.myzone.directory;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.android.lockated.LockatedApplication;
import com.android.lockated.R;
import com.android.lockated.crm.adapters.DirectoryMembersAdapter;
import com.android.lockated.information.AccountController;
import com.android.lockated.model.AccountData;
import com.android.lockated.model.usermodel.Directory.MembersDirectory.UserSociety;
import com.android.lockated.network.ConnectionDetector;
import com.android.lockated.preferences.LockatedPreferences;
import com.android.lockated.request.LockatedJSONObjectRequest;
import com.android.lockated.request.LockatedRequestError;
import com.android.lockated.request.LockatedVolleyRequestQueue;
import com.android.lockated.utils.ApplicationURL;
import com.android.lockated.utils.Utilities;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class MembersDirectoryFragment extends Fragment implements Response.ErrorListener, Response.Listener<JSONObject> {

    private RequestQueue mQueue;
    ListView listView;
    String pid;
    private String userType;
    int societyid;
    TextView errorMsg;
    ProgressBar progressBar;
    ArrayList<AccountData> data;
    ArrayList<UserSociety> userSocieties;
    ArrayList<AccountData> accountDataArrayList;
    AccountController accountController;
    private LockatedPreferences mLockatedPreferences;
    DirectoryMembersAdapter directoryManagementAdapter;
    public static final String REQUEST_TAG = "MembersDirectoryFragment";

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View directoryManagementView = inflater.inflate(R.layout.fragment_members_directory, container, false);
        init(directoryManagementView);
        return directoryManagementView;
    }

    @Override
    public void onResume() {
        super.onResume();
        LockatedApplication.getInstance().trackScreenView(getString(R.string.memberDirectory));
        LockatedApplication.getInstance().trackEvent(getString(R.string.memberDirectory),
                getString(R.string.visited), getString(R.string.memberDirectory));
        userSocieties.clear();
        getDirectory();
    }

    private void init(View directoryManagementView) {
        userSocieties = new ArrayList<>();
        mLockatedPreferences = new LockatedPreferences(getActivity());
        progressBar = (ProgressBar) directoryManagementView.findViewById(R.id.mProgressBarView);
        listView = (ListView) directoryManagementView.findViewById(R.id.listView);
        errorMsg = (TextView) directoryManagementView.findViewById(R.id.errorMsg);
        directoryManagementAdapter = new DirectoryMembersAdapter(getActivity(), userSocieties);
        listView.setAdapter(directoryManagementAdapter);
        /*accountController = AccountController.getInstance();
        data = accountController.getmAccountDataList();
        accountDataArrayList = accountController.getmAccountDataList();*/
        societyid = Integer.parseInt(mLockatedPreferences.getSocietyId());
        try {
            userType = getMySocietyRoles();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public String getMySocietyRoles() {
        String mySocietyRoles = "blank";
        if (mLockatedPreferences.getRolesJson() != null && !mLockatedPreferences.getRolesJson().equals("blank")) {
            try {
                JSONObject noticeJsonObj = new JSONObject(mLockatedPreferences.getRolesJson());

                for (int i = 0; i < noticeJsonObj.getJSONArray("permissions").length(); i++) {
                    JSONObject jsonObject1 = noticeJsonObj.getJSONArray("permissions").getJSONObject(i);
                    if (jsonObject1.has("section") && jsonObject1.getString("section").equals("spree_mysociety")) {
                        if (jsonObject1.getJSONObject("permission").has("index")) {
                            mySocietyRoles = jsonObject1.getJSONObject("permission").getString("index");
                        }
                    }
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return mySocietyRoles;
    }

    @Override
    public void onErrorResponse(VolleyError error) {
        progressBar.setVisibility(View.GONE);
        if (getActivity() != null) {
            LockatedRequestError.onRequestError(getActivity(), error);
        }
    }

    private void getDirectory() {
        if (!mLockatedPreferences.getSocietyId().equals("blank")) {
            if (!userType.equals("blank")) {
                if (ConnectionDetector.isConnectedToInternet(getActivity())) {
                    progressBar.setVisibility(View.VISIBLE);
                    String url = ApplicationURL.getApprovedUser + mLockatedPreferences.getSocietyId() + "&token="
                            + mLockatedPreferences.getLockatedToken();
                    mQueue = LockatedVolleyRequestQueue.getInstance(getActivity()).getRequestQueue();
                    LockatedJSONObjectRequest lockatedJSONObjectRequest = new LockatedJSONObjectRequest(Request.Method.GET,
                            url, null, this, this);
                    lockatedJSONObjectRequest.setTag(REQUEST_TAG);
                    mQueue.add(lockatedJSONObjectRequest);
                } else {
                    Utilities.showToastMessage(getActivity(), getActivity().getResources().getString(R.string.internet_connection_error));
                }
            } else {
                listView.setVisibility(View.GONE);
                errorMsg.setVisibility(View.VISIBLE);
                errorMsg.setText(R.string.not_in_society);
            }
        } else {
            listView.setVisibility(View.GONE);
            errorMsg.setVisibility(View.VISIBLE);
            errorMsg.setText(R.string.not_in_society);
        }
    }

    @Override
    public void onResponse(JSONObject response) {
        progressBar.setVisibility(View.INVISIBLE);
        if (getActivity() != null) {
            if (response != null && response.length() > 0) {
                try {
                    if (response.getJSONArray("user_societies").length() > 0) {
                        JSONArray jsonArray = response.getJSONArray("user_societies");
                        Gson gson = new Gson();
                        for (int i = 0; i < jsonArray.length(); i++) {
                            UserSociety userSociety = gson.fromJson(jsonArray.getJSONObject(i).toString(), UserSociety.class);
                            userSocieties.add(userSociety);
                        }
                        directoryManagementAdapter.notifyDataSetChanged();
                    } else {
                        listView.setVisibility(View.GONE);
                        errorMsg.setVisibility(View.VISIBLE);
                        errorMsg.setText(R.string.no_data_error);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                listView.setVisibility(View.GONE);
                errorMsg.setVisibility(View.VISIBLE);
                errorMsg.setText(R.string.no_data_error);
            }
        }
    }

    /*private class DirectoryMembersAdapter extends BaseAdapter implements Response.ErrorListener, Response.Listener<JSONObject> {
        private final ArrayList<UserSociety> userSocieties;
        Context contextMain;
        LayoutInflater layoutInflater;

        public DirectoryMembersAdapter(Context context, ArrayList<UserSociety> userSocieties) {
            this.contextMain = context;
            this.userSocieties = userSocieties;
            layoutInflater = LayoutInflater.from(context);
        }


        @Override
        public int getCount() {
            return userSocieties.size();
        }

        @Override
        public Object getItem(int position) {
            return position;
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public void onErrorResponse(VolleyError error) {
            progressBar.setVisibility(View.GONE);
            if (getActivity() != null) {
                LockatedRequestError.onRequestError(getActivity(), error);
            }
        }

        @Override
        public void onResponse(JSONObject response) {
            progressBar.setVisibility(View.GONE);
        }

        private class ViewHolder {
            ImageView mImageDelete;
            ImageView mImageEdit;
            TextView mTextNumber;
            TextView mTextDirectoryName;
            TextView mServiceType;
            ImageButton mImageMessage;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
            ViewHolder holder = null;
            if (convertView == null) {
                convertView = layoutInflater.inflate(R.layout.directory_member_child, parent, false);
                holder = new ViewHolder();
                convertView.setTag(holder);
            } else {
                holder = (ViewHolder) convertView.getTag();
            }

            holder.mImageEdit = (ImageView) convertView.findViewById(R.id.mImageEdit);
            holder.mImageDelete = (ImageView) convertView.findViewById(R.id.mImageDelete);
            holder.mTextDirectoryName = (TextView) convertView.findViewById(R.id.mTextDirectoryName);
            holder.mTextNumber = (TextView) convertView.findViewById(R.id.mTextNumber);
            holder.mImageMessage = (ImageButton) convertView.findViewById(R.id.mImageMessage);

            if ((userSocieties.get(position).getUser() != null) && !userSocieties.get(position).getUser().toString().equals("null")) {
                String name = userSocieties.get(position).getUser().getFirstname()
                        + " " + userSocieties.get(position).getUser().getLastname();
                holder.mTextDirectoryName.setText(name);
                if (userSocieties.get(position).getUserFlat() != null) {
                    holder.mTextNumber.setText(userSocieties.get(position).getUserFlat().getFlat());
                } else {
                    holder.mTextNumber.setText("No Flat");
                }
            }
            holder.mImageMessage.setTag(position);
            holder.mImageMessage.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int position = (int) v.getTag();
                    String userName = "";
                    if (userSocieties.get(position).getUser() != null) {
                        userName = userSocieties.get(position).getUser().getFirstname()
                                + " " + userSocieties.get(position).getUser().getLastname();
                    }
                    if (userSocieties.get(position).getUser().getId() != Integer.valueOf(mLockatedPreferences.getLockatedUserId())) {
                        Intent intent = new Intent(contextMain, PeerChatActivity.class);
                        intent.putExtra("userId", "" + userSocieties.get(position).getUser().getId());
                        intent.putExtra("userName", userName);
                        contextMain.startActivity(intent);
                    } else {
                        Utilities.showToastMessage(contextMain, "You cannot chat with yourself");
                    }
                }
            });

            return convertView;
        }
    }*/

}
