package com.android.lockated.register;

import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.telephony.SmsMessage;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.lockated.HomeActivity;
import com.android.lockated.LockatedApplication;
import com.android.lockated.OnBoardActivity;
import com.android.lockated.R;
import com.android.lockated.information.AccountController;
import com.android.lockated.model.AccountData;
import com.android.lockated.network.ConnectionDetector;
import com.android.lockated.preferences.LockatedPreferences;
import com.android.lockated.request.LockatedRequestError;
import com.android.lockated.request.LockatedVolleyRequestQueue;
import com.android.lockated.utils.ApplicationURL;
import com.android.lockated.utils.Utilities;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;

import org.json.JSONException;
import org.json.JSONObject;

public class OTPGeneratorFragment extends Fragment implements View.OnClickListener, Response.Listener, Response.ErrorListener {
    private View mOTPView;
    private TextView mTextViewEnterOTP;
    private TextView mTextViewSubmitOTP;
    private TextView mTextViewResendOTP;
    private TextView mTextViewContactMessage;

    private EditText mEditTextOTPNumber;
    private EditText mEditTextContactNumber;

    private ProgressDialog mProgressDialog;
    private AccountController mAccountController;
    private LockatedPreferences mLockatedPreferences;
    private LockatedVolleyRequestQueue lockatedVolleyRequestQueue;

    private String mRedirectToScreen = "OTPGeneratorFragment";
    public static final String REQUEST_TAG = "OTPGeneratorFragment";
    public static final String REQUEST_VERIFY_TAG = "OTPVerifyFragment";
    public static final String ACCOUNT_REQUEST_TAG = "AccountDetail";
    boolean otpGenerated = false;
    boolean receiverRegistered = false;
    private int longestToast = 20 * 1000;
    private LockatedPreferences lockatedPreferences;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mOTPView = inflater.inflate(R.layout.fragment_otp_generate, container, false);
        lockatedPreferences = new LockatedPreferences(getActivity());
        if (getActivity() != null) {
            getActivity().registerReceiver(mIncommingSmsBroadcastReceiver,
                    new IntentFilter("android.provider.Telephony.SMS_RECEIVED"));
            receiverRegistered = true;
        }
        init();
        if (!lockatedPreferences.getIsOtpGenerated()) {
            generateOTP();
        }

        return mOTPView;
    }

    BroadcastReceiver mIncommingSmsBroadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            final Bundle bundle = intent.getExtras();
            //Log.e("bundle", "" + bundle.toString());
            if (intent.getAction().equalsIgnoreCase("android.provider.Telephony.SMS_RECEIVED")) {
                try {
                    if (bundle != null) {
                        final Object[] pdusObj = (Object[]) bundle.get("pdus");
                        for (int i = 0; i < pdusObj.length; i++) {
                            SmsMessage currentMessage = SmsMessage.createFromPdu((byte[]) pdusObj[i]);
                            //String phoneNumber = currentMessage.getDisplayOriginatingAddress();
                            String message = currentMessage.getDisplayMessageBody();
                            message = message.replaceAll("\\D+", "");
                            //Log.e("message", "" + message);
                            mEditTextOTPNumber.setText(message);
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }

    };

    @Override
    public void onResume() {
        super.onResume();
        LockatedApplication.getInstance().trackScreenView(getActivity().getString(R.string.otp_screen));
    }

    private void init() {
        mAccountController = AccountController.getInstance();
        mLockatedPreferences = new LockatedPreferences(getActivity());
        mTextViewEnterOTP = (TextView) mOTPView.findViewById(R.id.mTextViewEnterOTP);
        mTextViewSubmitOTP = (TextView) mOTPView.findViewById(R.id.mTextViewSubmitOTP);
        mTextViewResendOTP = (TextView) mOTPView.findViewById(R.id.mTextViewResendOTP);
        mTextViewContactMessage = (TextView) mOTPView.findViewById(R.id.mTextViewContactMessage);

        mEditTextOTPNumber = (EditText) mOTPView.findViewById(R.id.mEditTextOTPNumber);
        mEditTextContactNumber = (EditText) mOTPView.findViewById(R.id.mEditTextContactNumber);

        mTextViewSubmitOTP.setEnabled(false);
        mTextViewSubmitOTP.setTextColor(ContextCompat.getColor(getActivity(), R.color.secondary_text));

        mTextViewSubmitOTP.setOnClickListener(this);
        mTextViewResendOTP.setOnClickListener(this);
        if (getArguments() != null) {
            String mobileNumberText = getArguments().getString("mobileNumber");
            mRedirectToScreen = getArguments().getString("EditPersonalInformationFragment");
            mEditTextContactNumber.setText(mobileNumberText);

        } else {
            mEditTextContactNumber.setText(mLockatedPreferences.getContactNumber());
        }
        setEditTextListener();
    }

    @Override
    public void onPause() {
        super.onPause();
        if (getActivity() != null) {
            if (receiverRegistered) {
                getActivity().unregisterReceiver(mIncommingSmsBroadcastReceiver);
                receiverRegistered = false;
            }
        }
    }


    private void hideEditTextEnterOTP() {
        mTextViewEnterOTP.setVisibility(View.INVISIBLE);
    }

    private void showEditTextEnterOTP() {
        mTextViewEnterOTP.setVisibility(View.VISIBLE);
    }

    private void setEditTextListener() {
        mEditTextOTPNumber.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.length() > 0) {
                    showEditTextEnterOTP();

                    mTextViewSubmitOTP.setEnabled(true);
                    mTextViewSubmitOTP.setTextColor(ContextCompat.getColor(getActivity(), R.color.primary));
                } else {
                    hideEditTextEnterOTP();

                    mTextViewSubmitOTP.setEnabled(false);
                    mTextViewSubmitOTP.setTextColor(ContextCompat.getColor(getActivity(), R.color.secondary_text));
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }

    private void generateOTP() {
        if (ConnectionDetector.isConnectedToInternet(getActivity())) {
            int length = mEditTextContactNumber.getText().toString().length();
            if (!mLockatedPreferences.getContactNumber().equals("") && length == 10) {
                mLockatedPreferences.setContactNumber(mEditTextContactNumber.getText().toString());
                mEditTextContactNumber.setSelection(length);
                mEditTextContactNumber.setCursorVisible(false);

                mProgressDialog = ProgressDialog.show(getActivity(), "", "Please Wait...", false);
                mProgressDialog.show();

                JSONObject jsonObject = new JSONObject();

                try {
                    jsonObject.put("token", mLockatedPreferences.getLockatedToken());
                    jsonObject.put("mobile", mLockatedPreferences.getContactNumber());
                    jsonObject.put("change_number", "true");
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                String url = ApplicationURL.generateOTPUrl;
                lockatedVolleyRequestQueue = LockatedVolleyRequestQueue.getInstance(getActivity());
                lockatedVolleyRequestQueue.sendRequest(REQUEST_TAG, Request.Method.POST, url, jsonObject, this, this);
            } else {
                Utilities.showToastMessage(getActivity(), getActivity().getResources().getString(R.string.mobile_number_error));
            }
        } else {
            Utilities.showToastMessage(getActivity(), getActivity().getResources().getString(R.string.internet_connection_error));
        }
    }

    private void getScreenDetail() {
        if (ConnectionDetector.isConnectedToInternet(getActivity())) {
            mProgressDialog = ProgressDialog.show(getActivity(), "", "Please Wait...", false);
            mProgressDialog.show();
            String url = ApplicationURL.getScreenUrl + "?token=" + mLockatedPreferences.getLockatedToken();
            lockatedVolleyRequestQueue = LockatedVolleyRequestQueue.getInstance(getActivity());
            lockatedVolleyRequestQueue.sendRequest(ACCOUNT_REQUEST_TAG, Request.Method.GET, url, null, this, this);
        } else {
            Utilities.showToastMessage(getActivity(), getActivity().getResources().getString(R.string.internet_connection_error));
        }
    }

    private void onSubmitButtonClicked() {
        String strOTPNumber = mEditTextOTPNumber.getText().toString();
        String message = "";

        boolean OTPCancel = false;
        View OTPFocusView = null;

        if (TextUtils.isEmpty(strOTPNumber)) {
            OTPFocusView = mEditTextOTPNumber;
            OTPCancel = true;
            message = getActivity().getResources().getString(R.string.otp_verify_error);
        }

        if (mEditTextContactNumber.getText().toString().length() != 10) {
            OTPFocusView = mEditTextOTPNumber;
            OTPCancel = true;
            message = getActivity().getResources().getString(R.string.mobile_number_error);
        }

        if (OTPCancel) {
            OTPFocusView.requestFocus();
            Utilities.showToastMessage(getActivity(), message);
        } else {
            if (ConnectionDetector.isConnectedToInternet(getActivity())) {
                mProgressDialog = ProgressDialog.show(getActivity(), "", "Please Wait...", false);
                mProgressDialog.show();

                JSONObject jsonObject = new JSONObject();

                try {
                    jsonObject.put("token", mLockatedPreferences.getLockatedToken());
                    jsonObject.put("mobile", mLockatedPreferences.getContactNumber());
                    jsonObject.put("otp", strOTPNumber);

                    /*Log.w(REQUEST_VERIFY_TAG, jsonObject.toString());*/
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                lockatedVolleyRequestQueue = LockatedVolleyRequestQueue.getInstance(getActivity());
                lockatedVolleyRequestQueue.sendRequest(REQUEST_VERIFY_TAG, Request.Method.POST,
                        ApplicationURL.verifyOTPUrl, jsonObject, this, this);


            } else {
                Utilities.showToastMessage(getActivity(), getActivity().getResources().getString(R.string.internet_connection_error));
            }
        }
    }

    @Override
    public void onErrorResponse(VolleyError error) {
        mProgressDialog.dismiss();
        if (getActivity() != null) {
            LockatedRequestError.onRequestError(getActivity(), error);
        }
    }

    @Override
    public void onResponse(Object response) {
        mProgressDialog.dismiss();
        if (getActivity() != null) {
            JSONObject jsonObject = (JSONObject) response;
            try {
                if (lockatedVolleyRequestQueue.getLockatedJSONObjectRequest().getTag().toString().equals(REQUEST_TAG)) {
                    mTextViewContactMessage.setText(getResources().getString(R.string.otp_message));
                    otpGenerated = true;
                    mEditTextContactNumber.setText(mLockatedPreferences.getContactNumber());

                    int length = mEditTextContactNumber.getText().toString().length();
                    mEditTextContactNumber.setSelection(length);
                    mEditTextContactNumber.setCursorVisible(true);
                    String message;
                    if (jsonObject.has("message")) {
                        message = jsonObject.getString("message");
                        Utilities.showToastMessage(getActivity(), message);
                        Toast.makeText(getActivity(), "SMS will be auto detected", longestToast).show();
                        lockatedPreferences.setIsOtpGenerated(true);
                    } else if (jsonObject.has("error")) {
                        message = jsonObject.getString("error");
                        Utilities.showToastMessage(getActivity(), message);
                    }
                } else if (lockatedVolleyRequestQueue.getLockatedJSONObjectRequest().getTag().toString().equals(REQUEST_VERIFY_TAG)) {
                    boolean verified = jsonObject.getBoolean("verified");
                    if (verified) {
                        getScreenDetail();
                    } else {
                        Utilities.showToastMessage(getActivity(), getResources().getString(R.string.otp_verify_error));
                    }
                } else if (lockatedVolleyRequestQueue.getLockatedJSONObjectRequest().getTag().toString().equals(ACCOUNT_REQUEST_TAG)) {
                    AccountData accountData = new AccountData(jsonObject);
                    mLockatedPreferences.setUserSocietyId(accountData.getSelected_user_society());
                    mAccountController.mAccountDataList.add(accountData);
                    mLockatedPreferences.setAccountData(accountData);
                    openOnBoardingActivity();
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.mTextViewResendOTP:
                Utilities.lockatedGoogleAnalytics(getString(R.string.resend_otp), getString(R.string.resend_otp), getString(R.string.resend_otp));
                if (getActivity() != null) {
                    getActivity().registerReceiver(mIncommingSmsBroadcastReceiver,
                            new IntentFilter("android.provider.Telephony.SMS_RECEIVED"));
                }
                generateOTP();
                break;
            case R.id.mTextViewSubmitOTP:
                onSubmitButtonClicked();
                Utilities.lockatedGoogleAnalytics(getString(R.string.submit_otp), getString(R.string.submit_otp), getString(R.string.submit_otp));
                break;
        }
    }

    private void openOnBoardingActivity() {
        if (getActivity() != null) {
            if (mRedirectToScreen.equals("EditPersonalInformationFragment")) {
                Intent intent = new Intent(getActivity(), HomeActivity.class);
                intent.putExtra("EditPersonalInformationFragment", "EditPersonalInformationFragment");
                startActivity(intent);
                getActivity().finish();
            } else {
                Intent intent = new Intent(getActivity(), OnBoardActivity.class);
                startActivity(intent);
                getActivity().finish();
            }
        }
    }
}
