
package com.android.lockated.model.expectedVisitor;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class GatekeeperMimo {

    @SerializedName("id")
    @Expose
    private int id;
    @SerializedName("gatekeeper_id")
    @Expose
    private int gatekeeperId;
    @SerializedName("mimo_type")
    @Expose
    private String mimoType;
    @SerializedName("item_number")
    @Expose
    private int itemNumber;
    @SerializedName("details")
    @Expose
    private String details;
    @SerializedName("need_approval")
    @Expose
    private int needApproval;
    @SerializedName("approve")
    @Expose
    private Object approve;
    @SerializedName("approved_by")
    @Expose
    private Object approvedBy;
    @SerializedName("active")
    @Expose
    private Object active;
    @SerializedName("created_at")
    @Expose
    private String createdAt;
    @SerializedName("updated_at")
    @Expose
    private String updatedAt;
    @SerializedName("documents")
    @Expose
    private ArrayList<Document> documents = new ArrayList<Document>();

    /**
     * 
     * @return
     *     The id
     */
    public int getId() {
        return id;
    }

    /**
     * 
     * @param id
     *     The id
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * 
     * @return
     *     The gatekeeperId
     */
    public int getGatekeeperId() {
        return gatekeeperId;
    }

    /**
     * 
     * @param gatekeeperId
     *     The gatekeeper_id
     */
    public void setGatekeeperId(int gatekeeperId) {
        this.gatekeeperId = gatekeeperId;
    }

    /**
     * 
     * @return
     *     The mimoType
     */
    public String getMimoType() {
        return mimoType;
    }

    /**
     * 
     * @param mimoType
     *     The mimo_type
     */
    public void setMimoType(String mimoType) {
        this.mimoType = mimoType;
    }

    /**
     * 
     * @return
     *     The itemNumber
     */
    public int getItemNumber() {
        return itemNumber;
    }

    /**
     * 
     * @param itemNumber
     *     The item_number
     */
    public void setItemNumber(int itemNumber) {
        this.itemNumber = itemNumber;
    }

    /**
     * 
     * @return
     *     The details
     */
    public String getDetails() {
        return details;
    }

    /**
     * 
     * @param details
     *     The details
     */
    public void setDetails(String details) {
        this.details = details;
    }

    /**
     * 
     * @return
     *     The needApproval
     */
    public int getNeedApproval() {
        return needApproval;
    }

    /**
     * 
     * @param needApproval
     *     The need_approval
     */
    public void setNeedApproval(int needApproval) {
        this.needApproval = needApproval;
    }

    /**
     * 
     * @return
     *     The approve
     */
    public Object getApprove() {
        return approve;
    }

    /**
     * 
     * @param approve
     *     The approve
     */
    public void setApprove(Object approve) {
        this.approve = approve;
    }

    /**
     * 
     * @return
     *     The approvedBy
     */
    public Object getApprovedBy() {
        return approvedBy;
    }

    /**
     * 
     * @param approvedBy
     *     The approved_by
     */
    public void setApprovedBy(Object approvedBy) {
        this.approvedBy = approvedBy;
    }

    /**
     * 
     * @return
     *     The active
     */
    public Object getActive() {
        return active;
    }

    /**
     * 
     * @param active
     *     The active
     */
    public void setActive(Object active) {
        this.active = active;
    }

    /**
     * 
     * @return
     *     The createdAt
     */
    public String getCreatedAt() {
        return createdAt;
    }

    /**
     * 
     * @param createdAt
     *     The created_at
     */
    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    /**
     * 
     * @return
     *     The updatedAt
     */
    public String getUpdatedAt() {
        return updatedAt;
    }

    /**
     * 
     * @param updatedAt
     *     The updated_at
     */
    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    /**
     * 
     * @return
     *     The documents
     */
    public ArrayList<Document> getDocuments() {
        return documents;
    }

    /**
     * 
     * @param documents
     *     The documents
     */
    public void setDocuments(ArrayList<Document> documents) {
        this.documents = documents;
    }

}
