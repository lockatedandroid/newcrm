package com.android.lockated.pushnotification;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;

import com.android.lockated.ChatSupportActivity;
import com.android.lockated.HomeActivity;
import com.android.lockated.IndexActivity;
import com.android.lockated.LockatedApplication;
import com.android.lockated.categories.olacabs.activity.OlaMainActivity_Latest;
import com.android.lockated.crm.activity.AllEventsActivity;
import com.android.lockated.crm.activity.AllNoticesActivity;
import com.android.lockated.crm.activity.CrmPollsListActivity;
import com.android.lockated.crm.activity.EventDetailActivity;
import com.android.lockated.crm.activity.MyEventDetailActivity;
import com.android.lockated.crm.activity.MySocietyActivity;
import com.android.lockated.crm.fragment.myzone.gatekeeper.NewExpectedVisitorInDetailView;
import com.android.lockated.crm.p2pchat.PeerChatActivity;
import com.android.lockated.crmadmin.activity.AdminActivity;
import com.android.lockated.crmadmin.activity.AdminEventDetailActivity;
import com.android.lockated.crmadmin.activity.AdminNoticeDetailActivity;
import com.android.lockated.crmadmin.activity.AdminPollsActivity;
import com.android.lockated.crmadmin.activity.AdminUserManageActivity;
import com.android.lockated.preferences.LockatedPreferences;
import com.google.android.gms.gcm.GcmListenerService;


public class PushNotificationGcmReceiver extends GcmListenerService {

    private static final String TAG = PushNotificationGcmReceiver.class.getSimpleName();

    private NotificationUtils notificationUtils;

    /**
     * Called when message is received.
     *
     * @param from   SenderID of the sender.
     * @param bundle Data bundle containing message data as key/value pairs.
     *               For Set of keys use data.keySet().
     */

    @Override
    public void onMessageReceived(String from, Bundle bundle) {

        String title, message, image, timestamp, deptName = null, user_id, ntype, notice_id, event_id, gatekeeper_id,cab_type,cab_model,driver_number,car_number;;
        boolean force_close;
        int depId = 0;
        title = bundle.getString("title");
        message = bundle.getString("message");
        image = bundle.getString("image");
        timestamp = bundle.getString("created_at");
        user_id = bundle.getString("user_id");
        notice_id = bundle.getString("notice_id");
        event_id = bundle.getString("event_id");

        //-------------GateKepper-------------------
        gatekeeper_id = bundle.getString("gatekeeper_id");
        Log.e(TAG, "gatekeeper_id: " + gatekeeper_id);
        Log.e(TAG, "notice_id: " + notice_id);
        Log.e(TAG, "event_id: " + event_id);


        //--------------Ola----------------------
        cab_type = bundle.getString("cab_type");
        cab_model = bundle.getString("cab_model");
        driver_number = bundle.getString("driver_number");
        car_number = bundle.getString("car_number");

        Log.e(TAG ,"cab_type: " + cab_type);
        Log.e(TAG,"cab_model: " + cab_model);
        Log.e(TAG,"driver_number: "+driver_number);
        Log.e(TAG,"car_number: "+car_number);

        ntype = bundle.getString("ntype");
        force_close = Boolean.valueOf(bundle.getString("force_close"));
        Log.e(TAG, "bundle: " + bundle);
        //Log.e(TAG, "From: " + from);
        //Log.e(TAG, "Title: " + title);
        //Log.e(TAG, "message: " + message);
        //Log.e(TAG, "image: " + image);
        //Log.e(TAG, "timestamp: " + timestamp);
        //Log.e(TAG, "user_id: " + user_id);
        Log.e(TAG, "ntype: " + ntype);
        //Log.e(TAG, "force_close: " + force_close);
        LockatedPreferences lockatedPreferences = new LockatedPreferences(getApplicationContext());
        if (!lockatedPreferences.getLogout()) {
            //Log.e("user_id: " + user_id, "getId " + lockatedPreferences.getAccountData().getId());
            if (lockatedPreferences.getAccountData().getId().equals(user_id)) {
                if (bundle.containsKey("dep_id")) {
                    if (bundle.getString("dep_id") != null) {
                        depId = Integer.valueOf(bundle.getString("dep_id"));
                    } else {
                        depId = ((LockatedApplication) getApplicationContext()).getmSectionId();
                    }
                    if (bundle.getString("dep_name") != null) {
                        deptName = bundle.getString("dep_name");
                    } else {
                        deptName = "Chat";
                    }
                } else {
                    deptName = "Chat";
                }

                /*if (!NotificationUtils.isAppIsInBackground(getApplicationContext())) {
                    Log.e("!NotificationUtils.isAppIsInBackground", "if");
                    // app is in foreground, broadcast the push message
                    Intent pushNotification = new Intent(PushNotificationConfig.PUSH_NOTIFICATION);
                    pushNotification.putExtra("message", message);
                    LocalBroadcastManager.getInstance(this).sendBroadcast(pushNotification);
                    // play notification sound
                    NotificationUtils notificationUtils = new NotificationUtils();
                    notificationUtils.playNotificationSound();

                } else {
                Log.e("!NotificationUtils.isAppIsInBackground", "else");*/
                Intent resultIntent;

                if (ntype != null && ntype.equals("chat")) {
                    if (depId != 0 && deptName != null) {
                        if (lockatedPreferences.getLogout()) {
                            resultIntent = new Intent(getApplicationContext(), IndexActivity.class);
                            resultIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        } else {
                            resultIntent = new Intent(getApplicationContext(), ChatSupportActivity.class);
                            resultIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        }
                        resultIntent.putExtra("dep_id", depId);
                        resultIntent.putExtra("dep_name", deptName);
                    } else {
                        resultIntent = new Intent(getApplicationContext(), HomeActivity.class);
                        resultIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    }
                } else if (ntype != null && ntype.equalsIgnoreCase("offer")) {
                    resultIntent = new Intent(getApplicationContext(), HomeActivity.class);
                    resultIntent.putExtra("offer", "offer");
                    resultIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                } else if (ntype != null && ntype.equalsIgnoreCase("updateapp")) {
                    if (NotificationUtils.isAppIsInBackground(getApplicationContext())) {
                        resultIntent = new Intent(getApplicationContext(), HomeActivity.class);
                        resultIntent.putExtra("force_close", force_close);
                        resultIntent.putExtra("message", message);
                    } else {
                        resultIntent = new Intent(getApplicationContext(), UpdateAppActivity.class);
                        resultIntent.putExtra("force_close", force_close);
                        resultIntent.putExtra("message", message);
                    }
                } else if (ntype != null && ntype.equalsIgnoreCase("membermessage")) {
                    resultIntent = new Intent(getApplicationContext(), PeerChatActivity.class);
                    resultIntent.putExtra("userId", bundle.getString("sender_id"));
                    resultIntent.putExtra("userName", title);
                }
                //Manage User Notification  ================
                else if (ntype != null && ntype.equalsIgnoreCase("newmemberadmin")) {
                    resultIntent = new Intent(getApplicationContext(), AdminUserManageActivity.class);
                    resultIntent.putExtra("AdminUserPosition", 0);
                } else if (ntype != null && ntype.equalsIgnoreCase("rejectedonsociety")) {
                    lockatedPreferences.setNotificationValue(true);
                    resultIntent = new Intent(getApplicationContext(), HomeActivity.class);
                } else if (ntype != null && ntype.equalsIgnoreCase("approvedonsociety")) {
                    lockatedPreferences.setNotificationValue(true);
                    resultIntent = new Intent(getApplicationContext(), HomeActivity.class);
                }
                //Notice Notification ======================
                else if (ntype != null && ntype.equalsIgnoreCase("newnoticeadmin")) {
                    resultIntent = new Intent(getApplicationContext(), AdminNoticeDetailActivity.class);
                    //resultIntent.putExtra("notice_id", "" + notice_id);
                    resultIntent.putExtra("visibilityValue", "0");
                    lockatedPreferences.setNotice_id(notice_id);
                } else if (ntype != null && ntype.equalsIgnoreCase("allnotices")) {
                    resultIntent = new Intent(getApplicationContext(), AllNoticesActivity.class);
                    resultIntent.putExtra("AllNoticesActivity", 0);
                } else if (ntype != null && ntype.equalsIgnoreCase("approvednotice")) {
                    resultIntent = new Intent(getApplicationContext(), AllNoticesActivity.class);
                    resultIntent.putExtra("AllNoticesActivity", 0);
                } else if (ntype != null && ntype.equalsIgnoreCase("rejectednotice")) {
                    resultIntent = new Intent(getApplicationContext(), AllNoticesActivity.class);
                    resultIntent.putExtra("AllNoticesActivity", 1);
                }
                //Event Notifications  ======================
                else if (ntype != null && ntype.equalsIgnoreCase("neweventadmin")) {
                    resultIntent = new Intent(getApplicationContext(), AdminEventDetailActivity.class);
                    resultIntent.putExtra("event_id", "" + event_id);
                    resultIntent.putExtra("pendin_publish_value", "1");
                    lockatedPreferences.setEvent_id(event_id);
                } else if (ntype != null && ntype.equalsIgnoreCase("allevents")) {
                    resultIntent = new Intent(getApplicationContext(), AllEventsActivity.class);
                    resultIntent.putExtra("AllEventsActivity", 0);
                } else if (ntype != null && ntype.equalsIgnoreCase("approvedevent")) {
                    resultIntent = new Intent(getApplicationContext(), EventDetailActivity.class);
                    resultIntent.putExtra("event_id", "" + event_id);
                    lockatedPreferences.setEvent_id(event_id);
                } else if (ntype != null && ntype.equalsIgnoreCase("rejectedevent")) {
                    resultIntent = new Intent(getApplicationContext(), MyEventDetailActivity.class);
                    resultIntent.putExtra("event_id", "" + event_id);
                    lockatedPreferences.setEvent_id(event_id);
                    //Polls Notification======================
                } else if (ntype != null && ntype.equalsIgnoreCase("newpolladmin")) {
                    resultIntent = new Intent(getApplicationContext(), AdminPollsActivity.class);
                    resultIntent.putExtra("AdminPollsActivity", 0);
                } else if (ntype != null && ntype.equalsIgnoreCase("allpolls")) {
                    resultIntent = new Intent(getApplicationContext(), CrmPollsListActivity.class);
                    resultIntent.putExtra("CrmPollsListActivity", 0);
                } else if (ntype != null && ntype.equalsIgnoreCase("approvedpoll")) {
                    resultIntent = new Intent(getApplicationContext(), CrmPollsListActivity.class);
                    resultIntent.putExtra("CrmPollsListActivity", 0);
                } else if (ntype != null && ntype.equalsIgnoreCase("rejectedpoll")) {
                    resultIntent = new Intent(getApplicationContext(), CrmPollsListActivity.class);
                    resultIntent.putExtra("CrmPollsListActivity", 1);
                }
                // Complaint notification=====================
                else if (ntype != null && ntype.equalsIgnoreCase("newcomplaintadmin")) {
                    resultIntent = new Intent(getApplicationContext(), AdminActivity.class);
                    resultIntent.putExtra("AdminActivityPosition", 4);
                } else if (ntype != null && ntype.equalsIgnoreCase("statuschangecomplaint")) {
                    resultIntent = new Intent(getApplicationContext(), MySocietyActivity.class);
                    resultIntent.putExtra("crmItemPosition", 3);
                }
                //Gatekeeper Notification======================
                else if (ntype != null && ntype.equalsIgnoreCase("newvisitoruser")) {
                    lockatedPreferences.setGateKeeper_id(gatekeeper_id);
                    resultIntent = new Intent(getApplicationContext(), NewExpectedVisitorInDetailView.class);
                }
                else if (ntype != null && ntype.equalsIgnoreCase("newvisitorgatekeeper")) {
                    lockatedPreferences.setGateKeeper_id(gatekeeper_id);
                    resultIntent = new Intent(getApplicationContext(), NewExpectedVisitorInDetailView.class);
                } else if (ntype != null && ntype.equalsIgnoreCase("moveinapproveduser")) {
                    lockatedPreferences.setGateKeeper_id(gatekeeper_id);
                    resultIntent = new Intent(getApplicationContext(), NewExpectedVisitorInDetailView.class);
                }else if (ntype != null && ntype.equalsIgnoreCase("moveoutapproveduser")) {
                    lockatedPreferences.setGateKeeper_id(gatekeeper_id);
                    resultIntent = new Intent(getApplicationContext(), NewExpectedVisitorInDetailView.class);
                }else if (ntype != null && ntype.equalsIgnoreCase("moveooutapproveduser")) {
                    lockatedPreferences.setGateKeeper_id(gatekeeper_id);
                    resultIntent = new Intent(getApplicationContext(), NewExpectedVisitorInDetailView.class);
                }

                //----------------Ola Notification--------------
                else if (ntype != null && ntype.equalsIgnoreCase("olabookingcancel")) {
                    resultIntent = new Intent(getApplicationContext(),OlaMainActivity_Latest.class);
                    resultIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                }
                else if (ntype != null && ntype.equalsIgnoreCase("olabookingsuccess")) {

                    resultIntent = new Intent(getApplicationContext(),OlaMainActivity_Latest.class);
                    resultIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                }
                else if (ntype != null && ntype.equalsIgnoreCase("olabookingcomplete")) {

                    resultIntent = new Intent(getApplicationContext(),OlaMainActivity_Latest.class);
                    resultIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                }
                //----------------------------------------------------

                else {
                    resultIntent = new Intent(getApplicationContext(), HomeActivity.class);
                    resultIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                }
                resultIntent.putExtra("message", message);
                if (TextUtils.isEmpty(image)) {
                    showNotificationMessage(getApplicationContext(), title, message, timestamp, resultIntent);
                } else {
                    showNotificationMessageWithBigImage(getApplicationContext(), title, message, timestamp, resultIntent, image);
                }
                //}

                if(!TextUtils.isEmpty(driver_number))
                {
                    showNotificationMessageWithDriverNumber(getApplicationContext(), title, message, timestamp, resultIntent,driver_number);
                }
            }
        }

    }

    private void showNotificationMessage(Context context, String title, String message, String timeStamp, Intent intent) {
        notificationUtils = new NotificationUtils(context);
        if (intent.getExtras().containsKey("force_close") || intent.getExtras().containsKey("userId")) {
            //Log.e("showNotificationMessage", "userId if " + intent.getFlags());
        } else {
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK
                    | Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_TASK_ON_HOME);
        }
        notificationUtils.showNotificationMessage(title, message, timeStamp, intent);
    }

    private void showNotificationMessageWithDriverNumber(Context context, String title, String message, String timeStamp, Intent intent,String driver_number) {
        notificationUtils = new NotificationUtils(context);
        if (intent.getExtras().containsKey("force_close") || intent.getExtras().containsKey("userId")) {
            //Log.e("showNotificationMessage", "userId if " + intent.getFlags());
        } else {
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK
                    | Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_TASK_ON_HOME);
        }
        notificationUtils.showNotificationMessageWithDriverNumber(title, message, timeStamp, intent ,driver_number);
    }

    private void showNotificationMessageWithBigImage(Context context, String title, String message,
                                                     String timeStamp, Intent intent, String imageUrl) {
        notificationUtils = new NotificationUtils(context);
        if (intent.getExtras().containsKey("force_close") || intent.getExtras().containsKey("userId")) {
           /* Log.e("showNotificationMessageWithBigImage", "userId if ");*/
        } else {
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK
                    | Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_TASK_ON_HOME);
        }

        /*if (!intent.getExtras().containsKey("force_close") || !intent.getExtras().containsKey("Listposition")) {
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK
                    | Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_TASK_ON_HOME);
        }*/
        notificationUtils.showNotificationMessage(title, message, timeStamp, intent, imageUrl);
    }

}