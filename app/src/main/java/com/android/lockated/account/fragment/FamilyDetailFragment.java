package com.android.lockated.account.fragment;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;

import com.android.lockated.HomeActivity;
import com.android.lockated.Interfaces.IFamilyMemberAdded;
import com.android.lockated.Interfaces.IGeneralDetailAdded;
import com.android.lockated.Interfaces.IServantDetailAdded;
import com.android.lockated.LockatedApplication;
import com.android.lockated.R;
import com.android.lockated.information.AccountController;
import com.android.lockated.model.AccountData;
import com.android.lockated.model.FamilyData;
import com.android.lockated.model.GeneralDetailData;
import com.android.lockated.model.ServantData;
import com.android.lockated.network.ConnectionDetector;
import com.android.lockated.preferences.LockatedPreferences;
import com.android.lockated.request.LockatedRequestError;
import com.android.lockated.request.LockatedVolleyRequestQueue;
import com.android.lockated.utils.ApplicationURL;
import com.android.lockated.utils.Utilities;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class FamilyDetailFragment extends Fragment implements View.OnClickListener, IFamilyMemberAdded, IGeneralDetailAdded, IServantDetailAdded, Response.Listener, Response.ErrorListener {
    private View mFamilyDetailView;

    private ViewGroup mNewFamilyView;
    private ViewGroup mNewServantView;
    private ViewGroup mNewGeneralView;
    private ViewGroup mFamilyDetailContainer;
    private ViewGroup mServantDetailContainer;
    private ViewGroup mGeneralDetailContainer;

    private LinearLayout mLLFamilyDetailAdd;
    private LinearLayout mLLServantDetailAdd;
    private LinearLayout mLLGeneralDetailAdd;
    private LinearLayout mLLGeneralDetailEdit;
    private LinearLayout mLLFamilyMemberAdd;
    private LinearLayout mLLFamilyServantAdd;

    private TextView mTextViewFamilyDetailRowIndex;
    private TextView mTextViewFamilyDetailRowName;
    private TextView mButtonFamilyDetailSkip;
    private TextView mTextViewFamilyDetailRowSex;
    private TextView mTextViewFamilyDetailRowContact;
    private TextView mTextViewFamilyDetailRowContactMaterialStatus;

    private AccountController mAccountController;
    private RequestQueue mQueue;
    private ProgressDialog mProgressDialog;
    private LockatedPreferences mLockatedPreferences;
    private LockatedVolleyRequestQueue lockatedVolleyRequestQueue;

    private ArrayList<FamilyData> mFamilyDataList;
    private ArrayList<ServantData> mServantDataList;
    private ArrayList<GeneralDetailData> mGeneralDataList;

    public static final String DELETE_REQUEST_TAG = "DeleteFamilyMemberFragment";
    public static final String DELETE_SERVANT_REQUEST_TAG = "DeleteServantFragment";
    private int mPosition;
    private int mFamilyBottomView;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mFamilyDetailView = inflater.inflate(R.layout.fragment_family_detail_new, container, false);
        init();
        Utilities.ladooIntegration(getActivity(), getActivity().getString(R.string.family_detail_screen));
        return mFamilyDetailView;
    }

    @Override
    public void onResume() {
        super.onResume();
        LockatedApplication.getInstance().trackScreenView(getActivity().getString(R.string.family_detail_screen));
        Utilities.lockatedGoogleAnalytics(getActivity().getString(R.string.family_detail_screen),
                getActivity().getString(R.string.visited), getActivity().getString(R.string.family_detail_screen));
    }

    public void setFamilyBottomView(int id) {
        mFamilyBottomView = id;
    }

    private void init() {
        mFamilyDataList = new ArrayList<>();
        mServantDataList = new ArrayList<>();
        mGeneralDataList = new ArrayList<>();
        mLockatedPreferences = new LockatedPreferences(getActivity());
        mAccountController = AccountController.getInstance();

        mButtonFamilyDetailSkip = (TextView) mFamilyDetailView.findViewById(R.id.mButtonFamilyDetailSkip);

        mLLFamilyDetailAdd = (LinearLayout) mFamilyDetailView.findViewById(R.id.mLLFamilyDetailAdd);
        mLLServantDetailAdd = (LinearLayout) mFamilyDetailView.findViewById(R.id.mLLServantDetailAdd);
        mLLGeneralDetailAdd = (LinearLayout) mFamilyDetailView.findViewById(R.id.mLLGeneralDetailAdd);
        mLLGeneralDetailEdit = (LinearLayout) mFamilyDetailView.findViewById(R.id.mLLGeneralDetailEdit);

        mFamilyDetailContainer = (ViewGroup) mFamilyDetailView.findViewById(R.id.mFamilyDetailContainer);
        mServantDetailContainer = (ViewGroup) mFamilyDetailView.findViewById(R.id.mServantDetailContainer);
        mGeneralDetailContainer = (ViewGroup) mFamilyDetailView.findViewById(R.id.mGeneralDetailContainer);

        mLLFamilyDetailAdd.setOnClickListener(this);
        mLLServantDetailAdd.setOnClickListener(this);
        mLLGeneralDetailAdd.setOnClickListener(this);
        mLLGeneralDetailEdit.setOnClickListener(this);
        mButtonFamilyDetailSkip.setOnClickListener(this);

        if (mFamilyBottomView == 0) {
            mButtonFamilyDetailSkip.setVisibility(View.VISIBLE);
            ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle(getString(R.string.family_detail));
        } else {
            mButtonFamilyDetailSkip.setVisibility(View.GONE);
        }

        ArrayList<AccountData> datas = mAccountController.getmAccountDataList();
        if (datas.size() > 0) {
            if (datas.get(0).getmFamilyDataList().size() > 0) {
                mFamilyDataList = datas.get(0).getmFamilyDataList();
            }

            if (datas.get(0).getmServantDataList().size() > 0) {
                mServantDataList = datas.get(0).getmServantDataList();
            }

            if (datas.get(0).getmGeneralDetailDataList().size() > 0) {
                mLLGeneralDetailAdd.setVisibility(View.GONE);
                mLLGeneralDetailEdit.setVisibility(View.VISIBLE);
                mGeneralDataList = datas.get(0).getmGeneralDetailDataList();
            } else {
                mLLGeneralDetailEdit.setVisibility(View.GONE);
                mLLGeneralDetailAdd.setVisibility(View.VISIBLE);
            }
        }

        for (int i = 0; i < mFamilyDataList.size(); i++) {
            addFamilyDetail(i);
        }

        for (int j = 0; j < mServantDataList.size(); j++) {
            addServantDetail(j);
        }

        for (int k = 0; k < mGeneralDataList.size(); k++) {
            addGeneralDetail(k);
        }
    }

    private void addFamilyDetail(int position) {
        mNewFamilyView = (ViewGroup) LayoutInflater.from(getActivity()).inflate(R.layout.fragment_family_details_row, mFamilyDetailContainer, false);

        mLLFamilyMemberAdd = (LinearLayout) mNewFamilyView.findViewById(R.id.mLLFamilyMemberAdd);
        mTextViewFamilyDetailRowIndex = (TextView) mNewFamilyView.findViewById(R.id.mTextViewFamilyDetailRowIndex);
        mTextViewFamilyDetailRowName = (TextView) mNewFamilyView.findViewById(R.id.mTextViewFamilyDetailRowName);
        mTextViewFamilyDetailRowSex = (TextView) mNewFamilyView.findViewById(R.id.mTextViewFamilyDetailRowSex);
        mTextViewFamilyDetailRowContact = (TextView) mNewFamilyView.findViewById(R.id.mTextViewFamilyDetailRowContact);
        mTextViewFamilyDetailRowContactMaterialStatus = (TextView) mNewFamilyView.findViewById(R.id.mTextViewFamilyDetailRowContactMaterialStatus);

        mLLFamilyMemberAdd.setTag(position);
        mTextViewFamilyDetailRowIndex.setText((position + 1) + "");
        mTextViewFamilyDetailRowName.setText(mFamilyDataList.get(position).getName());
        mTextViewFamilyDetailRowSex.setText(mFamilyDataList.get(position).getGender());
        mTextViewFamilyDetailRowContact.setText(mFamilyDataList.get(position).getMobile());
        mTextViewFamilyDetailRowContactMaterialStatus.setText(mFamilyDataList.get(position).getRelationship());

        mLLFamilyMemberAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onFamilyDetailEdit(v);
            }
        });

        mFamilyDetailContainer.addView(mNewFamilyView, position);
    }

    private void addServantDetail(int position) {
        mNewServantView = (ViewGroup) LayoutInflater.from(getActivity()).inflate(R.layout.fragment_servant_details_row, mServantDetailContainer, false);

        mLLFamilyServantAdd = (LinearLayout) mNewServantView.findViewById(R.id.mLLFamilyServantAdd);
        mTextViewFamilyDetailRowIndex = (TextView) mNewServantView.findViewById(R.id.mTextViewFamilyDetailRowIndex);
        mTextViewFamilyDetailRowName = (TextView) mNewServantView.findViewById(R.id.mTextViewFamilyDetailRowName);
        mTextViewFamilyDetailRowSex = (TextView) mNewServantView.findViewById(R.id.mTextViewFamilyDetailRowSex);
        mTextViewFamilyDetailRowContact = (TextView) mNewServantView.findViewById(R.id.mTextViewFamilyDetailRowContact);
        mTextViewFamilyDetailRowContactMaterialStatus = (TextView) mNewServantView.findViewById(R.id.mTextViewFamilyDetailRowContactMaterialStatus);

        mLLFamilyServantAdd.setTag(position);
        mTextViewFamilyDetailRowIndex.setText((position + 1) + "");
        mTextViewFamilyDetailRowName.setText(mServantDataList.get(position).getName());
        mTextViewFamilyDetailRowSex.setText(mServantDataList.get(position).getGender());
        mTextViewFamilyDetailRowContact.setText(mServantDataList.get(position).getMobile());
        if (mServantDataList.get(position).getMarried().equalsIgnoreCase("N")) {
            mTextViewFamilyDetailRowContactMaterialStatus.setText(getActivity().getString(R.string.unmarried));
        } else {
            mTextViewFamilyDetailRowContactMaterialStatus.setText(getActivity().getString(R.string.married));
        }

        mLLFamilyServantAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onServantDetailEdit(v);
            }
        });

        mServantDetailContainer.addView(mNewServantView, position);
    }

    private void addGeneralDetail(int position) {
        mNewGeneralView = (ViewGroup) LayoutInflater.from(getActivity()).inflate(R.layout.fragment_family_general_row, mGeneralDetailContainer, false);

        TextView mTextViewGeneralDetailRowName = (TextView) mNewGeneralView.findViewById(R.id.mTextViewGeneralDetailRowName);
        TextView mTextViewGeneralDetailRowContact = (TextView) mNewGeneralView.findViewById(R.id.mTextViewGeneralDetailRowContact);
        TextView mTextViewGeneralEmergencyRowName = (TextView) mNewGeneralView.findViewById(R.id.mTextViewGeneralEmergencyRowName);
        TextView mTextViewGeneralEmergencyRowContact = (TextView) mNewGeneralView.findViewById(R.id.mTextViewGeneralEmergencyRowContact);
        TextView mTextViewGeneralVehcleNumber = (TextView) mNewGeneralView.findViewById(R.id.mTextViewGeneralVehcleNumber);

        RadioGroup mRadioGroupVehicle = (RadioGroup) mNewGeneralView.findViewById(R.id.mRadioGroupVehicle);
        RadioButton mRadioTwoWheeler = (RadioButton) mNewGeneralView.findViewById(R.id.mRadioTwoWheeler);
        RadioButton mRadioFourWheeler = (RadioButton) mNewGeneralView.findViewById(R.id.mRadioFourWheeler);
        Spinner mPetsArray = (Spinner) mNewGeneralView.findViewById(R.id.mPetsArray);

        mPetsArray.setEnabled(false);
        mRadioTwoWheeler.setEnabled(false);
        mRadioFourWheeler.setEnabled(false);

        if (!mGeneralDataList.get(position).getDoc_name().equalsIgnoreCase("")) {
            mTextViewGeneralDetailRowName.setText(mGeneralDataList.get(position).getDoc_name());
        } else {
            mTextViewGeneralDetailRowName.setText(getResources().getString(R.string.no_details));
        }

        if (!mGeneralDataList.get(position).getDoc_phone().equalsIgnoreCase("")) {
            mTextViewGeneralDetailRowContact.setText(mGeneralDataList.get(position).getDoc_phone());
        } else {
            mTextViewGeneralDetailRowContact.setText("");
        }

        if (!mGeneralDataList.get(position).getEmergency_contact_person().equalsIgnoreCase("")) {
            mTextViewGeneralEmergencyRowName.setText(mGeneralDataList.get(position).getEmergency_contact_person());
        } else {
            mTextViewGeneralEmergencyRowName.setText(getResources().getString(R.string.no_details));
        }

        if (!mGeneralDataList.get(position).getEcp_phone().equalsIgnoreCase("")) {
            mTextViewGeneralEmergencyRowContact.setText(mGeneralDataList.get(position).getEcp_phone());
        } else {
            mTextViewGeneralEmergencyRowContact.setText("");
        }

        mPetsArray.setSelection(mGeneralDataList.get(position).getPets());

        if (!mGeneralDataList.get(position).getVeh_reg_number().equalsIgnoreCase("")) {
            mTextViewGeneralVehcleNumber.setText(mGeneralDataList.get(position).getVeh_reg_number());
        } else {
            mTextViewGeneralVehcleNumber.setText(getResources().getString(R.string.no_details));
        }

        if (mGeneralDataList.get(position).getVehicle() != 0) {
            if (mGeneralDataList.get(position).getVehicle() == 2) {
                mRadioTwoWheeler.setChecked(true);
                mRadioFourWheeler.setChecked(false);
            } else {
                mRadioTwoWheeler.setChecked(false);
                mRadioFourWheeler.setChecked(true);
            }
        } else {
            mRadioTwoWheeler.setChecked(false);
            mRadioFourWheeler.setChecked(false);
        }

        mGeneralDetailContainer.addView(mNewGeneralView, position);
    }

    private void onFamilyDetailAdd() {
        FragmentManager fragmentManager = getChildFragmentManager();
        AddFamilyDetailFragment addNewAddressFragment = new AddFamilyDetailFragment();
        addNewAddressFragment.setListener(this);
        addNewAddressFragment.setFamilyDataOnEdit(0, null, 0);
        addNewAddressFragment.show(fragmentManager, "FamilyDetailFragment");
    }

    private void onServantDetailAdd() {
        FragmentManager fragmentManager = getChildFragmentManager();
        AddServantDetailFragment addServantDetailFragment = new AddServantDetailFragment();
        addServantDetailFragment.setListener(this);
        addServantDetailFragment.setServantDataOnEdit(0, null, 0);
        addServantDetailFragment.show(fragmentManager, "FamilyDetailFragment");
    }

    private void onServantUpdateRecordClicked(int position) {
        FragmentManager fragmentManager = getChildFragmentManager();
        AddServantDetailFragment updateServantDetailFragment = new AddServantDetailFragment();
        updateServantDetailFragment.setListener(this);
        updateServantDetailFragment.setServantDataOnEdit(1, mServantDataList.get(position), position);
        updateServantDetailFragment.show(fragmentManager, "FamilyDetailFragment");
    }

    private void onUpdateRecordClicked(int position) {
        FragmentManager fragmentManager = getChildFragmentManager();
        AddFamilyDetailFragment addNewAddressFragment = new AddFamilyDetailFragment();
        addNewAddressFragment.setListener(this);
        addNewAddressFragment.setFamilyDataOnEdit(1, mFamilyDataList.get(position), position);
        addNewAddressFragment.show(fragmentManager, "FamilyDetailFragment");
    }

    private void OnAddGeneralDetailClicked() {
        FragmentManager fragmentManager = getChildFragmentManager();
        AddGeneralDetailFragment addGeneralDetailFragment = new AddGeneralDetailFragment();
        addGeneralDetailFragment.setListener(this);
        addGeneralDetailFragment.setGeneralDataOnEdit(0, null);
        addGeneralDetailFragment.show(fragmentManager, "GeneralDetailFragment");
    }

    private void OnEditGeneralDetailClicked(int position) {
        FragmentManager fragmentManager = getChildFragmentManager();
        AddGeneralDetailFragment addGeneralDetailFragment = new AddGeneralDetailFragment();
        addGeneralDetailFragment.setListener(this);
        addGeneralDetailFragment.setGeneralDataOnEdit(1, mGeneralDataList.get(position));
        addGeneralDetailFragment.show(fragmentManager, "GeneralDetailFragment");
    }

    private void openServantActionDialog(final int position) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle("Perform an action");
        builder.setItems(R.array.list_row_action_array, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                switch (which) {
                    case 0:
                        onServantUpdateRecordClicked(position);
                        break;
                    case 1:
                        onDeleteServantRecordClicked(position);
                        break;
                }
            }
        });
        builder.show();
    }

    private void openActionDialog(final int position) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle("Perform an action");
        builder.setItems(R.array.list_row_action_array, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                switch (which) {
                    case 0:
                        onUpdateRecordClicked(position);
                        break;
                    case 1:
                        onDeleteRecordClicked(position);
                        break;
                }
            }
        });
        builder.show();
    }

    private void onDeleteRecordClicked(int position) {
        if (ConnectionDetector.isConnectedToInternet(getActivity())) {
            mPosition = position;
            mProgressDialog = ProgressDialog.show(getActivity(), "", "Please Wait...", false);
            mProgressDialog.show();
            try {
//                mQueue = LockatedVolleyRequestQueue.getInstance(getActivity()).getRequestQueue();
//                lockatedJSONObjectRequest = new LockatedJSONObjectRequest(Request.Method.DELETE, ApplicationURL.deleteFamilyDetailUrl+mFamilyDataList.get(mPosition).getId()+".json?token=" + mLockatedPreferences.getLockatedToken(), null, this, this);
//                lockatedJSONObjectRequest.setTag(DELETE_REQUEST_TAG);
//                mQueue.add(lockatedJSONObjectRequest);

                lockatedVolleyRequestQueue = LockatedVolleyRequestQueue.getInstance(getActivity());
                lockatedVolleyRequestQueue.sendRequest(DELETE_REQUEST_TAG, Request.Method.DELETE, ApplicationURL.deleteFamilyDetailUrl + mFamilyDataList.get(mPosition).getId() + ".json?token=" + mLockatedPreferences.getLockatedToken(), null, this, this);
            } catch (Exception e) {
                e.getMessage();
            }
        } else {
            Utilities.showToastMessage(getActivity(), getActivity().getResources().getString(R.string.internet_connection_error));
        }
    }

    private void onDeleteServantRecordClicked(int position) {
        if (ConnectionDetector.isConnectedToInternet(getActivity())) {
            mPosition = position;
            mProgressDialog = ProgressDialog.show(getActivity(), "", "Please Wait...", false);
            mProgressDialog.show();
            try {

//                mQueue = LockatedVolleyRequestQueue.getInstance(getActivity()).getRequestQueue();
//                lockatedJSONObjectRequest = new LockatedJSONObjectRequest(Request.Method.DELETE, ApplicationURL.deleteServantDetailUrl+mServantDataList.get(mPosition).getId()+".json?token=" + mLockatedPreferences.getLockatedToken(), null, this, this);
//                lockatedJSONObjectRequest.setTag(DELETE_SERVANT_REQUEST_TAG);
//                mQueue.add(lockatedJSONObjectRequest);

                lockatedVolleyRequestQueue = LockatedVolleyRequestQueue.getInstance(getActivity());
                lockatedVolleyRequestQueue.sendRequest(DELETE_SERVANT_REQUEST_TAG, Request.Method.DELETE, ApplicationURL.deleteServantDetailUrl + mServantDataList.get(mPosition).getId() + ".json?token=" + mLockatedPreferences.getLockatedToken(), null, this, this);
            } catch (Exception e) {
                e.getMessage();
            }
        } else {
            Utilities.showToastMessage(getActivity(), getActivity().getResources().getString(R.string.internet_connection_error));
        }
    }

    private void onFamilyDetailEdit(View v) {
        openActionDialog((int) v.getTag());
    }

    private void onServantDetailEdit(View v) {
        openServantActionDialog((int) v.getTag());
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.mLLFamilyDetailAdd:
                onFamilyDetailAdd();
                break;
            case R.id.mLLServantDetailAdd:
                onServantDetailAdd();
                break;
            case R.id.mLLGeneralDetailAdd:
                OnAddGeneralDetailClicked();
                break;
            case R.id.mLLGeneralDetailEdit:
                OnEditGeneralDetailClicked(0);
                break;
            case R.id.mButtonFamilyDetailSkip:
                OnFamilyDetailSkipClicked();
                break;
        }
    }

    private void OnFamilyDetailSkipClicked() {
        if (getActivity() != null) {
            Intent intent = new Intent(getActivity(), HomeActivity.class);
            startActivity(intent);
            getActivity().finish();
        }
    }

    @Override
    public void onNewMemberAdded(ArrayList<FamilyData> familyDataList, int requestId) {
        if (requestId == 0) {
            mFamilyDetailContainer.removeAllViews();

            mFamilyDataList.clear();
            mAccountController.getmAccountDataList().get(0).getmFamilyDataList().clear();

            mAccountController.getmAccountDataList().get(0).setmFamilyDataList(familyDataList);

            for (int i = 0; i < familyDataList.size(); i++) {
                mFamilyDataList.add(familyDataList.get(i));
                addFamilyDetail(i);
            }
        }
    }

    @Override
    public void onNewMemberUpdated(FamilyData familyData, int requestId, int position) {
        mFamilyDetailContainer.removeAllViews();

        mFamilyDataList.get(position).setId(familyData.getId());
        mFamilyDataList.get(position).setId_user(familyData.getId_user());
        mFamilyDataList.get(position).setGender(familyData.getGender());
        mFamilyDataList.get(position).setName(familyData.getName());
        mFamilyDataList.get(position).setRelationship(familyData.getRelationship());
        mFamilyDataList.get(position).setMobile(familyData.getMobile());
        mFamilyDataList.get(position).setDob(familyData.getDob());
        mFamilyDataList.get(position).setMarried(familyData.getMarried());
        mFamilyDataList.get(position).setAnniversary(familyData.getAnniversary());

        for (int i = 0; i < mFamilyDataList.size(); i++) {
            addFamilyDetail(i);
        }

        FamilyData familyDetailData = mAccountController.getmAccountDataList().get(0).getmFamilyDataList().get(position);
        familyDetailData.setId(familyData.getId());
        familyDetailData.setId_user(familyData.getId_user());
        familyDetailData.setGender(familyData.getGender());
        familyDetailData.setName(familyData.getName());
        familyDetailData.setRelationship(familyData.getRelationship());
        familyDetailData.setMobile(familyData.getMobile());
        familyDetailData.setDob(familyData.getDob());
        familyDetailData.setMarried(familyData.getMarried());
        familyDetailData.setAnniversary(familyData.getAnniversary());
    }

    @Override
    public void onNewServantAdded(ArrayList<ServantData> servantDataList, int requestId) {
        if (requestId == 0) {
            mServantDetailContainer.removeAllViews();

            mServantDataList.clear();
            mAccountController.getmAccountDataList().get(0).getmServantDataList().clear();

            mAccountController.getmAccountDataList().get(0).setmServantDataList(servantDataList);

            for (int i = 0; i < servantDataList.size(); i++) {
                mServantDataList.add(servantDataList.get(i));
                addServantDetail(i);
            }
        }
    }

    @Override
    public void onNewServantUpdated(ServantData servantData, int requestId, int position) {
        mServantDataList.get(position).setId(servantData.getId());
        mServantDataList.get(position).setId_user(servantData.getId_user());
        mServantDataList.get(position).setGender(servantData.getGender());
        mServantDataList.get(position).setName(servantData.getName());
        mServantDataList.get(position).setMobile(servantData.getMobile());
        mServantDataList.get(position).setDob(servantData.getDob());
        mServantDataList.get(position).setMarried(servantData.getMarried());

        ServantData servantDetailData = mAccountController.getmAccountDataList().get(0).getmServantDataList().get(position);
        servantDetailData.setId(servantData.getId());
        servantDetailData.setId_user(servantData.getId_user());
        servantDetailData.setGender(servantData.getGender());
        servantDetailData.setName(servantData.getName());
        servantDetailData.setMobile(servantData.getMobile());
        servantDetailData.setDob(servantData.getDob());
        servantDetailData.setMarried(servantData.getMarried());

        mServantDetailContainer.removeAllViews();

        for (int i = 0; i < mServantDataList.size(); i++) {
            addServantDetail(i);
        }
    }

    @Override
    public void onGeneralDetailAdded(GeneralDetailData generalDetailData, int requestId) {
        if (requestId == 0) {
            mLLGeneralDetailAdd.setVisibility(View.GONE);
            mLLGeneralDetailEdit.setVisibility(View.VISIBLE);

            mGeneralDataList.add(generalDetailData);
            mAccountController.getmAccountDataList().get(0).getmGeneralDetailDataList().add(generalDetailData);
            for (int i = 0; i < mAccountController.getmAccountDataList().get(0).getmGeneralDetailDataList().size(); i++) {
                addGeneralDetail(i);
            }
        } else {
            GeneralDetailData generalData = mAccountController.getmAccountDataList().get(0).getmGeneralDetailDataList().get(0);
            generalData.setId(generalDetailData.getId());
            generalData.setId_user(generalDetailData.getId_user());
            generalData.setPets(generalDetailData.getPets());
            generalData.setDoc_name(generalDetailData.getDoc_name());
            generalData.setDoc_phone(generalDetailData.getDoc_phone());
            generalData.setEmergency_contact_person(generalDetailData.getEmergency_contact_person());
            generalData.setEcp_phone(generalDetailData.getEcp_phone());
            generalData.setVehicle(generalDetailData.getVehicle());
            generalData.setVeh_reg_number(generalDetailData.getVeh_reg_number());

            mGeneralDataList.get(0).setId(generalDetailData.getId());
            mGeneralDataList.get(0).setId_user(generalDetailData.getId_user());
            mGeneralDataList.get(0).setPets(generalDetailData.getPets());
            mGeneralDataList.get(0).setDoc_name(generalDetailData.getDoc_name());
            mGeneralDataList.get(0).setDoc_phone(generalDetailData.getDoc_phone());
            mGeneralDataList.get(0).setEmergency_contact_person(generalDetailData.getEmergency_contact_person());
            mGeneralDataList.get(0).setEcp_phone(generalDetailData.getEcp_phone());
            mGeneralDataList.get(0).setVehicle(generalDetailData.getVehicle());
            mGeneralDataList.get(0).setVeh_reg_number(generalDetailData.getVeh_reg_number());

            mGeneralDetailContainer.removeView(mNewGeneralView);
            addGeneralDetail(0);
        }
    }

    @Override
    public void onErrorResponse(VolleyError error) {
        mProgressDialog.dismiss();
        if (getActivity() != null) {
            LockatedRequestError.onRequestError(getActivity(), error);
        }
    }

    @Override
    public void onResponse(Object response) {
        mProgressDialog.dismiss();
        if (getActivity() != null) {
            if (lockatedVolleyRequestQueue.getLockatedJSONObjectRequest().getTag().toString().equals(DELETE_REQUEST_TAG)) {
                JSONObject jsonObject = (JSONObject) response;

                try {
                    mFamilyDataList.clear();
                    mAccountController.getmAccountDataList().get(0).getmFamilyDataList().clear();

                    ArrayList<FamilyData> dataArrayList = new ArrayList<>();
                    JSONArray jsonArray = jsonObject.getJSONArray("user_family");
                    for (int i = 0; i < jsonArray.length(); i++) {
                        JSONObject familyObject = jsonArray.getJSONObject(i);
                        FamilyData familyData = new FamilyData(familyObject);
                        dataArrayList.add(familyData);
                    }

                    mFamilyDetailContainer.removeAllViews();

                    for (int i = 0; i < dataArrayList.size(); i++) {
                        mFamilyDataList.add(dataArrayList.get(i));
                        addFamilyDetail(i);
                    }

                    for (int j = 0; j < dataArrayList.size(); j++) {
                        mAccountController.getmAccountDataList().get(0).getmFamilyDataList().add(dataArrayList.get(j));
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else if (lockatedVolleyRequestQueue.getLockatedJSONObjectRequest().getTag().toString().equals(DELETE_SERVANT_REQUEST_TAG)) {
                JSONObject jsonObject = (JSONObject) response;

                try {
                    mServantDataList.clear();
                    mAccountController.getmAccountDataList().get(0).getmServantDataList().clear();

                    ArrayList<ServantData> dataArrayList = new ArrayList<>();
                    JSONArray jsonArray = jsonObject.getJSONArray("user_servants");
                    for (int i = 0; i < jsonArray.length(); i++) {
                        JSONObject servantObject = jsonArray.getJSONObject(i);
                        ServantData servantData = new ServantData(servantObject);
                        dataArrayList.add(servantData);
                    }

                    mServantDetailContainer.removeAllViews();

                    for (int i = 0; i < dataArrayList.size(); i++) {
                        mServantDataList.add(dataArrayList.get(i));
                        addServantDetail(i);
                    }

                    for (int j = 0; j < dataArrayList.size(); j++) {
                        mAccountController.getmAccountDataList().get(0).getmServantDataList().add(dataArrayList.get(j));
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}