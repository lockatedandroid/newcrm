
package com.android.lockated.model.usermodel.Polls;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class MyPolls {

    @SerializedName("my_polls")
    @Expose
    private ArrayList<MyPoll> myPolls = new ArrayList<MyPoll>();

    /**
     * 
     * @return
     *     The myPolls
     */
    public ArrayList<MyPoll> getMyPolls() {
        return myPolls;
    }

    /**
     * 
     * @param myPolls
     *     The my_polls
     */
    public void setMyPolls(ArrayList<MyPoll> myPolls) {
        this.myPolls = myPolls;
    }

}
