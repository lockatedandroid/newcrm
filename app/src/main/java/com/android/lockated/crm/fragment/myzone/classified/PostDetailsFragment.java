package com.android.lockated.crm.fragment.myzone.classified;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;

import com.android.lockated.R;
import com.android.lockated.crm.activity.MyZoneClassifiedActivity;
import com.android.lockated.information.AccountController;
import com.android.lockated.model.AccountData;
import com.android.lockated.network.ConnectionDetector;
import com.android.lockated.preferences.LockatedPreferences;
import com.android.lockated.request.LockatedJSONObjectRequest;
import com.android.lockated.request.LockatedRequestError;
import com.android.lockated.request.LockatedVolleyRequestQueue;
import com.android.lockated.utils.ApplicationURL;
import com.android.lockated.utils.DatePickerFragment;
import com.android.lockated.utils.MarshMallowPermission;
import com.android.lockated.utils.TimePickerFragment;
import com.android.lockated.utils.Utilities;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;

public class PostDetailsFragment extends Fragment implements View.OnClickListener, AdapterView.OnItemSelectedListener, Response.ErrorListener, Response.Listener<JSONObject> {

    public static final String CREATE = "PostDetailsFragment";
    public static final String REQUEST_TAG = "PostDetailsFragment";
    private static final int REQUEST_CAMERA = 3;
    private static final int REQUEST_GALLERY = 4;
    boolean isSetImage = false;
    int societyId;
    ArrayList<AccountData> accountDataArrayList;
    /*ProgressBar progressBar;*/
    ArrayAdapter<String> arrayadapter;
    MarshMallowPermission marshMallowPermission;
    Uri imageUri;
    JSONArray statusArray;
    int RESULT_OK = 1;
    private View PostDetailsFragmentView;
    private ImageView mImageView;
    private TextView mPost;
    private EditText mEditTextTitle;
    private EditText mEditTextDescription;
    private EditText mEditTextPrice;
    private EditText mExpiryDate;
    private TextView mEditPersonalName;
    private TextView mEditEmail;
    private Spinner mSpinnerCategory;
    private Spinner mSpinnerPurpose;
    private TextView mTextStartdate;
    private TextView mTextStartTime;
    private TextView mContactNumber;
    private ImageView mIageStartDate;
    private ImageView mImageStartTime;
    private int SelectCategory;
    private String SelectPurpose;
    private String strTitle;
    private String strDescription;
    private String strPrice;
    private String strExpiryDate;
    private String strPersonalName;
    private String strEmail;
    private String strContactNumber;
    private String encodedImage = "";
    private RequestQueue mQueue;
    private ProgressDialog mProgressDialog;
    private AccountController accountController;
    private LockatedPreferences mLockatedPreferences;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        PostDetailsFragmentView = inflater.inflate(R.layout.fragment_post_details, container, false);
        marshMallowPermission = new MarshMallowPermission(getActivity());
        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
        accountController = AccountController.getInstance();
        accountDataArrayList = accountController.getmAccountDataList();
        init();
        getData();
        getSpinnerCategory();
        return PostDetailsFragmentView;
    }

    private void getSpinnerCategory() {
        mLockatedPreferences = new LockatedPreferences(getActivity());
        mProgressDialog = ProgressDialog.show(getActivity(), "", "Please Wait...", false);
        mProgressDialog.show();
        if (ConnectionDetector.isConnectedToInternet(getActivity())) {
            mQueue = LockatedVolleyRequestQueue.getInstance(getActivity()).getRequestQueue();
            String url = ApplicationURL.getClassifiedCategory + mLockatedPreferences.getLockatedToken();
            LockatedVolleyRequestQueue lockatedVolleyRequestQueue = LockatedVolleyRequestQueue.getInstance(getActivity());
            lockatedVolleyRequestQueue.sendRequest(CREATE, Request.Method.GET, url,
                    null, new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            mProgressDialog.dismiss();
                            JSONObject jsonObject = null;
                            try {
                                jsonObject = new JSONObject(response);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                            if (jsonObject != null && jsonObject.length() > 0) {
                                try {
                                    if (jsonObject.has("classified_categories")) {
                                        try {
                                            statusArray = jsonObject.getJSONArray("classified_categories");
                                        } catch (JSONException e) {
                                            e.printStackTrace();
                                        }
                                        arrayadapter = new ArrayAdapter<>(getContext(), R.layout.support_simple_spinner_dropdown_item);
                                        try {
                                            for (int i = 0; i < jsonObject.getJSONArray("classified_categories").length(); i++) {
                                                arrayadapter.add(statusArray.getString(i));
                                            }
                                        } catch (JSONException e) {
                                            e.printStackTrace();
                                        }
                                        mSpinnerCategory.setAdapter(arrayadapter);
                                    }
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            } else {
                                Utilities.showToastMessage(getActivity(), "Nothing TO Display");
                            }
                        }
                    }, this, null);

        } else {
            Utilities.showToastMessage(getActivity(), getActivity().getResources().getString(R.string.internet_connection_error));
        }
    }

    private void getData() {

        String name = accountController.getmAccountDataList().get(0).getFirstname() + " " +
                accountController.getmAccountDataList().get(0).getLastname();
        mEditPersonalName.setText(name);
        mEditEmail.setText(accountController.getmAccountDataList().get(0).getEmail());
        mContactNumber.setText(accountController.getmAccountDataList().get(0).getMobile());
    }

    private void init() {
        mLockatedPreferences = new LockatedPreferences(getActivity());
        /*progressBar = (ProgressBar) PostDetailsFragmentView.findViewById(R.id.mProgressBarView);*/
        mImageView = (ImageView) PostDetailsFragmentView.findViewById(R.id.mImageView);
        mSpinnerCategory = (Spinner) PostDetailsFragmentView.findViewById(R.id.mSpinnerCategory);
        mSpinnerPurpose = (Spinner) PostDetailsFragmentView.findViewById(R.id.mSpinnerPurpose);
        mPost = (TextView) PostDetailsFragmentView.findViewById(R.id.mPost);
        mEditTextTitle = (EditText) PostDetailsFragmentView.findViewById(R.id.mEditTextTitle);
        mEditTextDescription = (EditText) PostDetailsFragmentView.findViewById(R.id.mEditTextDescription);
        mEditTextPrice = (EditText) PostDetailsFragmentView.findViewById(R.id.mEditTextPrice);
        mEditPersonalName = (TextView) PostDetailsFragmentView.findViewById(R.id.mEditPersonalName);
        mTextStartdate = (TextView) PostDetailsFragmentView.findViewById(R.id.mTextStartdate);
        mTextStartTime = (TextView) PostDetailsFragmentView.findViewById(R.id.mTextStartTime);
        mEditEmail = (TextView) PostDetailsFragmentView.findViewById(R.id.mEditEmail);
        mContactNumber = (TextView) PostDetailsFragmentView.findViewById(R.id.mContactNumber);
        mIageStartDate = (ImageView) PostDetailsFragmentView.findViewById(R.id.mIageStartDate);
        mImageStartTime = (ImageView) PostDetailsFragmentView.findViewById(R.id.mImageEndTime);

        mIageStartDate.setOnClickListener(this);
        mImageStartTime.setOnClickListener(this);
        mSpinnerCategory.setOnItemSelectedListener(this);
        mSpinnerPurpose.setOnItemSelectedListener(this);
        mPost.setOnClickListener(this);
        mImageView.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.mIageStartDate:
                openDatePicker(mTextStartdate);
                break;
            case R.id.mImageEndTime:
                openTimePicker(mTextStartTime);
                break;
            case R.id.mPost:
                PostDetails();
                break;
            case R.id.mImageView:
                if (!isSetImage) {
                    selectImage();
                } else {
                    selectImageAction();
                }
                break;
        }
    }

    private void selectImageAction() {
        final CharSequence[] options = {"Camera", "Gallery", "Remove Image"};
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle("Add Photo!");
        builder.setItems(options, new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int item) {
                if (options[item].equals("Camera")) {
                    getPhotoFromCamera();
                } else if (options[item].equals("Gallery")) {
                    Intent intent = new Intent();
                    intent.setType("image/*");
                    intent.setAction(Intent.ACTION_GET_CONTENT);
                    startActivityForResult(Intent.createChooser(intent, "Select Picture"), REQUEST_GALLERY);
                } else if (options[item].equals("Remove Image")) {
                    mImageView.setImageResource(R.drawable.ic_account_camera);
                    /*mImageView.setImageBitmap(null);*/
                    encodedImage = "";
                    isSetImage = false;
                }
            }

        });

        builder.show();
    }

    private void openTimePicker(TextView selectDate) {
        TimePickerFragment timePickerFragment = new TimePickerFragment();
        timePickerFragment.setTimePickerView(selectDate);
        DialogFragment newFragment = timePickerFragment;
        newFragment.show(getActivity().getSupportFragmentManager(), "TimePicker");
    }

    private void openDatePicker(TextView selectTime) {
        DatePickerFragment datePickerFragment = new DatePickerFragment();
        datePickerFragment.setDatePickerView(selectTime);
        DialogFragment newFragment = datePickerFragment;
        newFragment.show(getActivity().getSupportFragmentManager(), "DatePicker");
    }

    private void PostDetails() {
        String ExpiryDate = mTextStartdate.getText().toString();
        String ExpiryTime = mTextStartTime.getText().toString();
        societyId = Integer.parseInt(mLockatedPreferences.getSocietyId());
        strTitle = mEditTextTitle.getText().toString();
        strDescription = mEditTextDescription.getText().toString();
        strPrice = (mEditTextPrice.getText().toString());
        strPersonalName = mEditPersonalName.getText().toString();
        strEmail = mEditEmail.getText().toString();
        strContactNumber = mContactNumber.getText().toString();
        if ((mSpinnerPurpose.getSelectedItemPosition() == 0)) {
            Utilities.showToastMessage(getContext(), "Please select Purpose");
        } else if ((TextUtils.isEmpty(strTitle))) {
            Utilities.showToastMessage(getContext(), "Enter Title");
        } else if ((TextUtils.isEmpty(strDescription))) {
            Utilities.showToastMessage(getContext(), "Enter Description");
        } else if ((TextUtils.isEmpty(strPrice))) {
            Utilities.showToastMessage(getContext(), "Enter Price");
        } else if ((ExpiryDate.equals("Date"))) {
            Utilities.showToastMessage(getContext(), "Enter Expiry Date");
        } else if ((ExpiryTime.equals("Time"))) {
            Utilities.showToastMessage(getContext(), "Enter Expiry Time");
        } /*else if (encodedImage == null) {
            Utilities.showToastMessage(getContext(), "Please select Image");

        }*/ else {
            if (ConnectionDetector.isConnectedToInternet(getActivity())) {
                mProgressDialog = ProgressDialog.show(getActivity(), "", "Please Wait...", false);
                mProgressDialog.show();
                JSONObject jsonObjectMain = new JSONObject();
                JSONObject jsonObject = new JSONObject();
                try {
                    jsonObject.put("purpose", SelectPurpose);
                    jsonObject.put("classified_category_id", SelectCategory);
                    jsonObject.put("title", strTitle);
                    jsonObject.put("description", strDescription);
                    jsonObject.put("user_society_id", "" + mLockatedPreferences.getUserSocietyId());
                    jsonObject.put("price", strPrice);
                    jsonObject.put("expire", ExpiryDate + "T" + ExpiryTime);
                    jsonObject.put("society_id", societyId);
                    jsonObject.put("photo", encodedImage);
                    jsonObjectMain.put("classified", jsonObject);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                try {
                    String url = ApplicationURL.postSellClassified + mLockatedPreferences.getLockatedToken();
                    mQueue = LockatedVolleyRequestQueue.getInstance(getActivity()).getRequestQueue();
                    LockatedJSONObjectRequest lockatedJSONObjectRequest = new LockatedJSONObjectRequest(Request.Method.POST,
                            url, jsonObjectMain, this, this);
                    lockatedJSONObjectRequest.setTag(REQUEST_TAG);
                    mQueue.add(lockatedJSONObjectRequest);
                } catch (Exception e) {
                    e.printStackTrace();
                }

            } else {
                Utilities.showToastMessage(getActivity(), getActivity().getResources().getString(R.string.internet_connection_error));
            }
        }
    }

    private void selectImage() {
        final CharSequence[] options = {"Camera", "Gallery"};
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle("Add Photo!");
        builder.setItems(options, new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int item) {
                if (options[item].equals("Camera")) {
                    getPhotoFromCamera();
                } else if (options[item].equals("Gallery")) {
                    Intent intent = new Intent();
                    intent.setType("image/*");
                    intent.setAction(Intent.ACTION_GET_CONTENT);
                    startActivityForResult(Intent.createChooser(intent, "Select Picture"), REQUEST_GALLERY);
                }
            }

        });

        builder.show();

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case REQUEST_CAMERA:
                if (resultCode == Activity.RESULT_OK) {
                    Bitmap thumbnail = (Bitmap) data.getExtras().get("data");
                    if (thumbnail != null) {
                        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
                        thumbnail.compress(Bitmap.CompressFormat.PNG, 90, bytes);
                        File destination = new File(Environment.getExternalStorageDirectory(),
                                System.currentTimeMillis() + ".jpg");
                        FileOutputStream fo;
                        try {
                            destination.createNewFile();
                            fo = new FileOutputStream(destination);
                            fo.write(bytes.toByteArray());
                            fo.close();
                        } catch (FileNotFoundException e) {
                            e.printStackTrace();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                    encodedImage = Utilities.encodeTobase64(thumbnail);
                    mImageView.setImageBitmap(thumbnail);
                    isSetImage = true;
                }
                break;

            case REQUEST_GALLERY:
                Uri imageUri = data.getData();
                try {
                    Bitmap photo = MediaStore.Images.Media.getBitmap(getActivity().getContentResolver(), imageUri);
                    encodedImage = Utilities.encodeTobase64(photo);
                    mImageView.setImageBitmap(photo);
                    isSetImage = true;
                } catch (IOException e) {
                    e.printStackTrace();
                }
                break;
        }

    }

    private void getPhotoFromCamera() {
        if (!marshMallowPermission.checkPermissionForCamera()) {
            marshMallowPermission.requestPermissionForCamera();
        } else {
            if (!marshMallowPermission.checkPermissionForExternalStorage()) {
                marshMallowPermission.requestPermissionForExternalStorage();
            } else {
                Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                startActivityForResult(cameraIntent, REQUEST_CAMERA);
            }
        }
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        if (parent == mSpinnerCategory) {
            SelectCategory = parent.getSelectedItemPosition();
        } else if (parent == mSpinnerPurpose) {
            SelectPurpose = parent.getSelectedItem().toString();
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

    @Override
    public void onErrorResponse(VolleyError error) {
        mProgressDialog.dismiss();
        if (getActivity() != null) {
            LockatedRequestError.onRequestError(getActivity(), error);
        }
    }

    @Override
    public void onResponse(JSONObject response) {
        mProgressDialog.dismiss();
        if (response.has("id")) {
            Intent backIntent = new Intent(getActivity(), MyZoneClassifiedActivity.class);
            backIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            backIntent.putExtra("Listposition", 1);
            startActivity(backIntent);
            getActivity().finish();
        } else {
            Utilities.showToastMessage(getContext(), "Something Went Wrong");
        }

    }
}
