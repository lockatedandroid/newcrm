package com.android.lockated.model;

import org.json.JSONException;
import org.json.JSONObject;

public class ResidenceData
{
    private int id;
    private int id_user;
    private String residence_type;
    private int society_id;
    private int stateId;
    private int addressId;
    private String flat;
    private String block;
    private String building_number;
    private String ownership;
    private String intercom;
    private String landline;
    private String address_id;
    private String society;
    private String societyId;
    private String societyBuildingName;
    private String address1;
    private String address2;
    private String societyArea;
    private String postcode;
    private String city;
    private String societyLatitude;
    private String societyLongitude;
    private String state;
    private String country;
    private String firstName;
    private String lastName;
    private String HousePhoneNumber;


    public ResidenceData(JSONObject jsonObject)
    {
        try
        {
            id = jsonObject.optInt("id");
            id_user = jsonObject.optInt("id_user");
            residence_type = jsonObject.optString("residence_type");
            society_id = jsonObject.optInt("society_id");
            flat = jsonObject.optString("flat");
            block = jsonObject.optString("block");
            building_number = jsonObject.optString("building_number");
            ownership = jsonObject.optString("ownership");
            intercom = jsonObject.optString("intercom");
            landline = jsonObject.optString("landline");
            address_id = jsonObject.optString("address_id");

            JSONObject societyObject = jsonObject.getJSONObject("society");
            if (societyObject.length() > 0)
            {
                society_id = societyObject.optInt("id");
                societyBuildingName = societyObject.optString("building_name");
                address1 = societyObject.optString("address1");
                address2 = societyObject.optString("address2");
                societyArea = societyObject.optString("area");
                postcode = societyObject.optString("postcode");
                city = societyObject.optString("city");
                societyLatitude = societyObject.optString("latitude");
                societyLongitude = societyObject.optString("longitude");
                state = societyObject.optString("state");
                stateId = societyObject.optInt("state_id");
                country = societyObject.optString("country");
            }

            JSONObject individualObject = jsonObject.getJSONObject("address");
            if (individualObject.length() > 0)
            {
                addressId = individualObject.optInt("id");
                firstName = individualObject.optString("firstname");
                lastName = individualObject.optString("lastname");
                address1 = individualObject.optString("address1");
                address2 = individualObject.optString("address2");
                city = individualObject.optString("city");
                postcode = individualObject.optString("zipcode");
                HousePhoneNumber = individualObject.optString("phone");

                JSONObject stateObject = individualObject.getJSONObject("state");
                JSONObject countryObject = individualObject.getJSONObject("country");

                state = stateObject.optString("name");
                stateId = stateObject.optInt("id");
                country = countryObject.optString("iso_name");
            }
        }
        catch (JSONException e)
        {
            e.printStackTrace();
        }
    }

    public int getId()
    {
        return id;
    }

    public int getId_user()
    {
        return id_user;
    }

    public String getResidence_type()
    {
        return residence_type;
    }

    public int getSociety_id()
    {
        return society_id;
    }

    public int getStateId()
    {
        return stateId;
    }

    public int getAddressId()
    {
        return addressId;
    }

    public String getFlat()
    {
        return flat;
    }

    public String getBlock()
    {
        return block;
    }

    public String getBuilding_number()
    {
        return building_number;
    }

    public String getOwnership()
    {
        return ownership;
    }

    public String getIntercom()
    {
        return intercom;
    }

    public String getLandline()
    {
        return landline;
    }

    public String getAddress_id()
    {
        return address_id;
    }

    public String getSociety()
    {
        return society;
    }

    public String getSocietyId()
    {
        return societyId;
    }

    public String getSocietyBuildingName()
    {
        return societyBuildingName;
    }

    public String getAddress1()
    {
        return address1;
    }

    public String getAddress2()
    {
        return address2;
    }

    public String getSocietyArea()
    {
        return societyArea;
    }

    public String getPostcode()
    {
        return postcode;
    }

    public String getCity()
    {
        return city;
    }

    public String getSocietyLatitude()
    {
        return societyLatitude;
    }

    public String getSocietyLongitude()
    {
        return societyLongitude;
    }

    public String getState()
    {
        return state;
    }

    public String getCountry()
    {
        return country;
    }

    public String getFirstName()
    {
        return firstName;
    }

    public String getLastName()
    {
        return lastName;
    }

    public String getHousePhoneNumber()
    {
        return HousePhoneNumber;
    }
}
