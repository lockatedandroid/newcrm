package com.android.lockated.categories.vendors.model;

import org.json.JSONObject;

public class VendorData
{
    private int id;
    private int address_id;
    private String email;
    private String name;
    private String slug;
    private String latitude;
    private String longitude;
    private String distance;
    private String cod;
    private String min_amount;
    private int delivery_charges;

    public VendorData(JSONObject jsonObject)
    {
        id = jsonObject.optInt("id");
        address_id = jsonObject.optInt("address_id");
        email = jsonObject.optString("email");
        name = jsonObject.optString("name");
        slug = jsonObject.optString("slug");
        latitude = jsonObject.optString("latitude");
        longitude = jsonObject.optString("longitude");
        distance = jsonObject.optString("distance");
        cod = jsonObject.optString("cod");
        min_amount = jsonObject.optString("min_amount");
        delivery_charges = jsonObject.optInt("delivery_charges");
    }

    public int getId()
    {
        return id;
    }

    public int getAddress_id()
    {
        return address_id;
    }

    public String getEmail()
    {
        return email;
    }

    public String getName()
    {
        return name;
    }

    public String getSlug()
    {
        return slug;
    }

    public String getLatitude()
    {
        return latitude;
    }

    public String getLongitude()
    {
        return longitude;
    }

    public String getDistance()
    {
        return distance;
    }

    public String getCod()
    {
        return cod;
    }

    public String getMin_amount()
    {
        return min_amount;
    }

    public int getDelivery_charges()
    {
        return delivery_charges;
    }
}
