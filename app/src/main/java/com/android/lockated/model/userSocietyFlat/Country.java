
package com.android.lockated.model.userSocietyFlat;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
public class Country implements Parcelable {

    @SerializedName("id")
    @Expose
    private int id;
    @SerializedName("iso_name")
    @Expose
    private String isoName;
    @SerializedName("iso")
    @Expose
    private String iso;
    @SerializedName("iso3")
    @Expose
    private String iso3;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("numcode")
    @Expose
    private int numcode;

    /**
     * 
     * @return
     *     The id
     */
    public int getId() {
        return id;
    }

    /**
     * 
     * @param id
     *     The id
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * 
     * @return
     *     The isoName
     */
    public String getIsoName() {
        return isoName;
    }

    /**
     * 
     * @param isoName
     *     The iso_name
     */
    public void setIsoName(String isoName) {
        this.isoName = isoName;
    }

    /**
     * 
     * @return
     *     The iso
     */
    public String getIso() {
        return iso;
    }

    /**
     * 
     * @param iso
     *     The iso
     */
    public void setIso(String iso) {
        this.iso = iso;
    }

    /**
     * 
     * @return
     *     The iso3
     */
    public String getIso3() {
        return iso3;
    }

    /**
     * 
     * @param iso3
     *     The iso3
     */
    public void setIso3(String iso3) {
        this.iso3 = iso3;
    }

    /**
     * 
     * @return
     *     The name
     */
    public String getName() {
        return name;
    }

    /**
     * 
     * @param name
     *     The name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * 
     * @return
     *     The numcode
     */
    public int getNumcode() {
        return numcode;
    }

    /**
     * 
     * @param numcode
     *     The numcode
     */
    public void setNumcode(int numcode) {
        this.numcode = numcode;
    }

    @Override
    public int describeContents() {
        return this.hashCode();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(id);
        dest.writeString(isoName);
        dest.writeString(iso);
        dest.writeString(iso3);
        dest.writeString(name);
        dest.writeInt(numcode);
    }

    protected Country(Parcel in) {
        id = in.readInt();
        isoName = in.readString();
        iso = in.readString();
        iso3 = in.readString();
        name = in.readString();
        numcode = in.readInt();
    }

    public static final Creator<Country> CREATOR = new Creator<Country>() {
        @Override
        public Country createFromParcel(Parcel in) {
            return new Country(in);
        }

        @Override
        public Country[] newArray(int size) {
            return new Country[size];
        }
    };

}
