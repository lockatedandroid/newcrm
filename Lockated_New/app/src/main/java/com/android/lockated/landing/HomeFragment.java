package com.android.lockated.landing;

import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;

import com.android.lockated.Interfaces.IRecyclerItemClickListener;
import com.android.lockated.LockatedApplication;
import com.android.lockated.R;
import com.android.lockated.categories.CategoryViewFragment;
import com.android.lockated.categories.adapter.BannerAdapter;
import com.android.lockated.categories.olacabs.activity.OlaMainActivity_Latest;
import com.android.lockated.crm.activity.CRMActivity;
import com.android.lockated.crm.activity.CrmVisitorActivity;
import com.android.lockated.crmadmin.activity.AdminActivity;
import com.android.lockated.information.AccountController;
import com.android.lockated.landing.adapter.HomeScreenAdapter;
import com.android.lockated.landing.model.TaxonomiesData;
import com.android.lockated.model.AccountData;
import com.android.lockated.model.Banner.Banner;
import com.android.lockated.network.ConnectionDetector;
import com.android.lockated.preferences.LockatedPreferences;
import com.android.lockated.request.LockatedJSONObjectRequest;
import com.android.lockated.request.LockatedRequestError;
import com.android.lockated.request.LockatedVolleyRequestQueue;
import com.android.lockated.utils.ApplicationURL;
import com.android.lockated.utils.LockatedConfig;
import com.android.lockated.utils.Utilities;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

public class HomeFragment extends Fragment implements Response.Listener, Response.ErrorListener, IRecyclerItemClickListener,
        ViewPager.OnPageChangeListener {

    private View mHomeFragmentView;
    private RecyclerView mRecyclerView;
    private ProgressBar mProgressBarView;
    private HomeScreenAdapter mHomeScreenAdapter;
    private RecyclerView.LayoutManager mLayoutManager;
    private RequestQueue mQueue;
    ArrayList<AccountData> datas;
    private AccountController mAccountController;
    private LockatedPreferences mLockatedPreferences;
    private ArrayList<TaxonomiesData> mTaxonomiesDatas;
    public static final String REQUEST_TAG = "HomeFragment";
    public static final String SEND_USER_DETAILS = "GCMHomeFragment";

    String name = "home";//For Baner Adapter
    Banner banner;
    Runnable Update;
    Handler handler;
    Timer swipeTimer;
    private ImageView[] dots;
    BannerAdapter bannerAdapter;
    LinearLayout pager_indicator;
    ViewPager mImageViewCategoryItem;
    ArrayList<Banner> bannerArrayList;
    String GET_BANNER = "HomeFragmentBanner";
    boolean isUserApproved, timerNotCancelled = true;
    RelativeLayout pagerIndicatoRelative, recyclerGrid;
    String tagSocietyId, device_type, gcm_key, device_name, device_os_version;
    int oTypeItemId, dotsCount, NUM_PAGES, currentPage, taxon_id, otype_id;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mHomeFragmentView = inflater.inflate(R.layout.fragment_home, container, false);
        init();
        return mHomeFragmentView;
    }

    public void setHomeFragmentData(int oTypeItemId, int taxon_id, int otype_id) {
        this.oTypeItemId = oTypeItemId;
        this.taxon_id = taxon_id;
        this.otype_id = otype_id;
    }

    private void init() {
        handler = new Handler();
        swipeTimer = new Timer();
        bannerArrayList = new ArrayList<>();
        mLockatedPreferences = new LockatedPreferences(getActivity());
        mAccountController = AccountController.getInstance();
        datas = mAccountController.getmAccountDataList();
        Utilities.ladooIntegration(getActivity(), getString(R.string.home_screen));
        mTaxonomiesDatas = new ArrayList<>();
        mProgressBarView = (ProgressBar) mHomeFragmentView.findViewById(R.id.mProgressBarView);
        mRecyclerView = (RecyclerView) mHomeFragmentView.findViewById(R.id.mRecyclerView);

        mImageViewCategoryItem = (ViewPager) mHomeFragmentView.findViewById(R.id.mImageViewCategoryItem);
        mImageViewCategoryItem.setOnPageChangeListener(this);
        pager_indicator = (LinearLayout) mHomeFragmentView.findViewById(R.id.viewPagerCountDots);
        pagerIndicatoRelative = (RelativeLayout) mHomeFragmentView.findViewById(R.id.pagerIndicatoRelative);
        recyclerGrid = (RelativeLayout) mHomeFragmentView.findViewById(R.id.recyclerGrid);

        mLayoutManager = new GridLayoutManager(getActivity(), LockatedConfig.SPAN_COUNT);
        mRecyclerView.setLayoutManager(mLayoutManager);
        mHomeScreenAdapter = new HomeScreenAdapter(mTaxonomiesDatas, this, getContext());
        mRecyclerView.setAdapter(mHomeScreenAdapter);
        bannerAdapter = new BannerAdapter(getActivity(), bannerArrayList, true, name);
        mImageViewCategoryItem.setAdapter(bannerAdapter);

        getHomeViewData();
        sendDeviceDetails();
        getLandingBanner();
        if (oTypeItemId != 666) {
            onCategoryViewClicked(oTypeItemId, taxon_id, otype_id);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        LockatedApplication.getInstance().trackScreenView(getString(R.string.home_screen));
        Utilities.lockatedGoogleAnalytics(getString(R.string.home_screen),
                getString(R.string.visited), getString(R.string.home_screen));
        isUserApprovedValue();
    }

    private void isUserApprovedValue() {
        try {
            if (mLockatedPreferences.getAccountData().getmResidenceDataList().size() > 0) {
                tagSocietyId = String.valueOf(mLockatedPreferences.getAccountData().getmResidenceDataList().get(0).getSociety_id());
                isUserApproved = mLockatedPreferences.getAccountData().is_approve();
                mLockatedPreferences.setIsUserApprovedInSociety(isUserApproved);
                if (!mLockatedPreferences.getSocietyId().equalsIgnoreCase(getActivity().getResources().getString(R.string.blank_value))) {
                    //Log.e("Change", "Society");
                } else if (tagSocietyId == null || tagSocietyId.equals(getActivity().getResources().getString(R.string.null_value))
                        || tagSocietyId.equals(getActivity().getResources().getString(R.string.null_value))) {
                    mLockatedPreferences.setSocietyId(getActivity().getResources().getString(R.string.blank_value));
                } else {
                    mLockatedPreferences.setSocietyId(tagSocietyId);
                }
            } else {
                mLockatedPreferences.setSocietyId(getActivity().getResources().getString(R.string.blank_value));
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

   /* private void getRolesData() {
        if (ConnectionDetector.isConnectedToInternet(getActivity())) {
            mProgressBarView.setVisibility(View.VISIBLE);
            String url = ApplicationURL.getRolesDataUrl + mLockatedPreferences.getLockatedToken()
                    + ApplicationURL.societyId + mLockatedPreferences.getSocietyId();
            LockatedVolleyRequestQueue mLockatedVolleyRequestQueue = LockatedVolleyRequestQueue.getInstance(getActivity());
            mLockatedVolleyRequestQueue.sendRequest(GET_ROLES_TAG, Request.Method.GET, url, null, this, this);
        } else {
            Utilities.showToastMessage(getActivity(), getActivity().getResources().getString(R.string.internet_connection_error));
        }
    }*/

    private void getHomeViewData() {
        if (ConnectionDetector.isConnectedToInternet(getActivity())) {
            mProgressBarView.setVisibility(View.VISIBLE);
            String url = ApplicationURL.getCategoryUrl + "?token=" + mLockatedPreferences.getLockatedToken();
            LockatedVolleyRequestQueue mLockatedVolleyRequestQueue = LockatedVolleyRequestQueue.getInstance(getActivity());
            mLockatedVolleyRequestQueue.sendRequest(REQUEST_TAG, Request.Method.GET, url, null, this, this);
        } else {
            Utilities.showToastMessage(getActivity(), getActivity().getResources().getString(R.string.internet_connection_error));
        }
    }

    private void sendDeviceDetails() {
        if (ConnectionDetector.isConnectedToInternet(getActivity())) {
            mProgressBarView.setVisibility(View.VISIBLE);
            LockatedPreferences mLockatedPreferences = new LockatedPreferences(getActivity());
            int verCode = 0;
            PackageInfo pInfo;
            try {
                pInfo = getActivity().getPackageManager().getPackageInfo(getActivity().getPackageName(), 0);
                verCode = pInfo.versionCode;
            } catch (PackageManager.NameNotFoundException e) {
                e.printStackTrace();
            }

            device_type = "android";
            gcm_key = mLockatedPreferences.getGcmId();
            device_name = Build.MODEL;
            device_os_version = getAndroidVersion();

            JSONObject jsonObject = new JSONObject();
            JSONObject jsonObjectMain = new JSONObject();
            try {
                jsonObject.put("device_id", "1");
                jsonObject.put("device_type", device_type);
                jsonObject.put("gcm_key", gcm_key);
                jsonObject.put("device_name", device_name);
                jsonObject.put("device_os_version", device_os_version);
                jsonObject.put("app_version", verCode);
                jsonObjectMain.put("user_device", jsonObject);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            String url = ApplicationURL.sendUserDetails + mLockatedPreferences.getLockatedToken();
            LockatedVolleyRequestQueue mLockatedVolleyRequestQueue = LockatedVolleyRequestQueue.getInstance(getActivity());
            mLockatedVolleyRequestQueue.sendRequest(SEND_USER_DETAILS, Request.Method.POST, url, jsonObjectMain, this, this);
        } else {
            Utilities.showToastMessage(getActivity(), getActivity().getResources().getString(R.string.internet_connection_error));
        }
    }

    public String getAndroidVersion() {
        String release = Build.VERSION.RELEASE;
        int sdkVersion = Build.VERSION.SDK_INT;
        return "Android SDK: " + sdkVersion + " (" + release + ")";
    }

    public void getLandingBanner() {
        if (getActivity() != null) {
            if (ConnectionDetector.isConnectedToInternet(getActivity())) {
                String url = ApplicationURL.getLandingBanner + "&token=" + mLockatedPreferences.getLockatedToken();
                mQueue = LockatedVolleyRequestQueue.getInstance(getActivity()).getRequestQueue();
                LockatedJSONObjectRequest lockatedJSONObjectRequest = new LockatedJSONObjectRequest(Request.Method.GET,
                        url, null, this, this);
                lockatedJSONObjectRequest.setTag(GET_BANNER);
                mQueue.add(lockatedJSONObjectRequest);
            } else {
                Utilities.showToastMessage(getActivity(), getActivity().getResources().getString(R.string.internet_connection_error));
            }
        }
    }

    @Override
    public void onErrorResponse(VolleyError error) {
        mProgressBarView.setVisibility(View.INVISIBLE);
        if (getActivity() != null) {
            LockatedRequestError.onRequestError(getActivity(), error);
        }
    }

    @Override
    public void onResponse(Object response) {
        mProgressBarView.setVisibility(View.INVISIBLE);
        if (getActivity() != null) {
            try {
                JSONObject jsonObject = (JSONObject) response;
                if (((JSONObject) response).has("gcm_key")) {
                } else if (jsonObject.has("name")) {
                    mLockatedPreferences.setRolesJson(jsonObject.toString());
                } else if (jsonObject.has("deliverylabel")) {
                    if (!jsonObject.getString("deliverylabel").equals("BOOKING_CANCELLED")) {
                        if (jsonObject.has("message")) {
                            JSONObject jsonObject1 = jsonObject.getJSONObject("tpdetails");
                            boolean current_status = jsonObject1.getBoolean("current_status");

                            if (current_status) {
                                mLockatedPreferences.setIsAutoBooked(true);
                                mLockatedPreferences.setTpdetail(jsonObject1.toString());
                            }
                        } else {
                            JSONObject jsonObject1 = jsonObject.getJSONObject("tpdetails");
                            boolean current_status = jsonObject1.getBoolean("current_status");
                            if (current_status) {
                                mLockatedPreferences.setIsCabBooked(true);
                                //mLockatedPreferences.getOlaToken();
                                mLockatedPreferences.setTpdetail(jsonObject1.toString());
                            }
                        }
                    } else {
                        mLockatedPreferences.setIsCabBooked(false);
                        mLockatedPreferences.setTpdetail("");
                    }
                } else if (jsonObject.has("banners")) {
                    if (jsonObject.getJSONArray("banners").length() > 0) {
                        JSONArray jsonArray = jsonObject.getJSONArray("banners");
                        for (int i = 0; i < jsonArray.length(); i++) {
                            banner = new Banner(jsonArray.getJSONObject(i));
                            bannerArrayList.add(banner);
                        }
                        bannerAdapter.notifyDataSetChanged();
                        NUM_PAGES = jsonArray.length() + 1;
                        Update = new Runnable() {
                            public void run() {
                                if (currentPage == NUM_PAGES - 1) {
                                    currentPage = 0;
                                }
                                mImageViewCategoryItem.setCurrentItem(currentPage++, true);
                            }
                        };
                        if (timerNotCancelled) {
                            swipeTimer.schedule(new TimerTask() {
                                @Override
                                public void run() {
                                    handler.post(Update);
                                }
                            }, /*10000*/NUM_PAGES * 10000, 5000);
                            swipeTimer = new Timer(); //This is new
                            swipeTimer.schedule(new TimerTask() {
                                @Override
                                public void run() {
                                    handler.post(Update);
                                }
                            }, 0, NUM_PAGES * 2000); // execute in every 15sec
                            setUiPageViewController(jsonArray.length());
                        }
                    } else {
                        pagerIndicatoRelative.setVisibility(View.GONE);
                    }
                } else {
                    try {
                        JSONArray jsonArray = jsonObject.getJSONArray("taxonomies");
                        for (int i = 0; i < jsonArray.length(); i++) {
                            JSONObject taxonomiesObject = jsonArray.getJSONObject(i);
                            TaxonomiesData taxonomiesData = new TaxonomiesData(taxonomiesObject);
                            mTaxonomiesDatas.add(taxonomiesData);
                        }

                        //----------------------------Changes by Bhavesh on 8-9-2016 at 12.50-------------//
                        String societyIndexValue = getMySocietyRoles();
                        if (societyIndexValue != null && societyIndexValue.equals("true") &&
                                !mLockatedPreferences.getSocietyId().equals(getActivity().getResources().getString(R.string.blank_value))) {
                            String crmJobj = "{\n" + "\n" + "\"id\": 101,\n" + "\n" + "\"name\": \"My Society\",\n" +
                                    "\n" + "\"root\": {\n" + "\"id\": 111,\n" + "\n" + "\"name\": \"My Society\",\n" +
                                    "\n" + "\"pretty_name\": \"My Society\",\n" + "\n" + "\"permalink\": \"My Society\",\n" +
                                    "\n" + "\"parent_id\": null,\n" + "\n" + "\"taxonomy_id\": 101,\n" +
                                    "\n" + "\"taxons\": []\n" + "\n" + "}\n" + "\n" + "}";
                            JSONObject crmJsonObject = new JSONObject(crmJobj);
                            TaxonomiesData taxonomiesData1 = new TaxonomiesData(crmJsonObject);
                            mTaxonomiesDatas.add(taxonomiesData1);
                        }
                        // ------------------------------------------------------------------------------//

                        String indexVal = getUserRoles();
                        String IndexValue = getGatekeeperRole();
                        if (isUserApproved) {
                            if (indexVal != null && indexVal.equals("true")) {
                                String noticejObj = "{\n" + "\n" + "\"id\": 102,\n" + "\n" + "\"name\": \"Admin\",\n" +
                                        "\n" + "\"root\": {\n" + "\"id\": 112,\n" + "\n" + "\"name\": \"Admin\",\n" +
                                        "\n" + "\"pretty_name\": \"Admin\",\n" + "\n" + "\"permalink\": \"Admin\",\n" +
                                        "\n" + "\"parent_id\": null,\n" + "\n" + "\"taxonomy_id\": 102,\n" +
                                        "\n" + "\"taxons\": []\n" + "\n" + "}\n" + "\n" + "}";
                                JSONObject noticejsonObject1 = new JSONObject(noticejObj);
                                TaxonomiesData noticetaxonomiesData = new TaxonomiesData(noticejsonObject1);
                                mTaxonomiesDatas.add(noticetaxonomiesData);
                            }
                        }
                        if (IndexValue != null && IndexValue.equals("true") && societyIndexValue != null && !societyIndexValue.equals("true")) {
                            String gateObj = "{\n" + "\n" + "\"id\": 103,\n" + "\n" + "\"name\": \"Visitor Corner\",\n" +
                                    "\n" + "\"root\": {\n" + "\"id\": 113,\n" + "\n" + "\"name\": \"Visitor Corner\",\n" +
                                    "\n" + "\"pretty_name\": \"Visitor Corner\",\n" + "\n" + "\"permalink\": \"Visitor Corner\",\n" +
                                    "\n" + "\"parent_id\": null,\n" + "\n" + "\"taxonomy_id\": 103,\n" +
                                    "\n" + "\"taxons\": []\n" + "\n" + "}\n" + "\n" + "}";
                            JSONObject noticejsonObject = new JSONObject(gateObj);
                            TaxonomiesData noticetaxonomiesData = new TaxonomiesData(noticejsonObject);
                            mTaxonomiesDatas.add(noticetaxonomiesData);
                        }

                        ///-----------------Changes by Bhavesh on 8-9-2016 at 12.5.------------------///

                        String olaobj = "{\n" + "\n" + "\"id\": 104,\n" + "\n" + "\"name\": \"Cab\",\n" +
                                "\n" + "\"root\": {\n" + "\"id\": 113,\n" + "\n" + "\"name\": \"Cab\",\n" +
                                "\n" + "\"pretty_name\": \"Cab\",\n" + "\n" + "\"permalink\": \"Cab\",\n" +
                                "\n" + "\"parent_id\": null,\n" + "\n" + "\"taxonomy_id\": 104,\n" +
                                "\n" + "\"taxons\": []\n" + "\n" + "}\n" + "\n" + "}";
                        JSONObject olajsonobject = new JSONObject(olaobj);
                        TaxonomiesData olataxdata = new TaxonomiesData(olajsonobject);
                        mTaxonomiesDatas.add(olataxdata);


                        //--------------------------------------------------------------------------------//
                        mLockatedPreferences.setTaxonomyData(jsonArray);


                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        mHomeScreenAdapter.notifyDataSetChanged();
    }

    @Override
    public void onRecyclerItemClick(View view, int position) {
        switch (mTaxonomiesDatas.get(position).getId()) {
            case 1:
                onCategoryViewClicked(position, 666, 666);
                break;
            case 2:
                onCategoryViewClicked(position, 666, 666);
                break;
            case 6:
                onCategoryViewClicked(position, 666, 666);
                break;
            case 8:
                onCategoryViewClicked(position, 666, 666);
                break;
            case 10:
                onCategoryViewClicked(position, 666, 666);
                break;
            case 18:
                onCategoryViewClicked(position, 666, 666);
                break;
            case 28:
                onCategoryViewClicked(position, 666, 666);
                break;
            case 31:
                onCategoryViewClicked(position, 666, 666);
                break;
            case 33:
                onCategoryViewClicked(position, 666, 666);
                break;

            // --------------------Changes by Bhavesh on 8-9-2016 at 12.50 -------------------//

            case 101:
                String mySocietyRoles = getMySocietyRoles();
                String gatekeeperRole = getGatekeeperRole();
                if (!mLockatedPreferences.getSocietyId().equals(getActivity().getResources().getString(R.string.blank_value))) {
                    if (isUserApproved && !mySocietyRoles.equals(getActivity().getResources().getString(R.string.blank_value))) {
                        Intent cemIntent = new Intent(getActivity(), CRMActivity.class);
                        cemIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        startActivity(cemIntent);
                        getActivity().finish();
                    } else {
                        if (gatekeeperRole.equals("true")) {
                            Utilities.showToastMessage(getActivity(), getActivity().getResources().getString(R.string.no_gatekeeper_permission_error));
                        } else {
                            Utilities.showToastMessage(getActivity(), getActivity().getResources().getString(R.string.not_approved) + " or "
                                    + getActivity().getResources().getString(R.string.not_in_society));
                        }
                    }
                } else {
                    Utilities.showToastMessage(getActivity(), getActivity().getResources().getString(R.string.not_in_society));
                }
                break;


            //---------------------------------------------------------------------------------------------------------//

            case 102:
                String adminMySocietyRoles = getMySocietyRoles();
                if (!mLockatedPreferences.getSocietyId().equals(getActivity().getResources().getString(R.string.blank_value))) {
                    if (isUserApproved && !adminMySocietyRoles.equals(getActivity().getResources().getString(R.string.blank_value))) {
                        Intent adminIntent = new Intent(getActivity(), AdminActivity.class);
                        adminIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        startActivity(adminIntent);
                        getActivity().finish();
                    } else {
                        Utilities.showToastMessage(getActivity(), getActivity().getResources().getString(R.string.not_approved) + " or "
                                + getActivity().getResources().getString(R.string.not_in_society));
                    }
                } else {
                    Utilities.showToastMessage(getActivity(), getActivity().getResources().getString(R.string.not_in_society));
                }
                break;
            case 103:
                String gatekeeperRoles = getGatekeeperRole();
                if (!mLockatedPreferences.getSocietyId().equals(getActivity().getResources().getString(R.string.blank_value))) {
                    if (!gatekeeperRoles.equals(getActivity().getResources().getString(R.string.blank_value))) {
                        Intent intent = new Intent(getActivity(), CrmVisitorActivity.class);//HomeGatekeeperActivity
                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        getActivity().startActivity(intent);
                        getActivity().finish();
                    } else {
                        Utilities.showToastMessage(getActivity(), getActivity().getResources().getString(R.string.not_gatekeeper) + " or "
                                + getActivity().getResources().getString(R.string.please_contact_us));
                    }
                } else {
                    Utilities.showToastMessage(getActivity(), getActivity().getResources().getString(R.string.not_in_society));
                }
                break;

            case 104:
                Intent intent = new Intent(getActivity(), OlaMainActivity_Latest.class);
                getActivity().startActivity(intent);
                break;

            default:
                onCategoryViewClicked(position, 666, 666);
                break;

        }
    }

    private void onCategoryViewClicked(int pos, int taxon_id, int otype_id) {
        if (mTaxonomiesDatas.size() == 0) {
            ArrayList<TaxonomiesData> mTaxonomiesDatas = new ArrayList<>();
            JSONArray jsonArray = mLockatedPreferences.getTaxonomyData();

            for (int i = 0; i < jsonArray.length(); i++) {
                try {
                    JSONObject taxonomiesObject = jsonArray.getJSONObject(i);
                    TaxonomiesData taxonomiesData = new TaxonomiesData(taxonomiesObject);
                    mTaxonomiesDatas.add(taxonomiesData);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            CategoryViewFragment categoryViewFragment = new CategoryViewFragment();
            categoryViewFragment.setTaxonomiesData(mTaxonomiesDatas, pos, taxon_id, otype_id);
            getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.mLandingContainer, categoryViewFragment).commit();
        } else {
            CategoryViewFragment categoryViewFragment = new CategoryViewFragment();
            categoryViewFragment.setTaxonomiesData(mTaxonomiesDatas, pos, taxon_id, otype_id);
            getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.mLandingContainer, categoryViewFragment).commit();
        }
    }

    public String getGatekeeperRole() {
        String indexVal = getActivity().getResources().getString(R.string.blank_value);
        if (mLockatedPreferences.getRolesJson() != null && !mLockatedPreferences.getRolesJson().equals("blank")) {
            try {
                JSONObject visitorJsonObj = new JSONObject(mLockatedPreferences.getRolesJson());
                for (int i = 0; i < visitorJsonObj.getJSONArray(getActivity().getResources().getString(R.string.permissions_value)).length(); i++) {
                    JSONObject jsonObject1 = visitorJsonObj.getJSONArray(getActivity().getResources().getString(R.string.permissions_value)).getJSONObject(i);
                    if (jsonObject1.has(getActivity().getResources().getString(R.string.section_value))
                            && jsonObject1.getString(getActivity().getResources().getString(R.string.section_value)).equals(getActivity().getResources().getString(R.string.spree_gatekeeper))) {
                        indexVal = jsonObject1.getJSONObject(getActivity().getResources().getString(R.string.permission_value)).getString(getActivity().getResources().getString(R.string.index_value));
                    }
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return indexVal;
    }

    public String getUserRoles() {
        String indexVal = getActivity().getResources().getString(R.string.blank_value);
        if (mLockatedPreferences.getRolesJson() != null && !mLockatedPreferences.getRolesJson().equals("blank")) {
            try {
                JSONObject noticeJsonObj = new JSONObject(mLockatedPreferences.getRolesJson());
                for (int i = 0; i < noticeJsonObj.getJSONArray(getActivity().getResources().getString(R.string.permissions_value)).length(); i++) {
                    JSONObject jsonObject1 = noticeJsonObj.getJSONArray(getActivity().getResources().getString(R.string.permissions_value)).getJSONObject(i);
                    if (jsonObject1.has(getActivity().getResources().getString(R.string.section_value))
                            && jsonObject1.getString(getActivity().getResources().getString(R.string.section_value)).equals(getActivity().getResources().getString(R.string.spree_society_admin_value))) {
                        indexVal = jsonObject1.getJSONObject(getActivity().getResources().getString(R.string.permission_value)).getString(getActivity().getResources().getString(R.string.index_value));
                    }
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return indexVal;
    }

    public String getMySocietyRoles() {
        String mySocietyRoles = getActivity().getResources().getString(R.string.blank_value);
        if (mLockatedPreferences.getRolesJson() != null && !mLockatedPreferences.getRolesJson().equals("blank")) {
            try {
                JSONObject noticeJsonObj = new JSONObject(mLockatedPreferences.getRolesJson());
                for (int i = 0; i < noticeJsonObj.getJSONArray(getActivity().getResources().getString(R.string.permissions_value)).length(); i++) {
                    JSONObject jsonObject1 = noticeJsonObj.getJSONArray(getActivity().getResources().getString(R.string.permissions_value)).getJSONObject(i);
                    if (jsonObject1.has(getActivity().getResources().getString(R.string.section_value))
                            && jsonObject1.getString(getActivity().getResources().getString(R.string.section_value)).equals(getActivity().getResources().getString(R.string.spree_mysociety_value))) {
                        if (jsonObject1.getJSONObject(getActivity().getResources().getString(R.string.permission_value)).has(getActivity().getResources().getString(R.string.index_value))) {
                            mySocietyRoles = jsonObject1.getJSONObject(getActivity().getResources().getString(R.string.permission_value)).getString(getActivity().getResources().getString(R.string.index_value));
                        }
                    }
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return mySocietyRoles;
    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }

    @Override
    public void onPageSelected(int position) {
        if (getActivity() != null && dotsCount != 0) {
            for (int i = 0; i < dotsCount; i++) {
                dots[i].setImageDrawable(getResources().getDrawable(R.drawable.circle_128));
            }
            dots[position].setImageDrawable(getResources().getDrawable(R.drawable.bullet));
        }
    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }

    private void setUiPageViewController(int count) {

        dotsCount = count;
        dots = new ImageView[dotsCount];
        for (int i = 0; i < dotsCount; i++) {
            dots[i] = new ImageView(getActivity());
            dots[i].setImageDrawable(getResources().getDrawable(R.drawable.circle_128));
            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                    15, LinearLayout.LayoutParams.WRAP_CONTENT
            );
            params.setMargins(1, 0, 1, 0);
            pager_indicator.addView(dots[i], params);
        }
        dots[0].setImageDrawable(getResources().getDrawable(R.drawable.bullet));
    }

    @Override
    public void onPause() {
        super.onPause();
        swipeTimer.cancel();
        timerNotCancelled = false;
        handler.removeCallbacks(Update);
    }

}