package com.android.lockated.model.usermodel.EventModel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class EventClass {

    @SerializedName("events")
    @Expose
    private ArrayList<Events> events = new ArrayList<Events>();
    @SerializedName("shared")
    @Expose
    private ArrayList<Object> shared = new ArrayList<Object>();

    /**
     * @return The eventSubs
     */
    public ArrayList<Events> getEvents() {
        return events;
    }

    /**
     * @param events The events
     */
    public void setEvents(ArrayList<Events> events) {
        this.events = events;
    }

    /**
     * @return The shared
     */
    public ArrayList<Object> getShared() {
        return shared;
    }

    /**
     * @param shared The shared
     */
    public void setShared(ArrayList<Object> shared) {
        this.shared = shared;
    }

}
 /*{
    @SerializedName("date")
    @Expose
    private String date;
    @SerializedName("eventSubs")
    @Expose
    private ArrayList<EventSub> eventSubs = new ArrayList<EventSub>();

    public EventClass(JSONObject jsonObject) {

        try {
            date = jsonObject.optString("date");
            JSONArray jsonArray = jsonObject.getJSONArray("eventSubs");
            for (int i = 0; i < jsonArray.length(); i++) {
                EventSub event = new EventSub(jsonArray.getJSONObject(i));
                eventSubs.add(event);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public ArrayList<EventSub> getEventSubs() {
        return eventSubs;
    }

    public void setNotices(ArrayList<EventSub> eventSubs) {
        this.eventSubs = eventSubs;
    }

}*/