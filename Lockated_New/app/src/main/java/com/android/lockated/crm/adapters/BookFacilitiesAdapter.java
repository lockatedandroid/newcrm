package com.android.lockated.crm.adapters;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.android.lockated.R;
import com.android.lockated.crm.detailView.FacilitiesDetailView;
import com.android.lockated.model.usermodel.Facilities.FacilityHistory.Amenity;

import java.util.ArrayList;

public class BookFacilitiesAdapter extends RecyclerView.Adapter<BookFacilitiesAdapter.FacilitiesViewHolder> {

    private final ArrayList<Amenity> amenities;
    private final String CREATE;

    LayoutInflater layoutInflater;
    Context context;
    FragmentManager childFragmentManager;

    public BookFacilitiesAdapter(Context context, String CREATE, FragmentManager childFragmentManager, ArrayList<Amenity> amenities) {
        this.CREATE = CREATE;
        this.context = context;
        layoutInflater = LayoutInflater.from(context);
        this.amenities = amenities;
        this.childFragmentManager = childFragmentManager;
    }

    @Override
    public FacilitiesViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = layoutInflater.inflate(R.layout.facilities_adapter_view, parent, false);
        FacilitiesViewHolder facilitiesViewHolder = new FacilitiesViewHolder(v);
        return facilitiesViewHolder;
    }

    @Override
    public void onBindViewHolder(FacilitiesViewHolder holder, final int position) {
        holder.venueName.setText(amenities.get(position).getName());
        String cost = String.valueOf("" + amenities.get(position).getCost());
        if (cost.equals("0.0")) {
            holder.costValue.setText("Free");
        } else {
            holder.costValue.setText(cost);
        }
        holder.costTypeValue.setText(amenities.get(position).getCostType());

        if (CREATE!=null&&CREATE.equals("true")) {
            holder.block.setVisibility(View.VISIBLE);
        } else {
            holder.block.setVisibility(View.INVISIBLE);
        }
        holder.block.setTag(position);
        holder.block.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int position = (int) v.getTag();
                try {
                    Bundle args = new Bundle();
                    args.putString("facility_id", "" + amenities.get(position).getId());
                    args.putString("cost", "" + amenities.get(position).getCost());
                    args.putString("name", amenities.get(position).getName());
                    args.putString("valueId", "0");
                    args.putString("cost_type", amenities.get(position).getCostType());
                    DialogFragment dialog = new FacilitiesDetailView();
                    dialog.setArguments(args);
                    dialog.show(childFragmentManager, "dialog");
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return amenities.size();
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }

    public class FacilitiesViewHolder extends RecyclerView.ViewHolder {
        TextView venueName, costValue, block, costTypeValue;

        public FacilitiesViewHolder(View itemView) {
            super(itemView);

            venueName = (TextView) itemView.findViewById(R.id.venuTxt);
            costValue = (TextView) itemView.findViewById(R.id.costValue);
            block = (TextView) itemView.findViewById(R.id.blockTextView);
            costTypeValue = (TextView) itemView.findViewById(R.id.costTypeValue);

        }
    }
}
