package com.android.lockated.crmadmin.fragment;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.android.lockated.LockatedApplication;
import com.android.lockated.R;
import com.android.lockated.crmadmin.activity.AdminMyZoneActivity;
import com.android.lockated.crmadmin.activity.PublishSellClassifiesDetailActivity;
import com.android.lockated.information.AccountController;
import com.android.lockated.model.AccountData;
import com.android.lockated.model.AdminModel.AdminClassified.Classifieds;
import com.android.lockated.network.ConnectionDetector;
import com.android.lockated.preferences.LockatedPreferences;
import com.android.lockated.request.LockatedJSONObjectRequest;
import com.android.lockated.request.LockatedRequestError;
import com.android.lockated.request.LockatedVolleyRequestQueue;
import com.android.lockated.utils.ApplicationURL;
import com.android.lockated.utils.Utilities;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.google.gson.Gson;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;


public class PublishedClassifiedFragment extends Fragment implements Response.ErrorListener, Response.Listener<JSONObject> {
    public static final String REQUEST_TAG = "PublishedClassifiedFragment";
    String pid;
    int societyid;
    ListView SellList;
    TextView errorMsg;
    String ImagePicaso;
    ProgressBar progressBar;
    ArrayList<Classifieds> classifiedArrayList;
    AccountController accountController;
    ArrayList<AccountData> accountDataArrayList;
    PublishedClassifiedAdapter publishedClassifiedAdapter;
    String SHOW, CREATE, INDEX, UPDATE, EDIT;
    private RequestQueue mQueue;
    private LockatedPreferences mLockatedPreferences;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View SellView = inflater.inflate(R.layout.fragment_published_classified, container, false);
        accountController = AccountController.getInstance();
        accountDataArrayList = accountController.getmAccountDataList();
        progressBar = (ProgressBar) SellView.findViewById(R.id.mProgressBarView);
        init(SellView);
        Utilities.ladooIntegration(getActivity(), getActivity().getResources().getString(R.string.publishedClassifieds));
        //   getPublishedComplaints();
        return SellView;
    }

    @Override

    public void onResume() {
        super.onResume();
        LockatedApplication.getInstance().trackScreenView(getString(R.string.publishedClassifieds));
        Utilities.lockatedGoogleAnalytics(getString(R.string.publishedClassifieds),
                getString(R.string.visited), getString(R.string.publishedClassifieds));
    }

    private void init(View sellView) {
        classifiedArrayList = new ArrayList<>();
        mLockatedPreferences = new LockatedPreferences(getActivity());
        SellList = (ListView) sellView.findViewById(R.id.listView);
        errorMsg = (TextView) sellView.findViewById(R.id.noNotices);
        getClasssifiedRole();
        checkPermission();
        publishedClassifiedAdapter = new PublishedClassifiedAdapter(getActivity(), classifiedArrayList);
        SellList.setAdapter(publishedClassifiedAdapter);

    }

    private void checkPermission() {
        if (SHOW != null && SHOW.equals("true")) {
            getPublishedComplaints();
        } else {
            SellList.setVisibility(View.GONE);
            errorMsg.setVisibility(View.VISIBLE);
            errorMsg.setText(R.string.no_permission_error);
        }
    }

    private void getPublishedComplaints() {
        societyid = accountDataArrayList.get(0).getmResidenceDataList().get(0).getSociety_id();
        progressBar.setVisibility(View.VISIBLE);
        if (ConnectionDetector.isConnectedToInternet(getActivity())) {
            mLockatedPreferences = new LockatedPreferences(getActivity());
            String url = ApplicationURL.getAllPublishClassified + societyid + "&token=" + mLockatedPreferences.getLockatedToken();
            mQueue = LockatedVolleyRequestQueue.getInstance(getActivity()).getRequestQueue();
            LockatedJSONObjectRequest lockatedJSONObjectRequest = new LockatedJSONObjectRequest(Request.Method.GET,
                    url, null, this, this);
            lockatedJSONObjectRequest.setTag(REQUEST_TAG);
            mQueue.add(lockatedJSONObjectRequest);
        } else {
            Utilities.showToastMessage(getActivity(), getActivity().getResources().getString(R.string.internet_connection_error));
        }
    }

    @Override
    public void onErrorResponse(VolleyError error) {
        progressBar.setVisibility(View.GONE);
        if (getActivity() != null) {
            LockatedRequestError.onRequestError(getActivity(), error);
        }
    }

    @Override
    public void onResponse(JSONObject response) {
        progressBar.setVisibility(View.GONE);
        if (getActivity() != null) {
            try {
                if (response != null && response.length() > 0) {
                    if (response.has("classifieds") && response.getJSONArray("classifieds").length() > 0) {
                        JSONArray jsonArray = response.getJSONArray("classifieds");
                        Gson gson = new Gson();
                        for (int i = 0; i < jsonArray.length(); i++) {
                            Classifieds events = gson.fromJson(jsonArray.getJSONObject(i).toString(), Classifieds.class);
                            classifiedArrayList.add(events);
                        }
                        publishedClassifiedAdapter.notifyDataSetChanged();
                    } else {
                        MessagesToDispaly();
                    }
                } else {
                    MessagesToDispaly();
                }

            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            MessagesToDispaly();
        }
    }

    private void MessagesToDispaly() {
        SellList.setVisibility(View.GONE);
        errorMsg.setVisibility(View.VISIBLE);
        errorMsg.setText(R.string.no_data_error);
    }

    public String getClasssifiedRole() {

        String mySocietyRoles = getActivity().getResources().getString(R.string.blank_value);
        if (mLockatedPreferences.getRolesJson() != null && !mLockatedPreferences.getRolesJson().equals("blank")) {
            try {
                JSONObject noticeJsonObj = new JSONObject(mLockatedPreferences.getRolesJson());
                for (int i = 0; i < noticeJsonObj.getJSONArray(getActivity().getResources().getString(R.string.permissions_value)).length(); i++) {
                    JSONObject jsonObject1 = noticeJsonObj.getJSONArray(getActivity().getResources().getString(R.string.permissions_value)).getJSONObject(i);
                    if (jsonObject1.has(getActivity().getResources().getString(R.string.section_value))
                            && jsonObject1.getString(getActivity().getResources().getString(R.string.section_value)).equals("spree_classifieds")) {
                        if (jsonObject1.getJSONObject(getActivity().getResources().getString(R.string.permission_value)).has(getActivity().getResources().getString(R.string.index_value))) {
                            mySocietyRoles = jsonObject1.getJSONObject(getActivity().getResources().getString(R.string.permission_value)).getString(getActivity().getResources().getString(R.string.index_value));
                            CREATE = jsonObject1.getJSONObject(getActivity().getResources().getString(R.string.permission_value)).getString("create");
                            INDEX = jsonObject1.getJSONObject(getActivity().getResources().getString(R.string.permission_value)).getString("index");
                            UPDATE = jsonObject1.getJSONObject(getActivity().getResources().getString(R.string.permission_value)).getString("update");
                            EDIT = jsonObject1.getJSONObject(getActivity().getResources().getString(R.string.permission_value)).getString("edit");
                            SHOW = jsonObject1.getJSONObject(getActivity().getResources().getString(R.string.permission_value)).getString("show");
                        }
                    }
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return mySocietyRoles;
    }

    private class PublishedClassifiedAdapter extends BaseAdapter implements View.OnClickListener, Response.ErrorListener, Response.Listener<JSONObject> {
        private final ArrayList<Classifieds> classifieds;
        Context contextMain;
        ProgressDialog mProgressDialog;
        LayoutInflater layoutInflater;

        public PublishedClassifiedAdapter(Context context, ArrayList<Classifieds> classifieds) {
            this.contextMain = context;
            this.classifieds = classifieds;
            layoutInflater = LayoutInflater.from(context);
        }

        @Override
        public int getCount() {
            return classifieds.size();
            //jsonArray.length();
        }

        @Override
        public Object getItem(int position) {
            return position;
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public void onResponse(JSONObject response) {
           mProgressDialog.dismiss();
            if (response.has("id") && response.has("publish")) {
                try {
                    if (response.getString("publish").equals("0")) {
                        Intent intent = new Intent(contextMain, AdminMyZoneActivity.class);
                        intent.putExtra("AdminMyZone", 1);
                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        contextMain.startActivity(intent);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }

        @Override
        public void onErrorResponse(VolleyError error) {
           mProgressDialog.dismiss();
            if (getActivity() != null) {
                LockatedRequestError.onRequestError(getActivity(), error);
            }
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {

            ViewHolder holder = null;
            if (convertView == null) {
                convertView = layoutInflater.inflate(R.layout.admin_publish_sell_classified_child, parent, false);
                holder = new ViewHolder();
                convertView.setTag(holder);
            } else {
                holder = (ViewHolder) convertView.getTag();
            }
            holder.mThingToRent = (TextView) convertView.findViewById(R.id.mThingToRent);
            holder.msellImage = (ImageView) convertView.findViewById(R.id.msellImage);
            holder.mTextTO = (TextView) convertView.findViewById(R.id.mTextTO);
            holder.mTextRate = (TextView) convertView.findViewById(R.id.mTextRate);
            holder.mTextName = (TextView) convertView.findViewById(R.id.mTextName);
            holder.mTextDate = (TextView) convertView.findViewById(R.id.mTextDate);
            holder.mViewDetails = (TextView) convertView.findViewById(R.id.mViewDetails);
            holder.categoryText = (TextView) convertView.findViewById(R.id.categoryValue);
            holder.mDeny = (TextView) convertView.findViewById(R.id.mDeny);
            try {
                pid = String.valueOf(classifieds.get(position).getId());
                holder.mThingToRent.setText(classifieds.get(position).getTitle());
                holder.mTextTO.setText(classifieds.get(position).getPurpose());
                holder.mTextRate.setText(classifieds.get(position).getPrice());
                holder.mTextDate.setText(classifieds.get(position).getExpire());
                holder.categoryText.setText(classifieds.get(position).getClassifiedCategory());
                Picasso.with(contextMain)
                        .load("" + classifieds.get(position).getMedium())
                        .fit()
                        .into(holder.msellImage);

            } catch (Exception e) {
                e.printStackTrace();

            }
            holder.mViewDetails.setTag(position);
            holder.mViewDetails.setOnClickListener(this);
            holder.mDeny.setTag(position);
            holder.mDeny.setOnClickListener(this);
            return convertView;
        }

        @Override
        public void onClick(View v) {
            int position = (int) v.getTag();
            switch (v.getId()) {
                case R.id.mViewDetails:
                    FullDetailVIew(position);
                    break;
                case R.id.mDeny:
                    DiableNotice(position);
                    break;
            }
        }

        private void DiableNotice(int position) {
            mProgressDialog = ProgressDialog.show(getActivity(), "", "Please Wait...", false);
            mProgressDialog.show();
            if (getActivity() != null) {
                if (ConnectionDetector.isConnectedToInternet(getActivity())) {
                    mQueue = LockatedVolleyRequestQueue.getInstance(getActivity()).getRequestQueue();
                    String spid = String.valueOf(classifieds.get(position).getId());
                    String url = ApplicationURL.disableClassified
                            + spid + "&token=" + mLockatedPreferences.getLockatedToken();
                    LockatedJSONObjectRequest lockatedJSONObjectRequest = new LockatedJSONObjectRequest(Request.Method.GET,
                            url, null, this, this);
                    lockatedJSONObjectRequest.setTag(REQUEST_TAG);
                    mQueue.add(lockatedJSONObjectRequest);

                } else {
                    Utilities.showToastMessage(getActivity(), getActivity().getResources().getString(R.string.internet_connection_error));
                }
            }
        }

        private void FullDetailVIew(int position) {
            Intent intent = new Intent(getActivity(), PublishSellClassifiesDetailActivity.class);
            try {
                intent.putExtra("strThing", classifieds.get(position).getTitle());
                intent.putExtra("strThingTO", classifieds.get(position).getPurpose());
                intent.putExtra("strRate", classifieds.get(position).getPrice());
                intent.putExtra("strDescription", classifieds.get(position).getDescription());
                intent.putExtra("strDate", classifieds.get(position).getExpire());
                intent.putExtra("ImagePicaso", classifieds.get(position).getOriginal());
                intent.putExtra("categoryText", classifieds.get(position).getClassifiedCategory());
                intent.putExtra("strStatus", classifieds.get(position).getStatus());
                intent.putExtra("pid", "" + classifieds.get(position).getId());
                if (classifieds.get(position).getUser() != null) {
                    intent.putExtra("name", classifieds.get(position).getUser().getFirstname() + classifieds.get(position).getUser().getLastname());
                    intent.putExtra("mobile", classifieds.get(position).getUser().getMobile());
                    intent.putExtra("email", classifieds.get(position).getUser().getEmail());
                } else {
                    intent.putExtra("name", "Not Available");
                    intent.putExtra("mobile", "Not Available");
                    intent.putExtra("email", "Not Available");
                }
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        private class ViewHolder {
            TextView mThingToRent, mTextTO, mTextRate, mTextName, mTextDate, mDescription;
            TextView mViewDetails, mDeny, categoryText;
            ImageView msellImage;
        }

    }

}
