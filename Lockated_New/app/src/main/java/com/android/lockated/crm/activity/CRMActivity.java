package com.android.lockated.crm.activity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.android.lockated.HomeActivity;
import com.android.lockated.Interfaces.IRecyclerItemClickListener;
import com.android.lockated.LockatedApplication;
import com.android.lockated.R;
import com.android.lockated.cart.ecommerce.EcommerceCartActivity;
import com.android.lockated.crm.adapters.CrmAdapter;
import com.android.lockated.information.AccountController;
import com.android.lockated.landing.HomeFragment;
import com.android.lockated.landing.SupportFragment;
import com.android.lockated.model.AccountData;
import com.android.lockated.model.Banner.Banner;
import com.android.lockated.model.Banner.Society.SocietyBanner_all;
import com.android.lockated.model.CartDetail;
import com.android.lockated.network.ConnectionDetector;
import com.android.lockated.preferences.LockatedPreferences;
import com.android.lockated.request.LockatedJSONObjectRequest;
import com.android.lockated.request.LockatedVolleyRequestQueue;
import com.android.lockated.utils.ApplicationURL;
import com.android.lockated.utils.LockatedConfig;
import com.android.lockated.utils.Utilities;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

public class CRMActivity extends AppCompatActivity implements View.OnClickListener, IRecyclerItemClickListener, Response.ErrorListener, Response.Listener<JSONObject>, ViewPager.OnPageChangeListener {
    private ImageView mImageViewHome;
    private ImageView mImageViewChat;
    private ImageView mImageViewAccount;
    private ImageView mImageViewLocation;
    private ImageView mImageViewOffers;
    private LockatedPreferences mLockatedPreferences;
    private boolean doubleBackToExitPressedOnce;
    LinearLayout linearLayoutContainer;
    private RequestQueue mQueue;
    private RecyclerView mRecyclerView;
    private RecyclerView.LayoutManager mLayoutManager;
    CrmAdapter crmAdapter;
    /*String[] crmItemName = {"Notice Board", "Events", "Polls", "Help Desk", "My Zone", "Bill Payments",
            "About Society", "Society Committee", "Groups", "Gallery"};*/
    String[] crmItemName = {"Notice Board", "Events", "Polls", "Help Desk", "My Zone", "Bill Payment",
            "About Society", "Committee", "Groups", /*"Gallery"*/ "Visitors", "Classifieds",};
    String name = "crm";
    SocietyBanner_all banner;
    Runnable Update;
    Handler handler;
    Timer swipeTimer;
    private ImageView[] dots;
    SocietyBannerAdapter bannerAdapter;
    LinearLayout pager_indicator;
    ViewPager mImageViewCategoryItem;
    ArrayList<SocietyBanner_all> bannerArrayList;
    String GET_BANNER = "HomeFragmentBanner";
    boolean isUserApproved, timerNotCancelled = true;
    RelativeLayout pagerIndicatoRelative, recyclerGrid;
    String tagSocietyId, device_type, gcm_key, device_name, device_os_version;
    int oTypeItemId, dotsCount, NUM_PAGES, currentPage, taxon_id, otype_id;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_crm);
        setToolBar();
        mLockatedPreferences = new LockatedPreferences(this);
        init();
        setBottomTabView(null);
    }

    public void setHomeFragmentData(int oTypeItemId, int taxon_id, int otype_id) {
        this.oTypeItemId = oTypeItemId;
        this.taxon_id = taxon_id;
        this.otype_id = otype_id;
    }

    private void setToolBar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(true);
        getSupportActionBar().setTitle(R.string.mysociety);
    }

    @Override
    protected void onResume() {
        super.onResume();

        LockatedApplication.getInstance().trackScreenView(getString(R.string.mysociety));
        Utilities.lockatedGoogleAnalytics(getString(R.string.mysociety),
                getString(R.string.visited), getString(R.string.mysociety));
        Utilities.ladooIntegration(this, this.getString(R.string.mysociety));
        //isUserApprovedValue();
    }

    public void init() {
        handler = new Handler();
        swipeTimer = new Timer();
        bannerArrayList = new ArrayList<>();
        mImageViewCategoryItem = (ViewPager) findViewById(R.id.mImageViewCategoryItem);
        mImageViewCategoryItem.setOnPageChangeListener(this);
        pager_indicator = (LinearLayout) findViewById(R.id.viewPagerCountDots);
        pagerIndicatoRelative = (RelativeLayout) findViewById(R.id.pagerIndicatoRelative);
        recyclerGrid = (RelativeLayout) findViewById(R.id.recyclerGrid);
        bannerAdapter = new SocietyBannerAdapter(getApplicationContext(), bannerArrayList, true, name);
        mImageViewCategoryItem.setAdapter(bannerAdapter);

        mRecyclerView = (RecyclerView) findViewById(R.id.mRecyclerView);
        mLayoutManager = new GridLayoutManager(this, LockatedConfig.SPAN_COUNT);
        mRecyclerView.setLayoutManager(mLayoutManager);
        crmAdapter = new CrmAdapter(crmItemName, this,getApplicationContext());
        mRecyclerView.setAdapter(crmAdapter);
        getLandingBanner();

       /* if (oTypeItemId != 666) {
            onCategoryViewClicked(oTypeItemId, taxon_id, otype_id);
        }*/
    }

    private void isUserApprovedValue() {
        try {
            if (mLockatedPreferences.getAccountData().getmResidenceDataList().size() > 0) {
                tagSocietyId = String.valueOf(mLockatedPreferences.getAccountData().getmResidenceDataList().get(0).getSociety_id());
                isUserApproved = mLockatedPreferences.getAccountData().is_approve();
                mLockatedPreferences.setIsUserApprovedInSociety(isUserApproved);
                if (!mLockatedPreferences.getSocietyId().equalsIgnoreCase(getApplicationContext().getResources().getString(R.string.blank_value))) {
                    //Log.e("Change", "Society");
                } else if (tagSocietyId == null || tagSocietyId.equals(getApplicationContext().getResources().getString(R.string.null_value))
                        || tagSocietyId.equals(getApplicationContext().getResources().getString(R.string.null_value))) {
                    mLockatedPreferences.setSocietyId(getApplicationContext().getResources().getString(R.string.blank_value));
                } else {
                    mLockatedPreferences.setSocietyId(tagSocietyId);
                }
            } else {
                mLockatedPreferences.setSocietyId(getApplicationContext().getResources().getString(R.string.blank_value));
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void getLandingBanner() {
        if (ConnectionDetector.isConnectedToInternet(getApplicationContext())) {
            String url = ApplicationURL.crmBannerApi + "&token=" + mLockatedPreferences.getLockatedToken();
            mQueue = LockatedVolleyRequestQueue.getInstance(getApplicationContext()).getRequestQueue();
            LockatedJSONObjectRequest lockatedJSONObjectRequest = new LockatedJSONObjectRequest(Request.Method.GET,
                    url, null, this, this);
            lockatedJSONObjectRequest.setTag(GET_BANNER);
            mQueue.add(lockatedJSONObjectRequest);
        } else {
            Utilities.showToastMessage(this, this.getResources().getString(R.string.internet_connection_error));
        }
    }

    public void setBottomTabView(Bundle savedInstanceState) {
        LinearLayout mLinearLayoutHome = (LinearLayout) findViewById(R.id.mLinearLayoutHome);
        LinearLayout mLinearLayoutSupport = (LinearLayout) findViewById(R.id.mLinearLayoutSupport);
        LinearLayout mLinearLayoutAccount = (LinearLayout) findViewById(R.id.mLinearLayoutAccount);
        LinearLayout mLinearLayoutLocation = (LinearLayout) findViewById(R.id.mLinearLayoutLocation);
        LinearLayout mLinearLayoutOffers = (LinearLayout) findViewById(R.id.mLinearLayoutOffers);

        mImageViewHome = (ImageView) findViewById(R.id.mImageViewHome);
        mImageViewChat = (ImageView) findViewById(R.id.mImageViewChat);
        mImageViewAccount = (ImageView) findViewById(R.id.mImageViewAccount);
        mImageViewLocation = (ImageView) findViewById(R.id.mImageViewLocation);
        mImageViewOffers = (ImageView) findViewById(R.id.mImageViewOffers);

        mLinearLayoutHome.setOnClickListener(this);
        mLinearLayoutSupport.setOnClickListener(this);
        mLinearLayoutAccount.setOnClickListener(this);
        mLinearLayoutLocation.setOnClickListener(this);
        mLinearLayoutOffers.setOnClickListener(this);

    }

    private void onHomeClicked() {
        HomeFragment homeFragment = new HomeFragment();
        homeFragment.setHomeFragmentData(666, 666, 666);
        replaceFragment(homeFragment);
        onHomeFragmentClicked();
    }

    public void replaceFragment(Fragment fragment) {
        String backStateName = fragment.getClass().getName();
        String fragmentTag = backStateName;

        FragmentManager manager = getSupportFragmentManager();
        boolean fragmentPopped = manager.popBackStackImmediate(backStateName, 0);

        if (!fragmentPopped && manager.findFragmentByTag(fragmentTag) == null) {
            FragmentTransaction ft = manager.beginTransaction();
            ft.replace(R.id.mLandingContainer, fragment, fragmentTag);
            ft.commit();
        }
    }

    private void onHomeFragmentClicked() {
        mImageViewHome.setImageResource(R.drawable.ic_home_active);
        mImageViewChat.setImageResource(R.drawable.ic_chat);
        mImageViewAccount.setImageResource(R.drawable.ic_account);
        mImageViewLocation.setImageResource(R.drawable.ic_location);
        mImageViewOffers.setImageResource(R.drawable.ic_offers_inactive);
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.mLinearLayoutHome:
                bottomTabClicked(getApplicationContext().getResources().getString(R.string.home_clicked));
                break;
            case R.id.mLinearLayoutSupport:
                bottomTabClicked(getApplicationContext().getResources().getString(R.string.chat_support_clicked));
                break;
            case R.id.mLinearLayoutAccount:
                bottomTabClicked(getApplicationContext().getResources().getString(R.string.account_clicked));
                break;
            case R.id.mLinearLayoutLocation:
                bottomTabClicked(getApplicationContext().getResources().getString(R.string.location_clicked));
                break;
            case R.id.mLinearLayoutOffers:
                bottomTabClicked(getApplicationContext().getResources().getString(R.string.offer_clicked));
                break;
        }
    }

    private void bottomTabClicked(String name) {
        Intent intent = new Intent(this, HomeActivity.class);
        intent.putExtra("HomeActivityCalled", name);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
        finish();
    }

    public String checkAdminIndex() {

        String moduleName = "system";
        String indexVal = "null";
        JSONObject noticeJsonObj;
        try {
            noticeJsonObj = new JSONObject(mLockatedPreferences.getRolesJson());
            if (noticeJsonObj.has("name") && noticeJsonObj.get("name").equals("admin")) {
                for (int i = 0; i < noticeJsonObj.getJSONArray("permissions").length(); i++) {
                    JSONObject sectionJsonObject = noticeJsonObj.getJSONArray("permissions").getJSONObject(i);
                    if (sectionJsonObject.has("section") && sectionJsonObject.getString("section").equals(moduleName)) {
                        indexVal = sectionJsonObject.getJSONObject("permission").getString("administrator");
                    }
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
            indexVal = "null";
        }
        return indexVal;
    }

    public String checkIndex(int itemPosition) {

        String moduleName = null;
        String indexVal = "null";
        JSONObject noticeJsonObj = null;
        if (itemPosition == 0) {
            moduleName = "spree_noticeboards";
        } else if (itemPosition == 1) {
            moduleName = "spree_events";
        } else if (itemPosition == 2) {
            moduleName = "spree_polls";
        } else if (itemPosition == 3) {
            moduleName = "spree_helpdesk";
        } else {
            moduleName = "spree_noticeboards";
        }
        try {
            noticeJsonObj = new JSONObject(mLockatedPreferences.getRolesJson());
            for (int i = 0; i < noticeJsonObj.getJSONArray("permissions").length(); i++) {
                JSONObject sectionJsonObject = noticeJsonObj.getJSONArray("permissions").getJSONObject(i);
                if (sectionJsonObject.has("section") && sectionJsonObject.getString("section").equals(moduleName)) {
                    indexVal = sectionJsonObject.getJSONObject("permission").getString("index");
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
            indexVal = "false";
        }
        return indexVal;
    }

    public void intentPosition(int position) {
        if (position != 9 && position != 10) {
            Intent intent = new Intent(this, MySocietyActivity.class);
            intent.putExtra("crmItemPosition", position);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent);
        } else {
            switch (position) {
                case 9:
                    Intent intent = new Intent(getApplicationContext(), CrmVisitorActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent);
                    finish();
                    break;
                case 10:
                    Intent intent1 = new Intent(getApplicationContext(), MyZoneClassifiedActivity.class);
                    intent1.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent1);
                    finish();
                    break;
            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.home, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_cart:
                onCartClicked();
                return false;
            case android.R.id.home:
                callIntent();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void onCartClicked() {
        ArrayList<AccountData> accountDatas = AccountController.getInstance().getmAccountDataList();
        if (accountDatas.size() > 0) {
            ArrayList<CartDetail> cartDetails = accountDatas.get(0).getCartDetailList();
            if (cartDetails.size() > 0) {
                if (cartDetails.get(0).getmLineItemData().size() > 0) {
                    Intent intent = new Intent(this, EcommerceCartActivity.class);
                    startActivity(intent);
                } else {
                    Utilities.showToastMessage(this, getString(R.string.empty_cart));
                }
            } else {
                Utilities.showToastMessage(this, getString(R.string.empty_cart));
            }
        }
    }

    @Override
    public void onBackPressed() {

        Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.mLandingContainer);
        if (fragment instanceof SupportFragment) {
            if (((SupportFragment) fragment).isSampleMessageSelected) {
                ((SupportFragment) fragment).isSampleMessageSelected = false;
                ((SupportFragment) fragment).mSupportListSample.setVisibility(View.GONE);
                ((SupportFragment) fragment).mImageViewSampleMessage.setImageResource(R.drawable.ic_sample_message_disable);
            } else {
                if (!(fragment instanceof HomeFragment)) {
                    onHomeClicked();
                } else {
                    onApplicationBackPressed();
                }
            }
        } else {
            callIntent();
        }
    }

    private void onApplicationBackPressed() {
        if (doubleBackToExitPressedOnce) {
            super.onBackPressed();
            return;
        }
        this.doubleBackToExitPressedOnce = true;
        Utilities.showToastMessage(this, getString(R.string.back_twice));

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                doubleBackToExitPressedOnce = false;
            }
        }, 2000);
    }

    public void callIntent() {
        boolean nValue = mLockatedPreferences.getNotificationValue();
        if (nValue) {
            mLockatedPreferences.setNotificationValue(true);
        } else {
            mLockatedPreferences.setNotificationValue(false);
        }
        Intent intent = new Intent(this, HomeActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
        finish();

    }

    @Override
    public void onRecyclerItemClick(View view, int position) {
        if (checkAdminIndex().equals("true")) {
            intentPosition(position);
        } else if (checkIndex(position).equals("true")) {
            intentPosition(position);
        } else {
            Toast.makeText(this, this.getResources().getString(R.string.permission_denied), Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public void onErrorResponse(VolleyError error) {

    }

    @Override
    public void onResponse(JSONObject response) {
        JSONObject jsonObject = response;

        if (jsonObject.has("society_banners")) {
            try {
                if (jsonObject.getJSONArray("society_banners").length() > 0) {
                    JSONArray jsonArray = jsonObject.getJSONArray("society_banners");
                    for (int i = 0; i < jsonArray.length(); i++) {
                        banner = new SocietyBanner_all(jsonArray.getJSONObject(i));
                        bannerArrayList.add(banner);
                    }
                    bannerAdapter.notifyDataSetChanged();
                    NUM_PAGES = jsonArray.length() + 1;
                    Update = new Runnable() {
                        public void run() {
                            if (currentPage == NUM_PAGES - 1) {
                                currentPage = 0;
                            }
                            mImageViewCategoryItem.setCurrentItem(currentPage++, true);
                        }
                    };
                    if (timerNotCancelled) {
                        swipeTimer.schedule(new TimerTask() {
                            @Override
                            public void run() {
                                handler.post(Update);
                            }
                        }, /*10000*/NUM_PAGES * 10000, 5000);
                        swipeTimer = new Timer(); //This is new
                        swipeTimer.schedule(new TimerTask() {
                            @Override
                            public void run() {
                                handler.post(Update);
                            }
                        }, 0, NUM_PAGES * 2000); // execute in every 15sec
                        setUiPageViewController(jsonArray.length());
                    }
                } else {
                    pagerIndicatoRelative.setVisibility(View.GONE);
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    private void setUiPageViewController(int count) {
        dotsCount = count;
        dots = new ImageView[dotsCount];
        for (int i = 0; i < dotsCount; i++) {
            dots[i] = new ImageView(this);
            dots[i].setImageDrawable(getResources().getDrawable(R.drawable.circle_128));
            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                    15, LinearLayout.LayoutParams.WRAP_CONTENT
            );
            params.setMargins(1, 0, 1, 0);
            pager_indicator.addView(dots[i], params);
        }
        dots[0].setImageDrawable(getResources().getDrawable(R.drawable.bullet));
    }

    @Override
    public void onPause() {
        super.onPause();
        swipeTimer.cancel();
        timerNotCancelled = false;
        handler.removeCallbacks(Update);
    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }

    @Override
    public void onPageSelected(int position) {
        if (dotsCount != 0) {
            for (int i = 0; i < dotsCount; i++) {
                dots[i].setImageDrawable(getResources().getDrawable(R.drawable.circle_128));
            }
            dots[position].setImageDrawable(getResources().getDrawable(R.drawable.bullet));
        }
    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }
}