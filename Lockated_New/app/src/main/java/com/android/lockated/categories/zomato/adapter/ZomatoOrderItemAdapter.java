package com.android.lockated.categories.zomato.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.lockated.R;
import com.android.lockated.categories.zomato.activity.ZomatoEachOrderActivity;
import com.android.lockated.model.zomato.ThirdpartyTransaction;
import com.library.zomato.ordering.common.OrderSDK;

import java.util.ArrayList;


public class ZomatoOrderItemAdapter extends RecyclerView.Adapter<ZomatoOrderItemAdapter.ZomatoViewHolder> {

    Context context;
    ArrayList<ThirdpartyTransaction> thirdpartyTransactions;

    public ZomatoOrderItemAdapter(Context context, ArrayList<ThirdpartyTransaction> thirdpartyTransactions)
    {
        this.context = context;
        this.thirdpartyTransactions = thirdpartyTransactions;
    }
    @Override
    public ZomatoOrderItemAdapter.ZomatoViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.zomato_order_recycler, parent, false);
        return new ZomatoViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(ZomatoOrderItemAdapter.ZomatoViewHolder holder, final int position) {

        ZomatoEachOrderActivity.thirdpartyTransactions = thirdpartyTransactions;
        //if(thirdpartyTransactions.)
        holder.zomato_restau_name.setText(thirdpartyTransactions.get(position).getTpdetails().getRestaurantName());
        holder.zomato_restau_order_no.setText(thirdpartyTransactions.get(position).getTptransactionid());
        holder.zomato_restau_order_amount.setText(""+thirdpartyTransactions.get(position).getTpdetails().getTotalCost());
        holder.zomato_restau_order_date.setText(thirdpartyTransactions.get(position).getTpdetails().getCreatedTimestampStr());
        holder.mZRecyclerLLayout.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                OrderSDK.getInstance().openOrderDetailsPage(context ,thirdpartyTransactions.get(position).getTptransactionid());
                //Intent intent = new Intent(context,ZomatoEachOrderActivity.class);
                //intent.putExtra("Position",position);
                //context.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return thirdpartyTransactions.size();
    }


    public class ZomatoViewHolder extends RecyclerView.ViewHolder {

        TextView zomato_restau_name,zomato_restau_order_no,zomato_restau_order_amount,zomato_restau_order_date;
        LinearLayout mZRecyclerLLayout;
        public ZomatoViewHolder(View itemView) {
            super(itemView);
            zomato_restau_name = (TextView) itemView.findViewById(R.id.zomato_restau_name);
            zomato_restau_order_no = (TextView) itemView.findViewById(R.id.zomato_restau_order_no);
            zomato_restau_order_amount = (TextView) itemView.findViewById(R.id.zomato_restau_order_amount);
            zomato_restau_order_date = (TextView) itemView.findViewById(R.id.zomato_restau_order_date);
            mZRecyclerLLayout = (LinearLayout) itemView.findViewById(R.id.mZRecyclerLLayout);
        }
    }
}
