
package com.android.lockated.model.OlaCab.OlaAutoCompleteResponse;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class TripInfo {

    @SerializedName("billing_enabled")
    @Expose
    private boolean billingEnabled;
    @SerializedName("amount")
    @Expose
    private int amount;
    @SerializedName("distance")
    @Expose
    private Distance distance;
    @SerializedName("ride_time")
    @Expose
    private RideTime rideTime;
    @SerializedName("discount")
    @Expose
    private int discount;
    @SerializedName("cash_paid")
    @Expose
    private int cashPaid;
    @SerializedName("advance")
    @Expose
    private int advance;
    @SerializedName("convenience_charge")
    @Expose
    private int convenienceCharge;
    @SerializedName("night_charges_applied")
    @Expose
    private boolean nightChargesApplied;
    @SerializedName("night_charge_ratio")
    @Expose
    private String nightChargeRatio;

    /**
     * 
     * @return
     *     The billingEnabled
     */
    public boolean isBillingEnabled() {
        return billingEnabled;
    }

    /**
     * 
     * @param billingEnabled
     *     The billing_enabled
     */
    public void setBillingEnabled(boolean billingEnabled) {
        this.billingEnabled = billingEnabled;
    }

    /**
     * 
     * @return
     *     The amount
     */
    public int getAmount() {
        return amount;
    }

    /**
     * 
     * @param amount
     *     The amount
     */
    public void setAmount(int amount) {
        this.amount = amount;
    }

    /**
     * 
     * @return
     *     The distance
     */
    public Distance getDistance() {
        return distance;
    }

    /**
     * 
     * @param distance
     *     The distance
     */
    public void setDistance(Distance distance) {
        this.distance = distance;
    }

    /**
     * 
     * @return
     *     The rideTime
     */
    public RideTime getRideTime() {
        return rideTime;
    }

    /**
     * 
     * @param rideTime
     *     The ride_time
     */
    public void setRideTime(RideTime rideTime) {
        this.rideTime = rideTime;
    }

    /**
     * 
     * @return
     *     The discount
     */
    public int getDiscount() {
        return discount;
    }

    /**
     * 
     * @param discount
     *     The discount
     */
    public void setDiscount(int discount) {
        this.discount = discount;
    }

    /**
     * 
     * @return
     *     The cashPaid
     */
    public int getCashPaid() {
        return cashPaid;
    }

    /**
     * 
     * @param cashPaid
     *     The cash_paid
     */
    public void setCashPaid(int cashPaid) {
        this.cashPaid = cashPaid;
    }

    /**
     * 
     * @return
     *     The advance
     */
    public int getAdvance() {
        return advance;
    }

    /**
     * 
     * @param advance
     *     The advance
     */
    public void setAdvance(int advance) {
        this.advance = advance;
    }

    /**
     * 
     * @return
     *     The convenienceCharge
     */
    public int getConvenienceCharge() {
        return convenienceCharge;
    }

    /**
     * 
     * @param convenienceCharge
     *     The convenience_charge
     */
    public void setConvenienceCharge(int convenienceCharge) {
        this.convenienceCharge = convenienceCharge;
    }

    /**
     * 
     * @return
     *     The nightChargesApplied
     */
    public boolean isNightChargesApplied() {
        return nightChargesApplied;
    }

    /**
     * 
     * @param nightChargesApplied
     *     The night_charges_applied
     */
    public void setNightChargesApplied(boolean nightChargesApplied) {
        this.nightChargesApplied = nightChargesApplied;
    }

    /**
     * 
     * @return
     *     The nightChargeRatio
     */
    public String getNightChargeRatio() {
        return nightChargeRatio;
    }

    /**
     * 
     * @param nightChargeRatio
     *     The night_charge_ratio
     */
    public void setNightChargeRatio(String nightChargeRatio) {
        this.nightChargeRatio = nightChargeRatio;
    }

}
