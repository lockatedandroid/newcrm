package com.android.lockated.landing;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import com.android.lockated.Interfaces.IGeocoderAddressUpdateListener;
import com.android.lockated.Interfaces.ILocationUpdateListener;
import com.android.lockated.LockatedApplication;
import com.android.lockated.R;
import com.android.lockated.location.GeocoderHandler;
import com.android.lockated.location.LocationAddress;
import com.android.lockated.location.LocationUpdateManager;
import com.android.lockated.location.model.LocationData;
import com.android.lockated.network.ConnectionDetector;
import com.android.lockated.preferences.LockatedPreferences;
import com.android.lockated.request.LockatedVolleyRequestQueue;
import com.android.lockated.utils.ApplicationURL;
import com.android.lockated.utils.Utilities;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.google.android.gms.common.ConnectionResult;

import org.json.JSONException;
import org.json.JSONObject;

public class LocationFragment extends Fragment implements ILocationUpdateListener, IGeocoderAddressUpdateListener, Response.Listener, Response.ErrorListener {
    private View mLocationFragmentView;

    private EditText mEditTextLocation;
    private TextView mTextUserCurrentLocation;

    private boolean hasResetLocation;
    private boolean isLocationAvailable;

    private LockatedPreferences mLockatedPreferences;
    private LocationUpdateManager mLocationUpdateManager;

    private static final int REQUEST_LOCATION = 10;
    public static final String REQUEST_TAG = "LocationFragment";


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mLocationFragmentView = inflater.inflate(R.layout.fragment_location, container, false);
        init();
        return mLocationFragmentView;
    }

    @Override
    public void onResume() {
        super.onResume();
        if (((LockatedApplication) getActivity().getApplicationContext()).isGPSEnabled()) {
            ((LockatedApplication) getActivity().getApplicationContext()).setIsGPSEnabled(false);
            checkLocationPermission();
        }
        LockatedApplication.getInstance().trackScreenView(getString(R.string.location));
        Utilities.lockatedGoogleAnalytics(getString(R.string.location), getString(R.string.visited), getString(R.string.location));
    }

    @Override
    public void onStart() {
        super.onStart();
        checkLocationPermission();
    }

    @Override
    public void onStop() {
        super.onStop();
        removeLocationUpdater();
    }

    private void init() {
        mLockatedPreferences = new LockatedPreferences(getActivity());

        mLocationUpdateManager = new LocationUpdateManager(getActivity(), this);
        mEditTextLocation = (EditText) mLocationFragmentView.findViewById(R.id.mEditTextLocation);
        mTextUserCurrentLocation = (TextView) mLocationFragmentView.findViewById(R.id.mTextUserCurrentLocation);
        mTextUserCurrentLocation.setText("Finding your location");
    }

    private void sendLocationData(double latitude, double longitude) {
        if (ConnectionDetector.isConnectedToInternet(getActivity())) {
            JSONObject jsonObject = new JSONObject();
            try {
                jsonObject.put("latitude", latitude);
                jsonObject.put("longitude", longitude);
            } catch (JSONException e) {
                e.printStackTrace();
            }

            String url = ApplicationURL.editUserDetailUrl + mLockatedPreferences.getLockatedToken();
            LockatedVolleyRequestQueue lockatedVolleyRequestQueue = LockatedVolleyRequestQueue.getInstance(getActivity());
            lockatedVolleyRequestQueue.sendRequest(REQUEST_TAG, Request.Method.POST, url, jsonObject, this, this);

        } else {
            Utilities.showToastMessage(getActivity(), getActivity().getResources().getString(R.string.internet_connection_error));
        }
    }

    private void checkLocationPermission() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION)
                    != PackageManager.PERMISSION_GRANTED) {
                requestPermissions(new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, REQUEST_LOCATION);
            } else {
                initializeLocationUpdater();
            }
        } else {
            initializeLocationUpdater();
        }
    }

    private void initializeLocationUpdater() {
        LocationManager mLocationManager = (LocationManager) getActivity().getSystemService(Context.LOCATION_SERVICE);

        if (mLocationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
            if (mLocationUpdateManager.getGoogleApiClient() == null) {
                if (Utilities.isPlayServiceAvailable(getActivity())) {
                    if (hasResetLocation) {
                        mLocationUpdateManager.buildGoogleApiClient(true);
                    } else {
                        mLocationUpdateManager.buildGoogleApiClient();
                    }
                } else {
                    Utilities.serviceUpdateError(getActivity(), "Google Play Service is not Updated. Would you like to update?");
                }
            }
        } else {
            if (getActivity() != null) {
                Utilities.alertGPSConnectionError(getActivity());
            }
        }
    }

    private void removeLocationUpdater() {
        if (mLocationUpdateManager.getGoogleApiClient() != null) {
            if (mLocationUpdateManager.isLocationUpdating()) {
                mLocationUpdateManager.setIsLocationUpdating(false);
                mLocationUpdateManager.stopLocationUpdates();
            }
            mLocationUpdateManager.getGoogleApiClient().disconnect();
            mLocationUpdateManager.releaseMemory();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        if (requestCode == REQUEST_LOCATION) {
            if (grantResults.length == 1 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                if (mLocationUpdateManager.getGoogleApiClient() == null) {
                    mTextUserCurrentLocation.setText("Getting your location...");
                    initializeLocationUpdater();
                }
            }
        }
    }

    @Override
    public void onSuccessUpdate(Bundle bundle) {
        hasResetLocation = false;
    }

    @Override
    public void onLocationUpdate(Location location) {
        if (getActivity() != null) {
            hasResetLocation = false;
            LocationAddress locationAddress = new LocationAddress();
            double mLatitude = location.getLatitude();
            double mLongitude = location.getLongitude();
            isLocationAvailable = true;
            sendLocationData(mLatitude, mLongitude);
            locationAddress.getAddressFromLocation(mLatitude, mLongitude, getActivity(), new GeocoderHandler(this));
        }
    }

    @Override
    public void onFailedUpdate(ConnectionResult connectionResult) {
        hasResetLocation = false;
        Utilities.showToastMessage(getActivity(), "onFailedUpdate: " + connectionResult);
    }

    @Override
    public void onAddressUpdate(String address) {
        try {
            if (address != null) {
                JSONObject jsonObject = new JSONObject(address);
                LocationData locationData = new LocationData(jsonObject);
                mTextUserCurrentLocation.setText(locationData.address + " " + locationData.getLocality());
                removeLocationUpdater();
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onErrorResponse(VolleyError error) {

    }

    @Override
    public void onResponse(Object response) {

    }
}
