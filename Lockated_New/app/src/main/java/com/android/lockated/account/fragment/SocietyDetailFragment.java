package com.android.lockated.account.fragment;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;

import com.android.lockated.HomeActivity;
import com.android.lockated.LockatedApplication;
import com.android.lockated.OnBoardActivity;
import com.android.lockated.R;
import com.android.lockated.account.activity.AddNewSocietyActivity;
import com.android.lockated.information.AccountController;
import com.android.lockated.model.ResidenceDatas;
import com.android.lockated.model.SocietyData;
import com.android.lockated.network.ConnectionDetector;
import com.android.lockated.preferences.LockatedPreferences;
import com.android.lockated.request.LockatedRequestError;
import com.android.lockated.request.LockatedVolleyRequestQueue;
import com.android.lockated.utils.ApplicationURL;
import com.android.lockated.utils.Utilities;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class SocietyDetailFragment extends Fragment implements AdapterView.OnItemSelectedListener, View.OnClickListener, Response.ErrorListener, Response.Listener<JSONObject> {
    private View mSocietyView;

    private TextView mButtonSocietyDetailSkip;
    private TextView mButtonSocietyDetailSubmit;

    private EditText mEditTextSocietyCity;
    private EditText mEditTextSocietyPinCode;

    private EditText mEditTextHouseAddress1;
    private EditText mEditTextHouseAddress2;

    private AutoCompleteTextView mEditTextSocietyName;

    private EditText mEditTextSocietyBlock;
    private EditText mEditTextLandlineNumber;
    private EditText mEditTextSocietyAddress1;
    private EditText mEditTextSocietyAddress2;
    private EditText mEditTextSocietyIntercom;
    private EditText mEditTextSocietyFlatNumber;

    //----------Edit by bhavesh----------------------

    private LinearLayout mLLOfficeDetails;
    private AutoCompleteTextView mEditTextOfficeName;

    private EditText mEditTextOfficeBlock;
    private EditText mEditTextOfficeAddress1;
    private EditText mEditTextOfficeAddress2;
    private EditText mEditTextOfficeIntercom;
    private EditText mEditTextOfficeFlatNumber;

    //----------------------------------------------

    private LinearLayout mLLHouseDetails;
    private LinearLayout mLLSocietyDetails;


    private RadioGroup radioGroupOwnership;
    private RadioButton mRadioOwner;
    private RadioButton mRadioTenant;

    private Spinner mSpinnerSocietyType;
    private Spinner mSpinnerSocietyState;

    private int societyType;
    private int societyStateId;
    int societyId, officeId;
    private int mBottomBarView;

    private AccountController mAccountController;

    private RequestQueue mQueue;
    private ProgressDialog mProgressDialog;

    private LockatedPreferences mLockatedPreferences;

    private ArrayList<ResidenceDatas> mResidenceDataList;
    private ArrayList<SocietyData> mSocietyDataList;
    private ArrayList<String> mStringSocietyDataList;

    private ArrayAdapter<String> autoCompleteAdapter;
    private LockatedVolleyRequestQueue lockatedVolleyRequestQueue;

    public static final String REQUEST_TAG = "SocietyDetailFragment";
    public static final String REQUEST_TAG_SCREEN = "Screen_detail";
    public static final String SEND_REQUEST_TAG = "SocietyDetailPost";
    String callingClassName;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mSocietyView = inflater.inflate(R.layout.fragment_society_detail, container, false);

        mAccountController = AccountController.getInstance();
        mLockatedPreferences = new LockatedPreferences(getActivity());
        //getResidenceData();
        init();
        Utilities.ladooIntegration(getActivity(), getActivity().getString(R.string.society_details_screen));
        return mSocietyView;
    }

    @Override
    public void onResume() {
        super.onResume();
        // getScreenDetail();
        LockatedApplication.getInstance().trackScreenView(getActivity().getString(R.string.society_details_screen));
    }

    public void setBottomBar(int i, String callingClassName) {
        mBottomBarView = i;
        this.callingClassName = callingClassName;
    }

    private void init() {
        mResidenceDataList = new ArrayList<>();
        mSocietyDataList = new ArrayList<>();
        mStringSocietyDataList = new ArrayList<>();
        ArrayList<ResidenceDatas> datas = mAccountController.getResidenceDatas();
        if (datas.size() > 0) {
            if (datas.size() > 0) {
                mResidenceDataList = datas;
            }
        }

        mLLHouseDetails = (LinearLayout) mSocietyView.findViewById(R.id.mLLHouseDetails);
        mLLSocietyDetails = (LinearLayout) mSocietyView.findViewById(R.id.mLLSocietyDetails);

        mEditTextSocietyCity = (EditText) mSocietyView.findViewById(R.id.mEditTextSocietyCity);
        mEditTextSocietyPinCode = (EditText) mSocietyView.findViewById(R.id.mEditTextSocietyPinCode);

        mEditTextHouseAddress1 = (EditText) mSocietyView.findViewById(R.id.mEditTextHouseAddress1);
        mEditTextHouseAddress2 = (EditText) mSocietyView.findViewById(R.id.mEditTextHouseAddress2);

        mEditTextSocietyName = (AutoCompleteTextView) mSocietyView.findViewById(R.id.mEditTextSocietyName);
        mEditTextSocietyName.setThreshold(2);

        mEditTextSocietyBlock = (EditText) mSocietyView.findViewById(R.id.mEditTextSocietyBlock);
        mEditTextLandlineNumber = (EditText) mSocietyView.findViewById(R.id.mEditTextLandlineNumber);
        mEditTextSocietyAddress1 = (EditText) mSocietyView.findViewById(R.id.mEditTextSocietyAddress1);
        mEditTextSocietyAddress2 = (EditText) mSocietyView.findViewById(R.id.mEditTextSocietyAddress2);
        mEditTextSocietyIntercom = (EditText) mSocietyView.findViewById(R.id.mEditTextSocietyIntercom);
        mEditTextSocietyFlatNumber = (EditText) mSocietyView.findViewById(R.id.mEditTextSocietyFlatNumber);


        //-------------Edit by Bhavesh -------------------------------------

        mLLOfficeDetails = (LinearLayout) mSocietyView.findViewById(R.id.mLLOfficeDetails);

        mEditTextOfficeName = (AutoCompleteTextView) mSocietyView.findViewById(R.id.mEditTextOfficeName);
        mEditTextOfficeName.setThreshold(2);
        mEditTextOfficeBlock = (EditText) mSocietyView.findViewById(R.id.mEditTextOfficeBlock);
        mEditTextOfficeAddress1 = (EditText) mSocietyView.findViewById(R.id.mEditTextOfficeAddress1);
        mEditTextOfficeAddress2 = (EditText) mSocietyView.findViewById(R.id.mEditTextOfficeAddress2);
        mEditTextOfficeIntercom = (EditText) mSocietyView.findViewById(R.id.mEditTextOfficeIntercom);
        mEditTextOfficeFlatNumber = (EditText) mSocietyView.findViewById(R.id.mEditTextOfficeFlatNumber);

        //---------------------------------------------------------------------------

        radioGroupOwnership = (RadioGroup) mSocietyView.findViewById(R.id.radioGroupOwnership);
        mRadioOwner = (RadioButton) mSocietyView.findViewById(R.id.mRadioOwner);
        mRadioTenant = (RadioButton) mSocietyView.findViewById(R.id.mRadioTenant);

        mButtonSocietyDetailSkip = (TextView) mSocietyView.findViewById(R.id.mButtonSocietyDetailSkip);
        mButtonSocietyDetailSubmit = (TextView) mSocietyView.findViewById(R.id.mButtonSocietyDetailSubmit);

        mSpinnerSocietyType = (Spinner) mSocietyView.findViewById(R.id.mSpinnerSocietyType);
        mSpinnerSocietyState = (Spinner) mSocietyView.findViewById(R.id.mSpinnerSocietyState);

        hideHouseDetail();
        hideSocietyDetail();
        hideOfficeDetail();
        disableSubmitButton();
        mLLSocietyDetails.setVisibility(View.GONE);

        if (mBottomBarView == 0) {
            mButtonSocietyDetailSkip.setVisibility(View.VISIBLE);
            if (callingClassName.equals("OnBoardActivity") && getActivity() != null) {
                ((OnBoardActivity) getActivity()).getSupportActionBar().setTitle(getString(R.string.society_details));
            }
        } else {
            mButtonSocietyDetailSkip.setVisibility(View.GONE);
        }

        if (mResidenceDataList.size() > 0) {
            mButtonSocietyDetailSkip.setVisibility(View.VISIBLE);
            disableFields();
            mButtonSocietyDetailSubmit.setVisibility(View.GONE);
            setFieldValues(mResidenceDataList.get(0));
        } else {
            //  disableFields();
            mEditTextSocietyPinCode.setEnabled(true);
            mEditTextSocietyPinCode.requestFocus();
            mButtonSocietyDetailSubmit.setVisibility(View.VISIBLE);
            mSpinnerSocietyType.setSelection(0);
        }

        mSpinnerSocietyType.setOnItemSelectedListener(this);
        mSpinnerSocietyState.setOnItemSelectedListener(this);
        /*mEditTextSocietyName.setOnItemClickListener(this);*/
        mButtonSocietyDetailSkip.setOnClickListener(this);
        mButtonSocietyDetailSubmit.setOnClickListener(this);

        onEditTextListner();

        autoCompleteAdapter = new ArrayAdapter<>(getActivity(), android.R.layout.simple_list_item_1, mStringSocietyDataList);
        mEditTextSocietyName.setAdapter(autoCompleteAdapter);
        mEditTextSocietyName.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                int pos = mStringSocietyDataList.indexOf(mEditTextSocietyName.getText().toString());
                setSocietyDetail(mSocietyDataList.get(pos));
                societyId = mSocietyDataList.get(pos).getId();
            }
        });
        mEditTextOfficeName.setAdapter(autoCompleteAdapter);
        mEditTextOfficeName.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                int pos = mStringSocietyDataList.indexOf(mEditTextOfficeName.getText().toString());
                setOfficeDetail(mSocietyDataList.get(pos));
                officeId = mSocietyDataList.get(pos).getId();
            }
        });
    }


    private void disableSubmitButton() {
        mButtonSocietyDetailSubmit.setEnabled(false);
        mButtonSocietyDetailSubmit.setTextColor(ContextCompat.getColor(getActivity(), R.color.secondary_text));
    }

    private void enableSubmitButton() {
        mButtonSocietyDetailSubmit.setEnabled(true);
        mButtonSocietyDetailSubmit.setTextColor(ContextCompat.getColor(getActivity(), R.color.primary));
    }

    private void onEditTextListner() {

        mEditTextOfficeName.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.length() == 0) {
                    officeId = 0;
                    mEditTextOfficeAddress1.setText("");
                    mEditTextOfficeAddress2.setText("");
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        mEditTextSocietyName.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.length() == 0) {
                    societyId = 0;
                    mEditTextSocietyAddress1.setText("");
                    mEditTextSocietyAddress2.setText("");
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        mEditTextSocietyCity.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.length() > 0) {
                    if (mEditTextSocietyPinCode.getText().toString().length() > 0) {
                        enableSubmitButton();
                    }
                } else {
                    disableSubmitButton();
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        mEditTextSocietyPinCode.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (mResidenceDataList.size() == 0) {
                    if (s.length() > 0) {
                        if (s.length() == 6) {
                            getSocietyDetails(mEditTextSocietyPinCode.getText().toString());
                        }

                        if (mEditTextSocietyCity.getText().toString().length() > 0) {
                            enableSubmitButton();
                        }
                    } else {
                        disableSubmitButton();
                    }

                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }

    private void setSocietyDetail(SocietyData societyData) {
        mEditTextSocietyCity.setText(societyData.getCity());

        mEditTextSocietyName.setText(societyData.getBuilding_name());
        mEditTextSocietyAddress1.setText(societyData.getAddress1());
        mEditTextSocietyAddress2.setText(societyData.getAddress2());
    }

    private void setOfficeDetail(SocietyData societyData) {
        mEditTextSocietyCity.setText(societyData.getCity());

        mEditTextOfficeName.setText(societyData.getBuilding_name());
        mEditTextOfficeAddress1.setText(societyData.getAddress1());
        mEditTextOfficeAddress2.setText(societyData.getAddress2());
    }

    private void setFieldValues(ResidenceDatas mResidenceData) {
        mEditTextSocietyCity.setText(mResidenceData.getCity());
        mEditTextSocietyPinCode.setText(mResidenceData.getPostcode());

        if (mResidenceData.getResidence_type().equalsIgnoreCase("office")) {
            mSpinnerSocietyType.setSelection(3);
            mSpinnerSocietyState.setSelection(mResidenceData.getStateId() - 1);

            mEditTextOfficeName.setText(mResidenceData.getSocietyBuildingName());
            mEditTextOfficeBlock.setText(mResidenceData.getBlock());
            mEditTextLandlineNumber.setText(mResidenceData.getLandline());
            mEditTextOfficeAddress1.setText(mResidenceData.getAddress1());
            mEditTextOfficeAddress2.setText(mResidenceData.getAddress2());
            if (!mResidenceData.getIntercom().equalsIgnoreCase("null")) {
                mEditTextOfficeIntercom.setText(mResidenceData.getIntercom());
            } else {
                mEditTextOfficeIntercom.setText("");
            }
            mEditTextOfficeFlatNumber.setText(mResidenceData.getFlat());
        } else if (mResidenceData.getResidence_type().equalsIgnoreCase("society")) {
            mSpinnerSocietyType.setSelection(1);
            mSpinnerSocietyState.setSelection(mResidenceData.getStateId() - 1);

            mEditTextSocietyName.setText(mResidenceData.getSocietyBuildingName());
            mEditTextSocietyBlock.setText(mResidenceData.getBlock());
            mEditTextLandlineNumber.setText(mResidenceData.getLandline());
            mEditTextSocietyAddress1.setText(mResidenceData.getAddress1());
            mEditTextSocietyAddress2.setText(mResidenceData.getAddress2());
            if (!mResidenceData.getIntercom().equalsIgnoreCase("null")) {
                mEditTextSocietyIntercom.setText(mResidenceData.getIntercom());
            } else {
                mEditTextSocietyIntercom.setText("");
            }
            mEditTextSocietyFlatNumber.setText(mResidenceData.getFlat());
        } else {
            mSpinnerSocietyType.setSelection(2);
            mSpinnerSocietyState.setSelection(mResidenceData.getStateId() - 1);
            mEditTextHouseAddress1.setText(mResidenceData.getAddress1());
            mEditTextHouseAddress2.setText(mResidenceData.getAddress2());
            mEditTextLandlineNumber.setText(mResidenceData.getLandline());
        }

        if (mResidenceData.getOwnership().equalsIgnoreCase("owner")) {
            mRadioOwner.setChecked(true);
        } else {
            mRadioTenant.setChecked(true);
        }
    }

    private void disableFields() {
        mSpinnerSocietyType.setEnabled(false);
        mSpinnerSocietyState.setEnabled(false);

        mEditTextSocietyCity.setEnabled(false);
        mEditTextSocietyPinCode.setEnabled(false);

        mEditTextHouseAddress1.setEnabled(false);
        mEditTextHouseAddress2.setEnabled(false);

        mRadioOwner.setEnabled(false);
        mRadioTenant.setEnabled(false);

        mEditTextSocietyName.setEnabled(false);
        mEditTextSocietyBlock.setEnabled(false);
        mEditTextLandlineNumber.setEnabled(false);
        mEditTextSocietyAddress1.setEnabled(false);
        mEditTextSocietyAddress2.setEnabled(false);
        mEditTextSocietyIntercom.setEnabled(false);
        mEditTextSocietyFlatNumber.setEnabled(false);
    }

    private void enableFields() {
        mSpinnerSocietyType.setEnabled(true);
        mSpinnerSocietyState.setEnabled(true);

        mEditTextSocietyCity.setEnabled(true);
        mEditTextSocietyPinCode.setEnabled(true);

        mEditTextHouseAddress1.setEnabled(true);
        mEditTextHouseAddress2.setEnabled(true);

        mEditTextSocietyName.setEnabled(true);
        mEditTextSocietyBlock.setEnabled(true);
        mEditTextLandlineNumber.setEnabled(true);
        mEditTextSocietyAddress1.setEnabled(true);
        mEditTextSocietyAddress2.setEnabled(true);
        mEditTextSocietyIntercom.setEnabled(true);
        mEditTextSocietyFlatNumber.setEnabled(true);

        mSpinnerSocietyType.setSelection(0);

        mRadioOwner.setEnabled(true);
        mRadioTenant.setEnabled(true);
    }

    private void hideHouseDetail() {
        mLLHouseDetails.setVisibility(View.GONE);
    }

    private void hideSocietyDetail() {
        mLLSocietyDetails.setVisibility(View.GONE);
    }

    private void hideOfficeDetail() {
        mLLOfficeDetails.setVisibility(View.GONE);
    }


    private void showHouseDetail() {
        mLLHouseDetails.setVisibility(View.VISIBLE);
    }

    private void showSocietyDetail() {
        mLLSocietyDetails.setVisibility(View.VISIBLE);
    }

    private void showOfficeDetail() {
        mLLOfficeDetails.setVisibility(View.VISIBLE);
    }


    private void getSocietyDetails(String strPinCode) {
        if (ConnectionDetector.isConnectedToInternet(getActivity())) {
            mProgressDialog = ProgressDialog.show(getActivity(), "", "Please Wait...", false);
            lockatedVolleyRequestQueue = LockatedVolleyRequestQueue.getInstance(getActivity());
            lockatedVolleyRequestQueue.sendRequest(REQUEST_TAG, Request.Method.GET,
                    ApplicationURL.getSocietyUsingZipUrl + strPinCode, null, this, this);
        } else {
            Utilities.showToastMessage(getActivity(), getActivity().getResources().getString(R.string.internet_connection_error));
        }
    }

    public void getResidenceData() {
        if (ConnectionDetector.isConnectedToInternet(getActivity())) {
            String url = ApplicationURL.getResidenceData + mLockatedPreferences.getLockatedToken();
             lockatedVolleyRequestQueue = LockatedVolleyRequestQueue.getInstance(getActivity());
            lockatedVolleyRequestQueue.sendRequest(REQUEST_TAG_SCREEN, Request.Method.GET, url, null, this, this);
        } else {
            Utilities.showToastMessage(getActivity(), getActivity().getResources().getString(R.string.internet_connection_error));
        }
    }

    private void onSocietyDetailSkip() {
        if (getActivity() != null) {
            FamilyDetailFragment familyDetailFragment = new FamilyDetailFragment();
            familyDetailFragment.setFamilyBottomView(0);
            getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.mOnBoardContainer, familyDetailFragment).commit();
        }
    }

    private void onSocietyDetailSubmit() {
        String strSocietyCity = mEditTextSocietyCity.getText().toString();
        String strSocietyPinCode = mEditTextSocietyPinCode.getText().toString();

        String strHouseAddress1 = mEditTextHouseAddress1.getText().toString();
        String strHouseAddress2 = mEditTextHouseAddress2.getText().toString();

        String strSocietyName = mEditTextSocietyName.getText().toString();
        String strSocietyBlock = mEditTextSocietyBlock.getText().toString();
        String strSocietyIntercom = mEditTextSocietyIntercom.getText().toString();
        String strSocietyAddress1 = mEditTextSocietyAddress1.getText().toString();
        String strSocietyAddress2 = mEditTextSocietyAddress2.getText().toString();

        String strLandlineNumber = mEditTextLandlineNumber.getText().toString();
        String strSocietyFlatNumber = mEditTextSocietyFlatNumber.getText().toString();

        //---------------------Edit by Bhavesh-------------------------------

        String strOfficeName = mEditTextOfficeName.getText().toString();
        String strOfficeBlock = mEditTextOfficeBlock.getText().toString();
        String strOfficeIntercom = mEditTextOfficeIntercom.getText().toString();
        String strOfficeAddress1 = mEditTextOfficeAddress1.getText().toString();
        String strOfficeAddress2 = mEditTextOfficeAddress2.getText().toString();
        String strOfficeFlatNumber = mEditTextOfficeFlatNumber.getText().toString();

        //-------------------------------------------------------------------
        String residenceType = "";
        String message = getResources().getString(R.string.signup_blank_field_error);

        String strAddressOne = "";
        String strAddressTwo = "";
        String strName = "";
        String strBlock = "";
        String strInterCom = "";
        String strFlatNumber = "";

        boolean societyDetailCancel = false;
        boolean societyInfoCancel = false;
        View societyDetailFocusView = null;

        if (radioGroupOwnership.getCheckedRadioButtonId() == -1) {
            societyDetailCancel = true;
            societyInfoCancel = true;
            message = getResources().getString(R.string.select_ownership);
        }

        if (TextUtils.isEmpty(strSocietyCity)) {
            societyDetailFocusView = mEditTextSocietyCity;
            societyDetailCancel = true;
            message = getResources().getString(R.string.signup_blank_field_error);
        }

        if (TextUtils.isEmpty(strSocietyPinCode)) {
            societyDetailFocusView = mEditTextSocietyPinCode;
            societyDetailCancel = true;
            message = getResources().getString(R.string.signup_blank_field_error);
        }

        if (societyType == 3) {
            residenceType = "office";
            strAddressOne = strOfficeAddress1;
            strAddressTwo = strOfficeAddress2;
            strName = strOfficeName;
            strFlatNumber = strOfficeFlatNumber;
            strBlock = strOfficeBlock;
            strInterCom = strOfficeIntercom;

            mEditTextHouseAddress1.setText("");
            mEditTextHouseAddress2.setText("");

            //-----------Edit by Bhavesh --------------

            mEditTextSocietyAddress1.setText("");
            mEditTextSocietyAddress2.setText("");
            mEditTextSocietyName.setText("");
            mEditTextSocietyBlock.setText("");
            mEditTextSocietyFlatNumber.setText("");
            mEditTextSocietyIntercom.setText("");

            //---------------------------------------------

            if (TextUtils.isEmpty(strName)) {
                societyDetailFocusView = mEditTextOfficeName;
                societyDetailCancel = true;
                message = getResources().getString(R.string.signup_blank_field_error);
            }

            if (TextUtils.isEmpty(strFlatNumber)) {
                societyDetailFocusView = mEditTextOfficeFlatNumber;
                societyDetailCancel = true;
                message = getResources().getString(R.string.signup_blank_field_error);
            }

            if (TextUtils.isEmpty(strAddressOne)) {
                societyDetailFocusView = mEditTextOfficeAddress1;
                societyDetailCancel = true;
                message = getResources().getString(R.string.signup_blank_field_error);
            }

            if (TextUtils.isEmpty(strAddressTwo)) {
                societyDetailFocusView = mEditTextOfficeAddress2;
                societyDetailCancel = true;
                message = getResources().getString(R.string.signup_blank_field_error);
            }
        } else if (societyType == 2) {
            residenceType = "individual";
            strAddressOne = strHouseAddress1;
            strAddressTwo = strHouseAddress2;

            mEditTextSocietyAddress1.setText("");
            mEditTextSocietyAddress2.setText("");
            mEditTextSocietyName.setText("");
            mEditTextSocietyBlock.setText("");
            mEditTextSocietyFlatNumber.setText("");
            mEditTextSocietyIntercom.setText("");

            //--------Edit by Bhavesh---------------

            mEditTextOfficeAddress1.setText("");
            mEditTextOfficeAddress2.setText("");
            mEditTextOfficeName.setText("");
            mEditTextOfficeBlock.setText("");
            mEditTextOfficeFlatNumber.setText("");
            mEditTextOfficeIntercom.setText("");

            //------------------------------------------

            if (TextUtils.isEmpty(strHouseAddress1)) {
                societyDetailFocusView = mEditTextHouseAddress1;
                societyDetailCancel = true;
                message = getResources().getString(R.string.signup_blank_field_error);
            }

            if (TextUtils.isEmpty(strHouseAddress2)) {
                societyDetailFocusView = mEditTextHouseAddress2;
                societyDetailCancel = true;
                message = getResources().getString(R.string.signup_blank_field_error);
            }
        } else if (societyType == 1) {
            residenceType = "society";
            strAddressOne = strSocietyAddress1;
            strAddressTwo = strSocietyAddress2;
            strName = strSocietyName;
            strFlatNumber = strSocietyFlatNumber;
            strBlock = strSocietyBlock;
            strInterCom = strSocietyIntercom;

            mEditTextHouseAddress1.setText("");
            mEditTextHouseAddress2.setText("");

            mEditTextOfficeAddress1.setText("");
            mEditTextOfficeAddress2.setText("");
            mEditTextOfficeName.setText("");
            mEditTextOfficeBlock.setText("");
            mEditTextOfficeFlatNumber.setText("");
            mEditTextOfficeIntercom.setText("");

            if (TextUtils.isEmpty(strName)) {
                societyDetailFocusView = mEditTextSocietyName;
                societyDetailCancel = true;
                message = getResources().getString(R.string.signup_blank_field_error);
            }

            if (TextUtils.isEmpty(strFlatNumber)) {
                societyDetailFocusView = mEditTextSocietyFlatNumber;
                societyDetailCancel = true;
                message = getResources().getString(R.string.signup_blank_field_error);
            }

            if (TextUtils.isEmpty(strSocietyAddress1)) {
                societyDetailFocusView = mEditTextSocietyAddress1;
                societyDetailCancel = true;
                message = getResources().getString(R.string.signup_blank_field_error);
            }

            if (TextUtils.isEmpty(strSocietyAddress2)) {
                societyDetailFocusView = mEditTextSocietyAddress2;
                societyDetailCancel = true;
                message = getResources().getString(R.string.signup_blank_field_error);
            }
        } else {
            societyDetailCancel = true;
            societyInfoCancel = true;
        }

        if (societyDetailCancel) {
            if (societyInfoCancel) {
                Utilities.showToastMessage(getActivity(), message);
            } else {
                societyDetailFocusView.requestFocus();
                Utilities.showToastMessage(getActivity(), message);
            }
        } else {
            if (ConnectionDetector.isConnectedToInternet(getActivity())) {
                mProgressDialog = ProgressDialog.show(getActivity(), "", "Please Wait...", false);

                JSONObject jsonObject = new JSONObject();
                try {
                    if (societyType == 3) {
                        jsonObject.put("society_id", officeId);
                    } else {

                        jsonObject.put("society_id", societyId);
                    }
                    jsonObject.put("name", strName);
                    jsonObject.put("residence_type", residenceType);
                    jsonObject.put("flat", strFlatNumber);
                    jsonObject.put("block", strBlock);
                    jsonObject.put("address1", strAddressOne);
                    jsonObject.put("address2", strAddressTwo);
                    jsonObject.put("city", strSocietyCity);
                    jsonObject.put("zipcode", strSocietyPinCode);
                    jsonObject.put("state_id", societyStateId);
                    jsonObject.put("country_id", 1);
                    if (mRadioOwner.isChecked()) {
                        jsonObject.put("ownership", "owner");
                    } else {
                        jsonObject.put("ownership", "tenant");
                    }
                    jsonObject.put("intercom", strInterCom);
                    jsonObject.put("landline", strLandlineNumber);
                    lockatedVolleyRequestQueue = LockatedVolleyRequestQueue.getInstance(getActivity());
                    lockatedVolleyRequestQueue.sendRequest(SEND_REQUEST_TAG, Request.Method.POST, ApplicationURL.addSocietyDetailUrl + mLockatedPreferences.getLockatedToken(), jsonObject, this, this);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                Utilities.showToastMessage(getActivity(), getActivity().getResources().getString(R.string.internet_connection_error));
            }
        }
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        if (parent.getId() == R.id.mSpinnerSocietyType) {
            switch (position) {
                case 0:
                    societyType = 0;
                    societyId = 0;
                    hideHouseDetail();
                    hideOfficeDetail();
                    hideSocietyDetail();
                    break;
                case 1:
                    societyType = 1;
                    hideHouseDetail();
                    hideOfficeDetail();
                    showSocietyDetail();
                    break;
                case 2:
                    societyType = 2;
                    societyId = 0;
                    hideOfficeDetail();
                    hideSocietyDetail();
                    showHouseDetail();
                    break;
                case 3:
                    societyType = 3;
                    hideHouseDetail();
                    hideSocietyDetail();
                    showOfficeDetail();
                    break;
            }
        } else if (parent.getId() == R.id.mSpinnerSocietyState) {
            societyStateId = position + 1;
        }
    }


    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.mButtonSocietyDetailSkip:

                Intent intent = new Intent(getActivity(), HomeActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                getActivity().finish();
                // onSocietyDetailSkip();
                // OnFamilyDetailSkipClicked();
                break;
            case R.id.mButtonSocietyDetailSubmit:
                onSocietyDetailSubmit();
                break;
        }
    }

    @Override
    public void onErrorResponse(VolleyError error) {
        mProgressDialog.dismiss();
        if (getActivity() != null) {
            LockatedRequestError.onRequestError(getActivity(), error);
        }
    }

    @Override
    public void onResponse(JSONObject response) {
        Log.e("response", "" + response);
        JSONObject jsonObject = (JSONObject) response;
//        mProgressDialog.dismiss();
        if (getActivity() != null) {
            if (lockatedVolleyRequestQueue.getLockatedJSONObjectRequest().getTag().toString().equals(REQUEST_TAG_SCREEN)) {
                ResidenceDatas residenceDatas = new ResidenceDatas(jsonObject);
                mAccountController.residenceDatases.add(residenceDatas);
            } else if (lockatedVolleyRequestQueue.getLockatedJSONObjectRequest().getTag().toString().equals(REQUEST_TAG)) {
                enableFields();
                try {
                    mSocietyDataList.clear();
                    mStringSocietyDataList.clear();
                    JSONArray jsonArray = jsonObject.getJSONArray("societies");
                    for (int i = 0; i < jsonArray.length(); i++) {
                        JSONObject addressObject = jsonArray.getJSONObject(i);
                        SocietyData societyData = new SocietyData(addressObject);
                        mSocietyDataList.add(societyData);
                        mStringSocietyDataList.add(addressObject.getString("building_name"));
                    }

                    autoCompleteAdapter.notifyDataSetChanged();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else if (lockatedVolleyRequestQueue.getLockatedJSONObjectRequest().getTag().toString().equals(SEND_REQUEST_TAG)) {
                disableFields();
                mButtonSocietyDetailSubmit.setVisibility(View.GONE);
                ResidenceDatas residenceData = new ResidenceDatas(jsonObject);
                mAccountController.residenceDatases.add(residenceData);

                if (callingClassName.equalsIgnoreCase("AddNewSocietyActivity")) {
                    callBackPressed();
                } else if (mBottomBarView == 0) {
                    onSocietyDetailSkip();
                } else {
                }

                /*if (mBottomBarView == 0) {
                    onSocietyDetailSkip();
                }*/
            }
        }
    }

    private void callBackPressed() {
        AddNewSocietyActivity addNewSocietyActivity = (AddNewSocietyActivity) getActivity();
        addNewSocietyActivity.onBackPressed();
    }

    /* @Override
     public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
         int pos = mStringSocietyDataList.indexOf(mEditTextSocietyName.getText().toString());
         setSocietyDetail(mSocietyDataList.get(pos));
         societyId = mSocietyDataList.get(pos).getId();
     }
 */
    private void OnFamilyDetailSkipClicked() {
        if (getActivity() != null) {
            Intent intent = new Intent(getActivity(), HomeActivity.class);
            startActivity(intent);
            getActivity().finish();
        }
    }
}