package com.android.lockated.information;

import com.android.lockated.categories.services.model.ServiceRowData;

import java.util.ArrayList;

public class ServiceController
{
    public static ServiceController mInstance = null;
    public ArrayList<ServiceRowData> mServiceRowDataList;
    private ArrayList<String> mServiceNameList;
    private String mServiceTitle;
    private String mPrescriptionEncodedImage;
    private String mServiceDescription;

    private int categoryId = 0;
    private int subCategoryId = 0;

    public ServiceController()
    {
        mServiceRowDataList = new ArrayList<>();
        mServiceNameList = new ArrayList<>();
    }

    public static ServiceController getInstance()
    {
        if (mInstance == null)
        {
            mInstance = new ServiceController();
        }
        return mInstance;
    }

    public ArrayList<ServiceRowData> getServiceRowDataList()
    {
        return mServiceRowDataList;
    }

    public void resetData()
    {
        setServiceTitle("");
        setServiceDescription("");
        setPrescriptionEncodedImage("");
        mServiceRowDataList.clear();
    }

    public ArrayList<String> getServiceNameList()
    {
        return mServiceNameList;
    }

    public String getServiceTitle()
    {
        return mServiceTitle;
    }

    public void setServiceTitle(String mServiceTitle)
    {
        this.mServiceTitle = mServiceTitle;
    }

    public String getServiceDescription()
    {
        return mServiceDescription;
    }

    public void setServiceDescription(String mServiceDescription)
    {
        this.mServiceDescription = mServiceDescription;
    }

    public String getPrescriptionEncodedImage()
    {
        return mPrescriptionEncodedImage;
    }

    public void setPrescriptionEncodedImage(String mPrescriptionEncodedImage)
    {
        this.mPrescriptionEncodedImage = mPrescriptionEncodedImage;
    }

    public int getCategoryId()
    {
        return categoryId;
    }

    public void setCategoryId(int categoryId)
    {
        this.categoryId = categoryId;
    }

    public int getSubCategoryId()
    {
        return subCategoryId;
    }

    public void setSubCategoryId(int subCategoryId)
    {
        this.subCategoryId = subCategoryId;
    }
}
