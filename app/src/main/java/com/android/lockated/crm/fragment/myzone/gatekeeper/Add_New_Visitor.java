package com.android.lockated.crm.fragment.myzone.gatekeeper;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.DialogFragment;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AlertDialog;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;

import com.android.lockated.R;
import com.android.lockated.crm.activity.CrmVisitorActivity;
import com.android.lockated.information.AccountController;
import com.android.lockated.model.AccountData;
import com.android.lockated.model.PurposeArray.PurposesArray;
import com.android.lockated.model.usermodel.myGroups.UserSociety;
import com.android.lockated.network.ConnectionDetector;
import com.android.lockated.preferences.LockatedPreferences;
import com.android.lockated.request.LockatedJSONObjectRequest;
import com.android.lockated.request.LockatedRequestError;
import com.android.lockated.request.LockatedVolleyRequestQueue;
import com.android.lockated.utils.ApplicationURL;
import com.android.lockated.utils.DatePickerFragment;
import com.android.lockated.utils.MarshMallowPermission;
import com.android.lockated.utils.ShowImage;
import com.android.lockated.utils.TimePickerFragment;
import com.android.lockated.utils.Utilities;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

public class Add_New_Visitor extends DialogFragment implements Response.Listener<JSONObject>, Response.ErrorListener,
        CompoundButton.OnCheckedChangeListener, AdapterView.OnItemSelectedListener, View.OnClickListener,
        ViewPager.OnPageChangeListener, AdapterView.OnItemClickListener {
    public final String REQUEST_TAG = "Add_New_Visitor";
    static final int REQUEST_CAMERA_PHOTO = 3;
    static final int REQUEST_TAKE_PHOTO = 100;
    static final int CHOOSE_IMAGE_REQUEST = 101;
    static final int CHOOSE_IMAGE_REQUEST_G = 104;
    static final int REQUEST_CAMERA = 102;
    static final int REQUEST_STORAGE = 103;
    private final int REQUEST_GALLERY = 4;
    Window window;

    int RESULT_OK = 1;
    JSONArray jsonMultipleImageArray;
    boolean imageSet;
    boolean l_imageSet;
    boolean ischeckedMoveIn;
    String typeOfImage = "vImage";
    MarshMallowPermission marshMallowPermission;
    AccountController accountController;
    ArrayList<AccountData> accountDataArrayList;
    String societyId;
    ProgressDialog mProgressDialog;
    private AutoCompleteTextView mEditFlatNumber;
    private EditText mEditVisitorName;
    private EditText mEditTextMobileNumber;
    private EditText mEditVehicleNumber;
    private EditText mEditNotes;
    private EditText mEditNumberofItemList;
    private EditText mEditNumberofItem;
    private EditText mEditAdditionalMember;
    private EditText mEditMemberName;
    private EditText mEditTextPurpose;

    private TextView mTextFlatNumber;
    private TextView mTextViewName;
    private TextView mTextViewMobileNumber;
    private TextView mVehicleNumber;
    private TextView mTextNotes;
    private TextView mSubmit;
    private TextView mTextExpectedDate;
    private TextView mTextExpectedTime;
    private TextView uploadImage;
    private TextView mTextAdditionalMember;
    private TextView mTextViewMemberName;
    private ImageView mImageDate;
    private ImageView mImageExpectedTime;
    private RadioButton mRadioMoveIn;
    private RadioGroup mRadioGroup;


    private LinearLayout moveInOutLayout;

    private CheckBox checkBoxInOut;
    private CheckBox checkBoxOut;
    private CheckBox mCheckSupervisorApproval;
    private ImageView mImageView;
    private ImageView mLugageImage;

    private Spinner mSpinnerCategory;
    private String strVisitorName;
    private String strFlatNumber;
    private String strContactNumber;
    private String strDate;
    private String strNotes;
    private String strCategory;
    private String StrCheckBoxMoveIn;
    private String strVehicleNumber;
    private String strNumberOfItems;
    private String strNumberOfItemsList;
    private String strAdditionalMamber;
    private String mCurrentPhotoPath, encodedImage = "";
    private String encodedImage2, mCurrentPhotoPath2;
    private RequestQueue mQueue;
    private LockatedPreferences mLockatedPreferences;
    private View mAddNewVisitorView;
    ArrayList<String> purposeArray;
    ArrayList<Object> bitmapArray;
    ArrayList<UserSociety> userSocietyArrayList;
    ArrayList<Integer> userId;
    Integer user_society_id;
    ArrayList<String> mFlatNumberArray;
    ArrayList<String> userNameArrayList;
    ////////////////////////////
    ViewPager mImageViewCategoryItem;
    private LinearLayout pager_indicator;
    private LinearLayout pagerIndicatoRelative;
    private ImageView[] dots;
    String CREATE_TAG = "Members";
    ArrayList<PurposesArray> purposesArrays;
    int dotsCount;
    private String strApprovalNeeded = "0";


    //////////////////////////
    @Override
    public void onStart() {
        super.onStart();
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(getDialog().getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        getDialog().getWindow().setAttributes(lp);
    }

    private void setUiPageViewController(int count) {
        dotsCount = count;
        dots = new ImageView[dotsCount];
        for (int i = 0; i < dotsCount; i++) {
            dots[i] = new ImageView(getActivity());
            dots[i].setImageDrawable(getResources().getDrawable(R.drawable.circle_128));
            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                    15, LinearLayout.LayoutParams.WRAP_CONTENT
            );
            params.setMargins(1, 0, 1, 0);
            pager_indicator.addView(dots[i], params);
        }
        dots[0].setImageDrawable(getResources().getDrawable(R.drawable.bullet));
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        getActivity().overridePendingTransition(R.anim.right_in, R.anim.left_out);
        super.onCreate(savedInstanceState);
        setStyle(STYLE_NO_TITLE, 0);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mAddNewVisitorView = inflater.inflate(R.layout.fragment_add__new__visitor, container);
        marshMallowPermission = new MarshMallowPermission(getActivity());
        init();
        getAllMemberApi();
        getPurposes();
        return mAddNewVisitorView;
    }

    private void getPurposes() {
        String url = ApplicationURL.getVisitorAllPurposes + mLockatedPreferences.getSocietyId()
                + "&token=" + mLockatedPreferences.getLockatedToken();
        requestGetApi(url);
    }

    private void init() {
        mLockatedPreferences = new LockatedPreferences(getActivity());
        purposeArray = new ArrayList<>();
        bitmapArray = new ArrayList<>();
        userSocietyArrayList = new ArrayList<>();
        userNameArrayList = new ArrayList<>();
        userId = new ArrayList<>();
        purposesArrays = new ArrayList<>();
        mFlatNumberArray = new ArrayList<>();
        jsonMultipleImageArray = new JSONArray();
        mImageViewCategoryItem = (ViewPager) mAddNewVisitorView.findViewById(R.id.mImageViewCategoryItem);
        pager_indicator = (LinearLayout) mAddNewVisitorView.findViewById(R.id.viewPagerCountDots);
        pagerIndicatoRelative = (LinearLayout) mAddNewVisitorView.findViewById(R.id.pagerIndicatoRelative);
        mEditFlatNumber = (AutoCompleteTextView) mAddNewVisitorView.findViewById(R.id.mEditFlatNumber);
        mEditTextPurpose = (EditText) mAddNewVisitorView.findViewById(R.id.mEditTextPurpose);
        mEditVisitorName = (EditText) mAddNewVisitorView.findViewById(R.id.mEditVisitorName);
        mEditTextMobileNumber = (EditText) mAddNewVisitorView.findViewById(R.id.mEditTextMobileNumber);
        mEditVehicleNumber = (EditText) mAddNewVisitorView.findViewById(R.id.mEditVehicleNumber);
        mEditNotes = (EditText) mAddNewVisitorView.findViewById(R.id.mEditNotes);
        mEditAdditionalMember = (EditText) mAddNewVisitorView.findViewById(R.id.mEditAdditionalMember);
        mEditNumberofItemList = (EditText) mAddNewVisitorView.findViewById(R.id.mEditNumberofItemList);
        mEditNumberofItem = (EditText) mAddNewVisitorView.findViewById(R.id.mEditNumberofItem);
        mEditMemberName = (EditText) mAddNewVisitorView.findViewById(R.id.mEditMemberName);
        mSpinnerCategory = (Spinner) mAddNewVisitorView.findViewById(R.id.mSpinnerCategory);
        mImageView = (ImageView) mAddNewVisitorView.findViewById(R.id.mImageView);
        mTextViewMemberName = (TextView) mAddNewVisitorView.findViewById(R.id.mTextViewMemberName);
        mTextFlatNumber = (TextView) mAddNewVisitorView.findViewById(R.id.mTextFlatNumber);
        mTextViewName = (TextView) mAddNewVisitorView.findViewById(R.id.mTextViewName);
        mVehicleNumber = (TextView) mAddNewVisitorView.findViewById(R.id.mVehicleNumber);
        mTextNotes = (TextView) mAddNewVisitorView.findViewById(R.id.mTextNotes);
        mTextViewMobileNumber = (TextView) mAddNewVisitorView.findViewById(R.id.mTextViewMobileNumber);
        mSubmit = (TextView) mAddNewVisitorView.findViewById(R.id.mSubmit);
        mTextExpectedDate = (TextView) mAddNewVisitorView.findViewById(R.id.mTextExpectedDate);
        mTextExpectedTime = (TextView) mAddNewVisitorView.findViewById(R.id.mTextExpectedTime);
        mTextAdditionalMember = (TextView) mAddNewVisitorView.findViewById(R.id.mTextAdditionalMember);
        uploadImage = (TextView) mAddNewVisitorView.findViewById(R.id.uploadImage);
        mImageDate = (ImageView) mAddNewVisitorView.findViewById(R.id.mImageDate);
        mImageExpectedTime = (ImageView) mAddNewVisitorView.findViewById(R.id.mImageExpectedTime);
        mLugageImage = (ImageView) mAddNewVisitorView.findViewById(R.id.mLugageImage);
        moveInOutLayout = (LinearLayout) mAddNewVisitorView.findViewById(R.id.moveInOutLayout);
        checkBoxInOut = (CheckBox) mAddNewVisitorView.findViewById(R.id.checkBoxInOut);
        checkBoxOut = (CheckBox) mAddNewVisitorView.findViewById(R.id.checkBoxOut);
        mCheckSupervisorApproval = (CheckBox) mAddNewVisitorView.findViewById(R.id.mCheckSupervisorApproval);
       /* mRadioGroup = (RadioGroup) mAddNewVisitorView.findViewById(R.id.mRadioGroup);
        mRadioMoveIn = (RadioButton) mAddNewVisitorView.findViewById(R.id.mRadioMoveIn);*/
        hideFlatNumber();
        hideName();
        hideVehicleNumber();
        hideNotes();
        hideMobileNumber();
        hideAdditionalMember();
        setEditTextListener();
        hideMoveInOutLayout();
        checkBoxInOut.setOnCheckedChangeListener(this);
        checkBoxOut.setOnCheckedChangeListener(this);
        mCheckSupervisorApproval.setOnCheckedChangeListener(this);
        mSpinnerCategory.setOnItemSelectedListener(this);
        mImageView.setOnClickListener(this);
        mImageView.setOnClickListener(this);
        /*mRadioGroup.setOnCheckedChangeListener(this);*/
        pagerIndicatoRelative.setVisibility(View.GONE);
        mImageViewCategoryItem.setOnPageChangeListener(this);
        uploadImage.setOnClickListener(this);
        mImageDate.setOnClickListener(this);
        mEditFlatNumber.setOnItemClickListener(this);
        mImageExpectedTime.setOnClickListener(this);
        mSubmit.setOnClickListener(this);

        ArrayAdapter<String> dropdown = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item, userNameArrayList);
        dropdown.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
    }

    private void hideAdditionalMember() {
        mTextAdditionalMember.setVisibility(View.INVISIBLE);
    }

    private void showMoveInOutLayout() {
        moveInOutLayout.setVisibility(View.VISIBLE);
    }

    private void hideMoveInOutLayout() {
        moveInOutLayout.setVisibility(View.GONE);
    }

    private void hideMobileNumber() {
        mTextViewMobileNumber.setVisibility(View.INVISIBLE);
    }

    private void setEditTextListener() {
        mEditFlatNumber.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.length() > 0) {
                    showFlatNumber();
                } else {
                    hideFlatNumber();
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        mEditVisitorName.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.length() > 0) {
                    showName();
                } else {
                    hideName();
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        mEditTextMobileNumber.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.length() > 0) {
                    showMobileNumber();
                } else {
                    hideMobileNumber();
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        mEditMemberName.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.length() > 0) {
                    showMemberName();
                } else {
                    hideMemberName();
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        mEditAdditionalMember.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.length() > 0) {
                    showAddMember();
                } else {
                    hideAdditionalMember();
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        mEditVehicleNumber.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.length() > 0) {
                    showVihicleNum();
                } else {
                    hideVehicleNumber();
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        mEditNotes.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.length() > 0) {
                    showNotes();
                } else {
                    hideNotes();
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }

    private void hideMemberName() {
        mTextViewMemberName.setVisibility(View.INVISIBLE);
    }

    private void showMemberName() {
        mTextViewMemberName.setVisibility(View.VISIBLE);
    }

    private void showAddMember() {
        mTextAdditionalMember.setVisibility(View.VISIBLE);
    }

    private void showNotes() {
        mTextNotes.setVisibility(View.VISIBLE);
    }

    private void showVihicleNum() {
        mVehicleNumber.setVisibility(View.VISIBLE);
    }

    private void showName() {
        mTextViewName.setVisibility(View.VISIBLE);
    }

    private void showMobileNumber() {
        mTextViewMobileNumber.setVisibility(View.VISIBLE);
    }

    private void showFlatNumber() {
        mTextFlatNumber.setVisibility(View.VISIBLE);
    }

    private void hideNotes() {
        mTextNotes.setVisibility(View.INVISIBLE);
    }

    private void hideVehicleNumber() {
        mVehicleNumber.setVisibility(View.INVISIBLE);
    }

    private void hideName() {
        mTextViewName.setVisibility(View.INVISIBLE);
    }

    private void hideFlatNumber() {
        mTextFlatNumber.setVisibility(View.INVISIBLE);
    }

    private void getAllMemberApi() {
        String url = ApplicationURL.getAllMemberList + mLockatedPreferences.getSocietyId()
                + "&token=" + mLockatedPreferences.getLockatedToken();
        requestGetApi(url);
    }

    private void requestGetApi(String url) {
        if (ConnectionDetector.isConnectedToInternet(getActivity())) {
            LockatedVolleyRequestQueue lockatedVolleyRequestQueue = LockatedVolleyRequestQueue.getInstance(getActivity());
            lockatedVolleyRequestQueue.sendRequest(CREATE_TAG, Request.Method.GET, url, null, this, this);
        } else {
            Utilities.showToastMessage(getActivity(), getActivity().getResources().getString(R.string.internet_connection_error));
        }
    }

    private void selectImage() {
        final CharSequence[] options2 = {"Camera", "Gallery"};
        final CharSequence[] options = {"Camera", "Gallery"};

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle("Add Photo!");
        if (typeOfImage.equals("vImage")) {
            builder.setItems(options2, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int item) {
                    if (options[item].equals("Camera")) {
                        checkCameraPermission();
                    } else if (options[item].equals("Gallery")) {
                        checkStoragePermission();
                    }
                }

            });
        } else {
            builder.setItems(options, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int item) {
                    if (options[item].equals("Camera")) {
                        checkCameraPermission();
                    } else if (options[item].equals("Gallery")) {
                        checkStoragePermission();
                    }
                }

            });
        }
        builder.show();
    }

    private void selectImageAction() {
        final CharSequence[] options = {"View Image", "Change Image", "Remove Image"};
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setItems(options, new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int item) {
                if (options[item].equals("View Image")) {
                    if (typeOfImage.equals("vImage")) {
                        Intent displayImage = new Intent(getActivity(), ShowImage.class);
                        displayImage.putExtra("imagePathString", mCurrentPhotoPath);
                        getActivity().startActivity(displayImage);
                    } else {
                        Intent displayImage = new Intent(getActivity(), ShowImage.class);
                        displayImage.putExtra("imagePathString", mCurrentPhotoPath2);
                        getActivity().startActivity(displayImage);
                    }

                } else if (options[item].equals("Change Image")) {
                    selectImage();
                } else {
                    if (typeOfImage.equals("vImage")) {
                        mImageView.setImageResource(R.drawable.account_image);
                        encodedImage = "";
                        imageSet = false;
                    } else {
                        mLugageImage.setImageResource(R.drawable.ic_grocery);
                        encodedImage2 = "";
                        l_imageSet = false;
                    }
                }
            }

        });
        builder.show();

    }

    private void checkStoragePermission() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                requestPermissions(new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, REQUEST_STORAGE);
            } else {
                if (typeOfImage.equals("vImage")) {
                    onGalleryClicked();
                } else {
                    onGalleryClicked2();
                }
            }
        } else {
            if (typeOfImage.equals("vImage")) {
                onGalleryClicked();
            } else {
                onGalleryClicked2();
            }
        }
    }

    private void onGalleryClicked() {
        Intent i = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(i, CHOOSE_IMAGE_REQUEST);
    }

    private void onGalleryClicked2() {
        Intent i = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(i, CHOOSE_IMAGE_REQUEST_G);
    }

    private void checkCameraPermission() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
                requestPermissions(new String[]{Manifest.permission.CAMERA}, REQUEST_CAMERA);
            } else {
                onCameraClicked();
            }
        } else {
            onCameraClicked();
        }
    }

    private void onCameraClicked() {
        if (!marshMallowPermission.checkPermissionForCamera()) {
            marshMallowPermission.requestPermissionForCamera();
        } else {
            if (!marshMallowPermission.checkPermissionForExternalStorage()) {
                marshMallowPermission.requestPermissionForExternalStorage();
            } else {
                Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                if (takePictureIntent.resolveActivity(getActivity().getPackageManager()) != null) {
                    File photoFile = null;
                    try {
                        photoFile = createImageFile();
                    } catch (IOException ex) {
                        ex.printStackTrace();
                    }

                    if (photoFile != null) {
                        takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(photoFile));
                        startActivityForResult(takePictureIntent, REQUEST_TAKE_PHOTO);
                    }
                }
            }
        }
    }

    private File createImageFile() throws IOException {
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss", Locale.ENGLISH).format(new Date());
        String imageFileName = "JPEG_" + timeStamp + "_";
        File storageDir = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES);
        File image = File.createTempFile(imageFileName, ".jpg", storageDir);

        if (typeOfImage.equals("vImage")) {
            mCurrentPhotoPath = image.getAbsolutePath();
        } else {
            mCurrentPhotoPath2 = image.getAbsolutePath();
        }
        return image;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode != getActivity().RESULT_CANCELED) {
            if (requestCode == REQUEST_TAKE_PHOTO && resultCode == getActivity().RESULT_OK) {
                setPic();
            } else if (requestCode == CHOOSE_IMAGE_REQUEST && resultCode == getActivity().RESULT_OK) {
                Uri selectedImageURI = data.getData();
                mCurrentPhotoPath = getPath(selectedImageURI);
                setPic();
            } else if (requestCode == CHOOSE_IMAGE_REQUEST_G && resultCode == getActivity().RESULT_OK) {
                Uri selectedImageURI = data.getData();
                mCurrentPhotoPath2 = getPath(selectedImageURI);
                setPic();
            } else if (requestCode == REQUEST_CAMERA_PHOTO && resultCode == getActivity().RESULT_OK) {
                Bitmap thumbnail = (Bitmap) data.getExtras().get("data");
                if (thumbnail != null) {
                    ByteArrayOutputStream bytes = new ByteArrayOutputStream();
                    File destination = new File(Environment.getExternalStorageDirectory(), System.currentTimeMillis() + ".jpg");
                    FileOutputStream fo;
                    try {
                        destination.createNewFile();
                        fo = new FileOutputStream(destination);
                        fo.write(bytes.toByteArray());
                        fo.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    setCameraPic(thumbnail);
                }
            }
        }
    }

    private String getPath(Uri uri) {
        if (uri == null) {
            return null;
        }
        String[] projection = {MediaStore.Images.Media.DATA};
        Cursor cursor = getActivity().getContentResolver().query(uri, projection, null, null, null);
        if (cursor != null) {
            int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
            cursor.moveToFirst();
            return cursor.getString(column_index);
        }
        return uri.getPath();
    }

    private void setCameraPic(Bitmap bitmap) {
        if (bitmap != null) {
            if (typeOfImage.equals("vImage")) {
                encodedImage = Utilities.encodeTobase64(bitmap);
                mImageView.setImageBitmap(bitmap);
                imageSet = true;
            } else {
                encodedImage2 = Utilities.encodeTobase64(bitmap);
                l_imageSet = true;
            }
        }
    }

    private void setPic() {
        if (typeOfImage.equals("vImage")) {
            try {
                File f = new File(mCurrentPhotoPath);
                ExifInterface exif = new ExifInterface(f.getPath());
                int orientation = exif.getAttributeInt(
                        ExifInterface.TAG_ORIENTATION,
                        ExifInterface.ORIENTATION_NORMAL);

                int angle = 0;

                if (orientation == ExifInterface.ORIENTATION_ROTATE_90) {
                    angle = 90;
                } else if (orientation == ExifInterface.ORIENTATION_ROTATE_180) {
                    angle = 180;
                } else if (orientation == ExifInterface.ORIENTATION_ROTATE_270) {
                    angle = 270;
                }

                Matrix mat = new Matrix();
                mat.postRotate(angle);
                BitmapFactory.Options options = new BitmapFactory.Options();
                options.inSampleSize = 2;
                Bitmap bitmap;
                Bitmap bmp = BitmapFactory.decodeStream(new FileInputStream(f),
                        null, options);
                bitmap = Bitmap.createBitmap(bmp, 0, 0, bmp.getWidth(),
                        bmp.getHeight(), mat, true);
                if (bitmap != null) {
                    encodedImage = Utilities.encodeTobase64(bitmap);
                    mImageView.setImageBitmap(bitmap);
                    imageSet = true;
                }

            } catch (IOException | OutOfMemoryError e) {
                e.printStackTrace();
            }

        } else {
            pagerIndicatoRelative.setVisibility(View.VISIBLE);
            try {
                File f = new File(mCurrentPhotoPath2);
                ExifInterface exif = new ExifInterface(f.getPath());
                int orientation = exif.getAttributeInt(
                        ExifInterface.TAG_ORIENTATION,
                        ExifInterface.ORIENTATION_NORMAL);

                int angle = 0;

                if (orientation == ExifInterface.ORIENTATION_ROTATE_90) {
                    angle = 90;
                } else if (orientation == ExifInterface.ORIENTATION_ROTATE_180) {
                    angle = 180;
                } else if (orientation == ExifInterface.ORIENTATION_ROTATE_270) {
                    angle = 270;
                }

                Matrix mat = new Matrix();
                mat.postRotate(angle);
                BitmapFactory.Options options = new BitmapFactory.Options();
                options.inSampleSize = 2;
                Bitmap bitmap;
                Bitmap bmp = BitmapFactory.decodeStream(new FileInputStream(f),
                        null, options);
                bitmap = Bitmap.createBitmap(bmp, 0, 0, bmp.getWidth(),
                        bmp.getHeight(), mat, true);
                if (bitmap != null) {
                    encodedImage2 = Utilities.encodeTobase64(bitmap);
                    l_imageSet = true;
                    jsonMultipleImageArray.put(encodedImage2);


                    bitmapArray.add(bitmap);
                    ImageAdapter imageAdapter = new ImageAdapter(getActivity(), bitmapArray, true);
                    mImageViewCategoryItem.setAdapter(imageAdapter);
                    imageAdapter.notifyDataSetChanged();
                    pager_indicator.removeAllViews();
                    setUiPageViewController(bitmapArray.size());
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    private void onSubmitDetails() {
        strVisitorName = mEditVisitorName.getText().toString();
        strFlatNumber = mEditFlatNumber.getText().toString();
        strContactNumber = mEditTextMobileNumber.getText().toString();
        strNotes = mEditNotes.getText().toString();
        strVehicleNumber = mEditVehicleNumber.getText().toString();
        strNumberOfItems = mEditNumberofItem.getText().toString();
        strAdditionalMamber = mEditAdditionalMember.getText().toString();
        strNumberOfItemsList = mEditNumberofItemList.getText().toString();
        String date = mTextExpectedDate.getText().toString();
        String time = mTextExpectedTime.getText().toString();
        if (mSpinnerCategory.getSelectedItem().equals("Other")) {
            strCategory = mEditTextPurpose.getText().toString();
        }
        String Creatername = mLockatedPreferences.getPersonalName();
        if (TextUtils.isEmpty(strVisitorName)) {
            Utilities.showToastMessage(getActivity(), "Please Enter Visitor Name");
        } else if (TextUtils.isEmpty(strContactNumber) && strContactNumber.length() != 10) {
            Utilities.showToastMessage(getActivity(), "Please Enter Mobile Number");
        } else if (ischeckedMoveIn && TextUtils.isEmpty(strNumberOfItems)) {
            Utilities.showToastMessage(getActivity(), "Please Enter Number Of Items");
        }/* else if (mSpinnerCategory.getSelectedItem().equals("Select Purpose")) {
            Utilities.showToastMessage(getActivity(), "Please Select Visit Purpose");
        }*/ else if (date.equals("Expected Date")) {
            Utilities.showToastMessage(getActivity(), "Please Select Date");
        } else if (time.equals("Expected Time")) {
            Utilities.showToastMessage(getActivity(), "Please Select Time");
        } else if (strContactNumber.length() != 10) {
            Utilities.showToastMessage(getActivity(), "Please Enter valid number");
        } else {
            if (ConnectionDetector.isConnectedToInternet(getActivity())) {
                mProgressDialog = ProgressDialog.show(getActivity(), "", "Please Wait...", false);
                mProgressDialog.show();
                JSONObject jsonObjectMain = new JSONObject();
                JSONObject jsonObject = new JSONObject();
                JSONObject jsonObjectMimo = new JSONObject();

                try {
                    if (ischeckedMoveIn) {
                        jsonObject.put("user_society_id", mLockatedPreferences.getUserSocietyId());
                        jsonObject.put("expected_at", date + "T" + time);
                        jsonObject.put("id_society", mLockatedPreferences.getSocietyId());
                        jsonObject.put("guest_name", strVisitorName);
                        jsonObject.put("guest_number", strContactNumber);
                        jsonObject.put("guest_vehicle_number", strVehicleNumber);
                        jsonObject.put("visit_to", "");
                        jsonObject.put("approve", "1");
                        jsonObject.put("visit_purpose", strCategory);
                        jsonObject.put("plus_person", strAdditionalMamber);
                        jsonObject.put("notes", strNotes);
                        jsonObject.put("image", encodedImage);
                        jsonObjectMimo.put("mimo_type", StrCheckBoxMoveIn);
                        jsonObjectMimo.put("item_number", strNumberOfItems);
                        jsonObjectMimo.put("details", strNumberOfItemsList);
                        jsonObjectMimo.put("documents", jsonMultipleImageArray);
                        jsonObject.put("mimo", jsonObjectMimo);
                        jsonObjectMain.put("gatekeeper", jsonObject);
                    } else {
                        jsonObject.put("user_society_id", mLockatedPreferences.getUserSocietyId());
                        jsonObject.put("expected_at", date + "T" + time);
                        jsonObject.put("guest_name", strVisitorName);
                        jsonObject.put("guest_number", strContactNumber);
                        jsonObject.put("guest_vehicle_number", strVehicleNumber);
                        jsonObject.put("id_society", mLockatedPreferences.getSocietyId());
                        jsonObject.put("approve", "1");
                        jsonObject.put("visit_to", "");
                        jsonObject.put("visit_purpose", strCategory);
                        jsonObject.put("image", encodedImage);
                        jsonObject.put("plus_person", strAdditionalMamber);
                        jsonObject.put("notes", strNotes);
                        jsonObjectMain.put("gatekeeper", jsonObject);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                try {
                    mQueue = LockatedVolleyRequestQueue.getInstance(getActivity()).getRequestQueue();
                    String url = ApplicationURL.addVisitor + mLockatedPreferences.getLockatedToken();
                    LockatedJSONObjectRequest lockatedJSONObjectRequest = new LockatedJSONObjectRequest(Request.Method.POST,
                            url, jsonObjectMain, this, this);
                    lockatedJSONObjectRequest.setTag(REQUEST_TAG);
                    mQueue.add(lockatedJSONObjectRequest);
                } catch (Exception e) {
                    e.printStackTrace();
                }

            } else {
                Utilities.showToastMessage(getActivity(), getActivity().getResources().getString(R.string.internet_connection_error));
            }

        }
    }


    @Override
    public void onResponse(JSONObject response) {
        if (response != null && response.length() > 0) {
            if (response.has("id")) {
                mProgressDialog.dismiss();
                Intent backIntent = new Intent(getActivity(), CrmVisitorActivity.class);
                backIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(backIntent);
                dismiss();
            } else if (response.has("user_societies")) {
                try {
                    if (response.getJSONArray("user_societies").length() > 0) {
                        JSONArray jsonArray = response.getJSONArray("user_societies");
                        Gson gson = new Gson();
                        for (int i = 0; i < jsonArray.length(); i++) {
                            UserSociety userSociety = gson.fromJson(jsonArray.getJSONObject(i).toString(), UserSociety.class);
                            userSocietyArrayList.add(userSociety);
                        }
                        for (int i = 0; i < userSocietyArrayList.size(); i++) {
                            int id = userSocietyArrayList.get(i).getId();
                            String name = userSocietyArrayList.get(i).getUser().getFirstname() + " " + userSocietyArrayList.get(i).getUser().getLastname();
                            String flat = userSocietyArrayList.get(i).getUserFlat().getFlat();

                            userId.add(id);
                            userNameArrayList.add(flat + " - " + name);
                        }
                        userId.add(0, 0);
                        userNameArrayList.add(0, "Select Name");
                        mFlatNumberArray.add(0, "Select Flat");

                        ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(),
                                android.R.layout.simple_spinner_dropdown_item, userNameArrayList);
                        mEditFlatNumber.setAdapter(adapter);


                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else if (response.has("soc_visit_purposes")) {
                try {
                    if (response.getJSONArray("soc_visit_purposes").length() > 0) {
                        JSONArray jsonArrayPurposes = response.getJSONArray("soc_visit_purposes");
                        ArrayAdapter<String> stringArrayadapter = new ArrayAdapter<String>(getContext(), android.R.layout.simple_spinner_item, purposeArray);
                        try {
                            int lastItem = jsonArrayPurposes.length() + 1;
                            for (int i = 0; i < jsonArrayPurposes.length(); i++) {
                                purposeArray.add(jsonArrayPurposes.getString(i));
                            }
                            purposeArray.add(0, "Select Purpose");
                            purposeArray.add(lastItem, "Other");
                            stringArrayadapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                            mSpinnerCategory.setAdapter(stringArrayadapter);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    @Override
    public void onErrorResponse(VolleyError error) {
        mProgressDialog.dismiss();
        if (getActivity() != null) {
            LockatedRequestError.onRequestError(getActivity(), error);
        }
    }

    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        if (buttonView == checkBoxInOut) {
            if (isChecked) {
                checkBoxOut.setChecked(false);
                showMoveInOutLayout();
                StrCheckBoxMoveIn = "move_in";
                ischeckedMoveIn = true;
            } else {
                hideMoveInOutLayout();
                StrCheckBoxMoveIn = "";
                ischeckedMoveIn = false;
            }
        } else if (buttonView == mCheckSupervisorApproval) {
            if (isChecked) {
                strApprovalNeeded = "1";
            } else {
                strApprovalNeeded = "0";
            }
        } else if (buttonView == checkBoxOut) {
            if (isChecked) {
                checkBoxInOut.setChecked(false);
                showMoveInOutLayout();
                StrCheckBoxMoveIn = "move_out";
                ischeckedMoveIn = true;
            } else {
                hideMoveInOutLayout();
                StrCheckBoxMoveIn = "";
                ischeckedMoveIn = false;
            }
        }

    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        if (parent == mSpinnerCategory) {
            if (position != 0) {
                strCategory = parent.getSelectedItem().toString();
            } else {
                strCategory = "";
            }
            if (parent.getSelectedItem().equals("Other")) {
                mEditTextPurpose.setVisibility(View.VISIBLE);
            } else {
                mEditTextPurpose.setVisibility(View.GONE);
            }

        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.mImageView:
                typeOfImage = "vImage";
                if (!imageSet) {
                    selectImage();
                } else {
                    selectImageAction();
                }
                break;
            case R.id.uploadImage:
                typeOfImage = "lImage";
                selectImage();
                break;
            case R.id.mSubmit:
                onSubmitDetails();
                break;
            case R.id.mImageDate:
                datePicker(mTextExpectedDate);
                break;
            case R.id.mImageExpectedTime:
                timePicker(mTextExpectedTime);
                break;
        }
    }

    private void timePicker(TextView selectTime) {
        TimePickerFragment timePickerFragment = new TimePickerFragment();
        timePickerFragment.setTimePickerView(selectTime);
        DialogFragment newFragment = timePickerFragment;
        newFragment.show(getActivity().getSupportFragmentManager(), "TimePicker");
    }

    private void datePicker(TextView selectDate) {
        DatePickerFragment datePickerFragment = new DatePickerFragment();
        datePickerFragment.setDatePickerView(selectDate);
        DialogFragment newFragment = datePickerFragment;
        newFragment.show(getActivity().getSupportFragmentManager(), "DatePicker");
    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }

    @Override
    public void onPageSelected(int position) {
        if (getActivity() != null && dotsCount != 0) {
            for (int i = 0; i < dotsCount; i++) {
                dots[i].setImageDrawable(getResources().getDrawable(R.drawable.circle_128));
            }
            dots[position].setImageDrawable(getResources().getDrawable(R.drawable.bullet));
        }
    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        String selection = (String) parent.getItemAtPosition(position);
        int pos = -1;
        for (int i = 0; i < userNameArrayList.size(); i++) {
            String item = userNameArrayList.get(i);
            if (item.equals(selection)) {
                pos = i;
                break;
            }
        }
        user_society_id = userId.get(pos);

    }


    private class ImageAdapter extends PagerAdapter {
        private final boolean clickable;
        Context context;
        LayoutInflater mLayoutInflater;
        ArrayList<Object> bitmapAarray;

        public ImageAdapter(Context context, ArrayList<Object> bitmapAarray, boolean clickable) {
            this.context = context;
            this.clickable = clickable;
            this.bitmapAarray = bitmapAarray;
            mLayoutInflater = LayoutInflater.from(context);

        }

        @Override
        public int getCount() {
            return bitmapAarray.size();
        }

        @Override
        public boolean isViewFromObject(View view, Object object) {
            return view == object;
        }

        @Override
        public Object instantiateItem(ViewGroup container, final int position) {
            View itemView = mLayoutInflater.inflate(R.layout.multiple_image, container, false);
            ImageView imageView = (ImageView) itemView.findViewById(R.id.mImageEdit);
            imageView.setImageBitmap((Bitmap) bitmapAarray.get(position));
            container.addView(itemView);
            if (clickable) {
                imageView.setTag(position);
                imageView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        int position = (int) v.getTag();
                        adapterImageAction(position);
                    }
                });
            }
            return itemView;
        }

        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            container.removeView((LinearLayout) object);
        }

    }

    private void adapterImageAction(int posss) {
        final int poss = posss;
        final CharSequence[] options = {/*"Zoom Image",*/ "Remove"};
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle("select Action!");
        builder.setItems(options, new DialogInterface.OnClickListener() {
            @TargetApi(Build.VERSION_CODES.KITKAT)
            @Override
            public void onClick(DialogInterface dialog, int item) {
                if (options[item].equals("Zoom Image")) {
                    Intent displayImage = new Intent(getActivity(), ShowImage.class);
                    displayImage.putExtra("bitmap", "" + bitmapArray.get(poss));
                    getActivity().startActivity(displayImage);
                } else if (options[item].equals("Remove")) {
                    bitmapArray.remove(poss);
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                        jsonMultipleImageArray.remove(poss);
                    } else {
                        jsonMultipleImageArray.remove(poss);
                    }
                    Log.e("", "" + bitmapArray.size());
                    if (bitmapArray.size() != 0) {
                        ImageAdapter imageAdapter = new ImageAdapter(getActivity(), bitmapArray, true);
                        mImageViewCategoryItem.setAdapter(imageAdapter);
                        imageAdapter.notifyDataSetChanged();
                        pager_indicator.removeAllViews();
                        setUiPageViewController(bitmapArray.size());
                    } else {
                        pagerIndicatoRelative.setVisibility(View.GONE);
                    }
                }
            }

        });
        builder.show();
    }
}