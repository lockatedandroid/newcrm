package com.android.lockated.categories.medicine.fragment;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.lockated.LockatedApplication;
import com.android.lockated.R;
import com.android.lockated.categories.medicine.MedicineActivity;
import com.android.lockated.information.ServiceController;
import com.android.lockated.utils.MarshMallowPermission;
import com.android.lockated.utils.Utilities;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class MedicineFragment extends Fragment implements View.OnClickListener {
    private LinearLayout mLinearLayoutBottomRow;
    private LinearLayout mLinearLayoutCenterRow;

    private TextView mTextViewMedicineTitle;
    private TextView mTextViewMedicineCamera;
    private TextView mTextViewMedicineGallery;
    private TextView mTextViewMedicineCancel;
    private TextView mTextViewMedicineContinue;

    private ImageView mImageViewMedicineContainer;

    private String mCurrentPhotoPath;

    private ServiceController mServiceController;

    static final int REQUEST_TAKE_PHOTO = 100;
    static final int REQUEST_CAMERA = 102;
    static final int REQUEST_STORAGE = 103;
    static final int CHOOSE_IMAGE_REQUEST = 101;

    static final int REQUEST_CAMERA_PHOTO = 3;

    MarshMallowPermission marshMallowPermission;
    String screenName = "Medicine Screen";

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View medicineView = inflater.inflate(R.layout.fragment_medicine, container, false);
        marshMallowPermission = new MarshMallowPermission(getActivity());
        init(medicineView);
        Utilities.ladooIntegration(getActivity(), screenName);
        Utilities.lockatedGoogleAnalytics(screenName, getString(R.string.visited), screenName);
        return medicineView;
    }

    private void init(View medicineView) {
        mServiceController = ServiceController.getInstance();

        mServiceController.resetData();

        mServiceController.setCategoryId(((LockatedApplication) getActivity().getApplicationContext()).getCategoryIdTwo());
        mServiceController.setSubCategoryId(((LockatedApplication) getActivity().getApplicationContext()).getCategoryId());

        mTextViewMedicineTitle = (TextView) medicineView.findViewById(R.id.mTextViewMedicineTitle);
        mTextViewMedicineCamera = (TextView) medicineView.findViewById(R.id.mTextViewMedicineCamera);
        mTextViewMedicineGallery = (TextView) medicineView.findViewById(R.id.mTextViewMedicineGallery);
        mTextViewMedicineCancel = (TextView) medicineView.findViewById(R.id.mTextViewMedicineCancel);
        mTextViewMedicineContinue = (TextView) medicineView.findViewById(R.id.mTextViewMedicineContinue);

        mImageViewMedicineContainer = (ImageView) medicineView.findViewById(R.id.mImageViewMedicineContainer);
        FrameLayout.LayoutParams layoutParams = new FrameLayout.LayoutParams(Utilities.getScreenWidth(), Utilities.getScreenHeight() / 2);
        layoutParams.gravity = Gravity.CENTER;
        mImageViewMedicineContainer.setLayoutParams(layoutParams);

        mLinearLayoutBottomRow = (LinearLayout) medicineView.findViewById(R.id.mLinearLayoutBottomRow);
        mLinearLayoutCenterRow = (LinearLayout) medicineView.findViewById(R.id.mLinearLayoutCenterRow);

        ((LockatedApplication) getActivity().getApplicationContext()).setMyAddressData(null);
        ((LockatedApplication) getActivity().getApplicationContext()).setmDeliveryTime("");
        ((LockatedApplication) getActivity().getApplicationContext()).setmDeliveryDate("");

        mTextViewMedicineCancel.setOnClickListener(this);
        mTextViewMedicineCamera.setOnClickListener(this);
        mTextViewMedicineGallery.setOnClickListener(this);
        mTextViewMedicineContinue.setOnClickListener(this);

        showCenterRow();
    }

    private void showCenterRow() {
        mServiceController.resetData();
        mTextViewMedicineTitle.setVisibility(View.GONE);
        mImageViewMedicineContainer.setImageDrawable(null);
        mLinearLayoutBottomRow.setVisibility(View.GONE);
        mLinearLayoutCenterRow.setVisibility(View.VISIBLE);
    }

    private void hideCenterRow() {
        mTextViewMedicineTitle.setVisibility(View.GONE);
        mLinearLayoutBottomRow.setVisibility(View.VISIBLE);
        mLinearLayoutCenterRow.setVisibility(View.GONE);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.mTextViewMedicineCancel:
                onCancelClicked();
                break;

            case R.id.mTextViewMedicineCamera:
                checkCameraPermission();
                //getPhotoFromCamera();
                break;

            case R.id.mTextViewMedicineGallery:
                checkStoragePermission();
                break;

            case R.id.mTextViewMedicineContinue:
                onContinueClicked();
                break;

        }
    }

    private void onContinueClicked() {
        if (!mServiceController.getPrescriptionEncodedImage().isEmpty()) {
            ((MedicineActivity) getActivity()).addNewFragmentToStack();
        }
    }

    private void checkCameraPermission() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
                requestPermissions(new String[]{Manifest.permission.CAMERA}, REQUEST_CAMERA);
            } else {
                onCameraClicked();
            }
        } else {
            onCameraClicked();
        }
    }

    private void checkStoragePermission() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                requestPermissions(new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, REQUEST_STORAGE);
            } else {
                onGalleryClicked();
            }
        } else {
            onGalleryClicked();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        if (requestCode == REQUEST_CAMERA) {
            if (grantResults.length == 1 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                onCameraClicked();
            }
        } else if (requestCode == REQUEST_STORAGE) {
            if (grantResults.length == 1 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                onGalleryClicked();
            }
        }
    }

    private void onGalleryClicked() {
        Intent i = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(i, CHOOSE_IMAGE_REQUEST);
    }

    private File createImageFile() throws IOException {
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = "JPEG_" + timeStamp + "_";
        File storageDir = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES);
        File image = File.createTempFile(imageFileName, ".jpg", storageDir);
        mCurrentPhotoPath = image.getAbsolutePath();
        return image;
    }

    private void onCameraClicked() {
        if (!marshMallowPermission.checkPermissionForCamera()) {
            marshMallowPermission.requestPermissionForCamera();
        } else {
            if (!marshMallowPermission.checkPermissionForExternalStorage()) {
                marshMallowPermission.requestPermissionForExternalStorage();
            } else {
                Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                if (takePictureIntent.resolveActivity(getActivity().getPackageManager()) != null) {
                    File photoFile = null;
                    try {
                        photoFile = createImageFile();
                    } catch (IOException ex) {
                        ex.printStackTrace();
                    }

                    if (photoFile != null) {
                        takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(photoFile));
                        startActivityForResult(takePictureIntent, REQUEST_TAKE_PHOTO);
                    }
                }
            }
        }
    }

    public void getPhotoFromCamera() {

        if (!marshMallowPermission.checkPermissionForCamera()) {
            marshMallowPermission.requestPermissionForCamera();
        } else {
            if (!marshMallowPermission.checkPermissionForExternalStorage()) {
                marshMallowPermission.requestPermissionForExternalStorage();
            } else {
                Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                startActivityForResult(cameraIntent, REQUEST_CAMERA_PHOTO);
            }
        }
    }

    private void onCancelClicked() {
        showCenterRow();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode != getActivity().RESULT_CANCELED) {
            /*Log.e("Intent data", "" + data.getData());*/
            if (requestCode == REQUEST_TAKE_PHOTO && resultCode == getActivity().RESULT_OK) {
                setPic();
            } else if (requestCode == CHOOSE_IMAGE_REQUEST && resultCode == getActivity().RESULT_OK) {
                Uri selectedImageURI = data.getData();
                mCurrentPhotoPath = getPath(selectedImageURI);
                setPic();
            } else if (requestCode == REQUEST_CAMERA_PHOTO && resultCode == getActivity().RESULT_OK) {
                Bitmap thumbnail = (Bitmap) data.getExtras().get("data");
                if (thumbnail != null) {
                    ByteArrayOutputStream bytes = new ByteArrayOutputStream();
                    //thumbnail.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
                    File destination = new File(Environment.getExternalStorageDirectory(), System.currentTimeMillis() + ".jpg");
                    FileOutputStream fo;
                    try {
                        destination.createNewFile();
                        fo = new FileOutputStream(destination);
                        fo.write(bytes.toByteArray());
                        fo.close();
                    } catch (FileNotFoundException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    setCameraPic(thumbnail);
                }
            }
        }
    }

    private void setCameraPic(Bitmap bitmap) {
        // Get the dimensions of the View
        /*int targetW = mImageViewMedicineContainer.getWidth();
        int targetH = mImageViewMedicineContainer.getHeight();

        // Get the dimensions of the bitmap
        BitmapFactory.Options bmOptions = new BitmapFactory.Options();
        bmOptions.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(mCurrentPhotoPath, bmOptions);
        int photoW = bmOptions.outWidth;
        int photoH = bmOptions.outHeight;

        int scaleFactor = Math.min(photoW / targetW, photoH / targetH);

        bmOptions.inJustDecodeBounds = false;
        bmOptions.inSampleSize = scaleFactor;
        bmOptions.inPurgeable = true;*/

        hideCenterRow();

        //Bitmap bitmap = BitmapFactory.decodeFile(mCurrentPhotoPath, bmOptions);
        if (bitmap != null) {
            String encodedImage = Utilities.encodeTobase64(bitmap);
            mServiceController.setPrescriptionEncodedImage(encodedImage);
            mImageViewMedicineContainer.setImageBitmap(bitmap);
        }
    }

    private void setPic() {
        // Get the dimensions of the View
        int targetW = mImageViewMedicineContainer.getWidth();
        int targetH = mImageViewMedicineContainer.getHeight();

        // Get the dimensions of the bitmap
        BitmapFactory.Options bmOptions = new BitmapFactory.Options();
        bmOptions.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(mCurrentPhotoPath, bmOptions);
        int photoW = bmOptions.outWidth;
        int photoH = bmOptions.outHeight;

        int scaleFactor = Math.min(photoW / targetW, photoH / targetH);

        bmOptions.inJustDecodeBounds = false;
        bmOptions.inSampleSize = scaleFactor;
        bmOptions.inPurgeable = true;

        hideCenterRow();

        Bitmap bitmap = BitmapFactory.decodeFile(mCurrentPhotoPath, bmOptions);
        if (bitmap != null) {
            String encodedImage = Utilities.encodeTobase64(bitmap);
            mServiceController.setPrescriptionEncodedImage(encodedImage);
            mImageViewMedicineContainer.setImageBitmap(bitmap);
        }
    }

    public String getPath(Uri uri) {
        if (uri == null) {
            return null;
        }
        String[] projection = {MediaStore.Images.Media.DATA};
        Cursor cursor = getActivity().getContentResolver().query(uri, projection, null, null, null);
        if (cursor != null) {
            int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
            cursor.moveToFirst();
            return cursor.getString(column_index);
        }
        return uri.getPath();
    }
}