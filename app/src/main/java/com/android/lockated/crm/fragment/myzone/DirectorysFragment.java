package com.android.lockated.crm.fragment.myzone;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import com.android.lockated.R;
import com.android.lockated.crm.activity.MyZoneDirectoryActivity;
import com.android.lockated.crm.adapters.CommanListViewAdapter;


public class DirectorysFragment extends Fragment implements AdapterView.OnItemClickListener {
    ListView DirectoryfiedList;
    String[] nameList = {/*"Personal Directory",*/ "Society Directory", "Members Directory"};
    CommanListViewAdapter commanListViewAdapter;
    private View DirectorysFragmentView;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        DirectorysFragmentView = inflater.inflate(R.layout.fragment_directorys, container, false);
        DirectoryfiedList = (ListView) DirectorysFragmentView.findViewById(R.id.ClassifiedList);
        commanListViewAdapter = new CommanListViewAdapter(getActivity(), nameList);
        DirectoryfiedList.setAdapter(commanListViewAdapter);
        DirectoryfiedList.setOnItemClickListener(this);
        return DirectorysFragmentView;
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

        Intent intent = new Intent(getContext(), MyZoneDirectoryActivity.class);
        intent.putExtra("Listposition", position);
        startActivity(intent);

    }

}
