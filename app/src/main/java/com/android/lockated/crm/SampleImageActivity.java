package com.android.lockated.crm;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.android.lockated.R;
import com.android.lockated.account.TouchImageView;
import com.squareup.picasso.Picasso;

public class SampleImageActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sample_image);
        TouchImageView img = new TouchImageView(this);
        String strImage = getIntent().getExtras().getString("mImageZoom");
        Picasso.with(this).load("" + strImage).fit().into(img);
        img.setMaxZoom(4f);
        setContentView(img);

    }
}
