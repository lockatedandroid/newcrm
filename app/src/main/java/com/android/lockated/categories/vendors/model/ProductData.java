package com.android.lockated.categories.vendors.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.android.lockated.utils.ApplicationURL;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class ProductData implements Parcelable {
    private int id;
    private String name;
    private String price;
    private String cost_price;
    private String imageUrl;
    private int quantity = 0;
    private int total_on_hand;

    public ProductData(JSONObject jsonObject) {
        try {
            id = jsonObject.optInt("id");
            name = jsonObject.optString("name");
            price = jsonObject.optString("price");
            cost_price = jsonObject.optString("cost_price");
            total_on_hand = jsonObject.optInt("total_on_hand");

            JSONArray imagesJsonArray = jsonObject.getJSONArray("images");
            for (int i = 0; i < imagesJsonArray.length(); i++) {
                JSONObject imageObject = imagesJsonArray.getJSONObject(i);
                imageUrl = ApplicationURL.ImageBaseURL + imageObject.optString("small_url");
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getPrice() {
        return price;
    }

    public String getCostPrice() {
        return cost_price;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public int getTotalItems() {
        return total_on_hand;
    }

    @Override
    public int describeContents() {
        return this.hashCode();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(id);
        dest.writeInt(quantity);
        dest.writeInt(total_on_hand);
        dest.writeString(name);
        dest.writeString(price);
        dest.writeString(cost_price);
        dest.writeString(imageUrl);
    }

    private ProductData(Parcel in) {
        id = in.readInt();
        quantity = in.readInt();
        total_on_hand = in.readInt();
        name = in.readString();
        price = in.readString();
        cost_price = in.readString();
        imageUrl = in.readString();
    }

    public static final Parcelable.Creator<ProductData> CREATOR = new Parcelable.Creator<ProductData>() {
        public ProductData createFromParcel(Parcel in) {
            return new ProductData(in);
        }

        public ProductData[] newArray(int size) {
            return new ProductData[size];
        }
    };
}
