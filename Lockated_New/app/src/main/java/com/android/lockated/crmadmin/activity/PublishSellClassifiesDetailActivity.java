package com.android.lockated.crmadmin.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.android.lockated.R;
import com.android.lockated.network.ConnectionDetector;
import com.android.lockated.preferences.LockatedPreferences;
import com.android.lockated.request.LockatedJSONObjectRequest;
import com.android.lockated.request.LockatedRequestError;
import com.android.lockated.request.LockatedVolleyRequestQueue;
import com.android.lockated.utils.ApplicationURL;
import com.android.lockated.utils.ShowImage;
import com.android.lockated.utils.Utilities;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

public class PublishSellClassifiesDetailActivity extends AppCompatActivity implements View.OnClickListener, Response.Listener<JSONObject>, Response.ErrorListener {
    String strImage;
    String spid;
    private ImageView mImageView;
    private TextView categoryValue;
    private RequestQueue mQueue;
    ProgressBar progressBar;
    private LockatedPreferences mLockatedPreferences;
    public static final String REQUEST_TAG = "PublishSellClassifiesDetailActivity";
    private TextView mThingType, mThingto, mRate, mExpiryDate, mDescription, mStatus, mClose, mDeny;
    private TextView mEmailId, mMobileNumber, mPostedBy;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_publish_sell_classifies_detail);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(true);
        getSupportActionBar().setTitle(R.string.classifieds);

        init();
        getDetails();
    }

    private void getDetails() {
        spid = getIntent().getExtras().getString("pid");
        mThingType.setText(getIntent().getExtras().getString("strThing"));
        mThingto.setText(getIntent().getExtras().getString("strThingTO"));
        mRate.setText(getIntent().getExtras().getString("strRate"));
        mExpiryDate.setText((getIntent().getExtras().getString("strDate")));
        mDescription.setText(getIntent().getExtras().getString("strDescription"));
        strImage = getIntent().getExtras().getString("ImagePicaso");
        mStatus.setText(getIntent().getExtras().getString("strStatus"));
        categoryValue.setText(getIntent().getExtras().getString("categoryText"));
        mPostedBy.setText(getIntent().getExtras().getString("name"));
        mMobileNumber.setText(getIntent().getExtras().getString("mobile"));
        mEmailId.setText(getIntent().getExtras().getString("email"));
        Picasso.with(this).load(strImage).fit().into(mImageView);
    }

    private void init() {
        progressBar = (ProgressBar) findViewById(R.id.mProgressBarView);
        mThingType = (TextView) findViewById(R.id.mThingType);
        mThingto = (TextView) findViewById(R.id.mThingto);
        mRate = (TextView) findViewById(R.id.mRate);
        mExpiryDate = (TextView) findViewById(R.id.mExpiryDate);
        mDescription = (TextView) findViewById(R.id.mDescription);
        mImageView = (ImageView) findViewById(R.id.mImageView);
        mStatus = (TextView) findViewById(R.id.mStatus);
        mClose = (TextView) findViewById(R.id.mClose);
        mDeny = (TextView) findViewById(R.id.mDeny);
        categoryValue = (TextView) findViewById(R.id.categoryValue);
        mPostedBy = (TextView) findViewById(R.id.mPostedBy);
        mMobileNumber = (TextView) findViewById(R.id.mMobileNumber);
        mEmailId = (TextView) findViewById(R.id.mEmailId);
        mClose.setOnClickListener(this);
        mDeny.setOnClickListener(this);
        mImageView.setOnClickListener(this);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            finish();
            return true;
        } else {
            return false;
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.mClose:
                finish();
                break;
            case R.id.mDeny:
                onDenyClicked();
                break;
            case  R.id.mImageView:
                Intent intent = new Intent(getApplicationContext(), ShowImage.class);
                intent.putExtra("imageUrlString",strImage);
                startActivity(intent);
                break;
        }
    }

    private void onDenyClicked() {
        progressBar.setVisibility(View.VISIBLE);
        if (ConnectionDetector.isConnectedToInternet(this)) {
            mLockatedPreferences = new LockatedPreferences(this);
            mQueue = LockatedVolleyRequestQueue.getInstance(this).getRequestQueue();
            String url = ApplicationURL.disableClassified
                    + spid
                    + "&token=" + mLockatedPreferences.getLockatedToken();
            LockatedJSONObjectRequest lockatedJSONObjectRequest = new LockatedJSONObjectRequest(Request.Method.GET,
                    url, null, this, this);
            lockatedJSONObjectRequest.setTag(REQUEST_TAG);
            mQueue.add(lockatedJSONObjectRequest);

        } else {
            Utilities.showToastMessage(this, this.getResources().getString(R.string.internet_connection_error));
        }
    }

    @Override
    public void onResponse(JSONObject response) {
        progressBar.setVisibility(View.GONE);
        if (response.has("id") && response.has("publish")) {
            try {
                if (response.getString("publish").equals("0")) {
                    Intent intent = new Intent(getApplicationContext(), AdminMyZoneActivity.class);
                    intent.putExtra("AdminMyZone", 1);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent);
                    finish();
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void onErrorResponse(VolleyError error) {
        progressBar.setVisibility(View.GONE);
        if (this != null) {
            LockatedRequestError.onRequestError(this, error);
        }
    }
}
