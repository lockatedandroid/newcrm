package com.android.lockated.crm.fragment.events;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.android.lockated.LockatedApplication;
import com.android.lockated.R;
import com.android.lockated.crm.activity.CreateNoticeListEventActivity;
import com.android.lockated.crm.adapters.MyEventAdapter;
import com.android.lockated.model.usermodel.EventModel.MyEvent.Classified;
import com.android.lockated.network.ConnectionDetector;
import com.android.lockated.preferences.LockatedPreferences;
import com.android.lockated.request.LockatedJSONObjectRequest;
import com.android.lockated.request.LockatedRequestError;
import com.android.lockated.request.LockatedVolleyRequestQueue;
import com.android.lockated.utils.ApplicationURL;
import com.android.lockated.utils.Utilities;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class MyEventsFragment extends Fragment implements View.OnClickListener, Response.Listener<JSONObject>, Response.ErrorListener {

    TextView errorMsg;
    RequestQueue mQueue;
    RecyclerView eventList;
    ProgressBar progressBar;
    JSONObject eventJsonObj;
    FloatingActionButton fab;
    MyEventAdapter myEventAdapter;
    ArrayList<Classified> eventArrayList;
    String SHOW, CREATE, INDEX, UPDATE, EDIT;
    String REQUEST_TAG = "MyEventsFragment";
    LockatedPreferences mLockatedPreferences;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View eventView = inflater.inflate(R.layout.fragment_my_event, container, false);
        Utilities.ladooIntegration(getActivity(), "My Events");
        init(eventView);
        return eventView;

    }

    private void init(View view) {
        eventArrayList = new ArrayList<>();
        eventList = (RecyclerView) view.findViewById(R.id.eventList);
        final LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        eventList.setLayoutManager(linearLayoutManager);
        eventList.setHasFixedSize(true);
        myEventAdapter = new MyEventAdapter(getActivity(), eventArrayList, getChildFragmentManager());
        eventList.setAdapter(myEventAdapter);
        progressBar = (ProgressBar) view.findViewById(R.id.mProgressBarView);
        errorMsg = (TextView) view.findViewById(R.id.noNotices);
        mLockatedPreferences = new LockatedPreferences(getActivity());
        fab = (FloatingActionButton) view.findViewById(R.id.fab);
        fab.setOnClickListener(this);
        getMySocietyRoles();
        checkPermission();
    }

    private void checkPermission() {
        if (CREATE!=null&&CREATE.equals("true")) {
            fab.setVisibility(View.VISIBLE);
        } else {
            fab.setVisibility(View.GONE);
        }
        if (SHOW!=null&&SHOW.equals("true")) {
            callApi();
        } else {
            eventList.setVisibility(View.GONE);
            errorMsg.setVisibility(View.VISIBLE);
            errorMsg.setText(R.string.no_permission_error);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        LockatedApplication.getInstance().trackScreenView(getString(R.string.my_events));
        Utilities.lockatedGoogleAnalytics(getString(R.string.my_events),
                getString(R.string.visited), getString(R.string.my_events));
       /* eventArrayList.clear();
        callApi();*/
    }

    public void callApi() {
        if (ConnectionDetector.isConnectedToInternet(getActivity())) {
            progressBar.setVisibility(View.VISIBLE);
            mLockatedPreferences = new LockatedPreferences(getActivity());
            String url = ApplicationURL.getMyEventsUrl + mLockatedPreferences.getLockatedToken()
                    + ApplicationURL.societyId + mLockatedPreferences.getSocietyId();
            LockatedVolleyRequestQueue lockatedVolleyRequestQueue = LockatedVolleyRequestQueue.getInstance(getActivity());
            lockatedVolleyRequestQueue.sendRequest(REQUEST_TAG, Request.Method.GET, url, null, this, this);
            /*mQueue = LockatedVolleyRequestQueue.getInstance(getActivity()).getRequestQueue();
            LockatedJSONObjectRequest lockatedJSONObjectRequest = new LockatedJSONObjectRequest(Request.Method.GET,
                    url, null, this, this);
            lockatedJSONObjectRequest.setTag(REQUEST_TAG);
            mQueue.add(lockatedJSONObjectRequest);*/
        } else {
            Utilities.showToastMessage(getActivity(), getActivity().getResources().getString(R.string.internet_connection_error));
        }
    }

    @Override
    public void onResponse(JSONObject response) {
        progressBar.setVisibility(View.GONE);
        if (getActivity() != null) {
            try {
                JSONObject jsonObject = (JSONObject) response;
                if (jsonObject != null && jsonObject.length() > 0 && jsonObject.getJSONArray("classifieds").length() > 0) {
                    if (response.has("code")) {
                        errorMsg.setVisibility(View.VISIBLE);
                        eventList.setVisibility(View.GONE);
                    } else {
                        JSONArray jsonArray = jsonObject.getJSONArray("classifieds");
                        Gson gson = new Gson();
                        for (int i = 0; i < jsonArray.length(); i++) {
                            Classified classified = gson.fromJson(jsonArray.getJSONObject(i).toString(), Classified.class);
                            eventArrayList.add(classified);
                        }
                        myEventAdapter.notifyDataSetChanged();
                    }
                } else {
                    eventList.setVisibility(View.GONE);
                    errorMsg.setVisibility(View.VISIBLE);
                    errorMsg.setText(R.string.no_data_error);
                }

            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            eventList.setVisibility(View.GONE);
            errorMsg.setVisibility(View.VISIBLE);
            errorMsg.setText(R.string.no_data_error);
        }
    }

    @Override
    public void onErrorResponse(VolleyError error) {
        if (getActivity() != null) {
            progressBar.setVisibility(View.GONE);
            LockatedRequestError.onRequestError(getActivity(), error);
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.fab:
                Intent intent = new Intent(getActivity(), CreateNoticeListEventActivity.class);
                startActivity(intent);
                break;
        }
    }

    public String getMySocietyRoles() {
        String mySocietyRoles = getActivity().getResources().getString(R.string.blank_value);
        if (mLockatedPreferences.getRolesJson() != null && !mLockatedPreferences.getRolesJson().equals("blank")) {
            try {
                JSONObject noticeJsonObj = new JSONObject(mLockatedPreferences.getRolesJson());
                for (int i = 0; i < noticeJsonObj.getJSONArray(getActivity().getResources().getString(R.string.permissions_value)).length(); i++) {
                    JSONObject jsonObject1 = noticeJsonObj.getJSONArray(getActivity().getResources().getString(R.string.permissions_value)).getJSONObject(i);
                    if (jsonObject1.has(getActivity().getResources().getString(R.string.section_value))
                            && jsonObject1.getString(getActivity().getResources().getString(R.string.section_value)).equals("spree_events")) {
                        if (jsonObject1.getJSONObject(getActivity().getResources().getString(R.string.permission_value)).has(getActivity().getResources().getString(R.string.index_value))) {
                            mySocietyRoles = jsonObject1.getJSONObject(getActivity().getResources().getString(R.string.permission_value)).getString(getActivity().getResources().getString(R.string.index_value));
                            CREATE = jsonObject1.getJSONObject(getActivity().getResources().getString(R.string.permission_value)).getString("create");
                            INDEX = jsonObject1.getJSONObject(getActivity().getResources().getString(R.string.permission_value)).getString("index");
                            UPDATE = jsonObject1.getJSONObject(getActivity().getResources().getString(R.string.permission_value)).getString("update");
                            EDIT = jsonObject1.getJSONObject(getActivity().getResources().getString(R.string.permission_value)).getString("edit");
                            SHOW = jsonObject1.getJSONObject(getActivity().getResources().getString(R.string.permission_value)).getString("show");
                        }
                    }
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return mySocietyRoles;
    }
}
