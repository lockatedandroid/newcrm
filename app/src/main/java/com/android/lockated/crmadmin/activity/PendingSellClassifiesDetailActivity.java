package com.android.lockated.crmadmin.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.android.lockated.R;
import com.android.lockated.network.ConnectionDetector;
import com.android.lockated.preferences.LockatedPreferences;
import com.android.lockated.request.LockatedJSONObjectRequest;
import com.android.lockated.request.LockatedRequestError;
import com.android.lockated.request.LockatedVolleyRequestQueue;
import com.android.lockated.utils.ApplicationURL;
import com.android.lockated.utils.ShowImage;
import com.android.lockated.utils.Utilities;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

public class PendingSellClassifiesDetailActivity extends AppCompatActivity implements View.OnClickListener, Response.Listener<JSONObject>, Response.ErrorListener {
    String spid;
    String strImage;
    private ImageView mImageView;
    private TextView categoryValue;
    private RequestQueue mQueue;
    private ProgressBar progressBar;
    private LockatedPreferences mLockatedPreferences;
    public static final String REQUEST_TAG = "PendingSellClassifiesDetailActivity";

    private TextView mThingType, mThingto, mRate, mExpiryDate, mDescription, mStatus, mPublish, mClose;
    private TextView mEmailId, mMobileNumber, mPostedBy;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pending_sell_classifies_detail);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(true);
        getSupportActionBar().setTitle("Classifieds");
        init();
        getDetails();
    }

    private void getDetails() {
        spid = getIntent().getExtras().getString("pid");
        mThingType.setText(getIntent().getExtras().getString("strThing"));
        mThingto.setText(getIntent().getExtras().getString("strThingTO"));
        mRate.setText(getIntent().getExtras().getString("strRate"));
        mExpiryDate.setText((getIntent().getExtras().getString("strDate")));
        mDescription.setText(getIntent().getExtras().getString("strDescription"));
        strImage = getIntent().getExtras().getString("ImagePicaso");
        mStatus.setText(getIntent().getExtras().getString("strStatus"));
        categoryValue.setText(getIntent().getExtras().getString("categoryValue"));
        mPostedBy.setText(getIntent().getExtras().getString("name"));
        mMobileNumber.setText(getIntent().getExtras().getString("mobile"));
        mEmailId.setText(getIntent().getExtras().getString("email"));
        Picasso.with(this).load(strImage).fit().into(mImageView);
    }

    private void init() {
        progressBar = (ProgressBar) findViewById(R.id.mProgressBarView);
        mThingType = (TextView) findViewById(R.id.mThingType);
        mThingto = (TextView) findViewById(R.id.mThingto);
        mRate = (TextView) findViewById(R.id.mRate);
        mExpiryDate = (TextView) findViewById(R.id.mExpiryDate);
        mDescription = (TextView) findViewById(R.id.mDescription);
        mImageView = (ImageView) findViewById(R.id.mImageView);
        mStatus = (TextView) findViewById(R.id.mStatus);
        mPublish = (TextView) findViewById(R.id.mPublish);
        mClose = (TextView) findViewById(R.id.mClose);
        mPostedBy = (TextView) findViewById(R.id.mPostedBy);
        mMobileNumber = (TextView) findViewById(R.id.mMobileNumber);
        mEmailId = (TextView) findViewById(R.id.mEmailId);
        categoryValue = (TextView) findViewById(R.id.categoryValue);
        mPublish.setOnClickListener(this);
        mClose.setOnClickListener(this);
        mImageView.setOnClickListener(this);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            finish();
            return true;
        } else {
            return false;
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.mClose:
                finish();
                break;
            case R.id.mPublish:
                onPublishClicked();
                break;
            case R.id.mImageView:
                Intent intent = new Intent(getApplicationContext(), ShowImage.class);
                intent.putExtra("imageUrlString", strImage);
                startActivity(intent);
                break;
        }
    }

    private void onPublishClicked() {
        progressBar.setVisibility(View.VISIBLE);
        if (ConnectionDetector.isConnectedToInternet(this)) {
            mLockatedPreferences = new LockatedPreferences(this);
            mQueue = LockatedVolleyRequestQueue.getInstance(this).getRequestQueue();
            String url = ApplicationURL.PublishClassified + spid
                    + "&token=" + mLockatedPreferences.getLockatedToken();
            LockatedJSONObjectRequest lockatedJSONObjectRequest = new LockatedJSONObjectRequest(Request.Method.GET,
                    url, null, this, this);
            //Log.e("Admin published url", "" + url);
            lockatedJSONObjectRequest.setTag(REQUEST_TAG);
            mQueue.add(lockatedJSONObjectRequest);
        } else {
            Utilities.showToastMessage(this, this.getResources().getString(R.string.internet_connection_error));
        }
    }

    @Override
    public void onResponse(JSONObject response) {
        progressBar.setVisibility(View.GONE);
        if (response.has("id") && response.has("publish")) {
            try {
                if (response.getString("publish").equals("1")) {
                    Intent intent = new Intent(this, AdminMyZoneActivity.class);
                    intent.putExtra("AdminMyZone", 0);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent);
                    finish();
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void onErrorResponse(VolleyError error) {
        //Log.e("pending Help. Activity",""+error);
        progressBar.setVisibility(View.GONE);
        if (this != null) {
            LockatedRequestError.onRequestError(this, error);
        }
    }
}
