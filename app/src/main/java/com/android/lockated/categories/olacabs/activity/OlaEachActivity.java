package com.android.lockated.categories.olacabs.activity;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.lockated.R;
import com.android.lockated.model.OlaCab.OlaDetailModel.ThirdpartyTransaction;
import com.android.lockated.utils.Utilities;

import java.util.ArrayList;


public class OlaEachActivity extends AppCompatActivity {
    int position;
    public static ArrayList<ThirdpartyTransaction> thirdpartyTransactions;
    EditText edt_booking_id,edt_status,edt_ola_amount,edt_ola_date,edt_ola_distance;
    TextView txt_cash_payable_amount;
    LinearLayout layout_ola_distance;
    RelativeLayout layout_ola_cash_payable;
    private ProgressBar progressBar;
    private String screenName = "OlaListActivity";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ola_each);
        Toolbar toolbar = (Toolbar) findViewById(R.id.custom_toolbar);
        setSupportActionBar(toolbar);

        Utilities.ladooIntegration(OlaEachActivity.this, screenName);
        Utilities.lockatedGoogleAnalytics(screenName, getString(R.string.visited), screenName);

        getSupportActionBar().setTitle("Booking Details");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(true);
        progressBar = (ProgressBar) findViewById(R.id.mProgressBarView);
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            position = extras.getInt("Position");
        }
        init();
        setData(position);
    }

    public void init() {

        edt_booking_id = (EditText) findViewById(R.id.edt_booking_id);
        edt_ola_amount = (EditText) findViewById(R.id.edt_ola_amount);
        edt_ola_date = (EditText) findViewById(R.id.edt_ola_date);
        edt_ola_distance = (EditText) findViewById(R.id.edt_ola_distance);
        edt_status = (EditText) findViewById(R.id.edt_status);

        txt_cash_payable_amount = (TextView) findViewById(R.id.txt_cash_payable_amount);

        layout_ola_cash_payable = (RelativeLayout)findViewById(R.id.layout_ola_cash_payable) ;
        layout_ola_distance = (LinearLayout)findViewById(R.id.layout_ola_distance);
    }


    public void setData(int position) {
        if (thirdpartyTransactions.get(position).getDeliverylabel().equals("COMPLETED"))
        {
            layout_ola_cash_payable.setVisibility(View.VISIBLE);
            layout_ola_distance.setVisibility(View.VISIBLE);
            txt_cash_payable_amount.setText(""+getResources().getString(R.string.rupees_symbol)+" "+thirdpartyTransactions.get(position).getTpdetails().getTripInfo().getPayableAmount());
            edt_ola_distance.setText(""+ thirdpartyTransactions.get(position).getTpdetails().getTripInfo().getDistance().getValue() +" "+ thirdpartyTransactions.get(position).getTpdetails().getTripInfo().getDistance().getUnit());
            edt_ola_amount.setText("" + thirdpartyTransactions.get(position).getTpdetails().getTripInfo().getPayableAmount()+" "+getResources().getString(R.string.rupees_symbol));
        }
        else
        {
            edt_ola_amount.setText("-");
        }
        progressBar.setVisibility(View.GONE);
        edt_ola_date.setText(Utilities.dateTime_AMPM_Format(thirdpartyTransactions.get(position).getUpdatedAt()));
        edt_booking_id.setText("" + thirdpartyTransactions.get(position).getTptransactionid());
        edt_status.setText(""+thirdpartyTransactions.get(position).getDeliverylabel());
    }
}
