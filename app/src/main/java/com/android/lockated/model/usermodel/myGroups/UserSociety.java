
package com.android.lockated.model.usermodel.myGroups;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


public class UserSociety implements Parcelable {

    @SerializedName("id")
    @Expose
    private int id;
    @SerializedName("id_society")
    @Expose
    private String idSociety;
    @SerializedName("approve")
    @Expose
    private boolean approve;
    @SerializedName("created_at")
    @Expose
    private String createdAt;
    @SerializedName("updated_at")
    @Expose
    private String updatedAt;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("user")
    @Expose
    private User user;
    @SerializedName("society")
    @Expose
    private Society society;
    @SerializedName("user_flat")
    @Expose
    private UserFlat userFlat;

    /**
     * @return The id
     */
    public int getId() {
        return id;
    }

    /**
     * @param id The id
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * @return The idSociety
     */
    public String getIdSociety() {
        return idSociety;
    }

    /**
     * @param idSociety The id_society
     */
    public void setIdSociety(String idSociety) {
        this.idSociety = idSociety;
    }

    /**
     * @return The approve
     */
    public boolean isApprove() {
        return approve;
    }

    /**
     * @param approve The approve
     */
    public void setApprove(boolean approve) {
        this.approve = approve;
    }

    /**
     * @return The createdAt
     */
    public String getCreatedAt() {
        return createdAt;
    }

    /**
     * @param createdAt The created_at
     */
    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    /**
     * @return The updatedAt
     */
    public String getUpdatedAt() {
        return updatedAt;
    }

    /**
     * @param updatedAt The updated_at
     */
    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    /**
     * @return The status
     */
    public String getStatus() {
        return status;
    }

    /**
     * @param status The status
     */
    public void setStatus(String status) {
        this.status = status;
    }

    /**
     * @return The user
     */
    public User getUser() {
        return user;
    }

    /**
     * @param user The user
     */
    public void setUser(User user) {
        this.user = user;
    }

    /**
     * @return The society
     */
    public Society getSociety() {
        return society;
    }

    /**
     * @param society The society
     */
    public void setSociety(Society society) {
        this.society = society;
    }

    /**
     * @return The userFlat
     */
    public UserFlat getUserFlat() {
        return userFlat;
    }

    /**
     * @param userFlat The user_flat
     */
    public void setUserFlat(UserFlat userFlat) {
        this.userFlat = userFlat;
    }

    @Override
    public int describeContents() {
        return this.hashCode();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(id);
        dest.writeString(idSociety);
        dest.writeByte((byte) (approve ? 1 : 0));
        dest.writeString(createdAt);
        dest.writeString(updatedAt);
        dest.writeString(status);
        dest.writeParcelable(user, flags);
        dest.writeParcelable(society, flags);
        dest.writeParcelable(userFlat, flags);
    }

    private UserSociety(Parcel in) {
        id = in.readInt();
        idSociety = in.readString();
        approve = in.readByte() != 0;
        createdAt = in.readString();
        updatedAt = in.readString();
        status = in.readString();
        user = in.readParcelable(User.class.getClassLoader());
        society = in.readParcelable(Society.class.getClassLoader());
        userFlat = in.readParcelable(UserFlat.class.getClassLoader());
    }

    public static final Parcelable.Creator<UserSociety> CREATOR = new Parcelable.Creator<UserSociety>() {
        public UserSociety createFromParcel(Parcel in) {
            return new UserSociety(in);
        }

        public UserSociety[] newArray(int size) {
            return new UserSociety[size];
        }
    };

}
