package com.android.lockated.crm.fragment.polls;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AlertDialog;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.lockated.R;
import com.android.lockated.account.adapter.GroupListAdapter;
import com.android.lockated.crm.activity.CrmPollsListActivity;
import com.android.lockated.information.AccountController;
import com.android.lockated.model.AccountData;
import com.android.lockated.network.ConnectionDetector;
import com.android.lockated.preferences.LockatedPreferences;
import com.android.lockated.request.LockatedJSONObjectRequest;
import com.android.lockated.request.LockatedRequestError;
import com.android.lockated.request.LockatedVolleyRequestQueue;
import com.android.lockated.utils.ApplicationURL;
import com.android.lockated.utils.DatePickerFragment;
import com.android.lockated.utils.TimePickerFragment;
import com.android.lockated.utils.Utilities;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class CreatePollsFragment extends Fragment implements View.OnClickListener,
        Response.ErrorListener, Response.Listener<JSONObject>, CompoundButton.OnCheckedChangeListener {

    private View PollsFragmentCreateNoticeView;
    private ImageButton mAddFields;
    private ImageButton mremoveFields;
    FragmentManager fragmentManager;
    private LinearLayout LinearParent;
    private LinearLayout LinearAdd;

    private TextView mTextStartdate;
    private TextView mTextStartTime;
    private TextView mTextEndTime;
    private TextView mTextEndDate;
    private TextView allMembers;
    private TextView groupMembers;

    private ImageView mIageStartDate;
    private ImageView mImageEndDate;
    private ImageView mImageStartTIme;
    private ImageView mImageEndTime;

    private TextView mPreview;
    private TextView mProceed;
    private EditText mEditTextSubject;
    private EditText mOption1, mOption2, mOption3, mOption4;
    private CheckBox mMultipleCheck;

    int MultipleCheck = 0;
    private String strSubject;
    private String strStartDate;
    private String strEndDate;
    private String strOption1;
    private String strOption2;
    private String strOption3;
    private String strOption4;
    private String Options;
    String strStartTime;
    String strEndTime;

    //private ProgressBar mProgressBar;
    private ProgressDialog mProgressDialog;
    private RequestQueue mQueue;
    private LockatedPreferences mLockatedPreferences;
    public static final String REQUEST_TAG = "CreatePollsFragment";
    public static final String GET_MY_GROUPS_TAG = "GetMyGroups";
    FloatingActionButton fab;
    AccountController accountController;
    ArrayList<AccountData> accountDataArrayList;
    String societyId;
    GroupListAdapter groupListAdapter;
    int groupIdValue = 0;

    public FragmentManager setFragmentManager(FragmentManager fragmentManager) {
        this.fragmentManager = fragmentManager;
        return fragmentManager;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        PollsFragmentCreateNoticeView = inflater.inflate(R.layout.fragment_polls, container, false);
        init();
        return PollsFragmentCreateNoticeView;
    }

    private void init() {
        mLockatedPreferences = new LockatedPreferences(getActivity());
        //LinearAdd = (LinearLayout) PollsFragmentCreateNoticeView.findViewById(R.id.LinearAdd);
        mProgressDialog = new ProgressDialog(getActivity());
        mProgressDialog.setMessage("Please Wait...");
        mProgressDialog.setCanceledOnTouchOutside(false);
        //mProgressDialog = ProgressDialog.show(getActivity(), "", "Please Wait...", false);
        LinearParent = (LinearLayout) PollsFragmentCreateNoticeView.findViewById(R.id.LinearParent);

        mIageStartDate = (ImageView) PollsFragmentCreateNoticeView.findViewById(R.id.mIageStartDate);
        mImageEndDate = (ImageView) PollsFragmentCreateNoticeView.findViewById(R.id.mImageEndDate);
        mImageStartTIme = (ImageView) PollsFragmentCreateNoticeView.findViewById(R.id.mImageStartTIme);
        mImageEndTime = (ImageView) PollsFragmentCreateNoticeView.findViewById(R.id.mImageEndTime);

        mTextStartdate = (TextView) PollsFragmentCreateNoticeView.findViewById(R.id.mTextStartdate);
        mTextEndDate = (TextView) PollsFragmentCreateNoticeView.findViewById(R.id.mTextEndDate);
        mTextStartTime = (TextView) PollsFragmentCreateNoticeView.findViewById(R.id.mTextStartTime);
        mTextEndTime = (TextView) PollsFragmentCreateNoticeView.findViewById(R.id.mTextEndTime);
        allMembers = (TextView) PollsFragmentCreateNoticeView.findViewById(R.id.allMembers);
        groupMembers = (TextView) PollsFragmentCreateNoticeView.findViewById(R.id.groupMembers);

        mEditTextSubject = (EditText) PollsFragmentCreateNoticeView.findViewById(R.id.mEditTextSubject);
        mOption1 = (EditText) PollsFragmentCreateNoticeView.findViewById(R.id.mOption1);
        mOption2 = (EditText) PollsFragmentCreateNoticeView.findViewById(R.id.mOption2);
        mOption3 = (EditText) PollsFragmentCreateNoticeView.findViewById(R.id.mOption3);
        mOption4 = (EditText) PollsFragmentCreateNoticeView.findViewById(R.id.mOption4);
        //mMultipleCheck = (CheckBox) PollsFragmentCreateNoticeView.findViewById(R.id.mMultipleCheck);
        mPreview = (TextView) PollsFragmentCreateNoticeView.findViewById(R.id.mPreview);
        mProceed = (TextView) PollsFragmentCreateNoticeView.findViewById(R.id.mProceed);

        allMembers.setOnClickListener(this);
        groupMembers.setOnClickListener(this);
        mIageStartDate.setOnClickListener(this);
        mImageEndDate.setOnClickListener(this);
        mImageEndTime.setOnClickListener(this);
        mImageStartTIme.setOnClickListener(this);
        mProceed.setOnClickListener(this);
        //mMultipleCheck.setOnCheckedChangeListener(this);
        mPreview.setOnClickListener(this);
       /* if (mLockatedPreferences.getGroupList() != null && !mLockatedPreferences.getGroupList().equals("NoData")) {
            JSONObject jsonObject = null;
            try {
                jsonObject = new JSONObject(mLockatedPreferences.getGroupList());
                groupListAdapter = new GroupListAdapter(getActivity(), jsonObject.getJSONArray("usergroups"));
            } catch (JSONException e) {
                e.printStackTrace();
            }
        } else {
            callGroupApi();
        }*/
    }

   /* private void callGroupApi() {
        if (getActivity() != null) {
            if (ConnectionDetector.isConnectedToInternet(getActivity())) {
                mProgressDialog.show();
                RequestQueue mQueue = LockatedVolleyRequestQueue.getInstance(getActivity()).getRequestQueue();
                String url = ApplicationURL.getMyGroups + mLockatedPreferences.getLockatedToken();
                LockatedJSONObjectRequest lockatedJSONObjectRequest = new LockatedJSONObjectRequest(Request.Method.GET,
                        url, null, this, this);
                lockatedJSONObjectRequest.setTag(GET_MY_GROUPS_TAG);
                mQueue.add(lockatedJSONObjectRequest);
            } else {
                Utilities.showToastMessage(getActivity(), getActivity().getResources()
                        .getString(R.string.internet_connection_error));
            }
        }
    }*/

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.mIageStartDate:
                openDatePicker(mTextStartdate);
                break;
            case R.id.mImageEndDate:
                openDatePicker(mTextEndDate);
                break;
            case R.id.mImageStartTIme:
                getPollStartTime(mTextStartTime);
                break;
            case R.id.mImageEndTime:
                getPollStartTime(mTextEndTime);
                break;
            case R.id.mPreview:
                onPreviewClicked();
                break;
            case R.id.mProceed:
                OnProceedDetails(groupIdValue);
                break;
            case R.id.allMembers:
                changeViewColor(allMembers, groupMembers);
                groupIdValue = 0;
                break;
            case R.id.groupMembers:
                changeViewColor(groupMembers, allMembers);
                groupMemberListAlertDialog(getActivity(), "Whome to send");
                break;
        }
    }

    private void changeViewColor(TextView textView1, TextView textView2) {
        textView1.setTextColor(this.getResources().getColor(R.color.white));
        textView1.setBackgroundColor(this.getResources().getColor(R.color.primary));
        textView2.setTextColor(this.getResources().getColor(R.color.primary));
        textView2.setBackgroundDrawable(this.getResources().getDrawable(R.drawable.border_card_layout));
    }

    private void getPollStartTime(TextView time) {
        TimePickerFragment timePickerFragment = new TimePickerFragment();
        timePickerFragment.setTimePickerView(time);
        DialogFragment newFragment = timePickerFragment;
        newFragment.show(getActivity().getSupportFragmentManager(), "TimePicker");
    }

    private void groupMemberListAlertDialog(Context context, String groupName) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle(groupName);
        builder.setAdapter(groupListAdapter, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                try {
                    JSONObject jsonObject = new JSONObject(mLockatedPreferences.getGroupList());
                    String groupId = jsonObject.getJSONArray("usergroups").getJSONObject(which).getString("id");
                    /*OnBProceedClicked(Integer.valueOf(groupId));*/
                    groupIdValue = Integer.valueOf(groupId);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
        AlertDialog dialog = builder.create();
        dialog.show();
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = 450;
        dialog.getWindow().setAttributes(lp);
        dialog.setCanceledOnTouchOutside(true);
        dialog.getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCH_MODAL,
                WindowManager.LayoutParams.FLAG_WATCH_OUTSIDE_TOUCH);
    }

    private void OnProceedDetails(int groupIdValue) {
        accountController = AccountController.getInstance();
        accountDataArrayList = accountController.getmAccountDataList();
        societyId = mLockatedPreferences.getSocietyId();
        strSubject = mEditTextSubject.getText().toString();

        strStartDate = mTextStartdate.getText().toString();
        strStartTime = mTextStartTime.getText().toString();
        strEndTime = mTextEndTime.getText().toString();
        strEndDate = mTextEndDate.getText().toString();

        strOption1 = mOption1.getText().toString();
        strOption2 = mOption2.getText().toString();
        strOption3 = mOption3.getText().toString();
        strOption4 = mOption4.getText().toString();
        strSubject = mEditTextSubject.getText().toString();
        if ((TextUtils.isEmpty(strSubject))) {
            Utilities.showToastMessage(getContext(), "Please Fill Subject");
        } else if ((TextUtils.isEmpty(strOption1))) {
            Utilities.showToastMessage(getContext(), "Please Fill Option1");
        } else if ((TextUtils.isEmpty(strOption2))) {
            Utilities.showToastMessage(getContext(), "Please Fill Option2");
        } else if ((strStartDate.equals("Start Date"))) {
            Utilities.showToastMessage(getContext(), "Please select Start Date");
        } else if (strStartTime.equals("Start Time")) {
            Utilities.showToastMessage(getContext(), "Please select Start Time");
        } else if (strEndDate.equals("End Date")) {
            Utilities.showToastMessage(getContext(), "Please select End date");
        } else if (strEndTime.equals("End Time")) {
            Utilities.showToastMessage(getContext(), "Please select End Time");
        } else {
            if (ConnectionDetector.isConnectedToInternet(getActivity())) {
                mProgressDialog.show();
                JSONObject jsonObjectMain = new JSONObject();
                JSONArray jsonArray = new JSONArray();
                JSONObject jsonObject = new JSONObject();
                JSONObject jsonObject1 = new JSONObject();
                JSONObject jsonObject2 = new JSONObject();
                JSONObject jsonObject3 = new JSONObject();
                try {
                    if (groupIdValue == 0) {
                        jsonObject.put("shared", "" + groupIdValue);
                        jsonObject.put("group_id", "" + groupIdValue);
                    } else {
                        jsonObject.put("shared", "1");
                        jsonObject.put("group_id", "" + groupIdValue);
                    }
                    jsonObject.put("user_society_id", "" + mLockatedPreferences.getUserSocietyId());
                    jsonObjectMain.put("subject", strSubject);
                    jsonObjectMain.put("society_id", societyId);
                    jsonObjectMain.put("start", strStartDate + "T" + strStartTime);
                    jsonObjectMain.put("end", strEndDate + "T" + strEndTime);
                    jsonObject.put("name", strOption1);

                    jsonObject1.put("name", strOption2);
                    if (!strOption3.equals("")) {
                        jsonObject2.put("name", strOption3);
                        jsonArray.put(2, jsonObject2);
                    }
                    if (!strOption4.equals("")) {
                        jsonObject3.put("name", strOption4);
                        jsonArray.put(3, jsonObject3);
                    }
                    jsonArray.put(0, jsonObject);
                    jsonArray.put(1, jsonObject1);
                    jsonObjectMain.put("options", jsonArray);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                try {
                    mQueue = LockatedVolleyRequestQueue.getInstance(getActivity()).getRequestQueue();
                    LockatedJSONObjectRequest lockatedJSONObjectRequest = new LockatedJSONObjectRequest(Request.Method.POST,
                            ApplicationURL.addPollsDetails + mLockatedPreferences.getLockatedToken(), jsonObjectMain, this, this);
                    lockatedJSONObjectRequest.setTag(REQUEST_TAG);
                    mQueue.add(lockatedJSONObjectRequest);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else {
                Utilities.showToastMessage(getActivity(), getActivity().getResources().getString(R.string.internet_connection_error));
            }
        }
    }

    private void onPreviewClicked() {
        strSubject = mEditTextSubject.getText().toString();
        strStartDate = mTextStartdate.getText().toString();
        strStartTime = mTextStartTime.getText().toString();
        strEndTime = mTextEndTime.getText().toString();
        strEndDate = mTextEndDate.getText().toString();

        strOption1 = mOption1.getText().toString();
        strOption2 = mOption2.getText().toString();
        strOption3 = mOption3.getText().toString();
        strOption4 = mOption4.getText().toString();
      //  Options = strOption1 + strOption2 + strOption3 + strOption4;
        if ((TextUtils.isEmpty(strSubject))) {
            Utilities.showToastMessage(getContext(), "Please Fill Subject");
        } else if ((TextUtils.isEmpty(strOption1))) {
            Utilities.showToastMessage(getContext(), "Please Fill Option1");
        } else if ((TextUtils.isEmpty(strOption2))) {
            Utilities.showToastMessage(getContext(), "Please Fill Option2");
        }/* else if ((TextUtils.isEmpty(strOption3))) {
            Utilities.showToastMessage(getContext(), "Please Fill Option3");
        } else if ((TextUtils.isEmpty(strOption4))) {
            Utilities.showToastMessage(getContext(), "Please Fill Option4");
        } */ else if ((strStartDate.equals("Start Date"))) {
            Utilities.showToastMessage(getContext(), "Please select Start Date");
        } else if (strStartTime.equals("Start Time")) {
            Utilities.showToastMessage(getContext(), "Please select Start Time");
        } else if (strEndDate.equals("End Date")) {
            Utilities.showToastMessage(getContext(), "Please select End date");
        } else if (strEndTime.equals("End Time")) {
            Utilities.showToastMessage(getContext(), "Please select End Time");
        } else {
            PollsNoticePreview pollsNoticePreview = new PollsNoticePreview();
            Bundle args = new Bundle();
            args.putString("subject", strSubject);
            args.putString("startdate", strStartDate + "T" + strStartTime + "5:30");
            args.putString("enddate", strEndDate + "T" + strEndTime + "5:30");
            args.putString("duration", "" + MultipleCheck);
            args.putString("option1", strOption1);
            args.putString("option2", strOption2);
            if(!strOption3.equals("")) {
                args.putString("option3", strOption3);
            }
            else {
                args.putString("option3", "blank");

            } if(!strOption4.equals("")) {
                args.putString("option4", strOption4);
            }
            else {
                args.putString("option4", "blank");

            }

            pollsNoticePreview.setArguments(args);
            pollsNoticePreview.show(fragmentManager, "PreviewDialog");
        }
    }

    private void openDatePicker(TextView DatePicker) {
        DatePickerFragment datePickerFragment = new DatePickerFragment();
        datePickerFragment.setDatePickerView(DatePicker);
        DialogFragment newFragment = datePickerFragment;
        newFragment.show(getActivity().getSupportFragmentManager(), "DatePicker");
    }

    @Override
    public void onErrorResponse(VolleyError error) {
        /*mProgressBar.setVisibility(View.GONE);*/
        mProgressDialog.dismiss();
        if (getActivity() != null) {
            LockatedRequestError.onRequestError(getActivity(), error);
        }
    }

    @Override
    public void onResponse(JSONObject response) {
        mProgressDialog.dismiss();
        if (response.has("id")) {
            Intent backIntent = new Intent(getActivity(), CrmPollsListActivity.class);
            backIntent.putExtra("CrmPollsListActivity", 0);
            backIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(backIntent);
        } else {
            Utilities.showToastMessage(getActivity(), "Error response");
        }
    }

    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        if (isChecked) {
            MultipleCheck = 1;
        }
    }
}
