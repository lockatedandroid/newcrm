package com.android.lockated.cart.ecommerce;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ProgressBar;

import com.android.lockated.Interfaces.IRecyclerItemClickListener;
import com.android.lockated.R;
import com.android.lockated.cart.ecommerce.adapter.EcommerceCartAdapter;
import com.android.lockated.information.AccountController;
import com.android.lockated.model.AccountData;
import com.android.lockated.model.CartDetail;
import com.android.lockated.model.LineItemData;
import com.android.lockated.network.ConnectionDetector;
import com.android.lockated.preferences.LockatedPreferences;
import com.android.lockated.request.LockatedJSONObjectRequest;
import com.android.lockated.request.LockatedRequestError;
import com.android.lockated.request.LockatedVolleyRequestQueue;
import com.android.lockated.utils.ApplicationURL;
import com.android.lockated.utils.Utilities;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;

import org.json.JSONObject;

import java.util.ArrayList;

public class EcommerceFragment extends Fragment implements IRecyclerItemClickListener, Response.Listener, Response.ErrorListener {
    //private View mEcommerceFragmentView;

    private ProgressBar mProgressBarView;
    private RecyclerView mRecyclerView;
    private LinearLayout mLinearLayoutEmptyCart;

    private LockatedPreferences mLockatedPreferences;
    private EcommerceCartAdapter mEcommerceCartAdapter;

    private CartDetail mCartDetail;
    ArrayList<LineItemData> mCartDetailList;
    private static final String REQUEST_TAG = "EcommerceFragment";
    private static final String UPDATE_REQUEST_TAG = "Update_Product_Quantity";

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View mEcommerceFragmentView = inflater.inflate(R.layout.fragment_ecommerce, container, false);
        init(mEcommerceFragmentView);
        getCartDetail();
        return mEcommerceFragmentView;
    }

    private void init(View mEcommerceFragmentView) {
        mCartDetailList = new ArrayList<>();
        mLockatedPreferences = new LockatedPreferences(getActivity());


        mProgressBarView = (ProgressBar) mEcommerceFragmentView.findViewById(R.id.mProgressBarView);
        mRecyclerView = (RecyclerView) mEcommerceFragmentView.findViewById(R.id.mRecyclerView);
        mLinearLayoutEmptyCart = (LinearLayout) mEcommerceFragmentView.findViewById(R.id.mLinearLayoutEmptyCart);
        mLinearLayoutEmptyCart.setVisibility(View.GONE);

        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        mRecyclerView.setLayoutManager(mLayoutManager);
        mRecyclerView.setHasFixedSize(true);

        mEcommerceCartAdapter = new EcommerceCartAdapter(mCartDetailList, this);
        mRecyclerView.setAdapter(mEcommerceCartAdapter);
    }

    private void getCartDetail() {
        if (ConnectionDetector.isConnectedToInternet(getActivity())) {
            mProgressBarView.setVisibility(View.VISIBLE);
            String url = ApplicationURL.getCartDetailUrl + mLockatedPreferences.getLockatedToken();
            LockatedVolleyRequestQueue lockatedVolleyRequestQueue = LockatedVolleyRequestQueue.getInstance(getActivity());
            lockatedVolleyRequestQueue.sendRequest(REQUEST_TAG, Request.Method.GET, url, null, this, this);
            /*int socketTimeout = Utilities.REQUEST_TIME_OUT;
            RequestQueue mQueue = LockatedVolleyRequestQueue.getInstance(getActivity()).getRequestQueue();
            LockatedJSONObjectRequest lockatedJSONObjectRequest = new LockatedJSONObjectRequest(Request.Method.GET,
                    ApplicationURL.getCartDetailUrl + mLockatedPreferences.getLockatedToken(), null, this, this);
            lockatedJSONObjectRequest.setTag(REQUEST_TAG);
            RetryPolicy policy = new DefaultRetryPolicy(socketTimeout,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
            lockatedJSONObjectRequest.setRetryPolicy(policy);
            mQueue.add(lockatedJSONObjectRequest);*/
        } else {
            Utilities.showToastMessage(getActivity(), getActivity().getResources().getString(R.string.internet_connection_error));
        }
    }

    private void onItemDeleteClicked(LineItemData lineItemData) {
        if (ConnectionDetector.isConnectedToInternet(getActivity())) {
            lineItemData.setQuantity(lineItemData.getQuantity() + 1);
            mProgressBarView.setVisibility(View.VISIBLE);
            LockatedVolleyRequestQueue lockatedVolleyRequestQueue = LockatedVolleyRequestQueue.getInstance(getActivity());
            lockatedVolleyRequestQueue.sendRequest(UPDATE_REQUEST_TAG, Request.Method.DELETE, ApplicationURL.updateProductQuantityUrl
                    + mCartDetail.getNumber() + "/line_items/" + lineItemData.getId() + "?token=" + mLockatedPreferences.getLockatedToken(), null, this, this);
        } else {
            Utilities.showToastMessage(getActivity(), getActivity().getResources().getString(R.string.internet_connection_error));
        }
    }

    private void onQuantityAddClicked(LineItemData lineItemData) {
        if (ConnectionDetector.isConnectedToInternet(getActivity())) {
            lineItemData.setQuantity(lineItemData.getQuantity() + 1);
            mProgressBarView.setVisibility(View.VISIBLE);
            LockatedVolleyRequestQueue lockatedVolleyRequestQueue = LockatedVolleyRequestQueue.getInstance(getActivity());
            lockatedVolleyRequestQueue.sendRequest(UPDATE_REQUEST_TAG, Request.Method.PUT, ApplicationURL.updateProductQuantityUrl
                    + mCartDetail.getNumber() + "/line_items/" + lineItemData.getId()
                    + "?line_item[variant_id]=" + lineItemData.getVariantId() + "&line_item[quantity]="
                    + lineItemData.getQuantity() + "&token=" + mLockatedPreferences.getLockatedToken(), null, this, this);
        } else {
            Utilities.showToastMessage(getActivity(), getActivity().getResources().getString(R.string.internet_connection_error));
        }
    }

    private void onQuantityRemoveClicked(LineItemData lineItemData) {
        if (lineItemData.getQuantity() > 1) {
            if (ConnectionDetector.isConnectedToInternet(getActivity())) {
                lineItemData.setQuantity(lineItemData.getQuantity() - 1);
                mProgressBarView.setVisibility(View.VISIBLE);
                LockatedVolleyRequestQueue lockatedVolleyRequestQueue = LockatedVolleyRequestQueue.getInstance(getActivity());
                lockatedVolleyRequestQueue.sendRequest(UPDATE_REQUEST_TAG, Request.Method.PUT, ApplicationURL.updateProductQuantityUrl
                        + mCartDetail.getNumber() + "/line_items/" + lineItemData.getId() + "?line_item[variant_id]=" + lineItemData.getVariantId() + "&line_item[quantity]=" + lineItemData.getQuantity() + "&token=" + mLockatedPreferences.getLockatedToken(), null, this, this);
            } else {
                Utilities.showToastMessage(getActivity(), getActivity().getResources().getString(R.string.internet_connection_error));
            }
        } else {
            Utilities.showToastMessage(getActivity(), getActivity().getString(R.string.nonzero_product));
        }
    }

    @Override
    public void onRecyclerItemClick(View view, int position) {
        LineItemData lineItemData = mCartDetailList.get(position);
        switch (view.getId()) {
            case R.id.mImageViewProductQuantityAdd:
                onQuantityAddClicked(lineItemData);
                break;
            case R.id.mImageViewProductQuantityRemove:
                onQuantityRemoveClicked(lineItemData);
                break;
            case R.id.mTextViewProductRemove:
                onItemDeleteClicked(lineItemData);
                break;

        }
    }

    @Override
    public void onErrorResponse(VolleyError error) {
        mProgressBarView.setVisibility(View.INVISIBLE);
        if (getActivity() != null) {
            LockatedRequestError.onRequestError(getActivity(), error);
        }
    }

    @Override
    public void onResponse(Object response) {
        mProgressBarView.setVisibility(View.INVISIBLE);
        if (getActivity() != null) {
            JSONObject jsonObject = (JSONObject) response;
            mCartDetail = new CartDetail(jsonObject);
            if (mCartDetail != null) {
                ((EcommerceCartActivity) getActivity()).setTotalPrice(mCartDetail.getItemTotal());

                mCartDetailList.clear();
                for (int i = 0; i < mCartDetail.getmLineItemData().size(); i++) {
                    mCartDetailList.add(mCartDetail.getmLineItemData().get(i));
                }

                ArrayList<AccountData> accountDatas = AccountController.getInstance().getmAccountDataList();
                if (accountDatas.size() > 0) {
                    accountDatas.get(0).getCartDetailList().clear();
                    accountDatas.get(0).getCartDetailList().add(mCartDetail);
                }

                mEcommerceCartAdapter.notifyDataSetChanged();
            }
        }
    }
}
