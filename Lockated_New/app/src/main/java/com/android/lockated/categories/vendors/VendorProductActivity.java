package com.android.lockated.categories.vendors;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.android.lockated.LockatedApplication;
import com.android.lockated.R;
import com.android.lockated.cart.CartController;
import com.android.lockated.cart.ecommerce.EcommerceCartActivity;
import com.android.lockated.categories.vendors.fragment.VendorProductMenuFragment;
import com.android.lockated.categories.vendors.model.ProductData;
import com.android.lockated.information.AccountController;
import com.android.lockated.model.AccountData;
import com.android.lockated.model.CartDetail;
import com.android.lockated.network.ConnectionDetector;
import com.android.lockated.preferences.LockatedPreferences;
import com.android.lockated.request.LockatedJSONObjectRequest;
import com.android.lockated.request.LockatedRequestError;
import com.android.lockated.request.LockatedVolleyRequestQueue;
import com.android.lockated.utils.ApplicationURL;
import com.android.lockated.utils.Utilities;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class VendorProductActivity extends AppCompatActivity implements View.OnClickListener, Response.Listener, Response.ErrorListener {
    private TextView mTextViewProductDetailViewNext;
    private TextView mTextViewProductDetailViewContinue;
    private TextView mTextViewProductDetailViewTotalPrice;

    private ProgressDialog mProgressDialog;

    private CartController mCartController;
    private LockatedPreferences mLockatedPreferences;
    private LockatedJSONObjectRequest mLockatedJSONObjectRequest;
    private static final String ADD_PRODUCT_TAG = "GroceryActivity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_vendor_product);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(true);
        getSupportActionBar().setTitle(((LockatedApplication) getApplicationContext()).getHeaderName());

        if (savedInstanceState == null) {
            VendorProductMenuFragment vendorProductMenuFragment = new VendorProductMenuFragment();
            vendorProductMenuFragment.setVendorMenuData(((LockatedApplication) getApplicationContext()).getVendorProductData(), ((LockatedApplication) getApplicationContext()).getmSectionId());
            getSupportFragmentManager().beginTransaction().add(R.id.mVendorProductContainer, vendorProductMenuFragment).commit();
        }

        init();
    }

    private void init() {
        mCartController = CartController.getInstance();
        mLockatedPreferences = new LockatedPreferences(VendorProductActivity.this);
        mTextViewProductDetailViewNext = (TextView) findViewById(R.id.mTextViewProductDetailViewNext);
        mTextViewProductDetailViewContinue = (TextView) findViewById(R.id.mTextViewProductDetailViewContinue);
        mTextViewProductDetailViewTotalPrice = (TextView) findViewById(R.id.mTextViewProductDetailViewTotalPrice);

        mTextViewProductDetailViewNext.setOnClickListener(this);
        mTextViewProductDetailViewContinue.setVisibility(View.GONE);
        mTextViewProductDetailViewContinue.setOnClickListener(this);
    }

    public void productAddToCart(ProductData productData) {
        if (mCartController.mVendorProductDataList.size() > 0) {
            if (mCartController.mVendorProductDataList.contains(productData)) {
                if (productData.getQuantity() != 0) {
                    mCartController.mVendorProductDataList.remove(productData);
                    mCartController.mVendorProductDataList.add(productData);
                } else {
                    mCartController.mVendorProductDataList.remove(productData);
                }
            } else {
                mCartController.mVendorProductDataList.add(productData);
            }
        } else {
            mCartController.mVendorProductDataList.add(productData);
        }

        updatePriceInfo();
    }

    private void updatePriceInfo() {
        double price = 0.00;
        for (int i = 0; i < mCartController.mVendorProductDataList.size(); i++) {
            price += (Double.valueOf(mCartController.mVendorProductDataList.get(i).getPrice()) * mCartController.mVendorProductDataList.get(i).getQuantity());
        }

        mTextViewProductDetailViewTotalPrice.setText(getString(R.string.rupees_symbol) + " " + price + "");
    }

    private void onNextButtonClicked() {
        ArrayList<AccountData> mAccountData = AccountController.getInstance().getmAccountDataList();
        if (mAccountData.size() > 0) {
            if (mAccountData.get(0).getCartDetailList().size() > 0) {
                int cartSize = mCartController.mVendorProductDataList.size();
                if (cartSize > 0) {
                    JSONObject jsonObject = new JSONObject();
                    JSONArray jsonArray = new JSONArray();
                    try {
                        jsonObject.put("line_items", jsonArray);
                        for (int i = 0; i < mCartController.mVendorProductDataList.size(); i++) {
                            JSONObject itemObject = new JSONObject();
                            itemObject.put("variant_id", mCartController.mVendorProductDataList.get(i).getId());
                            itemObject.put("quantity", mCartController.mVendorProductDataList.get(i).getQuantity());
                            jsonArray.put(i, itemObject);
                        }

                        addAnotherItemToCart(jsonObject);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                } else {
                    if (mAccountData.get(0).getCartDetailList().get(0).getmLineItemData().size() > 0) {
                        openEcommerceCartActivity();
                    } else {
                        Utilities.showToastMessage(VendorProductActivity.this, "Please Select an Item to Proceed");
                    }
                }
            } else {
                int cartSize = mCartController.mVendorProductDataList.size();
                if (cartSize > 0) {
                    JSONObject jsonObject = new JSONObject();
                    JSONObject orderObject = new JSONObject();
                    JSONArray jsonArray = new JSONArray();
                    try {
                        jsonObject.put("order", orderObject);
                        orderObject.put("line_items", jsonArray);
                        for (int i = 0; i < mCartController.mVendorProductDataList.size(); i++) {
                            JSONObject itemObject = new JSONObject();
                            itemObject.put("variant_id", mCartController.mVendorProductDataList.get(i).getId());
                            itemObject.put("quantity", mCartController.mVendorProductDataList.get(i).getQuantity());
                            jsonArray.put(i, itemObject);
                        }

                        addProductToCart(jsonObject);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                } else {
                    if (mAccountData.get(0).getCartDetailList().size() > 0) {
                        if (mAccountData.get(0).getCartDetailList().get(0).getmLineItemData().size() > 0) {
                            openEcommerceCartActivity();
                        } else {
                            Utilities.showToastMessage(VendorProductActivity.this, getString(R.string.item_selection_error));
                        }
                    } else {
                        Utilities.showToastMessage(VendorProductActivity.this, getString(R.string.item_selection_error));
                    }
                }
            }
        }
    }

    private void addAnotherItemToCart(JSONObject jsonObject) {
        if (ConnectionDetector.isConnectedToInternet(VendorProductActivity.this)) {
            mProgressDialog = ProgressDialog.show(VendorProductActivity.this, "", "Please Wait...", false);
            String url = ApplicationURL.addAnotherItemToCartUrl + mLockatedPreferences.getLockatedToken();
            LockatedVolleyRequestQueue lockatedVolleyRequestQueue = LockatedVolleyRequestQueue.getInstance(VendorProductActivity.this);
            lockatedVolleyRequestQueue.sendRequest(ADD_PRODUCT_TAG, Request.Method.POST,
                    url, jsonObject, this, this);
        } else {
            Utilities.showToastMessage(VendorProductActivity.this, getString(R.string.internet_connection_error));
        }
    }

    private void addProductToCart(JSONObject jsonObject) {
        if (ConnectionDetector.isConnectedToInternet(VendorProductActivity.this)) {
            mProgressDialog = ProgressDialog.show(VendorProductActivity.this, "", "Please Wait...", false);
            LockatedVolleyRequestQueue lockatedVolleyRequestQueue = LockatedVolleyRequestQueue.getInstance(VendorProductActivity.this);
            lockatedVolleyRequestQueue.sendRequest(ADD_PRODUCT_TAG, Request.Method.POST,
                    ApplicationURL.addProductToCartUrl + mLockatedPreferences.getLockatedToken(), jsonObject, this, this);
        } else {
            Utilities.showToastMessage(VendorProductActivity.this, getString(R.string.internet_connection_error));
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.mTextViewProductDetailViewNext:
                onNextButtonClicked();
                break;
            case R.id.mTextViewProductDetailViewContinue:
                VendorProductActivity.this.finish();
                break;
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.home, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_cart: {
                onCartClicked();
                return true;
            }
            case android.R.id.home: {
                super.onBackPressed();
                return true;
            }
        }
        return super.onOptionsItemSelected(item);
    }

    private void onCartClicked() {
        ArrayList<AccountData> accountDatas = AccountController.getInstance().getmAccountDataList();
        if (accountDatas.size() > 0) {
            ArrayList<CartDetail> cartDetails = accountDatas.get(0).getCartDetailList();
            if (cartDetails.size() > 0) {
                if (cartDetails.get(0).getmLineItemData().size() > 0) {
                    Intent intent = new Intent(VendorProductActivity.this, EcommerceCartActivity.class);
                    startActivity(intent);
                } else {
                    Utilities.showToastMessage(VendorProductActivity.this, getString(R.string.empty_cart));
                }
            } else {
                Utilities.showToastMessage(VendorProductActivity.this, getString(R.string.empty_cart));
            }
        }
    }

    @Override
    public void onErrorResponse(VolleyError error) {
        if (mProgressDialog != null && mProgressDialog.isShowing()) {
            mProgressDialog.dismiss();
        }
        if (!isFinishing()) {
            LockatedRequestError.onRequestError(VendorProductActivity.this, error);
        }
    }

    @Override
    public void onResponse(Object response) {
        if (mProgressDialog != null && mProgressDialog.isShowing()) {
            mProgressDialog.dismiss();
        }
        ArrayList<AccountData> accountDatas = AccountController.getInstance().getmAccountDataList();
        if (accountDatas.size() > 0) {
            accountDatas.get(0).getCartDetailList().clear();

            JSONObject jsonObject = (JSONObject) response;
            CartDetail cartDetail = new CartDetail(jsonObject);
            accountDatas.get(0).getCartDetailList().add(cartDetail);
            mCartController.resetItems();

            openEcommerceCartActivity();
        }
    }

    private void openEcommerceCartActivity() {
        Intent intent = new Intent(VendorProductActivity.this, EcommerceCartActivity.class);
        startActivity(intent);
    }
}
