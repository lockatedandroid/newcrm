package com.android.lockated.categories.reminder.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.android.lockated.Interfaces.IRecyclerItemClickListener;
import com.android.lockated.R;
import com.android.lockated.categories.reminder.model.ReminderData;
import com.android.lockated.holder.RecyclerViewHolder;

import java.util.ArrayList;

public class ReminderRowAdapter extends RecyclerView.Adapter<RecyclerViewHolder> {
    private ArrayList<ReminderData> mReminderDataList;
    private IRecyclerItemClickListener mIRecyclerItemClickListener;

    public ReminderRowAdapter(ArrayList<ReminderData> data, IRecyclerItemClickListener listener) {
        mReminderDataList = data;
        mIRecyclerItemClickListener = listener;
    }

    @Override
    public RecyclerViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.fragment_reminder_row, parent, false);
        return new RecyclerViewHolder(itemView, mIRecyclerItemClickListener);
    }

    @Override
    public void onBindViewHolder(RecyclerViewHolder holder, int position) {
        holder.onReminderViewHolder(mReminderDataList.get(position));
    }

    @Override
    public int getItemCount() {
        return mReminderDataList.size();
    }
}
