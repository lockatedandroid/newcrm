package com.android.lockated.crm.adapters;


import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.android.lockated.crm.fragment.polls.CrmPollListFragment;
import com.android.lockated.crm.fragment.events.AllEventFragment;
import com.android.lockated.crm.fragment.notices.NoticeFragment;

public class CrmPagerAdapter extends FragmentPagerAdapter {

    int tabCount;
    FragmentManager fragmentManager;

    public CrmPagerAdapter(FragmentManager fm, int tabCount) {
        super(fm);
        this.tabCount = tabCount;
        fragmentManager = fm;
    }

    @Override
    public Fragment getItem(int pos) {

        Fragment fragment;

        switch (pos) {

            case 0:
                fragment = new NoticeFragment();
                break;
            case 1:
                fragment = new AllEventFragment();
                break;
            case 2:
                fragment = new CrmPollListFragment();
                break;
            default:
                fragment = new NoticeFragment();
                break;

        }

        return fragment;
    }

    @Override
    public int getCount() {
        return tabCount;
    }

    @Override
    public int getItemPosition(Object object) {
        return super.getItemPosition(object);
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return super.getPageTitle(position);
    }
}