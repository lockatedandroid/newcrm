
package com.android.lockated.model.usermodel.myGroups;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


public class Society implements Parcelable {

    @SerializedName("id")
    @Expose
    private int id;
    @SerializedName("building_name")
    @Expose
    private String buildingName;
    @SerializedName("url")
    @Expose
    private String url;
    @SerializedName("address1")
    @Expose
    private String address1;
    @SerializedName("address2")
    @Expose
    private String address2;
    @SerializedName("area")
    @Expose
    private String area;
    @SerializedName("postcode")
    @Expose
    private int postcode;
    @SerializedName("city")
    @Expose
    private String city;
    @SerializedName("latitude")
    @Expose
    private String latitude;
    @SerializedName("longitude")
    @Expose
    private String longitude;
    @SerializedName("state")
    @Expose
    private String state;
    @SerializedName("country")
    @Expose
    private String country;

    /**
     * @return The id
     */
    public int getId() {
        return id;
    }

    /**
     * @param id The id
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * @return The buildingName
     */
    public String getBuildingName() {
        return buildingName;
    }

    /**
     * @param buildingName The building_name
     */
    public void setBuildingName(String buildingName) {
        this.buildingName = buildingName;
    }

    /**
     * @return The url
     */
    public String getUrl() {
        return url;
    }

    /**
     * @param url The url
     */
    public void setUrl(String url) {
        this.url = url;
    }

    /**
     * @return The address1
     */
    public String getAddress1() {
        return address1;
    }

    /**
     * @param address1 The address1
     */
    public void setAddress1(String address1) {
        this.address1 = address1;
    }

    /**
     * @return The address2
     */
    public String getAddress2() {
        return address2;
    }

    /**
     * @param address2 The address2
     */
    public void setAddress2(String address2) {
        this.address2 = address2;
    }

    /**
     * @return The area
     */
    public String getArea() {
        return area;
    }

    /**
     * @param area The area
     */
    public void setArea(String area) {
        this.area = area;
    }

    /**
     * @return The postcode
     */
    public int getPostcode() {
        return postcode;
    }

    /**
     * @param postcode The postcode
     */
    public void setPostcode(int postcode) {
        this.postcode = postcode;
    }

    /**
     * @return The city
     */
    public String getCity() {
        return city;
    }

    /**
     * @param city The city
     */
    public void setCity(String city) {
        this.city = city;
    }

    /**
     * @return The latitude
     */
    public String getLatitude() {
        return latitude;
    }

    /**
     * @param latitude The latitude
     */
    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    /**
     * @return The longitude
     */
    public String getLongitude() {
        return longitude;
    }

    /**
     * @param longitude The longitude
     */
    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    /**
     * @return The state
     */
    public String getState() {
        return state;
    }

    /**
     * @param state The state
     */
    public void setState(String state) {
        this.state = state;
    }

    /**
     * @return The country
     */
    public String getCountry() {
        return country;
    }

    /**
     * @param country The country
     */
    public void setCountry(String country) {
        this.country = country;
    }

    @Override
    public int describeContents() {
        return this.hashCode();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(id);
        dest.writeString(buildingName);
        dest.writeString(url);
        dest.writeString(address1);
        dest.writeString(address2);
        dest.writeString(area);
        dest.writeInt(postcode);
        dest.writeString(city);
        dest.writeString(latitude);
        dest.writeString(longitude);
        dest.writeString(state);
        dest.writeString(country);
    }

    private Society(Parcel in) {
        id = in.readInt();
        buildingName = in.readString();
        url = in.readString();
        address1 = in.readString();
        address2 = in.readString();
        area = in.readString();
        postcode = in.readInt();
        city = in.readString();
        latitude = in.readString();
        longitude = in.readString();
        state = in.readString();
        country = in.readString();
    }

    public static final Parcelable.Creator<Society> CREATOR = new Parcelable.Creator<Society>() {
        public Society createFromParcel(Parcel in) {
            return new Society(in);
        }

        public Society[] newArray(int size) {
            return new Society[size];
        }
    };

}
