
package com.android.lockated.model.AdminModel.AdminPolls.PendingPolls;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class PendingPolls {

    @SerializedName("polls")
    @Expose
    private ArrayList<PendingPoll> polls = new ArrayList<PendingPoll>();

    /**
     * 
     * @return
     *     The polls
     */
    public ArrayList<PendingPoll> getPolls() {
        return polls;
    }

    /**
     * 
     * @param polls
     *     The polls
     */
    public void setPolls(ArrayList<PendingPoll> polls) {
        this.polls = polls;
    }

}
