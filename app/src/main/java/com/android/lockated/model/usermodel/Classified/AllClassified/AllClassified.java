
package com.android.lockated.model.usermodel.Classified.AllClassified;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class AllClassified {

    @SerializedName("classifieds")
    @Expose
    private ArrayList<AllClassifiedList> classifieds = new ArrayList<AllClassifiedList>();

    /**
     * @return The classifieds
     */
    public ArrayList<AllClassifiedList> getClassifieds() {
        return classifieds;
    }

    /**
     * @param classifieds The classifieds
     */
    public void setClassifieds(ArrayList<AllClassifiedList> classifieds) {
        this.classifieds = classifieds;
    }

}
