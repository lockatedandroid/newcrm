package com.android.lockated.drawer;

import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.android.lockated.LockatedApplication;
import com.android.lockated.R;

public class PoliciesActivity extends AppCompatActivity implements View.OnClickListener {
    private WebView mWebViewPolicies;
    private ProgressBar mPBarPolicies;
    private LinearLayout mLLPoliciesError;

    private String mURL;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_policies);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(true);

        init();
        setViewData();
    }

    private void init() {
        mURL = ((LockatedApplication) getApplicationContext()).getSectionURL();
        String mName = ((LockatedApplication) getApplicationContext()).getSectionName();

        mPBarPolicies = (ProgressBar) findViewById(R.id.mPBarPolicies);
        mWebViewPolicies = (WebView) findViewById(R.id.mWebViewPolicies);
        mLLPoliciesError = (LinearLayout) findViewById(R.id.mLLPoliciesError);
        TextView mTextViewLoadPolicesRetry = (TextView) findViewById(R.id.mTextViewLoadPolicesRetry);

        mWebViewPolicies.getSettings().setJavaScriptEnabled(true);

        getSupportActionBar().setTitle(mName);
        mTextViewLoadPolicesRetry.setOnClickListener(this);

        mWebViewPolicies.loadUrl(mURL);
    }

    private void setViewData() {
        mWebViewPolicies.setWebViewClient(new WebViewClient() {
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                if (url.startsWith("mailto:")) {
                    url = url.replaceFirst("mailto:", "");
                    url = url.trim();
                    Intent intent = new Intent(Intent.ACTION_SEND);
                    intent.setType("message/rfc822");
                    intent.putExtra(Intent.EXTRA_EMAIL, new String[]{getResources().getString(R.string.contact_us_email)});
                    intent.putExtra(Intent.EXTRA_SUBJECT, getResources().getString(R.string.contact_us_subject));
                    startActivity(Intent.createChooser(intent, getResources().getString(R.string.contact_us_message)));
                    return true;
                }
                view.loadUrl(url);
                return true;
            }

            @Override
            public void onPageStarted(WebView view, String url, Bitmap favicon) {
                super.onPageStarted(view, url, favicon);
                hideWebViewContent();
            }

            @Override
            public void onPageFinished(WebView view, String url) {
                super.onPageFinished(view, url);
                showWebViewContent();
            }

            @Override
            public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
                super.onReceivedError(view, errorCode, description, failingUrl);
                showErrorLayout();
            }
        });
    }

    private void showWebViewContent() {
        mWebViewPolicies.setVisibility(View.VISIBLE);
        mPBarPolicies.setVisibility(View.GONE);
        mLLPoliciesError.setVisibility(View.GONE);
    }

    private void hideWebViewContent() {
        mWebViewPolicies.setVisibility(View.GONE);
        mPBarPolicies.setVisibility(View.VISIBLE);
        mLLPoliciesError.setVisibility(View.GONE);
    }

    private void showErrorLayout() {
        mLLPoliciesError.setVisibility(View.VISIBLE);
        mWebViewPolicies.setVisibility(View.GONE);
        mPBarPolicies.setVisibility(View.GONE);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home: {
                PoliciesActivity.this.finish();
                return true;
            }
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.mTextViewLoadPolicesRetry:
                mWebViewPolicies.loadUrl(mURL);
                break;
        }
    }
}
