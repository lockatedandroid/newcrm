package com.android.lockated.crm.detailView;


import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.lockated.R;
import com.android.lockated.crm.activity.FacilitiesActivity;
import com.android.lockated.network.ConnectionDetector;
import com.android.lockated.preferences.LockatedPreferences;
import com.android.lockated.request.LockatedRequestError;
import com.android.lockated.request.LockatedVolleyRequestQueue;
import com.android.lockated.utils.ApplicationURL;
import com.android.lockated.utils.DatePickerFragment;
import com.android.lockated.utils.TimePickerFragment;
import com.android.lockated.utils.Utilities;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

public class FacilitiesDetailView extends DialogFragment implements View.OnClickListener,
        Response.Listener<JSONObject>, Response.ErrorListener {
    private TextView mTextStartdate;
    private TextView mTextEndDate;
    private TextView mTextStartTime;
    private TextView mTextEndTime;
    private TextView costType;
    private TextView mUpdate;
    private ImageView mIageStartDate;
    private ImageView mImageEndDate;
    private ImageView mImageStartTIme;
    private ImageView mImageEndTime;
    private ProgressBar progressBarSmall;
    TextView venueName, costValue, block;
    EditText/* duration,*/ purpose, persons;
    Window window;
    private View DetailsFullViewView;
    LockatedPreferences mLockatedPreferences;
    String BOOK_FACILITIES = "FacilitiesDetailView";

    @Override
    public void onStart() {
        super.onStart();

        window = getDialog().getWindow();
        WindowManager.LayoutParams windowParams = window.getAttributes();
        windowParams.dimAmount = 0.75f;
        windowParams.flags |= WindowManager.LayoutParams.FLAG_DIM_BEHIND;
        window.setAttributes(windowParams);
        window.setBackgroundDrawableResource(android.R.color.transparent);

    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        DetailsFullViewView = getActivity().getLayoutInflater().inflate(R.layout.fragment_facilities_view,
                new LinearLayout(getActivity()), false);

        init(DetailsFullViewView);
        Dialog builder = new Dialog(getActivity());
        builder.requestWindowFeature(Window.FEATURE_NO_TITLE);
        builder.getWindow().setBackgroundDrawable(new ColorDrawable(Color.WHITE));

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(builder.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.WRAP_CONTENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        builder.show();
        builder.getWindow().setAttributes(lp);
        builder.setContentView(DetailsFullViewView);

        return builder;

    }

    private void init(View view) {

        progressBarSmall = (ProgressBar) view.findViewById(R.id.mProgressBarSmallView);
        mTextStartdate = (TextView) view.findViewById(R.id.mTextStartdate);
        mTextEndDate = (TextView) view.findViewById(R.id.mTextEndDate);
        mTextStartTime = (TextView) view.findViewById(R.id.mTextStartTime);
        mTextEndTime = (TextView) view.findViewById(R.id.mTextEndTime);
        venueName = (TextView) view.findViewById(R.id.venuTxt);
        costValue = (TextView) view.findViewById(R.id.costValue);
        block = (TextView) view.findViewById(R.id.blockTextOption);
        mUpdate = (TextView) view.findViewById(R.id.mUpdate);
        costType = (TextView) view.findViewById(R.id.costType);
        mIageStartDate = (ImageView) view.findViewById(R.id.mIageStartDate);
        mImageEndDate = (ImageView) view.findViewById(R.id.mImageEndDate);
        mImageStartTIme = (ImageView) view.findViewById(R.id.mImageStartTIme);
        mImageEndTime = (ImageView) view.findViewById(R.id.mImageEndTime);
        //   duration = (EditText) view.findViewById(R.id.daysNumber);
        purpose = (EditText) view.findViewById(R.id.purposeEdit);
        persons = (EditText) view.findViewById(R.id.personEdit);

        if (getArguments() != null) {
            venueName.setText(getArguments().getString("name"));
            costValue.setText(getArguments().getString("cost"));
            costType.setText(getArguments().getString("cost_type"));
        }
        String valueId = getArguments().getString("valueId");
        if (valueId.equals("0")) {
            block.setVisibility(View.VISIBLE);
            mUpdate.setVisibility(View.GONE);
        } else {

            block.setVisibility(View.GONE);
            mUpdate.setVisibility(View.VISIBLE);
            purpose.setText(getArguments().getString("book_purpose"));
            persons.setText(getArguments().getString("person_no"));

            String strStartDate = getArguments().getString("startdate");

            if (strStartDate.equals("null")) {
                mTextStartdate.setText("Start Date");
                mTextStartTime.setText("Start Time");
            } else {
                mTextStartdate.setText(Utilities.ddMMYYYFormat(strStartDate));
                //String time = (checkIfToday(strStartDate));
                String time = Utilities.timeAmPm(strStartDate);
                String[] separate = strStartDate.split("T");
                mTextStartTime.setText(time);

            }
            String ExitDate = getArguments().getString("enddate");
            if (ExitDate.equals("null")) {
                mTextEndDate.setText("End Date");
                mTextEndTime.setText("End Time");
            } else {
                mTextEndDate.setText(ddMMYYYFormat(ExitDate));
                //String time = (checkIfToday(ExitDate));
                String time = Utilities.timeAmPm(ExitDate);
                String[] separated = ExitDate.split("T");
                mTextEndTime.setText(time);
            }

        }
        mIageStartDate.setOnClickListener(this);
        mImageEndDate.setOnClickListener(this);
        mImageStartTIme.setOnClickListener(this);
        mImageEndTime.setOnClickListener(this);
        block.setOnClickListener(this);
        mUpdate.setOnClickListener(this);
        mLockatedPreferences = new LockatedPreferences(getActivity());

    }

    public String checkIfToday(String todayOrNot) {

        if (todayOrNot != null) {
            try {
                Calendar c = Calendar.getInstance();
                SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
                String formattedDate = df.format(c.getTime());
                Date currentDate = df.parse(formattedDate);
                Date respDate = df.parse(todayOrNot);

                /*if (currentDate.equals(respDate)) {
                    todayOrNot = todayOrNot.substring(todayOrNot.indexOf("T") + 1, todayOrNot.length());
                    Log.e("if", ""+todayOrNot);
                } else {*/
                todayOrNot = Utilities.ddMonTimeFormat(todayOrNot);
                //}
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }

        return todayOrNot;

    }

    private String ddMMYYYFormat(String strEntryDate) {
        if (strEntryDate != "null") {
            DateFormat format = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);
            Date date = null;
            try {
                date = format.parse(strEntryDate);
            } catch (ParseException e) {
                e.printStackTrace();
            }
            strEntryDate = new SimpleDateFormat("dd-MM-yyyy").format(date);
        } else {
            strEntryDate = "No Date";
        }
        return strEntryDate;

    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {

            case R.id.mIageStartDate:
                openDatePicker(mTextStartdate);
                break;
            case R.id.mImageEndDate:
                openDatePicker(mTextEndDate);
                break;
            case R.id.mImageStartTIme:
                openTimePicker(mTextStartTime);
                break;
            case R.id.mImageEndTime:
                openTimePicker(mTextEndTime);
                break;
            case R.id.blockTextOption:
                if (/*duration.getText().toString().length() > 0
                            &&*/ purpose.getText().toString().length() > 0
                        && persons.getText().toString().length() > 0
                        && !mTextStartdate.getText().toString().equals("Start Date")
                        && !mTextEndDate.getText().toString().equals("End Date")
                        && !mTextStartTime.getText().toString().equals("Start Time")
                        && !mTextEndTime.getText().toString().equals("End Time")) {
                    bookFacility();
                } else {
                  Utilities.showToastMessage(getActivity(), "Please fill all details");
                }
                break;
            case R.id.mUpdate:
                if (/*duration.getText().toString().length() > 0
                            &&*/ purpose.getText().toString().length() > 0
                        && persons.getText().toString().length() > 0
                        && !mTextStartdate.getText().toString().equals("Start Date")
                        && !mTextEndDate.getText().toString().equals("End Date")
                        && !mTextStartTime.getText().toString().equals("Start Time")
                        && !mTextEndTime.getText().toString().equals("End Time")) {
                    updateFacility();
                } else {
                    Utilities.showToastMessage(getActivity(), "Please fill all details");
                }
                break;
        }
    }


    private void updateFacility() {
        if (getArguments() != null) {
            progressBarSmall.setVisibility(View.VISIBLE);
            String pid = getArguments().getString("id");
            if (ConnectionDetector.isConnectedToInternet(getActivity())) {
                JSONObject jsonObject = new JSONObject();
                JSONObject jsonObjectMain = new JSONObject();
                try {
                    //  jsonObject.put("book_duration", duration.getText().toString());
                    jsonObject.put("book_purpose", purpose.getText().toString());
                    jsonObject.put("person_no", persons.getText().toString());
                    jsonObject.put("startdate", mTextStartdate.getText().toString() + "T" + mTextStartTime.getText().toString() + "+05:30");
                    jsonObject.put("enddate", mTextEndDate.getText().toString() + "T" + mTextEndTime.getText().toString() + "+05:30");
                    jsonObject.put("facility_id", getArguments().getString("facility_id"));
                    jsonObject.put("society_id", mLockatedPreferences.getSocietyId());
                    jsonObjectMain.put("facility_booking", jsonObject);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                String url = ApplicationURL.UpdateBooking + pid + ".json?token=" + mLockatedPreferences.getLockatedToken();
                LockatedVolleyRequestQueue mLockatedVolleyRequestQueue = LockatedVolleyRequestQueue.getInstance(getActivity());
                mLockatedVolleyRequestQueue.sendRequest(BOOK_FACILITIES, Request.Method.PUT, url, jsonObjectMain, this, this);
            } else {
                Utilities.showToastMessage(getActivity(), getActivity().getResources().getString(R.string.internet_connection_error));
            }
        }
    }

    private void openTimePicker(TextView selectDate) {
        TimePickerFragment timePickerFragment = new TimePickerFragment();
        timePickerFragment.setTimePickerView(selectDate);
        DialogFragment newFragment = timePickerFragment;
        newFragment.show(getActivity().getSupportFragmentManager(), "TimePicker");
    }

    private void openDatePicker(TextView selectTime) {
        DatePickerFragment datePickerFragment = new DatePickerFragment();
        datePickerFragment.setDatePickerView(selectTime);
        DialogFragment newFragment = datePickerFragment;
        newFragment.show(getActivity().getSupportFragmentManager(), "DatePicker");

    }

    public void bookFacility() {
        if (getArguments() != null) {
            progressBarSmall.setVisibility(View.VISIBLE);
            if (ConnectionDetector.isConnectedToInternet(getActivity())) {
                JSONObject jsonObject = new JSONObject();
                JSONObject jsonObjectMain = new JSONObject();
                try {
                    //  jsonObject.put("book_duration", duration.getText().toString());
                    jsonObject.put("book_purpose", purpose.getText().toString());
                    jsonObject.put("person_no", persons.getText().toString());
                    jsonObject.put("startdate", mTextStartdate.getText().toString() + "T" + mTextStartTime.getText().toString() + "+05:30");
                    jsonObject.put("enddate", mTextEndDate.getText().toString() + "T" + mTextEndTime.getText().toString() + "+05:30");
                    jsonObject.put("facility_id", getArguments().getString("facility_id"));
                    jsonObject.put("user_society_id", "" + mLockatedPreferences.getUserSocietyId());
                    jsonObject.put("society_id", mLockatedPreferences.getSocietyId());
                    jsonObjectMain.put("facility_booking", jsonObject);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                String url = ApplicationURL.bookFacility + mLockatedPreferences.getLockatedToken();
                LockatedVolleyRequestQueue mLockatedVolleyRequestQueue = LockatedVolleyRequestQueue.getInstance(getActivity());
                mLockatedVolleyRequestQueue.sendRequest(BOOK_FACILITIES, Request.Method.POST, url, jsonObjectMain, this, this);
            } else {
                Utilities.showToastMessage(getActivity(), getActivity().getResources().getString(R.string.internet_connection_error));
            }
        }
    }

    @Override
    public void onResponse(JSONObject response) {
        if (response.has("book_duration")) {
            progressBarSmall.setVisibility(View.GONE);
            Intent intent = new Intent(getActivity(), FacilitiesActivity.class);
            intent.putExtra("FacilitiesActivity", 1);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            getActivity().startActivity(intent);
            dismiss();
        }
    }

    @Override
    public void onErrorResponse(VolleyError error) {
        if (getActivity() != null) {
            LockatedRequestError.onRequestError(getActivity(), error);
        }
    }
}
