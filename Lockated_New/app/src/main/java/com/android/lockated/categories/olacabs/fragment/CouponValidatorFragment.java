package com.android.lockated.categories.olacabs.fragment;

import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentTransaction;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.android.lockated.R;

/**
 * Created by HAVEN INFOLINE on 7/4/2016.
 */
public class CouponValidatorFragment extends DialogFragment implements  View.OnClickListener, TextWatcher {

    private static final String REQUEST_TAG = "OLA CABS CONFIRM";
    private EditText edt_coupon;
    private LinearLayout coupon_apply;
    private TextView cancel_validator,coupon_message;
    private ProgressBar mCoupon_progress;
    private boolean validResponse;

    String access_token;
    String category;
    String pasteData;
    private FragmentTransaction fragmentTransaction;

    @Override
    public void onStart() {
        super.onStart();
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(getDialog().getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        getDialog().getWindow().setAttributes(lp);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(STYLE_NO_TITLE, 0);
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_coupon_validator, container);
        init(view);
        return view;
    }

    private void init(View view) {

        Bundle bundle = getArguments();
        if (bundle != null) {
            access_token = bundle.getString("ola_token");
            category = bundle.getString("category");
        }
        edt_coupon = (EditText) view.findViewById(R.id.edt_coupon);
        mCoupon_progress = (ProgressBar) view.findViewById(R.id.mCoupon_progress);
        coupon_apply = (LinearLayout) view.findViewById(R.id.coupon_apply);
        cancel_validator = (TextView) view.findViewById(R.id.cancel_validator);
        coupon_message = (TextView) view.findViewById(R.id.coupon_message);

        if (edt_coupon.getText().length()==0)
        {
            coupon_apply.setEnabled(false);
            coupon_apply.setBackgroundColor(Color.parseColor("#ffdbdb"));
        }
        /*else
        {
            coupon_apply.setBackgroundColor(getActivity().getResources().getColor(R.color.wine));
            coupon_apply.setEnabled(true);
        }*/


        coupon_apply.setOnClickListener(this);
        cancel_validator.setOnClickListener(this);
        edt_coupon.addTextChangedListener(this);
    }


    @Override
    public void onClick(View v) {
        switch (v.getId())
        {

            case R.id.coupon_apply:
                mCoupon_progress.setVisibility(View.VISIBLE);
                coupon_message.setVisibility(View.GONE);
                validate();
                break;

            case R.id.cancel_validator:
                dismiss();
                break;
        }
    }


    private void validate()
    {
        if(edt_coupon.getText().length()!=0)
        {
            coupon_message.setVisibility(View.VISIBLE);
            mCoupon_progress.setVisibility(View.GONE);
            coupon_message.setText("Coupon is not valid at Lockated");
            coupon_message.setTextColor(Color.parseColor("#000000"));
        }
        else {
            coupon_message.setVisibility(View.VISIBLE);
            mCoupon_progress.setVisibility(View.GONE);
            coupon_message.setText("Please Enter Coupon");
            coupon_message.setTextColor(Color.parseColor("#cc0000"));
        }
    }




    public void pastedata() {
        ClipboardManager clipboard = (ClipboardManager) getActivity().getSystemService(Context.CLIPBOARD_SERVICE);
        ClipData.Item item = clipboard.getPrimaryClip().getItemAt(0);
        pasteData = (String) item.getText();

        edt_coupon.setText(pasteData);
        ConfirmOlaBookingFragment.coupon_code = pasteData;
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {
        coupon_message.setVisibility(View.GONE);
        mCoupon_progress.setVisibility(View.GONE);
        if (edt_coupon.getText().length()==0)
        {
            coupon_apply.setBackgroundColor(Color.parseColor("#ffa8a8"));
            coupon_apply.setEnabled(false);
        }
        else
        {
            coupon_apply.setBackgroundColor(getActivity().getResources().getColor(R.color.wine));
            coupon_apply.setEnabled(true);
        }
    }

    @Override
    public void afterTextChanged(Editable s) {

    }

    /*
    public void sendConfirmation() {
        try {
            if (ConnectionDetector.isConnectedToInternet(getActivity())) {
                String url = "http://sandbox-t.olacabs.com//v1/coupons/validate?coupon_code=" + pasteData + "&category=" + category;

                JSONObject jsonObject = new JSONObject();
                jsonObject.put("category", category);
                jsonObject.put("coupon_code",pasteData);

                LockatedVolleyRequestQueue mLockatedVolleyRequestQueue = LockatedVolleyRequestQueue.getInstance(getActivity());
                mLockatedVolleyRequestQueue.sendPostRequestOlaCabsConfirmEx(REQUEST_TAG, Request.Method.POST, url, access_token, jsonObject, this,this);

            } else {
                Utilities.showToastMessage(getActivity(), getActivity().getResources().getString(R.string.internet_connection_error));
            }

        } catch (Exception e) {
            e.getMessage();
        }

    }
    */

}
