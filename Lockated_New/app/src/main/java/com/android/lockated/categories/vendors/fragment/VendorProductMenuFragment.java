package com.android.lockated.categories.vendors.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.android.lockated.R;
import com.android.lockated.categories.adapter.VendorMenuTabAdapter;
import com.android.lockated.categories.vendors.model.VendorProductData;
import com.android.lockated.component.LockatedPagerSlidingTabStrip;
import com.android.lockated.utils.Utilities;

import java.util.ArrayList;

public class VendorProductMenuFragment extends Fragment {
    private View mCategoryView;
    private ViewPager mViewPagerDetail;
    private LockatedPagerSlidingTabStrip mViewPagerSlidingTabs;
    private VendorMenuTabAdapter mVendorMenuTabAdapter;

    private int mPosition;
    String screenName = "Vendor Product List";
    private ArrayList<VendorProductData> mVendorProductDataList;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mCategoryView = inflater.inflate(R.layout.fragment_account, container, false);
        Utilities.ladooIntegration(getActivity(), screenName);
        Utilities.lockatedGoogleAnalytics(screenName, getString(R.string.visited), screenName);
        init();
        return mCategoryView;
    }

    public void setVendorMenuData(ArrayList<VendorProductData> productData, int pos) {
        mVendorProductDataList = productData;
        mPosition = pos;
    }

    private void init() {
        mViewPagerDetail = (ViewPager) mCategoryView.findViewById(R.id.mViewPagerDetail);
        mViewPagerSlidingTabs = (LockatedPagerSlidingTabStrip) mCategoryView.findViewById(R.id.mViewPagerSlidingTabs);

        mViewPagerSlidingTabs.setIndicatorColor(ContextCompat.getColor(getActivity(), R.color.primary));
        mViewPagerSlidingTabs.setTextColor(ContextCompat.getColor(getActivity(), R.color.primary_text));
        mViewPagerSlidingTabs.setActivateTextColor(ContextCompat.getColor(getActivity(), R.color.primary_text));
        mViewPagerSlidingTabs.setDeactivateTextColor(ContextCompat.getColor(getActivity(), R.color.primary_text));

        if (mVendorProductDataList != null && mVendorProductDataList.size() > 0) {
            mVendorMenuTabAdapter = new VendorMenuTabAdapter(getChildFragmentManager(), mVendorProductDataList);
            mViewPagerDetail.setAdapter(mVendorMenuTabAdapter);
            mViewPagerDetail.setCurrentItem(mPosition);
            mViewPagerSlidingTabs.setViewPager(mViewPagerDetail);
        }
    }
}