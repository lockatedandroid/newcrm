package com.android.lockated.model;

import org.json.JSONObject;

public class FamilyData
{
    private int id;
    private int id_user;
    private String gender;
    private String name;
    private String relationship;
    private String mobile;
    private String dob;
    private String married;
    private String anniversary;

    public FamilyData(JSONObject jsonObject)
    {
        id = jsonObject.optInt("id");
        id_user = jsonObject.optInt("id_user");
        gender = jsonObject.optString("gender");
        name = jsonObject.optString("name");
        relationship = jsonObject.optString("relationship");
        mobile = jsonObject.optString("mobile");
        dob = jsonObject.optString("dob");
        married = jsonObject.optString("married");
        anniversary = jsonObject.optString("anniversary");
    }

    public int getId()
    {
        return id;
    }

    public int getId_user()
    {
        return id_user;
    }

    public String getGender()
    {
        return gender;
    }

    public String getRelationship()
    {
        return relationship;
    }

    public String getMobile()
    {
        return mobile;
    }

    public String getDob()
    {
        return dob;
    }

    public String getMarried()
    {
        return married;
    }

    public String getAnniversary()
    {
        return anniversary;
    }

    public String getName()
    {
        return name;
    }

    public void setId(int id)
    {
        this.id = id;
    }

    public void setId_user(int id_user)
    {
        this.id_user = id_user;
    }

    public void setGender(String gender)
    {
        this.gender = gender;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public void setRelationship(String relationship)
    {
        this.relationship = relationship;
    }

    public void setMobile(String mobile)
    {
        this.mobile = mobile;
    }

    public void setDob(String dob)
    {
        this.dob = dob;
    }

    public void setMarried(String married)
    {
        this.married = married;
    }

    public void setAnniversary(String anniversary)
    {
        this.anniversary = anniversary;
    }
}
