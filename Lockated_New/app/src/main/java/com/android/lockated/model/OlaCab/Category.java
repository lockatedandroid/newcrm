
package com.android.lockated.model.OlaCab;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class Category {

    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("display_name")
    @Expose
    private String displayName;
    @SerializedName("currency")
    @Expose
    private String currency;
    @SerializedName("distance_unit")
    @Expose
    private String distanceUnit;
    @SerializedName("time_unit")
    @Expose
    private String timeUnit;
    @SerializedName("eta")
    @Expose
    private int eta;
    @SerializedName("distance")
    @Expose
    private String distance;
    @SerializedName("image")
    @Expose
    private String image;
    @SerializedName("fare_breakup")
    @Expose
    private ArrayList<FareBreakup> fareBreakup = new ArrayList<FareBreakup>();

    /**
     * 
     * @return
     *     The id
     */
    public String getId() {
        return id;
    }

    /**
     * 
     * @param id
     *     The id
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * 
     * @return
     *     The displayName
     */
    public String getDisplayName() {
        return displayName;
    }

    /**
     * 
     * @param displayName
     *     The display_name
     */
    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    /**
     * 
     * @return
     *     The currency
     */
    public String getCurrency() {
        return currency;
    }

    /**
     * 
     * @param currency
     *     The currency
     */
    public void setCurrency(String currency) {
        this.currency = currency;
    }

    /**
     * 
     * @return
     *     The distanceUnit
     */
    public String getDistanceUnit() {
        return distanceUnit;
    }

    /**
     * 
     * @param distanceUnit
     *     The distance_unit
     */
    public void setDistanceUnit(String distanceUnit) {
        this.distanceUnit = distanceUnit;
    }

    /**
     * 
     * @return
     *     The timeUnit
     */
    public String getTimeUnit() {
        return timeUnit;
    }

    /**
     * 
     * @param timeUnit
     *     The time_unit
     */
    public void setTimeUnit(String timeUnit) {
        this.timeUnit = timeUnit;
    }

    /**
     * 
     * @return
     *     The eta
     */
    public int getEta() {
        return eta;
    }

    /**
     * 
     * @param eta
     *     The eta
     */
    public void setEta(int eta) {
        this.eta = eta;
    }

    /**
     * 
     * @return
     *     The distance
     */
    public String getDistance() {
        return distance;
    }

    /**
     * 
     * @param distance
     *     The distance
     */
    public void setDistance(String distance) {
        this.distance = distance;
    }

    /**
     * 
     * @return
     *     The image
     */
    public String getImage() {
        return image;
    }

    /**
     * 
     * @param image
     *     The image
     */
    public void setImage(String image) {
        this.image = image;
    }

    /**
     * 
     * @return
     *     The fareBreakup
     */
    public ArrayList<FareBreakup> getFareBreakup() {
        return fareBreakup;
    }

    /**
     * 
     * @param fareBreakup
     *     The fare_breakup
     */
    public void setFareBreakup(ArrayList<FareBreakup> fareBreakup) {
        this.fareBreakup = fareBreakup;
    }

}
