package com.android.lockated.categories;

import android.Manifest;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Settings;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.android.lockated.ChatSupportActivity;
import com.android.lockated.Interfaces.ILocationUpdateListener;
import com.android.lockated.Interfaces.IRecyclerItemClickListener;
import com.android.lockated.LockatedApplication;
import com.android.lockated.R;
import com.android.lockated.categories.adapter.BannerAdapter;
import com.android.lockated.categories.adapter.CategoryItemAdapter;
import com.android.lockated.categories.beauty.BeautyActivity;
import com.android.lockated.categories.beauty.BeautyAppointmentActivity;
import com.android.lockated.categories.grocery.GroceryActivity;
import com.android.lockated.categories.medicine.MedicineActivity;
import com.android.lockated.categories.miscellaneous.MiscellaneousActivity;
import com.android.lockated.categories.reminder.ReminderActivity;
import com.android.lockated.categories.services.ServiceActivity;
import com.android.lockated.categories.vendors.VendorActivity;
import com.android.lockated.crm.fragment.RoadBlockDialogFragment;
import com.android.lockated.information.AccountController;
import com.android.lockated.landing.model.TaxonData;
import com.android.lockated.location.LocationUpdateManager;
import com.android.lockated.model.AccountData;
import com.android.lockated.model.Banner.Banner;
import com.android.lockated.network.ConnectionDetector;
import com.android.lockated.preferences.LockatedPreferences;
import com.android.lockated.request.LockatedRequestError;
import com.android.lockated.request.LockatedVolleyRequestQueue;
import com.android.lockated.utils.ApplicationURL;
import com.android.lockated.utils.Utilities;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.google.android.gms.common.ConnectionResult;
import com.library.zomato.ordering.common.OrderSDK;
import com.library.zomato.ordering.common.OrderSDKConfig;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

public class CategoryItemViewFragment extends Fragment implements ILocationUpdateListener, IRecyclerItemClickListener,
        Response.Listener<JSONObject>, Response.ErrorListener, ViewPager.OnPageChangeListener {

    private View mCategoryView;
    private int mDrawableResource;
    private ArrayList<TaxonData> mTaxonDataList;

    private CategoryItemAdapter mCategoryItemAdapter;

    //----------------Zomato-------------------------------
    private String zomato_token = null;
    private AccountController accountController;
    private ArrayList<AccountData> accountDataArrayList;
    private LocationUpdateManager mLocationUpdateManager;
    private static final int REQUEST_LOCATION = 10;
    private double mLatitude;
    private double mLongitude;
    private boolean hasResetLocation;
    private boolean isLocationAvailable;
    //-----------------------------------------------------


    BannerAdapter bannerAdapter;
    ArrayList<Banner> bannerArrayList;
    Bundle bundle;
    String name, headerName;
    boolean displayDF = true;
    ViewPager mImageViewCategoryItem;
    LockatedPreferences mLockatedPreferences;
    String GET_BANNER = "CategoryItemViewFragment";
    RoadBlockDialogFragment roadBlockDialogFragment;
    LinearLayout pager_indicator;
    private int dotsCount;
    private ImageView[] dots;
    int pos;
    int NUM_PAGES;
    int currentPage;
    int taxon_id, otype_id;
    Runnable Update;
    Handler handler;
    Timer swipeTimer;
    boolean insideForLoop = true;
    boolean timerNotCancelled = true;
    String departmentName;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        accountController = AccountController.getInstance();
        accountDataArrayList = accountController.getmAccountDataList();
        mLocationUpdateManager = new LocationUpdateManager(getActivity(), this);


        //Thread.setDefaultUncaughtExceptionHandler(new DefaultExceptionHandler(getActivity()));
        mCategoryView = inflater.inflate(R.layout.fragment_category_view, container, false);
        bundle = getArguments();
        Utilities.ladooIntegration(getActivity(), getString(R.string.category_screen));
        Utilities.lockatedGoogleAnalytics(getString(R.string.category_screen), getString(R.string.visited),
                getString(R.string.category_screen));
        roadBlockDialogFragment = new RoadBlockDialogFragment();
        roadBlockDialogFragment.setRoadBlockDialogFragment(getActivity());
        init(mCategoryView);
        return mCategoryView;
    }

    public void setTaxonData(ArrayList<TaxonData> mTaxonomiesData, int drawable, String name,
                             int pos, String headerName, int taxon_id, int otype_id) {

        mTaxonDataList = mTaxonomiesData;
        mDrawableResource = drawable;
        this.pos = pos;
        this.name = name;
        this.headerName = headerName;
        this.taxon_id = taxon_id;
        this.otype_id = otype_id;
    }

    private void init(View mCategoryView) {

        handler = new Handler();
        swipeTimer = new Timer();
        bannerArrayList = new ArrayList<>();
        mLockatedPreferences = new LockatedPreferences(getActivity());
        RecyclerView mRecyclerView = (RecyclerView) mCategoryView.findViewById(R.id.mRecyclerView);

        mImageViewCategoryItem = (ViewPager) mCategoryView.findViewById(R.id.mImageViewCategoryItem);
        mImageViewCategoryItem.setOnPageChangeListener(this);
        bannerAdapter = new BannerAdapter(getActivity(), bannerArrayList, false, name);
        mImageViewCategoryItem.setAdapter(bannerAdapter);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        mRecyclerView.setLayoutManager(mLayoutManager);
        mRecyclerView.setHasFixedSize(true);
        mCategoryItemAdapter = new CategoryItemAdapter(mTaxonDataList, this, "CategoryItemViewFragment");
        mRecyclerView.setAdapter(mCategoryItemAdapter);
        pager_indicator = (LinearLayout) mCategoryView.findViewById(R.id.viewPagerCountDots);
        getBannerImages(mTaxonDataList);
        if (taxon_id != 666 && taxon_id != 0) {
            int posi = findProductPosition(taxon_id, otype_id);
            if (posi != 666 && name.equals(departmentName)) {
                onOffersView(posi);
            }
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        handler = new Handler();
        swipeTimer = new Timer();
        if (name != null) {
            if (name.equals("Grocery") && pos == 1 && bundle.getInt("currentTapPosition") == 1) {
                displayDialogFrag();
            } else if (name.equals("Grocery") && pos == 1 && bundle.getInt("currentTapPosition") == 2) {
                displayDialogFrag();
            } else if (name.equals("Grocery") && pos == 1 && bundle.getInt("currentTapPosition") == 2) {
                displayDialogFrag();
            } else if (pos == 0 && bundle.getInt("currentTapPosition") == 2) {
                displayDialogFrag();
            } else if (pos == 2 && bundle.getInt("currentTapPosition") == 0) {
                displayDialogFrag();
            } else if (pos == 4 && bundle.getInt("currentTapPosition") == 0) {
                displayDialogFrag();
            } else if (pos == 3 && bundle.getInt("currentTapPosition") == 0) {
                displayDialogFrag();
            } else if (pos == 5 && bundle.getInt("currentTapPosition") == 0) {
                displayDialogFrag();
            } else if (pos == 6 && bundle.getInt("currentTapPosition") == 0) {
                displayDialogFrag();
            } else if (pos == 7 && bundle.getInt("currentTapPosition") == 0) {
                displayDialogFrag();
            } else if (pos == 8 && bundle.getInt("currentTapPosition") == 0) {
                displayDialogFrag();
            } else if (pos == 5 && bundle.getInt("currentTapPosition") == 1) {
            } else if (pos == 4 && bundle.getInt("currentTapPosition") == 2) {
            } else {
            }
        }
    }

    public void displayDialogFrag() {
        if (displayDF) {
            displayDF = false;
            roadBlockDialogFragment.show(getChildFragmentManager(), "Alert Dialog Fragment");
        }
    }

    @Override
    public void onRecyclerItemClick(View view, int position) {
        switch (mTaxonDataList.get(position).getTaxonomy_id()) {
            case 1:
                onLaundryDetailClicked(position);
                break;
            case 2:
                onBeautyDetailClicked(position);
                break;
            case 6:
                onGroceryDetailClicked(position);
                break;
            case 8:
                onMedicineDetailClicked(position);
                break;
            case 10:
                onHouseHoldDetailClicked(position);
                break;
            case 18:
                onFoodDetailClicked(position);
                break;
            case 28:
                onRepairDetailClicked(position);
                break;
            case 31:
                onMiscDetailClicked(position);
                break;
            case 33:
                onReminderDetailClicked(position);
                break;
        }
    }

    public int findProductPosition(int taxon_id, int otype_id) {
        int position = 666;
        try {
            JSONArray jsonArray = mLockatedPreferences.getTaxonomyData();
            for (int i = 0; i < jsonArray.length(); i++) {
                JSONObject jsonObject1 = jsonArray.getJSONObject(i);
                if (insideForLoop) {
                    if (jsonObject1.has("id") && jsonObject1.getInt("id") == otype_id) {
                        if (jsonObject1.has("root") && jsonObject1.getInt("id") == otype_id) {
                            if (insideForLoop) {
                                if (jsonObject1.getJSONObject("root").has("taxons")) {
                                    departmentName = jsonObject1.getJSONObject("root").getString("name");
                                    JSONArray jsonArray1 = jsonObject1.getJSONObject("root").getJSONArray("taxons");
                                    for (int j = 0; j < jsonArray1.length(); j++) {
                                        if (insideForLoop) {
                                            if (jsonArray1.getJSONObject(j).getInt("id") == taxon_id) {
                                                position = j;
                                                insideForLoop = false;
                                                break;
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return position;
    }

    private void onOffersView(int position) {
        switch (mTaxonDataList.get(position).getTaxonomy_id()) {
            case 1:
                notifyLaundryDetailClicked(position);
                break;
            case 2:
                notifyBeautyDetailClicked(position);
                break;
            case 6:
                notifyGroceryDetailClicked(position);
                break;
            case 8:
                notifyMedicineDetailClicked(position);
                break;
            case 10:
                notifyHouseHoldDetailClicked(position);
                break;
            case 18:
                notifyFoodDetailClicked(position);
                break;
            case 28:
                notifyRepairDetailClicked(position);
                break;
            case 31:
                notifyMiscDetailClicked(position);
                break;
            case 33:
                notifyReminderDetailClicked(position);
                break;
            default:
                break;
        }
    }

    private void onBeautyDetailClicked(int position) {
        int size = mTaxonDataList.size();
        if (size > 0) {
            if (size - 1 == position) {
                onChatSupportClicked(4, position);
            } else if (position == 0) {
                Intent groceryIntent = new Intent(getActivity(), BeautyActivity.class);
                ((LockatedApplication) getActivity().getApplicationContext()).setHeaderName(getString(R.string.beauty));
                ((LockatedApplication) getActivity().getApplicationContext()).setmSectionId(mTaxonDataList.get(position).getId());
                startActivity(groceryIntent);
            } else if (position == 1) {
                Intent groceryIntent = new Intent(getActivity(), BeautyActivity.class);
                ((LockatedApplication) getActivity().getApplicationContext()).setHeaderName(getString(R.string.beauty));
                ((LockatedApplication) getActivity().getApplicationContext()).setmSectionId(mTaxonDataList.get(position).getId());
                startActivity(groceryIntent);
            } else {
                Intent groceryIntent = new Intent(getActivity(), BeautyAppointmentActivity.class);
                ((LockatedApplication) getActivity().getApplicationContext()).setHeaderName(getString(R.string.beauty));
                ((LockatedApplication) getActivity().getApplicationContext()).setTaxonomyId(mTaxonDataList.get(position).getTaxonomy_id());
                ((LockatedApplication) getActivity().getApplicationContext()).setTaxonId(mTaxonDataList.get(position).getId());
                startActivity(groceryIntent);
            }
        }
    }

    private void onGroceryDetailClicked(int position) {
        int size = mTaxonDataList.size();
        if (size > 0) {
            if (size - 1 == position) {
                onChatSupportClicked(3, position);
            } else {
                Intent groceryIntent = new Intent(getActivity(), GroceryActivity.class);
                ((LockatedApplication) getActivity().getApplicationContext()).setmSectionId(position);
                ((LockatedApplication) getActivity().getApplicationContext()).setHeaderName("Grocery");
                ((LockatedApplication) getActivity().getApplicationContext()).setTaxonDatas(mTaxonDataList);
                startActivity(groceryIntent);
            }
        }
    }

    private void onFoodDetailClicked(int position) {
        int size = mTaxonDataList.size();
        if (size > 0) if (size - 1 == position) {
            onChatSupportClicked(1, position);
        } else {
            //--------------------------Lockated Food Ecommerce-------------------------

                /*Intent groceryIntent = new Intent(getActivity(), VendorActivity.class);
                ((LockatedApplication) getActivity().getApplicationContext()).setHeaderName("Food");
                ((LockatedApplication) getActivity().getApplicationContext()).setCategoryId(mTaxonDataList.get(position).getTaxonomy_id());
                startActivity(groceryIntent);*/

            //---------------------------------------------------------------------------

            checkLocationPermission();
        }
    }

    private void onReminderDetailClicked(int position) {
        int size = mTaxonDataList.size();
        if (size > 0) {
            if (size - 1 == position) {
                onChatSupportClicked(9, position);
            } else {
                Intent groceryIntent = new Intent(getActivity(), ReminderActivity.class);
                ((LockatedApplication) getActivity().getApplicationContext()).setHeaderName("Reminder");
                startActivity(groceryIntent);
            }
        }
    }

    private void onMiscDetailClicked(int position) {
        int size = mTaxonDataList.size();
        if (size > 0) {
            if (size - 1 == position) {
                onChatSupportClicked(2, position);
            } else {
                Intent groceryIntent = new Intent(getActivity(), MiscellaneousActivity.class);
                ((LockatedApplication) getActivity().getApplicationContext()).setHeaderName("Miscellaneous");
                ((LockatedApplication) getActivity().getApplicationContext()).setCategoryId(mTaxonDataList.get(position).getId());
                ((LockatedApplication) getActivity().getApplicationContext()).setCategoryIdTwo(mTaxonDataList.get(position).getTaxonomy_id());
                startActivity(groceryIntent);
            }
        }
    }

    private void onRepairDetailClicked(int position) {
        int size = mTaxonDataList.size();
        if (size > 0) {
            if (size - 1 == position) {
                onChatSupportClicked(5, position);
            } else if (position == 1) {
                Intent groceryIntent = new Intent(getActivity(), ServiceActivity.class);
                ((LockatedApplication) getActivity().getApplicationContext()).setHeaderName(mTaxonDataList.get(position).getName());
                ((LockatedApplication) getActivity().getApplicationContext()).setCategoryIdTwo(1);
                ((LockatedApplication) getActivity().getApplicationContext()).setCategoryId(mTaxonDataList.get(position).getId());
                startActivity(groceryIntent);
            } else {
                Intent groceryIntent = new Intent(getActivity(), ServiceActivity.class);
                ((LockatedApplication) getActivity().getApplicationContext()).setHeaderName(mTaxonDataList.get(position).getName());
                ((LockatedApplication) getActivity().getApplicationContext()).setCategoryIdTwo(0);
                ((LockatedApplication) getActivity().getApplicationContext()).setCategoryId(mTaxonDataList.get(position).getId());
                startActivity(groceryIntent);
            }
        }
    }

    private void onHouseHoldDetailClicked(int position) {
        int size = mTaxonDataList.size();
        if (size > 0) {
            if (size - 1 == position) {
                onChatSupportClicked(6, position);
            } else {
                Intent groceryIntent = new Intent(getActivity(), ServiceActivity.class);
                ((LockatedApplication) getActivity().getApplicationContext()).setHeaderName("Household Help");
                ((LockatedApplication) getActivity().getApplicationContext()).setCategoryIdTwo(0);
                ((LockatedApplication) getActivity().getApplicationContext()).setCategoryId(mTaxonDataList.get(position).getId());
                startActivity(groceryIntent);
            }
        }
    }

    private void onMedicineDetailClicked(int position) {
        int size = mTaxonDataList.size();
        if (size > 0) {
            if (size - 1 == position) {
                onChatSupportClicked(7, position);
            } else {
                Intent groceryIntent = new Intent(getActivity(), MedicineActivity.class);
                ((LockatedApplication) getActivity().getApplicationContext()).setHeaderName("Medicine");
                ((LockatedApplication) getActivity().getApplicationContext()).setCategoryId(mTaxonDataList.get(position).getId());
                ((LockatedApplication) getActivity().getApplicationContext()).setCategoryIdTwo(mTaxonDataList.get(position).getTaxonomy_id());
                startActivity(groceryIntent);
            }
        }
    }

    private void onLaundryDetailClicked(int position) {
        int size = mTaxonDataList.size();
        if (size > 0) {
            if (size - 1 == position) {
                onChatSupportClicked(8, position);
            } else {
                Intent groceryIntent = new Intent(getActivity(), VendorActivity.class);
                ((LockatedApplication) getActivity().getApplicationContext()).setHeaderName("Laundry");
                ((LockatedApplication) getActivity().getApplicationContext()).setCategoryId(mTaxonDataList.get(position).getTaxonomy_id());
                startActivity(groceryIntent);
            }
        }
    }

    //==================================================================================================
    private void notifyBeautyDetailClicked(int position) {
        if (position == 0) {
            Intent groceryIntent = new Intent(getActivity(), BeautyActivity.class);
            ((LockatedApplication) getActivity().getApplicationContext()).setHeaderName(getString(R.string.beauty));
            ((LockatedApplication) getActivity().getApplicationContext()).setmSectionId(mTaxonDataList.get(position).getId());
            startActivity(groceryIntent);
        } else if (position == 1) {
            Intent groceryIntent = new Intent(getActivity(), BeautyActivity.class);
            ((LockatedApplication) getActivity().getApplicationContext()).setHeaderName(getString(R.string.beauty));
            ((LockatedApplication) getActivity().getApplicationContext()).setmSectionId(mTaxonDataList.get(position).getId());
            startActivity(groceryIntent);
        } else {
            Intent groceryIntent = new Intent(getActivity(), BeautyAppointmentActivity.class);
            ((LockatedApplication) getActivity().getApplicationContext()).setHeaderName(getString(R.string.beauty));
            ((LockatedApplication) getActivity().getApplicationContext()).setTaxonomyId(mTaxonDataList.get(position).getTaxonomy_id());
            ((LockatedApplication) getActivity().getApplicationContext()).setTaxonId(mTaxonDataList.get(position).getId());
            startActivity(groceryIntent);
        }
    }

    private void notifyGroceryDetailClicked(int position) {
        Intent groceryIntent = new Intent(getActivity(), GroceryActivity.class);
        ((LockatedApplication) getActivity().getApplicationContext()).setmSectionId(position);
        ((LockatedApplication) getActivity().getApplicationContext()).setHeaderName("Grocery");
        ((LockatedApplication) getActivity().getApplicationContext()).setTaxonDatas(mTaxonDataList);
        startActivity(groceryIntent);
    }

    private void notifyFoodDetailClicked(int position) {
        Intent groceryIntent = new Intent(getActivity(), VendorActivity.class);
        ((LockatedApplication) getActivity().getApplicationContext()).setHeaderName("Food");
        ((LockatedApplication) getActivity().getApplicationContext()).setCategoryId(mTaxonDataList.get(position).getTaxonomy_id());
        startActivity(groceryIntent);
    }

    private void notifyReminderDetailClicked(int position) {
        Intent groceryIntent = new Intent(getActivity(), ReminderActivity.class);
        ((LockatedApplication) getActivity().getApplicationContext()).setHeaderName("Reminder");
        startActivity(groceryIntent);
    }

    private void notifyMiscDetailClicked(int position) {
        Intent groceryIntent = new Intent(getActivity(), MiscellaneousActivity.class);
        ((LockatedApplication) getActivity().getApplicationContext()).setHeaderName("Miscellaneous");
        ((LockatedApplication) getActivity().getApplicationContext()).setCategoryId(mTaxonDataList.get(position).getId());
        ((LockatedApplication) getActivity().getApplicationContext()).setCategoryIdTwo(mTaxonDataList.get(position).getTaxonomy_id());
        startActivity(groceryIntent);
    }

    private void notifyRepairDetailClicked(int position) {
        if (position == 1) {
            Intent groceryIntent = new Intent(getActivity(), ServiceActivity.class);
            ((LockatedApplication) getActivity().getApplicationContext()).setHeaderName(mTaxonDataList.get(position).getName());
            ((LockatedApplication) getActivity().getApplicationContext()).setCategoryIdTwo(1);
            ((LockatedApplication) getActivity().getApplicationContext()).setCategoryId(mTaxonDataList.get(position).getId());
            startActivity(groceryIntent);
        } else {
            Intent groceryIntent = new Intent(getActivity(), ServiceActivity.class);
            ((LockatedApplication) getActivity().getApplicationContext()).setHeaderName(mTaxonDataList.get(position).getName());
            ((LockatedApplication) getActivity().getApplicationContext()).setCategoryIdTwo(0);
            ((LockatedApplication) getActivity().getApplicationContext()).setCategoryId(mTaxonDataList.get(position).getId());
            startActivity(groceryIntent);
        }
    }

    private void notifyHouseHoldDetailClicked(int position) {
        Intent groceryIntent = new Intent(getActivity(), ServiceActivity.class);
        ((LockatedApplication) getActivity().getApplicationContext()).setHeaderName("Household Help");
        ((LockatedApplication) getActivity().getApplicationContext()).setCategoryIdTwo(0);
        ((LockatedApplication) getActivity().getApplicationContext()).setCategoryId(mTaxonDataList.get(position).getId());
        startActivity(groceryIntent);
    }

    private void notifyMedicineDetailClicked(int position) {
        Intent groceryIntent = new Intent(getActivity(), MedicineActivity.class);
        ((LockatedApplication) getActivity().getApplicationContext()).setHeaderName("Medicine");
        ((LockatedApplication) getActivity().getApplicationContext()).setCategoryId(mTaxonDataList.get(position).getId());
        ((LockatedApplication) getActivity().getApplicationContext()).setCategoryIdTwo(mTaxonDataList.get(position).getTaxonomy_id());
        startActivity(groceryIntent);
    }

    private void notifyLaundryDetailClicked(int position) {
        Intent groceryIntent = new Intent(getActivity(), VendorActivity.class);
        ((LockatedApplication) getActivity().getApplicationContext()).setHeaderName("Laundry");
        ((LockatedApplication) getActivity().getApplicationContext()).setCategoryId(mTaxonDataList.get(position).getTaxonomy_id());
        startActivity(groceryIntent);
    }
//==================================================================================================

    private void onChatSupportClicked(int id, int position) {
        Intent intent = new Intent(getActivity(), ChatSupportActivity.class);
        ((LockatedApplication) getActivity().getApplicationContext()).setmSectionId(id);
        ((LockatedApplication) getActivity().getApplicationContext()).setSectionName(name);
        startActivity(intent);
    }

    private void getBannerImages(ArrayList<TaxonData> mTaxonDataList) {
        if (getActivity() != null) {
            if (ConnectionDetector.isConnectedToInternet(getActivity())) {
                if (mTaxonDataList != null && mTaxonDataList.size() > 0) {
                    String url = ApplicationURL.getCategoryBanner + mTaxonDataList.get(0).getTaxonomy_id()
                            + "&token=" + mLockatedPreferences.getLockatedToken();
                    LockatedVolleyRequestQueue lockatedVolleyRequestQueue = LockatedVolleyRequestQueue.getInstance(getActivity());
                    lockatedVolleyRequestQueue.sendRequest(GET_BANNER, Request.Method.GET, url, null, this, this);
                }
            } else {
                Utilities.showToastMessage(getActivity(), getActivity().getResources().getString(R.string.internet_connection_error));
            }
        }
    }

    @Override
    public void onResponse(JSONObject response) {
        if (getActivity() != null) {
            try {
                if (response.has("banners") && response.getJSONArray("banners").length() > 0) {
                    JSONArray jsonArray = response.getJSONArray("banners");
                    for (int i = 0; i < jsonArray.length(); i++) {
                        Banner banner = new Banner(jsonArray.getJSONObject(i));
                        bannerArrayList.add(banner);
                    }
                    bannerAdapter.notifyDataSetChanged();
                    NUM_PAGES = jsonArray.length() + 1;
                    Update = new Runnable() {
                        public void run() {
                            if (currentPage == NUM_PAGES - 1) {
                                currentPage = 0;
                            }
                            mImageViewCategoryItem.setCurrentItem(currentPage++, true);
                        }
                    };
                    if (timerNotCancelled) {
                        swipeTimer.schedule(new TimerTask() {
                            @Override
                            public void run() {
                                handler.post(Update);
                            }
                        }, NUM_PAGES * 10000, 5000);
                        swipeTimer = new Timer(); //This is new
                        swipeTimer.schedule(new TimerTask() {
                            @Override
                            public void run() {
                                handler.post(Update);
                            }
                        }, 0, NUM_PAGES * 2000); // execute in every 15sec
                        setUiPageViewController(jsonArray.length());
                    }
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

    }

    private void setUiPageViewController(int count) {
        dotsCount = count;
        dots = new ImageView[dotsCount];
        for (int i = 0; i < dotsCount; i++) {
            dots[i] = new ImageView(getActivity());
            dots[i].setImageDrawable(getActivity().getResources().getDrawable(R.drawable.circle_128));
            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(15,
                    LinearLayout.LayoutParams.WRAP_CONTENT);
            params.setMargins(1, 0, 1, 0);
            pager_indicator.addView(dots[i], params);
        }
        dots[0].setImageDrawable(getActivity().getResources().getDrawable(R.drawable.bullet));
    }

    @Override
    public void onErrorResponse(VolleyError error) {
        if (getActivity() != null) {
            LockatedRequestError.onRequestError(getActivity(), error);
        }
    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }

    @Override
    public void onPageSelected(int position) {
        if (getActivity() != null) {
            for (int i = 0; i < dotsCount; i++) {
                dots[i].setImageDrawable(getActivity().getResources().getDrawable(R.drawable.circle_128));
            }
            dots[position].setImageDrawable(getActivity().getResources().getDrawable(R.drawable.bullet));
        }
    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }

    @Override
    public void onPause() {
        super.onPause();
        swipeTimer.cancel();
        timerNotCancelled = false;
        handler.removeCallbacks(Update);
    }


    //-------------------------Zomato Changes------------------------------

    private void checkLocationPermission() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION)
                    != PackageManager.PERMISSION_GRANTED) {
                requestPermissions(new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, REQUEST_LOCATION);
            } else {
                initializeLocationUpdater();
                //zomatoSdkOpen();
            }
        } else {
            initializeLocationUpdater();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == REQUEST_LOCATION) {
            if (grantResults.length == 1 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                if (mLocationUpdateManager.getGoogleApiClient() == null)
                {

                    if (Utilities.isPlayServiceAvailable(getActivity()))
                    {
                        mLocationUpdateManager.buildGoogleApiClient();
                        zomatoSdkOpen();
                    } else {
                        Utilities.serviceUpdateError(getActivity(), "Google Play Service is not Updated. Would you like to update?");
                    }

                    //zomatoSdkOpen();
                    //initializeLocationUpdater();
                }
            }
        }
    }

    private void initializeLocationUpdater() {
        LocationManager mLocationManager = (LocationManager) getActivity().getSystemService(Context.LOCATION_SERVICE);
        if (mLocationManager.isProviderEnabled(LocationManager.GPS_PROVIDER))
        {
            Log.e("mLocationUpdateManager",""+mLocationUpdateManager.getGoogleApiClient());
            if (mLocationUpdateManager.getGoogleApiClient() == null) {
                if (Utilities.isPlayServiceAvailable(getActivity())) {
                    if (hasResetLocation) {
                        mLocationUpdateManager.buildGoogleApiClient(true);
                        zomatoSdkOpen();
                    } else {
                        mLocationUpdateManager.buildGoogleApiClient();
                        zomatoSdkOpen();
                    }
                } else {
                    Utilities.serviceUpdateError(getActivity(), "Google Play Service is not Updated. Would you like to update?");
                }
            }
            else
            {
                zomatoSdkOpen();
            }

        } else {
            if (getActivity() != null) {
                /*Utilities.alertGPSConnectionError(getActivity());*/
                alertGPSConnectionError(getActivity());

            }
        }
    }

    public void alertGPSConnectionError(final Context context) {
        final AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setMessage(context.getString(R.string.gps_disabled))
                .setCancelable(false)
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    public void onClick(final DialogInterface dialog, final int id) {
                        ((LockatedApplication) context.getApplicationContext()).setIsGPSEnabled(true);
                        dialog.cancel();
                        context.startActivity(new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS));

                      //  zomatoSdkOpen();

                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    public void onClick(final DialogInterface dialog, final int id) {
                        dialog.cancel();
                    }
                });
        final AlertDialog alert = builder.create();
        alert.show();
    }

    private void zomatoSdkOpen() {
        OrderSDKConfig.allowLocationSearch(true);
        Log.e("size", "" + accountController.getmAccountDataList().size());
        Log.e("zom", "" + mLockatedPreferences.getZomatoToken());
        Log.e("accountController data", "" + accountController.getmAccountDataList().toString());
        Log.e("Zomato Token", "" + accountController.getmAccountDataList().get(0).getZomatotoken());
        Log.e("FirstName", "" + accountController.getmAccountDataList().get(0).getFirstname());
        Log.e("Email", "" + accountController.getmAccountDataList().get(0).getEmail());
        Log.e("Id", "" + accountController.getmAccountDataList().get(0).getId());
        Log.e("Mobile", "" + accountController.getmAccountDataList().get(0).getMobile());
        //this.zomato_token = accountController.getmAccountDataList().get(0).getZomatotoken();

        this.zomato_token = mLockatedPreferences.getZomatoToken();

        Log.e("zomato token", zomato_token + "");

        try {
            if (!this.zomato_token.equals("null")) {
                Log.e("Token Exist", "YES :" + zomato_token);
                OrderSDK.setUserToken(zomato_token);
                OrderSDK.startOnlineOrdering(getActivity());
            } else {
                Log.e("Token Exist", "NO :" + "Creates New");
                OrderSDK.startOnlineOrdering(getActivity());
            }
        } catch (Exception e) {
            Log.e("Exception", "" + e.getMessage());
        }

    }

    @Override
    public void onSuccessUpdate(Bundle bundle) {
        hasResetLocation = false;
    }

    @Override
    public void onLocationUpdate(Location location) {
        if (getActivity() != null) {
            hasResetLocation = false;
            mLatitude = location.getLatitude();
            mLongitude = location.getLongitude();
            isLocationAvailable = true;
            getActivity().invalidateOptionsMenu();
            OrderSDK.setCoordinates(mLatitude, mLongitude);
        }
    }

    @Override
    public void onFailedUpdate(ConnectionResult connectionResult) {
        hasResetLocation = false;
        Utilities.showToastMessage(getActivity(), "onFailedUpdate: " + connectionResult);
    }
    //-------------------------------------------------------------------------
}