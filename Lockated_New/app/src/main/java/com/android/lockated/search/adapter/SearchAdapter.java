package com.android.lockated.search.adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.lockated.R;
import com.android.lockated.utils.ApplicationURL;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;

public class SearchAdapter extends BaseAdapter implements Filterable {

    private ArrayList<String> nameList;
    Context context;
    LayoutInflater layoutInflater;

    public SearchAdapter(Context context, ArrayList<String> nameList) {
        this.context = context;
        this.nameList = nameList;
        layoutInflater = LayoutInflater.from(context);
    }

    @Override
    public int getCount() {
        return nameList.size();
    }

    @Override
    public String getItem(int position) {
        return nameList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder;
        if (convertView == null) {
            convertView = layoutInflater.inflate(R.layout.google_list_item, parent, false);
            viewHolder = new ViewHolder();
            viewHolder.imageView = (ImageView) convertView.findViewById(R.id.imageName);
            viewHolder.textView = (TextView) convertView.findViewById(R.id.locationText);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }
        viewHolder.imageView.setImageResource(R.drawable.arrow);
        viewHolder.textView.setText(nameList.get(position));

        return convertView;
    }

    class ViewHolder {
        ImageView imageView;
        TextView textView;
    }

    @Override
    public Filter getFilter() {
        Filter filter = new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence constraint) {
                FilterResults filterResults = new FilterResults();
                if (constraint != null) {
                    //nameList = autocomplete(constraint.toString());
                    filterResults.values = nameList;
                    filterResults.count = nameList.size();
                }
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {
                if (results != null && results.count > 0) {
                    notifyDataSetChanged();
                } else {
                    notifyDataSetInvalidated();
                }
            }
        };
        return filter;
    }

    public static ArrayList<String> autocomplete(String input) {
        ArrayList<String> resultList = null;

        HttpURLConnection conn = null;
        StringBuilder jsonResults = new StringBuilder();
        try {
            StringBuilder sb = new StringBuilder(ApplicationURL.PLACES_API_BASE
                    + ApplicationURL.TYPE_AUTOCOMPLETE + ApplicationURL.OUT_JSON);
            sb.append("?key=" + ApplicationURL.Server_KEY);
            sb.append("&components=country:in");
            sb.append("&components=administrative_area_level_1:MH");
            sb.append("&components=administrative_area_level_3:MUM");
            sb.append("&input=" + URLEncoder.encode(input, "utf8"));
            URL url = new URL(sb.toString());
            //Log.e("autocomplete", "" + url);
            conn = (HttpURLConnection) url.openConnection();
            InputStreamReader in = new InputStreamReader(conn.getInputStream());
            int read;
            char[] buff = new char[1024];
            while ((read = in.read(buff)) != -1) {
                jsonResults.append(buff, 0, read);
            }
        } catch (MalformedURLException e) {
            //Log.e(GET_REQUEST_TAG, "Error processing Places API URL", e);
            return resultList;
        } catch (IOException e) {
            //Log.e(GET_REQUEST_TAG, "Error connecting to Places API", e);
            return resultList;
        } finally {
            if (conn != null) {
                conn.disconnect();
            }
        }
        try {
            JSONObject jsonObj = new JSONObject(jsonResults.toString());
            JSONArray predsJsonArray = jsonObj.getJSONArray("predictions");
            resultList = new ArrayList<>(predsJsonArray.length());
            for (int i = 0; i < predsJsonArray.length(); i++) {
                /*System.out.println(predsJsonArray.getJSONObject(i).getString("description"));
                System.out.println("============================================================");*/
                String removeStateCountry;
                if (predsJsonArray.getJSONObject(i).getString("description").contains(", Maharashtra, India")) {
                    removeStateCountry = predsJsonArray.getJSONObject(i).getString("description")
                            .replaceAll(", Maharashtra, India", "");
                } else if (predsJsonArray.getJSONObject(i).getString("description").contains(", Maharashtra, India")) {
                    removeStateCountry = predsJsonArray.getJSONObject(i).getString("description")
                            .replaceAll(", Mumbai, Maharashtra, India", "");
                } else {
                    removeStateCountry = predsJsonArray.getJSONObject(i).getString("description");
                }
                resultList.add(removeStateCountry);
            }
        } catch (JSONException e) {
            //Log.e(GET_REQUEST_TAG, "Cannot process JSON results", e);
        }

        return resultList;
    }

}
