package com.android.lockated.model;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class AccountData {
    private String email;
    private String firstname;
    private String lastname;
    private String mobile;
    private String id;
    private int number_verified;
    private int selected_user_society;

    //------------------Ola Changes-------------------------
    private String olatoken;
    //------------------------------------------------------

    //------------------Zomato Changes----------------------
    private String zomatotoken;
    //------------------------------------------------------

    public boolean is_approve;

    private ArrayList<FamilyData> mFamilyDataList = new ArrayList<>();
    private ArrayList<MyAddressData> mMyAddressDataList = new ArrayList<>();
    private ArrayList<MyOrderData> mMyOrderList = new ArrayList<>();
    private ArrayList<ServantData> mServantDataList = new ArrayList<>();
    private ArrayList<GeneralDetailData> mGeneralDetailDataList = new ArrayList<>();
    private ArrayList<ResidenceData> mResidenceDataList = new ArrayList<>();
    private ArrayList<CartDetail> mCartDetailList = new ArrayList<>();

    public AccountData(JSONObject jsonObject) {
        try {
            email = jsonObject.optString("email");
            firstname = jsonObject.optString("firstname");
            lastname = jsonObject.optString("lastname");
            mobile = jsonObject.optString("mobile");
            number_verified = jsonObject.optInt("number_verified");
            is_approve = jsonObject.getBoolean("is_approve");
            id = jsonObject.getString("id");
            selected_user_society = jsonObject.optInt("selected_user_society");
            //------------ola changes---------------------
            olatoken = jsonObject.getString("olatoken");
            //---------------------------------------------

            //-------------zomato changes-------------------
            zomatotoken = jsonObject.getString("zomatotoken");
            //-----------------------------------------------

            JSONArray familyJsonArray = jsonObject.getJSONArray("family");
            JSONArray addressesJsonArray = jsonObject.getJSONArray("addresses");
            JSONArray orderJsonArray = jsonObject.getJSONArray("orders");
            JSONArray servantJsonArray = jsonObject.getJSONArray("servants");
            JSONArray generalJsonArray = jsonObject.getJSONArray("general_details");
            JSONArray residenceJsonArray = jsonObject.getJSONArray("residences");

            for (int i = 0; i < familyJsonArray.length(); i++) {
                JSONObject familyObject = familyJsonArray.getJSONObject(i);
                FamilyData familyData = new FamilyData(familyObject);
                mFamilyDataList.add(familyData);
            }

            for (int j = 0; j < addressesJsonArray.length(); j++) {
                JSONObject addressObject = addressesJsonArray.getJSONObject(j);
                MyAddressData myAddressData = new MyAddressData(addressObject);
                mMyAddressDataList.add(myAddressData);
            }

            for (int k = 0; k < orderJsonArray.length(); k++) {
                JSONObject orderObject = orderJsonArray.getJSONObject(k);
                MyOrderData myOrderData = new MyOrderData(orderObject);
                mMyOrderList.add(myOrderData);
            }

            for (int l = 0; l < servantJsonArray.length(); l++) {
                JSONObject servantObject = servantJsonArray.getJSONObject(l);
                ServantData servantData = new ServantData(servantObject);
                mServantDataList.add(servantData);
            }

            for (int m = 0; m < generalJsonArray.length(); m++) {
                JSONObject generalObject = generalJsonArray.getJSONObject(m);
                GeneralDetailData generalData = new GeneralDetailData(generalObject);
                mGeneralDetailDataList.add(generalData);
            }

            for (int n = 0; n < residenceJsonArray.length(); n++) {
                JSONObject residenceObject = residenceJsonArray.getJSONObject(n);
                ResidenceData residenceData = new ResidenceData(residenceObject);
                mResidenceDataList.add(residenceData);
            }

            JSONObject cartObject = new JSONObject(jsonObject.optString("cart"));
            if (cartObject.length() > 0) {
                CartDetail cartDetail = new CartDetail(cartObject);
                mCartDetailList.add(cartDetail);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public String getEmail() {
        return email;
    }

    public String getFirstname() {
        return firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public String getMobile() {
        return mobile;
    }

    public int getNumber_verified() {
        return number_verified;
    }

    public ArrayList<FamilyData> getmFamilyDataList() {
        return mFamilyDataList;
    }

    public ArrayList<MyAddressData> getmMyAddressDataList() {
        return mMyAddressDataList;
    }

    public ArrayList<MyOrderData> getmMyOrderList() {
        return mMyOrderList;
    }

    public void setmMyAddressDataList(ArrayList<MyAddressData> mMyAddressDataList) {
        this.mMyAddressDataList = mMyAddressDataList;
    }

    public ArrayList<ServantData> getmServantDataList() {
        return mServantDataList;
    }

    public ArrayList<GeneralDetailData> getmGeneralDetailDataList() {
        return mGeneralDetailDataList;
    }

    public ArrayList<ResidenceData> getmResidenceDataList() {
        return mResidenceDataList;
    }

    public void setmFamilyDataList(ArrayList<FamilyData> mFamilyDataList) {
        this.mFamilyDataList = mFamilyDataList;
    }

    public void setmServantDataList(ArrayList<ServantData> mServantDataList) {
        this.mServantDataList = mServantDataList;
    }

    public ArrayList<CartDetail> getCartDetailList() {
        return mCartDetailList;
    }

    public void setCartDetailList(ArrayList<CartDetail> mCartDetailList) {
        this.mCartDetailList = mCartDetailList;
    }

    public boolean is_approve() {
        return is_approve;
    }

    public void setIs_approve(boolean is_approve) {
        this.is_approve = is_approve;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public int getSelected_user_society() {
        return selected_user_society;
    }

    public void setSelected_user_society(int selected_user_society) {
        this.selected_user_society = selected_user_society;
    }

    //---------------------Ola changes---------------------------------
    public String getOlaToken() {return olatoken;}

    public void setOlaToken(String olatoken) {this.olatoken = olatoken;}
    //-----------------------------------------------------------------

    //---------------------Zomato Token--------------------------------
    public String getZomatotoken() {
        return zomatotoken;
    }
    public void setZomatotoken(String zomatotoken) {
        this.zomatotoken = zomatotoken;
    }
    //------------------------------------------------------------------
}
