package com.android.lockated.crm.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.android.lockated.R;

import org.json.JSONArray;

public class SocietyDirectoryAdapter extends BaseAdapter {
    Context contextMain;
    JSONArray jsonArray;
    LayoutInflater layoutInflater;

    public SocietyDirectoryAdapter(Context context, JSONArray jsonArray) {
        this.contextMain = context;
        this.jsonArray = jsonArray;
        layoutInflater = LayoutInflater.from(context);
    }

    @Override
    public int getCount() {
        return jsonArray.length();
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    private class ViewHolder {
        TextView mTextNumber;
        TextView mEmail_Id;
        TextView mTextDirectoryName;
        TextView mServiceType;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        ViewHolder holder = null;
        if (convertView == null) {

            convertView = layoutInflater.inflate(R.layout.socitychild, parent, false);
            holder = new ViewHolder();
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        holder.mTextDirectoryName = (TextView) convertView.findViewById(R.id.mTextDirectoryName);
        holder.mTextNumber = (TextView) convertView.findViewById(R.id.mTextNumber);
        holder.mEmail_Id = (TextView) convertView.findViewById(R.id.mEmail_Id);
        holder.mServiceType = (TextView) convertView.findViewById(R.id.mServiceType);
        try {
            holder.mTextDirectoryName.setText(jsonArray.getJSONObject(position).getString("firstname"));
            holder.mTextNumber.setText(jsonArray.getJSONObject(position).getString("mobile"));
            holder.mEmail_Id.setText(jsonArray.getJSONObject(position).getString("email"));
            holder.mServiceType.setText(jsonArray.getJSONObject(position).getString("nature"));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return convertView;
    }
}