
package com.android.lockated.model.zomato;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Total {

    @SerializedName("Tax_id")
    @Expose
    private int taxId;
    @SerializedName("Item_name")
    @Expose
    private String itemName;
    @SerializedName("Quantity")
    @Expose
    private int quantity;
    @SerializedName("Item_id")
    @Expose
    private int itemId;
    @SerializedName("Tag_ids")
    @Expose
    private String tagIds;
    @SerializedName("Display_cost")
    @Expose
    private String displayCost;
    @SerializedName("Type")
    @Expose
    private String type;
    @SerializedName("Old_total_cost")
    @Expose
    private int oldTotalCost;
    @SerializedName("Total_cost")
    @Expose
    private float totalCost;
    @SerializedName("Unit_cost")
    @Expose
    private float unitCost;
    @SerializedName("Choice_text")
    @Expose
    private String choiceText;

    /**
     * 
     * @return
     *     The taxId
     */
    public int getTaxId() {
        return taxId;
    }

    /**
     * 
     * @param taxId
     *     The Tax_id
     */
    public void setTaxId(int taxId) {
        this.taxId = taxId;
    }

    /**
     * 
     * @return
     *     The itemName
     */
    public String getItemName() {
        return itemName;
    }

    /**
     * 
     * @param itemName
     *     The Item_name
     */
    public void setItemName(String itemName) {
        this.itemName = itemName;
    }

    /**
     * 
     * @return
     *     The quantity
     */
    public int getQuantity() {
        return quantity;
    }

    /**
     * 
     * @param quantity
     *     The Quantity
     */
    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    /**
     * 
     * @return
     *     The itemId
     */
    public int getItemId() {
        return itemId;
    }

    /**
     * 
     * @param itemId
     *     The Item_id
     */
    public void setItemId(int itemId) {
        this.itemId = itemId;
    }

    /**
     * 
     * @return
     *     The tagIds
     */
    public String getTagIds() {
        return tagIds;
    }

    /**
     * 
     * @param tagIds
     *     The Tag_ids
     */
    public void setTagIds(String tagIds) {
        this.tagIds = tagIds;
    }

    /**
     * 
     * @return
     *     The displayCost
     */
    public String getDisplayCost() {
        return displayCost;
    }

    /**
     * 
     * @param displayCost
     *     The Display_cost
     */
    public void setDisplayCost(String displayCost) {
        this.displayCost = displayCost;
    }

    /**
     * 
     * @return
     *     The type
     */
    public String getType() {
        return type;
    }

    /**
     * 
     * @param type
     *     The Type
     */
    public void setType(String type) {
        this.type = type;
    }

    /**
     * 
     * @return
     *     The oldTotalCost
     */
    public int getOldTotalCost() {
        return oldTotalCost;
    }

    /**
     * 
     * @param oldTotalCost
     *     The Old_total_cost
     */
    public void setOldTotalCost(int oldTotalCost) {
        this.oldTotalCost = oldTotalCost;
    }

    /**
     * 
     * @return
     *     The totalCost
     */
    public float getTotalCost() {
        return totalCost;
    }

    /**
     * 
     * @param totalCost
     *     The Total_cost
     */
    public void setTotalCost(float totalCost) {
        this.totalCost = totalCost;
    }

    /**
     * 
     * @return
     *     The unitCost
     */
    public float getUnitCost() {
        return unitCost;
    }

    /**
     * 
     * @param unitCost
     *     The Unit_cost
     */
    public void setUnitCost(float unitCost) {
        this.unitCost = unitCost;
    }

    /**
     * 
     * @return
     *     The choiceText
     */
    public String getChoiceText() {
        return choiceText;
    }

    /**
     * 
     * @param choiceText
     *     The Choice_text
     */
    public void setChoiceText(String choiceText) {
        this.choiceText = choiceText;
    }

}
