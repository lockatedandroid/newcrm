package com.android.lockated.utils;

public class ApplicationURL {

    private static final String BaseURL = "https://www.lockated.com/api/";
    private static final String BaseURLTwo = "https://www.lockated.com/";
    private static final String BaseURLThree = "https://www.lockated.com/api/";
    private static final String BaseURLFour = "https://www.lockated.com/";
    public static final String ImageBaseURL = "https://lockated.com";
    public static final String ChatBaseURL = "http://chat.lockated.com/";

//    private static final String BaseURL = "http://dev.lockated.com/api/";
//    private static final String BaseURLTwo = "http://dev.lockated.com/";
//    public static final String ImageBaseURL = "http://dev.lockated.com";

    public static String societyId;

    public static String loginUrl;
    public static String signUpUrl;
    public static String generateOTPUrl;
    public static String verifyOTPUrl;
    public static String getCategoryUrl;
    public static String forgotPasswordUrl;
    public static String getScreenUrl;
    public static String getAddressUrl;
    public static String createAddressUrl;
    public static String updateAddressUrl;
    public static String deleteAddressUrl;
    public static String cancelOrderUrl;
    public static String addFamilyDetailUrl;
    public static String updateFamilyDetailUrl;
    public static String deleteFamilyDetailUrl;
    public static String addGeneralDetailUrl;
    public static String updateGeneralDetailUrl;
    public static String addServantDetailUrl;
    public static String updateServantDetailUrl;
    public static String deleteServantDetailUrl;
    public static String getConversationUrl;
    public static String getConversationByIdUrlPP;
    public static String sendConversationUrl;
    public static String sendConversationUrlPP;
    public static String getConversationByIdUrl;
    public static String getChatIdUrl;
    public static String getOperatorTypingUrl;
    public static String getUserTypingUrl;
    public static String getDeptOnlineUrl;
    public static String getSocietyUsingZipUrl;
    public static String addSocietyDetailUrl;
    public static String getMyAddressesUrl;
    public static String editUserDetailUrl;
    public static String getProductDetailUrl;
    public static String addProductToCartUrl;
    public static String addAnotherItemToCartUrl;
    public static String getCartDetailUrl;
    public static String updateProductQuantityUrl;
    public static String applyCouponCodeUrl;
    public static String proceedToPaymentUrl;
    public static String getSupplierBylatLongUrl;
    public static String getSupplierProductUrl;
    public static String getSearchedProductUrl;
    public static String getServiceDataUrl;
    public static String submitServiceDataUrl;
    public static String serviceDataUrl;
    public static String getAllProductNameUrl;
    public static String getEventsUrl;
    public static String updateEventsUrl;
    public static String getServiceabilityUrl;
    public static String addEventDetails;
    public static String getPendingEventsUrl;
    public static String publishEvent;
    public static String denyEvent;
    public static String publishedEventsUrl;
    public static String getMyEventsUrl;
    public static String addPollsDetails;
    public static String addNotice;
    public static String getAdminPendingNoticeUrl;
    public static String getNoticesUrl;
    public static String getMyNoticesUrl;
    public static String getPublishNoticeUrl;
    public static String getRejectNoticeUrl;
    public static String getRolesDataUrl;
    public static String getVisitorUrl;
    public static String addVisitor;
    public static String addHelpdeskComplain;
    public static String addDirectory;
    public static String getPendingUser;
    public static String getApprovedUser;
    public static String getHelpdeskUrl;
    public static String getAdminPendingClassifiedUrl;
    public static String paytmGenerateCheckSumUrl;
    public static String paytmVerifyCheckSumUrl;
    public static String getAdminPendingHelpDeskdUrl;
    public static String updatePendingComplaint;
    public static String getHelpDeskCatecory;
    public static String getDirectory;
    public static String deleteVisitor;
    public static String getSocietyDetails;
    public static String putSocietyDetails;
    public static String getPaytmTrxSuccessUrl;
    public static String UpdateDirectory;
    public static String deleteDirectory;
    public static String UpdateGateKeeper;
    public static String getExpectedVisitor;
    public static String getAmenitiesUrl;
    public static String bookFacility;
    public static String sendUserDetails;
    public static String getBookedFacility;
    public static String getBookedFacilityHistory;
    public static String approveBookedFacilities;
    public static String rejectBookedFacilities;
    public static String createFacilitiesUrl;
    public static String getAllMemberList;
    public static String deleteAmenities;
    public static String postSellClassified;
    public static String getPollsUrl;
    public static String manageUserAdmin;
    public static String addPublicDirectory;
    public static String getPublicDirectory;
    public static String updatePublicDirectory;
    public static String deletePublicDirectory;
    public static String getMangedMembers;
    public static String getuserClassified;
    public static String getPendingClassified;
    public static String PublishClassified;
    public static String getAllPublishClassified;
    public static String disableClassified;
    public static String votePollUrl;
    public static String cancelFacilitiesUrl;
    public static String publishPoll;
    public static String DenyPoll;
    public static String getPendingPollsUrl;
    public static String getadminPublishedPollsUrl;
    public static String getUseCreatedPolls;
    public static String getMyCreatedClassifieds;
    public static String getClassifiedCategory;
    public static String getCategoryBanner;
    public static String getLandingBanner;
    public static String getReminderDataUrl;
    public static String deleteReminderDataUrl;
    public static String PLACES_API_BASE;
    public static String TYPE_AUTOCOMPLETE;
    public static String OUT_JSON;
    public static String Server_KEY;
    public static String getOffersUrl;
    public static String UpdateBooking;
    public static String approveDisapproveUser;
    public static String getMyServiceRequests;
    public static String cancelMyServiceRequests;
    public static String getUserSocietyLists;
    public static String getForgotPasswordOtp;
    public static String getMySocietyList;
    public static String getPaytmPromoCodeUrl;
    public static String getMyGroups;
    public static String postMyGroups;
    public static String deleteGroup;
    public static String updatePersonalInformatio;
    public static String getPersonalInformation;
    public static String setUserSocietyId;
    public static String removeUserFromGroup;
    public static String updateCurrentGroup;
    public static String crmBannerApi;
    public static String getVisitorAllPurposes;
    public static String getResidenceData;
    public static String getFamilyData;


    //--------------Ola changes---------------
    public static String getAllOlaTransaction;
    public static String getLastOlaTransaction;
    //----------------------------------------

    //-------------Zomato Changes-----------------
    public static String getAllZomatoTransaction;
    public static String postSingleZomatoTransaction;
    public static String updateSingleZomatoTransaction;
    public static String getBaseAccountApi;
    //-----------------------------------------



    static {
        societyId = "&society_id=";
        loginUrl = BaseURL + "users/sign_in";
        signUpUrl = BaseURL + "users/sign_up";
        generateOTPUrl = BaseURLTwo + "generate_code";
        verifyOTPUrl = BaseURLTwo + "verify_code";
        getCategoryUrl = BaseURL + "taxonomies";
        forgotPasswordUrl = BaseURL + "users/forgot_password";
        //=============================Account Detail API===========================================
        getScreenUrl = BaseURL + "users/account";
        getResidenceData = BaseURLTwo + "user_flats.json?token=";
        getFamilyData = BaseURLTwo + "user_families.json?token=";
        getBaseAccountApi = BaseURL + "users/basics?token=";
        getAddressUrl = BaseURL + "users/addresses";
        createAddressUrl = BaseURL + "users/create_address";
        updateAddressUrl = BaseURL + "users/update_address";
        deleteAddressUrl = BaseURL + "users/remove_address";
        cancelOrderUrl = BaseURL + "orders/";
        addFamilyDetailUrl = BaseURLTwo + "user_families.json";
        updateFamilyDetailUrl = BaseURLTwo + "user_families/";
        deleteFamilyDetailUrl = BaseURLTwo + "user_families/";
        addGeneralDetailUrl = BaseURLTwo + "user_general_details.json";
        updateGeneralDetailUrl = BaseURLTwo + "user_general_details/";
        addServantDetailUrl = BaseURLTwo + "user_servants.json";
        updateServantDetailUrl = BaseURLTwo + "user_servants/";
        deleteServantDetailUrl = BaseURLTwo + "user_servants/";
        //=============================Chat API=====================================================
        sendConversationUrl = ChatBaseURL + "index.php/chat/addmsguser/";
        getOperatorTypingUrl = ChatBaseURL + "isoperatortyping.php?chat_id=";
        getUserTypingUrl = ChatBaseURL + "index.php/chat/usertyping/";
        getConversationByIdUrl = ChatBaseURL + "getmessages.php?chat_id=";
        getChatIdUrl = ChatBaseURL + "providechat.php?user_id=";
        getDeptOnlineUrl = ChatBaseURL + "index.php/restapi/isonlinedepartment/";
        //=====================================Peer To Peer Chat API================================
        getConversationUrl = BaseURL + "conversations";
        sendConversationUrlPP = BaseURL + "conversations/";
        getConversationByIdUrlPP = BaseURL + "conversations/";
        //=============================Society API==================================================
        getSocietyUsingZipUrl = BaseURL + "societies/search?q[postcode_eq]=";
        addSocietyDetailUrl = BaseURLTwo + "user_flats.json?token=";
        getMyAddressesUrl = BaseURL + "users/addresses?token=";
        editUserDetailUrl = BaseURL + "users?token=";
        getProductDetailUrl = BaseURL + "taxons/products?id=";
        addProductToCartUrl = BaseURL + "orders?token=";
        addAnotherItemToCartUrl = BaseURL + "line_items/multiple?token=";
        getCartDetailUrl = BaseURL + "orders/current?token=";
        updateProductQuantityUrl = BaseURL + "orders/";
        applyCouponCodeUrl = BaseURL + "checkouts/";
        proceedToPaymentUrl = BaseURL + "checkouts/";
        getSupplierBylatLongUrl = BaseURL + "supplier_location_taxonomy?token=";
        getSupplierProductUrl = BaseURL + "supplier_product_taxonomy?token=";
        getSearchedProductUrl = BaseURL + "products?token=";
        getServiceDataUrl = BaseURLTwo + "request_metadata?taxon_id=";
        serviceDataUrl = BaseURLTwo + "service_requests_multiple.json?token=";
        submitServiceDataUrl = BaseURLTwo + "service_requests.json?token=";
        getAllProductNameUrl = BaseURL + "auto_search_products?token=";
        //getMySocietyList = BaseURLTwo + "user_registered_societies.json?token=";
        getMySocietyList = BaseURLTwo + "user_flats.json?token=";//individual change
        //=============================Notice API==================================================
        getNoticesUrl = BaseURLTwo + "noticeboards.json?token=";
        addNotice = BaseURLTwo + "noticeboards.json?token=";
        getMyNoticesUrl = BaseURLTwo + "user_notices.json?token=";
        getAdminPendingNoticeUrl = BaseURLTwo + "pending_noticeboards.json?token=";
        getPublishNoticeUrl = BaseURLTwo + "publish_noticeboards.json?id=";
        getRejectNoticeUrl = BaseURLTwo + "deny_noticeboards.json?id=";
        //==============================Events API=================================================
        getEventsUrl = BaseURLTwo + "events.json?token=";
        updateEventsUrl = BaseURLTwo + "event_rsvps.json?token=";
        addEventDetails = BaseURLTwo + "events.json?token=";
        getPendingEventsUrl = BaseURLTwo + "pending_events.json?token=";
        publishEvent = BaseURLTwo + "publish_events.json?token=";
        denyEvent = BaseURLTwo + "deny_events.json?token=";
        publishedEventsUrl = BaseURLTwo + "deny_events.json?token=";
        getMyEventsUrl = BaseURLTwo + "user_events.json?token=";
        //==============================Polls API==================================================
        addPollsDetails = BaseURLTwo + "polls.json?token=";
        getPollsUrl = BaseURLTwo + "polls.json?society_id=";
        votePollUrl = BaseURLTwo + "poll_votes.json?token=";
        publishPoll = BaseURLTwo + "publish_polls.json?id=";
        DenyPoll = BaseURLTwo + "deny_polls.json?id=";
        getPendingPollsUrl = BaseURLTwo + "pending_polls.json?society_id=";
        getadminPublishedPollsUrl = BaseURLTwo + "admin_polls.json?society_id=";
        getUseCreatedPolls = BaseURLTwo + "user_polls.json?society_id=";
        //==============================Facilities=================================================
        UpdateBooking = BaseURLThree + "facility_bookings/";
        //==============================PayTm API==================================================
        paytmGenerateCheckSumUrl = BaseURLThree + "generate_checksum?";
        paytmVerifyCheckSumUrl = BaseURLFour + "verify_checksum";
        getPaytmTrxSuccessUrl = BaseURLThree + "paytm_network_callback";
        //==============================User Management============================================
        approveDisapproveUser = BaseURLTwo + "user_societies/";
        getServiceabilityUrl = BaseURLTwo + "serviceabilities/check?pincode=";
        getRolesDataUrl = BaseURLTwo + "admin/get_role.json?token=";
        getVisitorUrl = BaseURLTwo + "gatekeepers.json?token=";
        addVisitor = BaseURLTwo + "gatekeepers.json?token=";
        addHelpdeskComplain = BaseURLTwo + "complaints.json?token=";
        getHelpdeskUrl = BaseURLTwo + "user/helpdesk.json?token=";
        getPendingUser = BaseURLTwo + "user_societies.json?society_id=";
        getApprovedUser = BaseURLTwo + "approved_user_societies.json?society_id=";
        getAdminPendingClassifiedUrl = BaseURLTwo + "admin/helpdesk?token=";
        getAdminPendingHelpDeskdUrl = BaseURLTwo + "admin/helpdesk.json?token=";
        updatePendingComplaint = BaseURLTwo + "complaints/";
        addDirectory = BaseURLTwo + "directories.json?token=";
        getDirectory = BaseURLTwo + "directories.json?token=";
        deleteVisitor = BaseURLTwo + "gatekeepers/";
        getSocietyDetails = BaseURLTwo + "societies/";
        putSocietyDetails = BaseURLTwo + "societies/";
        UpdateDirectory = BaseURLTwo + "directories/";
        getHelpDeskCatecory = BaseURLTwo + "get_complaint_constants.json?token=";
        UpdateGateKeeper = BaseURLTwo + "gatekeepers/";
        getExpectedVisitor = BaseURLTwo + "expected_visiter.json?token=";
        deleteDirectory = BaseURLTwo + "directories/";
        getAmenitiesUrl = BaseURLTwo + "amenities.json?society_id=";
        bookFacility = BaseURLThree + "facility_bookings.json?token=";
        sendUserDetails = BaseURLTwo + "user_devices.json?token=";
        getBookedFacility = BaseURLThree + "booked_facilities.json?token=";
        getBookedFacilityHistory = BaseURLThree + "requested_facilities.json?token=";
        approveBookedFacilities = BaseURLThree + "approve_facilities.json?id=";
        createFacilitiesUrl = BaseURLTwo + "amenities.json?token=";
        getAllMemberList = BaseURLTwo + "approved_user_societies.json?society_id=";
        deleteAmenities = BaseURLTwo + "amenities/";
        postSellClassified = BaseURLTwo + "classifieds.json?token=";
        rejectBookedFacilities = BaseURLThree + "facility_bookings/";
        manageUserAdmin = BaseURLTwo + "user_societies/";
        addPublicDirectory = BaseURLTwo + "public_directories.json?token=";
        getPublicDirectory = BaseURLTwo + "public_directories.json?";
        updatePublicDirectory = BaseURLTwo + "public_directories/";
        deletePublicDirectory = BaseURLTwo + "/public_directories/";
        getMangedMembers = BaseURLTwo + "manage/members.json?token=";
        getuserClassified = BaseURLTwo + "classifieds.json?token=";
        getPendingClassified = BaseURLTwo + "pending_classifieds.json?token=";
        PublishClassified = BaseURLTwo + "publish_classifieds.json?id=";
        disableClassified = BaseURLTwo + "deny_classifieds.json?id=";
        getAllPublishClassified = BaseURLTwo + "admin_published_classifieds.json?society_id=";
        cancelFacilitiesUrl = BaseURLThree + "approve_facilities?id=";
        getMyCreatedClassifieds = BaseURLTwo + "user_classifieds.json?token=";
        getClassifiedCategory = BaseURLTwo + "classified_categories.json?token=";
        getCategoryBanner = BaseURLTwo + "banners.json?q[btype_eq]=taxonomy&q[btype_id_eq]=";
        getLandingBanner = BaseURLTwo + "banners.json?q[btype_eq]=landing&q[btype_id_eq]=";
        getReminderDataUrl = BaseURLTwo + "reminders.json?token=";
        deleteReminderDataUrl = BaseURLTwo + "reminders/";
        //=========================Google places API================================================
        PLACES_API_BASE = "https://maps.googleapis.com/maps/api/place";
        TYPE_AUTOCOMPLETE = "/autocomplete";
        OUT_JSON = "/json";
        //private static final String Browser_key = "AIzaSyAY-AzuFALdX2MGeolc9B5NhYTTFeY0O9w";
        //private static final String Server_key = "AIzaSyC3NDIJXyTKfrqtoZvZ9O_LTBgLJK0GTqU";
        //private static final String Server_KEYAc = "AIzaSyC5K3qyALLr6Zz8ApMr1Wr7o6Y-M2FjDWM";

        //=====app.lockated@gmail.com===============================================================
        Server_KEY = "AIzaSyDfOUfmu3tp1PN_pN6zkQ6IuZ4y20pL5Ng";
        //============================Offers========================================================
        getOffersUrl = BaseURLTwo + "offers.json?token=";
        //===============================My Services================================================
        getMyServiceRequests = BaseURLTwo + "service_requests.json?token=";
        cancelMyServiceRequests = BaseURLTwo + "service_requests/cancel.json?id=";
        getPaytmPromoCodeUrl = BaseURL + "get_paytm_promo_code";
        //==================================getUserSocietyLists=====================================
        getUserSocietyLists = BaseURLTwo + "/user_approved_societies.json?token=";
        setUserSocietyId = BaseURLTwo + "change_user_society.json?token=";
        //============================Forgot Password===============================================
        getForgotPasswordOtp = BaseURLThree + "users/forgot_password_otp";
        //============================My Groups=====================================================
        getMyGroups = BaseURLTwo + "usergroups.json?token=";
        postMyGroups = BaseURLTwo + "usergroups.json?token=";
        deleteGroup = BaseURLTwo + "usergroups/";
        removeUserFromGroup = BaseURLTwo + "usergroup_members/";
        updateCurrentGroup = BaseURLTwo + "usergroups/";
        getVisitorAllPurposes = BaseURLTwo + "purposes.json?society_id=";
        //============================PersonalInfo==================================================
        updatePersonalInformatio = BaseURL + "users?token=";
        getPersonalInformation = BaseURL + "users/personal_info?token=";
        //============================CrmActivity Banner============================================
        crmBannerApi=BaseURLTwo+"society_banners.json?token=";
        //============================Ola Changes===================================================
        getAllOlaTransaction = BaseURLFour +"get_all_thirdparty_transactions.json?q[thirdparty_id_eq]=2&token=";
        getLastOlaTransaction = BaseURLFour+"get_last_ola_transaction.json?token=";
        //============================Zomato Changes================================================
        postSingleZomatoTransaction = BaseURLTwo + "add_zomato_orders.json?token=";
        updateSingleZomatoTransaction = BaseURLTwo + "update_thirdparty_transactions.json?token=";
        getAllZomatoTransaction  = BaseURLTwo + "get_all_thirdparty_transactions.json?q[thirdparty_id_eq]=1&q[forandroid_eq]=1&token=";
    }
}
