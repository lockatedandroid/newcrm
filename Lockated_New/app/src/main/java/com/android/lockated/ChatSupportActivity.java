package com.android.lockated;

import android.app.ActivityManager;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;

import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;

import com.android.lockated.landing.SupportFragment;
import com.android.lockated.preferences.LockatedPreferences;

import java.util.Iterator;
import java.util.List;

public class ChatSupportActivity extends AppCompatActivity {

    private String Fragment_Tag = "SupportFragment";
    LockatedPreferences lockatedPreferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat_support);

        setToolBarTitle("blank");
        if (savedInstanceState == null) {
            SupportFragment supportFragment = new SupportFragment();
            supportFragment.setChatId(((LockatedApplication) getApplicationContext()).getmSectionId(),
                    ((LockatedApplication) getApplicationContext()).getSectionName());
            getSupportFragmentManager().beginTransaction().add(R.id.mChatContainer, supportFragment, Fragment_Tag).commit();
        }
    }

    public void setToolBarTitle(String titleName) {

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        ImageView imageView = (ImageView) toolbar.findViewById(R.id.statusView);
        ImageView refreshImage = (ImageView) toolbar.findViewById(R.id.refresh);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(true);
        getSupportActionBar().setTitle(((LockatedApplication) getApplicationContext()).getSectionName());

        if (titleName.equals("offline")) {
            iconImage(imageView, refreshImage, "offline");
        } else if (titleName.equals("online")) {
            iconImage(imageView, refreshImage, "online");
        } else if (titleName.equals("none")) {
            iconImage(imageView, refreshImage, "none");
        }
    }

    public void iconImage(ImageView onOffImg, ImageView refreshImg, String onOffStr) {

        if (onOffStr.equals("online")) {
            onOffImg.setImageResource(R.drawable.onlinegreen);
        } else if (onOffStr.equals("offline")) {
            onOffImg.setImageResource(R.drawable.offlinered);
        }
        /*refreshImg.setImageResource(R.drawable.refresh);
        refreshImg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                refreshFragment(Fragment_Tag);
            }
        });*/
    }

    public void refreshFragment(String Fragment_Tag) {

        SupportFragment supportFragment = new SupportFragment();
        supportFragment.setChatId(((LockatedApplication) getApplicationContext()).getmSectionId(),
                ((LockatedApplication) getApplicationContext()).getSectionName());
        final FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.detach(supportFragment);
        fragmentTransaction.attach(supportFragment);
        fragmentTransaction.commit();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();

        if (id == android.R.id.home) {
            onBackPressed();
            return true;
        } else {
            return false;
        }
    }

    @Override
    public void onBackPressed() {
        Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.mChatContainer);
        if (fragment instanceof SupportFragment) {
            if (((SupportFragment) fragment).isSampleMessageSelected) {
                ((SupportFragment) fragment).isSampleMessageSelected = false;
                ((SupportFragment) fragment).mSupportListSample.setVisibility(View.GONE);
                ((SupportFragment) fragment).mImageViewSampleMessage.setImageResource(R.drawable.ic_sample_message_disable);
            } else if (activityCount() == 1) {
                pressedBack();
            } else {
                super.onBackPressed();
            }
        } else if (activityCount() == 1) {
            pressedBack();
        } else {
            super.onBackPressed();
        }
    }

    public int activityCount() {
        ActivityManager m = (ActivityManager) getApplicationContext().getSystemService(Context.ACTIVITY_SERVICE);
        List<ActivityManager.RunningTaskInfo> runningTaskInfoList = m.getRunningTasks(10);
        Iterator<ActivityManager.RunningTaskInfo> itr = runningTaskInfoList.iterator();
        int numOfActivities = 0, runningActivities = 0;
        while (itr.hasNext()) {
            ActivityManager.RunningTaskInfo runningTaskInfo = (ActivityManager.RunningTaskInfo) itr.next();
            int id = runningTaskInfo.id;
            CharSequence desc = runningTaskInfo.description;
            numOfActivities = runningTaskInfo.numActivities;
            runningActivities = runningTaskInfo.numRunning;
            String topActivity = runningTaskInfo.topActivity.getClassName();
            /*Log.e("id " + id, "numOfActivities " + numOfActivities + " topActivity "
                    + topActivity + " runningActivities " + runningActivities + " desc " + desc);*/
        }
        //Log.e("numOfActivities " + numOfActivities, " runningActivities " + runningActivities);
        return numOfActivities;
    }

    public void pressedBack() {
        Intent intent = new Intent(this, HomeActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
        finish();
    }

}
