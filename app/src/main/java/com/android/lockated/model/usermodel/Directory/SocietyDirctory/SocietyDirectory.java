
package com.android.lockated.model.usermodel.Directory.SocietyDirctory;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class SocietyDirectory {

    @SerializedName("code")
    @Expose
    private int code;
    @SerializedName("public_directories")
    @Expose
    private ArrayList<PublicDirectory> publicDirectories = new ArrayList<PublicDirectory>();

    /**
     * 
     * @return
     *     The code
     */
    public int getCode() {
        return code;
    }

    /**
     * 
     * @param code
     *     The code
     */
    public void setCode(int code) {
        this.code = code;
    }

    /**
     * 
     * @return
     *     The publicDirectories
     */
    public ArrayList<PublicDirectory> getPublicDirectories() {
        return publicDirectories;
    }

    /**
     * 
     * @param publicDirectories
     *     The public_directories
     */
    public void setPublicDirectories(ArrayList<PublicDirectory> publicDirectories) {
        this.publicDirectories = publicDirectories;
    }

}
