package com.android.lockated.crmadmin.activity;

import android.Manifest;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.lockated.R;
import com.android.lockated.network.ConnectionDetector;
import com.android.lockated.preferences.LockatedPreferences;
import com.android.lockated.request.LockatedJSONObjectRequest;
import com.android.lockated.request.LockatedRequestError;
import com.android.lockated.request.LockatedVolleyRequestQueue;
import com.android.lockated.utils.ApplicationURL;
import com.android.lockated.utils.DatePickerFragment;
import com.android.lockated.utils.MarshMallowPermission;
import com.android.lockated.utils.ShowImage;
import com.android.lockated.utils.TimePickerFragment;
import com.android.lockated.utils.Utilities;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class GateKeeperActivity extends AppCompatActivity implements View.OnClickListener, Response.ErrorListener, Response.Listener<JSONObject> {
    public static final String REQUEST_TAG = "GateKeeperActivity";
    static final int REQUEST_CAMERA_PHOTO = 3;
    static final int REQUEST_TAKE_PHOTO = 100;
    static final int CHOOSE_IMAGE_REQUEST = 101;
    static final int REQUEST_CAMERA = 102;
    static final int REQUEST_STORAGE = 103;
    ProgressBar mProgressBar;
    String spid;
    String strUrl;

    String striName;
    String striVisitTo;
    String striPurpose;
    String striVehicleNumber;
    String striMember;
    String name;
    String approve;
    String striContactNumber;
    String doc_id;
    String striEntryDate;
    String striExitDate;
    ProgressBar progressBar;
    boolean imageSet;
    String encodedImage, mCurrentPhotoPath;
    MarshMallowPermission marshMallowPermission;
    private EditText mVisitorName;
    private EditText mAdditionalMember;
    private EditText mContactNumber;
    private EditText mExpectedDate;
    private EditText mFlatNumber;
    private TextView mTextEntryTime;
    private TextView mTextEntryDate;
    private TextView mTextExitTime;
    private TextView mTextExitDate;
    private TextView mTextStatus;
    private TextView textVisitorImage;
    private ImageView mImageEntryDate;
    private ImageView mImageEntryTime;
    private ImageView mImageExitTime;
    private ImageView mImageExitDate;
    private ImageView mVisitorImage;
    private TextView mSubmit;
    private EditText mVisitTo;
    private EditText mVisitPurpose;
    private EditText mVehiclrNumber;
    private ImageView mclose;
    private RequestQueue mQueue;
    private LockatedPreferences mLockatedPreferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_gate_keeper);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(true);
        getSupportActionBar().setTitle(R.string.gatekeeper);
        init();
        getVisitorDetails();
        getEntryExitDate();
    }

    private void getEntryExitDate() {
        if (ConnectionDetector.isConnectedToInternet(this)) {
            mLockatedPreferences = new LockatedPreferences(this);
            mQueue = LockatedVolleyRequestQueue.getInstance(this).getRequestQueue();
            LockatedJSONObjectRequest lockatedJSONObjectRequest = new LockatedJSONObjectRequest(Request.Method.GET,
                    ApplicationURL.getExpectedVisitor + mLockatedPreferences.getLockatedToken()
                            + ApplicationURL.societyId + mLockatedPreferences.getSocietyId(), null,
                    new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject response) {
                            progressBar.setVisibility(View.GONE);
                        }
                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    progressBar.setVisibility(View.GONE);
                }
            });
            lockatedJSONObjectRequest.setTag(REQUEST_TAG);
            mQueue.add(lockatedJSONObjectRequest);
        } else {
            Utilities.showToastMessage(this, this.getResources().getString(R.string.internet_connection_error));
        }
    }


    private void init() {
        mLockatedPreferences = new LockatedPreferences(this);
        progressBar = (ProgressBar) findViewById(R.id.mProgressBarView);
        mVisitTo = (EditText) findViewById(R.id.mVisitTo);
        mVisitPurpose = (EditText) findViewById(R.id.mVisitPurpose);
        mVehiclrNumber = (EditText) findViewById(R.id.mVehiclrNumber);
        mVisitorName = (EditText) findViewById(R.id.mVisitorName);
        mAdditionalMember = (EditText) findViewById(R.id.mAdditionalMember);
        mContactNumber = (EditText) findViewById(R.id.mContactNumber);
        mExpectedDate = (EditText) findViewById(R.id.mExpectedDate);

        mFlatNumber = (EditText) findViewById(R.id.mFlatNumber);
        mSubmit = (TextView) findViewById(R.id.mSubmit);
        textVisitorImage = (TextView) findViewById(R.id.textVisitorImage);
        mTextStatus = (TextView) findViewById(R.id.mTextStatus);

        mImageEntryDate = (ImageView) findViewById(R.id.mImageEntryDate);
        mImageEntryTime = (ImageView) findViewById(R.id.mImageEntryTime);
        mImageExitTime = (ImageView) findViewById(R.id.mImageExitTime);
        mImageExitDate = (ImageView) findViewById(R.id.mImageExitDate);
        mVisitorImage = (ImageView) findViewById(R.id.mVisitorImage);

        mTextEntryTime = (TextView) findViewById(R.id.mTextEntryTime);
        mTextEntryDate = (TextView) findViewById(R.id.mTextEntryDate);
        mTextExitTime = (TextView) findViewById(R.id.mTextExitTime);
        mTextExitDate = (TextView) findViewById(R.id.mTextExitDate);
        mSubmit.setOnClickListener(this);
        mImageEntryDate.setOnClickListener(this);
        mImageEntryTime.setOnClickListener(this);
        mImageExitTime.setOnClickListener(this);
        mImageExitDate.setOnClickListener(this);
        mVisitorImage.setOnClickListener(this);
    }

    private void getVisitorDetails() {
        if (!mLockatedPreferences.getGateKeeper_id().equals("blank") && mLockatedPreferences.getGateKeeper_id() != null) {
            getgatekeeperApi(mLockatedPreferences.getGateKeeper_id());
        } else {
            if (getIntent().getExtras() != null) {
                name = getIntent().getExtras().getString("name");
                String strEntryDate = getIntent().getExtras().getString("strEntryDate");
                if (strEntryDate == null) {
                    mTextEntryDate.setText("Entry Date");
                    mTextEntryTime.setText("Entry Time");
                } else {
                    mTextEntryDate.setText(Utilities.ddMMYYYFormat(strEntryDate));
                    String time1 = (Utilities.ddMonYYYYTimeFormat(strEntryDate));
                    String[] separate = time1.split(" ");
                    mTextEntryTime.setText((separate[1]));

                }
                String ExitDate = getIntent().getExtras().getString("strExitDate");

                if (ExitDate != null) {
                    mTextExitDate.setText(Utilities.ddMMYYYFormat(ExitDate));
                    String time = (Utilities.ddMonYYYYTimeFormat(ExitDate));
                    String[] separated = time.split(" ");
                    mTextExitTime.setText((separated[1]));
                } else {
                    mTextExitDate.setText("Exit Date");
                    mTextExitTime.setText("Exit Time");
                }
                mVisitorName.setText(getIntent().getExtras().getString("strName"));
                mVisitTo.setText(getIntent().getExtras().getString("strVisitTO"));
                mVisitPurpose.setText(getIntent().getExtras().getString("strPurpose"));
                String strNumberOfPerson = getIntent().getExtras().getString("strMember");
                if (strNumberOfPerson != null) {
                    mAdditionalMember.setText(getIntent().getExtras().getString("strMember"));
                } else {
                    mAdditionalMember.setText("0");
                }
                approve = getIntent().getExtras().getString("approved");
                String accompany = getIntent().getExtras().getString("accompany");

                statusChange(approve, accompany);

                mContactNumber.setText(getIntent().getExtras().getString("strNumber"));
                mExpectedDate.setText(Utilities.dateTime_AMPM_Format(getIntent().getExtras().getString("strExpDate")));
                mFlatNumber.setText(getIntent().getExtras().getString("strFlatNumber"));
                spid = getIntent().getExtras().getString("pid");
                String strVehicleNumber = getIntent().getExtras().getString("strVehicleNumber");
                if (strVehicleNumber != null) {
                    mVehiclrNumber.setText(getIntent().getExtras().getString("strVehicleNumber"));
                } else {
                    mVehiclrNumber.setText("No Vehicle");
                }
                doc_id = getIntent().getExtras().getString("doc_id");
                strUrl = getIntent().getExtras().getString("visitorImage");
                if (!strUrl.equals("No Document") && !doc_id.equals("No Id")) {
                    doc_id = getIntent().getExtras().getString("doc_id");
                    Picasso.with(this).load(strUrl).fit().into(mVisitorImage);
                } else {
                    doc_id = "";
                    encodedImage = "";
                    mVisitorImage.setVisibility(View.GONE);
                    textVisitorImage.setVisibility(View.VISIBLE);
                }
            }
        }
    }

    private void statusChange(String approve, String accompany) {
        switch (this.approve) {
            case "0":
                mTextStatus.setText(getResources().getString(R.string.status_pending));
                mTextStatus.setTextColor(getResources().getColor(R.color.primary));
                break;
            case "1":
                if (accompany != null && accompany.equals("1")) {
                    mTextStatus.setTextSize(12);
                    mTextStatus.setText(getResources().getString(R.string.status_accompany));
                    mTextStatus.setTextColor(getResources().getColor(R.color.primary));
                } else {
                    mTextStatus.setText(getResources().getString(R.string.status_approve));
                    mTextStatus.setTextColor(getResources().getColor(R.color.green));
                }
                break;
            default:
                mTextStatus.setText(getResources().getString(R.string.status_reject));
                mTextStatus.setTextColor(getResources().getColor(R.color.red));
                break;
        }
    }

    private void getgatekeeperApi(String gateKeeper_id) {
        progressBar.setVisibility(View.VISIBLE);
        if (ConnectionDetector.isConnectedToInternet(getApplicationContext())) {
            mQueue = LockatedVolleyRequestQueue.getInstance(getApplicationContext()).getRequestQueue();
            String url = "https://www.lockated.com/gatekeepers/" + gateKeeper_id + ".json?token=" + mLockatedPreferences.getLockatedToken()
                    + "&id_society=" + mLockatedPreferences.getSocietyId();
            LockatedJSONObjectRequest lockatedJSONObjectRequest = new LockatedJSONObjectRequest(Request.Method.GET,
                    url, null, this, this);
            lockatedJSONObjectRequest.setTag(REQUEST_TAG);
            mQueue.add(lockatedJSONObjectRequest);
        } else {
            Utilities.showToastMessage(getApplicationContext(), getApplicationContext().getResources().getString(R.string.internet_connection_error));
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.mSubmit:
                onUpdateDetails();
                break;
            case R.id.mImageEntryDate:
                onEditDateClicked(mTextEntryDate);
                break;
            case R.id.mImageEntryTime:
                onTimeClicked(mTextEntryTime);
                break;
            case R.id.mImageExitDate:
                onEditDateClicked(mTextExitDate);
                break;
            case R.id.mImageExitTime:
                onTimeClicked(mTextExitTime);
                break;
            case R.id.mVisitorImage:
                Intent displayImage = new Intent(getApplicationContext(), ShowImage.class);
                displayImage.putExtra("imageUrlString", strUrl);
                startActivity(displayImage);
                /* if (!imageSet) {
                    selectImage();
                } else {
                    selectImageAction();
                }*/
                break;
        }
    }

   /* private void selectImageAction() {
        final CharSequence[] options = {"View Image"*//*, "Change Image", "Remove Image"*//*};
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setItems(options, new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int item) {
                if (options[item].equals("View Image")) {
                    Intent displayImage = new Intent(getApplicationContext(), ShowImage.class);
                    displayImage.putExtra("imageUrlString", strUrl);
                    startActivity(displayImage);
                } else if (options[item].equals("Change Image")) {
                    selectImage();
                } else {
                    mVisitorImage.setImageResource(R.drawable.ic_account_camera);
                    encodedImage = "";
                    imageSet = false;
                }
            }

        });
        builder.show();

    }*/

    private void selectImage() {
        final CharSequence[] options = {"Camera", "Gallery"};
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Add Photo!");
        builder.setItems(options, new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int item) {
                if (options[item].equals("Camera")) {
                    checkCameraPermission();
                } else if (options[item].equals("Gallery")) {
                    checkStoragePermission();
                }
            }

        });

        builder.show();
    }

    private void checkStoragePermission() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                requestPermissions(new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, REQUEST_STORAGE);
            } else {
                onGalleryClicked();
            }
        } else {
            onGalleryClicked();
        }
    }

    private void onGalleryClicked() {
        Intent i = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(i, CHOOSE_IMAGE_REQUEST);
    }

    private void checkCameraPermission() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
                requestPermissions(new String[]{Manifest.permission.CAMERA}, REQUEST_CAMERA);
            } else {
                onCameraClicked();
            }
        } else {
            onCameraClicked();
        }
    }

    private void onCameraClicked() {
        if (!marshMallowPermission.checkPermissionForCamera()) {
            marshMallowPermission.requestPermissionForCamera();
        } else {
            if (!marshMallowPermission.checkPermissionForExternalStorage()) {
                marshMallowPermission.requestPermissionForExternalStorage();
            } else {
                Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                if (takePictureIntent.resolveActivity(this.getPackageManager()) != null) {
                    File photoFile = null;
                    try {
                        photoFile = createImageFile();
                    } catch (IOException ex) {
                        ex.printStackTrace();
                    }

                    if (photoFile != null) {
                        takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(photoFile));
                        startActivityForResult(takePictureIntent, REQUEST_TAKE_PHOTO);
                    }
                }
            }
        }
    }

    private File createImageFile() throws IOException {
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss", Locale.ENGLISH).format(new Date());
        String imageFileName = "JPEG_" + timeStamp + "_";
        File storageDir = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES);
        File image = File.createTempFile(imageFileName, ".jpg", storageDir);
        mCurrentPhotoPath = image.getAbsolutePath();
        return image;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode != this.RESULT_CANCELED) {
            if (requestCode == REQUEST_TAKE_PHOTO && resultCode == this.RESULT_OK) {
                setPic();
            } else if (requestCode == CHOOSE_IMAGE_REQUEST && resultCode == this.RESULT_OK) {
                Uri selectedImageURI = data.getData();
                mCurrentPhotoPath = getPath(selectedImageURI);
                setPic();
            } else if (requestCode == REQUEST_CAMERA_PHOTO && resultCode == this.RESULT_OK) {
                Bitmap thumbnail = (Bitmap) data.getExtras().get("data");
                if (thumbnail != null) {
                    ByteArrayOutputStream bytes = new ByteArrayOutputStream();
                    File destination = new File(Environment.getExternalStorageDirectory(), System.currentTimeMillis() + ".jpg");
                    FileOutputStream fo;
                    try {
                        destination.createNewFile();
                        fo = new FileOutputStream(destination);
                        fo.write(bytes.toByteArray());
                        fo.close();
                    } catch (FileNotFoundException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    setCameraPic(thumbnail);
                }
            }
        }
    }

    private String getPath(Uri uri) {
        if (uri == null) {
            return null;
        }
        String[] projection = {MediaStore.Images.Media.DATA};
        Cursor cursor = this.getContentResolver().query(uri, projection, null, null, null);
        if (cursor != null) {
            int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
            cursor.moveToFirst();
            return cursor.getString(column_index);
        }
        return uri.getPath();
    }

    private void setCameraPic(Bitmap bitmap) {
        if (bitmap != null) {
            encodedImage = Utilities.encodeTobase64(bitmap);
            mVisitorImage.setImageBitmap(bitmap);

            imageSet = true;
        }
    }

    private void setPic() {
        int targetW = mVisitorImage.getWidth();
        int targetH = mVisitorImage.getHeight();
        BitmapFactory.Options bmOptions = new BitmapFactory.Options();
        bmOptions.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(mCurrentPhotoPath, bmOptions);
        int photoW = bmOptions.outWidth;
        int photoH = bmOptions.outHeight;
        int scaleFactor = Math.min(photoW / targetW, photoH / targetH);

        bmOptions.inJustDecodeBounds = false;
        bmOptions.inSampleSize = scaleFactor;
        bmOptions.inPurgeable = true;
        try {

            ExifInterface exif = new ExifInterface(mCurrentPhotoPath);
            int orientation = exif.getAttributeInt(
                    ExifInterface.TAG_ORIENTATION,
                    ExifInterface.ORIENTATION_NORMAL);

            int angle = 0;

            if (orientation == ExifInterface.ORIENTATION_ROTATE_90) {
                angle = 90;
            } else if (orientation == ExifInterface.ORIENTATION_ROTATE_180) {
                angle = 180;
            } else if (orientation == ExifInterface.ORIENTATION_ROTATE_270) {
                angle = 270;
            }

            Matrix mat = new Matrix();
            mat.postRotate(angle);
            Bitmap bitmap;
            Bitmap bmp = BitmapFactory.decodeStream(new FileInputStream(mCurrentPhotoPath),
                    null, bmOptions);
            bitmap = Bitmap.createBitmap(bmp, 0, 0, bmp.getWidth(),
                    bmp.getHeight(), mat, true);
            if (bitmap != null) {
                encodedImage = Utilities.encodeTobase64(bitmap);
                mVisitorImage.setImageBitmap(bitmap);
                imageSet = true;
            }

        } catch (IOException | OutOfMemoryError e) {
            e.printStackTrace();
        }

    }

    private void onTimeClicked(TextView time) {
        TimePickerFragment timePickerFragment = new TimePickerFragment();
        timePickerFragment.setTimePickerView(time);
        DialogFragment newFragment = timePickerFragment;
        newFragment.show(this.getSupportFragmentManager(), "TimePicker");
    }

    private void onEditDateClicked(TextView date) {
        DatePickerFragment datePickerFragment = new DatePickerFragment();
        datePickerFragment.setDatePickerView(date);
        DialogFragment newFragment = datePickerFragment;
        newFragment.show(this.getSupportFragmentManager(), "DatePicker");
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            onBackPressed();
            return true;
        } else {
            return false;
        }

    }

    @Override
    public void onBackPressed() {
        if (name.equals("HomeGatekeeperActivity")) {
            gatekeeperHome();
        } else if (name.equals("GateKeeperFragment")) {
            callIntent();
        } else {
            gatekeeperHome();
        }

    }

    private void onUpdateDetails() {
        striName = mVisitorName.getText().toString();
        striVisitTo = mVisitTo.getText().toString();
        striPurpose = mVisitPurpose.getText().toString();
        striVehicleNumber = mVehiclrNumber.getText().toString();
        striMember = mAdditionalMember.getText().toString();
        striContactNumber = mContactNumber.getText().toString();
        striEntryDate = mTextEntryDate.getText().toString();
        striExitDate = mTextExitDate.getText().toString();
        String strEntryTime = mTextEntryTime.getText().toString();
        String strExitTime = mTextExitTime.getText().toString();
        if ((striEntryDate.equals("Entry Date") && (strEntryTime.equals("Entry Time")))) {
            striEntryDate = "";
            // strEntryTime="";
        } else {
            striEntryDate = mTextEntryDate.getText().toString() + "T" + mTextEntryTime.getText().toString();
        }
        if (striExitDate.equals("Exit Date") && (strExitTime.equals("Exit Time"))) {
            striExitDate = "" + "T";
            // strExitTime="";
        } else {
            striExitDate = (mTextExitDate.getText().toString() + "T" + (mTextExitTime.getText().toString()));
        }
        if (ConnectionDetector.isConnectedToInternet(this)) {
            if (spid != null) {
                JSONObject jsonObjectMain = new JSONObject();
                JSONObject jsonObject = new JSONObject();
                try {
                    jsonObject.put("guest_name", striName);
                    jsonObject.put("guest_entry_time", striEntryDate);
                    jsonObject.put("guest_exit_time", striExitDate);
                    jsonObject.put("guest_number", striContactNumber);
                    jsonObject.put("guest_vehicle_number", striVehicleNumber);
                    jsonObject.put("visit_purpose", striPurpose);
                    jsonObject.put("visit_to", striVisitTo);
                    jsonObject.put("plus_person", striMember);
                    jsonObject.put("document_id", doc_id);
                    jsonObject.put("document", encodedImage);
                    jsonObjectMain.put("gatekeeper", jsonObject);
                    //Log.e("jsonObjectMain", "" + jsonObjectMain);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                try {
                    mQueue = LockatedVolleyRequestQueue.getInstance(this).getRequestQueue();
                    String url = ApplicationURL.UpdateGateKeeper + spid + ".json?token="
                            + mLockatedPreferences.getLockatedToken();
                    LockatedJSONObjectRequest lockatedJSONObjectRequest = new LockatedJSONObjectRequest(Request.Method.PUT,
                            url, jsonObjectMain, this, this);
                    lockatedJSONObjectRequest.setTag(REQUEST_TAG);
                    mQueue.add(lockatedJSONObjectRequest);
                } catch (Exception e) {
                    e.printStackTrace();
                }

            } else {
                Toast.makeText(this, "Nothing To Display", Toast.LENGTH_SHORT).show();
            }
        } else {
            Utilities.showToastMessage(this, this.getResources().getString(R.string.internet_connection_error));
        }
    }


    @Override
    public void onErrorResponse(VolleyError error) {
        progressBar.setVisibility(View.GONE);
        LockatedRequestError.onRequestError(this, error);
    }

    @Override
    public void onResponse(JSONObject response) {
        progressBar.setVisibility(View.GONE);
        if (response != null && response.length() > 0) {
            if (response.has("id") && !mLockatedPreferences.getGateKeeper_id().equals("blank")) {
                name = "GateKeeperNotificationDetail";
                try {
                    mVisitorName.setText(response.getString("guest_name"));
                    mVisitPurpose.setText(response.getString("visit_purpose"));
                    mVisitTo.setText(response.getString("visit_to"));
                    mFlatNumber.setText(response.getString("flat"));
                    String strAdditionalMember = response.getString("plus_person");
                    if (strAdditionalMember != null) {
                        mAdditionalMember.setText(strAdditionalMember);
                    } else {
                        mAdditionalMember.setText("0");
                    }
                    String strVehiclenumber = response.getString("guest_vehicle_number");
                    mVehiclrNumber.setText(strVehiclenumber);
                    mContactNumber.setText(response.getString("guest_number"));
                    String strExpDate = response.getString("expected_at");
                    if (strExpDate != null) {
                        mExpectedDate.setText(Utilities.dateTime_AMPM_Format(strExpDate));
                    } else {
                        mExpectedDate.setText("No Date");
                    }
                    String strEntry = response.getString("guest_entry_time");
                    if (strEntry != null) {
                        mTextEntryDate.setText(Utilities.ddMMYYYFormat(strEntry));
                        mTextEntryTime.setText(Utilities.m24TimeFormat(strEntry));
                    } else {
                        mTextEntryDate.setText("Entry Date");
                        mTextEntryTime.setText("Entry Time");
                    }
                    String strExit = response.getString("guest_exit_time");
                    if (strExit != null) {
                        mTextExitDate.setText(Utilities.ddMMYYYFormat(strExit));
                        mTextExitTime.setText(Utilities.m24TimeFormat(strExit));
                    } else {
                        mTextExitDate.setText("Exit Date");
                        mTextExitTime.setText("Exit Time");
                    }
                    approve = response.getString("approve");
                    String accompany = response.getString("accompany");
                    statusChange(approve, accompany);
                    if (response.getJSONArray("documents") != null && response.getJSONArray("documents").length() > 0) {
                        String strUrl = response.getJSONArray("documents").getJSONObject(0).getString("document");
                        Picasso.with(this).load(strUrl).fit().into(mVisitorImage);
                    } else {
                        encodedImage = "";
                        mVisitorImage.setVisibility(View.GONE);
                        textVisitorImage.setVisibility(View.VISIBLE);
                    }
                    mLockatedPreferences.setGateKeeper_id("blank");
                } catch (Exception e) {
                }
            } else if (response.has("id") && mLockatedPreferences.getGateKeeper_id().equals("blank") && name.equals("HomeGatekeeperActivity")) {
                gatekeeperHome();
            } else if (response.has("id") && mLockatedPreferences.getGateKeeper_id().equals("blank") && name.equals("GateKeeperFragment")) {
                callIntent();
            }
        }
    }

    private void gatekeeperHome() {
        Intent homegatekeeper = new Intent(this, HomeGatekeeperActivity.class);
        homegatekeeper.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(homegatekeeper);
        finish();
    }

    private void callIntent() {
        Intent intent = new Intent(this, AdminMyZoneActivity.class);
        intent.putExtra("AdminMyZone", 2);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
        finish();
    }
}
