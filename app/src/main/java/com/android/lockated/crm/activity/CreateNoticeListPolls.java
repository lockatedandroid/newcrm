package com.android.lockated.crm.activity;

import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;

import com.android.lockated.R;
import com.android.lockated.crm.fragment.polls.CreatePollsFragment;

public class CreateNoticeListPolls extends AppCompatActivity {



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_notice_list_polls);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(true);
        getSupportActionBar().setTitle(R.string.polls);
        init();


    }

    private void init() {

        FragmentManager fragmentManager = getSupportFragmentManager();
        CreatePollsFragment checkoutFragment = new CreatePollsFragment();
        checkoutFragment.setFragmentManager(fragmentManager);
        getSupportFragmentManager().beginTransaction().replace(R.id.ContainerList, checkoutFragment).commit();

/*
        CreatePollsFragment checkoutFragment = new CreatePollsFragment();
        getSupportFragmentManager().beginTransaction().replace(R.id.ContainerList, checkoutFragment).commit();
*/
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        switch (item.getItemId())
        {
            case android.R.id.home:
            {
                super.onBackPressed();
                return true;
            }
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
