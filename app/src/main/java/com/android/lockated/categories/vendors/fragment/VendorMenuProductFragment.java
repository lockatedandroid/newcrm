package com.android.lockated.categories.vendors.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.android.lockated.Interfaces.IRecyclerItemClickListener;
import com.android.lockated.R;
import com.android.lockated.categories.adapter.VendorProductAdapter;
import com.android.lockated.categories.vendors.VendorProductActivity;
import com.android.lockated.categories.vendors.model.ProductData;
import com.android.lockated.utils.Utilities;

import java.util.ArrayList;

public class VendorMenuProductFragment extends Fragment implements IRecyclerItemClickListener {

    private View mVendorMenuProductView;
    private RecyclerView mRecyclerView;
    String screenName = "Vendor Product List";
    private ArrayList<ProductData> mVendorProductData;
    private VendorProductAdapter mVendorProductAdapter;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mVendorMenuProductView = inflater.inflate(R.layout.fragment_vendor_menu_product, container, false);
        Utilities.ladooIntegration(getActivity(), screenName);
        Utilities.lockatedGoogleAnalytics(screenName, getString(R.string.visited), screenName);
        init(savedInstanceState);
        return mVendorMenuProductView;
    }

    public void setVendorMenuProduct(ArrayList<ProductData> mProductData) {
        mVendorProductData = mProductData;
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putParcelableArrayList("ProductList", mVendorProductData);
    }

    private void init(Bundle savedInstanceState) {
        if (savedInstanceState != null) {
            mVendorProductData = savedInstanceState.getParcelableArrayList("ProductList");
        }

        mRecyclerView = (RecyclerView) mVendorMenuProductView.findViewById(R.id.mRecyclerView);

        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        mRecyclerView.setLayoutManager(mLayoutManager);
        mRecyclerView.setHasFixedSize(true);

        mVendorProductAdapter = new VendorProductAdapter(mVendorProductData, this);
        mRecyclerView.setAdapter(mVendorProductAdapter);
    }

    @Override
    public void onRecyclerItemClick(View view, int position) {
        ProductData productData = mVendorProductData.get(position);
        switch (view.getId()) {
            case R.id.mImageViewProductQuantityAdd:
                onQuantityAddClicked(productData);
                break;
            case R.id.mImageViewProductQuantityRemove:
                onQuantityRemoveClicked(productData);
                break;

        }
    }

    private void onQuantityAddClicked(ProductData productData) {
        productData.setQuantity(productData.getQuantity() + 1);
        ((VendorProductActivity) getActivity()).productAddToCart(productData);
        mVendorProductAdapter.notifyDataSetChanged();
    }

    private void onQuantityRemoveClicked(ProductData productData) {
        productData.setQuantity(productData.getQuantity() - 1);
        ((VendorProductActivity) getActivity()).productAddToCart(productData);
        mVendorProductAdapter.notifyDataSetChanged();
    }
}