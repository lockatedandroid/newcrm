package com.android.lockated.crm.fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.android.lockated.LockatedApplication;
import com.android.lockated.R;
import com.android.lockated.crm.adapters.SocietyMemberListAdapter;
import com.android.lockated.preferences.LockatedPreferences;
import com.android.lockated.request.LockatedRequestError;
import com.android.lockated.request.LockatedVolleyRequestQueue;
import com.android.lockated.utils.ApplicationURL;
import com.android.lockated.utils.Utilities;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;

import org.json.JSONException;
import org.json.JSONObject;


public class SocietyCommitteeListFragment extends Fragment implements Response.Listener<JSONObject>, Response.ErrorListener {

    private ListView societyMemberList;
    private TextView noData;
    private String SOCIETY_MEMBER = "SocietyCommitteeListFragment";
    LockatedPreferences lockatedPreferences;
    SocietyMemberListAdapter societyMemberListAdapter;
    private ProgressBar progressBarSmall;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View aboutUsView = inflater.inflate(R.layout.fragment_society_members, container, false);
        init(aboutUsView);
        Utilities.ladooIntegration(getActivity(), getActivity().getResources().getString(R.string.committeeMembers));
        return aboutUsView;
    }

    @Override
    public void onResume() {
        super.onResume();
        LockatedApplication.getInstance().trackScreenView(getString(R.string.committeeMembers));
        Utilities.lockatedGoogleAnalytics(getString(R.string.committeeMembers),
                getString(R.string.visited), getString(R.string.committeeMembers));
        getCommiteeMemberList();
    }

    private void init(View aboutUsView) {

        lockatedPreferences = new LockatedPreferences(getActivity());
        progressBarSmall = (ProgressBar) aboutUsView.findViewById(R.id.mProgressBarSmallView);
        societyMemberList = (ListView) aboutUsView.findViewById(R.id.memberList);
        noData = (TextView) aboutUsView.findViewById(R.id.noDetails);
    }

    private void getCommiteeMemberList() {
        if (getActivity() != null) {
            progressBarSmall.setVisibility(View.VISIBLE);
            String url = ApplicationURL.getMangedMembers + lockatedPreferences.getLockatedToken() +
                    ApplicationURL.societyId + lockatedPreferences.getSocietyId();
            LockatedVolleyRequestQueue lockatedVolleyRequestQueue = LockatedVolleyRequestQueue.getInstance(getActivity());
            lockatedVolleyRequestQueue.sendRequest(SOCIETY_MEMBER, Request.Method.GET, url, null, this, this);
        }
    }

    @Override
    public void onErrorResponse(VolleyError error) {
        progressBarSmall.setVisibility(View.GONE);
        if (getActivity() != null) {
            LockatedRequestError.onRequestError(getActivity(), error);
        }
    }

    @Override
    public void onResponse(JSONObject response) {
        progressBarSmall.setVisibility(View.GONE);
        if (response != null) {
            try {
                if (response.has("committee") && response.getJSONArray("committee").length() > 0) {
                    if (getActivity() != null) {
                        try {
                            societyMemberListAdapter = new SocietyMemberListAdapter(getActivity(), response.getJSONArray("committee"));
                            societyMemberList.setAdapter(societyMemberListAdapter);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                } else {

                    societyMemberList.setVisibility(View.GONE);
                    noData.setVisibility(View.VISIBLE);
                    noData.setText(R.string.no_data_error);
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        } else {
            societyMemberList.setVisibility(View.GONE);
            noData.setText(R.string.no_data_error);
            noData.setVisibility(View.VISIBLE);
        }

    }

}
