package com.android.lockated.crm.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;

import com.android.lockated.HomeActivity;
import com.android.lockated.R;
import com.android.lockated.crm.fragment.myzone.VisitorCornerFragment;
import com.android.lockated.preferences.LockatedPreferences;


public class CrmVisitorActivity extends AppCompatActivity {

    private LockatedPreferences mLockatedPreferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        mLockatedPreferences = new LockatedPreferences(this);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_crm_visitor);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(true);
        getSupportActionBar().setTitle(R.string.visitor);
        init();

    }

    private void init() {
        FragmentManager fragmentManager = getSupportFragmentManager();
        VisitorCornerFragment gateKeeperFragment = new VisitorCornerFragment();
        gateKeeperFragment.setFragmentManager(fragmentManager);
        getSupportFragmentManager().beginTransaction().replace(R.id.gatekeeperContainer, gateKeeperFragment).commit();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                callIntent();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        callIntent();
    }

    private void callIntent() {
        Intent intent = new Intent(this, HomeActivity.class);//---------Changes by Bhavesh on 8-9-2016 at 12.50 // ---- //CRMActivity
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
        finish();
    }

}
