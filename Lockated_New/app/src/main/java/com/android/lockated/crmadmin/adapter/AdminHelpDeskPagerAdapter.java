package com.android.lockated.crmadmin.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.android.lockated.crmadmin.fragment.ApprovedComplaintsFragment;
import com.android.lockated.crmadmin.fragment.PendingComplaintsFragment;

public class AdminHelpDeskPagerAdapter extends FragmentPagerAdapter {

    int tabCount, itemPosition;
    FragmentManager fragmentManager;
    String[] nameList;

    public AdminHelpDeskPagerAdapter(FragmentManager fm, int itemPosition, int tabCount, String[] nameList) {
        super(fm);
        fragmentManager = fm;
        this.itemPosition = itemPosition;
        this.tabCount = tabCount;
        this.nameList = nameList;
    }

    @Override
    public Fragment getItem(int pos) {

        Fragment fragment;

        switch (pos) {

            case 0:
                fragment = new PendingComplaintsFragment();
                break;
            case 1:
                fragment = new ApprovedComplaintsFragment();
                break;
            default:
                fragment = new PendingComplaintsFragment();
                break;
        }
        return fragment;
    }

    @Override
    public int getCount() {
        return tabCount;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return nameList[itemPosition];
    }

    @Override
    public int getItemPosition(Object object) {
        return super.getItemPosition(object);
    }

    @Override
    public long getItemId(int position) {
        return super.getItemId(position);
    }

}
