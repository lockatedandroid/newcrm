package com.android.lockated.crm.p2pchat;

import android.content.Context;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.lockated.R;
import com.android.lockated.landing.model.SupportData;
import com.android.lockated.preferences.LockatedPreferences;
import com.android.lockated.utils.Utilities;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.TreeSet;

public class PeerChatAdapter extends BaseAdapter {
    private static final int TYPE_ITEM = 0;
    private static final int TYPE_SEPARATOR = 1;

    private ArrayList<PeerChatData> mData = new ArrayList<>();
    private TreeSet<Integer> sectionHeader = new TreeSet<>();

    private Context mContext;
    private LayoutInflater mInflater;
    private LockatedPreferences mLockatedPreferences;

    public PeerChatAdapter(Context context) {
        mContext = context;
        mInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        mLockatedPreferences = new LockatedPreferences(context);
    }

    public void addItem(final int pos, final PeerChatData item) {
        mData.add(pos, item);
        notifyDataSetChanged();
    }

    public void clearList() {
        mData.clear();
        notifyDataSetChanged();
    }

    public void addSectionHeaderItem(final int pos, final SupportData item) {
        sectionHeader.add(pos);
        notifyDataSetChanged();
    }

    @Override
    public int getItemViewType(int position) {
        return sectionHeader.contains(position) ? TYPE_SEPARATOR : TYPE_ITEM;
    }

    @Override
    public int getViewTypeCount() {
        return 2;
    }

    @Override
    public int getCount() {
        return mData.size();
    }

    @Override
    public PeerChatData getItem(int position) {
        return mData.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder = null;
        int rowType = getItemViewType(position);
        if (convertView == null) {
            holder = new ViewHolder();
            switch (rowType) {
                case TYPE_ITEM:
                    convertView = mInflater.inflate(R.layout.fragment_support_row, null);
                    holder.textView = (TextView) convertView.findViewById(R.id.mTextViewSupportMessage);
                    holder.mTextViewMessageTime = (TextView) convertView.findViewById(R.id.mTextViewMessageTime);
                    holder.mTextViewMessageAgentName = (TextView) convertView.findViewById(R.id.mTextViewMessageAgentName);
                    holder.mLinearLayoutRowSupport = (LinearLayout) convertView.findViewById(R.id.mLinearLayoutRowSupport);
                    holder.mLinearLayoutRowBackground = (LinearLayout) convertView.findViewById(R.id.mLinearLayoutRowBackground);
                    break;
                case TYPE_SEPARATOR:
                    convertView = mInflater.inflate(R.layout.fragment_support_header, null);
                    holder.textView = (TextView) convertView.findViewById(R.id.mTextViewSupportHeader);
                    break;
            }
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        if (rowType == TYPE_ITEM) {
            if (mData.get(position).user_id != Integer.parseInt(mLockatedPreferences.getLockatedUserId())) {
                holder.mLinearLayoutRowSupport.setGravity(Gravity.START);
                holder.mTextViewMessageAgentName.setVisibility(View.VISIBLE);
                holder.mLinearLayoutRowBackground.setBackgroundResource(R.drawable.chat_bubble_left_drawable);
            } else {
                holder.mLinearLayoutRowSupport.setGravity(Gravity.END);
                holder.mTextViewMessageAgentName.setVisibility(View.GONE);
                holder.mLinearLayoutRowBackground.setBackgroundResource(R.drawable.chat_bubble_right_drawable);
            }

            holder.textView.setText(mData.get(position).getBody());
            holder.mTextViewMessageAgentName.setText(mData.get(position).getAgent_name());
            //holder.mTextViewMessageTime.setText(mData.get(position).getCreated_at()+" "+Utilities.dateConvertToTime(mData.get(position).getUpdated_at()));
            holder.mTextViewMessageTime.setText(checkIfToday(mData.get(position).getUpdated_at()));
        } else {
            holder.textView.setText(mData.get(position).getCreated_at());
        }

        return convertView;
    }

    public class ViewHolder {
        public TextView textView;
        public TextView mTextViewMessageTime;
        public TextView mTextViewMessageAgentName;
        public LinearLayout mLinearLayoutRowSupport;
        public LinearLayout mLinearLayoutRowBackground;
    }

    public ArrayList<PeerChatData> getmData() {
        return mData;
    }

    public String checkIfToday(String todayOrNot) {

        if (todayOrNot != null) {
            try {
                Calendar c = Calendar.getInstance();
                SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
                String formattedDate = df.format(c.getTime());
                Date currentDate = df.parse(formattedDate);
                Date respDate = df.parse(todayOrNot);

                /*if (currentDate.equals(respDate)) {
                    todayOrNot = todayOrNot.substring(todayOrNot.indexOf("T") + 1, todayOrNot.length());
                    Log.e("if", ""+todayOrNot);
                } else {*/
                todayOrNot = Utilities.ddMonYYYYFormat(todayOrNot);
                //}
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }

        return todayOrNot;

    }

}
