package com.android.lockated.crmadmin.detailView;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;

import com.android.lockated.R;
import com.android.lockated.crmadmin.activity.AdminActivity;
import com.android.lockated.crmadmin.activity.AdminHelpDeskActivity;
import com.android.lockated.information.AccountController;
import com.android.lockated.model.AccountData;
import com.android.lockated.network.ConnectionDetector;
import com.android.lockated.preferences.LockatedPreferences;
import com.android.lockated.request.LockatedJSONObjectRequest;
import com.android.lockated.request.LockatedRequestError;
import com.android.lockated.request.LockatedVolleyRequestQueue;
import com.android.lockated.utils.ApplicationURL;
import com.android.lockated.utils.ShowImage;
import com.android.lockated.utils.Utilities;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class PendingComplaintsDetailViewActivity extends AppCompatActivity implements AdapterView.OnItemSelectedListener, Response.ErrorListener, Response.Listener<JSONObject>, View.OnClickListener {

    private TextView mFlatNumber;
    private TextView mTextStatus;
    private TextView mPostedBy;
    private TextView mTextCategory;
    private TextView mIssueCategory;
    private TextView mIssueDescription;
    private TextView mPostedOn;
    private TextView mUpdatedOn;
    private ImageView mImageAttachment;
    private TextView mIssueType;
    private TextView mTextAttachment;
    private TextView mCurrentStatus;
    private Spinner mSpinnerStatus;
    String strImage;
    private String spid;
    ProgressBar progressBar;
    private RequestQueue mQueue;
    JSONArray statusArray;
    String strStatus;
    ArrayAdapter<String> arrayadapter;
    ArrayList<AccountData> accountDataArrayList;
    private LockatedPreferences mLockatedPreferences;
    private AccountController accountController;
    public static final String REQUEST_TAG = "PendingComplaintsDetailViewActivity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pending_help_desk_detail_view);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(true);
        getSupportActionBar().setTitle(R.string.helpdesk_detail);
        mLockatedPreferences = new LockatedPreferences(this);
        init();
        setSpinnerCategory();
        DataGetSet();
    }

    private void setSpinnerCategory() {
        int societyid;
        accountController = AccountController.getInstance();
        accountDataArrayList = accountController.getmAccountDataList();
        societyid = accountDataArrayList.get(0).getmResidenceDataList().get(0).getSociety_id();
        if (ConnectionDetector.isConnectedToInternet(this)) {
            progressBar.setVisibility(View.VISIBLE);
            String url = ApplicationURL.getAdminPendingHelpDeskdUrl
                    + mLockatedPreferences.getLockatedToken() +
                    "&society_id" + societyid;
            mQueue = LockatedVolleyRequestQueue.getInstance(this).getRequestQueue();
            LockatedJSONObjectRequest lockatedJSONObjectRequest = new LockatedJSONObjectRequest(Request.Method.GET,
                    url, null, this, this);
            lockatedJSONObjectRequest.setTag(REQUEST_TAG);
            mQueue.add(lockatedJSONObjectRequest);
        } else {
            Utilities.showToastMessage(this, this.getResources().getString(R.string.internet_connection_error));
        }
    }

    private void init() {
        progressBar = (ProgressBar) findViewById(R.id.mProgressBarView);
        mSpinnerStatus = (Spinner) findViewById(R.id.mSpinnerStatus);
        mFlatNumber = (TextView) findViewById(R.id.mFlatNumber);
        mTextStatus = (TextView) findViewById(R.id.mTextStatus);
        mPostedBy = (TextView) findViewById(R.id.mPostedBy);
        mTextCategory = (TextView) findViewById(R.id.mTextCategory);
        mIssueCategory = (TextView) findViewById(R.id.mIssueCategory);
        mPostedOn = (TextView) findViewById(R.id.mPostedOn);
        mUpdatedOn = (TextView) findViewById(R.id.mUpdatedOn);
        mIssueType = (TextView) findViewById(R.id.mIssueType);
        mIssueDescription = (TextView) findViewById(R.id.mIssueDescription);
        mTextAttachment = (TextView) findViewById(R.id.mTextAttachment);
        mCurrentStatus = (TextView) findViewById(R.id.mCurrentStatus);
        mImageAttachment = (ImageView) findViewById(R.id.mImageAttachment);
        mImageAttachment.setOnClickListener(this);
        mSpinnerStatus.setOnItemSelectedListener(this);

    }

    private void DataGetSet() {
        spid = (getIntent().getExtras().getString("pid"));
        mIssueType.setText(getIntent().getExtras().getString("strIssueType"));
        mIssueDescription.setText(getIntent().getExtras().getString("strDescription"));
        mFlatNumber.setText(getIntent().getExtras().getString("FlatNumber"));
        mPostedBy.setText(getIntent().getExtras().getString("strpostedBy"));
        mUpdatedOn.setText(getIntent().getExtras().getString("strmUpdatedOn"));
        mPostedOn.setText(getIntent().getExtras().getString("strPostedOn"));
        mIssueCategory.setText(getIntent().getExtras().getString("strIssueCategory"));
        mTextCategory.setText(getIntent().getExtras().getString("strmTextCategory"));
        mPostedBy.setText(getIntent().getExtras().getString("strpostedBy"));
        strImage = getIntent().getExtras().getString("attachmentImage");
        if (strImage != null && !strImage.equals("No Document")) {
            Picasso.with(this).load("" + strImage).fit().placeholder(R.drawable.loading).into(mImageAttachment);
        } else {
            mTextAttachment.setVisibility(View.VISIBLE);
            mImageAttachment.setVisibility(View.GONE);
        }
        String strStatus = getIntent().getExtras().getString("CurrentStatus");
        if (strStatus != null) {
            mCurrentStatus.setText(getIntent().getExtras().getString("CurrentStatus"));
        } else {
            mCurrentStatus.setText("Pending");
        }

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        strStatus = parent.getSelectedItem().toString();
        //   mCurrentStatus.setText(strStatus);
        String strUpdatedOn = mUpdatedOn.getText().toString();
        mLockatedPreferences = new LockatedPreferences(getApplication());
        if (parent.getSelectedItemPosition() != 0) {
            if (spid != null) {
                JSONObject jsonObjectMain = new JSONObject();
                JSONObject jsonObject = new JSONObject();
                try {
                    jsonObject.put("issue_status", strStatus);
                    jsonObject.put("updated_at", strUpdatedOn);
                    jsonObjectMain.put("complaint", jsonObject);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                try {
                    mQueue = LockatedVolleyRequestQueue.getInstance(getApplication()).getRequestQueue();
                    LockatedJSONObjectRequest lockatedJSONObjectRequest = new LockatedJSONObjectRequest(Request.Method.PUT,
                            ApplicationURL.updatePendingComplaint + spid + ".json?token="
                                    + mLockatedPreferences.getLockatedToken(),
                            jsonObjectMain, new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject response) {
                            progressBar.setVisibility(View.GONE);
                            if (this != null) {
                                try {
                                    if (response.has("id")) {
                                        Intent intent = new Intent(getApplicationContext(), AdminActivity.class);
                                        intent.putExtra("AdminActivityPosition", 4);
                                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                        startActivity(intent);
                                        finish();
                                    }
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            }

                        }
                    }, this);
                    lockatedJSONObjectRequest.setTag(REQUEST_TAG);
                    mQueue.add(lockatedJSONObjectRequest);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else {
            }
        } else {
        }

    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {
    }

    @Override
    public void onErrorResponse(VolleyError error) {
        if (getApplicationContext() != null) {
            progressBar.setVisibility(View.GONE);
            LockatedRequestError.onRequestError(getApplication(), error);
        }
    }

    @Override
    public void onResponse(JSONObject response) {
        progressBar.setVisibility(View.GONE);
        if (this != null) {
            try {
                if (response != null && response.length() > 0) {
                    if (response.has("issue_status")) {
                        statusArray = response.getJSONArray("issue_status");
                        arrayadapter = new ArrayAdapter<String>(this, R.layout.support_simple_spinner_dropdown_item);
                        for (int i = 0; i < response.getJSONArray("issue_status").length(); i++) {
                            arrayadapter.add(statusArray.getString(i));
                        }
                        mSpinnerStatus.setAdapter(arrayadapter);
                    }
                    if (response.has("id")) {
                        Intent intent = new Intent(getApplicationContext(), AdminHelpDeskActivity.class);
                        intent.putExtra("AdminHelpDeskActivity", 0);
                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(intent);
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

    }

    @Override
    public void onClick(View v) {
        Intent intent = new Intent(getApplicationContext(), ShowImage.class);
        intent.putExtra("imageUrlString", strImage);
        startActivity(intent);
    }
}

