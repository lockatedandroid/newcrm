package com.android.lockated.crmadmin.activity;


import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.android.lockated.HomeActivity;
import com.android.lockated.R;
import com.android.lockated.cart.ecommerce.EcommerceCartActivity;
import com.android.lockated.crmadmin.adapter.AdminFacilitiesPagerAdapter;
import com.android.lockated.information.AccountController;
import com.android.lockated.landing.AccountFragment;
import com.android.lockated.landing.HomeFragment;
import com.android.lockated.landing.LocationFragment;
import com.android.lockated.landing.SupportFragment;
import com.android.lockated.model.AccountData;
import com.android.lockated.model.CartDetail;
import com.android.lockated.preferences.LockatedPreferences;
import com.android.lockated.utils.Utilities;

import java.util.ArrayList;

public class AdminFacilitiesActivity extends AppCompatActivity implements View.OnClickListener {

    ViewPager MyZonePager;
    TabLayout tabLayout;
    View lineColor;
    int itemPosition;
    private ImageView mImageViewHome;
    private ImageView mImageViewChat;
    private ImageView mImageViewAccount;
    private ImageView mImageViewLocation;
    private ImageView mImageViewOffers;

    private LockatedPreferences mLockatedPreferences;
    private AccountController mAccountController;

    private static final int REQUEST_CALL = 11;
    LinearLayout linearLayoutContainer;
    String[] nameList = {"Requested Facilities", "Facilities list"};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_admin_facilities);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(true);
        getSupportActionBar().setTitle("Facilities");
        linearLayoutContainer = (LinearLayout) findViewById(R.id.mLandingContainer);
        mAccountController = AccountController.getInstance();
        mLockatedPreferences = new LockatedPreferences(this);
        itemPosition = getIntent().getExtras().getInt("AdminFacilitiesActivity");
        setTabLayout();
        //setBottomTabView();

    }

    public void setTabLayout() {


        lineColor = findViewById(R.id.colorView);
        tabLayout = (TabLayout) findViewById(R.id.tab_layout);
        for (String s : nameList) {
            tabLayout.addTab(tabLayout.newTab().setText(s));
        }
        tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);
        MyZonePager = (ViewPager) findViewById(R.id.noticeBoardPager);
        MyZonePager.setAdapter(new AdminFacilitiesPagerAdapter(getSupportFragmentManager(),
                tabLayout.getTabCount(), itemPosition, nameList));
        MyZonePager.setCurrentItem(itemPosition);
        MyZonePager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
        tabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {

            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                MyZonePager.setCurrentItem(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
    }

    private void setBottomTabView() {
        LinearLayout mLinearLayoutHome = (LinearLayout) findViewById(R.id.mLinearLayoutHome);
        LinearLayout mLinearLayoutSupport = (LinearLayout) findViewById(R.id.mLinearLayoutSupport);
        LinearLayout mLinearLayoutAccount = (LinearLayout) findViewById(R.id.mLinearLayoutAccount);
        LinearLayout mLinearLayoutLocation = (LinearLayout) findViewById(R.id.mLinearLayoutLocation);
        LinearLayout mLinearLayoutOffers = (LinearLayout) findViewById(R.id.mLinearLayoutOffers);

        mImageViewHome = (ImageView) findViewById(R.id.mImageViewHome);
        mImageViewChat = (ImageView) findViewById(R.id.mImageViewChat);
        mImageViewAccount = (ImageView) findViewById(R.id.mImageViewAccount);
        mImageViewLocation = (ImageView) findViewById(R.id.mImageViewLocation);
        mImageViewOffers = (ImageView) findViewById(R.id.mImageViewOffers);

        mLinearLayoutHome.setOnClickListener(this);
        mLinearLayoutSupport.setOnClickListener(this);
        mLinearLayoutAccount.setOnClickListener(this);
        mLinearLayoutLocation.setOnClickListener(this);
        mLinearLayoutOffers.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.mLinearLayoutHome:
                bottomTabClicked(getApplicationContext().getResources().getString(R.string.home_clicked));
                //onHomeClicked();
                break;
            case R.id.mLinearLayoutSupport:
                bottomTabClicked(getApplicationContext().getResources().getString(R.string.chat_support_clicked));
                //onSupportClicked();
                break;
            case R.id.mLinearLayoutAccount:
                bottomTabClicked(getApplicationContext().getResources().getString(R.string.account_clicked));
                //onAccountClicked(0);
                break;
            case R.id.mLinearLayoutLocation:
                bottomTabClicked(getApplicationContext().getResources().getString(R.string.location_clicked));
                //onLocationClicked();
                break;
            case R.id.mLinearLayoutOffers:
                bottomTabClicked(getApplicationContext().getResources().getString(R.string.offer_clicked));
                //onOfferClicked();
                break;
        }
    }

    private void bottomTabClicked(String name) {
        Intent intent = new Intent(this, HomeActivity.class);
        intent.putExtra("HomeActivityCalled", name);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
        finish();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.home, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_cart:
                onCartClicked();
                break;
            case android.R.id.home: {
                onBackPressed();
                return true;
            }
            default:
                return super.onOptionsItemSelected(item);
        }
        return super.onOptionsItemSelected(item);
    }

    private void onCartClicked() {
        ArrayList<AccountData> accountDatas = AccountController.getInstance().getmAccountDataList();
        if (accountDatas.size() > 0) {
            ArrayList<CartDetail> cartDetails = accountDatas.get(0).getCartDetailList();
            if (cartDetails.size() > 0) {
                if (cartDetails.get(0).getmLineItemData().size() > 0) {
                    Intent intent = new Intent(this, EcommerceCartActivity.class);
                    startActivity(intent);
                } else {
                    Utilities.showToastMessage(this, getString(R.string.empty_cart));
                }
            } else {
                Utilities.showToastMessage(this, getString(R.string.empty_cart));
            }
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        /*Intent intent = new Intent(this, HomeActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
        finish();*/
    }

}