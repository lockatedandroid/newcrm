
package com.android.lockated.model.usermodel.Directory.MembersDirectory;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class MembersDirectory {

    @SerializedName("user_societies")
    @Expose
    private ArrayList<UserSociety> userSocieties = new ArrayList<UserSociety>();

    /**
     * 
     * @return
     *     The userSocieties
     */
    public ArrayList<UserSociety> getUserSocieties() {
        return userSocieties;
    }

    /**
     * 
     * @param userSocieties
     *     The user_societies
     */
    public void setUserSocieties(ArrayList<UserSociety> userSocieties) {
        this.userSocieties = userSocieties;
    }

}
