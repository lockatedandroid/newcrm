package com.android.lockated.crmadmin.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.TextView;

import com.android.lockated.R;
import com.android.lockated.network.ConnectionDetector;
import com.android.lockated.preferences.LockatedPreferences;
import com.android.lockated.request.LockatedJSONObjectRequest;
import com.android.lockated.request.LockatedRequestError;
import com.android.lockated.request.LockatedVolleyRequestQueue;
import com.android.lockated.utils.ApplicationURL;
import com.android.lockated.utils.ShowImage;
import com.android.lockated.utils.Utilities;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

public class AdminEventDetailActivity extends AppCompatActivity implements View.OnClickListener, Response.Listener<JSONObject>, Response.ErrorListener {

    private TextView mCreatedAtDate;
    private TextView mEventAt;
    private TextView mDescription;
    private RadioButton mAttendingRadioButton;
    private RadioButton mNotAttendingRadioButton;
    private TextView mClose;
    private TextView mPublish;
    private TextView mDeny;
    private TextView mFlatNo;
    private TextView mSubject;
    private TextView mCreatedBY;
    private TextView mStartDateTime;
    private TextView mEndDateTime;
    private TextView mTextAttachment;
    private ImageView mImageAttachment;
    boolean isRsvpSelected = false;
    ProgressBar mProgressBarView;
    String rsvpValue, eventId, rsvpOptionSelected;
    String strImage;
    //String strId;
    private RequestQueue mQueue;
    private LockatedPreferences mLockatedPreferences;
    public static final String REQUEST_TAG = "PendingEventAdapter";
    String UPDATE_RSVP = "EventDetailActivity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_admin_event_detail);

        init();
        buttonVisiblity();
        setToolBarTitle("Events");
        displayData();

    }

    private void buttonVisiblity() {
        if (getIntent().getExtras() != null && getIntent().getExtras().getString("pendin_publish_value") != null) {
            String pendinPublishValue = getIntent().getExtras().getString("pendin_publish_value");
            if (pendinPublishValue.equals("1")) {
                mPublish.setVisibility(View.VISIBLE);
            } else {
                mPublish.setVisibility(View.GONE);
            }
        }
    }

    private void init() {

        mLockatedPreferences = new LockatedPreferences(this);
        mProgressBarView = (ProgressBar) findViewById(R.id.mProgressBarView);
        mFlatNo = (TextView) findViewById(R.id.mFlatNo);
        mSubject = (TextView) findViewById(R.id.mSubject);
        mCreatedBY = (TextView) findViewById(R.id.mCreatedBY);
        mStartDateTime = (TextView) findViewById(R.id.mStartDateTime);
        mEndDateTime = (TextView) findViewById(R.id.mEndDateTime);
        mDescription = (TextView) findViewById(R.id.mDescription);
        mCreatedAtDate = (TextView) findViewById(R.id.mCreatedAtDate);
        mEventAt = (TextView) findViewById(R.id.mEventAt);
        mTextAttachment = (TextView) findViewById(R.id.mTextAttachment);
        mImageAttachment = (ImageView) findViewById(R.id.mImageAttachment);

        mClose = (TextView) findViewById(R.id.mClose);
        mPublish = (TextView) findViewById(R.id.mPublish);
        mDeny = (TextView) findViewById(R.id.mDeny);

        mClose.setOnClickListener(this);
        mPublish.setOnClickListener(this);
        mDeny.setOnClickListener(this);
        mImageAttachment.setOnClickListener(this);
        mAttendingRadioButton = (RadioButton) findViewById(R.id.mAttendingRadioButton);
        mAttendingRadioButton.setOnClickListener(this);
        mNotAttendingRadioButton = (RadioButton) findViewById(R.id.mNotAttendingRadioButton);
        mNotAttendingRadioButton.setOnClickListener(this);

    }

    public void setToolBarTitle(String titleName) {

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(true);
        getSupportActionBar().setTitle(titleName);

    }

    private void displayData() {
        if (mLockatedPreferences.getEvent_id() != null) {
            if (!mLockatedPreferences.getEvent_id().equals("blank")) {
                getPendingEvents(mLockatedPreferences.getEvent_id());
            } else {
                try {
                    if (getIntent().getExtras() != null) {
                        JSONObject jsonObjectDetail = new JSONObject(getIntent().getExtras().getString("EventDetailViewData"));

                        if (!jsonObjectDetail.getString("username").equals("null") || !jsonObjectDetail.getString("username").equals("")) {
                            mCreatedBY.setText(jsonObjectDetail.getString("username"));
                        } else {
                            mCreatedBY.setText("Name not available");
                        }
                        if (jsonObjectDetail.getString("event_name") != "null") {
                            mSubject.setText(jsonObjectDetail.getString("event_name"));
                        } else {
                            mSubject.setText("No Subject");
                        }
                        if (jsonObjectDetail.getString("flat") != "null") {
                            //   mFlatNo.setText(jsonObjectDetail.getString("flat"));
                            mFlatNo.setText("No Flat");
                        } else {
                            mFlatNo.setText("No Flat");

                        }
                        mStartDateTime.setText(Utilities.ddMonTimeFormat(jsonObjectDetail.getString("from_time")));
                        mEndDateTime.setText(Utilities.ddMonTimeFormat(jsonObjectDetail.getString("to_time")));
                        if (jsonObjectDetail.getString("description") != "null") {
                            mDescription.setText(jsonObjectDetail.getString("description"));
                        } else {
                            mDescription.setText("No Description");
                        }
                        mCreatedAtDate.setText("Created On - " + Utilities.dateMonFormat(jsonObjectDetail.getString("created_at")));
                        mEventAt.setText(jsonObjectDetail.getString("event_at"));
                        eventId = jsonObjectDetail.getString("id");
                        rsvpValue = jsonObjectDetail.getString("rsvp");
                        if (rsvpValue.equals("1")) {
                            mAttendingRadioButton.setChecked(true);
                        } else if (rsvpValue.equals("0")) {
                            mNotAttendingRadioButton.setChecked(true);
                        }
                        if (!jsonObjectDetail.getString("document").equals("No Document")) {
                            strImage = jsonObjectDetail.getString("document");
                            Picasso.with(this).load("" + strImage).fit().placeholder(R.drawable.loading).into(mImageAttachment);
                        } else {
                            mTextAttachment.setVisibility(View.VISIBLE);
                            mImageAttachment.setVisibility(View.GONE);
                        }
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public void getPendingEvents(String pid) {
        if (ConnectionDetector.isConnectedToInternet(getApplicationContext())) {
            mProgressBarView.setVisibility(View.VISIBLE);
            mQueue = LockatedVolleyRequestQueue.getInstance(getApplicationContext()).getRequestQueue();
            String url = "https://www.lockated.com/events/" + pid + ".json?token=" + mLockatedPreferences.getLockatedToken()
                    + "&id_society=" + mLockatedPreferences.getSocietyId();
            //Log.e("getPendingEvents", "" + url);
            LockatedJSONObjectRequest lockatedJSONObjectRequest = new LockatedJSONObjectRequest(Request.Method.GET,
                    url, null, this, this);
            lockatedJSONObjectRequest.setTag(REQUEST_TAG);
            mQueue.add(lockatedJSONObjectRequest);
        } else {
            Utilities.showToastMessage(getApplicationContext(), getApplicationContext().getResources().getString(R.string.internet_connection_error));
        }

    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {

            case R.id.mAttendingRadioButton:

                rsvpOptionSelected = "1";
                isRsvpSelected = true;
                mNotAttendingRadioButton.setChecked(false);
                mAttendingRadioButton.setChecked(true);

                break;

            case R.id.mNotAttendingRadioButton:

                rsvpOptionSelected = "0";
                isRsvpSelected = true;
                mNotAttendingRadioButton.setChecked(true);
                mAttendingRadioButton.setChecked(false);

                break;
            case R.id.mImageAttachment:
                Intent intent = new Intent(getApplicationContext(), ShowImage.class);
                intent.putExtra("imageUrlString", strImage);
                startActivity(intent);
                break;
            case R.id.mClose:
                onBackClick();
                break;
            case R.id.mPublish:
                onPublishClicked();
                break;
            case R.id.mDeny:
                onDenyClicked();
                break;

        }

    }

    private void onDenyClicked() {
        request(null, ApplicationURL.denyEvent + mLockatedPreferences.getLockatedToken()
                + "&id=" + eventId, eventId);
    }

    private void onPublishClicked() {
        request(null, ApplicationURL.publishEvent + mLockatedPreferences.getLockatedToken()
                + "&id=" + eventId, eventId);
    }

    public void request(JSONObject jsonObject, String url, String id) {
        mProgressBarView.setVisibility(View.VISIBLE);
        if (ConnectionDetector.isConnectedToInternet(this)) {
            mLockatedPreferences = new LockatedPreferences(this);
            mQueue = LockatedVolleyRequestQueue.getInstance(this).getRequestQueue();
            LockatedJSONObjectRequest lockatedJSONObjectRequest = new LockatedJSONObjectRequest(Request.Method.GET,
                    url, jsonObject, this, this);
            lockatedJSONObjectRequest.setTag(REQUEST_TAG);
            mQueue.add(lockatedJSONObjectRequest);
        } else {
            Utilities.showToastMessage(this, this.getResources().getString(R.string.internet_connection_error));
        }
    }

    /* private void updateRsvp(String rsvpOptionSelected) {
         if (ConnectionDetector.isConnectedToInternet(this)) {
             mProgressBarView.setVisibility(View.VISIBLE);
             String url = ApplicationURL.updateEventsUrl + mLockatedPreferences.getLockatedToken();
             LockatedVolleyRequestQueue mLockatedVolleyRequestQueue = LockatedVolleyRequestQueue.getInstance(this);
             Log.e("updateRsvp url", "" + url);
             JSONObject jsonObject = new JSONObject();
             JSONObject jsonObjectMain = new JSONObject();
             try {
                 jsonObject.put("event_id", eventId);
                 jsonObject.put("rsvp", rsvpOptionSelected);
                 jsonObjectMain.put("event_rsvp", jsonObject);
                 Log.e("jsonObjectMain", "" + jsonObjectMain);
             } catch (JSONException e) {
                 e.printStackTrace();
             }
             mLockatedVolleyRequestQueue.sendRequest(UPDATE_RSVP, Request.Method.POST, url, jsonObjectMain, this, this);
         } else {
             Utilities.showToastMessage(this, this.getResources().getString(R.string.internet_connection_error));
         }
     }*/

    @Override
    public void onResponse(JSONObject response) {
        mProgressBarView.setVisibility(View.GONE);
        //Log.e("resp", "" + response);
        try {
            if (response != null && response.length() > 0) {
                try {
                    JSONObject jsonObjectDetail = response;
                    if (response.has("id") && !mLockatedPreferences.getEvent_id().equals("blank")) {
                        if (jsonObjectDetail.getJSONObject("user") != null) {
                            JSONObject jsonObject = jsonObjectDetail.getJSONObject("user");
                            if (jsonObject.getString("firstname") != null && jsonObject.getString("lastname") != null) {
                                mCreatedBY.setText(jsonObject.getString("firstname") + " " + jsonObject.getString("lastname"));
                            } else {
                                mCreatedBY.setText("Name not available");
                            }
                        }
                        if (jsonObjectDetail.getString("event_name") != "null") {
                            mSubject.setText(jsonObjectDetail.getString("event_name"));
                        } else {
                            mSubject.setText("No Subject");
                        }
                        mStartDateTime.setText(Utilities.ddMonTimeFormat(jsonObjectDetail.getString("from_time")));
                        mEndDateTime.setText(Utilities.ddMonTimeFormat(jsonObjectDetail.getString("to_time")));
                        if (jsonObjectDetail.getString("description") != "null") {
                            mDescription.setText(jsonObjectDetail.getString("description"));
                        } else {
                            mDescription.setText("No Description");
                        }
                        mCreatedAtDate.setText("Created On - " + Utilities.dateMonFormat(jsonObjectDetail.getString("created_at")));
                        mEventAt.setText(jsonObjectDetail.getString("event_at"));
                        eventId = jsonObjectDetail.getString("id");
                        if (jsonObjectDetail.getJSONArray("documents") != null && jsonObjectDetail.getJSONArray("documents").length() > 0) {
                            strImage = jsonObjectDetail.getJSONArray("documents").getJSONObject(0).getString("document");
                            Picasso.with(this).load("" + strImage).fit().placeholder(R.drawable.loading).into(mImageAttachment);
                        } else {
                            mTextAttachment.setVisibility(View.VISIBLE);
                            mImageAttachment.setVisibility(View.GONE);
                        }
                        mLockatedPreferences.setEvent_id("blank");
                    } else if (response.has("id") && mLockatedPreferences.getEvent_id().equals("blank")) {
                        Intent intent = new Intent(getApplicationContext(), AdminEventsActivity.class);
                        intent.putExtra("AdminEvents", 0);
                        startActivity(intent);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onErrorResponse(VolleyError error) {
        mProgressBarView.setVisibility(View.GONE);
        LockatedRequestError.onRequestError(this, error);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            onBackClick();
            return true;
        } else {
            return false;
        }
    }

    private void onBackClick() {
        Intent intent = new Intent(getApplicationContext(), AdminEventsActivity.class);
        intent.putExtra("AdminEvents", 0);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
        finish();
    }
}
