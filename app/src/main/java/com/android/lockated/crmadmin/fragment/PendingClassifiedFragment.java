package com.android.lockated.crmadmin.fragment;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.android.lockated.LockatedApplication;
import com.android.lockated.R;
import com.android.lockated.crmadmin.activity.AdminMyZoneActivity;
import com.android.lockated.crmadmin.activity.PendingSellClassifiesDetailActivity;
import com.android.lockated.information.AccountController;
import com.android.lockated.model.AccountData;
import com.android.lockated.model.AdminModel.AdminClassified.PendingClassified.AdminClassified;
import com.android.lockated.network.ConnectionDetector;
import com.android.lockated.preferences.LockatedPreferences;
import com.android.lockated.request.LockatedJSONObjectRequest;
import com.android.lockated.request.LockatedRequestError;
import com.android.lockated.request.LockatedVolleyRequestQueue;
import com.android.lockated.utils.ApplicationURL;
import com.android.lockated.utils.Utilities;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.google.gson.Gson;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class PendingClassifiedFragment extends Fragment implements Response.ErrorListener,
        Response.Listener<JSONObject> {
    String pid;
    int societyid;
    ListView SellList;
    TextView errorMsg;
    private View SellView;
    private RequestQueue mQueue;
    private ProgressBar progressBar;
    AccountController accountController;
    ArrayList<AdminClassified> adminClassifieds;
    ArrayList<AccountData> accountDataArrayList;
    private LockatedPreferences mLockatedPreferences;
    PendingClassifiedAdapter pendingClassifiedAdapter;
    public static final String REQUEST_TAG = "PendingClassifiedFragment";
    String SHOW, CREATE, INDEX, UPDATE, EDIT;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        SellView = inflater.inflate(R.layout.fragment_pending_classified, container, false);
        init(SellView);
        Utilities.ladooIntegration(getActivity(), getActivity().getResources().getString(R.string.pendingClassifieds));
        return SellView;
    }

    @Override
    public void onResume() {
        super.onResume();
        LockatedApplication.getInstance().trackScreenView(getString(R.string.pendingClassifieds));
        Utilities.lockatedGoogleAnalytics(getString(R.string.pendingClassifieds),
                getString(R.string.visited), getString(R.string.pendingClassifieds));
    }

    private void init(View sellView) {
        adminClassifieds = new ArrayList<>();
        mLockatedPreferences = new LockatedPreferences(getActivity());
        SellList = (ListView) sellView.findViewById(R.id.listView);
        errorMsg = (TextView) sellView.findViewById(R.id.noNotices);
        getClasssifiedRole();
        checkPermission();
        pendingClassifiedAdapter = new PendingClassifiedAdapter(getActivity(), adminClassifieds);
        SellList.setAdapter(pendingClassifiedAdapter);
    }

    private void checkPermission() {
        if (SHOW!=null&&SHOW.equals("true")) {
            getPendingClassifieds();
        } else {
            SellList.setVisibility(View.GONE);
            errorMsg.setVisibility(View.VISIBLE);
            errorMsg.setText(R.string.no_permission_error);
        }
    }

    private void getPendingClassifieds() {
        accountController = AccountController.getInstance();
        accountDataArrayList = accountController.getmAccountDataList();
        societyid = accountDataArrayList.get(0).getmResidenceDataList().get(0).getSociety_id();
        progressBar = (ProgressBar) SellView.findViewById(R.id.mProgressBarView);
        progressBar.setVisibility(View.VISIBLE);
        if (ConnectionDetector.isConnectedToInternet(getActivity())) {
            mLockatedPreferences = new LockatedPreferences(getActivity());
            String url = ApplicationURL.getPendingClassified + mLockatedPreferences.getLockatedToken() + ApplicationURL.societyId + societyid;
            mQueue = LockatedVolleyRequestQueue.getInstance(getActivity()).getRequestQueue();
            LockatedJSONObjectRequest lockatedJSONObjectRequest = new LockatedJSONObjectRequest(Request.Method.GET,
                    url, null, this, this);
            lockatedJSONObjectRequest.setTag(REQUEST_TAG);
            mQueue.add(lockatedJSONObjectRequest);
        } else {
            Utilities.showToastMessage(getActivity(), getActivity().getResources().getString(R.string.internet_connection_error));
        }

    }

    @Override
    public void onErrorResponse(VolleyError error) {
        progressBar.setVisibility(View.GONE);
        if (getActivity() != null) {
            LockatedRequestError.onRequestError(getActivity(), error);
        }
    }

    @Override
    public void onResponse(JSONObject response) {
        progressBar.setVisibility(View.GONE);
        if (getActivity() != null) {
            try {
                if (response != null && response.getJSONArray("classifieds").length() > 0) {
                    JSONArray jsonArray = response.getJSONArray("classifieds");
                    Gson gson = new Gson();
                    for (int i = 0; i < jsonArray.length(); i++) {
                        AdminClassified classified = gson.fromJson(jsonArray.getJSONObject(i).toString(), AdminClassified.class);
                        adminClassifieds.add(classified);
                    }
                    pendingClassifiedAdapter.notifyDataSetChanged();
                } else {
                    messagesToDisplay();
                }

            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            messagesToDisplay();
        }
    }

    private void messagesToDisplay() {
        SellList.setVisibility(View.GONE);
        errorMsg.setVisibility(View.VISIBLE);
        errorMsg.setText(R.string.no_data_error);
    }

    private class PendingClassifiedAdapter extends BaseAdapter implements View.OnClickListener,
            Response.ErrorListener, Response.Listener<JSONObject> {

        private final ArrayList<AdminClassified> classifieds;
        Context contextMain;
        ProgressDialog mProgressDialog;
        JSONArray jsonArray;
        LayoutInflater layoutInflater;

        public PendingClassifiedAdapter(Context context, ArrayList<AdminClassified> classifieds) {
            this.contextMain = context;
            this.classifieds = classifieds;
            layoutInflater = LayoutInflater.from(context);
        }

        @Override
        public int getCount() {
            return classifieds.size();
        }

        @Override
        public Object getItem(int position) {
            return position;
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public void onClick(View v) {
            int pos = (int) v.getTag();
            switch (v.getId()) {
                case R.id.mPublish:
                    PublishClassified(pos);
                    break;
            }
        }

        private void PublishClassified(int pos) {
           /* progressBar.setVisibility(View.VISIBLE);*/
            mProgressDialog = ProgressDialog.show(getActivity(), "", "Please Wait...", false);
            mProgressDialog.show();
            if (getActivity() != null) {
                if (ConnectionDetector.isConnectedToInternet(getActivity())) {
                    mQueue = LockatedVolleyRequestQueue.getInstance(getActivity()).getRequestQueue();
                    String url = ApplicationURL.PublishClassified + classifieds.get(pos).getId()
                            + "&token=" + mLockatedPreferences.getLockatedToken();
                    LockatedJSONObjectRequest lockatedJSONObjectRequest = new LockatedJSONObjectRequest(Request.Method.GET,
                            url, null, this, this);
                    lockatedJSONObjectRequest.setTag(REQUEST_TAG);
                    mQueue.add(lockatedJSONObjectRequest);
                } else {
                    Utilities.showToastMessage(getActivity(), getActivity().getResources().getString(R.string.internet_connection_error));
                }
            }
        }

        @Override
        public void onResponse(JSONObject response) {
            /*progressBar.setVisibility(View.GONE);*/
            mProgressDialog.dismiss();
            if (response.has("id") && response.has("publish")) {
                try {
                    if (response.getString("publish").equals("1")) {
                        Intent intent = new Intent(contextMain, AdminMyZoneActivity.class);
                        intent.putExtra("AdminMyZone", 0);
                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        contextMain.startActivity(intent);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

        }

        @Override
        public void onErrorResponse(VolleyError error) {
            /*progressBar.setVisibility(View.GONE);*/
            mProgressDialog.dismiss();
            if (getActivity() != null) {
                LockatedRequestError.onRequestError(getActivity(), error);
            }
        }

        private class ViewHolder {
            TextView mThingToRent, mTextTO, mTextRate, mTextName, mTextDate, mDescription;
            TextView mViewDetails, mPublish, categoryValue;
            ImageView msellImage;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {

            ViewHolder holder = null;
            if (convertView == null) {
                convertView = layoutInflater.inflate(R.layout.admin_pending_sell_classified_child, parent, false);
                holder = new ViewHolder();
                convertView.setTag(holder);
            } else {
                holder = (ViewHolder) convertView.getTag();
            }
            holder.mThingToRent = (TextView) convertView.findViewById(R.id.mThingToRent);
            holder.msellImage = (ImageView) convertView.findViewById(R.id.msellImage);
            holder.mTextTO = (TextView) convertView.findViewById(R.id.mTextTO);
            holder.mTextRate = (TextView) convertView.findViewById(R.id.mTextRate);
            holder.mTextName = (TextView) convertView.findViewById(R.id.mTextName);
            holder.mTextDate = (TextView) convertView.findViewById(R.id.mTextDate);
            holder.mViewDetails = (TextView) convertView.findViewById(R.id.mViewDetails);
            holder.mPublish = (TextView) convertView.findViewById(R.id.mPublish);
            holder.categoryValue = (TextView) convertView.findViewById(R.id.categoryValue);
            try {
                holder.mThingToRent.setText(classifieds.get(position).getTitle());
                holder.mTextTO.setText(classifieds.get(position).getPurpose());
                holder.mTextRate.setText(classifieds.get(position).getPrice());
                holder.categoryValue.setText(classifieds.get(position).getClassifiedCategory());
                holder.mTextDate.setText(classifieds.get(position).getExpire());
                Picasso.with(contextMain)
                        .load("" + classifieds.get(position).getMedium())
                        .fit()
                        .into(holder.msellImage);

            } catch (Exception e) {
                e.printStackTrace();

            }
            holder.mViewDetails.setTag(position);
            holder.mViewDetails.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int position = (int) v.getTag();
                    Intent intent = new Intent(getActivity(), PendingSellClassifiesDetailActivity.class);
                    try {
                        intent.putExtra("strThing", classifieds.get(position).getTitle());
                        intent.putExtra("strThingTO", classifieds.get(position).getPurpose());
                        intent.putExtra("strRate", classifieds.get(position).getPrice());
                        intent.putExtra("strDescription", classifieds.get(position).getDescription());
                        intent.putExtra("strDate", classifieds.get(position).getExpire());
                        intent.putExtra("ImagePicaso", classifieds.get(position).getOriginal());
                        intent.putExtra("categoryValue", classifieds.get(position).getClassifiedCategory());
                        intent.putExtra("strStatus", classifieds.get(position).getStatus());
                        intent.putExtra("pid", "" + classifieds.get(position).getId());
                        if (classifieds.get(position).getUser() != null) {
                            intent.putExtra("name", classifieds.get(position).getUser().getFirstname() + classifieds.get(position).getUser().getLastname());
                            intent.putExtra("mobile", classifieds.get(position).getUser().getMobile());
                            intent.putExtra("email", classifieds.get(position).getUser().getEmail());
                        } else {
                            intent.putExtra("name", "Not Available");
                            intent.putExtra("mobile", "Not Available");
                            intent.putExtra("email", "Not Available");
                        }
                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(intent);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            });
            holder.mPublish.setTag(position);
            holder.mPublish.setOnClickListener(this);
            return convertView;
        }

    }

    public String getClasssifiedRole() {
        String mySocietyRoles = getActivity().getResources().getString(R.string.blank_value);
        if (mLockatedPreferences.getRolesJson() != null && !mLockatedPreferences.getRolesJson().equals("blank")) {
            try {
                JSONObject noticeJsonObj = new JSONObject(mLockatedPreferences.getRolesJson());
                for (int i = 0; i < noticeJsonObj.getJSONArray(getActivity().getResources().getString(R.string.permissions_value)).length(); i++) {
                    JSONObject jsonObject1 = noticeJsonObj.getJSONArray(getActivity().getResources().getString(R.string.permissions_value)).getJSONObject(i);
                    if (jsonObject1.has(getActivity().getResources().getString(R.string.section_value))
                            && jsonObject1.getString(getActivity().getResources().getString(R.string.section_value)).equals("spree_classifieds")) {
                        if (jsonObject1.getJSONObject(getActivity().getResources().getString(R.string.permission_value)).has(getActivity().getResources().getString(R.string.index_value))) {
                            mySocietyRoles = jsonObject1.getJSONObject(getActivity().getResources().getString(R.string.permission_value)).getString(getActivity().getResources().getString(R.string.index_value));
                            CREATE = jsonObject1.getJSONObject(getActivity().getResources().getString(R.string.permission_value)).getString("create");
                            INDEX = jsonObject1.getJSONObject(getActivity().getResources().getString(R.string.permission_value)).getString("index");
                            UPDATE = jsonObject1.getJSONObject(getActivity().getResources().getString(R.string.permission_value)).getString("update");
                            EDIT = jsonObject1.getJSONObject(getActivity().getResources().getString(R.string.permission_value)).getString("edit");
                            SHOW = jsonObject1.getJSONObject(getActivity().getResources().getString(R.string.permission_value)).getString("show");
                        }
                    }
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return mySocietyRoles;
    }

}