package com.android.lockated.categories.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.android.lockated.Interfaces.IRecyclerItemClickListener;
import com.android.lockated.R;
import com.android.lockated.categories.vendors.model.VendorProductData;
import com.android.lockated.holder.RecyclerViewHolder;

import java.util.ArrayList;

public class VendorMenuAdapter extends RecyclerView.Adapter<RecyclerViewHolder>
{
    private ArrayList<VendorProductData> mVendorProductData;
    private IRecyclerItemClickListener mIRecyclerItemClickListener;

    public VendorMenuAdapter(ArrayList<VendorProductData> data, IRecyclerItemClickListener listener)
    {
        mVendorProductData = data;
        mIRecyclerItemClickListener = listener;
    }

    @Override
    public RecyclerViewHolder onCreateViewHolder(ViewGroup parent, int viewType)
    {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.fragment_category_view_row, parent, false);
        return new RecyclerViewHolder(itemView, mIRecyclerItemClickListener);
    }

    @Override
    public void onBindViewHolder(RecyclerViewHolder holder, int position)
    {
        holder.onVendorMenuViewHolder(mVendorProductData.get(position));
    }

    @Override
    public int getItemCount()
    {
        return mVendorProductData.size();
    }
}
