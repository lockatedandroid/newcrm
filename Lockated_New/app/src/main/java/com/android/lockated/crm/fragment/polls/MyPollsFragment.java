package com.android.lockated.crm.fragment.polls;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.android.lockated.LockatedApplication;
import com.android.lockated.R;
import com.android.lockated.crm.activity.CreateNoticeListPolls;
import com.android.lockated.crm.adapters.MyPollAdapter;
import com.android.lockated.information.AccountController;
import com.android.lockated.model.AccountData;
import com.android.lockated.model.usermodel.Polls.MyPoll;
import com.android.lockated.network.ConnectionDetector;
import com.android.lockated.preferences.LockatedPreferences;
import com.android.lockated.request.LockatedJSONObjectRequest;
import com.android.lockated.request.LockatedRequestError;
import com.android.lockated.request.LockatedVolleyRequestQueue;
import com.android.lockated.utils.ApplicationURL;
import com.android.lockated.utils.Utilities;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class MyPollsFragment extends Fragment implements View.OnClickListener, Response.ErrorListener, Response.Listener<JSONObject> {
    public TextView errorMsg;
    ProgressBar progressBar;
    RecyclerView pollsList;
    FloatingActionButton fab;
    String societyid;
    AccountController accountController;
    ArrayList<AccountData> accountDataArrayList;
    ArrayList<MyPoll> myPolls;
    MyPollAdapter adminPollsAdapter;
    private RequestQueue mQueue;
    private LockatedPreferences mLockatedPreferences;
    public static final String REQUEST_TAG = "MyPollsFragment";

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        Utilities.ladooIntegration(getActivity(), "My Polls");
        View pollView = inflater.inflate(R.layout.fragment_my_polls, container, false);
        init(pollView);
        return pollView;

    }

    @Override
    public void onResume() {
        super.onResume();
        LockatedApplication.getInstance().trackScreenView(getString(R.string.my_polls));
        Utilities.lockatedGoogleAnalytics(getString(R.string.my_polls),
                getString(R.string.visited), getString(R.string.my_polls));
        myPolls.clear();
        getMyPolls();
    }

    private void init(View view) {
        myPolls = new ArrayList<>();
        mLockatedPreferences = new LockatedPreferences(getActivity());
        pollsList = (RecyclerView) view.findViewById(R.id.noticeList);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        pollsList.setLayoutManager(linearLayoutManager);
        errorMsg = (TextView) view.findViewById(R.id.noNotices);
        progressBar = (ProgressBar) view.findViewById(R.id.mProgressBarView);
        adminPollsAdapter = new MyPollAdapter(getActivity(), myPolls, getChildFragmentManager());
        pollsList.setAdapter(adminPollsAdapter);
        fab = (FloatingActionButton) view.findViewById(R.id.fab);
        fab.setOnClickListener(this);

    }

    public void getMyPolls() {
        /*accountController = AccountController.getInstance();
        accountDataArrayList = accountController.getmAccountDataList();
        societyid = accountDataArrayList.get(0).getmResidenceDataList().get(0).getSociety_id();*/
        societyid = mLockatedPreferences.getSocietyId();

        if (ConnectionDetector.isConnectedToInternet(getActivity())) {
            progressBar.setVisibility(View.VISIBLE);
            mQueue = LockatedVolleyRequestQueue.getInstance(getActivity()).getRequestQueue();
            String url = ApplicationURL.getUseCreatedPolls + societyid + "&token=" + mLockatedPreferences.getLockatedToken();
            LockatedJSONObjectRequest lockatedJSONObjectRequest = new LockatedJSONObjectRequest(Request.Method.GET,
                    url, null, this, this);
            lockatedJSONObjectRequest.setTag(REQUEST_TAG);
            mQueue.add(lockatedJSONObjectRequest);
        } else {
            Utilities.showToastMessage(getActivity(), getActivity().getResources().getString(R.string.internet_connection_error));
        }
    }

    @Override
    public void onErrorResponse(VolleyError error) {
        if (getActivity() != null) {
            progressBar.setVisibility(View.GONE);
            LockatedRequestError.onRequestError(getActivity(), error);
        }
    }

    @Override
    public void onResponse(JSONObject response) {
        if (getActivity() != null) {
            progressBar.setVisibility(View.GONE);
            try {
                if (response != null && response.has("polls") && response.getJSONArray("polls").length() > 0) {
                    JSONArray jsonArray = response.getJSONArray("polls");
                    Gson gson = new Gson();
                    for (int i = 0; i < jsonArray.length(); i++) {
                        MyPoll myPoll = gson.fromJson(jsonArray.getJSONObject(i).toString(), MyPoll.class);
                        myPolls.add(myPoll);
                    }
                    adminPollsAdapter.notifyDataSetChanged();
                } else {
                    pollsList.setVisibility(View.GONE);
                    errorMsg.setVisibility(View.VISIBLE);
                    errorMsg.setText(R.string.no_data_error);
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        } else {
            pollsList.setVisibility(View.GONE);
            errorMsg.setVisibility(View.VISIBLE);
            errorMsg.setText(R.string.no_data_error);
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.fab:
                Intent intent = new Intent(getActivity(), CreateNoticeListPolls.class);
                startActivity(intent);
                break;
        }

    }
}
