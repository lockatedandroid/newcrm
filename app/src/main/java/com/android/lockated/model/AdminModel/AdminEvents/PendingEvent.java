
package com.android.lockated.model.AdminModel.AdminEvents;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class PendingEvent {

    @SerializedName("classifieds")
    @Expose
    private ArrayList<Classified> classifieds = new ArrayList<Classified>();

    /**
     * 
     * @return
     *     The classifieds
     */
    public ArrayList<Classified> getClassifieds() {
        return classifieds;
    }

    /**
     * 
     * @param classifieds
     *     The classifieds
     */
    public void setClassifieds(ArrayList<Classified> classifieds) {
        this.classifieds = classifieds;
    }

}
