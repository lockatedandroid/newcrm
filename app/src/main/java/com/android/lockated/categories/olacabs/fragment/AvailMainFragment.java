package com.android.lockated.categories.olacabs.fragment;


import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;

import com.android.lockated.R;
import com.android.lockated.categories.olacabs.activity.OlaMainActivity_Latest;
import com.android.lockated.categories.olacabs.adapter.OlaCabPagerAdapter;
import com.android.lockated.component.LockatedPagerSlidingTabStrip;
import com.android.lockated.utils.Utilities;

public class AvailMainFragment extends Fragment {

    private ViewPager mOlaViewPager;
    private LockatedPagerSlidingTabStrip mOlaViewPagerSlidingTabs;

    private OlaCabPagerAdapter mOlaCabPagerAdapter;
    //CharSequence Titles[] = {"Ola & Others", "Nearest"};
    CharSequence Titles[] = {"Ola cabs"};
    int Numboftabs = 1;
    private String screenName = "AvailMainFragment";

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        setHasOptionsMenu(true);

        if(OlaMainActivity_Latest.mGoogleApiClient !=null) {
            if (OlaMainActivity_Latest.mGoogleApiClient.isConnected()) {
                //Log.e("AvailMainFragment", "mGoogleApiClient is Connected");
            } else {
                //Log.e("AvailMainFragment", "mGoogleApiClient is Not Connected");
                OlaMainActivity_Latest.mGoogleApiClient.connect();
            }
        }

        //-----------------------------------------------------//
        OlaMainActivity_Latest.edt_olaSourceLocation.setFocusable(true);
        OlaMainActivity_Latest.edt_olaSourceLocation.setClickable(true);
        OlaMainActivity_Latest.edt_olaSourceLocation.setEnabled(true);
        //---------------------------------------------------------//

        OlaMainActivity_Latest.edt_olaSourceLocation.setClickable(true);
        //----------------Changed on 10-8-2016--------------------------------
       /*

       ----------------Changed on 20-9-2016--------------------------------
        if (OlaMainActivity_Latest.mOlaDestinationCard.getVisibility()== View.VISIBLE)
        {
            OlaMainActivity_Latest.mOlaDestinationCard.setVisibility(View.GONE);
        }

        */
        OlaMainActivity_Latest.mLayout.setEnabled(true);
        //-----------------------------------------------------------------

        //OlaMainActivity_Latest.gmap.getUiSettings().setScrollGesturesEnabled(true);
        //---    edt_olaSourceLocation  -setFocusable(true),setClickable(true)

        //----------------Changed on 10-8-2016--------------------

        /*if (OlaMainActivity_Latest.mOlaDestinationLayout.getVisibility()== View.VISIBLE)
        {
            OlaMainActivity_Latest.mOlaDestinationLayout.setVisibility(View.GONE);
        }*/
        //OlaMainActivity_Latest.mOlaSourceLayout.setFocusable(true);
        //OlaMainActivity_Latest.mOlaSourceLayout.setVisibility(View.VISIBLE);
        //OlaMainActivity_Latest.mOlaSourceLayout.setClickable(true);
        //OlaMainActivity_Latest.mLayout.setEnabled(true);

        //---------------------------------------------------------
        //OlaMainActivity_Latest.location_button_new.setVisibility(View.VISIBLE);//--------change on 19-06-2016
        OlaMainActivity_Latest.current_location_zoom.setVisibility(View.VISIBLE);//------change on 16-09-2016
        //OlaMainActivity_Latest.img_ola_cancel.setVisibility(View.VISIBLE);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_avail_main, container, false);
        init(v);
        Utilities.ladooIntegration(getActivity(), screenName);
        Utilities.lockatedGoogleAnalytics(screenName, getString(R.string.visited), screenName);
        return v;
    }

    @Override
    public void onResume() {
        super.onResume();


        OlaMainActivity_Latest.mLayout.setEnabled(true);

        /*---------------------Changes on  20-9-2016-----------------------

        if (OlaMainActivity_Latest.mOlaDestinationCard.getVisibility()== View.VISIBLE)
        {
            OlaMainActivity_Latest.mOlaDestinationCard.setVisibility(View.GONE);
        }

        */

        //---------------------Changes on  10-8-2016-----------------------
        /*if (OlaMainActivity_Latest.mOlaDestinationLayout.getVisibility()== View.VISIBLE)
        {
            OlaMainActivity_Latest.mOlaDestinationLayout.setVisibility(View.GONE);
        }*/

        //OlaMainActivity_Latest.mOlaSourceLayout.setAlpha(1);
        //OlaMainActivity_Latest.edt_olaSourceLocation.setFocusable(true);
        //OlaMainActivity_Latest.edt_olaSourceLocation.setVisibility(View.VISIBLE);
        //OlaMainActivity_Latest.edt_olaSourceLocation.setClickable(true);

        //----------------------------------------------------------

        OlaMainActivity_Latest.show_destination.setOnClickListener(null);
        //OlaMainActivity_Latest.location_button_new.setVisibility(View.VISIBLE);//--------change on 19-06-2016
        OlaMainActivity_Latest.current_location_zoom.setVisibility(View.VISIBLE);//------change on 16-09-2016
        OlaMainActivity_Latest.img_ola_cancel.setVisibility(View.VISIBLE);
    }

    public void init(View v)
    {
        mOlaViewPager = (ViewPager)v. findViewById(R.id.mOlaViewPager);
        mOlaViewPagerSlidingTabs = (LockatedPagerSlidingTabStrip)v. findViewById(R.id.mOlaViewPagerSlidingTabs);
        mOlaCabPagerAdapter = new OlaCabPagerAdapter(getChildFragmentManager(), Titles, Numboftabs);

        mOlaViewPagerSlidingTabs.setIndicatorColor(Color.BLACK);
        mOlaViewPagerSlidingTabs.setTextColor(Color.BLACK);

        mOlaViewPager.setAdapter(mOlaCabPagerAdapter);
        mOlaViewPagerSlidingTabs.setViewPager(mOlaViewPager);

        mOlaViewPager.setCurrentItem(0);
    }


   /* @Override
    public void onSaveInstanceState(Bundle outState) {
        *//*super.onSaveInstanceState(outState);*//*
    }*/
}
