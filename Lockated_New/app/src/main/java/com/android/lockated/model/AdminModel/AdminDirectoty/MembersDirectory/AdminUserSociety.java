
package com.android.lockated.model.AdminModel.AdminDirectoty.MembersDirectory;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class AdminUserSociety {

    @SerializedName("id")
    @Expose
    private int id;
    @SerializedName("id_society")
    @Expose
    private String idSociety;
    @SerializedName("approve")
    @Expose
    private boolean approve;
    @SerializedName("created_at")
    @Expose
    private Object createdAt;
    @SerializedName("updated_at")
    @Expose
    private String updatedAt;
    @SerializedName("user")
    @Expose
    private User user;
    @SerializedName("user_flat")
    @Expose
    private UserFlat userFlat;

    /**
     * 
     * @return
     *     The id
     */
    public int getId() {
        return id;
    }

    /**
     * 
     * @param id
     *     The id
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * 
     * @return
     *     The idSociety
     */
    public String getIdSociety() {
        return idSociety;
    }

    /**
     * 
     * @param idSociety
     *     The id_society
     */
    public void setIdSociety(String idSociety) {
        this.idSociety = idSociety;
    }

    /**
     * 
     * @return
     *     The approve
     */
    public boolean isApprove() {
        return approve;
    }

    /**
     * 
     * @param approve
     *     The approve
     */
    public void setApprove(boolean approve) {
        this.approve = approve;
    }

    /**
     * 
     * @return
     *     The createdAt
     */
    public Object getCreatedAt() {
        return createdAt;
    }

    /**
     * 
     * @param createdAt
     *     The created_at
     */
    public void setCreatedAt(Object createdAt) {
        this.createdAt = createdAt;
    }

    /**
     * 
     * @return
     *     The updatedAt
     */
    public String getUpdatedAt() {
        return updatedAt;
    }

    /**
     * 
     * @param updatedAt
     *     The updated_at
     */
    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    /**
     * 
     * @return
     *     The user
     */
    public User getUser() {
        return user;
    }

    /**
     * 
     * @param user
     *     The user
     */
    public void setUser(User user) {
        this.user = user;
    }

    /**
     * 
     * @return
     *     The userFlat
     */
    public UserFlat getUserFlat() {
        return userFlat;
    }

    /**
     * 
     * @param userFlat
     *     The user_flat
     */
    public void setUserFlat(UserFlat userFlat) {
        this.userFlat = userFlat;
    }

}
