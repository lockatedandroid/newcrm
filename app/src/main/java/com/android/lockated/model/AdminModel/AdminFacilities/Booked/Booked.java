
package com.android.lockated.model.AdminModel.AdminFacilities.Booked;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Booked {

    @SerializedName("id")
    @Expose
    private int id;
    @SerializedName("book_duration")
    @Expose
    private Object bookDuration;
    @SerializedName("book_purpose")
    @Expose
    private String bookPurpose;
    @SerializedName("person_no")
    @Expose
    private int personNo;
    @SerializedName("startdate")
    @Expose
    private String startdate;
    @SerializedName("enddate")
    @Expose
    private String enddate;
    @SerializedName("user_id")
    @Expose
    private int userId;
    @SerializedName("facility_id")
    @Expose
    private int facilityId;
    @SerializedName("society_id")
    @Expose
    private int societyId;
    @SerializedName("created_at")
    @Expose
    private String createdAt;
    @SerializedName("approve")
    @Expose
    private Object approve;
    @SerializedName("canceled_by")
    @Expose
    private Object canceledBy;
    @SerializedName("canceler_id")
    @Expose
    private Object cancelerId;
    @SerializedName("comment")
    @Expose
    private Object comment;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("flat")
    @Expose
    private String flat;
    @SerializedName("amenity")
    @Expose
    private Amenity amenity;
    @SerializedName("user")
    @Expose
    private User user;

    /**
     * 
     * @return
     *     The id
     */
    public int getId() {
        return id;
    }

    /**
     * 
     * @param id
     *     The id
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * 
     * @return
     *     The bookDuration
     */
    public Object getBookDuration() {
        return bookDuration;
    }

    /**
     * 
     * @param bookDuration
     *     The book_duration
     */
    public void setBookDuration(Object bookDuration) {
        this.bookDuration = bookDuration;
    }

    /**
     * 
     * @return
     *     The bookPurpose
     */
    public String getBookPurpose() {
        return bookPurpose;
    }

    /**
     * 
     * @param bookPurpose
     *     The book_purpose
     */
    public void setBookPurpose(String bookPurpose) {
        this.bookPurpose = bookPurpose;
    }

    /**
     * 
     * @return
     *     The personNo
     */
    public int getPersonNo() {
        return personNo;
    }

    /**
     * 
     * @param personNo
     *     The person_no
     */
    public void setPersonNo(int personNo) {
        this.personNo = personNo;
    }

    /**
     * 
     * @return
     *     The startdate
     */
    public String getStartdate() {
        return startdate;
    }

    /**
     * 
     * @param startdate
     *     The startdate
     */
    public void setStartdate(String startdate) {
        this.startdate = startdate;
    }

    /**
     * 
     * @return
     *     The enddate
     */
    public String getEnddate() {
        return enddate;
    }

    /**
     * 
     * @param enddate
     *     The enddate
     */
    public void setEnddate(String enddate) {
        this.enddate = enddate;
    }

    /**
     * 
     * @return
     *     The userId
     */
    public int getUserId() {
        return userId;
    }

    /**
     * 
     * @param userId
     *     The user_id
     */
    public void setUserId(int userId) {
        this.userId = userId;
    }

    /**
     * 
     * @return
     *     The facilityId
     */
    public int getFacilityId() {
        return facilityId;
    }

    /**
     * 
     * @param facilityId
     *     The facility_id
     */
    public void setFacilityId(int facilityId) {
        this.facilityId = facilityId;
    }

    /**
     * 
     * @return
     *     The societyId
     */
    public int getSocietyId() {
        return societyId;
    }

    /**
     * 
     * @param societyId
     *     The society_id
     */
    public void setSocietyId(int societyId) {
        this.societyId = societyId;
    }

    /**
     * 
     * @return
     *     The createdAt
     */
    public String getCreatedAt() {
        return createdAt;
    }

    /**
     * 
     * @param createdAt
     *     The created_at
     */
    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    /**
     * 
     * @return
     *     The approve
     */
    public Object getApprove() {
        return approve;
    }

    /**
     * 
     * @param approve
     *     The approve
     */
    public void setApprove(Object approve) {
        this.approve = approve;
    }

    /**
     * 
     * @return
     *     The canceledBy
     */
    public Object getCanceledBy() {
        return canceledBy;
    }

    /**
     * 
     * @param canceledBy
     *     The canceled_by
     */
    public void setCanceledBy(Object canceledBy) {
        this.canceledBy = canceledBy;
    }

    /**
     * 
     * @return
     *     The cancelerId
     */
    public Object getCancelerId() {
        return cancelerId;
    }

    /**
     * 
     * @param cancelerId
     *     The canceler_id
     */
    public void setCancelerId(Object cancelerId) {
        this.cancelerId = cancelerId;
    }

    /**
     * 
     * @return
     *     The comment
     */
    public Object getComment() {
        return comment;
    }

    /**
     * 
     * @param comment
     *     The comment
     */
    public void setComment(Object comment) {
        this.comment = comment;
    }

    /**
     * 
     * @return
     *     The status
     */
    public String getStatus() {
        return status;
    }

    /**
     * 
     * @param status
     *     The status
     */
    public void setStatus(String status) {
        this.status = status;
    }

    /**
     * 
     * @return
     *     The flat
     */
    public String getFlat() {
        return flat;
    }

    /**
     * 
     * @param flat
     *     The flat
     */
    public void setFlat(String flat) {
        this.flat = flat;
    }

    /**
     * 
     * @return
     *     The amenity
     */
    public Amenity getAmenity() {
        return amenity;
    }

    /**
     * 
     * @param amenity
     *     The amenity
     */
    public void setAmenity(Amenity amenity) {
        this.amenity = amenity;
    }

    /**
     * 
     * @return
     *     The user
     */
    public User getUser() {
        return user;
    }

    /**
     * 
     * @param user
     *     The user
     */
    public void setUser(User user) {
        this.user = user;
    }

}
