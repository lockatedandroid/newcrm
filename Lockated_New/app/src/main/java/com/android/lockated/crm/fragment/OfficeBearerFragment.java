package com.android.lockated.crm.fragment;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.android.lockated.R;


public class OfficeBearerFragment extends Fragment {

    public OfficeBearerFragment() {}

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View officeBearerView = inflater.inflate(R.layout.fragment_office_bearer, container, false);

        return officeBearerView;
    }

}
