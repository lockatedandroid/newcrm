
package com.android.lockated.model.usermodel.NoticeModel.AllNotices;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class AllNotices {

    @SerializedName("daily_feed")
    @Expose
    private Object dailyFeed;
    @SerializedName("noticeboards")
    @Expose
    private ArrayList<Noticeboard> noticeboards = new ArrayList<Noticeboard>();
    @SerializedName("shared")
    @Expose
    private ArrayList<Shared> shared = new ArrayList<Shared>();

    /**
     * 
     * @return
     *     The dailyFeed
     */
    public Object getDailyFeed() {
        return dailyFeed;
    }

    /**
     * 
     * @param dailyFeed
     *     The daily_feed
     */
    public void setDailyFeed(Object dailyFeed) {
        this.dailyFeed = dailyFeed;
    }

    /**
     * 
     * @return
     *     The noticeboards
     */
    public ArrayList<Noticeboard> getNoticeboards() {
        return noticeboards;
    }

    /**
     * 
     * @param noticeboards
     *     The noticeboards
     */
    public void setNoticeboards(ArrayList<Noticeboard> noticeboards) {
        this.noticeboards = noticeboards;
    }

    /**
     * 
     * @return
     *     The shared
     */
    public ArrayList<Shared> getShared() {
        return shared;
    }

    /**
     * 
     * @param shared
     *     The shared
     */
    public void setShared(ArrayList<Shared> shared) {
        this.shared = shared;
    }

}
