package com.android.lockated.categories.vendors;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;

import com.android.lockated.LockatedApplication;
import com.android.lockated.R;
import com.android.lockated.categories.vendors.fragment.VendorFragment;

public class VendorActivity extends AppCompatActivity {
    public Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_vendor);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(true);
        getSupportActionBar().setTitle(((LockatedApplication) getApplicationContext()).getHeaderName());

        ((LockatedApplication) getApplicationContext()).setMyAddressData(null);
        ((LockatedApplication) getApplicationContext()).setmDeliveryTime("");
        ((LockatedApplication) getApplicationContext()).setmDeliveryDate("");

        if (savedInstanceState == null) {
            setSuppliersFragment();
        }
    }

    private void setSuppliersFragment() {
        VendorFragment vendorFragment = new VendorFragment();
        getSupportFragmentManager().beginTransaction().add(R.id.mVendorContainer, vendorFragment).commit();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home: {
                super.onBackPressed();
                return true;
            }
        }
        return super.onOptionsItemSelected(item);
    }
}
