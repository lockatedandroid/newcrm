package com.android.lockated.crmadmin.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.lockated.LockatedApplication;
import com.android.lockated.R;
import com.android.lockated.information.AccountController;
import com.android.lockated.model.AccountData;
import com.android.lockated.network.ConnectionDetector;
import com.android.lockated.preferences.LockatedPreferences;
import com.android.lockated.request.LockatedJSONObjectRequest;
import com.android.lockated.request.LockatedRequestError;
import com.android.lockated.request.LockatedVolleyRequestQueue;
import com.android.lockated.utils.ApplicationURL;
import com.android.lockated.utils.Utilities;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;


public class AdminDirectoryMembersFragment extends Fragment implements Response.ErrorListener, Response.Listener<JSONObject> {

    FragmentManager fragmentManager;
    private RequestQueue mQueue;
    ListView listView;
    String pid;
    String userType;
    TextView errorMsg;
    JSONObject directoryJsonObj;
    ProgressBar progressBar;
    ArrayList<AccountData> data;
    AccountController accountController;
    private LockatedPreferences mLockatedPreferences;
    DirectoryMembersAdapter directoryManagementAdapter;
    public static final String REQUEST_TAG = "AdminDirectoryMembersFragment";

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View directoryManagementView = inflater.inflate(R.layout.fragment_directory_members, container, false);
        init(directoryManagementView);
        Utilities.ladooIntegration(getActivity(), getActivity().getResources().getString(R.string.adminDirectoryMembers));
        return directoryManagementView;
    }

    private void init(View directoryManagementView) {
        mLockatedPreferences = new LockatedPreferences(getActivity());
        accountController = AccountController.getInstance();
        data = accountController.getmAccountDataList();
        progressBar = (ProgressBar) directoryManagementView.findViewById(R.id.mProgressBarView);
        listView = (ListView) directoryManagementView.findViewById(R.id.listView);
        errorMsg = (TextView) directoryManagementView.findViewById(R.id.errorMsg);
        try {
            directoryJsonObj = new JSONObject(mLockatedPreferences.getRolesJson());
            userType = directoryJsonObj.getString("name");
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        LockatedApplication.getInstance().trackScreenView(getString(R.string.adminDirectoryMembers));
        Utilities.lockatedGoogleAnalytics(getString(R.string.adminDirectoryMembers),
                getString(R.string.visited), getString(R.string.adminDirectoryMembers));
        getDirectory();
    }

    @Override
    public void onErrorResponse(VolleyError error) {
        progressBar.setVisibility(View.GONE);
        if (getActivity() != null) {
            LockatedRequestError.onRequestError(getActivity(), error);
        }
    }

    private void getDirectory() {
        if (userType.equals("admin") || userType.equals("society_admin")) {
            if (ConnectionDetector.isConnectedToInternet(getActivity())) {
                if (ConnectionDetector.isConnectedToInternet(getActivity())) {
                    progressBar.setVisibility(View.VISIBLE);
                    String url = ApplicationURL.getApprovedUser + mLockatedPreferences.getSocietyId() + "&token="
                            + mLockatedPreferences.getLockatedToken();
                    mQueue = LockatedVolleyRequestQueue.getInstance(getActivity()).getRequestQueue();
                    LockatedJSONObjectRequest lockatedJSONObjectRequest = new LockatedJSONObjectRequest(Request.Method.GET,
                            url, null, this, this);
                    lockatedJSONObjectRequest.setTag(REQUEST_TAG);
                    mQueue.add(lockatedJSONObjectRequest);
                } else {
                    Utilities.showToastMessage(getActivity(), getActivity().getResources().getString(R.string.internet_connection_error));
                }
            } else {
                errorMsg.setVisibility(View.VISIBLE);
                errorMsg.setText(R.string.not_in_society);
            }
        } else {
            errorMsg.setVisibility(View.VISIBLE);
            errorMsg.setText(R.string.not_in_society);
        }

    }

    @Override
    public void onResponse(JSONObject response) {
        progressBar.setVisibility(View.INVISIBLE);
        if (getActivity() != null) {
            if (response != null && response.length() > 0) {
                try {
                    if (response.getJSONArray("user_societies").length() > 0) {
                        directoryManagementAdapter = new DirectoryMembersAdapter(getActivity(), response.getJSONArray("user_societies"));
                        listView.setAdapter(directoryManagementAdapter);
                    } else {
                        listView.setVisibility(View.GONE);
                        errorMsg.setVisibility(View.VISIBLE);
                        errorMsg.setText(R.string.no_data_error);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                listView.setVisibility(View.GONE);
                errorMsg.setVisibility(View.VISIBLE);
                errorMsg.setText(R.string.no_data_error);
            }
        }
    }

    private class DirectoryMembersAdapter extends BaseAdapter implements Response.ErrorListener,
            Response.Listener<JSONObject> {

        Context contextMain;
        JSONArray jsonArray;
        LayoutInflater layoutInflater;

        public DirectoryMembersAdapter(Context context, JSONArray jsonArray) {
            this.contextMain = context;
            this.jsonArray = jsonArray;
            layoutInflater = LayoutInflater.from(context);
        }

        @Override
        public int getCount() {
            return jsonArray.length();

        }

        @Override
        public Object getItem(int position) {
            return position;
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public void onErrorResponse(VolleyError error) {
            progressBar.setVisibility(View.GONE);
            if (getActivity() != null) {
                LockatedRequestError.onRequestError(getActivity(), error);
            }
        }

        @Override
        public void onResponse(JSONObject response) {
            progressBar.setVisibility(View.GONE);
        }

        private class ViewHolder {
            ImageView mImageDelete;
            ImageView mImageEdit;
            TextView mTextNumber;
            TextView mTextDirectoryName;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
            ViewHolder holder = null;
            if (convertView == null) {
                convertView = layoutInflater.inflate(R.layout.directory_member_child, parent, false);
                holder = new ViewHolder();
                convertView.setTag(holder);
            } else {
                holder = (ViewHolder) convertView.getTag();
            }
            holder.mImageEdit = (ImageView) convertView.findViewById(R.id.mImageEdit);
            holder.mImageDelete = (ImageView) convertView.findViewById(R.id.mImageDelete);
            holder.mTextDirectoryName = (TextView) convertView.findViewById(R.id.mTextDirectoryName);
            holder.mTextNumber = (TextView) convertView.findViewById(R.id.mTextNumber);
            try {
                if (!jsonArray.getJSONObject(position).isNull("user") && jsonArray.getJSONObject(position).getJSONObject("user") != null &&
                        !jsonArray.getJSONObject(position).getJSONObject("user").toString().equals("null")) {
                    String name = jsonArray.getJSONObject(position).getJSONObject("user").getString("firstname")
                            + " " + jsonArray.getJSONObject(position).getJSONObject("user").getString("lastname");
                    holder.mTextDirectoryName.setText(name);
                    if (!jsonArray.getJSONObject(position).isNull("user_flat")
                            && jsonArray.getJSONObject(position).getJSONObject("user_flat").length() > 0) {
                        holder.mTextNumber.setText(jsonArray.getJSONObject(position).getJSONObject("user_flat").getString("flat"));
                    } else {
                        holder.mTextNumber.setText("No Flat");
                    }
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return convertView;
        }

    }

}
