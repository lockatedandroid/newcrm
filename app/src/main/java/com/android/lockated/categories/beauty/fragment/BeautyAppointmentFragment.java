package com.android.lockated.categories.beauty.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import com.android.lockated.Interfaces.IRecyclerItemClickListener;
import com.android.lockated.LockatedApplication;
import com.android.lockated.R;
import com.android.lockated.categories.adapter.CategoryItemAdapter;
import com.android.lockated.categories.grocery.GroceryActivity;
import com.android.lockated.landing.model.TaxonData;
import com.android.lockated.network.ConnectionDetector;
import com.android.lockated.preferences.LockatedPreferences;
import com.android.lockated.request.LockatedRequestError;
import com.android.lockated.request.LockatedVolleyRequestQueue;
import com.android.lockated.utils.ApplicationURL;
import com.android.lockated.utils.Utilities;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

public class BeautyAppointmentFragment extends Fragment implements IRecyclerItemClickListener, Response.Listener, Response.ErrorListener {
    private static final String REQUEST_TAG = "BeautyAppointmentFragment";
    private ArrayList<TaxonData> mTaxonDatas;

    private ProgressBar mProgressBarView;

    private int mTaxonId;
    private int mTaxonomyId;

    private LockatedPreferences mLockatedPreferences;
    private CategoryItemAdapter mCategoryItemAdapter;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View beautyAppointmentView = inflater.inflate(R.layout.fragment_beauty_service, container, false);
        init(beautyAppointmentView, savedInstanceState);
        getCategoryData();
        Utilities.ladooIntegration(getActivity(), "Beauty Appointment");
        return beautyAppointmentView;
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

    }

    private void init(View reminderView, Bundle savedInstanceState) {
        if (savedInstanceState == null) {
            mTaxonId = ((LockatedApplication) getActivity().getApplicationContext()).getTaxonId();
            mTaxonomyId = ((LockatedApplication) getActivity().getApplicationContext()).getTaxonomyId();
        } else {
            mTaxonId = savedInstanceState.getInt("TaxonId");
            mTaxonomyId = savedInstanceState.getInt("TaxonomyId");
        }

        mTaxonDatas = new ArrayList<>();
        mLockatedPreferences = new LockatedPreferences(getActivity());

        mProgressBarView = (ProgressBar) reminderView.findViewById(R.id.mProgressBarView);
        RecyclerView mRecyclerView = (RecyclerView) reminderView.findViewById(R.id.mRecyclerView);

        LinearLayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        mRecyclerView.setLayoutManager(mLayoutManager);
        mRecyclerView.setHasFixedSize(true);

        mCategoryItemAdapter = new CategoryItemAdapter(mTaxonDatas, this, REQUEST_TAG);
        mRecyclerView.setAdapter(mCategoryItemAdapter);
    }

    private void getCategoryData() {
        if (ConnectionDetector.isConnectedToInternet(getActivity())) {
            mProgressBarView.setVisibility(View.VISIBLE);
            String url = ApplicationURL.getCategoryUrl + "/" + mTaxonomyId + "/taxons/" + mTaxonId
                    + ".json?token=" + mLockatedPreferences.getLockatedToken();
            LockatedVolleyRequestQueue lockatedVolleyRequestQueue = LockatedVolleyRequestQueue.getInstance(getActivity());
            lockatedVolleyRequestQueue.sendRequest(REQUEST_TAG, Request.Method.GET, url, null, this, this);
        } else {
            Utilities.showToastMessage(getActivity(), getActivity().getResources().getString(R.string.internet_connection_error));
        }
    }

    @Override
    public void onRecyclerItemClick(View view, int position) {
        Intent groceryIntent = new Intent(getActivity(), GroceryActivity.class);
        ((LockatedApplication) getActivity().getApplicationContext()).setmSectionId(position);
        ((LockatedApplication) getActivity().getApplicationContext()).setTaxonDatas(mTaxonDatas);
        groceryIntent.putExtra("GroceryActivity", "Beauty");
        startActivity(groceryIntent);
    }

    @Override
    public void onErrorResponse(VolleyError error) {
        if (getActivity() != null) {
            mProgressBarView.setVisibility(View.GONE);
            LockatedRequestError.onRequestError(getActivity(), error);
        }
    }

    @Override
    public void onResponse(Object response) {
        mProgressBarView.setVisibility(View.GONE);

        if (getActivity() != null) {
            JSONObject jsonObject = (JSONObject) response;
            try {
                JSONArray jsonArray = jsonObject.getJSONArray("taxons");
                for (int i = 0; i < jsonArray.length(); i++) {
                    JSONObject taxonObject = jsonArray.getJSONObject(i);
                    TaxonData taxonData = new TaxonData(taxonObject);
                    mTaxonDatas.add(taxonData);
                    if (i == jsonArray.length() - 1) {
                        String strSampleJson = "{\n" +
                                "      \"id\": 0,\n" +
                                "      \"name\": \"Bleach\",\n" +
                                "      \"pretty_name\": \"Beauty -> Home Service for Women -> Bleach\",\n" +
                                "      \"permalink\": \"beauty/chat-with-experts/bleach\",\n" +
                                "      \"parent_id\": 0,\n" +
                                "      \"taxonomy_id\": 0\n" +
                                "    }";
                        JSONObject jObject = new JSONObject(strSampleJson);
                        TaxonData taxonDataObject = new TaxonData(jObject);
                        mTaxonDatas.add(taxonDataObject);
                    }
                }
                mCategoryItemAdapter.notifyDataSetChanged();
            } catch (Exception ex) {
            }
        }
    }
}