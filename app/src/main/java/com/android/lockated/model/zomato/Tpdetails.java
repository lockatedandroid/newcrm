
package com.android.lockated.model.zomato;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class Tpdetails {

    @SerializedName("OrderStatus")
    @Expose
    private int orderStatus;

    @SerializedName("BillNumber")
    @Expose
    private String billNumber;
    @SerializedName("CreatedTimestampStr")
    @Expose
    private String createdTimestampStr;
    @SerializedName("RestaurantAvgPickupTime")
    @Expose
    private int restaurantAvgPickupTime;
    @SerializedName("RestaurantMinOrder")
    @Expose
    private int restaurantMinOrder;
    @SerializedName("RestaurantPhone")
    @Expose
    private String restaurantPhone;
    @SerializedName("getPaymentStatus")
    @Expose
    private int getPaymentStatus;
    @SerializedName("Order")
    @Expose
    private String order;
    @SerializedName("DeliveryMessage")
    @Expose
    private String deliveryMessage;
    @SerializedName("RestaurantAddress")
    @Expose
    private String restaurantAddress;
    @SerializedName("TotalCost")
    @Expose
    private float totalCost;
    @SerializedName("UserPhone")
    @Expose
    private String userPhone;
    @SerializedName("RestaurantLatitude")
    @Expose
    private float restaurantLatitude;
    @SerializedName("RestaurantDistance")
    @Expose
    private String restaurantDistance;
    @SerializedName("UserPhoneCountryId")
    @Expose
    private int userPhoneCountryId;
    @SerializedName("TipMax")
    @Expose
    private int tipMax;
    @SerializedName("Currency")
    @Expose
    private String currency;
    @SerializedName("Order Id")
    @Expose
    private int orderId;
    @SerializedName("RestaurantLongitude")
    @Expose
    private float restaurantLongitude;
    @SerializedName("OrderTotalAmount")
    @Expose
    private float orderTotalAmount;
    @SerializedName("YourDeliveryRating")
    @Expose
    private int yourDeliveryRating;
    @SerializedName("OrderDetails")
    @Expose
    private ArrayList<OrderDetail> orderDetails = new ArrayList<OrderDetail>();
    @SerializedName("TipEnabled")
    @Expose
    private int tipEnabled;
    @SerializedName("RestaurantCostForTwo")
    @Expose
    private String restaurantCostForTwo;
    @SerializedName("PaymentMethod")
    @Expose
    private String paymentMethod;
    @SerializedName("DeliveryLabel")
    @Expose
    private String deliveryLabel;
    @SerializedName("DeliveryAddressString")
    @Expose
    private String deliveryAddressString;
    @SerializedName("RestaurantCostForOne")
    @Expose
    private int restaurantCostForOne;
    @SerializedName("DominosOrderID")
    @Expose
    private String dominosOrderID;
    @SerializedName("DishString")
    @Expose
    private String dishString;
    @SerializedName("RestaurantName")
    @Expose
    private String restaurantName;
    @SerializedName("RestaurantAcceptanceRate")
    @Expose
    private String restaurantAcceptanceRate;
    @SerializedName("RestaurantFeaturedImage")
    @Expose
    private String restaurantFeaturedImage;
    @SerializedName("TipPercentage")
    @Expose
    private int tipPercentage;
    @SerializedName("PaymentText")
    @Expose
    private String paymentText;
    @SerializedName("DeliveryStatus")
    @Expose
    private int deliveryStatus;
    @SerializedName("Id")
    @Expose
    private String id;
    @SerializedName("RestaurantLocality")
    @Expose
    private String restaurantLocality;
    @SerializedName("RestaurantCostForTwoMultiplier")
    @Expose
    private int restaurantCostForTwoMultiplier;
    @SerializedName("DeliveryMode")
    @Expose
    private String deliveryMode;
    @SerializedName("LinkCode")
    @Expose
    private int linkCode;
    @SerializedName("thirdparty_transaction")
    @Expose
    private ThirdpartyTransaction_ thirdpartyTransaction;

    /**
     * 
     * @return
     *     The billNumber
     */
    public String getBillNumber() {
        return billNumber;
    }

    /**
     * 
     * @param billNumber
     *     The BillNumber
     */
    public void setBillNumber(String billNumber) {
        this.billNumber = billNumber;
    }

    /**
     * 
     * @return
     *     The createdTimestampStr
     */
    public String getCreatedTimestampStr() {
        return createdTimestampStr;
    }

    /**
     * 
     * @param createdTimestampStr
     *     The CreatedTimestampStr
     */
    public void setCreatedTimestampStr(String createdTimestampStr) {
        this.createdTimestampStr = createdTimestampStr;
    }

    /**
     * 
     * @return
     *     The restaurantAvgPickupTime
     */
    public int getRestaurantAvgPickupTime() {
        return restaurantAvgPickupTime;
    }

    /**
     * 
     * @param restaurantAvgPickupTime
     *     The RestaurantAvgPickupTime
     */
    public void setRestaurantAvgPickupTime(int restaurantAvgPickupTime) {
        this.restaurantAvgPickupTime = restaurantAvgPickupTime;
    }

    /**
     * 
     * @return
     *     The restaurantMinOrder
     */
    public int getRestaurantMinOrder() {
        return restaurantMinOrder;
    }

    /**
     * 
     * @param restaurantMinOrder
     *     The RestaurantMinOrder
     */
    public void setRestaurantMinOrder(int restaurantMinOrder) {
        this.restaurantMinOrder = restaurantMinOrder;
    }

    /**
     * 
     * @return
     *     The restaurantPhone
     */
    public String getRestaurantPhone() {
        return restaurantPhone;
    }

    /**
     * 
     * @param restaurantPhone
     *     The RestaurantPhone
     */
    public void setRestaurantPhone(String restaurantPhone) {
        this.restaurantPhone = restaurantPhone;
    }

    /**
     * 
     * @return
     *     The getPaymentStatus
     */
    public int getGetPaymentStatus() {
        return getPaymentStatus;
    }

    /**
     * 
     * @param getPaymentStatus
     *     The getPaymentStatus
     */
    public void setGetPaymentStatus(int getPaymentStatus) {
        this.getPaymentStatus = getPaymentStatus;
    }

    /**
     * 
     * @return
     *     The order
     */
    public String getOrder() {
        return order;
    }

    /**
     * 
     * @param order
     *     The Order
     */
    public void setOrder(String order) {
        this.order = order;
    }

    /**
     * 
     * @return
     *     The deliveryMessage
     */
    public String getDeliveryMessage() {
        return deliveryMessage;
    }

    /**
     * 
     * @param deliveryMessage
     *     The DeliveryMessage
     */
    public void setDeliveryMessage(String deliveryMessage) {
        this.deliveryMessage = deliveryMessage;
    }

    /**
     * 
     * @return
     *     The restaurantAddress
     */
    public String getRestaurantAddress() {
        return restaurantAddress;
    }

    /**
     * 
     * @param restaurantAddress
     *     The RestaurantAddress
     */
    public void setRestaurantAddress(String restaurantAddress) {
        this.restaurantAddress = restaurantAddress;
    }

    /**
     * 
     * @return
     *     The totalCost
     */
    public float getTotalCost() {
        return totalCost;
    }

    /**
     * 
     * @param totalCost
     *     The TotalCost
     */
    public void setTotalCost(float totalCost) {
        this.totalCost = totalCost;
    }

    /**
     * 
     * @return
     *     The userPhone
     */
    public String getUserPhone() {
        return userPhone;
    }

    /**
     * 
     * @param userPhone
     *     The UserPhone
     */
    public void setUserPhone(String userPhone) {
        this.userPhone = userPhone;
    }

    /**
     * 
     * @return
     *     The restaurantLatitude
     */
    public float getRestaurantLatitude() {
        return restaurantLatitude;
    }

    /**
     * 
     * @param restaurantLatitude
     *     The RestaurantLatitude
     */
    public void setRestaurantLatitude(float restaurantLatitude) {
        this.restaurantLatitude = restaurantLatitude;
    }

    /**
     * 
     * @return
     *     The restaurantDistance
     */
    public String getRestaurantDistance() {
        return restaurantDistance;
    }

    /**
     * 
     * @param restaurantDistance
     *     The RestaurantDistance
     */
    public void setRestaurantDistance(String restaurantDistance) {
        this.restaurantDistance = restaurantDistance;
    }

    /**
     * 
     * @return
     *     The userPhoneCountryId
     */
    public int getUserPhoneCountryId() {
        return userPhoneCountryId;
    }

    /**
     * 
     * @param userPhoneCountryId
     *     The UserPhoneCountryId
     */
    public void setUserPhoneCountryId(int userPhoneCountryId) {
        this.userPhoneCountryId = userPhoneCountryId;
    }

    /**
     * 
     * @return
     *     The tipMax
     */
    public int getTipMax() {
        return tipMax;
    }

    /**
     * 
     * @param tipMax
     *     The TipMax
     */
    public void setTipMax(int tipMax) {
        this.tipMax = tipMax;
    }

    /**
     * 
     * @return
     *     The currency
     */
    public String getCurrency() {
        return currency;
    }

    /**
     * 
     * @param currency
     *     The Currency
     */
    public void setCurrency(String currency) {
        this.currency = currency;
    }

    /**
     * 
     * @return
     *     The orderId
     */
    public int getOrderId() {
        return orderId;
    }

    /**
     * 
     * @param orderId
     *     The Order Id
     */
    public void setOrderId(int orderId) {
        this.orderId = orderId;
    }

    /**
     * 
     * @return
     *     The restaurantLongitude
     */
    public float getRestaurantLongitude() {
        return restaurantLongitude;
    }

    /**
     * 
     * @param restaurantLongitude
     *     The RestaurantLongitude
     */
    public void setRestaurantLongitude(float restaurantLongitude) {
        this.restaurantLongitude = restaurantLongitude;
    }

    /**
     * 
     * @return
     *     The orderTotalAmount
     */
    public float getOrderTotalAmount() {
        return orderTotalAmount;
    }

    /**
     * 
     * @param orderTotalAmount
     *     The OrderTotalAmount
     */
    public void setOrderTotalAmount(float orderTotalAmount) {
        this.orderTotalAmount = orderTotalAmount;
    }

    /**
     * 
     * @return
     *     The yourDeliveryRating
     */
    public int getYourDeliveryRating() {
        return yourDeliveryRating;
    }

    /**
     * 
     * @param yourDeliveryRating
     *     The YourDeliveryRating
     */
    public void setYourDeliveryRating(int yourDeliveryRating) {
        this.yourDeliveryRating = yourDeliveryRating;
    }

    /**
     * 
     * @return
     *     The orderDetails
     */
    public ArrayList<OrderDetail> getOrderDetails() {
        return orderDetails;
    }

    /**
     * 
     * @param orderDetails
     *     The OrderDetails
     */
    public void setOrderDetails(ArrayList<OrderDetail> orderDetails) {
        this.orderDetails = orderDetails;
    }

    /**
     * 
     * @return
     *     The tipEnabled
     */
    public int getTipEnabled() {
        return tipEnabled;
    }

    /**
     * 
     * @param tipEnabled
     *     The TipEnabled
     */
    public void setTipEnabled(int tipEnabled) {
        this.tipEnabled = tipEnabled;
    }

    /**
     * 
     * @return
     *     The restaurantCostForTwo
     */
    public String getRestaurantCostForTwo() {
        return restaurantCostForTwo;
    }

    /**
     * 
     * @param restaurantCostForTwo
     *     The RestaurantCostForTwo
     */
    public void setRestaurantCostForTwo(String restaurantCostForTwo) {
        this.restaurantCostForTwo = restaurantCostForTwo;
    }

    /**
     * 
     * @return
     *     The paymentMethod
     */
    public String getPaymentMethod() {
        return paymentMethod;
    }

    /**
     * 
     * @param paymentMethod
     *     The PaymentMethod
     */
    public void setPaymentMethod(String paymentMethod) {
        this.paymentMethod = paymentMethod;
    }

    /**
     * 
     * @return
     *     The deliveryLabel
     */
    public String getDeliveryLabel() {
        return deliveryLabel;
    }

    /**
     * 
     * @param deliveryLabel
     *     The DeliveryLabel
     */
    public void setDeliveryLabel(String deliveryLabel) {
        this.deliveryLabel = deliveryLabel;
    }

    /**
     * 
     * @return
     *     The deliveryAddressString
     */
    public String getDeliveryAddressString() {
        return deliveryAddressString;
    }

    /**
     * 
     * @param deliveryAddressString
     *     The DeliveryAddressString
     */
    public void setDeliveryAddressString(String deliveryAddressString) {
        this.deliveryAddressString = deliveryAddressString;
    }

    /**
     * 
     * @return
     *     The restaurantCostForOne
     */
    public int getRestaurantCostForOne() {
        return restaurantCostForOne;
    }

    /**
     * 
     * @param restaurantCostForOne
     *     The RestaurantCostForOne
     */
    public void setRestaurantCostForOne(int restaurantCostForOne) {
        this.restaurantCostForOne = restaurantCostForOne;
    }

    /**
     * 
     * @return
     *     The dominosOrderID
     */
    public String getDominosOrderID() {
        return dominosOrderID;
    }

    /**
     * 
     * @param dominosOrderID
     *     The DominosOrderID
     */
    public void setDominosOrderID(String dominosOrderID) {
        this.dominosOrderID = dominosOrderID;
    }

    /**
     * 
     * @return
     *     The dishString
     */
    public String getDishString() {
        return dishString;
    }

    /**
     * 
     * @param dishString
     *     The DishString
     */
    public void setDishString(String dishString) {
        this.dishString = dishString;
    }

    /**
     * 
     * @return
     *     The restaurantName
     */
    public String getRestaurantName() {
        return restaurantName;
    }

    /**
     * 
     * @param restaurantName
     *     The RestaurantName
     */
    public void setRestaurantName(String restaurantName) {
        this.restaurantName = restaurantName;
    }

    /**
     * 
     * @return
     *     The restaurantAcceptanceRate
     */
    public String getRestaurantAcceptanceRate() {
        return restaurantAcceptanceRate;
    }

    /**
     * 
     * @param restaurantAcceptanceRate
     *     The RestaurantAcceptanceRate
     */
    public void setRestaurantAcceptanceRate(String restaurantAcceptanceRate) {
        this.restaurantAcceptanceRate = restaurantAcceptanceRate;
    }

    /**
     * 
     * @return
     *     The restaurantFeaturedImage
     */
    public String getRestaurantFeaturedImage() {
        return restaurantFeaturedImage;
    }

    /**
     * 
     * @param restaurantFeaturedImage
     *     The RestaurantFeaturedImage
     */
    public void setRestaurantFeaturedImage(String restaurantFeaturedImage) {
        this.restaurantFeaturedImage = restaurantFeaturedImage;
    }

    /**
     * 
     * @return
     *     The tipPercentage
     */
    public int getTipPercentage() {
        return tipPercentage;
    }

    /**
     * 
     * @param tipPercentage
     *     The TipPercentage
     */
    public void setTipPercentage(int tipPercentage) {
        this.tipPercentage = tipPercentage;
    }

    /**
     * 
     * @return
     *     The paymentText
     */
    public String getPaymentText() {
        return paymentText;
    }

    /**
     * 
     * @param paymentText
     *     The PaymentText
     */
    public void setPaymentText(String paymentText) {
        this.paymentText = paymentText;
    }

    /**
     * 
     * @return
     *     The deliveryStatus
     */
    public int getDeliveryStatus() {
        return deliveryStatus;
    }

    /**
     * 
     * @param deliveryStatus
     *     The DeliveryStatus
     */
    public void setDeliveryStatus(int deliveryStatus) {
        this.deliveryStatus = deliveryStatus;
    }

    /**
     * 
     * @return
     *     The id
     */
    public String getId() {
        return id;
    }

    /**
     * 
     * @param id
     *     The Id
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * 
     * @return
     *     The restaurantLocality
     */
    public String getRestaurantLocality() {
        return restaurantLocality;
    }

    /**
     * 
     * @param restaurantLocality
     *     The RestaurantLocality
     */
    public void setRestaurantLocality(String restaurantLocality) {
        this.restaurantLocality = restaurantLocality;
    }

    /**
     * 
     * @return
     *     The restaurantCostForTwoMultiplier
     */
    public int getRestaurantCostForTwoMultiplier() {
        return restaurantCostForTwoMultiplier;
    }

    /**
     * 
     * @param restaurantCostForTwoMultiplier
     *     The RestaurantCostForTwoMultiplier
     */
    public void setRestaurantCostForTwoMultiplier(int restaurantCostForTwoMultiplier) {
        this.restaurantCostForTwoMultiplier = restaurantCostForTwoMultiplier;
    }

    /**
     * 
     * @return
     *     The deliveryMode
     */
    public String getDeliveryMode() {
        return deliveryMode;
    }

    /**
     * 
     * @param deliveryMode
     *     The DeliveryMode
     */
    public void setDeliveryMode(String deliveryMode) {
        this.deliveryMode = deliveryMode;
    }

    /**
     * 
     * @return
     *     The linkCode
     */
    public int getLinkCode() {
        return linkCode;
    }

    /**
     * 
     * @param linkCode
     *     The LinkCode
     */
    public void setLinkCode(int linkCode) {
        this.linkCode = linkCode;
    }

    /**
     * 
     * @return
     *     The thirdpartyTransaction
     */
    public ThirdpartyTransaction_ getThirdpartyTransaction() {
        return thirdpartyTransaction;
    }

    /**
     * 
     * @param thirdpartyTransaction
     *     The thirdparty_transaction
     */
    public void setThirdpartyTransaction(ThirdpartyTransaction_ thirdpartyTransaction) {
        this.thirdpartyTransaction = thirdpartyTransaction;
    }


    /**
     *
     * @return
     *     The orderStatus
     */
    public int getOrderStatus() {
        return orderStatus;
    }

    /**
     *
     * @param orderStatus
     *     The OrderStatus
     */
    public void setOrderStatus(int orderStatus) {
        this.orderStatus = orderStatus;
    }



}
