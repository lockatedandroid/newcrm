package com.android.lockated.categories.services.model;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

public class ServiceData
{
    private int id;
    private String service_name;
    private int category_id;
    private int sub_category_id;

    private ArrayList<ServiceAttributeData> mServiceAttributeDatas = new ArrayList<>();
    private ArrayList<String> mSubServiceNameList = new ArrayList<>();

    public ServiceData()
    {

    }

    public ServiceData(JSONObject jsonObject)
    {
        try
        {
            id = jsonObject.optInt("id");
            service_name = jsonObject.optString("service_name");
            category_id = jsonObject.optInt("category_id");
            sub_category_id = jsonObject.optInt("sub_category_id");

            JSONArray attributeJsonArray = jsonObject.getJSONArray("attribute_mappings");
            for (int i = 0; i < attributeJsonArray.length(); i++)
            {
                JSONObject attributeObject = attributeJsonArray.getJSONObject(i);
                ServiceAttributeData serviceAttributeData = new ServiceAttributeData(attributeObject);
                mServiceAttributeDatas.add(serviceAttributeData);
                mSubServiceNameList.add(attributeObject.optString("name"));
            }
        }
        catch (Exception ex)
        {
            ex.getMessage();
        }
    }

    public int getId()
    {
        return id;
    }

    public String getService_name()
    {
        return service_name;
    }

    public int getCategory_id()
    {
        return category_id;
    }

    public int getSub_category_id()
    {
        return sub_category_id;
    }

    public ArrayList<ServiceAttributeData> getServiceAttributeDatas()
    {
        return mServiceAttributeDatas;
    }

    public ArrayList<String> getSubServiceNameList()
    {
        return mSubServiceNameList;
    }
}
