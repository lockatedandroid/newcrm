
package com.android.lockated.model.userSocietyFlat;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class SocietyFlat implements Parcelable {

    @SerializedName("id")
    @Expose
    private int id;
    @SerializedName("flat_no")
    @Expose
    private String flatNo;
    @SerializedName("society_id")
    @Expose
    private int societyId;
    @SerializedName("block_no")
    @Expose
    private String blockNo;
    @SerializedName("is_enable")
    @Expose
    private Object isEnable;
    @SerializedName("approve")
    @Expose
    private Object approve;
    @SerializedName("approve_by")
    @Expose
    private Object approveBy;


    /**
     * @return The id
     */
    public int getId() {
        return id;
    }

    /**
     * @param id The id
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * @return The flatNo
     */
    public String getFlatNo() {
        return flatNo;
    }

    /**
     * @param flatNo The flat_no
     */
    public void setFlatNo(String flatNo) {
        this.flatNo = flatNo;
    }

    /**
     * @return The societyId
     */
    public int getSocietyId() {
        return societyId;
    }

    /**
     * @param societyId The society_id
     */
    public void setSocietyId(int societyId) {
        this.societyId = societyId;
    }

    /**
     * @return The blockNo
     */
    public String getBlockNo() {
        return blockNo;
    }

    /**
     * @param blockNo The block_no
     */
    public void setBlockNo(String blockNo) {
        this.blockNo = blockNo;
    }

    /**
     * @return The isEnable
     */
    public Object getIsEnable() {
        return isEnable;
    }

    /**
     * @param isEnable The is_enable
     */
    public void setIsEnable(Object isEnable) {
        this.isEnable = isEnable;
    }

    /**
     * @return The approve
     */
    public Object getApprove() {
        return approve;
    }

    /**
     * @param approve The approve
     */
    public void setApprove(Object approve) {
        this.approve = approve;
    }

    /**
     * @return The approveBy
     */
    public Object getApproveBy() {
        return approveBy;
    }

    /**
     * @param approveBy The approve_by
     */
    public void setApproveBy(Object approveBy) {
        this.approveBy = approveBy;
    }

    @Override
    public int describeContents() {
        return this.hashCode();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(id);
        dest.writeString(flatNo);
        dest.writeInt(societyId);
        dest.writeString(blockNo);
        dest.writeValue(isEnable);
        dest.writeValue(approve);
        dest.writeValue(approveBy);
    }

    private SocietyFlat(Parcel in) {
        id = in.readInt();
        flatNo = in.readString();
        societyId = in.readInt();
        blockNo = in.readString();
        isEnable = in.readValue(Object.class.getClassLoader());
        approve = in.readValue(Object.class.getClassLoader());
        approveBy = in.readValue(Object.class.getClassLoader());
    }

    public static final Creator<SocietyFlat> CREATOR = new Creator<SocietyFlat>() {
        @Override
        public SocietyFlat createFromParcel(Parcel in) {
            return new SocietyFlat(in);
        }

        @Override
        public SocietyFlat[] newArray(int size) {
            return new SocietyFlat[size];
        }
    };
}
