package com.android.lockated.crm.fragment.notices;

import android.app.Dialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.text.method.ScrollingMovementMethod;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.android.lockated.R;

public class NoticePreview extends DialogFragment implements View.OnClickListener{

    private View NoticePreviewView;
    private TextView mSubject;
    private TextView mDescription;
    private TextView mEndtDate;
    private TextView mEdit;
    private TextView mProceed;
    Window window;
    ProgressBar progressBar;


    @Override
    public void onStart() {
        super.onStart();

        window = getDialog().getWindow();
        WindowManager.LayoutParams windowParams = window.getAttributes();
        windowParams.dimAmount = 0.75f;
        windowParams.flags |= WindowManager.LayoutParams.FLAG_DIM_BEHIND;
        window.setAttributes(windowParams);
        window.setBackgroundDrawableResource(android.R.color.white);

    }


    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        NoticePreviewView = getActivity().getLayoutInflater().inflate(R.layout.fragment_notice_preview,
                new LinearLayout(getActivity()), false);

        init(NoticePreviewView);
        Dialog builder = new Dialog(getActivity());
        builder.requestWindowFeature(Window.FEATURE_NO_TITLE);
        builder.getWindow().setBackgroundDrawable(new ColorDrawable(Color.WHITE));

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(builder.getWindow().getAttributes());
        lp.width = 700;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        builder.show();
        builder.getWindow().setAttributes(lp);
        builder.setContentView(NoticePreviewView);

        return builder;

    }


    private void init(View NoticePreviewView) {

        mSubject = (TextView) NoticePreviewView.findViewById(R.id.mSubject);
        mDescription = (TextView) NoticePreviewView.findViewById(R.id.mDescription);
        mDescription.setMovementMethod(new ScrollingMovementMethod());
        mEndtDate = (TextView) NoticePreviewView.findViewById(R.id.mEndDate);
        mEdit = (TextView) NoticePreviewView.findViewById(R.id.mEdit);
        mSubject.setText(getArguments().getString("subject"));
        mDescription.setText(getArguments().getString("description"));
        mEndtDate.setText(getArguments().getString("enddate"));
        mEdit.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            /*case R.id.mProceed:
                proceedDetails();
                break;*/
            case R.id.mEdit:
                proceedDetails();
                break;
        }
    }

    private void proceedDetails() {
        dismiss();
    }
}
