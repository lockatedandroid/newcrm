package com.android.lockated.crm.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.util.SparseBooleanArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.lockated.R;
import com.android.lockated.model.usermodel.myGroups.UserSociety;

import java.util.ArrayList;
import java.util.Collections;

public class ListViewAdapter extends ArrayAdapter<UserSociety> implements View.OnClickListener {

    Context context;
    String updateGroup;
    LayoutInflater inflater;
    ArrayList<Integer> userId;
    ArrayList<UserSociety> userSocietyArrayList;
    private SparseBooleanArray mSelectedItemsIds;

    public ListViewAdapter(Context context, int resource, ArrayList<UserSociety> userSocietyArrayList,
                           ArrayList<Integer> userId, String updateGroup) {
        super(context, resource, userSocietyArrayList);
        this.context = context;
        inflater = LayoutInflater.from(context);
        mSelectedItemsIds = new SparseBooleanArray();
        this.userSocietyArrayList = userSocietyArrayList;
        this.userId = userId;
        this.updateGroup = updateGroup;
    }


    public View getView(int position, View view, ViewGroup parent) {
        final ViewHolder holder;
        if (view == null) {
            holder = new ViewHolder();
            view = inflater.inflate(R.layout.listview_item, parent, false);
            holder.userNameText = (TextView) view.findViewById(R.id.userNameText);
            holder.societyNameText = (TextView) view.findViewById(R.id.societyNameText);
            holder.flatNoText = (TextView) view.findViewById(R.id.flatNoText);
            holder.removeMember = (TextView) view.findViewById(R.id.removeMember);
            holder.mainLinear = (LinearLayout) view.findViewById(R.id.mainLinear);
            view.setTag(holder);
        } else {
            holder = (ViewHolder) view.getTag();
        }
        if (userSocietyArrayList.get(position).getUser() != null) {
            String name = userSocietyArrayList.get(position).getUser().getFirstname()
                    + " " + userSocietyArrayList.get(position).getUser().getLastname();
            holder.userNameText.setText(name);
        } else {
            holder.userNameText.setText("No Data");
        }
        if (userSocietyArrayList.get(position).getSociety() != null) {
            holder.societyNameText.setText(userSocietyArrayList.get(position).getSociety().getBuildingName());
        } else {
            holder.societyNameText.setText("No Data");
        }
        if (userSocietyArrayList.get(position).getUserFlat() != null) {
            holder.flatNoText.setText(userSocietyArrayList.get(position).getUserFlat().getFlat());
        } else {
            holder.flatNoText.setText("No Data");
        }
        if (updateGroup != null && updateGroup.equals("Update Group") && userId.contains(userSocietyArrayList.get(position).getUser().getId())) {
            holder.removeMember.setVisibility(View.VISIBLE);
            holder.removeMember.setTag(position);
            holder.removeMember.setOnClickListener(this);
        }
        return view;
    }

    private class ViewHolder {

        TextView userNameText;
        TextView societyNameText;
        TextView flatNoText;
        TextView removeMember;
        LinearLayout mainLinear;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.removeMember:
                int position = (int) v.getTag();
                //Log.e("removeMember", "clicked " + position);
                break;
        }
    }

    /*@Override
    public void remove(UserSociety object) {
        userSocietyArrayList.remove(object);
        notifyDataSetChanged();
    }*/

    @Override
    public void add(UserSociety object) {
        //super.add(object);
        userSocietyArrayList.add(object);
        notifyDataSetChanged();
    }

    public ArrayList<UserSociety> getWorldPopulation() {
        return userSocietyArrayList;
    }

    public void toggleSelection(int position) {
        selectView(position, !mSelectedItemsIds.get(position));
    }

    public void removeSelection() {
        mSelectedItemsIds = new SparseBooleanArray();
        notifyDataSetChanged();
    }

    public void selectView(int position, boolean value) {
        if (value) {
            mSelectedItemsIds.put(position, value);
        } else {
            mSelectedItemsIds.delete(position);
        }
        notifyDataSetChanged();
    }

    public int getSelectedCount() {
        return mSelectedItemsIds.size();
    }

    public SparseBooleanArray getSelectedIds() {
        return mSelectedItemsIds;
    }

}