package com.android.lockated.crm.adapters;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.android.lockated.crm.fragment.myzone.classified.MyClassifiedFragment;
import com.android.lockated.crm.fragment.myzone.classified.SellFragment;


public class SubClassifiedAdapter extends FragmentPagerAdapter {
    int tabCount;
    FragmentManager fragmentManager;

    public SubClassifiedAdapter(FragmentManager fm, int tabCount) {
        super(fm);
        this.tabCount = tabCount;
        fragmentManager = fm;
    }

    @Override
    public Fragment getItem(int pos) {

        Fragment fragment;

        switch (pos) {

            case 0:
                fragment = new SellFragment();
                break;
            case 1:
                fragment = new MyClassifiedFragment();
                break;
            default:
                fragment = new SellFragment();
                break;
        }
        return fragment;
    }

    @Override
    public int getCount() {
        return tabCount;
    }

}

