package com.android.lockated.account.fragment;

import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v4.content.ContextCompat;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import com.android.lockated.Interfaces.IServantDetailAdded;
import com.android.lockated.R;
import com.android.lockated.model.ServantData;
import com.android.lockated.network.ConnectionDetector;
import com.android.lockated.preferences.LockatedPreferences;
import com.android.lockated.request.LockatedRequestError;
import com.android.lockated.request.LockatedVolleyRequestQueue;
import com.android.lockated.utils.ApplicationURL;
import com.android.lockated.utils.Utilities;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;

public class AddServantDetailFragment extends DialogFragment implements View.OnClickListener, AdapterView.OnItemSelectedListener, Response.Listener, Response.ErrorListener
{
    private View mAddServantDetailView;

    private TextView mTextViewFamilyMemberName;
    private TextView mTextViewFamilyMemberRelationship;
    private TextView mTextViewFamilyMemberMobile;
    private TextView mTextViewFamilyMemberDob;
    private TextView mButtonAddAddress;

    private Spinner mSpinnerFamilyMemberGender;
    private Spinner mSpinnerFamilyMemberMaritalStatus;

    private EditText mEditTextFamilyMemberName;
    private EditText mEditTextFamilyMemberNumber;

    private int mGenderId;
    private int requestId;
    private int mMaritalStatusId;

    private RequestQueue mQueue;
    private ProgressDialog mProgressDialog;
    private LockatedPreferences mLockatedPreferences;

    private IServantDetailAdded mIServantDetailAdded;
    private ServantData mServantData;
    private ArrayList<ServantData> mServantDataList;
    private int mPosition;

    public static final String REQUEST_TAG = "AddServantDetailFragment";

    public AddServantDetailFragment()
    {

    }

    public void setServantDataOnEdit(int id, ServantData data, int position)
    {
        requestId = id;
        mServantData = data;
        mPosition = position;
    }

    public void setListener(IServantDetailAdded listener)
    {
        mIServantDetailAdded = listener;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setStyle(STYLE_NO_TITLE, 0);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        mAddServantDetailView = inflater.inflate(R.layout.fragment_add_new_servant, container);
        init();
        return mAddServantDetailView;
    }

    @Override
    public void onStart()
    {
        super.onStart();
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(getDialog().getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        getDialog().getWindow().setAttributes(lp);
    }

    private void init()
    {
        mServantDataList = new ArrayList<>();
        mLockatedPreferences = new LockatedPreferences(getActivity());

        mTextViewFamilyMemberName = (TextView) mAddServantDetailView.findViewById(R.id.mTextViewFamilyMemberName);
        mTextViewFamilyMemberRelationship = (TextView) mAddServantDetailView.findViewById(R.id.mTextViewFamilyMemberRelationship);
        mTextViewFamilyMemberMobile = (TextView) mAddServantDetailView.findViewById(R.id.mTextViewFamilyMemberMobile);
        mTextViewFamilyMemberDob = (TextView) mAddServantDetailView.findViewById(R.id.mTextViewFamilyMemberDob);

        TextView mButtonAddAddressCancel = (TextView) mAddServantDetailView.findViewById(R.id.mButtonAddAddressCancel);
        mButtonAddAddress = (TextView) mAddServantDetailView.findViewById(R.id.mButtonAddAddress);

        mSpinnerFamilyMemberGender = (Spinner) mAddServantDetailView.findViewById(R.id.mSpinnerFamilyMemberGender);
        mSpinnerFamilyMemberMaritalStatus = (Spinner) mAddServantDetailView.findViewById(R.id.mSpinnerFamilyMemberMaritalStatus);

        mEditTextFamilyMemberName = (EditText) mAddServantDetailView.findViewById(R.id.mEditTextFamilyMemberName);
        mEditTextFamilyMemberNumber = (EditText) mAddServantDetailView.findViewById(R.id.mEditTextFamilyMemberNumber);

        if (requestId == 0)
        {
            mButtonAddAddress.setText(R.string.add);
        }
        else
        {
            mButtonAddAddress.setText(R.string.update);
        }

        mSpinnerFamilyMemberGender.setOnItemSelectedListener(this);
        mSpinnerFamilyMemberMaritalStatus.setOnItemSelectedListener(this);
        mTextViewFamilyMemberDob.setOnClickListener(this);
        mButtonAddAddress.setOnClickListener(this);
        mButtonAddAddressCancel.setOnClickListener(this);

        hideName();
        hideMobileNumber();
        setEditTextListener();
        disableAddButton();
        setEditedData();
    }

    private void setEditedData()
    {
        if (requestId == 1)
        {
            if (mServantData.getGender().equalsIgnoreCase("M"))
            {
                mSpinnerFamilyMemberGender.setSelection(1);
            } else if (mServantData.getGender().equalsIgnoreCase("F"))
            {
                mSpinnerFamilyMemberGender.setSelection(2);
            } else
            {
                mSpinnerFamilyMemberGender.setSelection(0);
            }

            if (mServantData.getMarried().equalsIgnoreCase("Y"))
            {
                mSpinnerFamilyMemberMaritalStatus.setSelection(1);
            } else if (mServantData.getMarried().equalsIgnoreCase("N"))
            {
                mSpinnerFamilyMemberMaritalStatus.setSelection(2);
            } else
            {
                mSpinnerFamilyMemberMaritalStatus.setSelection(0);
            }

            if (mServantData.getDob() == "null")
            {
                mTextViewFamilyMemberDob.setText(R.string.dob);
            } else
            {
                mTextViewFamilyMemberDob.setText(mServantData.getDob());
            }

            mEditTextFamilyMemberName.setText(mServantData.getName());
            mEditTextFamilyMemberNumber.setText(mServantData.getMobile());
        }
    }

    private void hideName()
    {
        mTextViewFamilyMemberName.setVisibility(View.INVISIBLE);
    }

    private void hideRelationship()
    {
        mTextViewFamilyMemberRelationship.setVisibility(View.INVISIBLE);
    }

    private void hideMobileNumber()
    {
        mTextViewFamilyMemberMobile.setVisibility(View.INVISIBLE);
    }

    private void showName()
    {
        mTextViewFamilyMemberName.setVisibility(View.VISIBLE);
    }

    private void showRelationship()
    {
        mTextViewFamilyMemberRelationship.setVisibility(View.VISIBLE);
    }

    private void showMobileNumber()
    {
        mTextViewFamilyMemberMobile.setVisibility(View.VISIBLE);
    }

    private void setEditTextListener()
    {
        mEditTextFamilyMemberName.addTextChangedListener(new TextWatcher()
        {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after)
            {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count)
            {
                if (s.length() > 0)
                {
                    showName();

                    if (mEditTextFamilyMemberNumber.getText().toString().length() > 0)
                    {
                        enableAddButton();
                    }
                } else
                {
                    disableAddButton();
                    hideName();
                }
            }

            @Override
            public void afterTextChanged(Editable s)
            {

            }
        });

        mEditTextFamilyMemberNumber.addTextChangedListener(new TextWatcher()
        {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after)
            {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count)
            {
                if (s.length() > 0)
                {
                    showMobileNumber();

                    if (mEditTextFamilyMemberName.getText().toString().length() > 0)
                    {
                        enableAddButton();
                    }
                } else
                {
                    disableAddButton();
                    hideMobileNumber();
                }
            }

            @Override
            public void afterTextChanged(Editable s)
            {

            }
        });
    }

    private void disableAddButton()
    {
        mButtonAddAddress.setEnabled(false);
        mButtonAddAddress.setTextColor(ContextCompat.getColor(getActivity(), R.color.secondary_text));
    }

    private void enableAddButton()
    {
        mButtonAddAddress.setEnabled(true);
        mButtonAddAddress.setTextColor(ContextCompat.getColor(getActivity(), R.color.primary));
    }

    private void openDatePicker(final TextView textView)
    {
        final Calendar c = Calendar.getInstance();
        int year = c.get(Calendar.YEAR);
        int month = c.get(Calendar.MONTH);
        int day = c.get(Calendar.DAY_OF_MONTH);

        DatePickerDialog datePickerDialog = new DatePickerDialog(getActivity(),
                new DatePickerDialog.OnDateSetListener()
                {
                    @Override
                    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth)
                    {
                        textView.setText(year + "-" + (monthOfYear + 1) + "-" + dayOfMonth);
                    }
                }, year, month, day);
        datePickerDialog.show();
    }

    private void onAddServantButtonCicked()
    {
        String strFamilyMemberName = mEditTextFamilyMemberName.getText().toString();
        String strFamilyMemberNumber = mEditTextFamilyMemberNumber.getText().toString();
        String strFamilyMemberDob = mTextViewFamilyMemberDob.getText().toString();
        String strSelectedGender = "";
        String strSelectedMaritalStatus = "";

        boolean addAddressCancel = false;
        boolean addFamilyDetailCancel = false;
        View addAddressFocusView = null;
        String message = getActivity().getResources().getString(R.string.signup_blank_field_error);

        if (TextUtils.isEmpty(strFamilyMemberName))
        {
            addAddressFocusView = mEditTextFamilyMemberName;
            addAddressCancel = true;
            message = getActivity().getResources().getString(R.string.signup_blank_field_error);
        }

        // Validation for Number field, it should not be blank
        if (TextUtils.isEmpty(strFamilyMemberNumber))
        {
            addAddressFocusView = mEditTextFamilyMemberNumber;
            addAddressCancel = true;
            message = getActivity().getResources().getString(R.string.signup_blank_field_error);
        }

        if (strFamilyMemberNumber.length() != 10)
        {
            addAddressFocusView = mEditTextFamilyMemberNumber;
            addAddressCancel = true;
            message = getActivity().getResources().getString(R.string.mobile_number_error);
        }

        if (mGenderId == 0)
        {
            addFamilyDetailCancel = true;
            message = getActivity().getResources().getString(R.string.gender_message_error);
        } else
        {
            if (mGenderId == 1)
            {
                strSelectedGender = "M";
            } else
            {
                strSelectedGender = "F";
            }
        }

        if (mMaritalStatusId == 0)
        {
            strSelectedMaritalStatus = "";
            addFamilyDetailCancel = true;
            message = getActivity().getResources().getString(R.string.matrial_message_error);
        } else if (mMaritalStatusId == 1)
        {
            strSelectedMaritalStatus = "Y";
        } else
        {
            strSelectedMaritalStatus = "N";
        }

        if (strFamilyMemberDob.equalsIgnoreCase(getString(R.string.dob)))
        {
            addFamilyDetailCancel = false;
            strFamilyMemberDob = "";
        }

        if (addAddressCancel)
        {
            addAddressFocusView.requestFocus();
            Utilities.showToastMessage(getActivity(), message);
        } else if (addFamilyDetailCancel)
        {
            Utilities.showToastMessage(getActivity(), message);
        } else
        {
            if (ConnectionDetector.isConnectedToInternet(getActivity()))
            {
                mProgressDialog = ProgressDialog.show(getActivity(), "", "Please Wait...", false);
                mProgressDialog.show();

                if (requestId == 0)
                {
                    JSONObject servantObject = new JSONObject();
                    JSONObject jsonObject = new JSONObject();
                    JSONArray jsonArray = new JSONArray();

                    try
                    {
                        jsonObject.put("name", strFamilyMemberName);
                        jsonObject.put("gender", strSelectedGender);
                        jsonObject.put("mobile", strFamilyMemberNumber);
                        jsonObject.put("dob", strFamilyMemberDob);
                        jsonObject.put("married", strSelectedMaritalStatus);

                        jsonArray.put(0, jsonObject);

                        servantObject.put("user_servants", jsonArray);
                    }
                    catch (JSONException e)
                    {
                        e.printStackTrace();
                    }

//                    int socketTimeout = Utilities.REQUEST_TIME_OUT;
//                    mQueue = LockatedVolleyRequestQueue.getInstance(getActivity()).getRequestQueue();
//                    LockatedJSONObjectRequest lockatedJSONObjectRequest;
//                    lockatedJSONObjectRequest = new LockatedJSONObjectRequest(Request.Method.POST, ApplicationURL.addServantDetailUrl + "?token=" + mLockatedPreferences.getLockatedToken(), servantObject, this, this);
//                    lockatedJSONObjectRequest.setTag(REQUEST_TAG);
//                    RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
//                    lockatedJSONObjectRequest.setRetryPolicy(policy);
//                    mQueue.add(lockatedJSONObjectRequest);

                    LockatedVolleyRequestQueue lockatedVolleyRequestQueue = LockatedVolleyRequestQueue.getInstance(getActivity());
                    lockatedVolleyRequestQueue.sendRequest(REQUEST_TAG, Request.Method.POST, ApplicationURL.addServantDetailUrl + "?token=" + mLockatedPreferences.getLockatedToken(), servantObject, this, this);
                }
                else
                {
                    JSONObject jsonObject = new JSONObject();
                    JSONObject familyObject = new JSONObject();
                    try
                    {
                        jsonObject.put("name", strFamilyMemberName);
                        jsonObject.put("gender", strSelectedGender);
                        jsonObject.put("mobile", strFamilyMemberNumber);
                        jsonObject.put("dob", strFamilyMemberDob);
                        jsonObject.put("married", strSelectedMaritalStatus);

                        familyObject.put("user_servant", jsonObject);
                    }
                    catch (JSONException ex)
                    {
                        ex.printStackTrace();
                    }

//                    int socketTimeout = Utilities.REQUEST_TIME_OUT;
//                    mQueue = LockatedVolleyRequestQueue.getInstance(getActivity()).getRequestQueue();
//                    LockatedJSONObjectRequest lockatedJSONObjectRequest;
//                    lockatedJSONObjectRequest = new LockatedJSONObjectRequest(Request.Method.PUT, ApplicationURL.updateServantDetailUrl + mServantData.getId()+".json?token=" + mLockatedPreferences.getLockatedToken(), familyObject, this, this);
//                    lockatedJSONObjectRequest.setTag(REQUEST_TAG);
//                    RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
//                    lockatedJSONObjectRequest.setRetryPolicy(policy);
//                    mQueue.add(lockatedJSONObjectRequest);

                    LockatedVolleyRequestQueue lockatedVolleyRequestQueue = LockatedVolleyRequestQueue.getInstance(getActivity());
                    lockatedVolleyRequestQueue.sendRequest(REQUEST_TAG, Request.Method.PUT, ApplicationURL.updateServantDetailUrl + mServantData.getId()+".json?token=" + mLockatedPreferences.getLockatedToken(), familyObject, this, this);
                }
            } else
            {
                Utilities.showToastMessage(getActivity(), getActivity().getResources().getString(R.string.internet_connection_error));
            }
        }
    }

    @Override
    public void onErrorResponse(VolleyError error)
    {
        mProgressDialog.dismiss();
        if (getActivity() != null)
        {
            LockatedRequestError.onRequestError(getActivity(), error);
        }
    }

    @Override
    public void onResponse(Object response)
    {
        mProgressDialog.dismiss();
        if (getActivity() != null)
        {
            if (getDialog() != null)
            {
                getDialog().dismiss();
                JSONObject jsonObject = (JSONObject) response;

                if (requestId == 0)
                {
                    try
                    {
                        JSONArray jsonArray = jsonObject.getJSONArray("user_servants");
                        for (int i = 0; i < jsonArray.length(); i++)
                        {
                            JSONObject familyObject = jsonArray.getJSONObject(i);
                            ServantData servantData = new ServantData(familyObject);
                            mServantDataList.add(servantData);
                        }

                        mIServantDetailAdded.onNewServantAdded(mServantDataList, requestId);
                    }
                    catch (JSONException e)
                    {
                        e.printStackTrace();
                    }
                }
                else
                {
                    try
                    {
                        mServantData = new ServantData(jsonObject);
                        mIServantDetailAdded.onNewServantUpdated(mServantData, requestId, mPosition);
                    }
                    catch (Exception e)
                    {
                        e.printStackTrace();
                    }
                }
            }
        }
    }

    @Override
    public void onClick(View v)
    {
        switch (v.getId())
        {
            case R.id.mButtonAddAddress:
                if (getDialog() != null)
                {
                    onAddServantButtonCicked();
                }
                break;
            case R.id.mButtonAddAddressCancel:
                if (getDialog() != null)
                {
                    getDialog().dismiss();
                }
                break;
            case R.id.mTextViewFamilyMemberDob:
                openDatePicker(mTextViewFamilyMemberDob);
                break;
        }
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id)
    {
        switch (parent.getId())
        {
            case R.id.mSpinnerFamilyMemberGender:
                mGenderId = position;
                break;
            case R.id.mSpinnerFamilyMemberMaritalStatus:
                mMaritalStatusId = position;
                break;
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent)
    {

    }
}
