package com.android.lockated.pushnotification;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;

/*import com.clevertap.android.sdk.InstallReferrerBroadcastReceiver;*/
import com.clevertap.android.sdk.InstallReferrerBroadcastReceiver;
import com.google.android.gms.analytics.CampaignTrackingReceiver;



/*
 *  A simple Broadcast Receiver to receive an INSTALL_REFERRER
 *  intent and pass it to other receivers, including
 *  the Google Analytics receiver.
 */


public class GoogleCampaignReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {

        Log.e("GoogleCampaignReceiver", "called");
        // Pass the intent to other receivers.
        //new InstallReferrerBroadcastReceiver().onReceive(context, intent);
        // When you're done, pass the intent to the Google Analytics receiver.
        new CampaignTrackingReceiver().onReceive(context, intent);
        Bundle extras = intent.getExtras();
        String referrerString = extras.getString("referrer");
        Log.e("GoogleCampaignReceiver", "extras " + extras.toString());
        Log.e("GoogleCampaignReceiver", "referrer " + referrerString);

        new InstallReferrerBroadcastReceiver().onReceive(context, intent);
    }
}