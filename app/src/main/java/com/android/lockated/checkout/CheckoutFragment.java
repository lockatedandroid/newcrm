package com.android.lockated.checkout;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.lockated.LockatedApplication;
import com.android.lockated.R;
import com.android.lockated.account.fragment.MyAddressFragment;
import com.android.lockated.information.AccountController;
import com.android.lockated.model.AccountData;
import com.android.lockated.model.CartDetail;
import com.android.lockated.model.MyAddressData;
import com.android.lockated.network.ConnectionDetector;
import com.android.lockated.preferences.LockatedPreferences;
import com.android.lockated.request.LockatedRequestError;
import com.android.lockated.request.LockatedVolleyRequestQueue;
import com.android.lockated.utils.ApplicationURL;
import com.android.lockated.utils.DatePickerFragment;
import com.android.lockated.utils.TimePickerFragment;
import com.android.lockated.utils.Utilities;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class CheckoutFragment extends Fragment implements View.OnClickListener, Response.Listener, Response.ErrorListener {
    private static final String COUPON_REQUEST_TAG = "CheckoutFragment:ApplyCoupon";
    private static final String PROCEED_REQUEST_TAG = "CheckoutFragment:ProceedToPayment";

    //private View mCheckoutView;
    private ProgressDialog mProgressDialog;

    private TextView mTextViewCartApplyCode;
    private TextView mTextViewCartTotalAmount;
    private TextView mTextViewProceedToPayment;
    private TextView mTextViewCartAppliedPromoCode;
    private TextView mTextViewCartDeliveryCharges;
    private TextView mTextViewCartDeliveryDiscount;
    private TextView mTextViewCartTotalGrandAmount;
    private TextView mTextViewPreferredDeliveryDate;
    private TextView mTextViewPreferredDeliveryTime;
    private TextView mTextViewCheckoutDeliveryAddress;
    private TextView mTextViewCheckoutDeliveryAddressChange;

    private LinearLayout mLinearLayoutCheckoutApplyPromo;
    private LinearLayout mLinearLayoutCheckoutAppliedPromo;

    private EditText mEditTextCartPromoCode;

    private String selectedDate;
    private String selectedTime;

    private ImageView mImageViewDeliveryDate;
    private ImageView mImageViewDeliveryTime;
    private ImageView mImageViewRemovePromoCode;

    private MyAddressData myAddressData;
    private ArrayList<CartDetail> mCartDetail;

    private LockatedPreferences mLockatedPreferences;
    private LockatedVolleyRequestQueue mLockatedVolleyRequestQueue;
    String screenName = "Checkout Screen";
    String promoCode;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View mCheckoutView = inflater.inflate(R.layout.fragment_checkout, container, false);
        init(mCheckoutView);
        Utilities.ladooIntegration(getActivity(), screenName);
        Utilities.lockatedGoogleAnalytics(screenName, getString(R.string.visited), screenName);
        refreshValues();
        return mCheckoutView;
    }

    private void refreshValues() {
        setCheckoutDetails();
        setAddressDetails();
    }

    private void setAddressDetails() {
        myAddressData = ((LockatedApplication) getActivity().getApplicationContext()).getMyAddressData();
        selectedDate = ((LockatedApplication) getActivity().getApplicationContext()).getmDeliveryDate();
        selectedTime = ((LockatedApplication) getActivity().getApplicationContext()).getmDeliveryTime();
        if (myAddressData != null) {
            mTextViewCheckoutDeliveryAddressChange.setVisibility(View.VISIBLE);
            mTextViewCheckoutDeliveryAddress.setText(myAddressData.getFirstName() + " " + myAddressData.getLastName() + "\n\n" + myAddressData.getAddress1() + " " + myAddressData.getAddress2() + " " + myAddressData.getCity() + " " + myAddressData.getState() + " " + myAddressData.getZipcode());
        }

        mTextViewPreferredDeliveryDate.setText(selectedDate);
        mTextViewPreferredDeliveryTime.setText(selectedTime);
    }

    private void init(View mCheckoutView) {

        mLockatedPreferences = new LockatedPreferences(getActivity());

        mTextViewCartApplyCode = (TextView) mCheckoutView.findViewById(R.id.mTextViewCartApplyCode);
        mTextViewCartTotalAmount = (TextView) mCheckoutView.findViewById(R.id.mTextViewCartTotalAmount);
        mTextViewProceedToPayment = (TextView) mCheckoutView.findViewById(R.id.mTextViewProceedToPayment);
        mTextViewCartDeliveryCharges = (TextView) mCheckoutView.findViewById(R.id.mTextViewCartDeliveryCharges);
        mTextViewCartDeliveryDiscount = (TextView) mCheckoutView.findViewById(R.id.mTextViewCartDeliveryDiscount);
        mTextViewCartTotalGrandAmount = (TextView) mCheckoutView.findViewById(R.id.mTextViewCartTotalGrandAmount);
        mTextViewCartAppliedPromoCode = (TextView) mCheckoutView.findViewById(R.id.mTextViewCartAppliedPromoCode);
        mTextViewPreferredDeliveryDate = (TextView) mCheckoutView.findViewById(R.id.mTextViewPreferredDeliveryDate);
        mTextViewPreferredDeliveryTime = (TextView) mCheckoutView.findViewById(R.id.mTextViewPreferredDeliveryTime);
        mTextViewCheckoutDeliveryAddress = (TextView) mCheckoutView.findViewById(R.id.mTextViewCheckoutDeliveryAddress);
        mTextViewCheckoutDeliveryAddressChange = (TextView) mCheckoutView.findViewById(R.id.mTextViewCheckoutDeliveryAddressChange);

        mEditTextCartPromoCode = (EditText) mCheckoutView.findViewById(R.id.mEditTextCartPromoCode);

        mLinearLayoutCheckoutApplyPromo = (LinearLayout) mCheckoutView.findViewById(R.id.mLinearLayoutCheckoutApplyPromo);
        mLinearLayoutCheckoutAppliedPromo = (LinearLayout) mCheckoutView.findViewById(R.id.mLinearLayoutCheckoutAppliedPromo);

        mImageViewDeliveryDate = (ImageView) mCheckoutView.findViewById(R.id.mImageViewDeliveryDate);
        mImageViewDeliveryTime = (ImageView) mCheckoutView.findViewById(R.id.mImageViewDeliveryTime);
        mImageViewRemovePromoCode = (ImageView) mCheckoutView.findViewById(R.id.mImageViewRemovePromoCode);

        mTextViewCheckoutDeliveryAddressChange.setVisibility(View.GONE);

        mTextViewCartApplyCode.setOnClickListener(this);
        mImageViewDeliveryDate.setOnClickListener(this);
        mImageViewDeliveryTime.setOnClickListener(this);
        mTextViewProceedToPayment.setOnClickListener(this);
        mImageViewRemovePromoCode.setOnClickListener(this);
        mTextViewPreferredDeliveryDate.setOnClickListener(this);
        mTextViewPreferredDeliveryTime.setOnClickListener(this);
        mTextViewCheckoutDeliveryAddress.setOnClickListener(this);
        mTextViewCheckoutDeliveryAddressChange.setOnClickListener(this);
    }

    private void setCheckoutDetails() {
        ArrayList<AccountData> mAccountData = AccountController.getInstance().getmAccountDataList();

        if (mAccountData.size() > 0) {
            mCartDetail = mAccountData.get(0).getCartDetailList();
            if (mCartDetail != null && mCartDetail.size() > 0) {
                double cost = Double.parseDouble(mCartDetail.get(0).getItemTotal());
                double deliveryCost = Double.parseDouble(mCartDetail.get(0).getShipTotal());
                double discount = Double.parseDouble(mCartDetail.get(0).getAdjustmentTotal());

                double grandTotal = cost + deliveryCost + discount;

                mTextViewCartTotalAmount.setText(getActivity().getString(R.string.rupees_symbol) + " " + mCartDetail.get(0).getItemTotal());
                mTextViewCartDeliveryCharges.setText(getActivity().getString(R.string.rupees_symbol) + " " + deliveryCost + "");
                mTextViewCartDeliveryDiscount.setText(getActivity().getString(R.string.rupees_symbol) + " " + discount + "");

                mTextViewCartTotalGrandAmount.setText(getActivity().getString(R.string.rupees_symbol) + " " + grandTotal + "");

                if (mCartDetail.get(0).getAdjustmentTotal().equalsIgnoreCase("0.0")) {
                    mLinearLayoutCheckoutApplyPromo.setVisibility(View.VISIBLE);
                    mLinearLayoutCheckoutAppliedPromo.setVisibility(View.GONE);
                    mTextViewCartAppliedPromoCode.setText("");
                } else {
                    mLinearLayoutCheckoutAppliedPromo.setVisibility(View.VISIBLE);
                    mLinearLayoutCheckoutApplyPromo.setVisibility(View.GONE);
                    mTextViewCartAppliedPromoCode.setText(mCartDetail.get(0).getLabel());
                }
            }
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.mTextViewCheckoutDeliveryAddress:
                onSelectAddressClicked();
                break;

            case R.id.mTextViewCheckoutDeliveryAddressChange:
                onAddressChangeClicked();
                break;

            case R.id.mImageViewDeliveryDate:
                openDatePicker();
                break;

            case R.id.mImageViewDeliveryTime:
                onDeliveryTimeClicked();
                break;
            case R.id.mTextViewCartApplyCode:
                onApplyCouponClicked();
                break;
            case R.id.mTextViewProceedToPayment:
                onProceedToPaymentClicked();
                break;
            case R.id.mImageViewRemovePromoCode:
                onRemovePromoClicked();
                break;
        }
    }

    private void onRemovePromoClicked() {
        if (mCartDetail != null && mCartDetail.size() > 0) {
            mProgressDialog = ProgressDialog.show(getActivity(), "", "Please Wait...", false);
            mLockatedVolleyRequestQueue = LockatedVolleyRequestQueue.getInstance(getActivity());
            mLockatedVolleyRequestQueue.sendRequest(COUPON_REQUEST_TAG, Request.Method.PUT,
                    ApplicationURL.applyCouponCodeUrl + mCartDetail.get(0).getNumber() + "/remove_coupon_code?token="
                            + mLockatedPreferences.getLockatedToken(), null, this, this);
        }
    }

    private void onProceedToPaymentClicked() {
        myAddressData = ((LockatedApplication) getActivity().getApplicationContext()).getMyAddressData();
        selectedDate = ((LockatedApplication) getActivity().getApplicationContext()).getmDeliveryDate();
        selectedTime = ((LockatedApplication) getActivity().getApplicationContext()).getmDeliveryTime();

        String strPreferredDate = mTextViewPreferredDeliveryDate.getText().toString();
        String strPreferredTime = mTextViewPreferredDeliveryTime.getText().toString();

        String strMessage = "";
        boolean checkoutCancel = false;

        if (TextUtils.isEmpty(strPreferredDate)) {
            checkoutCancel = true;
            strMessage = "Please select preferred date";
        }

        if (TextUtils.isEmpty(strPreferredTime)) {
            checkoutCancel = true;
            strMessage = "Please select preferred time";
        }

        if (myAddressData == null) {
            checkoutCancel = true;
            strMessage = "Please select delivery address";
        }

        if (checkoutCancel) {
            Utilities.showToastMessage(getActivity(), strMessage);
        } else {
            if (myAddressData != null && selectedDate != null && selectedTime != null) {
                if (ConnectionDetector.isConnectedToInternet(getActivity())) {
                    mProgressDialog = ProgressDialog.show(getActivity(), "", "Please Wait...", false);
                    if (mCartDetail != null && mCartDetail.size() > 0) {
                        JSONObject jsonObject = new JSONObject();
                        JSONObject orderObject = new JSONObject();

                        try {
                            orderObject.put("address_id", myAddressData.getAddressId());
                            orderObject.put("preferred_date", mTextViewPreferredDeliveryDate.getText().toString());
                            orderObject.put("preferred_time", mTextViewPreferredDeliveryTime.getText().toString());

                            jsonObject.put("order", orderObject);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        String url = ApplicationURL.proceedToPaymentUrl + mCartDetail.get(0).getNumber()
                                + "/next.json?token=" + mLockatedPreferences.getLockatedToken();
                        mLockatedVolleyRequestQueue = LockatedVolleyRequestQueue.getInstance(getActivity());
                        mLockatedVolleyRequestQueue.sendRequest(PROCEED_REQUEST_TAG, Request.Method.PUT,
                                url, jsonObject, this, this);
                    }
                } else {
                    Utilities.showToastMessage(getActivity(), getActivity().getResources().getString(R.string.internet_connection_error));
                }
            } else {
                Utilities.showToastMessage(getActivity(), getActivity().getString(R.string.signup_blank_field_error));
            }
        }
    }

    private void onApplyCouponClicked() {
        promoCode = mEditTextCartPromoCode.getText().toString();
        if (!TextUtils.isEmpty(promoCode)) {
            if (ConnectionDetector.isConnectedToInternet(getActivity())) {
                if (mCartDetail != null && mCartDetail.size() > 0) {
                    mProgressDialog = ProgressDialog.show(getActivity(), "", "Please Wait...", false);

                    JSONObject jsonObject = new JSONObject();
                    JSONObject orderObject = new JSONObject();

                    try {
                        orderObject.put("coupon_code", mEditTextCartPromoCode.getText().toString());
                        jsonObject.put("order", orderObject);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    String url = ApplicationURL.applyCouponCodeUrl + mCartDetail.get(0).getNumber() +
                            ".json?token=" + mLockatedPreferences.getLockatedToken();
                    mLockatedVolleyRequestQueue = LockatedVolleyRequestQueue.getInstance(getActivity());
                    mLockatedVolleyRequestQueue.sendRequest(COUPON_REQUEST_TAG, Request.Method.PUT,
                            url, jsonObject, this, this);
                }
            } else {
                Utilities.showToastMessage(getActivity(), getActivity().getResources().getString(R.string.internet_connection_error));
            }
        } else {
            Utilities.showToastMessage(getActivity(), "Please enter promo code");
        }
    }

    private void openDatePicker() {
        DatePickerFragment datePickerFragment = new DatePickerFragment();
        datePickerFragment.setDatePickerView(mTextViewPreferredDeliveryDate);
        DialogFragment newFragment = datePickerFragment;
        newFragment.show(getActivity().getSupportFragmentManager(), "DatePicker");
    }

    private void onDeliveryTimeClicked() {
        TimePickerFragment timePickerFragment = new TimePickerFragment();
        timePickerFragment.setTimePickerView(mTextViewPreferredDeliveryTime);
        DialogFragment newFragment = timePickerFragment;
        newFragment.show(getActivity().getSupportFragmentManager(), "TimePicker");
    }

    private void onAddressChangeClicked() {
        MyAddressFragment myAddressFragment = new MyAddressFragment();
        myAddressFragment.setViewId(1);
        getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.mCheckoutContainer, myAddressFragment).addToBackStack("Checkout").commit();
    }

    private void onSelectAddressClicked() {
        myAddressData = ((LockatedApplication) getActivity().getApplicationContext()).getMyAddressData();
        if (myAddressData == null) {
            MyAddressFragment myAddressFragment = new MyAddressFragment();
            myAddressFragment.setViewId(1);
            getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.mCheckoutContainer, myAddressFragment).addToBackStack("Checkout").commit();
        } else {
            mTextViewCheckoutDeliveryAddressChange.setVisibility(View.VISIBLE);
            mTextViewCheckoutDeliveryAddress.setText(myAddressData.getFirstName() + " " + myAddressData.getLastName() + "\n\n" + myAddressData.getAddress1() + " " + myAddressData.getAddress2() + " " + myAddressData.getCity() + " " + myAddressData.getState() + " " + myAddressData.getZipcode());
        }
    }

    @Override
    public void onErrorResponse(VolleyError error) {
        if (mProgressDialog != null && mProgressDialog.isShowing()) {
            mProgressDialog.dismiss();
        }
        if (getActivity() != null) {
            LockatedRequestError.onRequestError(getActivity(), error);
        }
    }

    @Override
    public void onResponse(Object response) {
        if (mProgressDialog != null && mProgressDialog.isShowing()) {
            mProgressDialog.dismiss();
        }
        if (getActivity() != null) {
            try {
                if (mLockatedVolleyRequestQueue.getLockatedJSONObjectRequest().getTag().toString().equals(COUPON_REQUEST_TAG)) {
                    JSONObject jsonObject = (JSONObject) response;
                    int code = jsonObject.optInt("code");
                    if (code == 0) {
                        CartDetail cartDetail = new CartDetail(jsonObject);
                        ArrayList<AccountData> accountDatas = AccountController.getInstance().getmAccountDataList();
                        if (accountDatas.size() > 0) {
                            accountDatas.get(0).getCartDetailList().clear();
                            accountDatas.get(0).getCartDetailList().add(cartDetail);
                        }
                        cartDetail.setPromoCode(promoCode);
                        refreshValues();

                    } else {
                        Utilities.showToastMessage(getActivity(), jsonObject.getString("error"));
                    }
                } else if (mLockatedVolleyRequestQueue.getLockatedJSONObjectRequest().getTag().toString().equals(PROCEED_REQUEST_TAG)) {
                    JSONObject jsonObject = (JSONObject) response;

                    if (jsonObject.has("error")) {
                        Utilities.showToastMessage(getActivity(), jsonObject.getString("error"));
                    } else {
                        CartDetail cartDetail = new CartDetail(jsonObject);
                        ArrayList<AccountData> accountDatas = AccountController.getInstance().getmAccountDataList();
                        if (accountDatas.size() > 0) {
                            accountDatas.get(0).getCartDetailList().clear();
                            accountDatas.get(0).getCartDetailList().add(cartDetail);
                        }
                        cartDetail.setPromoCode(promoCode);
                        PaymentFragment paymentFragment = new PaymentFragment();
                        getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.mCheckoutContainer, paymentFragment).commit();
                    }
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }
}
