
package com.android.lockated.model.AdminModel.AdminGatekeeper;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class Gatekeeper {

    @SerializedName("code")
    @Expose
    private Integer code;
    @SerializedName("gatekeepers")
    @Expose
    private ArrayList<GatekeeperAdminModel> gatekeepers = new ArrayList<GatekeeperAdminModel>();

    /**
     * 
     * @return
     *     The code
     */
    public Integer getCode() {
        return code;
    }

    /**
     * 
     * @param code
     *     The code
     */
    public void setCode(Integer code) {
        this.code = code;
    }

    /**
     * 
     * @return
     *     The gatekeepers
     */
    public ArrayList<GatekeeperAdminModel> getGatekeepers() {
        return gatekeepers;
    }

    /**
     * 
     * @param gatekeepers
     *     The gatekeepers
     */
    public void setGatekeepers(ArrayList<GatekeeperAdminModel> gatekeepers) {
        this.gatekeepers = gatekeepers;
    }

}
