package com.android.lockated.crmadmin.adapter;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.android.lockated.R;
import com.android.lockated.crmadmin.activity.AdminEventDetailActivity;
import com.android.lockated.crmadmin.activity.AdminEventsActivity;
import com.android.lockated.information.AccountController;
import com.android.lockated.model.AccountData;
import com.android.lockated.model.AdminModel.AdminEvents.PublshedEvent.Event;
import com.android.lockated.model.AdminModel.AdminEvents.PublshedEvent.Event_Published;
import com.android.lockated.network.ConnectionDetector;
import com.android.lockated.preferences.LockatedPreferences;
import com.android.lockated.request.LockatedRequestError;
import com.android.lockated.request.LockatedVolleyRequestQueue;
import com.android.lockated.utils.ApplicationURL;
import com.android.lockated.utils.Utilities;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class PublishedEventAdapter extends RecyclerView.Adapter<PublishedEventAdapter.PublishedEventViewHolder>
        implements Response.Listener<JSONObject>, Response.ErrorListener {
    private final ArrayList<Event> event_published;
    Context contextMain;
    FragmentManager childFragmentManager;
    AccountController accountController;
    ArrayList<AccountData> datas;
    ProgressDialog mProgressDialog;
    LockatedPreferences mLockatedPreferences;

    public PublishedEventAdapter(Context context, ArrayList<Event> event_published, FragmentManager childFragmentManager) {
        this.contextMain = context;
        this.event_published = event_published;
        this.childFragmentManager = childFragmentManager;
        accountController = AccountController.getInstance();
        datas = accountController.getmAccountDataList();
        mLockatedPreferences = new LockatedPreferences(context);
    }

    @Override
    public PublishedEventViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(contextMain).inflate(R.layout.event_parent_view, parent, false);
        PublishedEventViewHolder pvh = new PublishedEventViewHolder(v);
        return pvh;
    }

    @Override
    public void onBindViewHolder(PublishedEventViewHolder holder, int position) {
        String dateSep = "(" + event_published.get(position).getDate() + ")";
        holder.dateSeperator.setText(dateSep);
        PublishedEventGridAdapter eventGridAdapter = new PublishedEventGridAdapter(contextMain, event_published.get(position).getEvents());
        holder.recycler.setAdapter(eventGridAdapter);
    }

    @Override
    public int getItemCount() {
        return event_published.size();
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }

    public void request(Context context, JSONObject jsonObject, String url, String id) {
        String REQUEST_TAG = "RejectEvent";
        mProgressDialog = ProgressDialog.show(context, "", "Please Wait...", false);
        mProgressDialog.show();
        if (context != null) {
            if (ConnectionDetector.isConnectedToInternet(context)) {
                LockatedVolleyRequestQueue lockatedVolleyRequestQueue = LockatedVolleyRequestQueue.getInstance(context);
                lockatedVolleyRequestQueue.sendRequest(REQUEST_TAG, Request.Method.GET, url, jsonObject, this, this);
                /*RequestQueue mQueue = LockatedVolleyRequestQueue.getInstance(context).getRequestQueue();
                LockatedJSONObjectRequest lockatedJSONObjectRequest = new LockatedJSONObjectRequest(Request.Method.GET,
                        url, jsonObject, this, this);
                lockatedJSONObjectRequest.setTag(REQUEST_TAG);
                mQueue.add(lockatedJSONObjectRequest);*/
            } else {
                Utilities.showToastMessage(context, context.getResources().getString(R.string.internet_connection_error));
            }
        }
    }

//======================================================================================================

    @Override
    public void onErrorResponse(VolleyError error) {
        mProgressDialog.dismiss();
        LockatedRequestError.onRequestError(contextMain, error);
    }

    @Override
    public void onResponse(JSONObject response) {
        mProgressDialog.dismiss();
        try {
            if (response != null && response.length() > 0) {
                if (response.has("id")) {
                    Intent intent = new Intent(contextMain, AdminEventsActivity.class);
                    intent.putExtra("AdminEvents", 1);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    contextMain.startActivity(intent);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public class PublishedEventViewHolder extends RecyclerView.ViewHolder {
        RecyclerView recycler;
        TextView dateSeperator;

        public PublishedEventViewHolder(View itemView) {
            super(itemView);
            dateSeperator = (TextView) itemView.findViewById(R.id.textItem);
            recycler = (RecyclerView) itemView.findViewById(R.id.recyclerViewFirstAdapter);
            LinearLayoutManager linearLayoutManager = new LinearLayoutManager(contextMain,
                    LinearLayoutManager.HORIZONTAL, false);
            recycler.setLayoutManager(linearLayoutManager);
        }
    }

    public class PublishedEventGridAdapter extends RecyclerView.Adapter<PublishedEventGridAdapter.EventGridViewHolder>
            implements View.OnClickListener {

        private final ArrayList<Event_Published> event_published;
        Context contextSub;
        JSONArray jsonArraySub;

        public PublishedEventGridAdapter(Context context, ArrayList<Event_Published> event_published) {
            this.contextSub = context;
            this.event_published = event_published;
        }

        @Override
        public EventGridViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View v = LayoutInflater.from(contextSub).inflate(R.layout.admin_published_event_child_view, parent, false);
            EventGridViewHolder EventGridViewHolder = new EventGridViewHolder(v);
            return EventGridViewHolder;
        }

        @Override
        public void onBindViewHolder(EventGridViewHolder holder, int position) {
            try {
                if (event_published.size() > 0 && event_published.get(position) != null) {

                    String fullDate = Utilities.dateTime_AMPM_Format(event_published.get(position).getFromTime());
                    holder.timeTxt1.setText(fullDate);
                    if (event_published.get(position).getEventName() != null) {
                        holder.subNameTxt1.setText(event_published.get(position).getEventName());
                    } else {
                        holder.subNameTxt1.setText("No Subject");
                    }
                    if (event_published.get(position).getUsername() != null) {
                        holder.postedNameTxt1.setText(event_published.get(position).getUsername());
                    } else {
                        holder.postedNameTxt1.setText("No Post Available");
                    }
                    holder.mDetailsView.setTag(position);
                    holder.reject.setTag(position);
                    holder.mDetailsView.setOnClickListener(this);
                    holder.reject.setOnClickListener(this);

                } else {
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        @Override
        public int getItemCount() {
            return event_published.size();
            //return jsonArraySub.length();
        }

        @Override
        public void onAttachedToRecyclerView(RecyclerView recyclerView) {
            super.onAttachedToRecyclerView(recyclerView);
        }

        @Override
        public void onClick(View v) {
            int position;
            switch (v.getId()) {
                case R.id.mDetailsView:
                    position = (int) v.getTag();
                    viewCardDetail(contextSub, event_published.get(position));
                    break;

                case R.id.rejectView:
                    position = (int) v.getTag();
                    String url = ApplicationURL.denyEvent + mLockatedPreferences.getLockatedToken()
                            + "&id=" + "" + event_published.get(position).getId();
                    request(contextSub, null, url, "" + event_published.get(position).getId());
                    break;
            }
        }

        public void viewCardDetail(Context context, Event_Published eventPublished) {
            JSONObject jsonObjectMain = new JSONObject();
            String username, updatedDate;
            try {
                //  jsonObjectMain.put("event_type", eventPublished.getEventType());
                jsonObjectMain.put("username", eventPublished.getUsername());
                jsonObjectMain.put("flat", eventPublished.getFlat());
                jsonObjectMain.put("event_name", eventPublished.getEventName());
                if (eventPublished.getDescription() != null) {
                    jsonObjectMain.put("description", eventPublished.getDescription());
                } else {
                    jsonObjectMain.put("description", "No Description");
                }
                jsonObjectMain.put("event_at", eventPublished.getEventAt());
                jsonObjectMain.put("from_time", eventPublished.getFromTime());
                jsonObjectMain.put("to_time", eventPublished.getToTime());
                jsonObjectMain.put("created_at", eventPublished.getCreatedAt());
                jsonObjectMain.put("id", "" + eventPublished.getId());
                jsonObjectMain.put("rsvp", "null");
                if (eventPublished.getUpdatedAt() != null) {
                    updatedDate = eventPublished.getUpdatedAt();
                } else {
                    updatedDate = "No Date";
                }
                jsonObjectMain.put("updated_at", updatedDate);
                if (eventPublished.getDocumentsEvent().size() > 0 && eventPublished.getDocumentsEvent() != null) {
                    jsonObjectMain.put("document", eventPublished.getDocumentsEvent().get(0).getDocument());

                } else {
                    jsonObjectMain.put("document", "No Document");

                }
            } catch (JSONException e) {
                e.printStackTrace();
            }

            Intent allNoticeDetailIntent = new Intent(context, AdminEventDetailActivity.class);
            allNoticeDetailIntent.putExtra("EventDetailViewData", jsonObjectMain.toString());
            allNoticeDetailIntent.putExtra("pendin_publish_value", "2");
            context.startActivity(allNoticeDetailIntent);
        }

        public class EventGridViewHolder extends RecyclerView.ViewHolder {
            TextView timeTxt1, subNameTxt1, postedNameTxt1, mDetailsView, reject;
            CardView cardView;

            public EventGridViewHolder(View itemView) {
                super(itemView);
                cardView = (CardView) itemView.findViewById(R.id.cardView);
                timeTxt1 = (TextView) itemView.findViewById(R.id.time1);
                subNameTxt1 = (TextView) itemView.findViewById(R.id.subjectName1);
                postedNameTxt1 = (TextView) itemView.findViewById(R.id.postedName1);
                mDetailsView = (TextView) itemView.findViewById(R.id.mDetailsView);
                reject = (TextView) itemView.findViewById(R.id.rejectView);
            }

        }
    }
}
