package com.android.lockated.crm.fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.android.lockated.R;
import com.android.lockated.crm.adapters.CrmBookingServicesListAdapter;

public class CrmBookingServicesFragment extends Fragment {

    ListView noticeBoardList;
    String[] nameList = {"Book Society Facilities", "Beauty", "Grocery", "Medicine", "Food", "Household Help",
            "Laundry", "Repair & Maintenance"};
    CrmBookingServicesListAdapter crmBookingServicesListAdapter;

    public CrmBookingServicesFragment() {}

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View billPaymentView = inflater.inflate(R.layout.fragment_booking_services, container, false);

        noticeBoardList = (ListView) billPaymentView.findViewById(R.id.billPaymentList);
        crmBookingServicesListAdapter = new CrmBookingServicesListAdapter(getActivity(), nameList);
        noticeBoardList.setAdapter(crmBookingServicesListAdapter);

        return billPaymentView;
    }

}