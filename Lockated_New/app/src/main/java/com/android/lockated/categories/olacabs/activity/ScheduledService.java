package com.android.lockated.categories.olacabs.activity;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.IBinder;

import java.util.Timer;

public class ScheduledService extends Service
{

    Context context;
    String booking_id;

    private Timer timer = new Timer();

    public ScheduledService(Context context, String booking_id)
    {
        this.context = context;
        this.booking_id = booking_id;
    }

    @Override
    public IBinder onBind(Intent intent)
    {
        return null;
    }

    @Override
    public void onCreate()
    {
        super.onCreate();
      /*  timer.scheduleAtFixedRate(new TimerTask() {
            @Override
            public void run() {
                private void sendBookResponseToServer(JSONObject jsonObject)
                {
                    Log.e(" Inside:",""+"sendBookResponseToServer");
                    if (context != null) {
                        if (ConnectionDetector.isConnectedToInternet(getActivity())) {
                            String url = "https://www.lockated.com/add_ola_orders.json?token="+"b06b9c80acb8e5b06cea63d4cc74a2297897a923cd2753cc";
                            Log.e("url of sendBookResponseToServer",""+url);
                            LockatedVolleyRequestQueue lockatedVolleyRequestQueue = LockatedVolleyRequestQueue.getInstance(getActivity());
                            lockatedVolleyRequestQueue.sendPostRequestOlaCabsConfirmEx(REQUEST_TAG, Request.Method.POST, url,AccessToken,jsonObject, this, this);
                        } else {
                            Utilities.showToastMessage(getActivity(), getActivity().getResources().getString(R.string.internet_connection_error));
                        }
                    }
                }
            }
        }, 0, 5*60*1000);//5 Minutes*/
    }

    @Override
    public void onDestroy()
    {
        super.onDestroy();
    }

}