package com.android.lockated.crm.fragment;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.widget.ListView;

import com.android.lockated.R;
import com.android.lockated.crm.adapters.CrmBillPaymentListAdapter;
import com.android.lockated.utils.Utilities;


public class CrmBillPaymentFragment extends Fragment implements View.OnClickListener {

    ListView noticeBoardList;
    FloatingActionButton fab, fab1, fab2;
    private Boolean isFabOpen = false;
    private Animation fab_open, fab_close, rotate_forward, rotate_backward;
    String[] nameList = {"Pending Dues and Bill Alerts", "Maintenance Charge", "Electricity Bill",
            "Water Bill", "Internet Bill", "Set Top Box Payment", "Landline Bill", "Miscellaneous"};
    CrmBillPaymentListAdapter crmBillPaymentListAdapter;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View billPaymentView = inflater.inflate(R.layout.fragment_bill_payment, container, false);

        Utilities.ladooIntegration(getActivity(), "Bill Payment");
        /*noticeBoardList = (ListView) billPaymentView.findViewById(R.id.billPaymentList);
        fab = (FloatingActionButton) billPaymentView.findViewById(R.id.fab);
        fab.setOnClickListener(this);
        fab1 = (FloatingActionButton) billPaymentView.findViewById(R.id.fab1);
        fab1.setOnClickListener(this);
        fab2 = (FloatingActionButton) billPaymentView.findViewById(R.id.fab2);
        fab2.setOnClickListener(this);
        fab_open = AnimationUtils.loadAnimation(getActivity(), R.anim.fab_open);
        fab_close = AnimationUtils.loadAnimation(getActivity(),R.anim.fab_close);
        rotate_forward = AnimationUtils.loadAnimation(getActivity(),R.anim.rotate_forward);
        rotate_backward = AnimationUtils.loadAnimation(getActivity(),R.anim.rotate_backward);
        crmBillPaymentListAdapter = new CrmBillPaymentListAdapter(getActivity(), nameList);
        noticeBoardList.setAdapter(crmBillPaymentListAdapter);*/

        return billPaymentView;
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {

            case R.id.fab:
                animateFAB();
                /*Snackbar.make(v, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();*/
                break;

            /*case R.id.fab1:
                Intent accStmt = new Intent(getActivity(), AccStatementActivity.class);
                startActivity(accStmt);
                getActivity().finish();
                break;*/
        }
    }

    public void animateFAB() {

        if (isFabOpen) {

            fab.startAnimation(rotate_backward);
            fab1.startAnimation(fab_close);
            fab2.startAnimation(fab_close);
            fab1.setClickable(false);
            fab2.setClickable(false);
            isFabOpen = false;

        } else {

            fab.startAnimation(rotate_forward);
            fab1.startAnimation(fab_open);
            fab2.startAnimation(fab_open);
            fab1.setClickable(true);
            fab2.setClickable(true);
            isFabOpen = true;

        }
    }

}