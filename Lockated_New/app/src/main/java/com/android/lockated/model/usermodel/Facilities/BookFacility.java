
package com.android.lockated.model.usermodel.Facilities;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class BookFacility {

    @SerializedName("code")
    @Expose
    private int code;
    @SerializedName("amenities")
    @Expose
    private ArrayList<Amenity> amenities = new ArrayList<Amenity>();

    /**
     * 
     * @return
     *     The code
     */
    public int getCode() {
        return code;
    }

    /**
     * 
     * @param code
     *     The code
     */
    public void setCode(int code) {
        this.code = code;
    }

    /**
     * 
     * @return
     *     The amenities
     */
    public ArrayList<Amenity> getAmenities() {
        return amenities;
    }

    /**
     * 
     * @param amenities
     *     The amenities
     */
    public void setAmenities(ArrayList<Amenity> amenities) {
        this.amenities = amenities;
    }

}
