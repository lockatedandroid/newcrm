package com.android.lockated.model.usermodel.EventModel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


public class DocumentsAllEvent {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("document_file_name")
    @Expose
    private String documentFileName;
    @SerializedName("document_content_type")
    @Expose
    private String documentContentType;
    @SerializedName("document_file_size")
    @Expose
    private Integer documentFileSize;
    @SerializedName("document_updated_at")
    @Expose
    private String documentUpdatedAt;
    @SerializedName("relation")
    @Expose
    private String relation;
    @SerializedName("relation_id")
    @Expose
    private Integer relationId;
    @SerializedName("active")
    @Expose
    private Integer active;
    @SerializedName("created_at")
    @Expose
    private String createdAt;
    @SerializedName("updated_at")
    @Expose
    private String updatedAt;
    @SerializedName("document")
    @Expose
    private String document;
    @SerializedName("doctype")
    @Expose
    private String doctype;

    /**
     * @return The id
     */
    public Integer getId() {
        return id;
    }

    /**
     * @param id The id
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * @return The documentFileName
     */
    public String getDocumentFileName() {
        return documentFileName;
    }

    /**
     * @param documentFileName The document_file_name
     */
    public void setDocumentFileName(String documentFileName) {
        this.documentFileName = documentFileName;
    }

    /**
     * @return The documentContentType
     */
    public String getDocumentContentType() {
        return documentContentType;
    }

    /**
     * @param documentContentType The document_content_type
     */
    public void setDocumentContentType(String documentContentType) {
        this.documentContentType = documentContentType;
    }

    /**
     * @return The documentFileSize
     */
    public Integer getDocumentFileSize() {
        return documentFileSize;
    }

    /**
     * @param documentFileSize The document_file_size
     */
    public void setDocumentFileSize(Integer documentFileSize) {
        this.documentFileSize = documentFileSize;
    }

    /**
     * @return The documentUpdatedAt
     */
    public String getDocumentUpdatedAt() {
        return documentUpdatedAt;
    }

    /**
     * @param documentUpdatedAt The document_updated_at
     */
    public void setDocumentUpdatedAt(String documentUpdatedAt) {
        this.documentUpdatedAt = documentUpdatedAt;
    }

    /**
     * @return The relation
     */
    public String getRelation() {
        return relation;
    }

    /**
     * @param relation The relation
     */
    public void setRelation(String relation) {
        this.relation = relation;
    }

    /**
     * @return The relationId
     */
    public Integer getRelationId() {
        return relationId;
    }

    /**
     * @param relationId The relation_id
     */
    public void setRelationId(Integer relationId) {
        this.relationId = relationId;
    }

    /**
     * @return The active
     */
    public Integer getActive() {
        return active;
    }

    /**
     * @param active The active
     */
    public void setActive(Integer active) {
        this.active = active;
    }

    /**
     * @return The createdAt
     */
    public String getCreatedAt() {
        return createdAt;
    }

    /**
     * @param createdAt The created_at
     */
    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    /**
     * @return The updatedAt
     */
    public String getUpdatedAt() {
        return updatedAt;
    }

    /**
     * @param updatedAt The updated_at
     */
    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    /**
     * @return The document
     */
    public String getDocument() {
        return document;
    }

    /**
     * @param document The document
     */
    public void setDocument(String document) {
        this.document = document;
    }

    /**
     * @return The doctype
     */
    public String getDoctype() {
        return doctype;
    }

    /**
     * @param doctype The doctype
     */
    public void setDoctype(String doctype) {
        this.doctype = doctype;
    }


    /*@SerializedName("id")
    @Expose
    private int id;
    @SerializedName("relation")
    @Expose
    private String relation;
    @SerializedName("relation_id")
    @Expose
    private int relationId;
    @SerializedName("document")
    @Expose
    private String document;
    @SerializedName("doctype")
    @Expose
    private String doctype;

    *//**
     * @return The id
     *//*
    public int getId() {
        return id;
    }

    *//**
     * @param id The id
     *//*
    public void setId(int id) {
        this.id = id;
    }

    *//**
     * @return The relation
     *//*
    public String getRelation() {
        return relation;
    }

    *//**
     * @param relation The relation
     *//*
    public void setRelation(String relation) {
        this.relation = relation;
    }

    *//**
     * @return The relationId
     *//*
    public int getRelationId() {
        return relationId;
    }

    *//**
     * @param relationId The relation_id
     *//*
    public void setRelationId(int relationId) {
        this.relationId = relationId;
    }

    *//**
     * @return The document
     *//*
    public String getDocument() {
        return document;
    }

    *//**
     * @param document The document
     *//*
    public void setDocument(String document) {
        this.document = document;
    }

    *//**
     * @return The doctype
     *//*
    public String getDoctype() {
        return doctype;
    }

    *//**
     * @param doctype The doctype
     *//*
    public void setDoctype(String doctype) {
        this.doctype = doctype;
    }*/

}