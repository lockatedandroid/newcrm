package com.android.lockated;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;

import com.android.lockated.account.fragment.SocietyDetailFragment;

public class OnBoardActivity extends AppCompatActivity {

    String className = "OnBoardActivity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_onboard);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        if (savedInstanceState == null) {
            SocietyDetailFragment societyDetailFragment = new SocietyDetailFragment();
            societyDetailFragment.setBottomBar(0, className);
            getSupportFragmentManager().beginTransaction().add(R.id.mOnBoardContainer, societyDetailFragment).commit();
        }
    }
}
