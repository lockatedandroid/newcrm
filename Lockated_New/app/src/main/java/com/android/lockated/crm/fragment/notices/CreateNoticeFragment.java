package com.android.lockated.crm.fragment.notices;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.android.lockated.Interfaces.IRecyclerItemClickListener;
import com.android.lockated.R;
import com.android.lockated.account.adapter.GroupListAdapter;
import com.android.lockated.information.AccountController;
import com.android.lockated.model.AccountData;
import com.android.lockated.model.userGroups.UserGroup;
import com.android.lockated.network.ConnectionDetector;
import com.android.lockated.preferences.LockatedPreferences;
import com.android.lockated.request.LockatedJSONObjectRequest;
import com.android.lockated.request.LockatedRequestError;
import com.android.lockated.request.LockatedVolleyRequestQueue;
import com.android.lockated.utils.ApplicationURL;
import com.android.lockated.utils.DatePickerFragment;
import com.android.lockated.utils.LockatedConfig;
import com.android.lockated.utils.MarshMallowPermission;
import com.android.lockated.utils.ShowImage;
import com.android.lockated.utils.Utilities;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

public class CreateNoticeFragment extends Fragment implements View.OnClickListener, Response.ErrorListener,
        Response.Listener<JSONObject>, TextWatcher, IRecyclerItemClickListener {

    FragmentManager fragmentManager;
    private EditText SpinerFunctionCategory;
    private EditText mNewCategoryDescription;
    private TextView mTextEndDate;
    private TextView mPreviewButton;
    private TextView mProceedButton;
    private TextView allMembers;
    private TextView groupMembers;
    private ImageView mImageEndDate;
    private ImageView mImageView;
    private ImageView mImageAttachment;
    int groupIdValue = 0;
    String societyId;
    private String selectedEndDate;
    private String StrSubject;
    private String mDescriptionString;
    /*private ProgressBar mProgressBar;*/
    ProgressDialog mProgressDialog;
    private LockatedPreferences mLockatedPreferences;
    MarshMallowPermission marshMallowPermission;
    private RequestQueue mQueue;
    AccountController accountController;
    ArrayList<AccountData> accountDataArrayList;
    public static final String REQUEST_TAG = "NoticePreview";
    public static final String GET_MY_GROUPS_TAG = "GetMyGroups";
    boolean imageSet = false;
    private String mCurrentPhotoPath, encodedImage;
    GroupListAdapter groupListAdapter;
    ArrayList<UserGroup> userGroupArrayList;

    public FragmentManager setFragmentManager(FragmentManager fragmentManager) {
        this.fragmentManager = fragmentManager;
        return fragmentManager;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View mFunctionCreateView = inflater.inflate(R.layout.fragment_create_notice, container, false);
        init(mFunctionCreateView);
        return mFunctionCreateView;
    }

    private void init(View mFunctionCreateView) {
        mLockatedPreferences = new LockatedPreferences(getActivity());
        marshMallowPermission = new MarshMallowPermission(getActivity());
        userGroupArrayList = new ArrayList<>();
//        mProgressBar = (ProgressBar) mFunctionCreateView.findViewById(R.id.mProgressBarView);
        mNewCategoryDescription = (EditText) mFunctionCreateView.findViewById(R.id.mNewCategoryDescription);
        mPreviewButton = (TextView) mFunctionCreateView.findViewById(R.id.mPreviewButton);
        mTextEndDate = (TextView) mFunctionCreateView.findViewById(R.id.mTextEndDate);
        mProceedButton = (TextView) mFunctionCreateView.findViewById(R.id.mProceedButton);
        allMembers = (TextView) mFunctionCreateView.findViewById(R.id.allMembers);
        groupMembers = (TextView) mFunctionCreateView.findViewById(R.id.groupMembers);
        mImageEndDate = (ImageView) mFunctionCreateView.findViewById(R.id.mImageEndDate);
        SpinerFunctionCategory = (EditText) mFunctionCreateView.findViewById(R.id.SpinerFunctionCategory);
        mImageAttachment = (ImageView) mFunctionCreateView.findViewById(R.id.mImageAttachment);
        mImageView = (ImageView) mFunctionCreateView.findViewById(R.id.mImageView);
        mImageView.setDrawingCacheEnabled(true);
        mImageView.buildDrawingCache();
        mProceedButton.setOnClickListener(this);
        allMembers.setOnClickListener(this);
        groupMembers.setOnClickListener(this);
        mImageEndDate.setOnClickListener(this);
        mPreviewButton.setOnClickListener(this);
        mImageView.setVisibility(View.GONE);
        mImageView.setOnClickListener(this);
        mImageAttachment.setOnClickListener(this);
        SpinerFunctionCategory.addTextChangedListener(this);
        mNewCategoryDescription.addTextChangedListener(this);
        if (mLockatedPreferences.getGroupList() != null && !mLockatedPreferences.getGroupList().equals("NoData")) {
            try {
                JSONObject jsonObject = new JSONObject(mLockatedPreferences.getGroupList());
                groupListAdapter = new GroupListAdapter(getActivity(), jsonObject.getJSONArray("usergroups"));
            } catch (JSONException e) {
                e.printStackTrace();
            }
        } else {
            callGroupApi();
        }
        disableSubmitButton();
    }

    private void callGroupApi() {
        if (getActivity() != null) {
            mProgressDialog = ProgressDialog.show(getActivity(), "", "Please Wait...", false);
            mProgressDialog.show();
            if (ConnectionDetector.isConnectedToInternet(getActivity())) {
                String url = ApplicationURL.getMyGroups + mLockatedPreferences.getLockatedToken();
                LockatedVolleyRequestQueue lockatedVolleyRequestQueue = LockatedVolleyRequestQueue.getInstance(getActivity());
                lockatedVolleyRequestQueue.sendRequest(GET_MY_GROUPS_TAG, Request.Method.GET,
                        url, null, this, this);
            } else {
                Utilities.showToastMessage(getActivity(), getActivity().getResources()
                        .getString(R.string.internet_connection_error));
            }
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.mImageEndDate:
                openDatePicker(mTextEndDate);
                break;
            case R.id.mProceedButton:
                //whomeToSendAlertDialog(getActivity(), "Whom to send");
                OnBProceedClicked(groupIdValue);
                break;
            case R.id.mPreviewButton:
                onPreviewClicked();
                break;
            case R.id.mImageView:
                Intent displayImage = new Intent(getActivity(), ShowImage.class);
                displayImage.putExtra("imagePathString", mCurrentPhotoPath);
                getActivity().startActivity(displayImage);
                break;
            case R.id.mImageAttachment:
                if (!imageSet) {
                    selectImage();
                } else {
                    selectImageAction();
                }
                break;
            case R.id.allMembers:
                changeViewColor(allMembers, groupMembers);
                groupIdValue = 0;
                break;
            case R.id.groupMembers:
                changeViewColor(groupMembers, allMembers);
                groupMemberListAlertDialog(getActivity(), "Whome to send");
                break;
        }
    }

    private void changeViewColor(TextView textView1, TextView textView2) {
        textView1.setTextColor(this.getResources().getColor(R.color.white));
        textView1.setBackgroundColor(this.getResources().getColor(R.color.primary));
        textView2.setTextColor(this.getResources().getColor(R.color.primary));
        textView2.setBackgroundDrawable(this.getResources().getDrawable(R.drawable.border_card_layout));
    }

    private void selectImage() {
        final CharSequence[] options = {"Camera", "Gallery"};
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle("Add Attachment!");
        builder.setItems(options, new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int item) {
                if (options[item].equals("Camera")) {
                    checkCameraPermission();
                } else if (options[item].equals("Gallery")) {
                    checkStoragePermission();
                }
            }

        });

        builder.show();

    }

    private void selectImageAction() {
        final CharSequence[] options = {"View Image", "Change Image", "Remove Image"};
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setItems(options, new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int item) {
                if (options[item].equals("View Image")) {
                    Intent displayImage = new Intent(getActivity(), ShowImage.class);
                    displayImage.putExtra("imagePathString", mCurrentPhotoPath);
                    getActivity().startActivity(displayImage);
                } else if (options[item].equals("Change Image")) {
                    selectImage();
                } else {
                    imageViewGone();
                    mImageView.setImageBitmap(null);
                    encodedImage = null;
                    imageSet = false;
                }
            }

        });

        builder.show();

    }

    private void checkCameraPermission() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
                requestPermissions(new String[]{Manifest.permission.CAMERA}, LockatedConfig.REQUEST_CAMERA);
            } else {
                onCameraClicked();
            }
        } else {
            onCameraClicked();
        }
    }

    private void checkStoragePermission() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.WRITE_EXTERNAL_STORAGE)
                    != PackageManager.PERMISSION_GRANTED) {
                requestPermissions(new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, LockatedConfig.REQUEST_STORAGE);
            } else {
                onGalleryClicked();
            }
        } else {
            onGalleryClicked();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        if (requestCode == LockatedConfig.REQUEST_CAMERA) {
            if (grantResults.length == 1 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                onCameraClicked();
            }
        } else if (requestCode == LockatedConfig.REQUEST_STORAGE) {
            if (grantResults.length == 1 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                onGalleryClicked();
            }
        }
    }

    private void onGalleryClicked() {
        Intent i = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(i, LockatedConfig.CHOOSE_IMAGE_REQUEST);
        imageViewVisible();
    }

    private File createImageFile() throws IOException {
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss", Locale.ENGLISH).format(new Date());
        String imageFileName = "JPEG_" + timeStamp + "_";
        File storageDir = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES);
        File image = File.createTempFile(imageFileName, ".jpg", storageDir);
        mCurrentPhotoPath = image.getAbsolutePath();
        return image;
    }

    private void onCameraClicked() {
        imageViewVisible();
        if (!marshMallowPermission.checkPermissionForCamera()) {
            marshMallowPermission.requestPermissionForCamera();
        } else {
            if (!marshMallowPermission.checkPermissionForExternalStorage()) {
                marshMallowPermission.requestPermissionForExternalStorage();
            } else {
                Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                if (takePictureIntent.resolveActivity(getActivity().getPackageManager()) != null) {
                    File photoFile = null;
                    try {
                        photoFile = createImageFile();
                    } catch (IOException ex) {
                        ex.printStackTrace();
                    }

                    if (photoFile != null) {
                        takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(photoFile));
                        startActivityForResult(takePictureIntent, LockatedConfig.REQUEST_TAKE_PHOTO);
                    }
                }
            }
        }
        /*imageViewVisible();*/

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode != getActivity().RESULT_CANCELED) {
            if (requestCode == LockatedConfig.REQUEST_TAKE_PHOTO && resultCode == getActivity().RESULT_OK) {
                setPic();
            } else if (requestCode == LockatedConfig.CHOOSE_IMAGE_REQUEST && resultCode == getActivity().RESULT_OK) {
                Uri selectedImageURI = data.getData();
                mCurrentPhotoPath = getPath(selectedImageURI);
                setPic();
            } else if (requestCode == LockatedConfig.REQUEST_CAMERA_PHOTO && resultCode == getActivity().RESULT_OK) {
                Bitmap thumbnail = (Bitmap) data.getExtras().get("data");
                if (thumbnail != null) {
                    ByteArrayOutputStream bytes = new ByteArrayOutputStream();
                    File destination = new File(Environment.getExternalStorageDirectory(), System.currentTimeMillis() + ".jpg");
                    FileOutputStream fo;
                    try {
                        destination.createNewFile();
                        fo = new FileOutputStream(destination);
                        fo.write(bytes.toByteArray());
                        fo.close();
                    } catch (FileNotFoundException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    imageViewVisible();
                    setCameraPic(thumbnail);
                }
            } else {
                imageViewGone();
            }
        }
    }

    private void setCameraPic(Bitmap bitmap) {
        if (bitmap != null) {
            encodedImage = Utilities.encodeTobase64(bitmap);
            mImageView.setImageBitmap(bitmap);
            imageSet = true;
        }
    }

    private void setPic() {
        int targetW = mImageView.getWidth();
        int targetH = mImageView.getHeight();
        BitmapFactory.Options bmOptions = new BitmapFactory.Options();
        bmOptions.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(mCurrentPhotoPath, bmOptions);
        int photoW = bmOptions.outWidth;
        int photoH = bmOptions.outHeight;
        int scaleFactor = Math.min(photoW / targetW, photoH / targetH);

        bmOptions.inJustDecodeBounds = false;
        bmOptions.inSampleSize = scaleFactor;
        bmOptions.inPurgeable = true;

        Bitmap bitmap = BitmapFactory.decodeFile(mCurrentPhotoPath, bmOptions);
        if (bitmap != null) {
            encodedImage = Utilities.encodeTobase64(bitmap);
            mImageView.setImageBitmap(bitmap);
            imageSet = true;
        }
    }

    private String getPath(Uri uri) {
        if (uri == null) {
            return null;
        }
        String[] projection = {MediaStore.Images.Media.DATA};
        Cursor cursor = getActivity().getContentResolver().query(uri, projection, null, null, null);
        if (cursor != null) {
            int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
            cursor.moveToFirst();
            return cursor.getString(column_index);
        }
        return uri.getPath();
    }

    private void openDatePicker(TextView selectTime) {
        DatePickerFragment datePickerFragment = new DatePickerFragment();
        datePickerFragment.setDatePickerView(selectTime);
        DialogFragment newFragment = datePickerFragment;
        newFragment.show(getActivity().getSupportFragmentManager(), "DatePicker");
    }

    private void onPreviewClicked() {

        mDescriptionString = mNewCategoryDescription.getText().toString();
        selectedEndDate = mTextEndDate.getText().toString();
        StrSubject = SpinerFunctionCategory.getText().toString();

        if ((TextUtils.isEmpty(mDescriptionString)) && (TextUtils.isEmpty(StrSubject))) {
            Utilities.showToastMessage(getContext(), "Please Fill Description");
        } else if ((selectedEndDate.equals("End Date"))) {
            Utilities.showToastMessage(getContext(), "Select Expiry Date");
        } else {
            NoticePreview noticePreview = new NoticePreview();
            Bundle args = new Bundle();
            args.putString("subject", StrSubject);
            args.putString("description", mDescriptionString);
            args.putString("enddate", selectedEndDate);
            noticePreview.setArguments(args);
            noticePreview.show(fragmentManager, "PreviewDialog");
        }
    }

    private void OnBProceedClicked(int groupId) {
        mProgressDialog = ProgressDialog.show(getActivity(), "", "Please Wait...", false);
        mProgressDialog.show();
        mDescriptionString = mNewCategoryDescription.getText().toString();
        selectedEndDate = mTextEndDate.getText().toString();
        StrSubject = SpinerFunctionCategory.getText().toString();
        if (TextUtils.isEmpty(StrSubject)) {
            Utilities.showToastMessage(getContext(), "Please Fill Subject");
        } else if ((TextUtils.isEmpty(mDescriptionString))) {
            Utilities.showToastMessage(getContext(), "Please Fill Description");
        } else if ((selectedEndDate.equals("End Date"))) {
            Utilities.showToastMessage(getContext(), "Select Expiry Date");
        } else {
            Calendar c = Calendar.getInstance();
            SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSz", Locale.FRANCE);
            String formattedDate = df.format(c.getTime());
            /*accountController = AccountController.getInstance();
            accountDataArrayList = accountController.getmAccountDataList();
            societyId = accountDataArrayList.get(0).getmResidenceDataList().get(0).getSociety_id();*/
            societyId = mLockatedPreferences.getSocietyId();

            if (ConnectionDetector.isConnectedToInternet(getActivity())) {
                JSONObject jsonObjectMain = new JSONObject();
                JSONObject jsonObject = new JSONObject();
                try {
                    if (groupId == 0) {
                        if (encodedImage != null) {
                            jsonObject.put("id_society", societyId);
                            jsonObject.put("notice_heading", StrSubject);
                            jsonObject.put("notice_text", mDescriptionString);
                            jsonObject.put("expire_time", selectedEndDate);
                            jsonObject.put("created_at", formattedDate);
                            jsonObject.put("updated_at", formattedDate);
                            jsonObject.put("shared", "" + groupId);
                            jsonObject.put("group_id", "" + groupId);
                            jsonObject.put("user_society_id", "" + mLockatedPreferences.getUserSocietyId());
                            jsonObject.put("document", encodedImage);
                            jsonObjectMain.put("noticeboard", jsonObject);
                        } else {
                            jsonObject.put("id_society", societyId);
                            jsonObject.put("notice_heading", StrSubject);
                            jsonObject.put("notice_text", mDescriptionString);
                            jsonObject.put("expire_time", selectedEndDate);
                            jsonObject.put("created_at", formattedDate);
                            jsonObject.put("updated_at", formattedDate);
                            jsonObject.put("shared", "" + groupId);
                            jsonObject.put("group_id", "" + groupId);
                            jsonObject.put("user_society_id", "" + mLockatedPreferences.getUserSocietyId());
                            jsonObjectMain.put("noticeboard", jsonObject);
                        }
                    } else {
                        if (encodedImage != null) {
                            jsonObject.put("id_society", societyId);
                            jsonObject.put("notice_heading", StrSubject);
                            jsonObject.put("notice_text", mDescriptionString);
                            jsonObject.put("expire_time", selectedEndDate);
                            jsonObject.put("created_at", formattedDate);
                            jsonObject.put("updated_at", formattedDate);
                            jsonObject.put("shared", "1");
                            jsonObject.put("group_id", "" + groupId);
                            jsonObject.put("document", encodedImage);
                            jsonObject.put("user_society_id", "" + mLockatedPreferences.getUserSocietyId());
                            jsonObjectMain.put("noticeboard", jsonObject);
                        } else {
                            jsonObject.put("id_society", societyId);
                            jsonObject.put("notice_heading", StrSubject);
                            jsonObject.put("notice_text", mDescriptionString);
                            jsonObject.put("expire_time", selectedEndDate);
                            jsonObject.put("created_at", formattedDate);
                            jsonObject.put("updated_at", formattedDate);
                            jsonObject.put("shared", "1");
                            jsonObject.put("group_id", "" + groupId);
                            jsonObject.put("user_society_id", "" + mLockatedPreferences.getUserSocietyId());
                            jsonObjectMain.put("noticeboard", jsonObject);
                        }

                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                try {
                    String url = ApplicationURL.addNotice + mLockatedPreferences.getLockatedToken();

                    LockatedVolleyRequestQueue lockatedVolleyRequestQueue = LockatedVolleyRequestQueue.getInstance(getActivity());
                    lockatedVolleyRequestQueue.sendRequest(REQUEST_TAG, Request.Method.POST, url, jsonObjectMain, this, this);
                    /*mQueue = LockatedVolleyRequestQueue.getInstance(getActivity()).getRequestQueue();
                    LockatedJSONObjectRequest lockatedJSONObjectRequest = new LockatedJSONObjectRequest(Request.Method.POST,
                            url, jsonObjectMain, this, this);
                    Log.e("jsonObjectMain", "" + jsonObjectMain);
                    Utilities.writeToFile(jsonObjectMain.toString(), "CreateNotice");
                    lockatedJSONObjectRequest.setTag(REQUEST_TAG);
                    mQueue.add(lockatedJSONObjectRequest);*/
                } catch (Exception e) {
                    e.printStackTrace();
                }

            } else {
                Utilities.showToastMessage(getActivity(), getActivity().getResources().getString(R.string.internet_connection_error));
            }
        }
    }

    private void groupMemberListAlertDialog(Context context, String groupName) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle(groupName);
        builder.setAdapter(groupListAdapter, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                try {
                    JSONObject jsonObject = new JSONObject(mLockatedPreferences.getGroupList());
                    String groupId = jsonObject.getJSONArray("usergroups").getJSONObject(which).getString("id");
                    /*OnBProceedClicked(Integer.valueOf(groupId));*/
                    groupIdValue = Integer.valueOf(groupId);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
        AlertDialog dialog = builder.create();
        dialog.show();
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = 450;
        dialog.getWindow().setAttributes(lp);
        dialog.setCanceledOnTouchOutside(true);
        dialog.getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCH_MODAL,
                WindowManager.LayoutParams.FLAG_WATCH_OUTSIDE_TOUCH);
    }

    @Override
    public void onErrorResponse(VolleyError error) {
        /*mProgressBar.setVisibility(View.GONE);*/
        mProgressDialog.dismiss();
        if (getActivity() != null) {
            LockatedRequestError.onRequestError(getActivity(), error);
        }
    }

    @Override
    public void onResponse(JSONObject response) {
        /*mProgressBar.setVisibility(View.GONE);*/
        mProgressDialog.dismiss();
        if (getActivity() != null) {
            try {
                if (response.has("id")) {
                    getActivity().finish();
                } else if (response.has("usergroups") && response.getJSONArray("usergroups").length() > 0) {
                    mLockatedPreferences.setGroupList(response);
                    groupListAdapter = new GroupListAdapter(getActivity(), response.getJSONArray("usergroups"));
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {
        if (s.length() > 0) {
            if (SpinerFunctionCategory.getText().toString().length() > 0
                    && mNewCategoryDescription.getText().toString().length() > 0) {
                enableSubmitButton();
            }
        } else {
            disableSubmitButton();
        }
    }

    private void disableSubmitButton() {
        mPreviewButton.setEnabled(false);
        mPreviewButton.setTextColor(ContextCompat.getColor(getActivity(), R.color.primary_text));
        mProceedButton.setEnabled(false);
        mProceedButton.setTextColor(ContextCompat.getColor(getActivity(), R.color.primary_text));

    }

    private void enableSubmitButton() {
        mPreviewButton.setEnabled(true);
        mPreviewButton.setTextColor(ContextCompat.getColor(getActivity(), R.color.white));
        mProceedButton.setEnabled(true);
        mProceedButton.setTextColor(ContextCompat.getColor(getActivity(), R.color.white));
    }

    @Override
    public void afterTextChanged(Editable s) {
    }

    @Override
    public void onRecyclerItemClick(View view, int position) {

    }

    private void imageViewVisible() {
        if (mImageView.getVisibility() == View.GONE) {
            mImageView.setVisibility(View.VISIBLE);
        }
    }

    private void imageViewGone() {
        if (mImageView.getVisibility() == View.VISIBLE) {
            mImageView.setVisibility(View.GONE);
        }
    }

}
