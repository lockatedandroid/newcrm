package com.android.lockated.crm.adapters;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.android.lockated.R;
import com.android.lockated.crm.activity.AllNoticesActivity;
import com.android.lockated.crm.activity.NoticeDetailActivity;
import com.android.lockated.model.usermodel.NoticeModel.MyNotices.Noticeboard;
import com.android.lockated.preferences.LockatedPreferences;
import com.android.lockated.request.LockatedRequestError;
import com.android.lockated.utils.Utilities;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;


public class MyNoticeAdapter extends RecyclerView.Adapter<MyNoticeAdapter.MyNoticeAdapterViewHolder> implements Response.ErrorListener,
        Response.Listener<JSONObject> {

    private final ArrayList<Noticeboard> noticeboards;
    Context contextSub;
    FragmentManager fragmentManager;
    LockatedPreferences lockatedPreferences;
    RequestQueue mQueue;
    String REQUEST_TAG = "MyNoticeAdapter", statusTag = "No Status";

    public MyNoticeAdapter(Context context, ArrayList<Noticeboard> noticeboards, FragmentManager fragmentManager) {
        this.contextSub = context;
        this.noticeboards = noticeboards;
        this.fragmentManager = fragmentManager;
        lockatedPreferences = new LockatedPreferences(context);
    }

    @Override
    public MyNoticeAdapterViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(contextSub).inflate(R.layout.my_notice_adapter_view, parent, false);
        MyNoticeAdapterViewHolder noticeGridViewHolder = new MyNoticeAdapterViewHolder(v);
        return noticeGridViewHolder;
    }

    @Override
    public void onBindViewHolder(MyNoticeAdapterViewHolder holder, final int position) {

        try {
            if (!noticeboards.get(position).getNoticeHeading().equals("null")) {
                holder.subject.setText(noticeboards.get(position).getNoticeHeading());
            } else {
                holder.subject.setText("No Subject");
            }
            if (!noticeboards.get(position).getNoticeText().equals("null")) {
                holder.description.setText(noticeboards.get(position).getNoticeText());
            } else {
                holder.description.setText("No Description Available");
            }
            if (!noticeboards.get(position).getUpdatedAt().equals("null")) {
                holder.createdOn.setText(Utilities.dateConvertMinify(noticeboards.get(position).getUpdatedAt()));
            } else {
                holder.createdOn.setText("No Date Available");
            }
            if (!noticeboards.get(position).getStatus().equals("null")) {
                statusTag = noticeboards.get(position).getStatus();
                if (statusTag.equalsIgnoreCase("rejected")) {
                    holder.status.setText(statusTag);
                    holder.status.setTextColor(Color.parseColor("#FF0000"));
                } else if (statusTag.equalsIgnoreCase("approved")) {
                    holder.status.setText(statusTag);
                    holder.status.setTextColor(Color.parseColor("#0000FF"));
                } else {
                    holder.status.setText(statusTag);
                }
            } else {
                holder.status.setText("No Status Available");
            }
            holder.detail.setTag(position);
            holder.detail.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int pos = (int) v.getTag();
                    viewCardDetail(contextSub, noticeboards.get(pos));
                }
            });
            if (statusTag.equalsIgnoreCase("rejected")) {
                holder.editText.setEnabled(false);
                holder.editText.setClickable(false);
            } else {
                holder.editText.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        OnRepublishClicked(noticeboards.get(position));
                    }
                });
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public int getItemCount() {
        return noticeboards.size();
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }

    public class MyNoticeAdapterViewHolder extends RecyclerView.ViewHolder {

        TextView subject, description, createdOn, status, editText, detail;

        public MyNoticeAdapterViewHolder(View itemView) {
            super(itemView);

            subject = (TextView) itemView.findViewById(R.id.subjectValue);
            description = (TextView) itemView.findViewById(R.id.descriptionValue);
            createdOn = (TextView) itemView.findViewById(R.id.createdOnValue);
            status = (TextView) itemView.findViewById(R.id.statusValue);
            detail = (TextView) itemView.findViewById(R.id.detailShow);
            editText = (TextView) itemView.findViewById(R.id.edit);
        }
    }

    public void viewCardDetail(Context context, Noticeboard noticeboard) {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.putOpt("notice_heading", noticeboard.getNoticeHeading());
            jsonObject.putOpt("description", noticeboard.getNoticeText());
            jsonObject.putOpt("created_at", noticeboard.getCreatedAt());
            String date;
            if (noticeboard.getExpireTime() != null && !noticeboard.getExpireTime().equals("null")) {
                date = Utilities.dateConvertMinify(noticeboard.getExpireTime());
            } else {
                date = "No Date";
            }

            jsonObject.putOpt("expire_time", date);
            jsonObject.putOpt("username", noticeboard.getUser().getFirstname()
                    + " " + noticeboard.getUser().getLastname());
            if (noticeboard.getDocuments() != null&&noticeboard.getDocuments().size()>0) {
                jsonObject.putOpt("document", noticeboard.getDocuments().get(0).getDocument());
            } else {
                jsonObject.putOpt("document", "No Document");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        Intent allNoticeDetailIntent = new Intent(context, NoticeDetailActivity.class);
        allNoticeDetailIntent.putExtra("NoticeDetailViewData", jsonObject.toString());
        context.startActivity(allNoticeDetailIntent);
    }

    private void OnRepublishClicked(Noticeboard noticeboardvalue) {

        JSONObject jsonObjectMain = new JSONObject();
        JSONObject jsonObject = new JSONObject();

        try {
            jsonObject.put("id_society", "" + noticeboardvalue.getId());
            jsonObject.put("notice_heading", noticeboardvalue.getNoticeHeading());
            jsonObject.put("notice_text", noticeboardvalue.getNoticeText());
            jsonObject.put("expire_time", noticeboardvalue.getExpireTime());
            jsonObject.put("created_at", noticeboardvalue.getCreatedAt());
            jsonObject.put("updated_at", noticeboardvalue.getUpdatedAt());
            jsonObjectMain.put("noticeboard", jsonObject);
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    @Override
    public void onErrorResponse(VolleyError error) {
        if (contextSub != null) {
            LockatedRequestError.onRequestError(contextSub, error);
        }
    }

    @Override
    public void onResponse(JSONObject response) {
        if (response.has("id")) {
            Intent intent = new Intent(contextSub, AllNoticesActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            intent.putExtra("AllNoticesActivity", 1);
            contextSub.startActivity(intent);
        } else {
            Utilities.showToastMessage(contextSub, "Something went wrong");
        }

    }

}