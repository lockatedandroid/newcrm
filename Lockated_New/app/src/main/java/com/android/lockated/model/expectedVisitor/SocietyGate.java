
package com.android.lockated.model.expectedVisitor;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class SocietyGate {

    @SerializedName("id")
    @Expose
    private int id;
    @SerializedName("society_id")
    @Expose
    private int societyId;
    @SerializedName("gate_name")
    @Expose
    private String gateName;
    @SerializedName("gate_device")
    @Expose
    private String gateDevice;
    @SerializedName("approve")
    @Expose
    private int approve;
    @SerializedName("approved_by")
    @Expose
    private Object approvedBy;
    @SerializedName("active")
    @Expose
    private Object active;
    @SerializedName("created_at")
    @Expose
    private String createdAt;
    @SerializedName("updated_at")
    @Expose
    private String updatedAt;

    /**
     * 
     * @return
     *     The id
     */
    public int getId() {
        return id;
    }

    /**
     * 
     * @param id
     *     The id
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * 
     * @return
     *     The societyId
     */
    public int getSocietyId() {
        return societyId;
    }

    /**
     * 
     * @param societyId
     *     The society_id
     */
    public void setSocietyId(int societyId) {
        this.societyId = societyId;
    }

    /**
     * 
     * @return
     *     The gateName
     */
    public String getGateName() {
        return gateName;
    }

    /**
     * 
     * @param gateName
     *     The gate_name
     */
    public void setGateName(String gateName) {
        this.gateName = gateName;
    }

    /**
     * 
     * @return
     *     The gateDevice
     */
    public String getGateDevice() {
        return gateDevice;
    }

    /**
     * 
     * @param gateDevice
     *     The gate_device
     */
    public void setGateDevice(String gateDevice) {
        this.gateDevice = gateDevice;
    }

    /**
     * 
     * @return
     *     The approve
     */
    public int getApprove() {
        return approve;
    }

    /**
     * 
     * @param approve
     *     The approve
     */
    public void setApprove(int approve) {
        this.approve = approve;
    }

    /**
     * 
     * @return
     *     The approvedBy
     */
    public Object getApprovedBy() {
        return approvedBy;
    }

    /**
     * 
     * @param approvedBy
     *     The approved_by
     */
    public void setApprovedBy(Object approvedBy) {
        this.approvedBy = approvedBy;
    }

    /**
     * 
     * @return
     *     The active
     */
    public Object getActive() {
        return active;
    }

    /**
     * 
     * @param active
     *     The active
     */
    public void setActive(Object active) {
        this.active = active;
    }

    /**
     * 
     * @return
     *     The createdAt
     */
    public String getCreatedAt() {
        return createdAt;
    }

    /**
     * 
     * @param createdAt
     *     The created_at
     */
    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    /**
     * 
     * @return
     *     The updatedAt
     */
    public String getUpdatedAt() {
        return updatedAt;
    }

    /**
     * 
     * @param updatedAt
     *     The updated_at
     */
    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

}
