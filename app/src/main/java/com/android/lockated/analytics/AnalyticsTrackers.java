package com.android.lockated.analytics;

import android.content.Context;

import com.android.lockated.R;
import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.Tracker;

import java.util.HashMap;
import java.util.Map;

public class AnalyticsTrackers {

    //GACampaign
    private static final String PROPERTY_ID = "UA-71907520-1";

    public enum Target {
        APP,
        APP_TRACKER, // Tracker used only in this app.
        GLOBAL_TRACKER, // Tracker used by all the apps from a company. eg: roll-up tracking.
        ECOMMERCE_TRACKER, // Tracker used by all ecommerce transactions from a company.
    }

    private static AnalyticsTrackers sInstance;

    public static synchronized void initialize(Context context) {
        if (sInstance != null) {
            throw new IllegalStateException("Extra call to initialize analytics trackers");
        }

        sInstance = new AnalyticsTrackers(context);
    }

    public static synchronized AnalyticsTrackers getInstance() {
        if (sInstance == null) {
            throw new IllegalStateException("Call initialize() before getInstance()");
        }

        return sInstance;
    }

    private final Map<Target, Tracker> mTrackers = new HashMap<Target, Tracker>();
    private final Context mContext;

    private AnalyticsTrackers(Context context) {
        mContext = context.getApplicationContext();
    }

    public synchronized Tracker get(Target target) {
        if (!mTrackers.containsKey(target)) {
            Tracker tracker;
            switch (target) {
                case APP:
                    tracker = GoogleAnalytics.getInstance(mContext).newTracker(R.xml.app_tracker);
                    break;
                case APP_TRACKER:
                    tracker = GoogleAnalytics.getInstance(mContext).newTracker(PROPERTY_ID);
                    break;
                case GLOBAL_TRACKER:
                    tracker = GoogleAnalytics.getInstance(mContext).newTracker(R.xml.global_tracker);
                    break;
                case ECOMMERCE_TRACKER:
                    tracker = GoogleAnalytics.getInstance(mContext).newTracker(/*R.xml.ecommerce_tracker*/PROPERTY_ID);
                    break;
                default:
                    throw new IllegalArgumentException("Unhandled analytics target " + target);
            }
            mTrackers.put(target, tracker);
        }

        return mTrackers.get(target);
    }

    /*synchronized Tracker getTracker(Target target) {
        if (!mTrackers.containsKey(target)) {

            GoogleAnalytics analytics = GoogleAnalytics.getInstance(mContext);
            Tracker t = (target == Target.APP_TRACKER) ? analytics.newTracker(PROPERTY_ID)
                    : (target == Target.GLOBAL_TRACKER) ? analytics.newTracker(R.xml.global_tracker)
                    : analytics.newTracker(R.xml.ecommerce_tracker);
            mTrackers.put(target, t);

        }
        return mTrackers.get(target);
    }*/

}
