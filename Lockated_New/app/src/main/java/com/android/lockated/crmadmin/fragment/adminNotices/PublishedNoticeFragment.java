package com.android.lockated.crmadmin.fragment.adminNotices;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.android.lockated.LockatedApplication;
import com.android.lockated.R;
import com.android.lockated.crmadmin.adapter.PublishedNoticeAdapter;
import com.android.lockated.model.AdminModel.Notice.PublishNotice.PublishNoticeboard;
import com.android.lockated.network.ConnectionDetector;
import com.android.lockated.preferences.LockatedPreferences;
import com.android.lockated.request.LockatedJSONObjectRequest;
import com.android.lockated.request.LockatedRequestError;
import com.android.lockated.request.LockatedVolleyRequestQueue;
import com.android.lockated.utils.ApplicationURL;
import com.android.lockated.utils.Utilities;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class PublishedNoticeFragment extends Fragment implements Response.ErrorListener, Response.Listener<JSONObject> {
    RecyclerView noticeList;
    TextView errorMsg;
    PublishedNoticeAdapter publishedNoticeAdapter;
    private RequestQueue mQueue;
    ArrayList<PublishNoticeboard> publishNoticeboardArrayList;
    private LockatedPreferences mLockatedPreferences;
    public static final String REQUEST_TAG = "PublishedNoticeFragment";
    ProgressBar progressBar;
    String SHOW, CREATE, INDEX, UPDATE, EDIT;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View publishedNotice = inflater.inflate(R.layout.fragment_published_notice, container, false);
        init(publishedNotice);
        Utilities.ladooIntegration(getActivity(), getActivity().getResources().getString(R.string.publishedNotices));
        return publishedNotice;
    }

    @Override
    public void onResume() {
        super.onResume();
        LockatedApplication.getInstance().trackScreenView(getString(R.string.publishedNotices));
        Utilities.lockatedGoogleAnalytics(getString(R.string.publishedNotices),
                getString(R.string.visited), getString(R.string.publishedNotices));
       // publishNoticeboardArrayList.clear();
      //  getPublishedNotices();
    }

    private void init(View view) {
        publishNoticeboardArrayList = new ArrayList<>();
        mLockatedPreferences = new LockatedPreferences(getActivity());
        noticeList = (RecyclerView) view.findViewById(R.id.noticeList);
        final LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        noticeList.setLayoutManager(linearLayoutManager);
        errorMsg = (TextView) view.findViewById(R.id.noNotices);
        progressBar = (ProgressBar) view.findViewById(R.id.mProgressBarView);
        getNoticeRole();
        checkPermission();
        publishedNoticeAdapter = new PublishedNoticeAdapter(getActivity(), publishNoticeboardArrayList);
        noticeList.setAdapter(publishedNoticeAdapter);
    }

    private void checkPermission() {
        if (SHOW!=null&&SHOW.equals("true")) {
            getPublishedNotices();
        } else {
            noticeList.setVisibility(View.GONE);
            errorMsg.setVisibility(View.VISIBLE);
            errorMsg.setText(R.string.no_permission_error);
        }
    }

    private void getPublishedNotices() {
        if (getActivity() != null) {
            if (!mLockatedPreferences.getSocietyId().equals("blank")) {
                if (ConnectionDetector.isConnectedToInternet(getActivity())) {
                    progressBar.setVisibility(View.VISIBLE);
                    String url = ApplicationURL.getNoticesUrl + mLockatedPreferences.getLockatedToken()
                            + "&id_society=" + mLockatedPreferences.getSocietyId();
                    mQueue = LockatedVolleyRequestQueue.getInstance(getActivity()).getRequestQueue();
                    LockatedJSONObjectRequest lockatedJSONObjectRequest = new LockatedJSONObjectRequest(Request.Method.GET,
                            url, null, this, this);
                    lockatedJSONObjectRequest.setTag(REQUEST_TAG);
                    mQueue.add(lockatedJSONObjectRequest);
                } else {
                    Utilities.showToastMessage(getActivity(), getActivity().getResources().getString(R.string.internet_connection_error));
                }
            } else {
                errorMsg.setVisibility(View.VISIBLE);
                errorMsg.setText("You are not associated with any society");
            }
        }
    }

    @Override
    public void onErrorResponse(VolleyError error) {
        if (getActivity() != null) {
            progressBar.setVisibility(View.GONE);
            LockatedRequestError.onRequestError(getActivity(), error);
        }
    }

    @Override
    public void onResponse(JSONObject response) {
        progressBar.setVisibility(View.GONE);
        if (getActivity() != null) {
            try {
                if (response != null && response.length() > 0) {
                    if (response.has("noticeboards") && response.getJSONArray("noticeboards").length() > 0) {
                        JSONArray jsonArray = response.getJSONArray("noticeboards");
                        Gson gson = new Gson();
                        for (int i = 0; i < jsonArray.length(); i++) {
                            PublishNoticeboard noticeboard = gson.fromJson(jsonArray.getJSONObject(i).toString(), PublishNoticeboard.class);
                            publishNoticeboardArrayList.add(noticeboard);
                        }
                        publishedNoticeAdapter.notifyDataSetChanged();
                    } else {
                        noDataError();
                    }
                } else {
                    noDataError();
                }

            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            noDataError();
        }
    }

    public void noDataError() {
        errorMsg.setVisibility(View.VISIBLE);
        noticeList.setVisibility(View.GONE);
        errorMsg.setText(R.string.no_data_error);
    }

    public String getNoticeRole() {
        String mySocietyRoles = getActivity().getResources().getString(R.string.blank_value);
        if (mLockatedPreferences.getRolesJson() != null && !mLockatedPreferences.getRolesJson().equals("blank")) {
            try {
                JSONObject noticeJsonObj = new JSONObject(mLockatedPreferences.getRolesJson());
                for (int i = 0; i < noticeJsonObj.getJSONArray(getActivity().getResources().getString(R.string.permissions_value)).length(); i++) {
                    JSONObject jsonObject1 = noticeJsonObj.getJSONArray(getActivity().getResources().getString(R.string.permissions_value)).getJSONObject(i);
                    if (jsonObject1.has(getActivity().getResources().getString(R.string.section_value))
                            && jsonObject1.getString(getActivity().getResources().getString(R.string.section_value)).equals("spree_noticeboards")) {
                        if (jsonObject1.getJSONObject(getActivity().getResources().getString(R.string.permission_value)).has(getActivity().getResources().getString(R.string.index_value))) {
                            mySocietyRoles = jsonObject1.getJSONObject(getActivity().getResources().getString(R.string.permission_value)).getString(getActivity().getResources().getString(R.string.index_value));
                            CREATE = jsonObject1.getJSONObject(getActivity().getResources().getString(R.string.permission_value)).getString("create");
                            INDEX = jsonObject1.getJSONObject(getActivity().getResources().getString(R.string.permission_value)).getString("index");
                            UPDATE = jsonObject1.getJSONObject(getActivity().getResources().getString(R.string.permission_value)).getString("update");
                            EDIT = jsonObject1.getJSONObject(getActivity().getResources().getString(R.string.permission_value)).getString("edit");
                            SHOW = jsonObject1.getJSONObject(getActivity().getResources().getString(R.string.permission_value)).getString("show");
                        }
                    }
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return mySocietyRoles;
    }
}
