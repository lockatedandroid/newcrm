
package com.android.lockated.crm.adapters;

import android.content.Context;
import android.content.Intent;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.android.lockated.R;
import com.android.lockated.crm.activity.EventDetailActivity;
import com.android.lockated.information.AccountController;
import com.android.lockated.model.AccountData;
import com.android.lockated.model.usermodel.EventModel.EventSub;
import com.android.lockated.model.usermodel.EventModel.Events;
import com.android.lockated.utils.Utilities;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;


public class EventAdapter extends RecyclerView.Adapter<EventAdapter.EventViewHolder> {

    Context contextMain;
    JSONArray jsonArray;
    FragmentManager childFragmentManager;
    AccountController accountController;
    ArrayList<AccountData> datas;
    ArrayList<Events> eventArrayList;

    public EventAdapter(Context context, ArrayList<Events> eventArrayList, FragmentManager childFragmentManager) {
        this.contextMain = context;
        this.eventArrayList = eventArrayList;
        this.childFragmentManager = childFragmentManager;
        accountController = AccountController.getInstance();
        datas = accountController.getmAccountDataList();
    }


    @Override
    public EventViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(contextMain).inflate(R.layout.event_parent_view, parent, false);
        EventViewHolder pvh = new EventViewHolder(v);
        return pvh;
    }

    @Override
    public void onBindViewHolder(EventViewHolder holder, int position) {

        /*String dateSep = "(" + jsonArray.getJSONObject(position).get("date") + ")";
        holder.dateSeperator.setText(dateSep);
        EventGridAdapter EventGridAdapter = new EventGridAdapter(contextMain,
                jsonArray.getJSONObject(position).getJSONArray("eventSubs"));
        holder.recycler.setAdapter(EventGridAdapter);*/

        //String dateSep = "(" + eventArrayList.get(position).getEventSubs().get(position).getCreatedAt() + ")";
        //  Log.e("CreaedAt", "" + eventArrayList.get(position).getDate());
        String dateSep = "(" + eventArrayList.get(position).getDate() + ")";
        holder.dateSeperator.setText(dateSep);
        EventGridAdapter eventGridAdapter = new EventGridAdapter(contextMain, eventArrayList.get(position).getEvents());
        holder.recycler.setAdapter(eventGridAdapter);


    }

    @Override
    public int getItemCount() {
        return eventArrayList.size();
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }

    public class EventViewHolder extends RecyclerView.ViewHolder {

        RecyclerView recycler;
        TextView dateSeperator;

        public EventViewHolder(View itemView) {
            super(itemView);

            dateSeperator = (TextView) itemView.findViewById(R.id.textItem);
            recycler = (RecyclerView) itemView.findViewById(R.id.recyclerViewFirstAdapter);
            LinearLayoutManager linearLayoutManager = new LinearLayoutManager(contextMain,
                    LinearLayoutManager.HORIZONTAL, false);
            recycler.setLayoutManager(linearLayoutManager);
            recycler.setHasFixedSize(true);

        }

    }

    //======================================================================================================

    public class EventGridAdapter extends RecyclerView.Adapter<EventGridAdapter.EventGridViewHolder> implements View.OnClickListener {

        Context contextSub;
        JSONArray jsonArraySub;
        ArrayList<EventSub> events;

        public EventGridAdapter(Context context, ArrayList<EventSub> events) {
            this.contextSub = context;
            this.jsonArraySub = jsonArray;
            this.events = events;
        }

        @Override
        public EventGridViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View v = LayoutInflater.from(contextSub).inflate(R.layout.event_child_view, parent, false);
            EventGridViewHolder EventGridViewHolder = new EventGridViewHolder(v);
            return EventGridViewHolder;

        }

        @Override
        public void onBindViewHolder(EventGridViewHolder holder, int position) {

            /*try {
                holder.mDetailsView.setTag(position);
                if (jsonArraySub.getJSONObject(position).length() > 0) {
                    String fullDate = jsonArraySub.getJSONObject(position).getString("created_at");
                    String trimTime = fullDate.substring(fullDate.indexOf("T") + 1, fullDate.length());
                    StringTokenizer tk = new StringTokenizer(trimTime);
                    String time = tk.nextToken();

                    SimpleDateFormat sdf = new SimpleDateFormat("hh:mm:ss");
                    SimpleDateFormat sdfs = new SimpleDateFormat("hh:mm a");
                    Date dt;
                    try {
                        dt = sdf.parse(time);
                        trimTime = sdfs.format(dt);
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                    holder.timeTxt1.setText(trimTime);
                    if (!jsonArraySub.getJSONObject(position).getString("event_type").equals("null")) {
                        holder.subNameTxt1.setText(jsonArraySub.getJSONObject(position).getString("event_type"));
                    } else {
                        holder.subNameTxt1.setText("No Subject");
                    }
                    if (!jsonArraySub.getJSONObject(position).getString("event_name").equals("null")) {
                        holder.postedNameTxt1.setText(jsonArraySub.getJSONObject(position).getString("event_name"));
                    } else {
                        holder.postedNameTxt1.setText("No Post Available");
                    }
                } else {
                }
            } catch (Exception e) {
                e.printStackTrace();
                e.printStackTrace();
            }*/

            holder.mDetailsView.setTag(position);
            holder.mDetailsView.setOnClickListener(this);
            if (events.size() > 0) {
                String fullDate = events.get(position).getCreatedAt();
                String Datestr = Utilities.dateTime_AMPM_Format(events.get(position).getCreatedAt());
                holder.timeTxt1.setText(Datestr);
                if (events.get(position).getEventName() != null && !events.get(position).getEventName().equals("null")) {
                    holder.subNameTxt1.setText(events.get(position).getEventName());
                } else {
                    holder.subNameTxt1.setText("No Subject");
                }
                if (!events.get(position).getEventName().equals("null")) {
                    holder.postedNameTxt1.setText(events.get(position).getUsername());
                } else {
                    holder.postedNameTxt1.setText("No Name");
                }
            }

        }

        @Override
        public int getItemCount() {
            return events.size();
        }


        @Override
        public void onAttachedToRecyclerView(RecyclerView recyclerView) {
            super.onAttachedToRecyclerView(recyclerView);
        }

        @Override
        public void onClick(View v) {
            int position = (int) v.getTag();
            switch (v.getId()) {
                case R.id.mDetailsView:
                    EventSub eventSub = events.get(position);
                    viewCardDetail(contextSub, eventSub);
                    break;
            }
        }

        public void viewCardDetail(Context context, EventSub events) {
            // Log.e("events", "" + events.toString());
            JSONObject args = new JSONObject();
            try {
                args.put("event_type", events.getEventType());
                args.put("username", events.getUsername());
                args.put("event_name", events.getEventName());
                args.put("description", events.getDescription());
                args.put("event_at", events.getEventAt());
                args.put("from_time", events.getFromTime());
                args.put("to_time", events.getToTime());
                args.put("created_at", events.getCreatedAt());
                args.put("updated_at", events.getUpdatedAt());
                if (events.getDocumentsEvent() != null && events.getDocumentsEvent().size() > 0) {
                    args.put("document", events.getDocumentsEvent().get(0).getDocument());
                } else {
                    args.put("document", "No Document");

                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
            Intent allNoticeDetailIntent = new Intent(context, EventDetailActivity.class);
            allNoticeDetailIntent.putExtra("EventDetailViewData", args.toString());
            context.startActivity(allNoticeDetailIntent);

        }

        public class EventGridViewHolder extends RecyclerView.ViewHolder /*implements View.OnClickListener */ {

            TextView timeTxt1, subNameTxt1, postedNameTxt1, mDetailsView;
            CardView cardView;

            public EventGridViewHolder(View itemView) {
                super(itemView);

                cardView = (CardView) itemView.findViewById(R.id.cardView);
                timeTxt1 = (TextView) itemView.findViewById(R.id.time1);
                subNameTxt1 = (TextView) itemView.findViewById(R.id.subjectName1);
                postedNameTxt1 = (TextView) itemView.findViewById(R.id.postedName1);
                mDetailsView = (TextView) itemView.findViewById(R.id.mDetailsView);
                /*mDetailsView.setOnClickListener(this);*/
            }

           /* @Override
            public void onClick(View v) {
                switch (v.getId()) {
                    case R.id.mDetailsView:
                        int position = (int) v.getTag();
                        EventSub eventSub = events.get(position);
                        viewCardDetail(contextSub, eventSub);
                        break;

                }
            }*/


        }
    }

}