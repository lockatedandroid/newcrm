
package com.android.lockated.model.zomato;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ThirdpartyTransaction {

    @SerializedName("id")
    @Expose
    private int id;
    @SerializedName("thirdparty_id")
    @Expose
    private int thirdpartyId;
    @SerializedName("tptransactionid")
    @Expose
    private String tptransactionid;
    @SerializedName("amount")
    @Expose
    private String amount;
    @SerializedName("paymenttext")
    @Expose
    private String paymenttext;
    @SerializedName("paymentmethod")
    @Expose
    private String paymentmethod;
    @SerializedName("deliverylabel")
    @Expose
    private String deliverylabel;
    @SerializedName("tpdetails")
    @Expose
    private Tpdetails tpdetails;
    @SerializedName("thirdparty")
    @Expose
    private Object thirdparty;

    /**
     * 
     * @return
     *     The id
     */
    public int getId() {
        return id;
    }

    /**
     * 
     * @param id
     *     The id
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * 
     * @return
     *     The thirdpartyId
     */
    public int getThirdpartyId() {
        return thirdpartyId;
    }

    /**
     * 
     * @param thirdpartyId
     *     The thirdparty_id
     */
    public void setThirdpartyId(int thirdpartyId) {
        this.thirdpartyId = thirdpartyId;
    }

    /**
     * 
     * @return
     *     The tptransactionid
     */
    public String getTptransactionid() {
        return tptransactionid;
    }

    /**
     * 
     * @param tptransactionid
     *     The tptransactionid
     */
    public void setTptransactionid(String tptransactionid) {
        this.tptransactionid = tptransactionid;
    }

    /**
     * 
     * @return
     *     The amount
     */
    public String getAmount() {
        return amount;
    }

    /**
     * 
     * @param amount
     *     The amount
     */
    public void setAmount(String amount) {
        this.amount = amount;
    }

    /**
     * 
     * @return
     *     The paymenttext
     */
    public String getPaymenttext() {
        return paymenttext;
    }

    /**
     * 
     * @param paymenttext
     *     The paymenttext
     */
    public void setPaymenttext(String paymenttext) {
        this.paymenttext = paymenttext;
    }

    /**
     * 
     * @return
     *     The paymentmethod
     */
    public String getPaymentmethod() {
        return paymentmethod;
    }

    /**
     * 
     * @param paymentmethod
     *     The paymentmethod
     */
    public void setPaymentmethod(String paymentmethod) {
        this.paymentmethod = paymentmethod;
    }

    /**
     * 
     * @return
     *     The deliverylabel
     */
    public String getDeliverylabel() {
        return deliverylabel;
    }

    /**
     * 
     * @param deliverylabel
     *     The deliverylabel
     */
    public void setDeliverylabel(String deliverylabel) {
        this.deliverylabel = deliverylabel;
    }

    /**
     * 
     * @return
     *     The tpdetails
     */
    public Tpdetails getTpdetails() {
        return tpdetails;
    }

    /**
     * 
     * @param tpdetails
     *     The tpdetails
     */
    public void setTpdetails(Tpdetails tpdetails) {
        this.tpdetails = tpdetails;
    }

    /**
     * 
     * @return
     *     The thirdparty
     */
    public Object getThirdparty() {
        return thirdparty;
    }

    /**
     * 
     * @param thirdparty
     *     The thirdparty
     */
    public void setThirdparty(Object thirdparty) {
        this.thirdparty = thirdparty;
    }

}
