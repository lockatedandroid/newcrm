
package com.android.lockated.model.usermodel.Classified;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class MyClassified {

    @SerializedName("classifieds")
    @Expose
    private ArrayList<MyClassifiedMain> classifieds = new ArrayList<MyClassifiedMain>();

    /**
     * 
     * @return
     *     The classifieds
     */
    public ArrayList<MyClassifiedMain> getClassifieds() {
        return classifieds;
    }

    /**
     * 
     * @param classifieds
     *     The classifieds
     */
    public void setClassifieds(ArrayList<MyClassifiedMain> classifieds) {
        this.classifieds = classifieds;
    }

}
