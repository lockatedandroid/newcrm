package com.android.lockated.crmadmin.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.RecyclerView;
import android.text.method.ScrollingMovementMethod;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.android.lockated.R;
import com.android.lockated.crmadmin.activity.AdminFacilitiesActivity;
import com.android.lockated.model.AdminModel.AdminFacilities.AdminAmenity;
import com.android.lockated.network.ConnectionDetector;
import com.android.lockated.preferences.LockatedPreferences;
import com.android.lockated.request.LockatedRequestError;
import com.android.lockated.request.LockatedVolleyRequestQueue;
import com.android.lockated.utils.ApplicationURL;
import com.android.lockated.utils.Utilities;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

public class AdminFacilitiesListAdapter extends RecyclerView.Adapter<AdminFacilitiesListAdapter.FacilitiesViewHolder>
        implements Response.ErrorListener, Response.Listener<JSONObject> {

    private final ArrayList<AdminAmenity> adminAmenities;
    LayoutInflater layoutInflater;
    Context context;
    JSONArray jsonArray;
    FragmentManager childFragmentManager;
    String FACILITIES_LIST = "AdminFacilitiesListAdapter";

    public AdminFacilitiesListAdapter(Context context, FragmentManager childFragmentManager, ArrayList<AdminAmenity> adminAmenities) {

        this.context = context;
        layoutInflater = LayoutInflater.from(context);
        this.adminAmenities = adminAmenities;
        this.childFragmentManager = childFragmentManager;
    }

    @Override
    public FacilitiesViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(context).inflate(R.layout.facilities_list_view, parent, false);
        FacilitiesViewHolder facilitiesViewHolder = new FacilitiesViewHolder(v);
        return facilitiesViewHolder;
    }

    @Override
    public void onBindViewHolder(FacilitiesViewHolder holder, final int position) {

        holder.venueName.setText(adminAmenities.get(position).getName());
        String cost = String.valueOf("" + adminAmenities.get(position).getCost());

        if (cost.equals("0.0")) {
            holder.costValue.setText("Free");
        } else {
            holder.costValue.setText(cost);
        }
       /* holder.costValue.setText(adminAmenities.get(position).getCost());*/
        holder.description.setText(adminAmenities.get(position).getDescription());
        holder.costTypeValue.setText(adminAmenities.get(position).getCostType());
        holder.description.setMovementMethod(new ScrollingMovementMethod());
        holder.delete.setTag(position);
        holder.delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int position = (int) v.getTag();
                try {
           /*         Bundle args = new Bundle();
                    args.putString("facility_id", "" + adminAmenities.get(position).getId());
                    args.putString("cost", "" + adminAmenities.get(position).getCost());
                    args.putString("name", adminAmenities.get(position).getName());*/
                    deleteFacilities(String.valueOf(adminAmenities.get(position).getId()));
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    @Override
    public int getItemCount() {

        return adminAmenities.size();
        //jsonArray.length();
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }

    public class FacilitiesViewHolder extends RecyclerView.ViewHolder {
        TextView venueName, costValue, delete, description, costTypeValue;

        public FacilitiesViewHolder(View itemView) {
            super(itemView);
            venueName = (TextView) itemView.findViewById(R.id.venuTxt);
            costValue = (TextView) itemView.findViewById(R.id.costValue);
            delete = (TextView) itemView.findViewById(R.id.blockTextView);
            description = (TextView) itemView.findViewById(R.id.descriptionValue);
            costTypeValue = (TextView) itemView.findViewById(R.id.costTypeValue);
        }
    }

    public void deleteFacilities(String id) {
        if (ConnectionDetector.isConnectedToInternet(context)) {
            LockatedPreferences mLockatedPreferences = new LockatedPreferences(context);
            String url = ApplicationURL.deleteAmenities + id + ".json?token=" + mLockatedPreferences.getLockatedToken();
            LockatedVolleyRequestQueue mLockatedVolleyRequestQueue = LockatedVolleyRequestQueue.getInstance(context);
            //Log.e("deleteAmenities url", "" + url);
            mLockatedVolleyRequestQueue.sendRequest(FACILITIES_LIST, Request.Method.DELETE, url, null, this, this);
        } else {
            Utilities.showToastMessage(context, context.getResources().getString(R.string.internet_connection_error));
        }
    }

    @Override
    public void onResponse(JSONObject response) {
        //Log.e("deleteAmenities response", "" + response);
        Intent intent = new Intent(context, AdminFacilitiesActivity.class);
        intent.putExtra("AdminFacilitiesActivity", 1);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        context.startActivity(intent);
        /*if (response.has("amenities")) {

        }*/
    }

    @Override
    public void onErrorResponse(VolleyError error) {
        if (context != null) {
            LockatedRequestError.onRequestError(context, error);
        }
    }

}
