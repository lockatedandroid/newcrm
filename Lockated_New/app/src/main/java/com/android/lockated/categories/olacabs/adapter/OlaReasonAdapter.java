package com.android.lockated.categories.olacabs.adapter;


import android.app.Activity;
import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.android.lockated.R;

import java.util.ArrayList;

public class OlaReasonAdapter extends ArrayAdapter<String> {
    Context context;
    int layoutResourceId;
    ArrayList<String> data = null;

    public OlaReasonAdapter(Context context, int layoutResourceId, ArrayList<String> data) {
        super(context, layoutResourceId, data);
        this.layoutResourceId = layoutResourceId;
        this.context = context;
        this.data = data;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View row = convertView;
        ItemHolder holder = null;

        if (row == null) {
            LayoutInflater inflater = ((Activity) context).getLayoutInflater();
            row = inflater.inflate(layoutResourceId, parent, false);

            holder = new ItemHolder();
            holder.txt_reason = (TextView) row.findViewById(R.id.txt_reason);
            row.setTag(holder);
        } else {
            holder = (ItemHolder) row.getTag();
        }

        Log.e("Size of array", String.valueOf(data.size()));
        String reason = data.get(position);
        holder.txt_reason.setText(reason);
        return row;
    }

    public static class ItemHolder {
        TextView txt_reason;
    }

    public void updateAdapter(ArrayList<String> pers) {
        this.data = pers;
    }

    @Override
    public int getCount() {
        return data.size();
    }
}

