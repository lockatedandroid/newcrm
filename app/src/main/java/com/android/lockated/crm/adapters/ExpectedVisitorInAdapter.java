package com.android.lockated.crm.adapters;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.lockated.R;
import com.android.lockated.crm.activity.CrmVisitorActivity;
import com.android.lockated.crm.fragment.myzone.gatekeeper.VisitorInExpectedDetailView;
import com.android.lockated.model.expectedVisitor.GatekeeperExpected;
import com.android.lockated.preferences.LockatedPreferences;
import com.android.lockated.request.LockatedJSONObjectRequest;
import com.android.lockated.request.LockatedRequestError;
import com.android.lockated.request.LockatedVolleyRequestQueue;
import com.android.lockated.utils.ApplicationURL;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class ExpectedVisitorInAdapter extends BaseAdapter implements View.OnClickListener, Response.ErrorListener, Response.Listener<JSONObject> {
    private final Context context;
    private final ArrayList<GatekeeperExpected> gatekeeperArrayList;
    private final FragmentManager supportFragmentManager;
    LayoutInflater layoutInflater;
    private String REQUEST_TAG;
    private LockatedPreferences mLockatedPreferences;
    private RequestQueue mQueue;

    public ExpectedVisitorInAdapter(Context context, ArrayList<GatekeeperExpected> gatekeeperArrayList, FragmentManager supportFragmentManager) {
        this.context = context;
        this.gatekeeperArrayList = gatekeeperArrayList;
        this.supportFragmentManager = supportFragmentManager;
        this.layoutInflater = LayoutInflater.from(context);
    }

    @Override
    public int getCount() {
        return gatekeeperArrayList.size();
    }

    @Override
    public int getViewTypeCount() {

        return getCount();
    }

    @Override
    public int getItemViewType(int position) {

        return position;
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder = null;
        if (convertView == null) {
            convertView = layoutInflater.inflate(R.layout.visitor_in_list_child, parent, false);
            holder = new ViewHolder();
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        holder.listItem = (LinearLayout) convertView.findViewById(R.id.listItem);
        holder.mStatusLayout = (LinearLayout) convertView.findViewById(R.id.mStatusLayout);
        holder.mImageViewOut = (ImageView) convertView.findViewById(R.id.mImageViewOut);
        holder.mImageViewIn = (ImageView) convertView.findViewById(R.id.mImageViewIn);
        holder.mImageViewVisitor = (ImageView) convertView.findViewById(R.id.mImageViewVisitor);
        holder.mTextApprove = (TextView) convertView.findViewById(R.id.mTextApprove);
        holder.textReject = (TextView) convertView.findViewById(R.id.textReject);
        holder.mStatus = (TextView) convertView.findViewById(R.id.mStatus);
        holder.textAcompany = (TextView) convertView.findViewById(R.id.textAcompany);
        holder.mImageMoveIn = (TextView) convertView.findViewById(R.id.mImageMoveIn);
        holder.mTextVisitorName = (TextView) convertView.findViewById(R.id.mTextVisitorName);
        holder.mTextMobileNumber = (TextView) convertView.findViewById(R.id.mTextMobileNumber);
        /*holder.mImageViewVisitor.setImageResource(R.drawable.sagar);*/
        holder.mTextVisitorName.setText(gatekeeperArrayList.get(position).getGuestName());
        holder.mTextMobileNumber.setText(gatekeeperArrayList.get(position).getGuestNumber());
        //null null
        holder.mImageViewOut.setTag(position);
        holder.mImageViewIn.setTag(position);


        String approve = "" + gatekeeperArrayList.get(position).getApprove();
        switch (approve) {
            case "0":
                holder.mStatusLayout.setVisibility(View.VISIBLE);
                holder.mTextApprove.setVisibility(View.VISIBLE);
                holder.textReject.setVisibility(View.VISIBLE);
                holder.textAcompany.setVisibility(View.VISIBLE);
                holder.mStatus.setText(context.getResources().getString(R.string.status_pending));
                break;
            case "1":
                holder.mStatusLayout.setVisibility(View.GONE);
                holder.mTextApprove.setVisibility(View.GONE);
                holder.textReject.setVisibility(View.GONE);
                holder.textAcompany.setVisibility(View.GONE);
                String accompany = gatekeeperArrayList.get(position).getAccompany();
                if (accompany != null && accompany.equals("1")) {
                    holder.mStatus.setText(context.getResources().getString(R.string.status_accompany));
                } else {
                    holder.mStatus.setText(context.getResources().getString(R.string.status_approve));
                }
                break;
            case "2":
                holder.mStatusLayout.setVisibility(View.GONE);
                holder.mTextApprove.setVisibility(View.GONE);
                holder.textReject.setVisibility(View.GONE);
                holder.textAcompany.setVisibility(View.GONE);
                holder.mStatus.setText(context.getResources().getString(R.string.status_reject));
                break;
        }

        //Log.e("image", "" + gatekeeperArrayList.get(position).getImage());
        if (gatekeeperArrayList.get(position).getImage() != null && !gatekeeperArrayList.get(position).getImage().equals("")) {
            String url = gatekeeperArrayList.get(position).getImage();
            Picasso.with(context).load(url).fit().into(holder.mImageViewVisitor);

        }
        if (gatekeeperArrayList.get(position).getGuestEntryTime() != null &&
                gatekeeperArrayList.get(position).getGuestExitTime() != null) {
            //Log.e("Visible out", "" + position);
            holder.mImageViewOut.setVisibility(View.VISIBLE);
        } else if (gatekeeperArrayList.get(position).getGuestEntryTime() != null && gatekeeperArrayList.get(position).getGuestExitTime() == null) {
            holder.mImageViewIn.setVisibility(View.VISIBLE);
            //Log.e("Visible in", "" + position);
        } else if (gatekeeperArrayList.get(position).getGuestEntryTime() == null && gatekeeperArrayList.get(position).getGuestExitTime() != null) {
            //Log.e("Visible out2", "" + position);
            holder.mImageViewOut.setVisibility(View.VISIBLE);

        }
        holder.mImageDelete = (ImageView) convertView.findViewById(R.id.mImageDelete);
        holder.mImageDelete.setTag(position);
        holder.mImageDelete.setOnClickListener(this);
        holder.textReject.setTag(position);
        holder.textReject.setOnClickListener(this);
        holder.mTextApprove.setTag(position);
        holder.mTextApprove.setOnClickListener(this);
        holder.listItem.setTag(position);
        holder.listItem.setOnClickListener(this);
        holder.textAcompany.setTag(position);
        holder.textAcompany.setOnClickListener(this);

        return convertView;
    }

    @Override
    public void onClick(View v) {
        int position = (int) v.getTag();
        switch (v.getId()) {
            case R.id.listItem:
                //Log.e("position", "" + position);
                detailViewClick(position);
                break;
            case R.id.mTextApprove:
                onApprovedClicked(gatekeeperArrayList.get(position).getId());
                break;
            case R.id.textAcompany:
                onAcompanyClicked(gatekeeperArrayList.get(position).getId());
                break;
            case R.id.textReject:
                onRejectClicked(gatekeeperArrayList.get(position).getId());
                break;
            case R.id.mImageDelete:
                onVisitorDelete(gatekeeperArrayList.get(position).getId());
                break;
        }
    }

    private void onApprovedClicked(int approve) {
        mLockatedPreferences = new LockatedPreferences(context);
        JSONObject jsonObjectMain = new JSONObject();
        try {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("approve", "1");
            jsonObjectMain.put("gatekeeper", jsonObject);

        } catch (JSONException e) {
            e.printStackTrace();
        }
        mQueue = LockatedVolleyRequestQueue.getInstance(context).getRequestQueue();
        String url = ApplicationURL.UpdateGateKeeper + approve + ".json?token="
                + mLockatedPreferences.getLockatedToken();
        LockatedJSONObjectRequest lockatedJSONObjectRequest = new LockatedJSONObjectRequest(Request.Method.PUT,
                url,
                jsonObjectMain, this, this);
        lockatedJSONObjectRequest.setTag(REQUEST_TAG);
        mQueue.add(lockatedJSONObjectRequest);
    }

    private void onAcompanyClicked(int approve) {
        mLockatedPreferences = new LockatedPreferences(context);
        JSONObject jsonObjectMain = new JSONObject();
        try {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("approve", "1");
            jsonObject.put("accompany", "1");
            jsonObjectMain.put("gatekeeper", jsonObject);

        } catch (JSONException e) {
            e.printStackTrace();
        }
        mQueue = LockatedVolleyRequestQueue.getInstance(context).getRequestQueue();
        String url = ApplicationURL.UpdateGateKeeper + approve + ".json?token="
                + mLockatedPreferences.getLockatedToken();
        LockatedJSONObjectRequest lockatedJSONObjectRequest = new LockatedJSONObjectRequest(Request.Method.PUT,
                url,
                jsonObjectMain, this, this);
        lockatedJSONObjectRequest.setTag(REQUEST_TAG);
        mQueue.add(lockatedJSONObjectRequest);
    }

    private void onRejectClicked(int approve) {
        mLockatedPreferences = new LockatedPreferences(context);
        JSONObject jsonObjectMain = new JSONObject();
        try {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("approve", "2");
            jsonObjectMain.put("gatekeeper", jsonObject);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        mQueue = LockatedVolleyRequestQueue.getInstance(context).getRequestQueue();
        String url = ApplicationURL.UpdateGateKeeper + approve + ".json?token="
                + mLockatedPreferences.getLockatedToken();
        LockatedJSONObjectRequest lockatedJSONObjectRequest = new LockatedJSONObjectRequest(Request.Method.PUT,
                url,
                jsonObjectMain, this, this);
        lockatedJSONObjectRequest.setTag(REQUEST_TAG);
        mQueue.add(lockatedJSONObjectRequest);
    }


    private void detailViewClick(int position) {
        VisitorInExpectedDetailView visitorInDetailView = new VisitorInExpectedDetailView();
        Bundle bundle = new Bundle();
        bundle.putString("guest_name", gatekeeperArrayList.get(position).getGuestName());
        bundle.putString("geuest_number", gatekeeperArrayList.get(position).getGuestNumber());
        bundle.putString("id", String.valueOf(gatekeeperArrayList.get(position).getId()));
        bundle.putString("flat", gatekeeperArrayList.get(position).getFlat());
        bundle.putString("status",gatekeeperArrayList.get(position).getVstatus());
        if (gatekeeperArrayList.get(position).getGuestVehicleNumber() != null) {
            bundle.putString("geuest_vehicle_number", gatekeeperArrayList.get(position).getGuestVehicleNumber());
        } else {
            bundle.putString("geuest_vehicle_number", "Not Available");
        }
        if (gatekeeperArrayList.get(position).getNotes() != null) {
            bundle.putString("geuest_notes", gatekeeperArrayList.get(position).getNotes());
        } else {
            bundle.putString("geuest_notes", "Not Available");
        }

        bundle.putString("geuest_visit_to", gatekeeperArrayList.get(position).getVisitTo());

        if (gatekeeperArrayList.get(position).getCreatedBy() != null) {
            bundle.putString("member_name", gatekeeperArrayList.get(position).getCreatedBy());
        } else {
            bundle.putString("member_name", "Not Available");
        }

        if (gatekeeperArrayList.get(position).getExpectedAt() != null) {
            bundle.putString("guest_expected_at", gatekeeperArrayList.get(position).getExpectedAt());
        } else {
            bundle.putString("guest_expected_at", "Not Available");
        }
        bundle.putString("approve", String.valueOf(gatekeeperArrayList.get(position).getApprove()));
        bundle.putString("geuest_approve", String.valueOf(gatekeeperArrayList.get(position).getApprove()));
        bundle.putString("geuest_purpose", gatekeeperArrayList.get(position).getVisitPurpose());
        bundle.putString("geuest_plus_person", String.valueOf(gatekeeperArrayList.get(position).getPlusPerson()));

        if (gatekeeperArrayList.get(position).getImage() != null && !gatekeeperArrayList.get(position).getImage().equals("")) {
            bundle.putString("guest_image", gatekeeperArrayList.get(position).getImage());
        } else {
            bundle.putString("guest_image", "no");
        }
        if (gatekeeperArrayList.get(position).getGatekeeperMimos() != null &&
                gatekeeperArrayList.get(position).getGatekeeperMimos().size() > 0) {
            Log.e("getMimoType",""+gatekeeperArrayList.get(position).getGatekeeperMimos().get(0).getMimoType());
            if (gatekeeperArrayList.get(position).getGatekeeperMimos().get(0).getMimoType().equals("move_in"))
            {
                bundle.putString("geuest_mimo_type", "Move In");
            }
            else if (gatekeeperArrayList.get(position).getGatekeeperMimos().get(0).getMimoType().equals("move_out"))
            {
                bundle.putString("geuest_mimo_type", "Move Out");
            }

            bundle.putString("geuest_item_number", String.valueOf(gatekeeperArrayList.get(position).getGatekeeperMimos().get(0).getItemNumber()));

            if (gatekeeperArrayList.get(position).getGatekeeperMimos().get(0).getDetails() != null && !gatekeeperArrayList.get(position).getGatekeeperMimos().get(0).getDetails().equals("")) {

                bundle.putString("geuest_item_list", gatekeeperArrayList.get(position).getGatekeeperMimos().get(0).getDetails());
            } else {
                bundle.putString("geuest_item_list", "Not Available");

            }

            bundle.putString("geuest_need_approval", String.valueOf(gatekeeperArrayList.get(position).getGatekeeperMimos().get(0).
                    getNeedApproval()));
            if (gatekeeperArrayList.get(position).getGatekeeperMimos().get(0).getDocuments() != null &&
                    gatekeeperArrayList.get(position).getGatekeeperMimos().get(0).getDocuments().size() > 0) {
                //Log.e("size", "" + gatekeeperArrayList.get(position).getGatekeeperMimos().get(0).getDocuments().size());
                bundle.putParcelableArrayList("geuest_document", gatekeeperArrayList.get(position).getGatekeeperMimos().get(0).getDocuments());
            } else {
                bundle.putParcelableArrayList("geuest_document", null);

            }
        } else {
            bundle.putString("geuest_mimo_type", "Not Available");
            bundle.putString("geuest_item_list", "Not Available");
            bundle.putString("geuest_item_number", "0");
            bundle.putString("geuest_need_approval", "0");
        }
        visitorInDetailView.setArguments(bundle);
        visitorInDetailView.show(supportFragmentManager, "Show");
    }

    private void onVisitorDelete(int pid) {
        mLockatedPreferences = new LockatedPreferences(context);
        mQueue = LockatedVolleyRequestQueue.getInstance(context).getRequestQueue();
        LockatedJSONObjectRequest lockatedJSONObjectRequest = new LockatedJSONObjectRequest(Request.Method.DELETE,
                ApplicationURL.deleteVisitor + pid + ".json?token="
                        + mLockatedPreferences.getLockatedToken(), null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                if (response != null && response.length() > 0) {

                    if (response.has("code") && response.has("message")) {
                        Intent intent = new Intent(context, CrmVisitorActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        context.startActivity(intent);
                    }
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                LockatedRequestError.onRequestError(context, error);

            }
        });
        lockatedJSONObjectRequest.setTag(REQUEST_TAG);
        mQueue.add(lockatedJSONObjectRequest);
    }


    @Override
    public void onErrorResponse(VolleyError error) {
        LockatedRequestError.onRequestError(context, error);
    }

    @Override
    public void onResponse(JSONObject response) {
        if (response.has("id")) {
            Intent intent = new Intent(context, CrmVisitorActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            context.startActivity(intent);
        }
    }

    private class ViewHolder {
        TextView mTextVisitorName, mTextMobileNumber;
        ImageView mImageViewVisitor;
        LinearLayout listItem;
        LinearLayout mStatusLayout;
        TextView mImageMoveIn;
        TextView mTextApprove;
        TextView textReject;
        TextView textAcompany;
        TextView mStatus;
        ImageView mImageDelete;
        ImageView mImageViewOut;
        ImageView mImageViewIn;
    }
}
