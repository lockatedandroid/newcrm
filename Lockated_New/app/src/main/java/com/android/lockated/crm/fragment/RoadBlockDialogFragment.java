package com.android.lockated.crm.fragment;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.android.lockated.R;
import com.android.lockated.network.ConnectionDetector;
import com.android.lockated.request.LockatedJSONObjectRequest;
import com.android.lockated.request.LockatedRequestError;
import com.android.lockated.request.LockatedVolleyRequestQueue;
import com.android.lockated.utils.ApplicationURL;
import com.android.lockated.utils.Utilities;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;

import org.json.JSONException;
import org.json.JSONObject;


public class RoadBlockDialogFragment extends DialogFragment implements View.OnClickListener,
        Response.ErrorListener, Response.Listener<JSONObject>, TextWatcher {

    LayoutInflater inflatedLayout;
    EditText pincodeEdt;
    Button checkBtn, continueBtn, changeBtn, checkAgainBtn;
    TextView msgText, messageSucces;
    LinearLayout linearLayoutMain, linearLayoutSuccess, linearLayoutError;
    TextView citySpinner;
    private RequestQueue mQueue;
    private ProgressBar progressBar;
    public static final String REQUEST_TAG = "RoadBlockDialogFragment";
    Window window;
    private static boolean backPressFinish = false;
    Context context;


    public void setRoadBlockDialogFragment(Context context) {
        this.context = context;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getActivity() != null) {
            inflatedLayout = LayoutInflater.from(getActivity());
        }
    }

    @Override
    public void onStart() {
        super.onStart();

        window = getDialog().getWindow();
        WindowManager.LayoutParams windowParams = window.getAttributes();
        windowParams.dimAmount = 0.75f;
        windowParams.flags |= WindowManager.LayoutParams.FLAG_DIM_BEHIND;
        window.setAttributes(windowParams);
        window.setBackgroundDrawableResource(android.R.color.transparent);

    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        View roadBlockDialogFragmentView = inflatedLayout.inflate(R.layout.fragment_road_block_dialog,
                new LinearLayout(getActivity()), false);

        init(roadBlockDialogFragmentView);
        Dialog builder = new Dialog(getActivity());
        builder.requestWindowFeature(Window.FEATURE_NO_TITLE);
        builder.getWindow().setBackgroundDrawable(new ColorDrawable(Color.WHITE));
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(builder.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.WRAP_CONTENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        builder.setCanceledOnTouchOutside(false);
        builder.show();
        builder.getWindow().setAttributes(lp);
        builder.setContentView(roadBlockDialogFragmentView);

        return builder;

    }

    private void init(View view) {

        backPressFinish = true;
        Utilities.lockatedGoogleAnalytics(getString(R.string.road_block),
                getString(R.string.visited), getString(R.string.road_block));
        linearLayoutMain = (LinearLayout) view.findViewById(R.id.linearMain);
        linearLayoutSuccess = (LinearLayout) view.findViewById(R.id.linearSuccess);
        linearLayoutError = (LinearLayout) view.findViewById(R.id.linearError);
        progressBar = (ProgressBar) view.findViewById(R.id.mProgressBarSmallView);
        pincodeEdt = (EditText) view.findViewById(R.id.pincode);
        checkBtn = (Button) view.findViewById(R.id.checkButton);
        continueBtn = (Button) view.findViewById(R.id.continueButton);
        checkAgainBtn = (Button) view.findViewById(R.id.checkAgainButton);
        changeBtn = (Button) view.findViewById(R.id.changeButton);
        msgText = (TextView) view.findViewById(R.id.messageError);
        messageSucces = (TextView) view.findViewById(R.id.messageSucces);
        citySpinner = (TextView) view.findViewById(R.id.spinnerCity);
        disableBtn();
        pincodeEdt.addTextChangedListener(this);
        checkBtn.setOnClickListener(this);
        changeBtn.setOnClickListener(this);
        continueBtn.setOnClickListener(this);
        checkAgainBtn.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {

            case R.id.checkButton:
                roadBlock(pincodeEdt.getText().toString());
                break;

            case R.id.continueButton:
                backPressFinish = false;
                dismiss();
                break;

            case R.id.changeButton:

                linearLayoutSuccess.setVisibility(View.GONE);
                linearLayoutMain.setVisibility(View.VISIBLE);
                linearLayoutError.setVisibility(View.GONE);

                break;

            case R.id.checkAgainButton:
                backPressFinish = true;
                linearLayoutSuccess.setVisibility(View.GONE);
                linearLayoutMain.setVisibility(View.VISIBLE);
                linearLayoutError.setVisibility(View.GONE);

                break;

        }

    }

    public void enableBtn() {
        checkBtn.setEnabled(true);
        checkBtn.setBackgroundColor(getActivity().getResources().getColor(R.color.submit));
    }

    public void disableBtn() {
        checkBtn.setEnabled(false);
        checkBtn.setBackgroundColor(getActivity().getResources().getColor(R.color.close));
    }

    private void roadBlock(String pinCode) {

        if (ConnectionDetector.isConnectedToInternet(getActivity())) {
            progressBar.setVisibility(View.VISIBLE);
            mQueue = LockatedVolleyRequestQueue.getInstance(getActivity()).getRequestQueue();
            LockatedJSONObjectRequest lockatedJSONObjectRequest = new LockatedJSONObjectRequest(Request.Method.GET,
                    ApplicationURL.getServiceabilityUrl + pinCode + "+&service_id=6", null, this, this);
            lockatedJSONObjectRequest.setTag(REQUEST_TAG);
            mQueue.add(lockatedJSONObjectRequest);
        } else {
            Utilities.showToastMessage(getActivity(), getActivity().getResources().getString(R.string.internet_connection_error));
        }

    }

    @Override
    public void onErrorResponse(VolleyError error) {
        if (getActivity() != null) {
            progressBar.setVisibility(View.GONE);
            LockatedRequestError.onRequestError(getActivity(), error);
            pincodeEdt.setText("");
        }
    }

    @Override
    public void onResponse(JSONObject response) {
        if (getActivity() != null) {
            progressBar.setVisibility(View.GONE);
            pincodeEdt.setText("");
            try {
                String responseCode = response.getString("code");

                if (responseCode.equalsIgnoreCase("200")) {
                    linearLayoutMain.setVisibility(View.GONE);
                    linearLayoutSuccess.setVisibility(View.VISIBLE);
                    messageSucces.setText(response.getString("message"));
                } else {
                    linearLayoutMain.setVisibility(View.GONE);
                    linearLayoutError.setVisibility(View.VISIBLE);
                    msgText.setText(response.getString("error"));
                }

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void onDismiss(DialogInterface dialog) {
        if (backPressFinish) {
            if (getActivity() != null) {
                getActivity().onBackPressed();
            }
        } else {
            dialog.dismiss();
        }
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {

        if (s.length() == 6) {
            enableBtn();
        }

    }

    @Override
    public void afterTextChanged(Editable s) {

    }
}
