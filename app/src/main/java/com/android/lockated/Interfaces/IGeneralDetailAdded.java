package com.android.lockated.Interfaces;

import com.android.lockated.model.GeneralDetailData;

public interface IGeneralDetailAdded
{
    public void onGeneralDetailAdded(GeneralDetailData generalData, int requestId);
}
