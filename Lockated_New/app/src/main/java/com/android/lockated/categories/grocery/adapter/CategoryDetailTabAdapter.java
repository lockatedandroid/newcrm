package com.android.lockated.categories.grocery.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.android.lockated.categories.grocery.fragment.CategoryProductDetailViewFragment;
import com.android.lockated.landing.model.TaxonData;

import java.util.ArrayList;

public class CategoryDetailTabAdapter extends FragmentPagerAdapter {
    private ArrayList<TaxonData> mTaxonData;

    public CategoryDetailTabAdapter(FragmentManager fm, ArrayList<TaxonData> tabs) {
        super(fm);
        mTaxonData = tabs;
    }

    @Override
    public Fragment getItem(int position) {
        CategoryProductDetailViewFragment categoryViewFragment = new CategoryProductDetailViewFragment();
        //Log.e("mTaxonData", String.valueOf(mTaxonData.get(position).getId()));
        categoryViewFragment.setProductId(mTaxonData.get(position).getId());
        return categoryViewFragment;
    }

    @Override
    public int getItemPosition(Object object) {
        return super.getItemPosition(object);
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return mTaxonData.get(position).getName();
    }

    @Override
    public long getItemId(int position) {
        return super.getItemId(position);
    }

    @Override
    public int getCount() {
        return mTaxonData.size() - 1;
    }
}
