package com.android.lockated.pushnotification;

import android.app.ActivityManager;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.support.v7.app.NotificationCompat;
import android.text.Html;
import android.text.TextUtils;
import android.util.Patterns;

import com.android.lockated.LockatedApplication;
import com.android.lockated.R;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Random;

public class NotificationUtils {

    private Context mContext;
    Random random = new Random();
    int NOTIFICATION_ID = random.nextInt(9999 - 1000) + 1000;

    public NotificationUtils() {
    }

    public NotificationUtils(Context mContext) {
        this.mContext = mContext;
    }

    public void showNotificationMessage(String title, String message, String timeStamp, Intent intent) {
        showNotificationMessage(title, message, timeStamp, intent, null);
    }

    public void showNotificationMessageWithDriverNumber(String title, String message, String timeStamp, Intent intent)
    {
        showNotificationMessageWithDriverNumber(title, message, timeStamp, intent,null);
    }


    public void showNotificationMessage(final String title, final String message, final String timeStamp, Intent intent, String imageUrl) {
        if (TextUtils.isEmpty(message))
            return;
        final int icon = R.mipmap.ic_launcher;
        //final int icon = getNotificationIcon();
        final PendingIntent resultPendingIntent;
        if (intent.getExtras().containsKey("force_close") || intent.getExtras().containsKey("userId")) {
            resultPendingIntent = PendingIntent.getActivity(mContext, 0, intent, /*PendingIntent.FLAG_IMMUTABLE*/0);
        } else {
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            resultPendingIntent = PendingIntent.getActivity(mContext, 0, intent, 0);
        }
        final NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(mContext);
        Uri notification = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        if (!TextUtils.isEmpty(imageUrl)) {
            if (imageUrl != null && imageUrl.length() > 4 && Patterns.WEB_URL.matcher(imageUrl).matches()) {
                Bitmap bitmap = getBitmapFromURL(imageUrl);
                if (bitmap != null) {
                    showBigNotification(bitmap, mBuilder, icon, title, message, timeStamp, resultPendingIntent, notification, intent);
                } else {
                    showSmallNotification(mBuilder, icon, title, message, timeStamp, resultPendingIntent, notification, intent);
                }
            }
        } else {
            showSmallNotification(mBuilder, icon, title, message, timeStamp, resultPendingIntent, notification, intent);
            playNotificationSound();
        }

    }

    private void showSmallNotification(NotificationCompat.Builder mBuilder, int icon, String title,
                                       String message, String timeStamp, PendingIntent resultPendingIntent,
                                       Uri alarmSound, Intent intent) {

        NotificationCompat.InboxStyle inboxStyle = new NotificationCompat.InboxStyle();
        if (PushNotificationConfig.appendNotificationMessages) {
            LockatedApplication.getInstance().getPrefManager().addNotification(message);
            String oldNotification = LockatedApplication.getInstance().getPrefManager().getNotifications();
            List<String> messages = Arrays.asList(oldNotification.split("\\|"));
            for (int i = messages.size() - 1; i >= 0; i--) {
                inboxStyle.addLine(messages.get(i));
            }
        } else {
            inboxStyle.addLine(message);
        }
        if (intent.getExtras() != null && intent.getExtras().containsKey("dep_id") && intent.getExtras().containsKey("dep_name") &&
                intent.getExtras().getInt("dep_id") != 0) {
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            ((LockatedApplication) mContext).setmSectionId(intent.getExtras().getInt("dep_id"));
            ((LockatedApplication) mContext).setSectionName(intent.getExtras().getString("dep_name"));
        }
        Notification notification;
        notification = mBuilder.setSmallIcon(icon).setTicker(title).setWhen(0)
                .setAutoCancel(true)
                .setContentTitle(title)
                .setContentIntent(resultPendingIntent)
                .setSound(alarmSound)
                .setStyle(inboxStyle)
                .setWhen(getTimeMilliSec(timeStamp))
                .setSmallIcon(icon)
                .setLargeIcon(BitmapFactory.decodeResource(mContext.getResources(), icon))
                .setContentText(message)
                .build();
        notification.flags |= Notification.FLAG_AUTO_CANCEL;
        NotificationManager notificationManager = (NotificationManager) mContext.getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.notify(/*PushNotificationConfig.NOTIFICATION_ID*/NOTIFICATION_ID, notification);
    }

    private void showBigNotification(Bitmap bitmap, NotificationCompat.Builder mBuilder, int icon,
                                     String title, String message, String timeStamp,
                                     PendingIntent resultPendingIntent, Uri alarmSound, Intent intent) {

        if (intent.getExtras() != null && intent.getExtras().containsKey("dep_id") && intent.getExtras().containsKey("dep_name") &&
                intent.getExtras().getInt("dep_id") != 0) {
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            ((LockatedApplication) mContext).setmSectionId(intent.getExtras().getInt("dep_id"));
            ((LockatedApplication) mContext).setSectionName(intent.getExtras().getString("dep_name"));
        }
        NotificationCompat.BigPictureStyle bigPictureStyle = new NotificationCompat.BigPictureStyle();
        bigPictureStyle.setBigContentTitle(title);
        bigPictureStyle.setSummaryText(Html.fromHtml(message).toString());
        bigPictureStyle.bigPicture(bitmap);
        Notification notification;
        notification = mBuilder.setSmallIcon(icon).setTicker(title).setWhen(0)
                .setAutoCancel(true)
                .setContentTitle(title)
                .setContentIntent(resultPendingIntent)
                .setSound(alarmSound)
                .setStyle(bigPictureStyle)
                .setWhen(getTimeMilliSec(timeStamp))
                .setSmallIcon(icon)
                .setLargeIcon(BitmapFactory.decodeResource(mContext.getResources(), icon))
                .setContentText(message)
                .setStyle(new NotificationCompat.BigTextStyle()
                        .bigText(message))
                .build();
        NotificationManager notificationManager = (NotificationManager) mContext.getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.notify(PushNotificationConfig.NOTIFICATION_ID_BIG_IMAGE, notification);
    }

    public void showNotificationMessageWithDriverNumber(final String title, final String message, final String timeStamp, Intent intent, String driverNumber) {
        if (TextUtils.isEmpty(message))
            return;
        final int icon = R.mipmap.ic_launcher;
        final PendingIntent resultPendingIntent;
        if (intent.getExtras().containsKey("force_close") || intent.getExtras().containsKey("userId")) {
            resultPendingIntent = PendingIntent.getActivity(mContext, 0, intent, /*PendingIntent.FLAG_IMMUTABLE*/0);
        } else {
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            resultPendingIntent = PendingIntent.getActivity(mContext, 0, intent, 0);
        }
        final NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(mContext);
        Uri notification = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        if (!TextUtils.isEmpty(driverNumber)) {
            if (driverNumber != null && driverNumber.length() != 0) {
                //showBigNotification(bitmap, mBuilder, icon, title, message, timeStamp, resultPendingIntent, notification, intent);
                showSmallNotification(mBuilder, icon, title, message, timeStamp, resultPendingIntent, notification, intent);
            }
        } else {
            showSmallNotification(mBuilder, icon, title, message, timeStamp, resultPendingIntent, notification, intent);
            playNotificationSound();
        }

    }


    public Bitmap getBitmapFromURL(String strURL) {
        try {
            URL url = new URL(strURL);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setDoInput(true);
            connection.connect();
            InputStream input = connection.getInputStream();
            Bitmap myBitmap = BitmapFactory.decodeStream(input);
            return myBitmap;
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    public void playNotificationSound() {

        try {
            Uri notification = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
            Ringtone r = RingtoneManager.getRingtone(LockatedApplication.getInstance().getApplicationContext(), notification);
            r.play();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public static boolean isAppIsInBackground(Context context) {
        boolean isInBackground = true;
        ActivityManager am = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.KITKAT_WATCH) {
            List<ActivityManager.RunningAppProcessInfo> runningProcesses = am.getRunningAppProcesses();
            for (ActivityManager.RunningAppProcessInfo processInfo : runningProcesses) {
                if (processInfo.importance == ActivityManager.RunningAppProcessInfo.IMPORTANCE_FOREGROUND) {
                    for (String activeProcess : processInfo.pkgList) {
                        if (activeProcess.equals(context.getPackageName())) {
                            //isInBackground = true;
                            isInBackground = false;
                        }
                    }
                }
            }
        } else {
            List<ActivityManager.RunningTaskInfo> taskInfo = am.getRunningTasks(1);
            ComponentName componentInfo = taskInfo.get(0).topActivity;
            if (componentInfo.getPackageName().equals(context.getPackageName())) {
                //isInBackground = true;
                isInBackground = false;
            }
        }
        return isInBackground;
    }

    public static void clearNotifications() {
        NotificationManager notificationManager = (NotificationManager) LockatedApplication.getInstance()
                .getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.cancelAll();
    }

    public static long getTimeMilliSec(String timeStamp) {
        if (timeStamp != null) {
            SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.ENGLISH);
            try {
                Date date = format.parse(timeStamp);
                return date.getTime();
            } catch (ParseException e) {
                e.printStackTrace();
                return 0;
            }
        } else {
            Calendar c = Calendar.getInstance();
            SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.ENGLISH);
            String formattedDate = df.format(c.getTime());
            try {
                Date date = df.parse(formattedDate);
                return date.getTime();
            } catch (ParseException e) {
                e.printStackTrace();
                return 0;
            }
        }
    }

    private int getNotificationIcon() {
        boolean useWhiteIcon = (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP);
        return useWhiteIcon ? R.mipmap.ic_launcher : R.mipmap.ic_launcher;
    }

}