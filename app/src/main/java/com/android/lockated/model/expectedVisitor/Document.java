
package com.android.lockated.model.expectedVisitor;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Document implements Parcelable {

    @SerializedName("id")
    @Expose
    private int id;
    @SerializedName("relation")
    @Expose
    private String relation;
    @SerializedName("relation_id")
    @Expose
    private int relationId;
    @SerializedName("document")
    @Expose
    private String document;
    @SerializedName("doctype")
    @Expose
    private String doctype;

    /**
     * @return The id
     */
    public int getId() {
        return id;
    }

    /**
     * @param id The id
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * @return The relation
     */
    public String getRelation() {
        return relation;
    }

    /**
     * @param relation The relation
     */
    public void setRelation(String relation) {
        this.relation = relation;
    }

    /**
     * @return The relationId
     */
    public int getRelationId() {
        return relationId;
    }

    /**
     * @param relationId The relation_id
     */
    public void setRelationId(int relationId) {
        this.relationId = relationId;
    }

    /**
     * @return The document
     */
    public String getDocument() {
        return document;
    }

    /**
     * @param document The document
     */
    public void setDocument(String document) {
        this.document = document;
    }

    /**
     * @return The doctype
     */
    public String getDoctype() {
        return doctype;
    }

    /**
     * @param doctype The doctype
     */
    public void setDoctype(String doctype) {
        this.doctype = doctype;
    }

    @Override
    public int describeContents() {
        return this.hashCode();

    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(id);
        dest.writeString(relation);
        dest.writeInt(relationId);
        dest.writeString(document);
        dest.writeString(doctype);
    }

    private Document(Parcel in) {
        id = in.readInt();
        relation = in.readString();
        relationId = in.readInt();
        document = in.readString();
        doctype = in.readString();
    }

    public static final Creator<Document> CREATOR = new Creator<Document>() {
        public Document createFromParcel(Parcel in) {
            return new Document(in);
        }

        public Document[] newArray(int size) {
            return new Document[size];
        }
    };

}
