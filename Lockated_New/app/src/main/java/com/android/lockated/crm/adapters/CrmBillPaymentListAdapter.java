package com.android.lockated.crm.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.lockated.R;


public class CrmBillPaymentListAdapter extends BaseAdapter {

    Context context;
    String[] list;
    LayoutInflater layoutInflater;

    public CrmBillPaymentListAdapter(Context context, String[] list){
        this.context = context;
        this.list = list;
        layoutInflater = LayoutInflater.from(context);
    }

    @Override
    public int getCount() {
        return list.length;
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        ViewHolder viewHolder;
        if(convertView == null) {
            convertView = layoutInflater.inflate(R.layout.bill_payment_adapter, parent, false);
            viewHolder = new ViewHolder();
            viewHolder.imageView = (ImageView) convertView.findViewById(R.id.imageName);
            viewHolder.textView = (TextView) convertView.findViewById(R.id.textItem);
            convertView.setTag(viewHolder);
        }else{
            viewHolder = (ViewHolder) convertView.getTag();
        }
        viewHolder.imageView.setImageResource(R.drawable.ic_account);
        viewHolder.textView.setText(list[position]);

        return convertView;
    }

    class ViewHolder{
        ImageView imageView;
        TextView textView;
    }

}
