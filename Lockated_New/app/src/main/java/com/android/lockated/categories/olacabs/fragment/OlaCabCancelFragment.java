package com.android.lockated.categories.olacabs.fragment;


import android.Manifest;
import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.widget.SlidingPaneLayout;
import android.support.v7.app.AlertDialog;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.android.lockated.R;
import com.android.lockated.categories.olacabs.activity.Constants;
import com.android.lockated.categories.olacabs.activity.FetchAddressIntentService;
import com.android.lockated.categories.olacabs.activity.OlaMainActivity_Latest;
import com.android.lockated.categories.olacabs.adapter.OlaReasonAdapter;
import com.android.lockated.information.AccountController;
import com.android.lockated.model.AccountData;
import com.android.lockated.model.OlaCab.CancelResponse;
import com.android.lockated.model.OlaCab.OlaCabCancelReason.CancelReasons;
import com.android.lockated.model.OlaCab.OlaTrackResponse.TrackResponse;
import com.android.lockated.network.ConnectionDetector;
import com.android.lockated.preferences.LockatedPreferences;
import com.android.lockated.request.LockatedVolleyRequestQueue;
import com.android.lockated.utils.Utilities;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.google.gson.Gson;
import com.sothree.slidinguppanel.SlidingUpPanelLayout;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;


public class OlaCabCancelFragment extends android.support.v4.app.Fragment implements View.OnClickListener, Response.ErrorListener, Response.Listener<JSONObject> {

    private static final String REQUEST_TAG = "OLA CABS CANCEL";

    TextView txt_ola_cab_driver_name, txt_ola_cab_number, txt_ola_cab_model, txt_ola_cab_cancel;
    Button btn_call_ola_driver;
    private String access_token = null;
    private ProgressDialog mProgressDialog;
    private Context context;
    //private LayoutInflater layoutInflater;

    private int action_bar_height, status_bar_height, panel_height, window_height;

    public CancelReasons cancelReasons;
    public CancelResponse cancelResponse;

    private AccountController accountController;
    private ArrayList<AccountData> accountDataArrayList;
    private LinearLayout cancel_layout, half_cancel_layout;

    //private AlertDialog alertD;

    public TrackResponse trackResponse;
    private LockatedPreferences mLockatedPreferences;
    String BookingId, CabNumber, CabType, CarModel, Crn, DriverName, DriverNumber, ShareRideUrl, Surcharge, AccessToken, BookingDateTime;
    float DriverLat, DriverLng, UserLat, UserLng;
    int Eta;
    String reason;
    private static final int REQUEST_CALL = 11;
    private SlidingPaneLayout mLayout;

    private OlaReasonAdapter adapter;
    JSONObject confirm_jsonObject;

    private ScheduledExecutorService executor;
    //public AlertDialog alertD;

    public Context mContext;
    private String screenName = "OlaCabCancelFragment";

    public OlaCabCancelFragment() {
        // Required empty public constructor
    }

    @SuppressLint("LongLogTag")
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //---------------------------------------------------------------------------
        if (OlaMainActivity_Latest.mOlaDestinationCard.getVisibility() == View.VISIBLE) {
            OlaMainActivity_Latest.mOlaDestinationCard.setVisibility(View.GONE);
        }
        //-------------- Changes on 13-8-2016----------------------------------------

        mContext = getActivity();

/*        // Calculate ActionBar height
        TypedValue tv = new TypedValue();
        if (getActivity().getTheme().resolveAttribute(android.R.attr.actionBarSize, tv, true))
        {
            action_bar_height = TypedValue.complexToDimensionPixelSize(tv.data,getResources().getDisplayMetrics());
        }

        status_bar_height = getStatusBarHeight();

        FrameLayout.LayoutParams lp = new FrameLayout.LayoutParams(FrameLayout.LayoutParams.MATCH_PARENT,
                window_height-status_bar_height-panel_height-action_bar_height); // or set height to any fixed value you want

        Log.e("window_height",""+window_height);
        Log.e("status_bar_height",""+status_bar_height);
        Log.e("panel_height",""+panel_height);
        Log.e("action_bar_height",""+action_bar_height);
        int layout_marker_width = window_height-status_bar_height-panel_height-action_bar_height;

        Log.e("window_height-status_bar_height-panel_height-action_bar_height",""+layout_marker_width);
        OlaMainActivity_Latest.layout_marker.setLayoutParams(lp);*/
        //--------------------------------------------------------------------------
        OlaMainActivity_Latest.mLayout.setPanelState(SlidingUpPanelLayout.PanelState.COLLAPSED);
        //------please put check here it works correctly or not.---------------
        OlaMainActivity_Latest.mLayout.setEnabled(false);


        setPanelHeight();

       /* DisplayMetrics dm = new DisplayMetrics();
        getActivity().getWindowManager().getDefaultDisplay().getMetrics(dm);
        int height = dm.heightPixels;*/



        /*if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        OlaMainActivity_Latest.gmap.setMyLocationEnabled(false);*/

        //OlaMainActivity_Latest.gmap.setPadding(0, 0, 0, (int) (height / 2.5));
        //setPanelHeight();
        //---------------------------------------------------------------------
        //OlaMainActivity_Latest.location_button_new.setVisibility(View.GONE);//--------change on 19-06-2016
        OlaMainActivity_Latest.current_location_zoom.setVisibility(View.GONE);//------change on 16-09-2016
        OlaMainActivity_Latest.imageMarker.setVisibility(View.GONE);
        OlaMainActivity_Latest.edt_olaSourceLocation.setFocusable(false);
        OlaMainActivity_Latest.edt_olaSourceLocation.setClickable(false);
        OlaMainActivity_Latest.edt_olaSourceLocation.setEnabled(false);

        //---------------Changes on  10-8-2016----------------------------
        //OlaMainActivity_Latest.mOlaSourceLayout.setAlpha(0.8f);
        //----------------------------------------------------------------
        OlaMainActivity_Latest.edt_olaSourceLocation.setOnClickListener(null);
        OlaMainActivity_Latest.show_destination.setOnClickListener(null);

        if (OlaMainActivity_Latest.mGoogleApiClient != null) {
            Log.e("onCreateView of OlaCabCancelFragment ", "mGoogleApiClient != null" + "Yes");
            //LocationServices.FusedLocationApi.removeLocationUpdates(OlaMainActivity_Latest.mGoogleApiClient, (LocationListener) getActivity());
            if (OlaMainActivity_Latest.mGoogleApiClient.isConnected()) {
                Log.e("mGoogleApiClient is connected in OlaCabCancel", "True");
            } else {
                Log.e("mGoogleApiClient is connected in OlaCabCancel", "False");
            }
            OlaMainActivity_Latest.mGoogleApiClient.disconnect();

        } else {
            Log.e("onCreateView of OlaCabCancelFragment ", "mGoogleApiClient == null" + "Yes");
            //OlaMainActivity_Latest.mGoogleApiClient.connect(); //commented on 16-8-2016
        }

        Log.e("I am ", "onCreate of Cab Cancel");
        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

        mLockatedPreferences = new LockatedPreferences(getActivity());
        accountController = AccountController.getInstance();
        accountDataArrayList = accountController.getmAccountDataList();

        Log.e("Tesst log before is tpdetail -- ", "tpdetail");
        Log.e("tpdetails onCab", "" + mLockatedPreferences.getTpDetail());
        Log.e("Tesst log before iscabBooked -- ", "teestbefore");
        Log.e("iscabBooked onCab", "" + mLockatedPreferences.getIsCabBooked());
        Log.e("Tesst log after iscabBooked -- ", "teestafter");


        Log.e("Ola Token", "" + mLockatedPreferences.getOlaToken());
        AccessToken = mLockatedPreferences.getOlaToken();

        if (mLockatedPreferences.getIsCabBooked()) {

            //Log.e("Ola Token", "" + accountController.getmAccountDataList().get(0).getOlatoken());
            //AccessToken = accountController.getmAccountDataList().get(0).getOlatoken();

            Log.e("I am", "getIsBooked");
            String jsonString = mLockatedPreferences.getTpDetail();
            JSONObject jsonObject;
            try {
                jsonObject = new JSONObject(jsonString);
                BookingId = jsonObject.getString("booking_id");
                CabNumber = jsonObject.getString("cab_number");
                CabType = jsonObject.getString("cab_type");
                CarModel = jsonObject.getString("car_model");
                Crn = jsonObject.getString("crn");
                DriverName = jsonObject.getString("driver_name");
                DriverNumber = jsonObject.getString("driver_number");
                DriverLat = (float) jsonObject.getDouble("driver_lat");
                DriverLng = (float) jsonObject.getDouble("driver_lng");
                UserLat = (float) jsonObject.getDouble("user_lat");
                UserLng = (float) jsonObject.getDouble("user_lng");
                ShareRideUrl = jsonObject.getString("share_ride_url");
                Eta = jsonObject.getInt("eta");

                //OlaMainActivity_Latest.setDriverLocation(DriverLat,DriverLng);

                if (jsonObject.getString("surcharge_value") != null) {
                    Surcharge = jsonObject.getString("surcharge_value");
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }

        } else {
            if (savedInstanceState == null) {
                Bundle bundle = getArguments();
                Log.e("Bundle Arguments", "" + getArguments());

                if (bundle != null) {

                    BookingId = bundle.getString("booking_id");
                    CabNumber = bundle.getString("cab_number");
                    CabType = bundle.getString("cab_type");
                    CarModel = bundle.getString("car_model");
                    Crn = bundle.getString("crn");
                    DriverName = bundle.getString("driver_name");
                    DriverNumber = bundle.getString("driver_number");
                    DriverLat = bundle.getFloat("driver_lat");
                    DriverLng = bundle.getFloat("driver_lng");
                    UserLat = bundle.getFloat("user_lat");
                    UserLng = bundle.getFloat("user_lng");
                    ShareRideUrl = bundle.getString("share_ride_url");
                    Eta = bundle.getInt("eta");

                    //-------------  5-8-2016  3.07pm --------------------

                    //AccessToken = bundle.getString("AccessToken");

                    //----------------------------------------------------

                    if (bundle.getString("surcharge_value") != null)
                        Surcharge = bundle.getString("surcharge_value");
                }
            } else {
                BookingId = savedInstanceState.getString("booking_id");
                CabNumber = savedInstanceState.getString("cab_number");
                CabType = savedInstanceState.getString("cab_type");
                CarModel = savedInstanceState.getString("car_model");
                Crn = savedInstanceState.getString("crn");
                DriverName = savedInstanceState.getString("driver_name");
                DriverNumber = savedInstanceState.getString("driver_number");
                DriverLat = savedInstanceState.getFloat("driver_lat");
                DriverLng = savedInstanceState.getFloat("driver_lng");
                UserLat = savedInstanceState.getFloat("user_lat");
                UserLng = savedInstanceState.getFloat("user_lng");

                ShareRideUrl = savedInstanceState.getString("share_ride_url");
                Eta = savedInstanceState.getInt("eta");
                if (savedInstanceState.getString("surcharge_value") != null) {
                    Surcharge = savedInstanceState.getString("surcharge_value");
                }


                Log.e("BookingId", "" + savedInstanceState.getString("booking_id"));
                /*Log.e("CabNumber", "" + savedInstanceState.getString("cab_number"));
                Log.e("CabType", "" + savedInstanceState.getString("cab_type"));
                Log.e("CarModel", "" + savedInstanceState.getString("car_model"));
                Log.e("Crn", "" + savedInstanceState.getString("crn"));
                Log.e("DriverName", "" + savedInstanceState.getString("driver_name"));
                Log.e("DriverNumber", "" + savedInstanceState.getString("driver_number"));
                Log.e("DriverLat", "" + savedInstanceState.getFloat("driver_lat"));
                Log.e("DriverLng", "" + savedInstanceState.getFloat("driver_lng"));
                Log.e("UserLat", "" + savedInstanceState.getFloat("user_lat"));
                Log.e("UserLng", "" + savedInstanceState.getFloat("user_lng"));
                Log.e("ShareRideUrl", "" + savedInstanceState.getString("share_ride_url"));
                Log.e("Eta", "" + savedInstanceState.getInt("eta"));
                Log.e("Surcharge", "" + savedInstanceState.getString("surcharge_value"));*/

            }

            confirm_jsonObject = new JSONObject();
            try {
                confirm_jsonObject.put("booking_id", BookingId);
                confirm_jsonObject.put("cab_number", CabNumber);
                confirm_jsonObject.put("cab_type", CabType);
                confirm_jsonObject.put("car_model", CarModel);
                confirm_jsonObject.put("crn", Crn);
                confirm_jsonObject.put("driver_name", DriverName);
                confirm_jsonObject.put("driver_number", DriverNumber);
                confirm_jsonObject.put("driver_lat", DriverLat);
                confirm_jsonObject.put("driver_lng", DriverLng);
                confirm_jsonObject.put("user_lat", UserLat);
                confirm_jsonObject.put("user_lng", UserLng);
                confirm_jsonObject.put("drop_lat", mLockatedPreferences.getDstLat());
                confirm_jsonObject.put("drop_lng", mLockatedPreferences.getDstLng());

                confirm_jsonObject.put("share_ride_url", ShareRideUrl);
                confirm_jsonObject.put("eta", Eta);
                confirm_jsonObject.put("booking_status", "Booked");
                confirm_jsonObject.put("current_status", true);
                if (Surcharge != null) {
                    confirm_jsonObject.put("surcharge_value", Surcharge);
                }
                mLockatedPreferences.setIsCabBooked(true);
                mLockatedPreferences.setTpdetail(confirm_jsonObject.toString());
                sendBookResponseToServer(confirm_jsonObject);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        setHasOptionsMenu(true);
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        outState.putString("booking_id", BookingId);
        outState.putString("cab_number", CabNumber);
        outState.putString("cab_type", CabType);
        outState.putString("car_model", CarModel);
        outState.putString("crn", Crn);
        outState.putString("driver_name", DriverName);
        outState.putString("driver_number", DriverNumber);
        outState.putFloat("driver_lat", DriverLat);
        outState.putFloat("driver_lng", DriverLng);
        outState.putFloat("use", DriverLat);
        outState.putFloat("driver_lng", DriverLng);
        outState.putString("share_ride_url", ShareRideUrl);
        outState.putInt("eta", Eta);
        if (Surcharge != null) {
            outState.putString("surcharge_value", Surcharge);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        //this.layoutInflater = inflater;
        View view = inflater.inflate(R.layout.fragment_ola_cab_cancel, container, false);

        Utilities.ladooIntegration(getActivity(), screenName);
        Utilities.lockatedGoogleAnalytics(screenName, getString(R.string.visited), screenName);
        //-----------------------------------------------------------
        context = getContext().getApplicationContext();
        //---------------------------------------------------------------

        cancel_layout = (LinearLayout) view.findViewById(R.id.cancel_layout);
        half_cancel_layout = (LinearLayout) view.findViewById(R.id.half_cancel_layout);

        DisplayMetrics dm = new DisplayMetrics();
        getActivity().getWindowManager().getDefaultDisplay().getMetrics(dm);
        int height = dm.heightPixels;

        /*if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            //return TODO;
        }
        OlaMainActivity_Latest.gmap.setMyLocationEnabled(false);
        OlaMainActivity_Latest.gmap.setPadding(0, 0, 0, (int) (panel_height / 3)+5);*/

        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, (int) (height / 3));//2.5
        cancel_layout.setLayoutParams(layoutParams);

        //-----------------------------------------------------------
        OlaMainActivity_Latest.mLayout.setEnabled(false);
        getActivity().setTitle("Cancellation");

        Log.e("UserLat", "" + UserLat);
        Log.e("UserLng", "" + UserLng);
        Log.e("DriverLat", "" + DriverLat);
        Log.e("DriverLng", "" + DriverLng);

        executor = Executors.newScheduledThreadPool(1);
        executor.scheduleAtFixedRate(new Runnable() {
            @SuppressLint("LongLogTag")
            @Override
            public void run() {
                Log.e(" Inside:", "" + "Runnable Task");
                if (getActivity() != null) {
                    if (ConnectionDetector.isConnectedToInternet(getActivity())) {
                        //String url = "http://sandbox-t.olacabs.com/v1/bookings/track_ride?booking_id="+ BookingId;
                        String url = "https://devapi.olacabs.com/v1/bookings/track_ride?booking_id=" + BookingId;
                        Log.e("url of sendBookResponseToServer", "" + url);
                        LockatedVolleyRequestQueue lockatedVolleyRequestQueue = LockatedVolleyRequestQueue.getInstance(getActivity());
                        lockatedVolleyRequestQueue.sendPostRequestOlaCabsConfirmEx(REQUEST_TAG, Request.Method.GET, url, AccessToken, null, new Response.Listener<JSONObject>() {
                            @Override
                            public void onResponse(JSONObject response) {
                                Log.e("Response", response.toString());
                                Gson gson = new Gson();
                                try {
                                    if (response.has("message") && response.has("code")) {
                                        Log.e("Response", response.toString());
                                        Utilities.showToastMessage(getActivity(), response.getString("message"));
                                    } else if (response.has("booking_status")) {

                                        if (response.getString("booking_status").equals("CALL_DRIVER")) {
                                            Log.e("UserLat", "" + UserLat);
                                            Log.e("UserLng", "" + UserLng);
                                            Log.e("DriverLat", "" + DriverLat);
                                            Log.e("DriverLng", "" + DriverLng);

                                            Log.e("Response", response.toString());
                                            trackResponse = gson.fromJson(response.toString(), TrackResponse.class);

                                            Log.e("Driver Lat", "" + String.valueOf(trackResponse.getDriverLat()));
                                            Log.e("Driver_Lng", "" + String.valueOf(trackResponse.getDriverLng()));

                                            Location location = new Location("");
                                            location.setLatitude(UserLat);
                                            location.setLongitude(UserLng);
                                            //OlaMainActivity_Latest.startIntentServiceCab(location,getActivity());

                                            Intent intent = new Intent(getActivity(), FetchAddressIntentService.class);
                                            intent.putExtra(Constants.RECEIVER, OlaMainActivity_Latest.mResultReceiver);
                                            intent.putExtra(Constants.LOCATION_DATA_EXTRA, location);
                                            getActivity().startService(intent);

                                            OlaMainActivity_Latest.removeUserCurrentLocation();
                                            OlaMainActivity_Latest.setDriverLocation(UserLat, UserLng, trackResponse.getDriverLat(), trackResponse.getDriverLng(), true);

                                            if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                                                // TODO: Consider calling
                                                //    ActivityCompat#requestPermissions
                                                // here to request the missing permissions, and then overriding
                                                //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                                                //                                          int[] grantResults)
                                                // to handle the case where the user grants the permission. See the documentation
                                                // for ActivityCompat#requestPermissions for more details.
                                                return;
                                            }
                                            OlaMainActivity_Latest.gmap.setMyLocationEnabled(false);
                                            OlaMainActivity_Latest.gmap.setPadding(0, 0, 0, (int) (panel_height / 3)+5);
                                        }
                                        else if (response.getString("booking_status").equals("CLIENT_LOCATED"))
                                        {
                                            Log.e("UserLat",""+UserLat);
                                            Log.e("UserLng",""+UserLng);
                                            Log.e("DriverLat",""+DriverLat);
                                            Log.e("DriverLng",""+DriverLng);

                                            Log.e("Response",response.toString());
                                            trackResponse = gson.fromJson(response.toString(), TrackResponse.class);

                                            Log.e("Driver Lat", ""+String.valueOf(trackResponse.getDriverLat()));
                                            Log.e("Driver_Lng", ""+String.valueOf(trackResponse.getDriverLng()));

                                            Location location = new Location("");
                                            location.setLatitude(UserLat);
                                            location.setLongitude(UserLng);


                                            Intent intent = new Intent(getActivity(),FetchAddressIntentService.class);
                                            intent.putExtra(Constants.RECEIVER, OlaMainActivity_Latest.mResultReceiver);
                                            intent.putExtra(Constants.LOCATION_DATA_EXTRA, location);
                                            getActivity().startService(intent);

                                            OlaMainActivity_Latest.removeUserCurrentLocation();
                                            OlaMainActivity_Latest.setDriverLocation(UserLat,UserLng,trackResponse.getDriverLat(),trackResponse.getDriverLng(),true);
                                            OlaMainActivity_Latest.gmap.setMyLocationEnabled(false);
                                            OlaMainActivity_Latest.gmap.setPadding(0, 0, 0, (int) (panel_height / 3)+5);
                                        }
                                        else if (response.getString("booking_status").equals("IN_PROGRESS"))
                                        {
                                            Log.e("UserLat",""+UserLat);
                                            Log.e("UserLng",""+UserLng);
                                            Log.e("DriverLat",""+DriverLat);
                                            Log.e("DriverLng",""+DriverLng);

                                            Log.e("Response",response.toString());
                                            trackResponse = gson.fromJson(response.toString(), TrackResponse.class);

                                            half_cancel_layout.setVisibility(View.GONE);
                                            Log.e("Driver Lat", ""+String.valueOf(trackResponse.getDriverLat()));
                                            Log.e("Driver_Lng", ""+String.valueOf(trackResponse.getDriverLng()));

                                            Location location = new Location("");
                                            location.setLatitude(UserLat);
                                            location.setLongitude(UserLng);
//----------------------Error on BackPress----------------------------------
                                            Intent intent = new Intent(getActivity(),FetchAddressIntentService.class);//-----------------------------------
                                            intent.putExtra(Constants.RECEIVER, OlaMainActivity_Latest.mResultReceiver);
                                            intent.putExtra(Constants.LOCATION_DATA_EXTRA, location);
                                            getActivity().startService(intent);

                                            DisplayMetrics dm = new DisplayMetrics();
                                            getActivity().getWindowManager().getDefaultDisplay().getMetrics(dm);
                                            int height = dm.heightPixels;

                                            //OlaMainActivity_Latest.hideUsermarker();
                                            txt_ola_cab_cancel.setVisibility(View.INVISIBLE);
                                            OlaMainActivity_Latest.mLayout.setPanelHeight((int) (height/3)/2);
                                            OlaMainActivity_Latest.gmap.setMyLocationEnabled(false);
                                            OlaMainActivity_Latest.gmap.setPadding(0,0,0, (int) ((height/3)/2)+5);
                                            OlaMainActivity_Latest.removeUserCurrentLocation();
                                            OlaMainActivity_Latest.setNewDriverLocation(trackResponse.getDriverLat(),trackResponse.getDriverLng());
                                        }
                                        else if (response.getString("booking_status").equals("COMPLETED")) {
                                            executor.shutdown();
                                            executor.shutdownNow();

                                            Log.e("Response", response.toString());
                                            trackResponse = gson.fromJson(response.toString(), TrackResponse.class);

                                            Log.e("Driver Lat", "" + String.valueOf(trackResponse.getDriverLat()));
                                            Log.e("Driver_Lng", "" + String.valueOf(trackResponse.getDriverLng()));

                                            //OlaMainActivity_Latest.setDriverLocation(UserLat,UserLng,trackResponse.getDriverLat(),trackResponse.getDriverLng(),true);
                                            //sendTrackResponseToServer(response.put("current_status",false));
                                            response.put("current_status", false);
                                            response.put("drop_lat",mLockatedPreferences.getDstLat());
                                            response.put("drop_lng",mLockatedPreferences.getDstLng());
                                            sendBookResponseToServer(response);

                                            //----------------------------------------------------------------------

                                            final Context alertContext = getActivity();
                                            LayoutInflater layoutInflater = LayoutInflater.from(getActivity());//context
                                            // LayoutInflater layoutInflater = (LayoutInflater)getContext().getApplicationContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                                            View promptView = layoutInflater.inflate(R.layout.cancel_cab_alert, null);

                                            final AlertDialog alertD = new AlertDialog.Builder(getActivity()).create();
                                            TextView cancel_response = (TextView) promptView.findViewById(R.id.txt_cancel_response);
                                            //cancel_response.setText("Thank you for riding with us, your ride is completed." + "Your bill amount is "+getActivity().getResources().getString(R.string.rupees_symbol)+" "+trackResponse.getTripInfo().getAmount());
                                            cancel_response.setText(getActivity().getResources().getString(R.string.text_ride_completed) + " " + trackResponse.getTripInfo().getAmount());
                                            TextView cancel_ok = (TextView) promptView.findViewById(R.id.btn_cancel_ok);

                                            alertD.setView(promptView);

                                            //alertD = new AlertDialog.Builder(getActivity()).create();

                                            cancel_ok.setOnClickListener(new View.OnClickListener() {
                                                public void onClick(View v) {
                                                    alertD.dismiss();
                                                    mLockatedPreferences.setIsCabBooked(false);
                                                    mLockatedPreferences.setTpdetail("");
                                                    mLockatedPreferences.setDstLat(0);
                                                    mLockatedPreferences.setDstLng(0);
                                                    Intent intent = new Intent(mContext, OlaMainActivity_Latest.class);
                                                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                                    startActivity(intent);
                                                    getActivity().finish();
                                                }
                                            });
                                            if (!getActivity().isFinishing()) {
                                                alertD.show();
                                            }
                                            alertD.setCanceledOnTouchOutside(false);
                                                      executor.shutdownNow();
                                            //------------------------------------------------------
                                        }
                                        else if (response.getString("booking_status").equals("BOOKING_CANCELLED"))
                                        {
                                            executor.shutdown();

                                            Log.e("UserLat",""+UserLat);
                                            Log.e("UserLng",""+UserLng);
                                            Log.e("DriverLat",""+DriverLat);
                                            Log.e("DriverLng",""+DriverLng);

                                            Log.e("Response",response.toString());
                                            response.put("current_status",false);
                                            response.put("booking_id",BookingId);
                                            response.put("drop_lat",mLockatedPreferences.getDstLat());
                                            response.put("drop_lng",mLockatedPreferences.getDstLng());
                                            Log.e("Response",response.toString());

                                            trackResponse = gson.fromJson(response.toString(), TrackResponse.class);
                                            ///OlaMainActivity_Latest.setDriverLocation(trackResponse.getDriverLat(),trackResponse.getDriverLng());
                                            sendBookResponseToServer(response);

                                            //----------------------------------------------------------------------
                                        }
                                        else if (response.getString("reason").equals("BOOKING_ALREADY_CANCELLED"))
                                        {
                                            Log.e("Response",response.toString());
                                            trackResponse = gson.fromJson(response.toString(), TrackResponse.class);
                                            //OlaMainActivity_Latest.setDriverLocation(trackResponse.getDriverLat(),trackResponse.getDriverLng());
                                        }

                                    }
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }
                        }, new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                Log.e("Error Response",error.toString());
                            }
                        });
                    } else {
                        Utilities.showToastMessage(getActivity(), getActivity().getResources().getString(R.string.internet_connection_error));
                    }
                }
            }
        }, 0, 2 * 1000, TimeUnit.MILLISECONDS);
        init(view);
        getCancelReason(AccessToken);
        return view;
    }

    public void init(View view) {


        OlaMainActivity_Latest.mLayout.setEnabled(false);
        if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }

        //OlaMainActivity_Latest.gmap.setMyLocationEnabled(true);
        //OlaMainActivity_Latest.gmap.getUiSettings().setMyLocationButtonEnabled(false);
        //OlaMainActivity_Latest.location_button.setVisibility(View.GONE);
        //OlaMainActivity_Latest.mLayout.setCoveredFadeColor(Color.WHITE);


        //OlaMainActivity_Latest.mLayout.setPanelHeight(180);
        //OlaMainActivity_Latest.mLayout.setPanelHeight(180);


        btn_call_ola_driver = (Button) view.findViewById(R.id.btn_call_ola_driver);

        txt_ola_cab_driver_name = (TextView) view.findViewById(R.id.txt_ola_cab_driver_name);
        txt_ola_cab_number = (TextView) view.findViewById(R.id.txt_ola_cab_number);
        txt_ola_cab_model = (TextView) view.findViewById(R.id.txt_ola_cab_model);
        txt_ola_cab_cancel = (TextView) view.findViewById(R.id.txt_ola_cab_cancel);

        Log.e("DriverName ","" + DriverName);
        Log.e("CabNumber ","" + CabNumber);
        Log.e("CabType ","" + CabType);

        txt_ola_cab_driver_name.setText(DriverName);
        txt_ola_cab_number.setText(CabNumber);
        txt_ola_cab_model.setText(CarModel);

        txt_ola_cab_cancel.setOnClickListener(this);
        btn_call_ola_driver.setOnClickListener(this);

    }


    public void sendCabCancel() {
        try {
            Log.e("sendCabCancel", "m inside");
            Log.e("reason", reason);
            if (ConnectionDetector.isConnectedToInternet(getActivity())) {

                mProgressDialog = ProgressDialog.show(getActivity(), "", "Please Wait...", false);
                //String url = "http://sandbox-t.olacabs.com/v1/bookings/cancel";
                String url = "https://devapi.olacabs.com/v1/bookings/cancel";

                JSONObject jsonObject = new JSONObject();
                jsonObject.put("booking_id", BookingId);
                jsonObject.put("reason", reason);

                Log.e("Request JsonObject", "" + jsonObject.toString());

                LockatedVolleyRequestQueue mLockatedVolleyRequestQueue = LockatedVolleyRequestQueue.getInstance(getActivity());
                mLockatedVolleyRequestQueue.sendPostRequestOlaCabsConfirmEx(REQUEST_TAG, Request.Method.POST, url, AccessToken, jsonObject, this,this);

            } else {
                Utilities.showToastMessage(getActivity(), getActivity().getResources().getString(R.string.internet_connection_error));
            }

        } catch (Exception e) {
            e.getMessage();
        }
    }

    @Override
    public void onResume()
    {super.onResume();
    }


    public void getCancelReason(String access_token) {
        if (getActivity() != null) {
            if (ConnectionDetector.isConnectedToInternet(getActivity())) {
                //String url = "http://sandbox-t.olacabs.com/v1/bookings/cancel/reasons";
                String url = "https://devapi.olacabs.com/v1/bookings/cancel/reasons";
                Log.e("getCancelReason", "" + url);

                LockatedVolleyRequestQueue mLockatedVolleyRequestQueue = LockatedVolleyRequestQueue.getInstance(getActivity());
                mLockatedVolleyRequestQueue.sendPostRequestOlaCabsConfirmEx(REQUEST_TAG, Request.Method.GET, url, access_token,null,new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.e("Cancel Response", "" + response);
                        if (response != null && response.length() > 0) {
                            try {
                                if (response.has("message") && response.has("code")) {
                                    Utilities.showToastMessage(getActivity(), response.getString("message"));
                                }
                                else if(response.has("cancel_reasons"))
                                {
                                    Gson gson = new Gson();
                                    cancelReasons = gson.fromJson(response.get("cancel_reasons").toString(), CancelReasons.class);

                                    for ( int i =0 ; i<cancelReasons.getAuto().size();i++) {
                                        Log.e("Cancel Reason", ""+cancelReasons.getAuto().get(i));
                                    }
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    }
                }, this);
            } else {
                Utilities.showToastMessage(getActivity(), getActivity().getResources().getString(R.string.internet_connection_error));
            }
        }
    }


    @Override
    public void onClick(View v) {
        switch (v.getId())
        {
            case R.id.txt_ola_cab_cancel:
            {
                getReasonAlert(CabType);
                break;
            }

            case R.id.btn_call_ola_driver:
            {
                onCallUsClicked();
                break;
            }
        }

    }

    /* @Override
    public void onPause()
    {
        super.onPause();
        Log.e("on Cancel cab", "paused");
        executor.shutdown();
        executor.shutdownNow();
    }*/

    @Override
    public void onErrorResponse(VolleyError error) {
        Log.e("Error Response",error.toString());
    }

    @SuppressLint("LongLogTag")
    @Override
    public void onResponse(JSONObject response) {
        Log.e("Response",""+response);
        if (response != null && response.length() > 0) {
            try {
                if (response.has("message") && response.has("code")) {
                    Utilities.showToastMessage(getActivity(), response.getString("message"));
                    Log.e("Response",response.toString());
                }
                else if (response.has("request_type") && response.has("status"))
                {
                    Gson gson = new Gson();
                    cancelResponse = gson.fromJson(response.toString(), CancelResponse.class);

                    Log.e("request_type ", "" + cancelResponse.getRequestType());
                    Log.e("status ", "" + cancelResponse.getStatus());
                    Log.e("text ", "" + cancelResponse.getText());
                    Log.e("header ", "" + cancelResponse.getHeader());

                    response.put("booking_status","BOOKING_CANCELLED");
                    response.put("current_status",false);
                    response.put("booking_id",BookingId);
                    Log.e("Response",response.toString());

                    sendBookResponseToServer(response);
                    //Utilities.showToastMessage(getActivity(),""+cancelResponse.getText());
                }
                else if(response.has("deliverylabel"))
                {
                    if (response.getString("deliverylabel").equals("BOOKING_CANCELLED")) {
                        Log.e("mLockatedPreferences.getIsCabBooked  Before setIsBooked", String.valueOf(mLockatedPreferences.getIsCabBooked()));

                        mProgressDialog.dismiss();
                        mLockatedPreferences.setTpdetail("");
                        mLockatedPreferences.setIsCabBooked(false);
                        mLockatedPreferences.setDstLat(0);
                        mLockatedPreferences.setDstLng(0);

                        /*Log.e("mLockatedPreferences.getIsCabBooked  After setIsBooked", String.valueOf(mLockatedPreferences.getIsCabBooked()));
                        if (OlaMainActivity_Latest.mGoogleApiClient.isConnected()) {
                            Log.e("OlaMainActivity_Latest.mGoogleApiClient.isConnected","Yes");
                        }
                        else {
                            Log.e("OlaMainActivity_Latest.mGoogleApiClient.isConnected","No");
                            OlaMainActivity_Latest.mGoogleApiClient.connect();
                        }

                        if (OlaMainActivity_Latest.mGoogleApiClient != null) {
                            OlaMainActivity_Latest.mGoogleApiClient.connect();
                            Log.e("OlaMainActivity_Latest.mGoogleApiClient != null", "Yes");
                            Log.e("Made m GoogleApiCliet Connected","True");
                        }
                        else
                        {
                            Log.e("OlaMainActivity_Latest.mGoogleApiClient == null", "Yes");
                        }*/


                        //OlaMainActivity_Latest.imageMarker.setVisibility(View.VISIBLE);
                        //OlaMainActivity_Latest.gmap.getUiSettings().setMyLocationButtonEnabled(false);
                        //OlaMainActivity_Latest.removeUserCurrentLocation();

                        //--- Cancel Alert----------------------------------------------
                        final Context alertContext = getActivity();
                        LayoutInflater layoutInflater = LayoutInflater.from(mContext);//context
                        //LayoutInflater layoutInflater = (LayoutInflater)getContext().getApplicationContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                        View promptView = layoutInflater.inflate(R.layout.cancel_cab_alert, null);
                        final AlertDialog alertD = new AlertDialog.Builder(mContext).create();
                        //alertD = new AlertDialog.Builder(getActivity()).create();

                        TextView cancel_response = (TextView) promptView.findViewById(R.id.txt_cancel_response);
                        cancel_response.setText("Your booking has been cancelled");
                        TextView cancel_ok = (TextView) promptView.findViewById(R.id.btn_cancel_ok);
                        alertD.setView(promptView);


                        cancel_ok.setOnClickListener(new View.OnClickListener() {
                            public void onClick(View v) {

                               /* mLockatedPreferences.setIsCabBooked(false);
                                mLockatedPreferences.setTpdetail("");*/
                                alertD.dismiss();
                                executor.shutdown();
                                executor.shutdownNow();
                                Intent intent = new Intent(mContext, OlaMainActivity_Latest.class);
                                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                startActivity(intent);
                                getActivity().finish();
                            }
                        });
                        if (!getActivity().isFinishing()) {
                            alertD.show();
                        }
                        alertD.setCanceledOnTouchOutside(true);
                        //--------------------------------------
                    }
                }
                /*else if(!response.has("thirdparty") && response.has("tpdetails"))
                {
                    BookingId = response.getJSONObject("tpdetails").getString("booking_id");
                    CabNumber = response.getJSONObject("tpdetails").getString("cab_number");
                    CabType = response.getJSONObject("tpdetails").getString("cab_type");
                    CarModel = response.getJSONObject("tpdetails").getString("car_model");
                    Crn = response.getJSONObject("tpdetails").getString("crn");
                    DriverName = response.getJSONObject("tpdetails").getString("driver_name");
                    DriverNumber = response.getJSONObject("tpdetails").getString("driver_number");
                    DriverLat = (float) response.getJSONObject("tpdetails").getDouble("driver_lat");
                    DriverLng = (float) response.getJSONObject("tpdetails").getDouble("driver_lng");
                    ShareRideUrl = response.getJSONObject("tpdetails").getString("share_ride_url");
                    Eta = response.getJSONObject("tpdetails").getInt("eta");
                    if (response.getJSONObject("tpdetails").getString("surcharge_value") != null) {
                        Surcharge = response.getJSONObject("tpdetails").getString("surcharge_value");
                    }
                }*/
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    private void getReasonAlert(String cabType) {

        Type type = Type.valueOf(cabType); // surround with try/catch
        ArrayList<String> strings = new ArrayList<>() ;;
        switch(type) {
            case mini:
                strings = cancelReasons.getMini();
                Log.e("strings",""+strings);
                adapter = new OlaReasonAdapter(getActivity(),R.layout.layout_each_reason,cancelReasons.getMini());
                break;
            case micro:
                strings = cancelReasons.getMicro();
                Log.e("strings",""+strings);
                adapter = new OlaReasonAdapter(getActivity(), R.layout.layout_each_reason, cancelReasons.getMicro());
                break;
            case prime:
                strings = cancelReasons.getPrime();
                Log.e("strings",""+strings);
                adapter = new OlaReasonAdapter(getActivity(),R.layout.layout_each_reason,cancelReasons.getPrime());
                break;
            case sedan:
                strings = cancelReasons.getSedan();
                Log.e("strings",""+strings);
                adapter = new OlaReasonAdapter(getActivity(),R.layout.layout_each_reason,cancelReasons.getSedan());
                break;
            case auto:
                strings = cancelReasons.getAuto();
                Log.e("strings",""+strings);
                adapter = new OlaReasonAdapter(getActivity(),R.layout.layout_each_reason,cancelReasons.getAuto());
                break;
        }
        final AlertDialog.Builder dialog = new AlertDialog.Builder(getActivity());
        View view = getActivity().getLayoutInflater().inflate( R.layout.reason_layout, null );
        dialog.setTitle("Select Reason of Cancel");
        dialog.setView(view);
        final AlertDialog alertDialog = dialog.show();

        ListView listView = (ListView)view.findViewById(R.id.listView_reason);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                reason = parent.getItemAtPosition(position).toString();
                Log.e("Reason",""+reason);
                sendCabCancel();
                alertDialog.dismiss();
            }
        });
        listView.setAdapter(adapter);

    }

    @SuppressLint("LongLogTag")
    private void sendBookResponseToServer(JSONObject jsonObject)
    {
        Log.e(" Inside:",""+"sendBookResponseToServer");
        if (getActivity() != null) {
            if (ConnectionDetector.isConnectedToInternet(getActivity())) {
                String url = "https://www.lockated.com/add_ola_orders.json?token="+mLockatedPreferences.getLockatedToken();
                Log.w("url of sendBookResponseToServer",""+url);
                Log.e("sendBookResponseToServer json",""+jsonObject);
                LockatedVolleyRequestQueue lockatedVolleyRequestQueue = LockatedVolleyRequestQueue.getInstance(getActivity());
                lockatedVolleyRequestQueue.sendPostRequestLocated(REQUEST_TAG, Request.Method.POST, url,jsonObject, this, this);
                //lockatedVolleyRequestQueue.sendPostRequestOlaCabsConfirmEx(REQUEST_TAG, Request.Method.POST, url,jsonObject, this, this);
            } else {
                Utilities.showToastMessage(getActivity(), getActivity().getResources().getString(R.string.internet_connection_error));
            }
        }
    }

    @SuppressLint("LongLogTag")
    private void sendTrackResponseToServer(JSONObject jsonObject)
    {
        Log.e(" Inside:",""+"sendTrackResponseToServer");
        if (getActivity() != null) {
            if (ConnectionDetector.isConnectedToInternet(getActivity())) {
                String url = "http://sandbox-t.olacabs.com/v1/bookings/track_ride?booking_id="+BookingId;
                Log.e("url of sendTrackResponseToServer",""+url);
                LockatedVolleyRequestQueue lockatedVolleyRequestQueue = LockatedVolleyRequestQueue.getInstance(getActivity());
                lockatedVolleyRequestQueue.sendPostRequestOlaCabsConfirmEx(REQUEST_TAG, Request.Method.POST, url,AccessToken,jsonObject, this, this);
            } else {
                Utilities.showToastMessage(getActivity(), getActivity().getResources().getString(R.string.internet_connection_error));
            }
        }
    }

    private  enum Type
    {
        mini,micro,sedan,prime,auto
    }

    private void onCallUsClicked() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                requestPermissions(new String[]{Manifest.permission.CALL_PHONE}, REQUEST_CALL);
            } else {
                callingIntent();
            }
        } else {
            callingIntent();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == REQUEST_CALL) {
            if (grantResults.length == 1 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                callingIntent();
            }
        }
    }

    private void callingIntent() {
        Intent intent = new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + DriverNumber));
        if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        startActivity(intent);
    }

    public int getStatusBarHeight() {
        int result = 0;
        int resourceId = getResources().getIdentifier("status_bar_height", "dimen", "android");
        if (resourceId > 0) {
            result = getResources().getDimensionPixelSize(resourceId);
        }
        return result;
    }

    private void setPanelHeight() {
        DisplayMetrics dm = new DisplayMetrics();
        getActivity().getWindowManager().getDefaultDisplay().getMetrics(dm);
        window_height = dm.heightPixels;
        OlaMainActivity_Latest.mLayout.setPanelHeight((int) (window_height / 3));
        panel_height = (int) (window_height / 3);
    }
}
