package com.android.lockated.crmadmin.fragment.adminNotices;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.RelativeLayout;

import com.android.lockated.R;
import com.android.lockated.crm.adapters.CommanListViewAdapter;
import com.android.lockated.crm.adapters.CrmNoticeBoardListAdapter;
import com.android.lockated.crmadmin.activity.AdminNoticeBoardActivity;
import com.android.lockated.utils.Utilities;


public class AdminNoticeBoardFragment extends Fragment implements AdapterView.OnItemClickListener {

    ListView noticeBoardList;
    RelativeLayout relativeLayout;
    CommanListViewAdapter commanListViewAdapter;
    String[] nameList = {"Pending Notice", "Published Notice"};

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        Utilities.ladooIntegration(getActivity(), "Admin Notice List");
        View noticeBoardView = inflater.inflate(R.layout.fragment_notice_board_admin, container, false);
        relativeLayout = (RelativeLayout) noticeBoardView.findViewById(R.id.relativeMain);
        noticeBoardList = (ListView) noticeBoardView.findViewById(R.id.boardList);
        commanListViewAdapter = new CommanListViewAdapter(getActivity(), nameList);
        noticeBoardList.setAdapter(commanListViewAdapter);
        noticeBoardList.setOnItemClickListener(this);

        return noticeBoardView;
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

        Intent intent = new Intent(getActivity(), AdminNoticeBoardActivity.class);
        intent.putExtra("AdminNotice", position);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);

    }

}
