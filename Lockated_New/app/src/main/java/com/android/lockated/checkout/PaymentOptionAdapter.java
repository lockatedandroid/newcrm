package com.android.lockated.checkout;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.android.lockated.Interfaces.IRecyclerItemClickListener;
import com.android.lockated.R;
import com.android.lockated.holder.RecyclerViewHolder;

import java.util.ArrayList;

public class PaymentOptionAdapter extends RecyclerView.Adapter<RecyclerViewHolder> {
    ArrayList<String> mPaymentData;
    ArrayList<Integer> mIcons;
    private IRecyclerItemClickListener mIRecyclerItemClickListener;

    public PaymentOptionAdapter(ArrayList<String> data, ArrayList<Integer> mPaymentIcons, IRecyclerItemClickListener listener)
    {
        mPaymentData = data;
        mIcons = mPaymentIcons;
        mIRecyclerItemClickListener = listener;
    }

    @Override
    public RecyclerViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.fragment_payment_option, parent, false);
        return new RecyclerViewHolder(itemView, mIRecyclerItemClickListener);
    }

    @Override
    public void onBindViewHolder(RecyclerViewHolder holder, int position)
    {
        holder.paymentOptionViewHolder(mPaymentData.get(position),mIcons.get(position));
       // holder.paymentOptionViewHolder(mPaymentData.get(position));
    }

    @Override
    public int getItemCount() {
        return mPaymentData.size();
    }
}
