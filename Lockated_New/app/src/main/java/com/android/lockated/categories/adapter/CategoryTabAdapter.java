package com.android.lockated.categories.adapter;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.android.lockated.R;
import com.android.lockated.categories.CategoryItemViewFragment;
import com.android.lockated.landing.model.TaxonomiesData;
import com.android.lockated.preferences.LockatedPreferences;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class CategoryTabAdapter extends FragmentPagerAdapter {
    private ArrayList<TaxonomiesData> mTaxonomiesData;
    int mPosition;
    LockatedPreferences mLockatedPreferences;
    boolean isUserApproved;
    int taxon_id, otype_id;

    public CategoryTabAdapter(Context context, FragmentManager fm, ArrayList<TaxonomiesData> tabs, int mPosition, int taxon_id, int otype_id) {
        super(fm);
        mTaxonomiesData = tabs;
        this.mPosition = mPosition;
        mLockatedPreferences = new LockatedPreferences(context);
        isUserApproved = mLockatedPreferences.getAccountData().is_approve();
        this.taxon_id = taxon_id;
        this.otype_id = otype_id;
    }

    @Override
    public Fragment getItem(int position) {
        int resource = R.drawable.ic_reminder;
        switch (position) {
            case 0:
                resource = R.drawable.ic_beauty;
                break;
            case 1:
                resource = R.drawable.ic_grocery;
                break;
            case 2:
                resource = R.drawable.ic_food;
                break;
            case 3:
                resource = R.drawable.ic_laundry;
                break;
            case 4:
                resource = R.drawable.ic_medicine;
                break;
            case 5:
                resource = R.drawable.ic_household;
                break;
            case 6:
                resource = R.drawable.ic_repair_maint;
                break;
            case 7:
                resource = R.drawable.ic_misc;
                break;
            case 8:
                resource = R.drawable.ic_reminder;
                break;
            case 9:
                resource = R.drawable.mysociety;
                break;
            case 10:
                resource = R.drawable.admin;
        }

        CategoryItemViewFragment categoryViewFragment = new CategoryItemViewFragment();
        String nameGrocery = "blank";
        if (mTaxonomiesData.get(position).getName().equals("Grocery")) {
            nameGrocery = mTaxonomiesData.get(position).getName();
        } else {
            nameGrocery = mTaxonomiesData.get(position).getName();
        }
        Bundle bundle = new Bundle();
        bundle.putInt("currentTapPosition", position);
        categoryViewFragment.setArguments(bundle);
        categoryViewFragment.setTaxonData(mTaxonomiesData.get(position).getmTaxonDatas(), resource,
                nameGrocery, mPosition, nameGrocery, taxon_id, otype_id);
        return categoryViewFragment;
    }

    @Override
    public int getItemPosition(Object object) {
        return super.getItemPosition(object);
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return mTaxonomiesData.get(position).getName();
    }

    @Override
    public long getItemId(int position) {
        return super.getItemId(position);
    }

    @Override
    public int getCount() {

        String mySocietyRoles = getMySocietyRoles();
        if (!mLockatedPreferences.getSocietyId().equals("blank")) {
            if (isUserApproved && !mySocietyRoles.equals("blank")) {
                /*if (mTaxonomiesData.size() == 9) {
                    return mTaxonomiesData.size();
                } else if (mTaxonomiesData.size() == 10) {
                    return mTaxonomiesData.size() - 1;
                } else if (mTaxonomiesData.size() == 11) {
                    return mTaxonomiesData.size() - 2;
                } else {
                    return mTaxonomiesData.size() - 3;
                }*/
                return mTaxonomiesData.size() - (mTaxonomiesData.size() - 9);
            } else {
                /*if (mTaxonomiesData.size() == 9) {
                    return mTaxonomiesData.size();
                } else if (mTaxonomiesData.size() == 10) {
                    return mTaxonomiesData.size() - 1;
                } else {
                    return mTaxonomiesData.size() - 1;
                }*/
                return mTaxonomiesData.size() - (mTaxonomiesData.size() - 9);
            }
        } else {
            /*if (mTaxonomiesData.size() == 9) {
                return mTaxonomiesData.size();
            } else {
                return mTaxonomiesData.size() - 1;
            }*/
            return mTaxonomiesData.size() - (mTaxonomiesData.size() - 9);
        }
    }

    public String getMySocietyRoles() {
        String mySocietyRoles = "blank";
        if (mLockatedPreferences.getRolesJson() != null && !mLockatedPreferences.getRolesJson().equals("blank")) {
            try {
                JSONObject noticeJsonObj = new JSONObject(mLockatedPreferences.getRolesJson());
                for (int i = 0; i < noticeJsonObj.getJSONArray("permissions").length(); i++) {
                    JSONObject jsonObject1 = noticeJsonObj.getJSONArray("permissions").getJSONObject(i);
                    if (jsonObject1.has("section") && jsonObject1.getString("section").equals("spree_mysociety")) {
                        if (jsonObject1.getJSONObject("permission").has("index")) {
                            mySocietyRoles = jsonObject1.getJSONObject("permission").getString("index");
                        }
                    }
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return mySocietyRoles;
    }
}
