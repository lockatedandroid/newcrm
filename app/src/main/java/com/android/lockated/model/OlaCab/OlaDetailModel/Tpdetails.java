
package com.android.lockated.model.OlaCab.OlaDetailModel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class Tpdetails {


    @SerializedName("date")
    @Expose
    private String date;

    @SerializedName("share_ride_url")
    @Expose
    private String shareRideUrl;
    @SerializedName("eta")
    @Expose
    private int eta;
    @SerializedName("driver_number")
    @Expose
    private String driverNumber;

    @SerializedName("driver_lng")
    @Expose
    private float driverLng;
    @SerializedName("driver_name")
    @Expose
    private String driverName;
    @SerializedName("car_model")
    @Expose
    private String carModel;
    @SerializedName("crn")
    @Expose
    private String crn;
    @SerializedName("cab_number")
    @Expose
    private String cabNumber;
    @SerializedName("driver_lat")
    @Expose
    private float driverLat;
    @SerializedName("cab_type")
    @Expose
    private String cabType;

    @SerializedName("text")
    @Expose
    private String text;

    @SerializedName("header")
    @Expose
    private String header;

    @SerializedName("ola_money_balance")
    @Expose
    private int olaMoneyBalance;
    @SerializedName("trip_info")
    @Expose
    private TripInfo tripInfo;



    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("request_type")
    @Expose
    private String requestType;
    @SerializedName("booking_status")
    @Expose
    private String bookingStatus;
    @SerializedName("booking_id")
    @Expose
    private String bookingId;



    /**
     *
     * @return
     *     The eta
     */
    public String getDate() {
        return date;
    }

    /**
     *
     * @param date
     *     The eta
     */
    public void setDate(String date) {
        this.date = date;
    }

    /**
     *
     * @return
     *     The status
     */
    public String getStatus() {
        return status;
    }

    /**
     *
     * @param status
     *     The status
     */
    public void setStatus(String status) {
        this.status = status;
    }

    /**
     *
     * @return
     *     The requestType
     */
    public String getRequestType() {
        return requestType;
    }

    /**
     *
     * @param requestType
     *     The request_type
     */
    public void setRequestType(String requestType) {
        this.requestType = requestType;
    }

    /**
     *
     * @return
     *     The bookingStatus
     */
    public String getBookingStatus() {
        return bookingStatus;
    }

    /**
     *
     * @param bookingStatus
     *     The booking_status
     */
    public void setBookingStatus(String bookingStatus) {
        this.bookingStatus = bookingStatus;
    }

    /**
     *
     * @param text
     *     The booking_status
     */
    public void setText(String text) {
        this.text = text;
    }

    /**
     *
     * @return
     *     The bookingStatus
     */
    public String getText() {
        return text;
    }


    /**
     *
     * @param header
     *     The booking_status
     */
    public void setHeader(String header) {
        this.header = header;
    }

    /**
     *
     * @return
     *     The bookingStatus
     */
    public String getHeader() {
        return header;
    }


    /**
     *
     * @return
     *     The bookingId
     */
    public String getBookingId() {
        return bookingId;
    }

    /**
     *
     * @param bookingId
     *     The booking_id
     */
    public void setBookingId(String bookingId) {
        this.bookingId = bookingId;
    }



    @SerializedName("thirdparty_transaction")
    @Expose
    private ThirdpartyTransaction_ thirdpartyTransaction;

    /**
     * 
     * @return
     *     The shareRideUrl
     */
    public String getShareRideUrl() {
        return shareRideUrl;
    }

    /**
     * 
     * @param shareRideUrl
     *     The share_ride_url
     */
    public void setShareRideUrl(String shareRideUrl) {
        this.shareRideUrl = shareRideUrl;
    }

    /**
     * 
     * @return
     *     The eta
     */
    public int getEta() {
        return eta;
    }

    /**
     * 
     * @param eta
     *     The eta
     */
    public void setEta(int eta) {
        this.eta = eta;
    }

    /**
     * 
     * @return
     *     The driverNumber
     */
    public String getDriverNumber() {
        return driverNumber;
    }

    /**
     * 
     * @param driverNumber
     *     The driver_number
     */
    public void setDriverNumber(String driverNumber) {
        this.driverNumber = driverNumber;
    }

    /**
     * 
     * @return
     *     The driverLng
     */
    public float getDriverLng() {
        return driverLng;
    }

    /**
     * 
     * @param driverLng
     *     The driver_lng
     */
    public void setDriverLng(float driverLng) {
        this.driverLng = driverLng;
    }

    /**
     * 
     * @return
     *     The driverName
     */
    public String getDriverName() {
        return driverName;
    }

    /**
     * 
     * @param driverName
     *     The driver_name
     */
    public void setDriverName(String driverName) {
        this.driverName = driverName;
    }

    /**
     * 
     * @return
     *     The carModel
     */
    public String getCarModel() {
        return carModel;
    }

    /**
     * 
     * @param carModel
     *     The car_model
     */
    public void setCarModel(String carModel) {
        this.carModel = carModel;
    }

    /**
     * 
     * @return
     *     The crn
     */
    public String getCrn() {
        return crn;
    }

    /**
     * 
     * @param crn
     *     The crn
     */
    public void setCrn(String crn) {
        this.crn = crn;
    }

    /**
     * 
     * @return
     *     The cabNumber
     */
    public String getCabNumber() {
        return cabNumber;
    }

    /**
     * 
     * @param cabNumber
     *     The cab_number
     */
    public void setCabNumber(String cabNumber) {
        this.cabNumber = cabNumber;
    }

    /**
     * 
     * @return
     *     The driverLat
     */
    public float getDriverLat() {
        return driverLat;
    }

    /**
     * 
     * @param driverLat
     *     The driver_lat
     */
    public void setDriverLat(float driverLat) {
        this.driverLat = driverLat;
    }

    /**
     * 
     * @return
     *     The cabType
     */
    public String getCabType() {
        return cabType;
    }

    /**
     * 
     * @param cabType
     *     The cab_type
     */
    public void setCabType(String cabType) {
        this.cabType = cabType;
    }

    /**
     * 
     * @return
     *     The thirdpartyTransaction
     */
    public ThirdpartyTransaction_ getThirdpartyTransaction() {
        return thirdpartyTransaction;
    }

    /**
     * 
     * @param thirdpartyTransaction
     *     The thirdparty_transaction
     */
    public void setThirdpartyTransaction(ThirdpartyTransaction_ thirdpartyTransaction) {
        this.thirdpartyTransaction = thirdpartyTransaction;
    }




    @SerializedName("fare_breakup")
    @Expose
    private ArrayList<FareBreakup> fareBreakup = new ArrayList<FareBreakup>();

    /**
     *
     * @return
     *     The olaMoneyBalance
     */
    public int getOlaMoneyBalance() {
        return olaMoneyBalance;
    }

    /**
     *
     * @param olaMoneyBalance
     *     The ola_money_balance
     */
    public void setOlaMoneyBalance(int olaMoneyBalance) {
        this.olaMoneyBalance = olaMoneyBalance;
    }

    /**
     *
     * @return
     *     The tripInfo
     */
    public TripInfo getTripInfo() {
        return tripInfo;
    }

    /**
     *
     * @param tripInfo
     *     The trip_info
     */
    public void setTripInfo(TripInfo tripInfo) {
        this.tripInfo = tripInfo;
    }

    /**
     *
     * @return
     *     The fareBreakup
     */
    public ArrayList<FareBreakup> getFareBreakup() {
        return fareBreakup;
    }

    /**
     *
     * @param fareBreakup
     *     The fare_breakup
     */
    public void setFareBreakup(ArrayList<FareBreakup> fareBreakup) {
        this.fareBreakup = fareBreakup;
    }

}
