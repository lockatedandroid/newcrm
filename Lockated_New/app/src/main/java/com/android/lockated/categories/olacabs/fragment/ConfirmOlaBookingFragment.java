package com.android.lockated.categories.olacabs.fragment;


import android.Manifest;
import android.annotation.SuppressLint;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.android.lockated.LockatedApplication;
import com.android.lockated.R;
import com.android.lockated.categories.olacabs.activity.OlaMainActivity_Latest;
import com.android.lockated.categories.olacabs.adapter.CouponAdapter;
import com.android.lockated.model.OlaCab.OlaCabCategoryDetail;
import com.android.lockated.network.ConnectionDetector;
import com.android.lockated.preferences.LockatedPreferences;
import com.android.lockated.request.LockatedVolleyRequestQueue;
import com.android.lockated.utils.Utilities;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.google.gson.Gson;
import com.sothree.slidinguppanel.SlidingUpPanelLayout;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;


public class ConfirmOlaBookingFragment extends Fragment implements Response.ErrorListener, Response.Listener<String>, TextWatcher, View.OnClickListener     //ILocationUpdateListener,
{
    private static final int REQUEST_LOCATION = 10;
    private static final String REQUEST_TAG = "OLA CABS";
    //private String ola_token = null;
    static String coupon_code;
    boolean enable_booking = true;
    static boolean flag;
    String category, distance, travel_time_in_minutes, amount_min, amount_max;
    public static ProgressBar progressBar;
    private ArrayList<OlaCabCategoryDetail> olaCabCategoryDetails;

    //------Change done on 5-8-2016-----2.42pm------------------------------

        //private AccountController accountController;
        //private ArrayList<AccountData> accountDataArrayList;

    //----------------------------------------------------------------------
    private CouponAdapter couponAdapter;
    public double pickup_latitude, drop_latitude;
    public double pickup_longitude, drop_longitude;

    private LinearLayout confirm_layout;

    private ImageView mImageViewPickup_CLocation, mImageViewDrop_CLocation, img_ola_cab_image;
    private LinearLayout layout_ola_cab_rateCard, layout_ola_cab_fareCalculator, layout_ola_cab_applyCoupon, layout_surcharge_confirm;
    private AutoCompleteTextView mEditTextPickup_CLocation, mEditTextDrop_CLocation;
    public static TextView txt_ola_cab_type, txt_ola_cab_cost, txt_olaCab_fare_surcharge_confirm, txt_ola_cab_minutes, txt_fare_cal;
    private Button btn_olaCabBooking;
    private String ola_cab_type, ola_cab_fare, ola_cab_eta, ola_cab_image, ola_cab_id, ola_cab_FareObject;
    private boolean ola_dest_exist;

    private float ola_cab_surcharge;
    private LockatedPreferences mLockatedPreferences;

    JSONObject fare_object;
    private String screenName = "ConfirmOlaBookingFragment";

    public ConfirmOlaBookingFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mLockatedPreferences = new LockatedPreferences(getActivity());

        //pickup_latitude = mLockatedPreferences.getSrcLat();
        //pickup_longitude = mLockatedPreferences.getSrcLng();

        //drop_latitude = mLockatedPreferences.getDstLat();
        //drop_longitude = mLockatedPreferences.getDstLng();

        //OlaMainActivity_Latest.mLayout.setPanelHeight(200);
        //Log.e("", "" + SlidingUpPanelLayout.PanelState.COLLAPSED);
        //Log.e("", "" + OlaMainActivity_Latest.mLayout.getPanelState());
        //Log.e("", "" + OlaMainActivity_Latest.mLayout.getAnchorPoint());
        //Log.e("", "" + OlaMainActivity_Latest.mLayout.getCoveredFadeColor());
        //Log.e("", "" + OlaMainActivity_Latest.mLayout.getCurrentParallaxOffset());
        //Log.e("", "" + OlaMainActivity_Latest.mLayout.getPanelHeight());
        //Log.e("", "" + OlaMainActivity_Latest.mLayout.getShadowHeight());
        //Log.e("", "" + OlaMainActivity_Latest.mLayout.getPanelHeight());


        /* if (OlaMainActivity_Latest.mLayout != null && (OlaMainActivity_Latest.mLayout.getPanelState() == SlidingUpPanelLayout.PanelState.EXPANDED)) {
            OlaMainActivity_Latest.mLayout.setPanelState(SlidingUpPanelLayout.PanelState.ANCHORED);
        }*/

        ///----------------------------------------------//
        OlaMainActivity_Latest.edt_olaSourceLocation.setFocusable(false);
        OlaMainActivity_Latest.edt_olaSourceLocation.setClickable(false);
        OlaMainActivity_Latest.edt_olaSourceLocation.setEnabled(false);
        ///--------------------------------------------------//

        OlaMainActivity_Latest.edt_olaSourceLocation.setClickable(false);
        OlaMainActivity_Latest.mLayout.setPanelState(SlidingUpPanelLayout.PanelState.COLLAPSED);
        OlaMainActivity_Latest.mLayout.setEnabled(true);
        //OlaMainActivity_Latest.mOlaSourceLayout.setEnabled(false);

        //--------------Changes on 10-8-2016-----------------------------------

        //OlaMainActivity_Latest.mOlaSourceLayout.setAlpha(0.8f);

        //---------------------------------------------------------------------
        //OlaMainActivity_Latest.gmap.setOnCameraChangeListener(null);
        OlaMainActivity_Latest.gmap.getUiSettings().setScrollGesturesEnabled(false);
        OlaMainActivity_Latest.gmap.getUiSettings().setMyLocationButtonEnabled(false);
        //OlaMainActivity_Latest.location_button_new.setVisibility(View.GONE);//--------change on 19-06-2016
        OlaMainActivity_Latest.current_location_zoom.setVisibility(View.GONE);//------change on 16-09-2016
        //OlaMainActivity_Latest.img_ola_cancel.setVisibility(View.GONE);
        OlaMainActivity_Latest.show_destination.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                //--------------Changes on 10-8-2016-----------------------------------
                /*if (OlaMainActivity_Latest.mOlaDestinationLayout.getVisibility() == View.GONE) {
                    OlaMainActivity_Latest.mOlaDestinationLayout.setVisibility(View.VISIBLE);
                } else {
                    OlaMainActivity_Latest.mOlaDestinationLayout.setVisibility(View.GONE);
                }*/
                //--------------------------------------------------------------------------



                /*------------------Changes on 20-9-2016-----------------------------------

                if (OlaMainActivity_Latest.mOlaDestinationCard.getVisibility() == View.GONE) {
                    OlaMainActivity_Latest.mOlaDestinationCard.setVisibility(View.VISIBLE);
                } else {
                    OlaMainActivity_Latest.mOlaDestinationCard.setVisibility(View.GONE);
                }


                */
            }
        });
        OlaMainActivity_Latest.edt_olaSourceLocation.setClickable(false);
        OlaMainActivity_Latest.edt_olaSourceLocation.setFocusable(false);
        //OlaMainActivity_Latest.edt_olaDestinationLocation.addTextChangedListener(this);


        if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED ) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }


        //OlaMainActivity_Latest.mLayout.setCoveredFadeColor(Color.BLUE);

        //accountController = AccountController.getInstance();
        //accountDataArrayList = accountController.getmAccountDataList();

        getActivity().setTitle("Confirmation");
        Bundle bundle = getArguments();
        if (bundle != null) {
            ola_cab_id = bundle.getString("ola_cab_id");
            ola_cab_type = bundle.getString("ola_cab_type");
            ola_cab_fare = bundle.getString("ola_cab_fare");
            ola_cab_eta = bundle.getString("ola_cab_eta");
            ola_cab_image = bundle.getString("ola_cab_image");
            ola_cab_surcharge = bundle.getFloat("ola_cab_surcharge");
            ola_cab_FareObject = bundle.getString("ola_cab_jsonObject");
            ola_dest_exist = bundle.getBoolean("ola_dest_exist");
        }

        mLockatedPreferences.setOlaCategory(ola_cab_id);
        /*try {
            fare_object = new JSONObject(ola_cab_FareObject);
            fare_type = fare_object.getString("type");
            fare_minimum_distance = fare_object.getString("minimum_distance");
            fare_minimum_time = fare_object.getString("minimum_time");
            fare_base_fare = fare_object.getString("base_fare");
            fare_minimum_fare = fare_object.getString("minimum_fare");
            fare_cost_per_distance = fare_object.getString("cost_per_distance");
            fare_waiting_cost_per_minute = fare_object.getString("waiting_cost_per_minute");
            fare_ride_cost_per_minute = fare_object.getString("ride_cost_per_minute");

        } catch (JSONException e) {
            e.printStackTrace();
            Log.e("Fare Exception",e.toString());
        }

*/
        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_confirm_ola_booking, container, false);
        //-----------------------------------------------------------

        Utilities.ladooIntegration(getActivity(), screenName);
        Utilities.lockatedGoogleAnalytics(screenName, getString(R.string.visited), screenName);
        confirm_layout = (LinearLayout) view.findViewById(R.id.confirm_layout);

        DisplayMetrics dm = new DisplayMetrics();
        getActivity().getWindowManager().getDefaultDisplay().getMetrics(dm);
        int height = dm.heightPixels;

        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, (int) (height / 2.5));
        confirm_layout.setLayoutParams(layoutParams);
        //-----------------------------------------------------------
        init(view);
        return view;
    }

    /*@Override
    public void onSaveInstanceState(Bundle outState) {
        *//*super.onSaveInstanceState(outState);*//*
    }*/

    @Override
    public void onResume() {
        super.onResume();
        if (((LockatedApplication) getActivity().getApplicationContext()).isGPSEnabled()) {
            ((LockatedApplication) getActivity().getApplicationContext()).setIsGPSEnabled(false);
        }
    }


    public void init(View view) {

        olaCabCategoryDetails = new ArrayList<>();
        progressBar = (ProgressBar) view.findViewById(R.id.mProgressBarView);

        img_ola_cab_image = (ImageView) view.findViewById(R.id.img_ola_cab_image);
        layout_ola_cab_rateCard = (LinearLayout) view.findViewById(R.id.layout_ola_cab_rateCard);
        layout_ola_cab_fareCalculator = (LinearLayout) view.findViewById(R.id.layout_ola_cab_fareCalculator);
        layout_ola_cab_applyCoupon = (LinearLayout) view.findViewById(R.id.layout_ola_cab_applyCoupon);
        layout_surcharge_confirm = (LinearLayout) view.findViewById(R.id.layout_surcharge_confirm);
        btn_olaCabBooking = (Button) view.findViewById(R.id.btn_olaCabBooking);

        txt_ola_cab_type = (TextView) view.findViewById(R.id.txt_ola_cab_type);
        txt_ola_cab_cost = (TextView) view.findViewById(R.id.txt_ola_cab_cost);
        txt_olaCab_fare_surcharge_confirm = (TextView) view.findViewById(R.id.olaCab_fare_surcharge_confirm);
        txt_ola_cab_minutes = (TextView) view.findViewById(R.id.txt_ola_cab_minutes);
        txt_fare_cal = (TextView) view.findViewById(R.id.txt_fare_cal);
        txt_ola_cab_cost.setText(ola_cab_fare);
        txt_ola_cab_type.setText(ola_cab_type);
        if (ola_dest_exist)
        {
            txt_ola_cab_cost.setTextColor(Color.parseColor("#5CB3FF"));
            txt_fare_cal.setText("For destination");
        }
        else
        {
            txt_ola_cab_cost.setTextColor(Color.parseColor("#000000"));
            txt_fare_cal.setText("For 5 Kms");
        }
        //txt_ola_cab_cost.setText(ola_cab_fare);
        txt_ola_cab_minutes.setText(ola_cab_eta + " min");
        Picasso.with(getActivity()).load(ola_cab_image).into(img_ola_cab_image);

        if (ola_cab_surcharge == 0) {

            if (layout_surcharge_confirm.getVisibility() == View.VISIBLE)
                layout_surcharge_confirm.setVisibility(View.GONE);
        } else {
            layout_surcharge_confirm.setVisibility(View.VISIBLE);
            txt_olaCab_fare_surcharge_confirm.setText("" + ola_cab_surcharge);
        }


        /*Log.e("pickup_latitude :", "" + pickup_latitude);
        Log.e("pickup_longitude :", "" + pickup_longitude);
        Log.e("drop_latitude :", "" + drop_latitude);
        Log.e("drop_longitude :", "" + drop_longitude);*/

        //Log.e("pickup_latitude :", "" + mLockatedPreferences.getSrcLat());
        //Log.e("pickup_longitude :", "" + mLockatedPreferences.getSrcLng());
        //Log.e("drop_latitude :", "" + mLockatedPreferences.getDstLat());
        //Log.e("drop_longitude :", "" + mLockatedPreferences.getDstLng());

        //checkEstimate(pickup_latitude, pickup_longitude, drop_latitude, drop_longitude);

        btn_olaCabBooking.setOnClickListener(this);
        layout_ola_cab_fareCalculator.setOnClickListener(this);
        layout_ola_cab_rateCard.setOnClickListener(this);
        layout_ola_cab_applyCoupon.setOnClickListener(this);

    }

    private void checkEstimate(double pickup_latitude, double pickup_longitude, double drop_latitude, double drop_longitude) {
        if (getActivity() != null) {
            if (ConnectionDetector.isConnectedToInternet(getActivity())) {
               /* if(txt_ola_cab_cost.getVisibility() == View.VISIBLE) {
                    txt_ola_cab_cost.setVisibility(View.GONE);
                    progressBar.setVisibility(View.VISIBLE);
                }
                if (txt_ola_cab_cost.getVisibility() == View.GONE)
                {
                    txt_ola_cab_cost.setVisibility(View.VISIBLE);
                    progressBar.setVisibility(View.VISIBLE);
                }*/
                txt_ola_cab_cost.setVisibility(View.GONE);
                progressBar.setVisibility(View.VISIBLE);

                //String url = "http://sandbox-t.olacabs.com/v1/products?pickup_lat=12.9491416&pickup_lng=77.64298" + "&drop_lat=" + drop_latitude + "&drop_lng=" + drop_longitude + "&category=" + ola_cab_id;
                //Log.e("onLocationPointListener", "" + url);
                //https://devapi.olacabs.com/v1/products?pickup_lat=12.9491416&pickup_lng=77.64298&drop_lat=12.96&drop_lng=77.678&category=prime
                String url = "https://devapi.olacabs.com/v1/products?pickup_lat=" + pickup_latitude + "&pickup_lng=" + pickup_longitude + "&drop_lat=" + drop_latitude + "&drop_lng=" + drop_longitude + "&category=" + ola_cab_id;
                //Log.e("onLocationPointListener", "" + url);

                LockatedVolleyRequestQueue mLockatedVolleyRequestQueue = LockatedVolleyRequestQueue.getInstance(getActivity());
                mLockatedVolleyRequestQueue.sendGetRequestOlaCabs(REQUEST_TAG, Request.Method.GET, url, this, this);
            } else {
                Utilities.showToastMessage(getActivity(), getActivity().getResources().getString(R.string.internet_connection_error));
                progressBar.setVisibility(View.GONE);
            }
        }
    }

    @Override
    public void onErrorResponse(VolleyError error) {
        //Log.e("VolleyError", String.valueOf(error));
    }

    @Override
    public void onResponse(String response) {
        //Log.e("RESPONSE", "" + response);
        if (getActivity() != null) {
            progressBar.setVisibility(View.GONE);
            txt_ola_cab_cost.setVisibility(View.VISIBLE);
            if (response != null && response.length() > 0) {
                JSONObject jsonObject = null;
                try {
                    jsonObject = new JSONObject(response);
                    if (jsonObject.has("message") && jsonObject.has("code") && !jsonObject.has("categories") && !jsonObject.has("ride_estimate"))
                    {
                        enable_booking = false;
                        Utilities.showToastMessage(getActivity(), jsonObject.getString("message"));
                        txt_ola_cab_cost.setText(ola_cab_fare);
                        txt_ola_cab_cost.setTextColor(Color.parseColor("#000000"));

                        if (mLockatedPreferences.getDstLat() != 0 && mLockatedPreferences.getDstLng() != 0) {
                            txt_fare_cal.setText("For destination");
                        }
                        else
                        {
                            txt_fare_cal.setText("For 5 kms");
                        }

                    } else {
                        enable_booking = true;
                        if (olaCabCategoryDetails.size() > 0) {
                            olaCabCategoryDetails.clear();
                        }
                        if (jsonObject.has("categories") && jsonObject.getJSONArray("categories").length() > 0 && jsonObject.has("ride_estimate")) {
                            Gson gson = new Gson();
                            OlaCabCategoryDetail mOlaCabCategoryDetail = gson.fromJson(jsonObject.toString(), OlaCabCategoryDetail.class);
                            olaCabCategoryDetails.add(mOlaCabCategoryDetail);

                            category = olaCabCategoryDetails.get(0).getRideEstimate().get(0).getCategory();
                            amount_max = "" + olaCabCategoryDetails.get(0).getRideEstimate().get(0).getAmountMax();
                            amount_min = "" + olaCabCategoryDetails.get(0).getRideEstimate().get(0).getAmountMin();
                            distance = "" + olaCabCategoryDetails.get(0).getRideEstimate().get(0).getDistance();
                            travel_time_in_minutes = "" + olaCabCategoryDetails.get(0).getRideEstimate().get(0).getTravelTimeInMinutes() + "mins";
                            txt_ola_cab_cost.setVisibility(View.VISIBLE);
                            txt_ola_cab_cost.setText(getString(R.string.rupees_symbol) + "" + "" + amount_min + "-" + amount_max);
                            txt_ola_cab_cost.setTextColor(Color.parseColor("#5CB3FF"));
                            txt_fare_cal.setText("For destination");
                        }
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }
    }

    @SuppressLint("LongLogTag")
    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {
        //Log.e("beforeTextChanged","true");
        //Log.e("mLockatedPreferences.getDstLat","beforeTextChanged "+mLockatedPreferences.getDstLat());
        //Log.e("mLockatedPreferences.getDstLng","beforeTextChanged "+mLockatedPreferences.getDstLng());
    }

    @SuppressLint("LongLogTag")
    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {
        //Log.e("onTextChanged","true");
        //Log.e("mLockatedPreferences.getDstLat","onTextChanged "+mLockatedPreferences.getDstLat());
        //Log.e("mLockatedPreferences.getDstLng","onTextChanged "+mLockatedPreferences.getDstLng());
        //checkEstimate(mLockatedPreferences.getSrcLat(),mLockatedPreferences.getSrcLng(),mLockatedPreferences.getDstLat(),mLockatedPreferences.getDstLng());
    }

    @SuppressLint("LongLogTag")
    @Override
    public void afterTextChanged(Editable s) {
        //Log.e("afterTextChanged","true");
        //Log.e("mLockatedPreferences.getDstLat","afterTextChanged "+mLockatedPreferences.getDstLat());
        //Log.e("mLockatedPreferences.getDstLng","afterTextChanged "+mLockatedPreferences.getDstLng());
        Log.e("confirm Destination"," "+"Im here");
        checkEstimate(mLockatedPreferences.getSrcLat(),mLockatedPreferences.getSrcLng(),mLockatedPreferences.getDstLat(),mLockatedPreferences.getDstLng());
    }

    @SuppressLint("LongLogTag")
    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_olaCabBooking:

                //------Change done on 5-8-2016-----2.42pm------------------------------

                    //Log.e("Ola Token", "" + accountController.getmAccountDataList().get(0).getOlatoken());
                    //ola_token = accountController.getmAccountDataList().get(0).getOlatoken();

                //---------------------------------------------------------------------------------


                //Log.e("Ola Token", "" + mLockatedPreferences.getOlaToken());
                //ola_token = mLockatedPreferences.getOlaToken();

                final Bundle bundle = new Bundle();
                /*float srcLat = (float) pickup_latitude;
                float srcLng = (float) pickup_longitude;
                float dntLat = (float) drop_latitude;
                float dntLng = (float) drop_longitude;

                bundle.putFloat("pickup_latitude", srcLat);
                bundle.putFloat("pickup_longitude", srcLng);
                bundle.putFloat("drop_latitude", dntLat);
                bundle.putFloat("drop_longitude", dntLng);*/
                bundle.putString("category", ola_cab_id);

               /* Log.e("pickup_latitude", String.valueOf(srcLat));
                Log.e("pickup_longitude", String.valueOf(srcLng));
                Log.e("drop_latitude", String.valueOf(dntLat));
                Log.e("drop_longitude", String.valueOf(dntLng));*/
                //Log.e("category", ola_cab_id);

                //if (()) //checkForBookng{

                    //if (mLockatedPreferences.getDstLat() == 0 || mLockatedPreferences.getDstLng() == 0) {

                    ///------------------------ Coupon Coding----------------------------

                        /*if (!this.ola_token.equals("null") && coupon_code != null) {
                        Log.e("Token Exist", "YES :" + ola_token);
                        bundle.putString("ola_token", ola_token);
                        bundle.putString("coupon_code", coupon_code);

                        OlaLoadCancelFragment olaLoadCancelFragment = new OlaLoadCancelFragment();
                        olaLoadCancelFragment.setArguments(bundle);
                        olaLoadCancelFragment.show(getFragmentManager(), "progress");
                        } else */

                    //--------------------------------------------------------------------
                    //Log.e("ola_token",""+mLockatedPreferences.getOlaToken());
                    //Log.e("enable_booking",""+enable_booking);
                    if (enable_booking) {
                        if (!mLockatedPreferences.getOlaToken().equals("null") )//.equals("null") //!ola_token.equals("null"
                        {
                            //Log.e("ola_token", "" + mLockatedPreferences.getOlaToken());
                            //Log.e("Token Exist", "YES :" + mLockatedPreferences.getOlaToken());
                            //bundle.putString("ola_token", ola_token);

                            OlaLoadCancelFragment olaLoadCancelFragment = new OlaLoadCancelFragment();
                            olaLoadCancelFragment.setArguments(bundle);
                            olaLoadCancelFragment.show(getFragmentManager(), "progress");
                        } else {
                            //Log.e("Token Exist", "NO :" + "Creates New");

                            OlaCabAuthFragment olaCabAuthFragment = new OlaCabAuthFragment();
                            olaCabAuthFragment.setArguments(bundle);
                            olaCabAuthFragment.show(getFragmentManager(), "Web View");
                        }
                    }
                    else {
                        //Log.e("enable_booking",""+enable_booking);
                        if (mLockatedPreferences.getDstLat() == 0.0 && mLockatedPreferences.getDstLng() == 0.0)
                        {
                            //Log.e("mLockatedPreferences.getOlaToken","Lat : 0.0 & Lng : 0.0");
                            if (!mLockatedPreferences.getOlaToken().equals("null") )//.equals("null") //!ola_token.equals("null"
                            {
                                //Log.e("ola_token", "" + mLockatedPreferences.getOlaToken());
                                //Log.e("Token Exist", "YES :" + mLockatedPreferences.getOlaToken());
                                //bundle.putString("ola_token", ola_token);

                                OlaLoadCancelFragment olaLoadCancelFragment = new OlaLoadCancelFragment();
                                olaLoadCancelFragment.setArguments(bundle);
                                olaLoadCancelFragment.show(getFragmentManager(), "progress");
                            } else {
                                //Log.e("Token Exist", "NO :" + "Creates New");

                                OlaCabAuthFragment olaCabAuthFragment = new OlaCabAuthFragment();
                                olaCabAuthFragment.setArguments(bundle);
                                olaCabAuthFragment.show(getFragmentManager(), "Web View");
                            }
                        }
                        else
                        {
                            //Log.e("Show Msg","Different pickup city");
                            Utilities.showToastMessage(getActivity(),"Pickup city is different than drop city.");
                        }
                    }
                //}
                break;

            case R.id.layout_ola_cab_fareCalculator:
                //Log.e("Click Detected at ", "layout_ola_cab_fareCalculator");
                //txt_ola_cab_cost.setVisibility(View.GONE);
                flag = checkForFareCalculation();
                break;

            case R.id.layout_ola_cab_rateCard:
                //Log.e("Click Detected at ", "layout_ola_cab_rateCard");

                Bundle bundle_new = new Bundle();
                bundle_new.putString("FareObject", ola_cab_FareObject);
                if (ola_cab_surcharge != 0) {
                    bundle_new.putFloat("Surcharge", ola_cab_surcharge);
                }
                //Log.e("Fare Object", ola_cab_FareObject);
                //Log.e("Surcharge", "" + ola_cab_surcharge);

                FareCardFragment fareCardFragment = new FareCardFragment();
                fareCardFragment.setArguments(bundle_new);
                fareCardFragment.show(getFragmentManager(), "Web View");
                break;


           case R.id.layout_ola_cab_applyCoupon :
                Bundle new_bundle = new Bundle();
                new_bundle.putString("category", ola_cab_id);


               CouponValidatorFragment couponValidatorFragment = new CouponValidatorFragment();
               couponValidatorFragment.setArguments(new_bundle);
               couponValidatorFragment.show(getFragmentManager(), "progress");
               break;

                /*if (!this.ola_token.equals("null")) {
                    Log.e("Token Exist", "YES :" + ola_token);
                    new_bundle.putString("ola_token", ola_token);

                    CouponValidatorFragment couponValidatorFragment = new CouponValidatorFragment();
                    couponValidatorFragment.setArguments(new_bundle);
                    couponValidatorFragment.show(getFragmentManager(), "progress");
                } else {
                    Log.e("Token Exist", "NO :" + "Creates New");

                    OlaCabAuthFragment olaCabAuthFragment = new OlaCabAuthFragment();
                    olaCabAuthFragment.setArguments(bundle);
                    olaCabAuthFragment.show(getFragmentManager(), "Web View");
                }
*/
               /* final AlertDialog.Builder dialog = new AlertDialog.Builder(getActivity());
                View view = getActivity().getLayoutInflater().inflate(R.layout.coupon_layout, null);
                dialog.setView(view);
                final AlertDialog alertDialog = dialog.show();*/
        }
    }


    public boolean checkForFareCalculation() {
        boolean flag = false;
        //----------------Changes on 10-8-2016-------------------------------------------------

        /* if (OlaMainActivity_Latest.mOlaDestinationLayout.getVisibility() == View.GONE) {
            OlaMainActivity_Latest.mOlaDestinationLayout.setVisibility(View.VISIBLE);
            if (OlaMainActivity_Latest.edt_olaDestinationLocation.getText().length() == 0) {*/

        //----------------------------------------------------------------------------------
        if (OlaMainActivity_Latest.mOlaDestinationCard.getVisibility() == View.GONE) {
            OlaMainActivity_Latest.mOlaDestinationCard.setVisibility(View.VISIBLE);
            if (OlaMainActivity_Latest.edt_olaDestinationLocation.getText().length() == 0) {
                Animation shake = AnimationUtils.loadAnimation(getActivity(), R.anim.shake);
                String error = "Please enter drop location for fare";
                Utilities.showToastMessage(getActivity(), error);
                mLockatedPreferences.setDstLat(0);
                mLockatedPreferences.setDstLng(0);
                OlaMainActivity_Latest.edt_olaDestinationLocation.setText(error);
                OlaMainActivity_Latest.edt_olaDestinationLocation.setTextColor(Color.parseColor("#cc0000"));
                //OlaMainActivity_Latest.edt_olaDestinationLocation.setAnimation(shake);
            } else {
                flag = true;
                checkEstimate(mLockatedPreferences.getSrcLat(), mLockatedPreferences.getSrcLng(), mLockatedPreferences.getDstLat(), mLockatedPreferences.getDstLng());
            }
        }
        //--------------------------Changes on 10-8-2016----------------------------
        //else if (OlaMainActivity_Latest.mOlaDestinationLayout.getVisibility() == View.VISIBLE) {
        //--------------------------------------------------------------------------
        else if (OlaMainActivity_Latest.mOlaDestinationCard.getVisibility() == View.VISIBLE) {
        if (OlaMainActivity_Latest.edt_olaDestinationLocation.getText().length() == 0) {
                Animation shake = AnimationUtils.loadAnimation(getActivity(), R.anim.shake);
                String error = "Please enter drop location for fare";
                Utilities.showToastMessage(getActivity(), error);
                OlaMainActivity_Latest.edt_olaDestinationLocation.setText(error);
                OlaMainActivity_Latest.edt_olaDestinationLocation.setTextColor(Color.RED);
                //OlaMainActivity_Latest.edt_olaDestinationLocation.setAnimation(shake);
            } else {
                flag = true;
                checkEstimate(mLockatedPreferences.getSrcLat(), mLockatedPreferences.getSrcLng(), mLockatedPreferences.getDstLat(), mLockatedPreferences.getDstLng());
            }

        }

        return flag;
    }

}
