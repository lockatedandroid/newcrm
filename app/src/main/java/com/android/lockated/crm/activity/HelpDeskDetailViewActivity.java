package com.android.lockated.crm.activity;


import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;

import com.android.lockated.R;
import com.android.lockated.information.AccountController;
import com.android.lockated.preferences.LockatedPreferences;
import com.android.lockated.utils.ShowImage;
import com.android.volley.RequestQueue;
import com.google.android.gms.common.api.GoogleApiClient;
import com.squareup.picasso.Picasso;

public class HelpDeskDetailViewActivity extends AppCompatActivity implements View.OnClickListener {
    private TextView mFlatNumber;
    private TextView mTextStatus;
    private TextView mPostedBy;
    private TextView mTextCategory;
    private TextView mIssueCategory;
    private TextView mIssueDescription;
    private TextView mPostedOn;
    private TextView mUpdatedOn;
    private TextView mTextAttachment;
    private TextView mIssueType;
    private ImageView mComplaintAttachment;
    private Spinner mSpinnerStatus;
    private String spid;
    String strImage;
    ProgressBar progressBar;
    private RequestQueue mQueue;
    private LockatedPreferences mLockatedPreferences;
    private AccountController accountController;
    public static final String REQUEST_TAG = "HelpDeskDetailViewActivity";
    private GoogleApiClient client;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_help_desk_detail_view);//
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(true);
        getSupportActionBar().setTitle(R.string.helpdesk_detail);
        init();
        DataGetSet();
    }

    private void init() {
        progressBar = (ProgressBar) findViewById(R.id.mProgressBarView);
        mTextStatus = (TextView) findViewById(R.id.mTextStatus);
        mTextCategory = (TextView) findViewById(R.id.mTextCategory);
        mIssueCategory = (TextView) findViewById(R.id.mIssueCategory);
        mPostedOn = (TextView) findViewById(R.id.mPostedOn);
        mUpdatedOn = (TextView) findViewById(R.id.mUpdatedOn);
        mIssueType = (TextView) findViewById(R.id.mIssueType);
        mTextAttachment = (TextView) findViewById(R.id.mTextAttachment);
        mIssueDescription = (TextView) findViewById(R.id.mIssueDescription);
        mComplaintAttachment = (ImageView) findViewById(R.id.mComplaintAttachment);
        mComplaintAttachment.setOnClickListener(this);
    }

    private void DataGetSet() {
        if (getIntent().getExtras() != null) {
            mIssueType.setText(getIntent().getExtras().getString("strIssueType"));
            mIssueDescription.setText(getIntent().getExtras().getString("strDescription"));
            mUpdatedOn.setText(getIntent().getExtras().getString("strmUpdatedOn"));
            mPostedOn.setText(getIntent().getExtras().getString("strPostedOn"));
            mIssueCategory.setText(getIntent().getExtras().getString("strIssueCategory"));
            mTextCategory.setText(getIntent().getExtras().getString("strmTextCategory"));
            spid = (getIntent().getExtras().getString("pid"));
            strImage = getIntent().getExtras().getString("strComplaintImage");
            String status = getIntent().getExtras().getString("strStatus");
            if (status.equals("null")) {
                mTextStatus.setText("Pending");
            } else {
                mTextStatus.setText(getIntent().getExtras().getString("strStatus"));
            }
            if (strImage != null && !strImage.equals("No Document")) {
                Picasso.with(this).load("" + strImage).fit().placeholder(R.drawable.loading).into(mComplaintAttachment);
            } else {
                mTextAttachment.setVisibility(View.VISIBLE);
                mComplaintAttachment.setVisibility(View.GONE);
            }

//        mTextStatus.setText(getIntent().getExtras().getString("strStatus"));
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home: {
                super.onBackPressed();
                return true;
            }
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onClick(View v) {
        Intent intent = new Intent(getApplicationContext(), ShowImage.class);
        intent.putExtra("imageUrlString", strImage);
        startActivity(intent);
    }
}
