package com.android.lockated.crm.activity;

import android.Manifest;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.NavigationView;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.android.lockated.LockatedApplication;
import com.android.lockated.R;
import com.android.lockated.crm.fragment.events.CreateEventFragment;
import com.android.lockated.crm.fragment.polls.CreatePollsFragment;
import com.android.lockated.drawer.PoliciesActivity;
import com.android.lockated.information.AccountController;
import com.android.lockated.landing.AccountFragment;
import com.android.lockated.landing.HomeFragment;
import com.android.lockated.landing.LocationFragment;
import com.android.lockated.landing.SupportFragment;
import com.android.lockated.network.ConnectionDetector;
import com.android.lockated.preferences.LockatedPreferences;
import com.android.lockated.utils.Utilities;


public class CreateNoticeListActivity extends AppCompatActivity implements AdapterView.OnItemSelectedListener, View.OnClickListener/*, NavigationView.OnNavigationItemSelectedListener*/ {

    private LinearLayout ContainerList;
    private ImageView mImageViewHome;
    private ImageView mImageViewChat;
    private ImageView mImageViewAccount;
    private ImageView mImageViewLocation;
    private LockatedPreferences mLockatedPreferences;
    private AccountController mAccountController;
    private boolean doubleBackToExitPressedOnce;
    LinearLayout linearLayoutContainer, linearLayoutContainerSub;
    private DrawerLayout drawer;
    private Toolbar toolbar;
    private static final int REQUEST_CALL = 11;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_notice_list);
        /*Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);*/

        setToolbar();
        setBottomTabView(null);
        init();


    }

    public void setToolbar() {

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setLogo(R.drawable.ic_header_logo);
        toolbar.setTitle("");
        setSupportActionBar(toolbar);

        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, drawer, toolbar,
                R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        //navigationView.getMenu().findItem(R.id.crmScreen).setVisible(true);
        //navigationView.setNavigationItemSelectedListener(this);

    }

    public void setBottomTabView(Bundle savedInstanceState) {
        LinearLayout mLinearLayoutHome = (LinearLayout) findViewById(R.id.mLinearLayoutHome);
        LinearLayout mLinearLayoutSupport = (LinearLayout) findViewById(R.id.mLinearLayoutSupport);
        LinearLayout mLinearLayoutAccount = (LinearLayout) findViewById(R.id.mLinearLayoutAccount);
        LinearLayout mLinearLayoutLocation = (LinearLayout) findViewById(R.id.mLinearLayoutLocation);

        mImageViewHome = (ImageView) findViewById(R.id.mImageViewHome);
        mImageViewChat = (ImageView) findViewById(R.id.mImageViewChat);
        mImageViewAccount = (ImageView) findViewById(R.id.mImageViewAccount);
        mImageViewLocation = (ImageView) findViewById(R.id.mImageViewLocation);

        mLinearLayoutHome.setOnClickListener(this);
        mLinearLayoutSupport.setOnClickListener(this);
        mLinearLayoutAccount.setOnClickListener(this);
        mLinearLayoutLocation.setOnClickListener(this);

    }

    private void init() {

        mAccountController = AccountController.getInstance();
        mLockatedPreferences = new LockatedPreferences(this);
        //mSpinnerCreateList = (Spinner) findViewById(R.id.mSpinnerCreateList);
        ContainerList = (LinearLayout) findViewById(R.id.ContainerList);
        //mSpinnerCreateList.setOnItemSelectedListener(this);
    }


    void showToast(CharSequence msg) {

        Toast.makeText(this, msg, Toast.LENGTH_SHORT).show();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home: {
                super.onBackPressed();
                return true;
            }
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

        //showToast(" id=" + id + ":-" + parent.getSelectedItem().toString());
        if (position == 0 && id == 0) {

            //ContainerList.removeView(parent);
        }
        if (position == 1 && id == 1) {
            //Function And Event
            CreateEventFragment checkoutFragment = new CreateEventFragment();
            getSupportFragmentManager().beginTransaction().replace(R.id.ContainerList, checkoutFragment).commit();
        }
        if (position == 2 && id == 2) {
            //Obituries And Condolences
            CreatePollsFragment checkoutFragment = new CreatePollsFragment();
            getSupportFragmentManager().beginTransaction().replace(R.id.ContainerList, checkoutFragment).commit();
        }

        if (position == 3 && id == 3) {
            //Votting And Polling
            CreateEventFragment checkoutFragment = new CreateEventFragment();
            getSupportFragmentManager().beginTransaction().replace(R.id.ContainerList, checkoutFragment).commit();
        }
        if (position == 4 && id == 4) {
//Cams And Drives
            CreateEventFragment checkoutFragment = new CreateEventFragment();
            getSupportFragmentManager().beginTransaction().replace(R.id.ContainerList, checkoutFragment).commit();
        }
        if (position == 5 && id == 5) {
//Invites And Appreciations
            CreateEventFragment checkoutFragment = new CreateEventFragment();
            getSupportFragmentManager().beginTransaction().replace(R.id.ContainerList, checkoutFragment).commit();
        }
        if (position == 6 && id == 6) {
//Facilities And Amenities
            CreateEventFragment checkoutFragment = new CreateEventFragment();
            getSupportFragmentManager().beginTransaction().replace(R.id.ContainerList, checkoutFragment).commit();
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

    @Override
    public void onClick(View v) {
        /*linearLayoutContainer.setVisibility(View.VISIBLE);
        linearLayoutContainerSub.setVisibility(View.GONE);*/

        switch (v.getId()) {
            case R.id.mLinearLayoutHome:
                onHomeClicked();
                break;
            case R.id.mLinearLayoutSupport:
                onSupportClicked();
                break;
            case R.id.mLinearLayoutAccount:
                onAccountClicked();
                break;
            case R.id.mLinearLayoutLocation:
                onLocationClicked();
                break;
        }
    }

    /*@Override
    public boolean onNavigationItemSelected(MenuItem item) {
        int id = item.getItemId();

        *//*if (id == R.id.crmScreen) {
            Intent intent = new Intent(this, CRMActivity.class);
            startActivity(intent);
            finish();
        } else *//*
        if (id == R.id.about_us) {
            onPoliciesClicked(getString(R.string.about_us), getString(R.string.about_us_link));
            LockatedApplication.getInstance().trackScreenView(getString(R.string.about_us));
            LockatedApplication.getInstance().trackEvent(getString(R.string.about_us), getString(R.string.visited), getString(R.string.about_us));
        } else if (id == R.id.rating) {
            onRatingClicked();
            LockatedApplication.getInstance().trackEvent(getString(R.string.rating), getString(R.string.visited), getString(R.string.rating));
        } else if (id == R.id.feedback) {
            onFeedbackClicked();
            LockatedApplication.getInstance().trackEvent(getString(R.string.feedback), "Click", getString(R.string.feedback));
        } else if (id == R.id.invite_friends) {
            onShareClicked();
            LockatedApplication.getInstance().trackEvent(getString(R.string.invite_friends), "Invite", getString(R.string.invite_friends));
        } else if (id == R.id.contact_us) {
            onContactUsClicked();
            LockatedApplication.getInstance().trackEvent(getString(R.string.contact_us), "Contact", getString(R.string.contact_us));
        } else if (id == R.id.call_us) {
            onCallUsClicked();
            LockatedApplication.getInstance().trackEvent(getString(R.string.call_us), "Call Us", getString(R.string.call_us));
        } else if (id == R.id.refund_policy) {
            onPoliciesClicked(getString(R.string.refund_policy), getString(R.string.refund_link));
            LockatedApplication.getInstance().trackScreenView(getString(R.string.refund_policy));
            LockatedApplication.getInstance().trackEvent(getString(R.string.refund_policy), getString(R.string.visited), getString(R.string.refund_policy));
        } else if (id == R.id.privacy_policy) {
            onPoliciesClicked(getString(R.string.privacy_policy), getString(R.string.privacy_link));
            LockatedApplication.getInstance().trackScreenView(getString(R.string.privacy_policy));
            LockatedApplication.getInstance().trackEvent(getString(R.string.privacy_policy), getString(R.string.visited), getString(R.string.privacy_policy));
        } else if (id == R.id.terms_conditions) {
            onPoliciesClicked(getString(R.string.terms_conditions), getString(R.string.terms_condition_link));
            LockatedApplication.getInstance().trackScreenView(getString(R.string.terms_conditions));
            LockatedApplication.getInstance().trackEvent(getString(R.string.terms_conditions), "Visited", getString(R.string.terms_conditions));
        } else if (id == R.id.logout) {
            showAlertDialog(this);
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }*/

    private void onHomeClicked() {
        HomeFragment homeFragment = new HomeFragment();
        replaceFragment(homeFragment);
        onHomeFragmentClicked();
    }

    private void onLocationClicked() {
        LocationFragment locationFragment = new LocationFragment();
        replaceFragment(locationFragment);
        onLocationFragmentClicked();
    }

    private void onAccountClicked() {
        AccountFragment accountFragment = new AccountFragment();
        replaceFragment(accountFragment);
        onAccountFragmentClicked();
    }

    private void onSupportClicked() {
        SupportFragment supportFragment = new SupportFragment();
        supportFragment.setChatId(157, getString(R.string.support_screen));
        replaceFragment(supportFragment);
        onChatFragmentClicked();
    }

    private void replaceFragment(Fragment fragment) {
        String backStateName = fragment.getClass().getName();
        String fragmentTag = backStateName;
        FragmentManager manager = getSupportFragmentManager();
        boolean fragmentPopped = manager.popBackStackImmediate(backStateName, 0);
        if (!fragmentPopped && manager.findFragmentByTag(fragmentTag) == null) {
            FragmentTransaction ft = manager.beginTransaction();
            ft.replace(R.id.mLandingContainer, fragment, fragmentTag);
            ft.commit();
        }
    }

    private void onHomeFragmentClicked() {
        mImageViewHome.setImageResource(R.drawable.ic_home_active);
        mImageViewChat.setImageResource(R.drawable.ic_chat);
        mImageViewAccount.setImageResource(R.drawable.ic_account);
        mImageViewLocation.setImageResource(R.drawable.ic_location);
    }

    private void onChatFragmentClicked() {
        mImageViewHome.setImageResource(R.drawable.ic_home);
        mImageViewChat.setImageResource(R.drawable.ic_chat_active);
        mImageViewAccount.setImageResource(R.drawable.ic_account);
        mImageViewLocation.setImageResource(R.drawable.ic_location);
    }

    private void onAccountFragmentClicked() {
        mImageViewHome.setImageResource(R.drawable.ic_home);
        mImageViewChat.setImageResource(R.drawable.ic_chat);
        mImageViewAccount.setImageResource(R.drawable.ic_account_active);
        mImageViewLocation.setImageResource(R.drawable.ic_location);
    }

    private void onLocationFragmentClicked() {
        mImageViewHome.setImageResource(R.drawable.ic_home);
        mImageViewChat.setImageResource(R.drawable.ic_chat);
        mImageViewAccount.setImageResource(R.drawable.ic_account);
        mImageViewLocation.setImageResource(R.drawable.ic_location_active);
    }

    private void onCallUsClicked() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                requestPermissions(new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, REQUEST_CALL);
            } else {
                callingIntent();
            }
        } else {
            callingIntent();
        }
    }

    private void callingIntent() {
        Intent intent = new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + getString(R.string.calling_number)));
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        startActivity(intent);
    }

    private void onRatingClicked() {
        Uri uri = Uri.parse("market://details?id=" + getPackageName());
        Intent myAppLinkToMarket = new Intent(Intent.ACTION_VIEW, uri);
        try {
            startActivity(myAppLinkToMarket);
        } catch (ActivityNotFoundException e) {
            Utilities.showToastMessage(this, "Unable to find application on Play store.");
        }
    }

    private void onShareClicked() {
        Intent sendIntent = new Intent();
        sendIntent.setAction(Intent.ACTION_SEND);
        sendIntent.putExtra(Intent.EXTRA_TEXT, getString(R.string.sharing_url));
        sendIntent.setType("text/plain");
        startActivity(Intent.createChooser(sendIntent, getResources().getText(R.string.share_message)));
    }

    private void onPoliciesClicked(String name, String url) {
        if (ConnectionDetector.isConnectedToInternet(this)) {
            Intent detailIntent = new Intent(this, PoliciesActivity.class);
            ((LockatedApplication) getApplicationContext()).setSectionName(name);
            ((LockatedApplication) getApplicationContext()).setSectionURL(url);
            startActivity(detailIntent);
        } else {
            Utilities.showToastMessage(this, getString(R.string.internet_connection_error));
        }
    }

    private void onFeedbackClicked() {
        Intent intent = new Intent(Intent.ACTION_SEND);
        intent.setType("message/rfc822");
        intent.putExtra(Intent.EXTRA_EMAIL, new String[]{getString(R.string.feedback_email)});
        intent.putExtra(Intent.EXTRA_SUBJECT, getString(R.string.feedback_subject));
        startActivity(Intent.createChooser(intent, getString(R.string.contact_us_message)));
    }

    private void onContactUsClicked() {
        Intent intent = new Intent(Intent.ACTION_SEND);
        intent.setType("message/rfc822");
        intent.putExtra(Intent.EXTRA_EMAIL, new String[]{getString(R.string.contact_us_email)});
        intent.putExtra(Intent.EXTRA_SUBJECT, getString(R.string.contact_us_subject));
        startActivity(Intent.createChooser(intent, getString(R.string.contact_us_message)));
    }

    public void showAlertDialog(Context context) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setMessage(R.string.logout_dialog_message);

        // Add the buttons
        builder.setPositiveButton(R.string.logout, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.dismiss();
                //onLogout();
            }
        });

        builder.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.dismiss();
            }
        });

        AlertDialog dialog = builder.create();
        dialog.show();
    }

    /*private void onLogout() {
        mAccountController.getmAccountDataList().clear();
        mLockatedPreferences.setLockatedToken("");
        mLockatedPreferences.setLogout(true);

        LockatedApplication.getInstance().trackEvent(getString(R.string.logout), "Logout", getString(R.string.logout));

        Intent intent = new Intent(this, IndexActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
    }*/

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.mLandingContainer);
            if (fragment instanceof SupportFragment) {
                if (((SupportFragment) fragment).isSampleMessageSelected) {
                    ((SupportFragment) fragment).isSampleMessageSelected = false;
                    ((SupportFragment) fragment).mSupportListSample.setVisibility(View.GONE);
                    ((SupportFragment) fragment).mImageViewSampleMessage.setImageResource(R.drawable.ic_sample_message_disable);
                } else {
                    if (!(fragment instanceof HomeFragment)) {
                        onHomeClicked();
                    } else {
                        onApplicationBackPressed();
                    }
                }
            } else if (!(fragment instanceof HomeFragment)) {
                onHomeClicked();
            } else {
                onApplicationBackPressed();
            }
        }
    }

    private void onApplicationBackPressed() {
        if (doubleBackToExitPressedOnce) {
            super.onBackPressed();
            return;
        }

        this.doubleBackToExitPressedOnce = true;
        Utilities.showToastMessage(this, getString(R.string.back_twice));

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                doubleBackToExitPressedOnce = false;
            }
        }, 2000);
    }

}
