package com.android.lockated.crmadmin.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;

import com.android.lockated.HomeActivity;
import com.android.lockated.R;
import com.android.lockated.crmadmin.fragment.GateKeeperFragment;

public class HomeGatekeeperActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home_gatekeeper);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(true);
        getSupportActionBar().setTitle(R.string.gatekeeper);
        init();

    }

    private void init() {
        FragmentManager fragmentManager = getSupportFragmentManager();
        GateKeeperFragment gateKeeperFragment = new GateKeeperFragment();
        gateKeeperFragment.setFragmentManager(fragmentManager);
        Bundle bundle=new Bundle();
        bundle.putString("nameString","HomeGatekeeperActivity");
        gateKeeperFragment.setArguments(bundle);
        getSupportFragmentManager().beginTransaction().replace(R.id.gatekeeperContainer, gateKeeperFragment).commit();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                callIntent();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        callIntent();
    }

    private void callIntent() {
        Intent intent = new Intent(this, HomeActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
        finish();
    }

}
