
package com.android.lockated.model.AdminModel.Notice.PublishNotice;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class Notice {

    @SerializedName("id")
    @Expose
    private int id;
    @SerializedName("id_society")
    @Expose
    private int idSociety;
    @SerializedName("notice_heading")
    @Expose
    private String noticeHeading;
    @SerializedName("notice_text")
    @Expose
    private String noticeText;
    @SerializedName("active")
    @Expose
    private Object active;
    @SerializedName("IsDelete")
    @Expose
    private Object IsDelete;
    @SerializedName("expire_time")
    @Expose
    private String expireTime;
    @SerializedName("created_at")
    @Expose
    private String createdAt;
    @SerializedName("updated_at")
    @Expose
    private String updatedAt;
    @SerializedName("id_user")
    @Expose
    private int idUser;
    @SerializedName("publish")
    @Expose
    private int publish;
    @SerializedName("notice_type")
    @Expose
    private Object noticeType;
    @SerializedName("deny")
    @Expose
    private int deny;
    @SerializedName("flag_expire")
    @Expose
    private Object flagExpire;
    @SerializedName("canceled_by")
    @Expose
    private Object canceledBy;
    @SerializedName("canceler_id")
    @Expose
    private Object cancelerId;
    @SerializedName("comment")
    @Expose
    private Object comment;
    @SerializedName("shared")
    @Expose
    private int shared;
    @SerializedName("username")
    @Expose
    private String username;
    @SerializedName("flat")
    @Expose
    private String flat;
    @SerializedName("documents")
    @Expose
    private ArrayList<AdminNoticeDocument> documents = new ArrayList<AdminNoticeDocument>();

    /**
     * @return The id
     */
    public int getId() {
        return id;
    }

    /**
     * @param id The id
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * @return The idSociety
     */
    public int getIdSociety() {
        return idSociety;
    }

    /**
     * @param idSociety The id_society
     */
    public void setIdSociety(int idSociety) {
        this.idSociety = idSociety;
    }

    /**
     * @return The noticeHeading
     */
    public String getNoticeHeading() {
        return noticeHeading;
    }

    /**
     * @param noticeHeading The notice_heading
     */
    public void setNoticeHeading(String noticeHeading) {
        this.noticeHeading = noticeHeading;
    }

    /**
     * @return The noticeText
     */
    public String getNoticeText() {
        return noticeText;
    }

    /**
     * @param noticeText The notice_text
     */
    public void setNoticeText(String noticeText) {
        this.noticeText = noticeText;
    }

    /**
     * @return The active
     */
    public Object getActive() {
        return active;
    }

    /**
     * @param active The active
     */
    public void setActive(Object active) {
        this.active = active;
    }

    /**
     * @return The IsDelete
     */
    public Object getIsDelete() {
        return IsDelete;
    }

    /**
     * @param IsDelete The IsDelete
     */
    public void setIsDelete(Object IsDelete) {
        this.IsDelete = IsDelete;
    }

    /**
     * @return The expireTime
     */
    public String getExpireTime() {
        return expireTime;
    }

    /**
     * @param expireTime The expire_time
     */
    public void setExpireTime(String expireTime) {
        this.expireTime = expireTime;
    }

    /**
     * @return The createdAt
     */
    public String getCreatedAt() {
        return createdAt;
    }

    /**
     * @param createdAt The created_at
     */
    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    /**
     * @return The updatedAt
     */
    public String getUpdatedAt() {
        return updatedAt;
    }

    /**
     * @param updatedAt The updated_at
     */
    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    /**
     * @return The idUser
     */
    public int getIdUser() {
        return idUser;
    }

    /**
     * @param idUser The id_user
     */
    public void setIdUser(int idUser) {
        this.idUser = idUser;
    }

    /**
     * @return The publish
     */
    public int getPublish() {
        return publish;
    }

    /**
     * @param publish The publish
     */
    public void setPublish(int publish) {
        this.publish = publish;
    }

    /**
     * @return The noticeType
     */
    public Object getNoticeType() {
        return noticeType;
    }

    /**
     * @param noticeType The notice_type
     */
    public void setNoticeType(Object noticeType) {
        this.noticeType = noticeType;
    }

    /**
     * @return The deny
     */
    public int getDeny() {
        return deny;
    }

    /**
     * @param deny The deny
     */
    public void setDeny(int deny) {
        this.deny = deny;
    }

    /**
     * @return The flagExpire
     */
    public Object getFlagExpire() {
        return flagExpire;
    }

    /**
     * @param flagExpire The flag_expire
     */
    public void setFlagExpire(Object flagExpire) {
        this.flagExpire = flagExpire;
    }

    /**
     * @return The canceledBy
     */
    public Object getCanceledBy() {
        return canceledBy;
    }

    /**
     * @param canceledBy The canceled_by
     */
    public void setCanceledBy(Object canceledBy) {
        this.canceledBy = canceledBy;
    }

    /**
     * @return The cancelerId
     */
    public Object getCancelerId() {
        return cancelerId;
    }

    /**
     * @param cancelerId The canceler_id
     */
    public void setCancelerId(Object cancelerId) {
        this.cancelerId = cancelerId;
    }

    /**
     * @return The comment
     */
    public Object getComment() {
        return comment;
    }

    /**
     * @param comment The comment
     */
    public void setComment(Object comment) {
        this.comment = comment;
    }

    /**
     * @return The shared
     */
    public int getShared() {
        return shared;
    }

    /**
     * @param shared The shared
     */
    public void setShared(int shared) {
        this.shared = shared;
    }

    /**
     * @return The username
     */
    public String getUsername() {
        return username;
    }

    /**
     * @param username The username
     */
    public void setUsername(String username) {
        this.username = username;
    }

    /**
     * @return The flat
     */
    public String getFlat() {
        return flat;
    }

    /**
     * @param flat The flat
     */
    public void setFlat(String flat) {
        this.flat = flat;
    }

    public ArrayList<AdminNoticeDocument> getDocuments() {
        return documents;
    }

    public void setDocuments(ArrayList<AdminNoticeDocument> documents) {
        this.documents = documents;
    }
}
