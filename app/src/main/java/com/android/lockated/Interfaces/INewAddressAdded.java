package com.android.lockated.Interfaces;

import com.android.lockated.model.MyAddressData;

import java.util.ArrayList;

public interface INewAddressAdded
{
    public void onNewAddressAdded(ArrayList<MyAddressData> addressDatas);
}
