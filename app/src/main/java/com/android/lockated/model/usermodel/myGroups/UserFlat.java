
package com.android.lockated.model.usermodel.myGroups;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


public class UserFlat implements Parcelable {

    @SerializedName("id")
    @Expose
    private int id;
    @SerializedName("flat")
    @Expose
    private String flat;
    @SerializedName("block")
    @Expose
    private String block;
    @SerializedName("residence_type")
    @Expose
    private String residenceType;
    @SerializedName("ownership")
    @Expose
    private String ownership;
    @SerializedName("intercom")
    @Expose
    private int intercom;
    @SerializedName("landline")
    @Expose
    private String landline;

    /**
     * @return The id
     */
    public int getId() {
        return id;
    }

    /**
     * @param id The id
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * @return The flat
     */
    public String getFlat() {
        return flat;
    }

    /**
     * @param flat The flat
     */
    public void setFlat(String flat) {
        this.flat = flat;
    }

    /**
     * @return The block
     */
    public String getBlock() {
        return block;
    }

    /**
     * @param block The block
     */
    public void setBlock(String block) {
        this.block = block;
    }

    /**
     * @return The residenceType
     */
    public String getResidenceType() {
        return residenceType;
    }

    /**
     * @param residenceType The residence_type
     */
    public void setResidenceType(String residenceType) {
        this.residenceType = residenceType;
    }

    /**
     * @return The ownership
     */
    public String getOwnership() {
        return ownership;
    }

    /**
     * @param ownership The ownership
     */
    public void setOwnership(String ownership) {
        this.ownership = ownership;
    }

    /**
     * @return The intercom
     */
    public int getIntercom() {
        return intercom;
    }

    /**
     * @param intercom The intercom
     */
    public void setIntercom(int intercom) {
        this.intercom = intercom;
    }

    /**
     * @return The landline
     */
    public String getLandline() {
        return landline;
    }

    /**
     * @param landline The landline
     */
    public void setLandline(String landline) {
        this.landline = landline;
    }

    @Override
    public int describeContents() {
        return this.hashCode();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(id);
        dest.writeString(flat);
        dest.writeString(block);
        dest.writeString(residenceType);
        dest.writeString(ownership);
        dest.writeInt(intercom);
        dest.writeString(landline);
    }

    private UserFlat(Parcel in) {
        id = in.readInt();
        flat = in.readString();
        block = in.readString();
        residenceType = in.readString();
        ownership = in.readString();
        intercom = in.readInt();
        landline = in.readString();
    }

    public static final Parcelable.Creator<UserFlat> CREATOR = new Parcelable.Creator<UserFlat>() {
        public UserFlat createFromParcel(Parcel in) {
            return new UserFlat(in);
        }

        public UserFlat[] newArray(int size) {
            return new UserFlat[size];
        }
    };

}
