
package com.android.lockated.model.usermodel.EventModel.MyEvent;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class Classified {

    @SerializedName("id")
    @Expose
    private int id;

    @SerializedName("id_society")
    @Expose
    private int idSociety;
    @SerializedName("description")
    @Expose
    private String description;
    @SerializedName("user_id")
    @Expose
    private int userId;
    @SerializedName("event_type")
    @Expose
    private String eventType;
    @SerializedName("event_name")
    @Expose
    private String eventName;
    @SerializedName("event_at")
    @Expose
    private String eventAt;
    @SerializedName("from_time")
    @Expose
    private String fromTime;
    @SerializedName("to_time")
    @Expose
    private String toTime;
    @SerializedName("created_at")
    @Expose
    private String createdAt;
    @SerializedName("action")
    @Expose
    private Object action;
    @SerializedName("IsDelete")
    @Expose
    private Object IsDelete;
    @SerializedName("canceled_by")
    @Expose
    private Object canceledBy;
    @SerializedName("canceler_id")
    @Expose
    private Object cancelerId;
    @SerializedName("comment")
    @Expose
    private Object comment;
    @SerializedName("documents")
    @Expose
    private ArrayList<DocumentsEvent> documentsEvent = new ArrayList<DocumentsEvent>();
    @SerializedName("shared")
    @Expose
    private int shared;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("user")
    @Expose
    private User user;

    /**
     * 
     * @return
     *     The id
     */
    public int getId() {
        return id;
    }

    /**
     * 
     * @param id
     *     The id
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * 
     * @return
     *     The idSociety
     */
    public int getIdSociety() {
        return idSociety;
    }

    /**
     * 
     * @param idSociety
     *     The id_society
     */
    public void setIdSociety(int idSociety) {
        this.idSociety = idSociety;
    }

    /**
     * 
     * @return
     *     The description
     */
    public String getDescription() {
        return description;
    }

    /**
     * 
     * @param description
     *     The description
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * 
     * @return
     *     The userId
     */
    public int getUserId() {
        return userId;
    }

    /**
     * 
     * @param userId
     *     The user_id
     */
    public void setUserId(int userId) {
        this.userId = userId;
    }
    public  ArrayList<DocumentsEvent> getDocumentsEvent() {
        return documentsEvent;
    }

    public void setDocumentsEvent(ArrayList<DocumentsEvent> documentsEvent) {
        this.documentsEvent = documentsEvent;
    }
    /**
     * 
     * @return
     *     The eventType
     */
    public String getEventType() {
        return eventType;
    }

    /**
     * 
     * @param eventType
     *     The event_type
     */
    public void setEventType(String eventType) {
        this.eventType = eventType;
    }

    /**
     * 
     * @return
     *     The eventName
     */
    public String getEventName() {
        return eventName;
    }

    /**
     * 
     * @param eventName
     *     The event_name
     */
    public void setEventName(String eventName) {
        this.eventName = eventName;
    }

    /**
     * 
     * @return
     *     The eventAt
     */
    public String getEventAt() {
        return eventAt;
    }

    /**
     * 
     * @param eventAt
     *     The event_at
     */
    public void setEventAt(String eventAt) {
        this.eventAt = eventAt;
    }

    /**
     * 
     * @return
     *     The fromTime
     */
    public String getFromTime() {
        return fromTime;
    }

    /**
     * 
     * @param fromTime
     *     The from_time
     */
    public void setFromTime(String fromTime) {
        this.fromTime = fromTime;
    }

    /**
     * 
     * @return
     *     The toTime
     */
    public String getToTime() {
        return toTime;
    }

    /**
     * 
     * @param toTime
     *     The to_time
     */
    public void setToTime(String toTime) {
        this.toTime = toTime;
    }

    /**
     * 
     * @return
     *     The createdAt
     */
    public String getCreatedAt() {
        return createdAt;
    }

    /**
     * 
     * @param createdAt
     *     The created_at
     */
    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    /**
     * 
     * @return
     *     The action
     */
    public Object getAction() {
        return action;
    }

    /**
     * 
     * @param action
     *     The action
     */
    public void setAction(Object action) {
        this.action = action;
    }

    /**
     * 
     * @return
     *     The IsDelete
     */
    public Object getIsDelete() {
        return IsDelete;
    }

    /**
     * 
     * @param IsDelete
     *     The IsDelete
     */
    public void setIsDelete(Object IsDelete) {
        this.IsDelete = IsDelete;
    }

    /**
     * 
     * @return
     *     The canceledBy
     */
    public Object getCanceledBy() {
        return canceledBy;
    }

    /**
     * 
     * @param canceledBy
     *     The canceled_by
     */
    public void setCanceledBy(Object canceledBy) {
        this.canceledBy = canceledBy;
    }

    /**
     * 
     * @return
     *     The cancelerId
     */
    public Object getCancelerId() {
        return cancelerId;
    }

    /**
     * 
     * @param cancelerId
     *     The canceler_id
     */
    public void setCancelerId(Object cancelerId) {
        this.cancelerId = cancelerId;
    }

    /**
     * 
     * @return
     *     The comment
     */
    public Object getComment() {
        return comment;
    }

    /**
     * 
     * @param comment
     *     The comment
     */
    public void setComment(Object comment) {
        this.comment = comment;
    }

    /**
     * 
     * @return
     *     The shared
     */
    public int getShared() {
        return shared;
    }

    /**
     * 
     * @param shared
     *     The shared
     */
    public void setShared(int shared) {
        this.shared = shared;
    }

    /**
     * 
     * @return
     *     The status
     */
    public String getStatus() {
        return status;
    }

    /**
     * 
     * @param status
     *     The status
     */
    public void setStatus(String status) {
        this.status = status;
    }

    /**
     * 
     * @return
     *     The user
     */
    public User getUser() {
        return user;
    }

    /**
     * 
     * @param user
     *     The user
     */
    public void setUser(User user) {
        this.user = user;
    }

}
