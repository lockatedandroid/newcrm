package com.android.lockated.location;

import android.content.Context;
import android.location.Location;
import android.os.Bundle;
import android.util.Log;

import com.android.lockated.Interfaces.ILocationUpdateListener;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;

public class LocationUpdateManager implements GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener, LocationListener {
    private Context mContext;
    private GoogleApiClient mGoogleApiClient;
    private LocationRequest mLocationRequest;

    private boolean isLocationUpdating;
    private boolean hasUpdated;
    private boolean hasResetLocation;
    private ILocationUpdateListener mILocationUpdateListener;

    public LocationUpdateManager(Context context, ILocationUpdateListener iLocationUpdateListener) {
        mContext = context;
        mILocationUpdateListener = iLocationUpdateListener;
    }

    public synchronized void buildGoogleApiClient() {
        mGoogleApiClient = new GoogleApiClient.Builder(mContext)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();

        createLocationRequest();
        mGoogleApiClient.connect();
    }

    public synchronized void buildGoogleApiClient(boolean isReset) {
        hasResetLocation = isReset;
        mGoogleApiClient = new GoogleApiClient.Builder(mContext)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();

        createLocationRequest();
        mGoogleApiClient.connect();
    }

    private boolean isPlayServiceAvailable() {
        int status = GoogleApiAvailability.getInstance().isGooglePlayServicesAvailable(mContext);
        switch (status) {
            case ConnectionResult.SUCCESS:
                return true;
            case ConnectionResult.SERVICE_VERSION_UPDATE_REQUIRED:
                return false;
        }
        return false;
    }

    protected void createLocationRequest() {
        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(7000);
        mLocationRequest.setFastestInterval(3000);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
    }

    public void startLocationUpdates() {
        try {
            if (hasResetLocation) {
                LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this);
            } else {
                Location mCurrentLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
                if (mCurrentLocation != null) {
                    mILocationUpdateListener.onLocationUpdate(mCurrentLocation);
                } else {
                    LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this);
                }
            }
        } catch (SecurityException ex) {
            ex.getMessage();
        }
    }

    public void stopLocationUpdates() {
        hasResetLocation = false;
        LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, this);
    }

    @Override
    public void onConnected(Bundle bundle) {
        if (bundle == null) {
            isLocationUpdating = true;
            startLocationUpdates();
        } else {
            isLocationUpdating = false;
            mILocationUpdateListener.onSuccessUpdate(bundle);
        }
    }

    @Override
    public void onConnectionSuspended(int i) {
        mGoogleApiClient.connect();
    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
        mILocationUpdateListener.onFailedUpdate(connectionResult);
    }

    @Override
    public void onLocationChanged(Location location) {
        if (hasUpdated) {
            hasUpdated = false;
            isLocationUpdating = false;
            mILocationUpdateListener.onLocationUpdate(location);
            stopLocationUpdates();
        } else {
            hasUpdated = true;
        }
    }

    public GoogleApiClient getGoogleApiClient() {
        return mGoogleApiClient;
    }

    public void releaseMemory() {
        mGoogleApiClient = null;
        mLocationRequest = null;
    }

    public boolean isLocationUpdating() {
        return isLocationUpdating;
    }

    public void setIsLocationUpdating(boolean isLocationUpdating) {
        this.isLocationUpdating = isLocationUpdating;
    }
}