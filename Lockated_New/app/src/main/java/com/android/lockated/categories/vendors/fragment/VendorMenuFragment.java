package com.android.lockated.categories.vendors.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.android.lockated.Interfaces.IRecyclerItemClickListener;
import com.android.lockated.LockatedApplication;
import com.android.lockated.R;
import com.android.lockated.categories.adapter.VendorMenuAdapter;
import com.android.lockated.categories.vendors.VendorActivity;
import com.android.lockated.categories.vendors.VendorProductActivity;
import com.android.lockated.categories.vendors.model.VendorProductData;
import com.android.lockated.network.ConnectionDetector;
import com.android.lockated.preferences.LockatedPreferences;
import com.android.lockated.request.LockatedJSONObjectRequest;
import com.android.lockated.request.LockatedRequestError;
import com.android.lockated.request.LockatedVolleyRequestQueue;
import com.android.lockated.utils.ApplicationURL;
import com.android.lockated.utils.Utilities;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class VendorMenuFragment extends Fragment implements Response.Listener, Response.ErrorListener, IRecyclerItemClickListener {
    private View mVendorMenuView;

    private RecyclerView mRecyclerView;
    private ProgressBar mProgressBarView;
    private TextView mTextViewVendorName;

    private int mId;
    private int mSectionId;
    private String mSectionName;
    private String screenName = "Vendor List";

    private LockatedPreferences mLockatedPreferences;
    private LockatedJSONObjectRequest mLockatedJSONObjectRequest;

    private ArrayList<VendorProductData> mVendorProductData;

    private VendorMenuAdapter mVendorMenuAdapter;

    private static final String GET_REQUEST_TAG = "VendorFragment:GetVendorMenu";

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mVendorMenuView = inflater.inflate(R.layout.fragment_vendor_menu, container, false);
        Utilities.ladooIntegration(getActivity(), screenName);
        Utilities.lockatedGoogleAnalytics(screenName, getString(R.string.visited), screenName);
        init(savedInstanceState);
        getVendorProducts();
        return mVendorMenuView;
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putInt("Id", mId);
        outState.putInt("SectionId", mSectionId);
        outState.putString("SectionName", mSectionName);
    }

    private void init(Bundle savedInstanceState) {
        if (getActivity() != null) {
            ((VendorActivity) getActivity()).toolbar.setVisibility(View.VISIBLE);
        }

        if (savedInstanceState == null) {
            mId = ((LockatedApplication) getActivity().getApplicationContext()).getmSectionIdTwo();
            mSectionId = ((LockatedApplication) getActivity().getApplicationContext()).getmSectionId();
            mSectionName = ((LockatedApplication) getActivity().getApplicationContext()).getSectionName();
        } else {
            mId = savedInstanceState.getInt("Id");
            mSectionId = savedInstanceState.getInt("SectionId");
            mSectionName = savedInstanceState.getString("SectionName");
        }

        mVendorProductData = new ArrayList<>();
        mLockatedPreferences = new LockatedPreferences(getActivity());

        mProgressBarView = (ProgressBar) mVendorMenuView.findViewById(R.id.mProgressBarView);
        mRecyclerView = (RecyclerView) mVendorMenuView.findViewById(R.id.mRecyclerView);

        mTextViewVendorName = (TextView) mVendorMenuView.findViewById(R.id.mTextViewVendorName);
        mTextViewVendorName.setText(mSectionName);

        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        mRecyclerView.setLayoutManager(mLayoutManager);
        mRecyclerView.setHasFixedSize(true);

        mVendorMenuAdapter = new VendorMenuAdapter(mVendorProductData, this);
        mRecyclerView.setAdapter(mVendorMenuAdapter);
    }

    private void getVendorProducts() {
        if (ConnectionDetector.isConnectedToInternet(getActivity())) {
            mProgressBarView.setVisibility(View.VISIBLE);
            String url = ApplicationURL.getSupplierProductUrl + mLockatedPreferences.getLockatedToken()
                    + "&id=" + mId + "&taxonomy_id=" + mSectionId;
            LockatedVolleyRequestQueue lockatedVolleyRequestQueue = LockatedVolleyRequestQueue.getInstance(getActivity());
            lockatedVolleyRequestQueue.sendRequest(GET_REQUEST_TAG, Request.Method.GET, url, null, this, this);
        } else {
            Utilities.showToastMessage(getActivity(), getActivity().getResources().getString(R.string.internet_connection_error));
        }
    }

    @Override
    public void onErrorResponse(VolleyError error) {
        if (getActivity() != null) {
            mProgressBarView.setVisibility(View.GONE);
            LockatedRequestError.onRequestError(getActivity(), error);
        }
    }

    @Override
    public void onResponse(Object response) {
        if (getActivity() != null) {
            mProgressBarView.setVisibility(View.GONE);
            JSONObject jsonObject = (JSONObject) response;
            try {
                JSONArray jsonArray = jsonObject.getJSONArray("supplier_products");
                if (jsonArray.length() > 0) {
                    mVendorProductData.clear();

                    for (int i = 0; i < jsonArray.length(); i++) {
                        JSONObject addressObject = jsonArray.getJSONObject(i);
                        VendorProductData vendorProductData = new VendorProductData(addressObject);
                        mVendorProductData.add(vendorProductData);
                    }
                } else {
                    Utilities.showToastMessage(getActivity(), getActivity().getString(R.string.menu_card_unavailable));
                }

                mVendorMenuAdapter.notifyDataSetChanged();
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void onRecyclerItemClick(View view, int position) {
        switch (view.getId()) {
            case R.id.mTextViewCategoryItem:
                onVendorMenuCategoryClicked(position);
                break;
        }
    }

    private void onVendorMenuCategoryClicked(int position) {
        Intent vendorProductIntent = new Intent(getActivity(), VendorProductActivity.class);
        ((LockatedApplication) getActivity().getApplicationContext()).setVendorProductData(mVendorProductData);
        ((LockatedApplication) getActivity().getApplicationContext()).setmSectionId(position);
        startActivity(vendorProductIntent);
    }
}