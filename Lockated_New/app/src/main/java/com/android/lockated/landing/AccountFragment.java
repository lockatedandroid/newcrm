package com.android.lockated.landing;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.android.lockated.R;
import com.android.lockated.account.adapter.AccountTabAdapter;
import com.android.lockated.component.LockatedPagerSlidingTabStrip;
import com.android.lockated.utils.Utilities;

import java.util.ArrayList;

public class AccountFragment extends Fragment {
    private View mHomeFragmentView;

    private ViewPager mViewPagerDetail;
    private LockatedPagerSlidingTabStrip mViewPagerSlidingTabs;

    private AccountTabAdapter mAccountTabAdapter;

    private ArrayList<String> accountTabs;

    public static final String REQUEST_TAG = "AccountFragment";

    int itemPosition = 0;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mHomeFragmentView = inflater.inflate(R.layout.fragment_account, container, false);
        init();
        Utilities.ladooIntegration(getActivity(), getString(R.string.account));
        return mHomeFragmentView;
    }

    public void setFragment(int pagerTab) {
        itemPosition = pagerTab;
    }

    @Override
    public void onResume() {
        super.onResume();
        Utilities.lockatedGoogleAnalytics(getString(R.string.account), getString(R.string.visited), getString(R.string.account));
    }

    private void init() {
        accountTabs = new ArrayList<>();
        accountTabs.add(getActivity().getString(R.string.personal_info));
        accountTabs.add(getActivity().getString(R.string.society_details));
        accountTabs.add(getActivity().getString(R.string.family_detail));
        accountTabs.add(getActivity().getString(R.string.my_account));
        accountTabs.add(getActivity().getString(R.string.my_orders));
        accountTabs.add(getActivity().getString(R.string.my_services));

        mViewPagerDetail = (ViewPager) mHomeFragmentView.findViewById(R.id.mViewPagerDetail);
        mViewPagerSlidingTabs = (LockatedPagerSlidingTabStrip) mHomeFragmentView.findViewById(R.id.mViewPagerSlidingTabs);

        mViewPagerSlidingTabs.setIndicatorColor(ContextCompat.getColor(getActivity(), R.color.primary));
        mViewPagerSlidingTabs.setTextColor(ContextCompat.getColor(getActivity(), R.color.primary_text));
        mViewPagerSlidingTabs.setActivateTextColor(ContextCompat.getColor(getActivity(), R.color.primary_text));
        mViewPagerSlidingTabs.setDeactivateTextColor(ContextCompat.getColor(getActivity(), R.color.primary_text));

        mAccountTabAdapter = new AccountTabAdapter(getChildFragmentManager(), accountTabs);
        mViewPagerDetail.setAdapter(mAccountTabAdapter);
        mViewPagerDetail.setCurrentItem(itemPosition);
        mViewPagerSlidingTabs.setViewPager(mViewPagerDetail);
    }
}
