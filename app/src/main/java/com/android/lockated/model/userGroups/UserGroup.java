
package com.android.lockated.model.userGroups;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;


public class UserGroup {

    @SerializedName("id")
    @Expose
    private int id;
    @SerializedName("user_id")
    @Expose
    private int userId;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("active")
    @Expose
    private Object active;
    @SerializedName("groupmembers")
    @Expose
    private ArrayList<Groupmember> groupmembers = new ArrayList<Groupmember>();
    @SerializedName("user")
    @Expose
    private UserSelf user;

    /**
     * @return The id
     */
    public int getId() {
        return id;
    }

    /**
     * @param id The id
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * @return The userId
     */
    public int getUserId() {
        return userId;
    }

    /**
     * @param userId The user_id
     */
    public void setUserId(int userId) {
        this.userId = userId;
    }

    /**
     * @return The name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name The name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return The active
     */
    public Object getActive() {
        return active;
    }

    /**
     * @param active The active
     */
    public void setActive(Object active) {
        this.active = active;
    }

    /**
     * @return The groupmembers
     */
    public ArrayList<Groupmember> getGroupmembers() {
        return groupmembers;
    }

    /**
     * @param groupmembers The groupmembers
     */
    public void setGroupmembers(ArrayList<Groupmember> groupmembers) {
        this.groupmembers = groupmembers;
    }

    /**
     * @return The user
     */
    public UserSelf getUser() {
        return user;
    }

    /**
     * @param user The user
     */
    public void setUser(UserSelf user) {
        this.user = user;
    }

}
