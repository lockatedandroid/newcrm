package com.android.lockated.Interfaces;

import com.android.lockated.model.FamilyData;

import java.util.ArrayList;

public interface IFamilyMemberAdded
{
    public void onNewMemberAdded(ArrayList<FamilyData> familyDataList, int requestId);
    public void onNewMemberUpdated(FamilyData familyData, int requestId, int position);
}
