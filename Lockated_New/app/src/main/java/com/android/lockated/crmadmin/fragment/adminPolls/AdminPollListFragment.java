package com.android.lockated.crmadmin.fragment.adminPolls;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import com.android.lockated.R;
import com.android.lockated.crm.adapters.CommanListViewAdapter;
import com.android.lockated.crmadmin.activity.AdminPollsActivity;
import com.android.lockated.utils.Utilities;

public class AdminPollListFragment extends Fragment implements AdapterView.OnItemClickListener {

    ListView listNotice;
    String[] nameList = {"Pending Polls", "Approved Polls"};
    CommanListViewAdapter commanListViewAdapter;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View allNoticeView = inflater.inflate(R.layout.fragment_admin_poll_list, container, false);
        init(allNoticeView);
        Utilities.ladooIntegration(getActivity(), "Admin Polls List");
        return allNoticeView;
    }

    private void init(View allNoticeView) {
        listNotice = (ListView) allNoticeView.findViewById(R.id.noticeList);
        commanListViewAdapter = new CommanListViewAdapter(getActivity(), nameList);
        listNotice.setAdapter(commanListViewAdapter);
        listNotice.setOnItemClickListener(this);
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        Intent intent = new Intent(getActivity(), AdminPollsActivity.class);
        intent.putExtra("AdminPollsActivity", position);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
    }

}
