package com.android.lockated.Interfaces;

public interface IGeocoderAddressUpdateListener
{
    public void onAddressUpdate(String address);
}
