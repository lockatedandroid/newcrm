package com.android.lockated.crmadmin.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import com.android.lockated.R;
import com.android.lockated.crm.adapters.CommanListViewAdapter;
import com.android.lockated.crmadmin.activity.AdminMyZoneDirectoryActivity;
import com.android.lockated.utils.Utilities;


public class AdminDirectorysFragment extends Fragment implements AdapterView.OnItemClickListener {

    ListView DirectoryfiedList;
    String[] nameList = {/*"Personal Directory",*/ "Society Directory", "Members Directory"};
    CommanListViewAdapter commanListViewAdapter;
    private View DirectorysFragmentView;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        Utilities.ladooIntegration(getActivity(), "Admin Directory List");
        DirectorysFragmentView = inflater.inflate(R.layout.fragment_directorys, container, false);
        DirectoryfiedList = (ListView) DirectorysFragmentView.findViewById(R.id.ClassifiedList);
        commanListViewAdapter = new CommanListViewAdapter(getActivity(), nameList);
        DirectoryfiedList.setAdapter(commanListViewAdapter);
        DirectoryfiedList.setOnItemClickListener(this);
        return DirectorysFragmentView;

    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

        Intent intent = new Intent(getContext(), AdminMyZoneDirectoryActivity.class);
        intent.putExtra("Listposition", position);
        startActivity(intent);
    }

}
