package com.android.lockated.crm.adapters;

import android.content.Context;
import android.content.Intent;
import android.support.v4.app.FragmentManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.lockated.R;
import com.android.lockated.crm.activity.CrmVisitorActivity;
import com.android.lockated.crm.activity.MyZoneActivity;
import com.android.lockated.crm.activity.VisitorDetailViewActivity;
import com.android.lockated.model.AdminModel.AdminGatekeeper.GatekeeperAdminModel;
import com.android.lockated.preferences.LockatedPreferences;
import com.android.lockated.request.LockatedJSONObjectRequest;
import com.android.lockated.request.LockatedRequestError;
import com.android.lockated.request.LockatedVolleyRequestQueue;
import com.android.lockated.utils.ApplicationURL;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class VisitorAdapter extends BaseAdapter implements View.OnClickListener, Response.ErrorListener, Response.Listener<JSONObject> {
    public static final String REQUEST_TAG = "VisitorCornerFragment";
    private final ArrayList<GatekeeperAdminModel> arrayList;
    private final String EDIT;
    private final String DESTROY;
    TextView errorMsg;
    Context contextMain;
    JSONArray jsonArray;
    LayoutInflater layoutInflater;
    FragmentManager fragmentManager;
    private RequestQueue mQueue;
    private LockatedPreferences mLockatedPreferences;

    public VisitorAdapter(Context context, ArrayList<GatekeeperAdminModel> arrayList, String EDIT, String DESTROY) {
        this.contextMain = context;
        this.EDIT = EDIT;
        this.DESTROY = DESTROY;
        this.arrayList = arrayList;
        layoutInflater = LayoutInflater.from(context);
    }

    @Override
    public int getCount() {
        return arrayList.size();
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        ViewHolder holder = null;
        if (convertView == null) {
            convertView = layoutInflater.inflate(R.layout.visitorchild, parent, false);
            holder = new ViewHolder();
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        holder.mTextApprove = (TextView) convertView.findViewById(R.id.mTextApprove);
        holder.mImageEdit = (ImageView) convertView.findViewById(R.id.mImageEdit);
        holder.mImageDelete = (ImageView) convertView.findViewById(R.id.mImageDelete);
        holder.mTextDateails = (TextView) convertView.findViewById(R.id.mTextDateails);
        holder.txtTitle = (TextView) convertView.findViewById(R.id.mTextVisitorName);
        holder.txtnumber = (TextView) convertView.findViewById(R.id.mTextDate);
        holder.mStatus = (TextView) convertView.findViewById(R.id.mStatus);
        holder.textAcompany = (TextView) convertView.findViewById(R.id.textAcompany);
        holder.textReject = (TextView) convertView.findViewById(R.id.textReject);

        if (EDIT != null && EDIT.equals("true")) {
            holder.mImageEdit.setVisibility(View.VISIBLE);
        } else {
            holder.mImageEdit.setVisibility(View.INVISIBLE);
        }
        if (DESTROY != null && DESTROY.equals("true")) {
            holder.mImageDelete.setVisibility(View.VISIBLE);
        } else {
            holder.mImageDelete.setVisibility(View.INVISIBLE);
        }
        try {
            String accompany = arrayList.get(position).getAccompany();
            String approve = "" + arrayList.get(position).getApprove();
            switch (approve) {
                case "0":
                    holder.mTextApprove.setVisibility(View.VISIBLE);
                    holder.textReject.setVisibility(View.VISIBLE);
                    holder.textAcompany.setVisibility(View.VISIBLE);
                    holder.mStatus.setText(contextMain.getResources().getString(R.string.status_pending));
                    break;
                case "1":
                    holder.mTextApprove.setVisibility(View.GONE);
                    holder.textReject.setVisibility(View.GONE);
                    holder.textAcompany.setVisibility(View.GONE);
                    if (accompany != null && accompany.equals("1")) {
                        holder.mStatus.setText(contextMain.getResources().getString(R.string.status_accompany));
                    } else {
                        holder.mStatus.setText(contextMain.getResources().getString(R.string.status_approve));
                    }
                    break;
                case "2":
                    holder.mTextApprove.setVisibility(View.GONE);
                    holder.textReject.setVisibility(View.GONE);
                    holder.textAcompany.setVisibility(View.GONE);
                    holder.mStatus.setText(contextMain.getResources().getString(R.string.status_reject));
                    break;
            }
            holder.textReject.setTag(position);
            holder.textReject.setOnClickListener(this);
            holder.mTextApprove.setTag(position);
            holder.mTextApprove.setOnClickListener(this);
            holder.txtTitle.setText(arrayList.get(position).getGuestName());
            holder.txtnumber.setText((arrayList.get(position).getGuestNumber()));
        } catch (Exception e) {
            e.printStackTrace();
        }
        holder.textAcompany.setTag(position);
        holder.textAcompany.setOnClickListener(this);

        holder.mImageDelete.setTag(position);
        holder.mImageDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int position = (int) v.getTag();
                onVisitorDelete(String.valueOf(arrayList.get(position).getId()));
            }
        });
        holder.mTextDateails.setTag(position);
        holder.mTextDateails.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int position = (int) v.getTag();
                String pid = null;
                Intent intent = new Intent(contextMain, VisitorDetailViewActivity.class);
                intent.putExtra("pid", pid);
                intent.putExtra("id", "" + arrayList.get(position).getId());
                intent.putExtra("approve", "" + arrayList.get(position).getApprove());
                intent.putExtra("strName", arrayList.get(position).getGuestName());
                intent.putExtra("strNumber", "" + arrayList.get(position).getGuestNumber());
                intent.putExtra("strExpDate", arrayList.get(position).getExpectedAt());
                String strVehiclenum = arrayList.get(position).getGuestVehicleNumber();
                if (!strVehiclenum.equals("")) {
                    intent.putExtra("strVehicleNumber", arrayList.get(position).getGuestVehicleNumber());
                } else {
                    intent.putExtra("strVehicleNumber", "Not Available");
                }
                if (arrayList.get(position).getDocuments() != null && arrayList.get(position).getDocuments().size() > 0) {
                    intent.putExtra("doc_id", "" + arrayList.get(position).getDocuments().get(0).getId());
                    intent.putExtra("visitorImage", arrayList.get(position).getDocuments().get(0).getDocument());
                } else {
                    intent.putExtra("doc_id", "No Id");
                    intent.putExtra("visitorImage", "No Document");
                }
                intent.putExtra("strPurpose", arrayList.get(position).getVisitPurpose());
                intent.putExtra("strMember", "" + arrayList.get(position).getPlusPerson());
                /*intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);*/
                contextMain.startActivity(intent);

            }
        });
        holder.mImageEdit.setTag(position);
        holder.mImageEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int position = (int) v.getTag();
                Intent intent = new Intent(contextMain, VisitorDetailViewActivity.class);
                intent.putExtra("pid", "" + arrayList.get(position).getId());
                intent.putExtra("id", "" + arrayList.get(position).getId());
                intent.putExtra("approve", "" + arrayList.get(position).getApprove());
                intent.putExtra("strName", arrayList.get(position).getGuestName());
                intent.putExtra("strNumber", "" + arrayList.get(position).getGuestNumber());
                intent.putExtra("strExpDate", arrayList.get(position).getExpectedAt());
                String strVehiclenum = arrayList.get(position).getGuestVehicleNumber();
                if (!strVehiclenum.equals("")) {
                    intent.putExtra("strVehicleNumber", arrayList.get(position).getGuestVehicleNumber());
                } else {
                    intent.putExtra("strVehicleNumber", "Not Available");
                }
                if (arrayList.get(position).getDocuments() != null && arrayList.get(position).getDocuments().size() > 0) {
                    intent.putExtra("doc_id", "" + arrayList.get(position).getDocuments().get(0).getId());
                    intent.putExtra("visitorImage", arrayList.get(position).getDocuments().get(0).getDocument());
                } else {
                    intent.putExtra("doc_id", "No Id");
                    intent.putExtra("visitorImage", "No Document");
                }
                intent.putExtra("strPurpose", arrayList.get(position).getVisitPurpose());
                intent.putExtra("strMember", "" + arrayList.get(position).getPlusPerson());
                contextMain.startActivity(intent);
            }
        });
        return convertView;
    }

    private void onVisitorDelete(String pid) {
        mLockatedPreferences = new LockatedPreferences(contextMain);
        if (contextMain != null) {
            if (!mLockatedPreferences.getSocietyId().equals("blank")) {
                if (pid != null) {
                    try {
                        mQueue = LockatedVolleyRequestQueue.getInstance(contextMain).getRequestQueue();
                        LockatedJSONObjectRequest lockatedJSONObjectRequest = new LockatedJSONObjectRequest(Request.Method.DELETE,
                                ApplicationURL.deleteVisitor + pid + ".json?token="
                                        + mLockatedPreferences.getLockatedToken(), null, new Response.Listener<JSONObject>() {
                            @Override
                            public void onResponse(JSONObject response) {
                                if (response != null && response.length() > 0) {

                                    if (response.has("code") && response.has("message")) {
                                        Intent intent = new Intent(contextMain, MyZoneActivity.class);
                                        intent.putExtra("MyZonePosition", 0);
                                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                        contextMain.startActivity(intent);
                                    }
                                } else {
                                    errorMsg.setVisibility(View.VISIBLE);
                                    errorMsg.setText(R.string.no_data_error);
                                }
                            }
                        }, new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                if (contextMain != null) {
                                    LockatedRequestError.onRequestError(contextMain, error);
                                }
                            }
                        });
                        lockatedJSONObjectRequest.setTag(REQUEST_TAG);
                        mQueue.add(lockatedJSONObjectRequest);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        }
    }

    @Override
    public void onClick(View v) {
        int position = (int) v.getTag();
        switch (v.getId()) {
            case R.id.mTextApprove:
                onApprovedClicked(arrayList.get(position).getId());
                break;
            case R.id.textAcompany:
                onAcompanyClicked(arrayList.get(position).getId());
                break;
            case R.id.textReject:
                onRejectClicked(arrayList.get(position).getId());
                break;
        }
    }

    private void onApprovedClicked(int approve) {
        mLockatedPreferences = new LockatedPreferences(contextMain);
        JSONObject jsonObjectMain = new JSONObject();
        try {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("approve", "1");
            jsonObjectMain.put("gatekeeper", jsonObject);

        } catch (JSONException e) {
            e.printStackTrace();
        }
        mQueue = LockatedVolleyRequestQueue.getInstance(contextMain).getRequestQueue();
        String url = ApplicationURL.UpdateGateKeeper + approve + ".json?token="
                + mLockatedPreferences.getLockatedToken();
        LockatedJSONObjectRequest lockatedJSONObjectRequest = new LockatedJSONObjectRequest(Request.Method.PUT,
                url,
                jsonObjectMain, this, this);
        lockatedJSONObjectRequest.setTag(REQUEST_TAG);
        mQueue.add(lockatedJSONObjectRequest);
    }

    private void onAcompanyClicked(int approve) {
        mLockatedPreferences = new LockatedPreferences(contextMain);
        JSONObject jsonObjectMain = new JSONObject();
        try {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("approve", "1");
            jsonObject.put("accompany", "1");
            jsonObjectMain.put("gatekeeper", jsonObject);

        } catch (JSONException e) {
            e.printStackTrace();
        }
        mQueue = LockatedVolleyRequestQueue.getInstance(contextMain).getRequestQueue();
        String url = ApplicationURL.UpdateGateKeeper + approve + ".json?token="
                + mLockatedPreferences.getLockatedToken();
        LockatedJSONObjectRequest lockatedJSONObjectRequest = new LockatedJSONObjectRequest(Request.Method.PUT,
                url,
                jsonObjectMain, this, this);
        lockatedJSONObjectRequest.setTag(REQUEST_TAG);
        mQueue.add(lockatedJSONObjectRequest);
    }

    private void onRejectClicked(int approve) {
        mLockatedPreferences = new LockatedPreferences(contextMain);
        JSONObject jsonObjectMain = new JSONObject();
        try {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("approve", "2");
            jsonObjectMain.put("gatekeeper", jsonObject);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        mQueue = LockatedVolleyRequestQueue.getInstance(contextMain).getRequestQueue();
        String url = ApplicationURL.UpdateGateKeeper + approve + ".json?token="
                + mLockatedPreferences.getLockatedToken();
        LockatedJSONObjectRequest lockatedJSONObjectRequest = new LockatedJSONObjectRequest(Request.Method.PUT,
                url,
                jsonObjectMain, this, this);
        lockatedJSONObjectRequest.setTag(REQUEST_TAG);
        mQueue.add(lockatedJSONObjectRequest);
    }

    @Override
    public void onErrorResponse(VolleyError error) {
        LockatedRequestError.onRequestError(contextMain, error);
    }

    @Override
    public void onResponse(JSONObject response) {
        if (response.has("id")) {
            Intent intent = new Intent(contextMain, CrmVisitorActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            contextMain.startActivity(intent);
        }
    }

    private class ViewHolder {
        ImageView mImageEdit, mImageDelete;
        TextView txtTitle, mTextDateails, mTextApprove, mTextReject, mStatus, textAcompany, textReject;
        TextView txtnumber;
    }
}