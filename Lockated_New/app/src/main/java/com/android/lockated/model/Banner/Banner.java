package com.android.lockated.model.Banner;

import org.json.JSONObject;

public class Banner {

    //("id")
    private int id;
    //("bimage")
    private String bimage;
    //("btype")
    private String btype;
    //("btype_id")
    private int btypeId;
    //("active")
    private int active;
    //("expiry")
    private Object expiry;
    //("taxon_id")
    private int taxonId;
    //("rediurl")
    private String rediurl;
    //("title")
    private String title;
    //("description")
    private String description;
    //("url")
    private String url;
    //("medium")
    private String medium;
    //("thumb")
    private String thumb;
    //("original")
    private String original;

    public Banner(JSONObject jsonObject) {
        id = jsonObject.optInt("id");
        bimage = jsonObject.optString("bimage");
        btype = jsonObject.optString("btype");
        btypeId = jsonObject.optInt("btype_id");
        active = jsonObject.optInt("active");
        expiry = jsonObject.opt("expiry");
        taxonId = jsonObject.optInt("taxon_id");
        rediurl = jsonObject.optString("rediurl");
        title = jsonObject.optString("title");
        description = jsonObject.optString("description");
        url = jsonObject.optString("url");
        medium = jsonObject.optString("medium");
        thumb = jsonObject.optString("thumb");
        original = jsonObject.optString("original");
    }

    public int getId() {
        return id;
    }

    public String getBimage() {
        return bimage;
    }

    public String getBtype() {
        return btype;
    }

    public int getBtypeId() {
        return btypeId;
    }

    public int getActive() {
        return active;
    }

    public Object getExpiry() {
        return expiry;
    }

    public int getTaxonId() {
        return taxonId;
    }

    public String getRediurl() {
        return rediurl;
    }

    public String getTitle() {
        return title;
    }

    public String getDescription() {
        return description;
    }

    public String getUrl() {
        return url;
    }

    public String getMedium() {
        return medium;
    }

    public String getThumb() {
        return thumb;
    }

    public String getOriginal() {
        return original;
    }

}