
package com.android.lockated.model.AdminModel.AdminFacilities;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class FacilityList {

    @SerializedName("code")
    @Expose
    private int code;
    @SerializedName("amenities")
    @Expose
    private List<AdminAmenity> amenities = new ArrayList<AdminAmenity>();

    /**
     * 
     * @return
     *     The code
     */
    public int getCode() {
        return code;
    }

    /**
     * 
     * @param code
     *     The code
     */
    public void setCode(int code) {
        this.code = code;
    }

    /**
     * 
     * @return
     *     The amenities
     */
    public List<AdminAmenity> getAmenities() {
        return amenities;
    }

    /**
     * 
     * @param amenities
     *     The amenities
     */
    public void setAmenities(List<AdminAmenity> amenities) {
        this.amenities = amenities;
    }

}
