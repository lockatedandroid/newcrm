package com.android.lockated.account.fragment;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v4.content.ContextCompat;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import com.android.lockated.Interfaces.IGeneralDetailAdded;
import com.android.lockated.R;
import com.android.lockated.model.GeneralDetailData;
import com.android.lockated.network.ConnectionDetector;
import com.android.lockated.preferences.LockatedPreferences;
import com.android.lockated.request.LockatedRequestError;
import com.android.lockated.request.LockatedVolleyRequestQueue;
import com.android.lockated.utils.ApplicationURL;
import com.android.lockated.utils.Utilities;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;

import org.json.JSONException;
import org.json.JSONObject;

public class AddGeneralDetailFragment extends DialogFragment implements View.OnClickListener, Response.Listener, Response.ErrorListener, AdapterView.OnItemSelectedListener
{
    private TextView mButtonAddAddress;
    private TextView mButtonAddAddressCancel;

    private TextView mTextViewGeneralDocName;
    private TextView mTextViewGeneralDocNumber;
    private TextView mTextViewGeneralEmergencyPerson;
    private TextView mTextViewGeneralEmergencyNumber;
    private TextView mTextViewGeneralVehicleNumber;

    private Spinner mSpinnerGeneralDetailPets;
    private Spinner mSpinnerGeneralDetailVehicle;

    private EditText mEditTextGeneralDocName;
    private EditText mEditTextGeneralDocNumber;
    private EditText mEditTextGeneralEmergencyPersonName;
    private EditText mEditTextGeneralEmergencyNumber;
    private EditText mEditTextGeneralVehicleNumber;

    private int requestId;
    private int mGeneralPets;
    private int mGeneralVehicleType;

    private GeneralDetailData mGeneralDetailData;
    private IGeneralDetailAdded mIGeneralDetailAdded;

    private RequestQueue mQueue;
    private ProgressDialog mProgressDialog;
    private LockatedPreferences mLockatedPreferences;

    private View mAddGeneralDetailView;

    public static final String REQUEST_TAG = "AddGeneralDetailFragment";

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setStyle(STYLE_NO_TITLE, 0);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        mAddGeneralDetailView = inflater.inflate(R.layout.fragment_add_new_general_detail, container);
        init();
        return mAddGeneralDetailView;
    }

    @Override
    public void onStart()
    {
        super.onStart();
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(getDialog().getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        getDialog().getWindow().setAttributes(lp);
    }

    public void setGeneralDataOnEdit(int id, GeneralDetailData data)
    {
        requestId = id;
        mGeneralDetailData = data;
    }

    public void setListener(IGeneralDetailAdded listener)
    {
        mIGeneralDetailAdded = listener;
    }

    private void init()
    {
        mLockatedPreferences = new LockatedPreferences(getActivity());

        mButtonAddAddressCancel = (TextView) mAddGeneralDetailView.findViewById(R.id.mButtonAddAddressCancel);
        mButtonAddAddress = (TextView) mAddGeneralDetailView.findViewById(R.id.mButtonAddAddress);

        mTextViewGeneralDocName = (TextView) mAddGeneralDetailView.findViewById(R.id.mTextViewGeneralDocName);
        mTextViewGeneralDocNumber = (TextView) mAddGeneralDetailView.findViewById(R.id.mTextViewGeneralDocNumber);
        mTextViewGeneralEmergencyPerson = (TextView) mAddGeneralDetailView.findViewById(R.id.mTextViewGeneralEmergencyPerson);
        mTextViewGeneralEmergencyNumber = (TextView) mAddGeneralDetailView.findViewById(R.id.mTextViewGeneralEmergencyNumber);
        mTextViewGeneralVehicleNumber = (TextView) mAddGeneralDetailView.findViewById(R.id.mTextViewGeneralVehicleNumber);

        mSpinnerGeneralDetailPets = (Spinner) mAddGeneralDetailView.findViewById(R.id.mSpinnerGeneralDetailPets);
        mSpinnerGeneralDetailVehicle = (Spinner) mAddGeneralDetailView.findViewById(R.id.mSpinnerGeneralDetailVehicle);

        mEditTextGeneralDocName = (EditText) mAddGeneralDetailView.findViewById(R.id.mEditTextGeneralDocName);
        mEditTextGeneralDocNumber = (EditText) mAddGeneralDetailView.findViewById(R.id.mEditTextGeneralDocNumber);
        mEditTextGeneralEmergencyPersonName = (EditText) mAddGeneralDetailView.findViewById(R.id.mEditTextGeneralEmergencyPersonName);
        mEditTextGeneralEmergencyNumber = (EditText) mAddGeneralDetailView.findViewById(R.id.mEditTextGeneralEmergencyNumber);
        mEditTextGeneralVehicleNumber = (EditText) mAddGeneralDetailView.findViewById(R.id.mEditTextGeneralVehicleNumber);

        if (requestId == 0)
        {
            mButtonAddAddress.setText(R.string.add);
            disableAddButton();
        }
        else
        {
            mButtonAddAddress.setText(R.string.update);
            enableAddButton();
        }

        mButtonAddAddress.setOnClickListener(this);
        mButtonAddAddressCancel.setOnClickListener(this);

        mSpinnerGeneralDetailPets.setOnItemSelectedListener(this);
        mSpinnerGeneralDetailVehicle.setOnItemSelectedListener(this);

        setEditTextListener();
        setEditedData();
    }

    private void setEditedData()
    {
        if (requestId == 1)
        {
            int vehicleType = 0;
            mSpinnerGeneralDetailPets.setSelection(mGeneralDetailData.getPets());
            switch (mGeneralDetailData.getVehicle())
            {
                case 0:
                    vehicleType = 0;
                    break;
                case 2:
                    vehicleType = 1;
                    break;
                case 4:
                    vehicleType = 2;
                    break;
            }

            mSpinnerGeneralDetailVehicle.setSelection(vehicleType);

            mEditTextGeneralDocName.setText(mGeneralDetailData.getDoc_name());
            mEditTextGeneralDocName.setSelection(mEditTextGeneralDocName.getText().toString().length());

            mEditTextGeneralDocNumber.setText(mGeneralDetailData.getDoc_phone());
            mEditTextGeneralDocNumber.setSelection(mEditTextGeneralDocNumber.getText().toString().length());

            mEditTextGeneralEmergencyPersonName.setText(mGeneralDetailData.getEmergency_contact_person());
            mEditTextGeneralEmergencyPersonName.setSelection(mEditTextGeneralEmergencyPersonName.getText().toString().length());

            mEditTextGeneralEmergencyNumber.setText(mGeneralDetailData.getEcp_phone());
            mEditTextGeneralEmergencyNumber.setSelection(mEditTextGeneralEmergencyNumber.getText().toString().length());

            mEditTextGeneralVehicleNumber.setText(mGeneralDetailData.getVeh_reg_number());
            mEditTextGeneralVehicleNumber.setSelection(mEditTextGeneralVehicleNumber.getText().toString().length());
        }
    }

    private void showVehicleNumber()
    {
        mTextViewGeneralVehicleNumber.setVisibility(View.VISIBLE);
    }

    private void hideVehicleNumber()
    {
        mTextViewGeneralVehicleNumber.setVisibility(View.GONE);
    }

    private void showEmergencyNumber()
    {
        mTextViewGeneralEmergencyNumber.setVisibility(View.VISIBLE);
    }

    private void hideEmergencyNumber()
    {
        mTextViewGeneralEmergencyNumber.setVisibility(View.GONE);
    }

    private void showEmergencyPerson()
    {
        mTextViewGeneralEmergencyPerson.setVisibility(View.VISIBLE);
    }

    private void hideEmergencyPerson()
    {
        mTextViewGeneralEmergencyPerson.setVisibility(View.GONE);
    }

    private void showDocName()
    {
        mTextViewGeneralDocName.setVisibility(View.VISIBLE);
    }

    private void hideDocName()
    {
        mTextViewGeneralDocName.setVisibility(View.GONE);
    }

    private void showDocNumber()
    {
        mTextViewGeneralDocNumber.setVisibility(View.VISIBLE);
    }

    private void hideDocNumber()
    {
        mTextViewGeneralDocNumber.setVisibility(View.GONE);
    }

    private void disableAddButton()
    {
        mButtonAddAddress.setEnabled(false);
        mButtonAddAddress.setTextColor(ContextCompat.getColor(getActivity(), R.color.secondary_text));
    }

    private void enableAddButton()
    {
        mButtonAddAddress.setEnabled(true);
        mButtonAddAddress.setTextColor(ContextCompat.getColor(getActivity(), R.color.primary));
    }

    private void setEditTextListener()
    {
        mEditTextGeneralDocName.addTextChangedListener(new TextWatcher()
        {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after)
            {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count)
            {
                if (s.length() > 0)
                {
                    showDocName();

                    if (mEditTextGeneralDocNumber.getText().toString().length() > 0 && mEditTextGeneralEmergencyPersonName.getText().toString().length() > 0 && mEditTextGeneralEmergencyNumber.getText().toString().length() > 0)
                    {
                        enableAddButton();
                    }
                } else
                {
                    disableAddButton();
                    hideDocName();
                }
            }

            @Override
            public void afterTextChanged(Editable s)
            {

            }
        });

        mEditTextGeneralDocNumber.addTextChangedListener(new TextWatcher()
        {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after)
            {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count)
            {
                if (s.length() > 0)
                {
                    showDocNumber();

                    if (mEditTextGeneralDocName.getText().toString().length() > 0 && mEditTextGeneralEmergencyPersonName.getText().toString().length() > 0 && mEditTextGeneralEmergencyNumber.getText().toString().length() > 0)
                    {
                        enableAddButton();
                    }
                } else
                {
                    disableAddButton();
                    hideDocNumber();
                }
            }

            @Override
            public void afterTextChanged(Editable s)
            {

            }
        });

        mEditTextGeneralEmergencyPersonName.addTextChangedListener(new TextWatcher()
        {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after)
            {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count)
            {
                if (s.length() > 0)
                {
                    showEmergencyPerson();

                    if (mEditTextGeneralDocName.getText().toString().length() > 0 && mEditTextGeneralDocNumber.getText().toString().length() > 0 && mEditTextGeneralEmergencyNumber.getText().toString().length() > 0)
                    {
                        enableAddButton();
                    }
                } else
                {
                    disableAddButton();
                    hideEmergencyPerson();
                }
            }

            @Override
            public void afterTextChanged(Editable s)
            {

            }
        });

        mEditTextGeneralEmergencyNumber.addTextChangedListener(new TextWatcher()
        {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after)
            {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count)
            {
                if (s.length() > 0)
                {
                    showEmergencyNumber();

                    if (mEditTextGeneralDocName.getText().toString().length() > 0 && mEditTextGeneralDocNumber.getText().toString().length() > 0 && mEditTextGeneralEmergencyPersonName.getText().toString().length() > 0)
                    {
                        enableAddButton();
                    }
                } else
                {
                    disableAddButton();
                    hideEmergencyNumber();
                }
            }

            @Override
            public void afterTextChanged(Editable s)
            {

            }
        });

        mEditTextGeneralVehicleNumber.addTextChangedListener(new TextWatcher()
        {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after)
            {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count)
            {
                if (s.length() > 0)
                {
                    showVehicleNumber();
                } else
                {
                    hideVehicleNumber();
                }
            }

            @Override
            public void afterTextChanged(Editable s)
            {

            }
        });
    }

    private void onAddGeneralInfoButtonClicked()
    {
        String strGeneralDocName = mEditTextGeneralDocName.getText().toString();
        String strGeneralDocNumber = mEditTextGeneralDocNumber.getText().toString();
        String strGeneralEmergencyPerson = mEditTextGeneralEmergencyPersonName.getText().toString();
        String strGeneralEmergencyNumber = mEditTextGeneralEmergencyNumber.getText().toString();
        String strGeneralVehicleNumber = mEditTextGeneralVehicleNumber.getText().toString();

        boolean addAddressCancel = false;
        View addAddressFocusView = null;
        String message = getActivity().getResources().getString(R.string.signup_blank_field_error);

        if (TextUtils.isEmpty(strGeneralDocName))
        {
            addAddressFocusView = mEditTextGeneralDocName;
            addAddressCancel = true;
            message = getActivity().getResources().getString(R.string.signup_blank_field_error);
        }

        if (TextUtils.isEmpty(strGeneralDocNumber))
        {
            addAddressFocusView = mEditTextGeneralDocNumber;
            addAddressCancel = true;
            message = getString(R.string.mobile_number_error);
        }

        if (TextUtils.isEmpty(strGeneralEmergencyPerson))
        {
            addAddressFocusView = mEditTextGeneralEmergencyPersonName;
            addAddressCancel = true;
            message = getString(R.string.signup_blank_field_error);
        }

        if (TextUtils.isEmpty(strGeneralEmergencyNumber))
        {
            addAddressFocusView = mEditTextGeneralEmergencyNumber;
            addAddressCancel = true;
            message = getString(R.string.mobile_number_error);
        }

        if (!TextUtils.isEmpty(strGeneralVehicleNumber))
        {
            if (mGeneralVehicleType == 0)
            {
                addAddressFocusView = mEditTextGeneralVehicleNumber;
                addAddressCancel = true;
                message = getString(R.string.select_vehicle_type);
            }
        }

        if (mGeneralVehicleType != 0)
        {
            if (TextUtils.isEmpty(strGeneralVehicleNumber))
            {
                addAddressFocusView = mEditTextGeneralVehicleNumber;
                addAddressCancel = true;
                message = getString(R.string.enter_vehicle_number);
            }
        }

        if (addAddressCancel)
        {
            addAddressFocusView.requestFocus();
            Utilities.showToastMessage(getActivity(), message);
        }
        else
        {
            if (ConnectionDetector.isConnectedToInternet(getActivity()))
            {
                mProgressDialog = ProgressDialog.show(getActivity(), "", "Please Wait...", false);
                mProgressDialog.show();

                JSONObject jsonObject = new JSONObject();

                try
                {
                    jsonObject.put("token", mLockatedPreferences.getLockatedToken());
                    jsonObject.put("doc_name", strGeneralDocName);
                    jsonObject.put("doc_phone", strGeneralDocNumber);
                    jsonObject.put("emergency_contact_person", strGeneralEmergencyPerson);
                    jsonObject.put("ecp_phone", strGeneralEmergencyNumber);
                    jsonObject.put("pets", String.valueOf(mGeneralPets));
                    jsonObject.put("vehicle", String.valueOf(mGeneralVehicleType));
                    jsonObject.put("veh_reg_number", strGeneralVehicleNumber);
                }
                catch (JSONException e)
                {
                    e.printStackTrace();
                }

//                int socketTimeout = Utilities.REQUEST_TIME_OUT;
//                mQueue = LockatedVolleyRequestQueue.getInstance(getActivity()).getRequestQueue();
//                LockatedJSONObjectRequest lockatedJSONObjectRequest;
//                if (requestId == 1)
//                {
//                    lockatedJSONObjectRequest = new LockatedJSONObjectRequest(Request.Method.PUT, ApplicationURL.updateGeneralDetailUrl+mGeneralDetailData.getId()+".json", jsonObject, this, this);
//                } else
//                {
//                    lockatedJSONObjectRequest = new LockatedJSONObjectRequest(Request.Method.POST, ApplicationURL.addGeneralDetailUrl, jsonObject, this, this);
//                }
//                lockatedJSONObjectRequest.setTag(REQUEST_TAG);
//                RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
//                lockatedJSONObjectRequest.setRetryPolicy(policy);
//                mQueue.add(lockatedJSONObjectRequest);

                LockatedVolleyRequestQueue lockatedVolleyRequestQueue = LockatedVolleyRequestQueue.getInstance(getActivity());
                if (requestId == 1)
                {
                    lockatedVolleyRequestQueue.sendRequest(REQUEST_TAG, Request.Method.PUT, ApplicationURL.updateGeneralDetailUrl+mGeneralDetailData.getId()+".json", jsonObject, this, this);
                } else
                {
                    lockatedVolleyRequestQueue.sendRequest(REQUEST_TAG, Request.Method.POST, ApplicationURL.addGeneralDetailUrl, jsonObject, this, this);
                }
            } else
            {
                Utilities.showToastMessage(getActivity(), getActivity().getResources().getString(R.string.internet_connection_error));
            }
        }
    }

    @Override
    public void onClick(View v)
    {
        switch (v.getId())
        {
            case R.id.mButtonAddAddress:
                if (getDialog() != null)
                {
                    onAddGeneralInfoButtonClicked();
                }
                break;
            case R.id.mButtonAddAddressCancel:
                if (getDialog() != null)
                {
                    getDialog().dismiss();
                }
                break;
        }
    }

    @Override
    public void onErrorResponse(VolleyError error)
    {
        mProgressDialog.dismiss();
        if (getActivity() != null)
        {
            LockatedRequestError.onRequestError(getActivity(), error);
        }
    }

    @Override
    public void onResponse(Object response)
    {
        mProgressDialog.dismiss();
        if (getActivity() != null)
        {
            if (getDialog() != null)
            {
                JSONObject jsonObject = (JSONObject) response;
                try
                {
                    mGeneralDetailData = new GeneralDetailData(jsonObject);
                    mIGeneralDetailAdded.onGeneralDetailAdded(mGeneralDetailData, requestId);
                }
                catch (Exception e)
                {
                    e.printStackTrace();
                }
                getDialog().dismiss();
            }
        }
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id)
    {
        switch (parent.getId())
        {
            case R.id.mSpinnerGeneralDetailVehicle:
                if (position == 0)
                {
                    mGeneralVehicleType = 0;
                }
                else if (position == 1)
                {
                    mGeneralVehicleType = 2;
                }
                else
                {
                    mGeneralVehicleType = 4;
                }
                break;
            case R.id.mSpinnerGeneralDetailPets:
                mGeneralPets = position;
                break;
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent)
    {

    }
}
