package com.android.lockated.categories.services.model;

import org.json.JSONObject;

public class ServiceAttributeData
{
    private int id;
    private String name;
    private int service_metadata_id;
    private int active;
    private String approx_charge;

    public ServiceAttributeData(JSONObject jsonObject)
    {
        id = jsonObject.optInt("id");
        name = jsonObject.optString("name");
        service_metadata_id = jsonObject.optInt("service_metadata_id");
        active = jsonObject.optInt("active");
        approx_charge = jsonObject.optString("approx_charge");
    }

    public int getId()
    {
        return id;
    }

    public String getName()
    {
        return name;
    }

    public int getService_metadata_id()
    {
        return service_metadata_id;
    }

    public int getActive()
    {
        return active;
    }

    public String getApprox_charge()
    {
        return approx_charge;
    }
}
