package com.android.lockated.categories.vendors.model;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class VendorProductData
{
    private int catid;
    private String catname;

    private ArrayList<ProductData> mProductDatas = new ArrayList<>();

    public VendorProductData(JSONObject jsonObject)
    {
        try
        {
            catid = jsonObject.optInt("catid");
            catname = jsonObject.optString("catname");

            JSONArray productJsonArray = jsonObject.getJSONArray("products");
            for (int i = 0; i < productJsonArray.length(); i++)
            {
                JSONObject productObject = productJsonArray.getJSONObject(i);
                ProductData productData = new ProductData(productObject);
                mProductDatas.add(productData);
            }
        }
        catch (JSONException e)
        {
            e.printStackTrace();
        }
    }

    public int getCatId()
    {
        return catid;
    }

    public String getCatName()
    {
        return catname;
    }

    public ArrayList<ProductData> getProductData()
    {
        return mProductDatas;
    }
}
