package com.android.lockated.cart.ecommerce;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.android.lockated.LockatedApplication;
import com.android.lockated.R;
import com.android.lockated.checkout.CheckoutActivity;
import com.android.lockated.information.AccountController;
import com.android.lockated.model.AccountData;
import com.android.lockated.model.CartDetail;
import com.android.lockated.utils.Utilities;
import com.google.android.gms.analytics.ecommerce.Product;
import com.google.android.gms.analytics.ecommerce.ProductAction;

import java.util.ArrayList;

public class EcommerceCartActivity extends AppCompatActivity implements View.OnClickListener {
    private TextView mTextViewCartContinue;
    private TextView mTextViewCartCheckout;
    private TextView mTextViewCartTotalPrice;
    String screenName = "Ecommerce Cart";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ecommerce_cart);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(true);
        getSupportActionBar().setTitle(R.string.my_cart);

        if (savedInstanceState == null) {
            EcommerceFragment ecommerceFragment = new EcommerceFragment();
            getSupportFragmentManager().beginTransaction().add(R.id.mEcommerceContainer, ecommerceFragment).commit();
        }
        Utilities.ladooIntegration(this, screenName);
        Utilities.lockatedGoogleAnalytics(screenName, getString(R.string.visited), screenName);
        init();
    }

    private void init() {
        mTextViewCartContinue = (TextView) findViewById(R.id.mTextViewCartContinue);
        mTextViewCartCheckout = (TextView) findViewById(R.id.mTextViewCartCheckout);
        mTextViewCartTotalPrice = (TextView) findViewById(R.id.mTextViewCartTotalPrice);

        mTextViewCartContinue.setOnClickListener(this);
        mTextViewCartCheckout.setOnClickListener(this);
    }

    public void setTotalPrice(String price) {
        mTextViewCartTotalPrice.setText(getString(R.string.rupees_symbol) + " " + price);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                super.onBackPressed();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.mTextViewCartContinue:
                EcommerceCartActivity.this.finish();
                break;
            case R.id.mTextViewCartCheckout:
                onCheckoutClicked();
                break;
        }
    }

    private void onCheckoutClicked() {
        ArrayList<AccountData> accountDatas = AccountController.getInstance().getmAccountDataList();
        if (accountDatas.size() > 0) {
            ArrayList<CartDetail> cartDetails = accountDatas.get(0).getCartDetailList();
            if (cartDetails.size() > 0) {
                if (cartDetails.get(0).getmLineItemData().size() > 0) {
                    Product product = new Product()
                            .setId(String.valueOf(cartDetails.get(0).getmLineItemData().get(0).getVariantId()))
                            .setName(cartDetails.get(0).getmLineItemData().get(0).getVariantName())
                            /*.setCategory(cartDetails.get(0).)*/
                            /*.setBrand("Google")
                            .setVariant("Black")
                            .setPosition(1)*/
                            .setPrice(Double.valueOf(cartDetails.get(0).getmLineItemData().get(0).getVariantCostPrice()))
                            .setQuantity(cartDetails.get(0).getmLineItemData().get(0).getQuantity())
                            .setCustomDimension(1, "Member");
                    ecommerceMeasureTransaction(product, screenName, cartDetails.get(0).getmLineItemData().get(0).getVariantName());
                    Intent intent = new Intent(EcommerceCartActivity.this, CheckoutActivity.class);
                    startActivity(intent);
                } else {
                    Utilities.showToastMessage(EcommerceCartActivity.this, getString(R.string.empty_cart));
                }
            } else {
                Utilities.showToastMessage(EcommerceCartActivity.this, getString(R.string.empty_cart));
            }
        }
    }

    public void ecommerceMeasureTransaction(Product productObject, String screenName, String productName) {
        ((LockatedApplication) this.getApplicationContext()).gaMeasureImpression(productObject, screenName, productName);
    }

}
