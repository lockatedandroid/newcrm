
package com.android.lockated.model.AdminModel.Notice;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class Noticeboard {

    @SerializedName("id")
    @Expose
    private int id;
    @SerializedName("id_society")
    @Expose
    private int idSociety;
    @SerializedName("notice_heading")
    @Expose
    private String noticeHeading;
    @SerializedName("notice_text")
    @Expose
    private String noticeText;
    @SerializedName("active")
    @Expose
    private Object active;
    @SerializedName("IsDelete")
    @Expose
    private Object IsDelete;
    @SerializedName("expire_time")
    @Expose
    private String expireTime;
    @SerializedName("created_at")
    @Expose
    private String createdAt;
    @SerializedName("updated_at")
    @Expose
    private String updatedAt;
    @SerializedName("canceled_by")
    @Expose
    private Object canceledBy;
    @SerializedName("canceler_id")
    @Expose
    private Object cancelerId;
    @SerializedName("comment")
    @Expose
    private Object comment;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("user_flat")
    @Expose
    private UserFlat userFlat;
    @SerializedName("user")
    @Expose
    private User user;
    @SerializedName("documents")
    @Expose
    private ArrayList<NoticeDocument> documents = new ArrayList<NoticeDocument>();

    /**
     * @return The id
     */
    public int getId() {
        return id;
    }

    /**
     * @param id The id
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * @return The idSociety
     */
    public int getIdSociety() {
        return idSociety;
    }

    /**
     * @param idSociety The id_society
     */
    public void setIdSociety(int idSociety) {
        this.idSociety = idSociety;
    }

    /**
     * @return The noticeHeading
     */
    public String getNoticeHeading() {
        return noticeHeading;
    }

    /**
     * @param noticeHeading The notice_heading
     */
    public void setNoticeHeading(String noticeHeading) {
        this.noticeHeading = noticeHeading;
    }

    /**
     * @return The noticeText
     */
    public String getNoticeText() {
        return noticeText;
    }

    /**
     * @param noticeText The notice_text
     */
    public void setNoticeText(String noticeText) {
        this.noticeText = noticeText;
    }

    /**
     * @return The active
     */
    public Object getActive() {
        return active;
    }

    /**
     * @param active The active
     */
    public void setActive(Object active) {
        this.active = active;
    }

    /**
     * @return The IsDelete
     */
    public Object getIsDelete() {
        return IsDelete;
    }

    /**
     * @param IsDelete The IsDelete
     */
    public void setIsDelete(Object IsDelete) {
        this.IsDelete = IsDelete;
    }

    /**
     * @return The expireTime
     */
    public String getExpireTime() {
        return expireTime;
    }

    /**
     * @param expireTime The expire_time
     */
    public void setExpireTime(String expireTime) {
        this.expireTime = expireTime;
    }

    /**
     * @return The createdAt
     */
    public String getCreatedAt() {
        return createdAt;
    }

    /**
     * @param createdAt The created_at
     */
    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    /**
     * @return The updatedAt
     */
    public String getUpdatedAt() {
        return updatedAt;
    }

    /**
     * @param updatedAt The updated_at
     */
    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    /**
     * @return The canceledBy
     */
    public Object getCanceledBy() {
        return canceledBy;
    }

    /**
     * @param canceledBy The canceled_by
     */
    public void setCanceledBy(Object canceledBy) {
        this.canceledBy = canceledBy;
    }

    /**
     * @return The cancelerId
     */
    public Object getCancelerId() {
        return cancelerId;
    }

    /**
     * @param cancelerId The canceler_id
     */
    public void setCancelerId(Object cancelerId) {
        this.cancelerId = cancelerId;
    }

    /**
     * @return The comment
     */
    public Object getComment() {
        return comment;
    }

    /**
     * @param comment The comment
     */
    public void setComment(Object comment) {
        this.comment = comment;
    }

    /**
     * @return The status
     */
    public String getStatus() {
        return status;
    }

    /**
     * @param status The status
     */
    public void setStatus(String status) {
        this.status = status;
    }

    /**
     * @return The userFlat
     */
    public UserFlat getUserFlat() {
        return userFlat;
    }

    /**
     * @param userFlat The user_flat
     */
    public void setUserFlat(UserFlat userFlat) {
        this.userFlat = userFlat;
    }

    /**
     * @return The user
     */
    public User getUser() {
        return user;
    }

    /**
     * @param user The user
     */
    public void setUser(User user) {
        this.user = user;
    }

    public ArrayList<NoticeDocument> getDocuments() {
        return documents;
    }

    public void setDocuments(ArrayList<NoticeDocument> documents) {
        this.documents = documents;
    }
}
