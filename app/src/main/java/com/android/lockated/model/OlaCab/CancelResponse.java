package com.android.lockated.model.OlaCab;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


public class CancelResponse {

    @SerializedName("request_type")
    @Expose
    private String requestType;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("text")
    @Expose
    private String text;
    @SerializedName("header")
    @Expose
    private String header;

    /**
     *
     * @return
     *     The requestType
     */
    public String getRequestType() {
        return requestType;
    }

    /**
     *
     * @param requestType
     *     The request_type
     */
    public void setRequestType(String requestType) {
        this.requestType = requestType;
    }

    /**
     *
     * @return
     *     The status
     */
    public String getStatus() {
        return status;
    }

    /**
     *
     * @param status
     *     The status
     */
    public void setStatus(String status) {
        this.status = status;
    }

    /**
     *
     * @return
     *     The text
     */
    public String getText() {
        return text;
    }

    /**
     *
     * @param text
     *     The text
     */
    public void setText(String text) {
        this.text = text;
    }

    /**
     *
     * @return
     *     The header
     */
    public String getHeader() {
        return header;
    }

    /**
     *
     * @param header
     *     The header
     */
    public void setHeader(String header) {
        this.header = header;
    }

}
