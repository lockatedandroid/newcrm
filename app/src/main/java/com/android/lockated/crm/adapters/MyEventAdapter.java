package com.android.lockated.crm.adapters;

import android.content.Context;
import android.content.Intent;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.android.lockated.R;
import com.android.lockated.crm.activity.MyEventDetailActivity;
import com.android.lockated.information.AccountController;
import com.android.lockated.model.AccountData;
import com.android.lockated.model.usermodel.EventModel.MyEvent.Classified;
import com.android.lockated.preferences.LockatedPreferences;
import com.android.lockated.utils.Utilities;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class MyEventAdapter extends RecyclerView.Adapter<MyEventAdapter.MyEventViewHolder> {

    private final ArrayList<Classified> classifieds;
    Context contextMain;
    JSONArray jsonArray;
    FragmentManager childFragmentManager;
    AccountController accountController;
    ArrayList<AccountData> datas;
    LockatedPreferences lockatedPreferences;
    String statusTag = "No Status";

    public MyEventAdapter(Context context, ArrayList<Classified> classifieds, FragmentManager childFragmentManager) {
        this.contextMain = context;
        this.classifieds = classifieds;
        this.childFragmentManager = childFragmentManager;
        accountController = AccountController.getInstance();
        datas = accountController.getmAccountDataList();
        lockatedPreferences = new LockatedPreferences(context);
    }

    @Override
    public MyEventViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(contextMain).inflate(R.layout.my_event_adapter, parent, false);
        MyEventViewHolder pvh = new MyEventViewHolder(v);
        return pvh;
    }

    @Override
    public void onBindViewHolder(MyEventViewHolder holder, final int position) {

        try {
            holder.detail.setTag(position);
            if (!classifieds.get(position).getEventName().equals("null")) {
                holder.subject.setText(classifieds.get(position).getEventName());
                //Log.e("getEventName", "" + classifieds.get(position).getEventName());
            } else {
                holder.subject.setText("No Subject");
            }
            if (!classifieds.get(position).getDescription().equals("null")) {
                holder.description.setText(classifieds.get(position).getDescription());
                //Log.e("Description", "" + classifieds.get(position).getDescription());
            } else {
                holder.description.setText("No Description Available");
            }
            if (!classifieds.get(position).getCreatedAt().equals("null")) {
                //Log.e("getCreatedAt", "" + classifieds.get(position).getCreatedAt());
                holder.createdOn.setText(Utilities.dateTime_AMPM_Format(classifieds.get(position).getCreatedAt()));
            } else {
                holder.createdOn.setText("No Date Available");
            }
            if (!classifieds.get(position).getEventAt().equals("null")) {
                //Log.e("getEventAt", "" + classifieds.get(position).getEventAt());
                holder.eventAtValue.setText(classifieds.get(position).getEventAt());
            } else {
                holder.eventAtValue.setText("No date Available");
            }
            if (!classifieds.get(position).getStatus().equals("null")) {
                statusTag = classifieds.get(position).getStatus();
                //Log.e("statusTag", "" + statusTag);
                if (statusTag.equalsIgnoreCase("rejected")) {
                    holder.status.setText(statusTag);
                } else if (statusTag.equalsIgnoreCase("approved")) {
                    holder.status.setText(statusTag);
                } else {
                    holder.status.setText(statusTag);
                }
            } else {
                holder.status.setText("No Status Available");
            }

            holder.detail.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    viewCardDetail(contextMain, classifieds.get(position));
                }
            });
            /*if (statusTag.equalsIgnoreCase("rejected")) {
                holder.editText.setEnabled(false);
                holder.editText.setClickable(false);
            }*//* else {
                holder.editText.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        //     OnRepublishClicked(classifieds.get(position));
                    }
                });
            }*/

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public int getItemCount() {
        //    return jsonArray.length();

        return classifieds.size();
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }

    public class MyEventViewHolder extends RecyclerView.ViewHolder {

        TextView subject, description, createdOn, status, editText, detail, eventAtValue;

        public MyEventViewHolder(View itemView) {
            super(itemView);

            subject = (TextView) itemView.findViewById(R.id.subjectValue);
            description = (TextView) itemView.findViewById(R.id.descriptionValue);
            createdOn = (TextView) itemView.findViewById(R.id.createdOnValue);
            status = (TextView) itemView.findViewById(R.id.statusValue);
            detail = (TextView) itemView.findViewById(R.id.detailShow);
            eventAtValue = (TextView) itemView.findViewById(R.id.eventAtValue);
            editText = (TextView) itemView.findViewById(R.id.edit);

        }

    }

    /*private void OnRepublishClicked(Classified classified) {
        JSONObject jsonObject = new JSONObject();
        JSONObject jsonObject1Main=new JSONObject();
        try {
            jsonObject.put("id_society", lockatedPreferences.getSocietyId());
            jsonObject.put("notice_heading", jsonObjectValue.getString("notice_heading"));
            jsonObject.put("notice_text", jsonObjectValue.getString("notice_text"));
            jsonObject.put("expire_time", jsonObjectValue.getString("expire_time"));
            jsonObject.put("created_at", jsonObjectValue.getString("created_at"));
            jsonObject.put("updated_at", jsonObjectValue.getString("updated_at"));
            jsonObject1Main.put("noticeboard", jsonObject);
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }
*/
    public void viewCardDetail(Context context, Classified classified) {

        JSONObject jsonObjectMain = new JSONObject();
        try {
            String userName;
            jsonObjectMain.put("event_type", classified.getEventType());
            if (classified.getUser().getFirstname().equals("null")) {
                userName = "No Name";

            } else {
                userName = classified.getUser().getFirstname()
                        + " " + classified.getUser().getLastname();
            }
            jsonObjectMain.put("username", userName);
            jsonObjectMain.put("subject", classified.getEventName());
            jsonObjectMain.put("event_name", classified.getEventName());
            jsonObjectMain.put("description", classified.getDescription());
            jsonObjectMain.put("event_at", classified.getEventAt());
            jsonObjectMain.put("from_time", classified.getFromTime());
            jsonObjectMain.put("to_time", classified.getToTime());
            jsonObjectMain.put("created_at", classified.getCreatedAt());
            jsonObjectMain.put("updated_at", classified.getCreatedAt());
            jsonObjectMain.put("id", classified.getId());
            if (classified.getDocumentsEvent().size() > 0 && classified.getDocumentsEvent() != null) {
                jsonObjectMain.put("document", classified.getDocumentsEvent().get(0).getDocument());
            } else {
                jsonObjectMain.put("document", "No Document");
            }
            jsonObjectMain.put("rsvp", "No Data");
            //Log.e("jsonObjectMain..", "" + jsonObjectMain.toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }

        Intent allNoticeDetailIntent = new Intent(context, MyEventDetailActivity.class);
        allNoticeDetailIntent.putExtra("EventDetailViewData", jsonObjectMain.toString());
        context.startActivity(allNoticeDetailIntent);

    }

/*    public String ddMMYYYFormat(String dateStr) {
        DateFormat format = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);
        Date date = null;
        try {
            date = format.parse(dateStr);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        dateStr = new SimpleDateFormat("dd-MM-yyyy").format(date);
        return dateStr;
    }*/

}