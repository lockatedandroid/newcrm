package com.android.lockated.crmadmin.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.android.lockated.LockatedApplication;
import com.android.lockated.R;
import com.android.lockated.crmadmin.activity.CreateFacilitiesActivity;
import com.android.lockated.crmadmin.adapter.AdminFacilitiesListAdapter;
import com.android.lockated.model.AdminModel.AdminFacilities.AdminAmenity;
import com.android.lockated.network.ConnectionDetector;
import com.android.lockated.preferences.LockatedPreferences;
import com.android.lockated.request.LockatedRequestError;
import com.android.lockated.request.LockatedVolleyRequestQueue;
import com.android.lockated.utils.ApplicationURL;
import com.android.lockated.utils.Utilities;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class AdminFacilitiesListFragment extends Fragment implements Response.Listener<JSONObject>,
        Response.ErrorListener, View.OnClickListener {

    RecyclerView recyclerView;
    TextView errorMsg;
    ProgressBar mProgressBarView;
    AdminFacilitiesListAdapter adminFacilitiesListAdapter;
    LockatedPreferences mLockatedPreferences;
    ArrayList<AdminAmenity> adminAmenities;
    LockatedVolleyRequestQueue mLockatedVolleyRequestQueue;
    String FACILITIES_LIST = "AdminFacilitiesListFragment";
    FloatingActionButton floatingActionButton;
    String SHOW, CREATE, INDEX, UPDATE, EDIT;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View facilitiesView = inflater.inflate(R.layout.fragment_recycler_view, container, false);
        init(facilitiesView);
        Utilities.ladooIntegration(getActivity(), "Admin Facilities List");
        return facilitiesView;
    }

    @Override
    public void onResume() {
        super.onResume();
        LockatedApplication.getInstance().trackScreenView(getString(R.string.adminFacilitiesList));
        Utilities.lockatedGoogleAnalytics(getString(R.string.adminFacilitiesList),
                getString(R.string.visited), getString(R.string.adminFacilitiesList));
    }

    private void init(View facilitiesView) {
        adminAmenities = new ArrayList<>();
        mLockatedPreferences = new LockatedPreferences(getActivity());
        mProgressBarView = (ProgressBar) facilitiesView.findViewById(R.id.mProgressBarView);
        recyclerView = (RecyclerView) facilitiesView.findViewById(R.id.recyclerViewElement);
        errorMsg = (TextView) facilitiesView.findViewById(R.id.errorMsg);
        floatingActionButton = (FloatingActionButton) facilitiesView.findViewById(R.id.fab);
        floatingActionButton.setOnClickListener(this);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerView.setLayoutManager(linearLayoutManager);
        getFacilitiesRole();
        checkPermission();
        adminFacilitiesListAdapter = new AdminFacilitiesListAdapter(getActivity(), getChildFragmentManager(),
                adminAmenities);
        recyclerView.setAdapter(adminFacilitiesListAdapter);
    }

    private void checkPermission() {
        if (SHOW!=null&&SHOW.equals("true")) {
            getFacilitiesList();
        } else {
            recyclerView.setVisibility(View.GONE);
            errorMsg.setVisibility(View.VISIBLE);
            errorMsg.setText(R.string.no_permission_error);
        }
    }

    private void getFacilitiesList() {
        if (ConnectionDetector.isConnectedToInternet(getActivity())) {
            mProgressBarView.setVisibility(View.VISIBLE);
            String url = ApplicationURL.getAmenitiesUrl + mLockatedPreferences.getSocietyId()
                    + "&token=" + mLockatedPreferences.getLockatedToken();
            mLockatedVolleyRequestQueue = LockatedVolleyRequestQueue.getInstance(getActivity());
            mLockatedVolleyRequestQueue.sendRequest(FACILITIES_LIST, Request.Method.GET, url, null, this, this);
//            Log.e("List facilityurl",""+url);

        } else {
            Utilities.showToastMessage(getActivity(), getActivity().getResources().getString(R.string.internet_connection_error));
        }
    }

    @Override
    public void onResponse(JSONObject response) {
        mProgressBarView.setVisibility(View.GONE);
        try {
            if (response.has("amenities") && response.getJSONArray("amenities").length() > 0) {
                JSONArray jsonArray = response.getJSONArray("amenities");
                Gson gson = new Gson();
                for (int i = 0; i < jsonArray.length(); i++) {
                    AdminAmenity adminAmenity = gson.fromJson(jsonArray.getJSONObject(i).toString(), AdminAmenity.class);
                    adminAmenities.add(adminAmenity);
                }
                adminFacilitiesListAdapter.notifyDataSetChanged();
            } else {
                recyclerView.setVisibility(View.GONE);
                errorMsg.setVisibility(View.VISIBLE);
                errorMsg.setText(R.string.no_data_error);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onErrorResponse(VolleyError error) {
        mProgressBarView.setVisibility(View.GONE);
        if (getActivity() != null) {
            LockatedRequestError.onRequestError(getActivity(), error);
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.fab:
                Intent intent = new Intent(getActivity(), CreateFacilitiesActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                break;

        }
    }

    public String getFacilitiesRole() {
        String mySocietyRoles = getActivity().getResources().getString(R.string.blank_value);
        if (mLockatedPreferences.getRolesJson() != null && !mLockatedPreferences.getRolesJson().equals("blank")) {
            try {
                JSONObject noticeJsonObj = new JSONObject(mLockatedPreferences.getRolesJson());
                for (int i = 0; i < noticeJsonObj.getJSONArray(getActivity().getResources().getString(R.string.permissions_value)).length(); i++) {
                    JSONObject jsonObject1 = noticeJsonObj.getJSONArray(getActivity().getResources().getString(R.string.permissions_value)).getJSONObject(i);
                    if (jsonObject1.has(getActivity().getResources().getString(R.string.section_value))
                            && jsonObject1.getString(getActivity().getResources().getString(R.string.section_value)).equals("spree_facilities")) {
                        if (jsonObject1.getJSONObject(getActivity().getResources().getString(R.string.permission_value)).has(getActivity().getResources().getString(R.string.index_value))) {
                            mySocietyRoles = jsonObject1.getJSONObject(getActivity().getResources().getString(R.string.permission_value)).getString(getActivity().getResources().getString(R.string.index_value));
                            CREATE = jsonObject1.getJSONObject(getActivity().getResources().getString(R.string.permission_value)).getString("create");
                            INDEX = jsonObject1.getJSONObject(getActivity().getResources().getString(R.string.permission_value)).getString("index");
                            UPDATE = jsonObject1.getJSONObject(getActivity().getResources().getString(R.string.permission_value)).getString("update");
                            EDIT = jsonObject1.getJSONObject(getActivity().getResources().getString(R.string.permission_value)).getString("edit");
                            SHOW = jsonObject1.getJSONObject(getActivity().getResources().getString(R.string.permission_value)).getString("show");
                        }
                    }
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return mySocietyRoles;
    }

}
