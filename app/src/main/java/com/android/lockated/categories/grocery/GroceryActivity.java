package com.android.lockated.categories.grocery;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.android.lockated.LockatedApplication;
import com.android.lockated.R;
import com.android.lockated.cart.CartController;
import com.android.lockated.cart.ecommerce.EcommerceCartActivity;
import com.android.lockated.categories.grocery.fragment.CategoryProductDetailViewFragment;
import com.android.lockated.categories.grocery.fragment.CategoryProductViewFragment;
import com.android.lockated.categories.grocery.model.GroceryItemData;
import com.android.lockated.information.AccountController;
import com.android.lockated.landing.model.TaxonData;
import com.android.lockated.model.AccountData;
import com.android.lockated.model.CartDetail;
import com.android.lockated.network.ConnectionDetector;
import com.android.lockated.preferences.LockatedPreferences;
import com.android.lockated.request.LockatedRequestError;
import com.android.lockated.request.LockatedVolleyRequestQueue;
import com.android.lockated.search.SearchActivity;
import com.android.lockated.utils.ApplicationURL;
import com.android.lockated.utils.Utilities;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class GroceryActivity extends AppCompatActivity implements View.OnClickListener, Response.Listener, Response.ErrorListener, CategoryProductViewFragment.OnPagerPageChangeListener {
    private TextView mTextViewProductDetailViewNext;
    private TextView mTextViewProductDetailViewContinue;
    private TextView mTextViewProductDetailViewTotalPrice;

    private ProgressDialog mProgressDialog;

    private int mSelectedPosition;

    private ArrayList<TaxonData> mTaxonDataList;

    private CartController mCartController;
    private LockatedPreferences mLockatedPreferences;
    private static final String ADD_PRODUCT_TAG = "GroceryActivity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_grocery);

        setToolBar();

        if (savedInstanceState == null) {
            CategoryProductViewFragment categoryProductViewFragment = new CategoryProductViewFragment();
            //Log.e("mTaxonDataList", String.valueOf(mTaxonDataList));
            categoryProductViewFragment.setTaxonData(mTaxonDataList, mSelectedPosition, this);
            getSupportFragmentManager().beginTransaction().add(R.id.mGroceryContainer, categoryProductViewFragment).commit();
        }
        init();
    }

    private void setToolBar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(true);
        if (getIntent().getExtras() != null) {
            getSupportActionBar().setTitle(getIntent().getExtras().getString("GroceryActivity"));
        } else {
            getSupportActionBar().setTitle(R.string.grocery);
        }

        ((LockatedApplication) getApplicationContext()).setMyAddressData(null);
        ((LockatedApplication) getApplicationContext()).setmDeliveryTime("");
        ((LockatedApplication) getApplicationContext()).setmDeliveryDate("");

        mSelectedPosition = ((LockatedApplication) getApplicationContext()).getmSectionId();
        mTaxonDataList = ((LockatedApplication) getApplicationContext()).getTaxonDatas();
    }

    private void init() {
        mCartController = CartController.getInstance();
        mLockatedPreferences = new LockatedPreferences(this);
        mTextViewProductDetailViewNext = (TextView) findViewById(R.id.mTextViewProductDetailViewNext);
        mTextViewProductDetailViewContinue = (TextView) findViewById(R.id.mTextViewProductDetailViewContinue);
        mTextViewProductDetailViewTotalPrice = (TextView) findViewById(R.id.mTextViewProductDetailViewTotalPrice);

        mTextViewProductDetailViewNext.setOnClickListener(this);
        mTextViewProductDetailViewContinue.setOnClickListener(this);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.grocery_home, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                super.onBackPressed();
                return true;

            case R.id.action_cart:
                onCartClicked();
                return true;

            case R.id.action_search:
                onSearchClicked();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void onCartClicked() {
        ArrayList<AccountData> accountDatas = AccountController.getInstance().getmAccountDataList();
        if (accountDatas.size() > 0) {
            ArrayList<CartDetail> cartDetails = accountDatas.get(0).getCartDetailList();
            if (cartDetails.size() > 0) {
                if (cartDetails.get(0).getmLineItemData().size() > 0) {
                    Intent intent = new Intent(this, EcommerceCartActivity.class);
                    startActivity(intent);
                } else {
                    Utilities.showToastMessage(this, getString(R.string.empty_cart));
                }
            } else {
                Utilities.showToastMessage(this, getString(R.string.empty_cart));
            }
        }
    }

    private void onSearchClicked() {
        Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.mGroceryContainer);
        if (fragment instanceof CategoryProductDetailViewFragment) {
            openSearchView(0, mTaxonDataList.get(mSelectedPosition).getTaxonomy_id());
        } else {
            openSearchView(0, mTaxonDataList.get(mSelectedPosition).getTaxonomy_id());
        }
    }

    private void openSearchView(int mTaxonId, int mTaxonomyId) {
        if (ConnectionDetector.isConnectedToInternet(this)) {
            Intent searchIntent = new Intent(this, SearchActivity.class);
            ((LockatedApplication) getApplicationContext()).setTaxonId(mTaxonId);
            ((LockatedApplication) getApplicationContext()).setTaxonomyId(mTaxonomyId);
            startActivity(searchIntent);
        } else {
            Utilities.showToastMessage(this, getString(R.string.internet_connection_error));
        }
    }

    public void productAddToCart(GroceryItemData groceryItemData) {
        if (mCartController.mGroceryCartDataList.size() > 0) {
            if (mCartController.mGroceryCartDataList.contains(groceryItemData)) {
                if (groceryItemData.getQuantity() != 0) {
                    mCartController.mGroceryCartDataList.remove(groceryItemData);
                    mCartController.mGroceryCartDataList.add(groceryItemData);
                } else {
                    mCartController.mGroceryCartDataList.remove(groceryItemData);
                }
            } else {
                mCartController.mGroceryCartDataList.add(groceryItemData);
            }
        } else {
            mCartController.mGroceryCartDataList.add(groceryItemData);
        }

        updatePriceInfo();
    }

    private void updatePriceInfo() {
        double price = 0.00;
        for (int i = 0; i < mCartController.mGroceryCartDataList.size(); i++) {
            price += (Double.valueOf(mCartController.mGroceryCartDataList.get(i).getPrice())
                    * mCartController.mGroceryCartDataList.get(i).getQuantity());
        }
        mTextViewProductDetailViewTotalPrice.setText(getString(R.string.rupees_symbol) + " " + price + "");
    }

    private void onNextButtonClicked() {
        ArrayList<AccountData> mAccountData = AccountController.getInstance().getmAccountDataList();
        if (mAccountData.size() > 0) {
            if (mAccountData.get(0).getCartDetailList().size() > 0) {
                int cartSize = mCartController.mGroceryCartDataList.size();
                if (cartSize > 0) {
                    JSONObject jsonObject = new JSONObject();
                    JSONArray jsonArray = new JSONArray();
                    try {
                        jsonObject.put("line_items", jsonArray);
                        for (int i = 0; i < mCartController.mGroceryCartDataList.size(); i++) {
                            JSONObject itemObject = new JSONObject();
                            itemObject.put("variant_id", mCartController.mGroceryCartDataList.get(i).getMasterId());
                            itemObject.put("quantity", mCartController.mGroceryCartDataList.get(i).getQuantity());
                            jsonArray.put(i, itemObject);
                        }

                        addAnotherItemToCart(jsonObject);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                } else {
                    if (mAccountData.get(0).getCartDetailList().get(0).getmLineItemData().size() > 0) {
                        openEcommerceCartActivity();
                    } else {
                        Utilities.showToastMessage(this, "Please Select an Item to Proceed");
                    }
                }
            } else {
                int cartSize = mCartController.mGroceryCartDataList.size();
                if (cartSize > 0) {
                    JSONObject jsonObject = new JSONObject();
                    JSONObject orderObject = new JSONObject();
                    JSONArray jsonArray = new JSONArray();
                    try {
                        jsonObject.put("order", orderObject);
                        orderObject.put("line_items", jsonArray);
                        for (int i = 0; i < mCartController.mGroceryCartDataList.size(); i++) {
                            JSONObject itemObject = new JSONObject();
                            itemObject.put("variant_id", mCartController.mGroceryCartDataList.get(i).getMasterId());
                            itemObject.put("quantity", mCartController.mGroceryCartDataList.get(i).getQuantity());
                            jsonArray.put(i, itemObject);
                        }

                        addProductToCart(jsonObject);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                } else {
                    if (mAccountData.get(0).getCartDetailList().size() > 0) {
                        if (mAccountData.get(0).getCartDetailList().get(0).getmLineItemData().size() > 0) {
                            openEcommerceCartActivity();
                        } else {
                            Utilities.showToastMessage(this, getString(R.string.item_selection_error));
                        }
                    } else {
                        Utilities.showToastMessage(this, getString(R.string.item_selection_error));
                    }
                }
            }
        }
    }

    private void addAnotherItemToCart(JSONObject jsonObject) {
        if (ConnectionDetector.isConnectedToInternet(this)) {
            mProgressDialog = ProgressDialog.show(this, "", "Please Wait...", false);
            String url = ApplicationURL.addAnotherItemToCartUrl + mLockatedPreferences.getLockatedToken();
            LockatedVolleyRequestQueue lockatedVolleyRequestQueue = LockatedVolleyRequestQueue.getInstance(this);
            lockatedVolleyRequestQueue.sendRequest(ADD_PRODUCT_TAG, Request.Method.POST, url, jsonObject, this, this);
        } else {
            Utilities.showToastMessage(this, getString(R.string.internet_connection_error));
        }
    }

    private void addProductToCart(JSONObject jsonObject) {
        if (ConnectionDetector.isConnectedToInternet(this)) {
            mProgressDialog = ProgressDialog.show(this, "", "Please Wait...", false);
            String url = ApplicationURL.addProductToCartUrl + mLockatedPreferences.getLockatedToken();
            LockatedVolleyRequestQueue lockatedVolleyRequestQueue = LockatedVolleyRequestQueue.getInstance(this);
            lockatedVolleyRequestQueue.sendRequest(ADD_PRODUCT_TAG, Request.Method.POST, url, jsonObject, this, this);
        } else {
            Utilities.showToastMessage(this, getString(R.string.internet_connection_error));
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.mTextViewProductDetailViewNext:
                onNextButtonClicked();
                break;
            case R.id.mTextViewProductDetailViewContinue:
                this.finish();
                break;
        }
    }

    @Override
    public void onErrorResponse(VolleyError error) {
        if (mProgressDialog != null && mProgressDialog.isShowing()) {
            mProgressDialog.dismiss();
        }
        if (!isFinishing()) {
            LockatedRequestError.onRequestError(this, error);
        }
    }

    @Override
    public void onResponse(Object response) {
        if (mProgressDialog != null && mProgressDialog.isShowing()) {
            mProgressDialog.dismiss();
        }
        ArrayList<AccountData> accountDatas = AccountController.getInstance().getmAccountDataList();
        if (accountDatas.size() > 0) {
            accountDatas.get(0).getCartDetailList().clear();

            JSONObject jsonObject = (JSONObject) response;
            //Utilities.writeToFile(jsonObject.toString(), "AddProductAddAnother");
            CartDetail cartDetail = new CartDetail(jsonObject);
            accountDatas.get(0).getCartDetailList().add(cartDetail);
            mCartController.resetItems();
            openEcommerceCartActivity();
        }
    }

    private void openEcommerceCartActivity() {
        Intent intent = new Intent(this, EcommerceCartActivity.class);
        /*intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);*/
        startActivity(intent);
        /*finish();*/
    }

    @Override
    public void onPageChanged(int position) {
        mSelectedPosition = position;
    }
}
