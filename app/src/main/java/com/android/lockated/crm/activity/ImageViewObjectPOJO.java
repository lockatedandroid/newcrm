package com.android.lockated.crm.activity;

public class ImageViewObjectPOJO {

    Object imageView;
    boolean imageSet;
    String encodedImage;

    public Object getImageView() {
        return imageView;
    }

    public void setImageView(Object imageView) {
        this.imageView = imageView;
    }

    public boolean isImageSet() {
        return imageSet;
    }

    public void setImageSet(boolean imageSet) {
        this.imageSet = imageSet;
    }

    public String getEncodedImage() {
        return encodedImage;
    }

    public void setEncodedImage(String encodedImage) {
        this.encodedImage = encodedImage;
    }
}
