
package com.android.lockated.model.usermodel.NoticeModel.MyNotices;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class MyNotice {

    @SerializedName("noticeboards")
    @Expose
    private ArrayList<Noticeboard> noticeboards = new ArrayList<Noticeboard>();

    /**
     * 
     * @return
     *     The noticeboards
     */
    public ArrayList<Noticeboard> getNoticeboards() {
        return noticeboards;
    }

    /**
     * 
     * @param noticeboards
     *     The noticeboards
     */
    public void setNoticeboards(ArrayList<Noticeboard> noticeboards) {
        this.noticeboards = noticeboards;
    }

}
