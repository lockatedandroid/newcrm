package com.android.lockated.landing.adapter;

import android.content.Context;
import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.android.lockated.Interfaces.IRecyclerItemClickListener;
import com.android.lockated.R;
import com.android.lockated.holder.RecyclerViewHolder;
import com.android.lockated.landing.model.TaxonomiesData;

import java.util.ArrayList;

public class HomeScreenAdapter extends RecyclerView.Adapter<RecyclerViewHolder> {
    private final Context context;
    ArrayList<TaxonomiesData> mTaxonomiesData;
    private IRecyclerItemClickListener mIRecyclerItemClickListener;

    public HomeScreenAdapter(ArrayList<TaxonomiesData> data, IRecyclerItemClickListener listener, Context context) {
        mTaxonomiesData = data;
        this.context=context;
        mIRecyclerItemClickListener = listener;
    }

    @Override
    public RecyclerViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.fragment_home_row, parent, false);
        return new RecyclerViewHolder(itemView, mIRecyclerItemClickListener);
    }

    @Override
    public void onBindViewHolder(RecyclerViewHolder holder, int position) {
        holder.onHomeScreenViewHolder(mTaxonomiesData.get(position),context);
    }

    @Override
    public int getItemCount() {
        return mTaxonomiesData.size();
    }
}
