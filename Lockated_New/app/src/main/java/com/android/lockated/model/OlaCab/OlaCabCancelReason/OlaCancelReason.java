
package com.android.lockated.model.OlaCab.OlaCabCancelReason;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class OlaCancelReason {

    @SerializedName("cancel_reasons")
    @Expose
    private CancelReasons cancelReasons;

    /**
     * 
     * @return
     *     The cancelReasons
     */
    public CancelReasons getCancelReasons() {
        return cancelReasons;
    }

    /**
     * 
     * @param cancelReasons
     *     The cancel_reasons
     */
    public void setCancelReasons(CancelReasons cancelReasons) {
        this.cancelReasons = cancelReasons;
    }

}
