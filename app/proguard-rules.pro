# Add project specific ProGuard rules here.
# By default, the flags in this file are appended to flags specified
# in E:\VijayArora\StudioSDK/tools/proguard/proguard-android.txt
# You can edit the include path and order by changing the proguardFiles
# directive in build.gradle.
#
# For more details, see
#   http://developer.android.com/guide/developing/tools/proguard.html

# Add any project specific keep options here:

# If your project uses WebView with JS, uncomment the following
# and specify the fully qualified class name to the JavaScript interface
# class:
#-keepclassmembers class fqcn.of.javascript.interface.for.webview {
#   public *;
#}

-dontwarn org.apache.http.**
-dontwarn android.net.http.AndroidHttpClient
-dontwarn com.google.android.gms.**
-dontwarn com.android.volley.toolbox.**
-dontwarn okio.**

-keepattributes SourceFile,LineNumberTable
-keep class com.parse.*{ *; }
-dontwarn com.parse.**
-dontwarn com.squareup.picasso.**
-keepclasseswithmembernames class * {
    native <methods>;
}
##--------------------------PayTm------------------------------
-keepclassmembers class com.paytm.pgsdk.PaytmWebView$PaytmJavaScriptInterface {
   public *;
}
##--------------------------For CleverTap SDK------------------------------
-dontwarn com.clevertap.android.sdk.**

##--------------------------AirLoyal Ladoo Genie----------------------------------------------------
-keep public class com.google.android.gms.iid.InstanceID {
    public static com.google.android.gms.iid.InstanceID getInstance(android.content.Context);
    public java.lang.String getToken(java.lang.String, java.lang.String);
}

-keep public class com.google.android.gms.gcm.GoogleCloudMessaging {
    public static java.lang.String INSTANCE_ID_SCOPE;
}

-keep public class com.genie.base.utils.AdvertisingIdClient {
    public static com.genie.base.utils.AdvertisingIdClient$AdInfo getAdvertisingIdInfo(android.content.Context);
}

-keep class com.genie.base.utils.AdvertisingIdClient$AdInfo { *; }

##--------------------------GSON------------------------------
##---------------Begin: proguard configuration for Gson  ----------
# Gson uses generic type information stored in a class file when working with fields. Proguard
# removes such information by default, so configure it to keep all of it.
-keepattributes Signature

# For using GSON @Expose annotation
-keepattributes *Annotation*

# Gson specific classes
-keep class sun.misc.Unsafe { *; }
-keep class com.google.gson.stream.** { *; }

# Application classes that will be serialized/deserialized over Gson
-keep class com.google.gson.examples.android.model.** { *; }
-keep class com.android.lockated.model.usermodel.EventModel.** { *; }
-keep class com.android.lockated.model.usermodel.EventModel.MyEvent.** { *; }
-keep class com.android.lockated.crm.model.HelpDesk.** { *; }
-keep class com.android.lockated.model.usermodel.NoticeModel.** { *; }
-keep class com.android.lockated.offers.model.** { *; }
-keep class com.android.lockated.categories.model.** { *; }
-keep class com.android.lockated.model.MyServices.** { *; }
-keep class com.android.lockated.model.** { *; }

##---------------End: proguard configuration for Gson  ----------

##-----------------------------------AirLoyal Ladoo  ------------------------------
-keep class com.genie.base.utils.AdvertisingIdClient.** { *; }
#-----------------------------Facebook------------------------------------------------
-keepattributes Signature
-keep class com.facebook.android.*
-keep class android.webkit.WebViewClient
-keep class * extends android.webkit.WebViewClient
-keepclassmembers class * extends android.webkit.WebViewClient {
    <methods>;
}

-keepclassmembers class * implements java.io.Serializable {
    private static final java.io.ObjectStreamField[] serialPersistentFields;
    private void writeObject(java.io.ObjectOutputStream);
    private void readObject(java.io.ObjectInputStream);
    java.lang.Object writeReplace();
    java.lang.Object readResolve();
}
#------------CircularImageView
-keep class com.github.siyamed.shapeimageview.** { *; }
#-keep interface com.github.siyamed.shapeimageview.** { *; }
#-keep class org.kxml2.io.KXmlParser.** { *; }
#-keep interface org.kxml2.io.KXmlParser.** { *; }
-dontwarn com.github.siyamed.shapeimageview.**
#-----------------------------Parcelable----------------------------------------------
#-keep class * implements android.os.Parcelable {
# public static final android.os.Parcelable$Creator *;
# }
# -keepclassmembers class * implements android.os.Parcelable {
#     static ** CREATOR;
# }

#---------------------------zomato rules--------------------------------
-keep class com.library.zomato.ordering.** { *; }
-keep interface com.library.zomato.ordering.** { *; }
#-----------------------------------------------------------------------
