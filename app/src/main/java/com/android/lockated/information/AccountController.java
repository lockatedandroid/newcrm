package com.android.lockated.information;

import com.android.lockated.model.AccountData;
import com.android.lockated.model.BaseApi.BaseAccountApi;
import com.android.lockated.model.ResidenceDatas;

import java.util.ArrayList;

public class AccountController {
    public static AccountController mInstance = null;
    public ArrayList<AccountData> mAccountDataList;
    public ArrayList<BaseAccountApi> mBaseAccountsApi;
    public ArrayList<ResidenceDatas> residenceDatases;

    public AccountController() {
        mBaseAccountsApi = new ArrayList<>();
        residenceDatases = new ArrayList<>();
        mAccountDataList = new ArrayList<>();
    }

    public static AccountController getInstance() {
        if (mInstance == null) {
            mInstance = new AccountController();
        }
        return mInstance;
    }

    public ArrayList<AccountData> getmAccountDataList() {
        return mAccountDataList;
    }
    public ArrayList<BaseAccountApi> getBaseAccountsData() {
        return mBaseAccountsApi;
    }
    public ArrayList<ResidenceDatas> getResidenceDatas() {
        return residenceDatases;
    }
}
