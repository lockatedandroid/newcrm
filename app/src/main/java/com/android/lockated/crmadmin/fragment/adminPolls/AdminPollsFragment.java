package com.android.lockated.crmadmin.fragment.adminPolls;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.android.lockated.LockatedApplication;
import com.android.lockated.R;
import com.android.lockated.crm.activity.CreateNoticeListPolls;
import com.android.lockated.crmadmin.adapter.AdminPollAdapter;
import com.android.lockated.information.AccountController;
import com.android.lockated.model.AccountData;
import com.android.lockated.model.AdminModel.AdminPolls.PendingPolls.PendingPoll;
import com.android.lockated.network.ConnectionDetector;
import com.android.lockated.preferences.LockatedPreferences;
import com.android.lockated.request.LockatedJSONObjectRequest;
import com.android.lockated.request.LockatedRequestError;
import com.android.lockated.request.LockatedVolleyRequestQueue;
import com.android.lockated.utils.ApplicationURL;
import com.android.lockated.utils.Utilities;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class AdminPollsFragment extends Fragment implements View.OnClickListener, Response.ErrorListener, Response.Listener<JSONObject> {

    public TextView errorMsg;
    ProgressBar progressBar;
    RecyclerView pollsList;
    FloatingActionButton fab;
    int societyid;
    AccountController accountController;
    ArrayList<AccountData> accountDataArrayList;
    ArrayList<PendingPoll> pendingPolls;
    AdminPollAdapter adminPollsAdapter;
    private RequestQueue mQueue;
    private LockatedPreferences mLockatedPreferences;
    String SHOW, CREATE, INDEX, UPDATE, EDIT;
    public static final String REQUEST_TAG = "AdminPollsFragment";

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View pollView = inflater.inflate(R.layout.fragment_admin_polls, container, false);
        init(pollView);
        Utilities.ladooIntegration(getActivity(), "Admin Pending Polls");
        return pollView;

    }

    @Override
    public void onResume() {
        super.onResume();
        LockatedApplication.getInstance().trackScreenView(getString(R.string.pendingPolls));
        Utilities.lockatedGoogleAnalytics(getString(R.string.pendingPolls),
                getString(R.string.visited), getString(R.string.pendingPolls));

    }

    private void init(View view) {
        pendingPolls = new ArrayList<>();
        mLockatedPreferences = new LockatedPreferences(getActivity());
        pollsList = (RecyclerView) view.findViewById(R.id.noticeList);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        pollsList.setLayoutManager(linearLayoutManager);
        errorMsg = (TextView) view.findViewById(R.id.noNotices);
        progressBar = (ProgressBar) view.findViewById(R.id.mProgressBarView);
        fab = (FloatingActionButton) view.findViewById(R.id.fab);
        getPollsRoll();
        checkPermission();
        adminPollsAdapter = new AdminPollAdapter(getActivity(), pendingPolls, getChildFragmentManager());
        pollsList.setAdapter(adminPollsAdapter);

        fab.setOnClickListener(this);

    }

    private void checkPermission() {
        if (SHOW!=null&&SHOW.equals("true")) {
            getPendingPolls();
        } else {
            pollsList.setVisibility(View.GONE);
            errorMsg.setVisibility(View.VISIBLE);
            errorMsg.setText(R.string.no_permission_error);
        }
    }

    public void getPendingPolls() {
        accountController = AccountController.getInstance();
        accountDataArrayList = accountController.getmAccountDataList();
        societyid = accountDataArrayList.get(0).getmResidenceDataList().get(0).getSociety_id();

        if (ConnectionDetector.isConnectedToInternet(getActivity())) {
            progressBar.setVisibility(View.VISIBLE);
            mQueue = LockatedVolleyRequestQueue.getInstance(getActivity()).getRequestQueue();
            String url = ApplicationURL.getPendingPollsUrl + societyid + "&token=" + mLockatedPreferences.getLockatedToken();
            LockatedJSONObjectRequest lockatedJSONObjectRequest = new LockatedJSONObjectRequest(Request.Method.GET,
                    url, null, this, this);
            // Log.e("Pending Polls", "" + url);
            lockatedJSONObjectRequest.setTag(REQUEST_TAG);
            mQueue.add(lockatedJSONObjectRequest);
        } else {
            Utilities.showToastMessage(getActivity(), getActivity().getResources().getString(R.string.internet_connection_error));
        }
    }

    @Override
    public void onErrorResponse(VolleyError error) {
        if (getActivity() != null) {
            progressBar.setVisibility(View.GONE);
            LockatedRequestError.onRequestError(getActivity(), error);
        }
    }

    @Override
    public void onResponse(JSONObject response) {
        if (getActivity() != null) {
            progressBar.setVisibility(View.GONE);
            try {
                if (response != null && response.has("polls") && response.getJSONArray("polls").length() > 0) {
                    JSONArray jsonArray = response.getJSONArray("polls");
                    Gson gson = new Gson();
                    for (int i = 0; i < jsonArray.length(); i++) {
                        PendingPoll events = gson.fromJson(jsonArray.getJSONObject(i).toString(), PendingPoll.class);
                        pendingPolls.add(events);
                    }
                    adminPollsAdapter.notifyDataSetChanged();

                } else {
                    pollsList.setVisibility(View.GONE);
                    errorMsg.setVisibility(View.VISIBLE);
                    errorMsg.setText(R.string.no_data_error);
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        } else {
            pollsList.setVisibility(View.GONE);
            errorMsg.setVisibility(View.VISIBLE);
            errorMsg.setText(R.string.no_data_error);
        }

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.fab:
                Intent intent = new Intent(getActivity(), CreateNoticeListPolls.class);
                startActivity(intent);
                break;
        }
    }

    public String getPollsRoll() {
        String mySocietyRoles = getActivity().getResources().getString(R.string.blank_value);
        if (mLockatedPreferences.getRolesJson() != null && !mLockatedPreferences.getRolesJson().equals("blank")) {
            try {
                JSONObject noticeJsonObj = new JSONObject(mLockatedPreferences.getRolesJson());
                for (int i = 0; i < noticeJsonObj.getJSONArray(getActivity().getResources().getString(R.string.permissions_value)).length(); i++) {
                    JSONObject jsonObject1 = noticeJsonObj.getJSONArray(getActivity().getResources().getString(R.string.permissions_value)).getJSONObject(i);
                    if (jsonObject1.has(getActivity().getResources().getString(R.string.section_value))
                            && jsonObject1.getString(getActivity().getResources().getString(R.string.section_value)).equals("spree_polls")) {
                        if (jsonObject1.getJSONObject(getActivity().getResources().getString(R.string.permission_value)).has(getActivity().getResources().getString(R.string.index_value))) {
                            mySocietyRoles = jsonObject1.getJSONObject(getActivity().getResources().getString(R.string.permission_value)).getString(getActivity().getResources().getString(R.string.index_value));
                            CREATE = jsonObject1.getJSONObject(getActivity().getResources().getString(R.string.permission_value)).getString("create");
                            INDEX = jsonObject1.getJSONObject(getActivity().getResources().getString(R.string.permission_value)).getString("index");
                            UPDATE = jsonObject1.getJSONObject(getActivity().getResources().getString(R.string.permission_value)).getString("update");
                            EDIT = jsonObject1.getJSONObject(getActivity().getResources().getString(R.string.permission_value)).getString("edit");
                            SHOW = jsonObject1.getJSONObject(getActivity().getResources().getString(R.string.permission_value)).getString("show");
                            //    Log.e("Permissions ", " CREATE" + CREATE + "INDEX" + " " + INDEX + "UPDATE" + " " + UPDATE + " " + "EDIT" + EDIT);
                        }
                    }
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return mySocietyRoles;
    }
}
