package com.android.lockated.account.activity;

import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.android.lockated.R;
import com.android.lockated.model.userSocietyFlat.UserFlat;

import java.util.ArrayList;

public class MySocietyDetailActivity extends AppCompatActivity {

    TextView mEditTextSocietyCity, mEditTextSocietyPinCode;
    TextView mSpinnerSocietyState, mSpinnerSocietyType, mEditTextSocietyName;
    TextView mEditTextSocietyBlock, mEditTextSocietyFlatNumber, mEditTextSocietyAddress1;
    TextView mEditTextSocietyAddress2, mEditTextSocietyIntercom, mEditTextLandlineNumber;
    TextView mButtonSocietyDetailClose;
    LinearLayout mLinearLayoutIntercome, linearLayoutSociety;
    RadioButton mRadioOwner, mRadioTenant;
    RadioGroup radioGroupOwnership;
    int position;
    ArrayList<UserFlat> userSocietyArrayList;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_society_detail);

        setToolBar("Society Detail");
        init();
        setValues();
    }

    private void setToolBar(String name) {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(true);
        getSupportActionBar().setTitle(name);
    }

    private void init() {
        mEditTextSocietyCity = (TextView) findViewById(R.id.mEditTextSocietyCity);
        mEditTextSocietyPinCode = (TextView) findViewById(R.id.mEditTextSocietyPinCode);
        mSpinnerSocietyState = (TextView) findViewById(R.id.mSpinnerSocietyState);
        mSpinnerSocietyType = (TextView) findViewById(R.id.mSpinnerSocietyType);
        mEditTextSocietyName = (TextView) findViewById(R.id.mEditTextSocietyName);
        mEditTextSocietyBlock = (TextView) findViewById(R.id.mEditTextSocietyBlock);
        mEditTextSocietyFlatNumber = (TextView) findViewById(R.id.mEditTextSocietyFlatNumber);
        mEditTextSocietyAddress1 = (TextView) findViewById(R.id.mEditTextSocietyAddress1);
        mEditTextSocietyAddress2 = (TextView) findViewById(R.id.mEditTextSocietyAddress2);
        mEditTextSocietyIntercom = (TextView) findViewById(R.id.mEditTextSocietyIntercom);
        mEditTextLandlineNumber = (TextView) findViewById(R.id.mEditTextLandlineNumber);
        mLinearLayoutIntercome = (LinearLayout) findViewById(R.id.mLinearLayoutIntercome);
        linearLayoutSociety = (LinearLayout) findViewById(R.id.linearLayoutSociety);
        mButtonSocietyDetailClose = (TextView) findViewById(R.id.mButtonSocietyDetailClose);
        mRadioOwner = (RadioButton) findViewById(R.id.mRadioOwner);
        mRadioTenant = (RadioButton) findViewById(R.id.mRadioTenant);
        mSpinnerSocietyType.setTypeface(null, Typeface.NORMAL);
        mSpinnerSocietyState.setTypeface(null, Typeface.NORMAL);
    }

    public void setValues() {
        if (getIntent().getExtras() != null) {
            userSocietyArrayList = getIntent().getParcelableArrayListExtra("userSocietyArrayList");
            position = getIntent().getExtras().getInt("position");
            if (userSocietyArrayList.size() > 0) {
                if (userSocietyArrayList.get(position).getResidenceType().equalsIgnoreCase("society")) {
                    mLinearLayoutIntercome.setVisibility(View.VISIBLE);
                    linearLayoutSociety.setVisibility(View.VISIBLE);
                    if (userSocietyArrayList.get(position).getSociety() != null) {
                        mEditTextSocietyCity.setText(userSocietyArrayList.get(position).getSociety().getCity());
                        try {
                            mEditTextSocietyPinCode.setText("" + userSocietyArrayList.get(position).getSociety().getPostcode());
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        mSpinnerSocietyState.setText(userSocietyArrayList.get(position).getSociety().getState());
                        mEditTextSocietyName.setText(userSocietyArrayList.get(position).getSociety().getBuildingName());
                    } else {
                        mEditTextSocietyCity.setText(this.getResources().getString(R.string.no_data_error));
                        mEditTextSocietyPinCode.setText(this.getResources().getString(R.string.no_data_error));
                        mSpinnerSocietyState.setText(this.getResources().getString(R.string.no_data_error));
                        mEditTextSocietyName.setText(this.getResources().getString(R.string.no_data_error));
                    }
                    if (userSocietyArrayList.get(position).getFlat() != null) {
                        mEditTextSocietyBlock.setText(userSocietyArrayList.get(position).getBlock());
                        mEditTextSocietyFlatNumber.setText(userSocietyArrayList.get(position).getFlat());
                        mSpinnerSocietyType.setText(userSocietyArrayList.get(position).getResidenceType());
                        mEditTextSocietyIntercom.setText("" + userSocietyArrayList.get(position).getIntercom());
                        if (userSocietyArrayList.get(position).getLandline() != null) {
                            String landLine = userSocietyArrayList.get(position).getLandline();
                            mEditTextLandlineNumber.setText(landLine);
                        }
                        if (userSocietyArrayList.get(position).getOwnership() != null) {
                            if (userSocietyArrayList.get(position).getOwnership().equalsIgnoreCase("owner")) {
                                mRadioOwner.setEnabled(true);
                                mRadioOwner.setChecked(true);
                            } else if (userSocietyArrayList.get(position).getOwnership().equalsIgnoreCase("tenant")) {
                                mRadioTenant.setEnabled(true);
                                mRadioTenant.setChecked(true);
                            } else {
                            }
                        }
                    } else {
                        mEditTextSocietyBlock.setText(this.getResources().getString(R.string.no_data_error));
                        mEditTextSocietyFlatNumber.setText(this.getResources().getString(R.string.no_data_error));
                    }
                    if (userSocietyArrayList.get(position).getSociety() != null) {
                        mEditTextSocietyAddress1.setText(userSocietyArrayList.get(position).getSociety().getAddress1());
                        mEditTextSocietyAddress2.setText(userSocietyArrayList.get(position).getSociety().getAddress2());
                    } else {
                        mEditTextSocietyAddress1.setText(this.getResources().getString(R.string.no_data_error));
                        mEditTextSocietyAddress2.setText(this.getResources().getString(R.string.no_data_error));
                    }
                } else {
                    mLinearLayoutIntercome.setVisibility(View.GONE);
                    linearLayoutSociety.setVisibility(View.GONE);
                    if (userSocietyArrayList.size() > 0) {
                        mSpinnerSocietyType.setText(userSocietyArrayList.get(position).getResidenceType());
                        if (userSocietyArrayList.get(position).getAddress() != null) {
                            mEditTextSocietyCity.setText(userSocietyArrayList.get(position).getAddress().getCity());
                            try {
                                mEditTextSocietyPinCode.setText("" + userSocietyArrayList.get(position).getAddress().getZipcode());
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                            mSpinnerSocietyState.setText(userSocietyArrayList.get(position).getAddress().getState().getName());
                            String landLine = userSocietyArrayList.get(position).getAddress().getPhone();
                            mEditTextLandlineNumber.setText(landLine);
                        } else {
                            mEditTextSocietyCity.setText(this.getResources().getString(R.string.no_data_error));
                            mEditTextSocietyPinCode.setText(this.getResources().getString(R.string.no_data_error));
                            mSpinnerSocietyState.setText(this.getResources().getString(R.string.no_data_error));
                            mEditTextLandlineNumber.setText(this.getResources().getString(R.string.no_data_error));
                        }
                        if (userSocietyArrayList.get(position).getAddress() != null) {
                            mEditTextSocietyAddress1.setText(userSocietyArrayList.get(position).getAddress().getAddress1());
                            mEditTextSocietyAddress2.setText(userSocietyArrayList.get(position).getAddress().getAddress2());
                        } else {
                            mEditTextSocietyAddress1.setText(this.getResources().getString(R.string.no_data_error));
                            mEditTextSocietyAddress2.setText(this.getResources().getString(R.string.no_data_error));
                        }
                        if (userSocietyArrayList.get(position).getOwnership() != null) {
                            if (userSocietyArrayList.get(position).getOwnership().equalsIgnoreCase("owner")) {
                                mRadioOwner.setEnabled(true);
                                mRadioOwner.setChecked(true);
                            } else if (userSocietyArrayList.get(position).getOwnership().equalsIgnoreCase("tenant")) {
                                mRadioTenant.setEnabled(true);
                                mRadioTenant.setChecked(true);
                            } else {
                            }
                        }
                    }
                }
            }
        }
        mButtonSocietyDetailClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MySocietyDetailActivity.this.finish();
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home: {
                super.onBackPressed();
                return true;
            }
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onBackPressed() {
        MySocietyDetailActivity.this.finish();
    }

}
