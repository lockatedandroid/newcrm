package com.android.lockated.categories.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.android.lockated.Interfaces.IRecyclerItemClickListener;
import com.android.lockated.R;
import com.android.lockated.categories.vendors.model.VendorData;
import com.android.lockated.holder.RecyclerViewHolder;

import java.util.ArrayList;

public class VendorAdapter extends RecyclerView.Adapter<RecyclerViewHolder>
{
    private ArrayList<VendorData> _mVendorData;
    private IRecyclerItemClickListener mIRecyclerItemClickListener;

    public VendorAdapter(ArrayList<VendorData> data, IRecyclerItemClickListener listener)
    {
        _mVendorData = data;
        mIRecyclerItemClickListener = listener;
    }

    @Override
    public RecyclerViewHolder onCreateViewHolder(ViewGroup parent, int viewType)
    {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.fragment_food_supplier_row, parent, false);
        return new RecyclerViewHolder(itemView, mIRecyclerItemClickListener);
    }

    @Override
    public void onBindViewHolder(RecyclerViewHolder holder, int position)
    {
        holder.onSupplierViewHolder(_mVendorData.get(position));
    }

    @Override
    public int getItemCount()
    {
        return _mVendorData.size();
    }
}
