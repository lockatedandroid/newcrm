
package com.android.lockated.model.BaseApi;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class BaseAccountApi {
    private String id;
    private String email;
    private String firstname;
    private String lastname;
    private String mobile;
    private String spreeApiKey;
    private int numberVerified;
    private String zomatotoken;
    private String olaToken;
    private int selectedUserSociety;
    public boolean isApprove;
    private ArrayList<Society> mSocietyArrayList = new ArrayList<>();
    private ArrayList<Avatar> mAvatarArrayList = new ArrayList<>();

    public BaseAccountApi(JSONObject jsonObject) {
        try {
            email = jsonObject.optString("email");
            firstname = jsonObject.optString("firstname");
            lastname = jsonObject.optString("lastname");
            zomatotoken = jsonObject.optString("zomatotoken");
            olaToken = jsonObject.optString("olatoken");
            mobile = jsonObject.optString("mobile");
            numberVerified = jsonObject.optInt("number_verified");
            isApprove = jsonObject.getBoolean("is_approve");
            id = jsonObject.getString("id");
            selectedUserSociety = jsonObject.optInt("selected_user_society");
            Society society = new Society(jsonObject.getJSONObject("society"));
            mSocietyArrayList.add(society);
            Avatar avatar = new Avatar(jsonObject.getJSONObject("avatar"));
            mAvatarArrayList.add(avatar);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getSpreeApiKey() {
        return spreeApiKey;
    }

    public void setSpreeApiKey(String spreeApiKey) {
        this.spreeApiKey = spreeApiKey;
    }

    public int getNumberVerified() {
        return numberVerified;
    }

    public void setNumberVerified(int numberVerified) {
        this.numberVerified = numberVerified;
    }

    public String getZomatotoken() {
        return zomatotoken;
    }

    public void setZomatotoken(String zomatotoken) {
        this.zomatotoken = zomatotoken;
    }
    public String getOlatoken() {
        return olaToken;
    }

    public void setOlaToken(String olaToken) {
        this.olaToken = olaToken;
    }

    public boolean isIsApprove() {
        return isApprove;
    }

    public void setIsApprove(boolean isApprove) {
        this.isApprove = isApprove;
    }

    public int getSelectedUserSociety() {
        return selectedUserSociety;
    }

    public void setSelectedUserSociety(int selectedUserSociety) {
        this.selectedUserSociety = selectedUserSociety;
    }

    public ArrayList<Society> getSocietyDataList() {
        return mSocietyArrayList;
    }

}
