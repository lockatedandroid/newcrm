package com.android.lockated.categories.olacabs.fragment;


import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.ProgressDialog;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.webkit.WebResourceError;
import android.webkit.WebResourceRequest;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.android.lockated.R;
import com.android.lockated.model.OlaCab.OlaAutoBookResponse.AutoBookResponse;
import com.android.lockated.model.OlaCab.OlaCabBookResponse;
import com.android.lockated.network.ConnectionDetector;
import com.android.lockated.preferences.LockatedPreferences;
import com.android.lockated.request.LockatedRequestError;
import com.android.lockated.request.LockatedVolleyRequestQueue;
import com.android.lockated.utils.ApplicationURL;
import com.android.lockated.utils.Utilities;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

public class OlaCabAuthFragment extends DialogFragment implements Response.ErrorListener, Response.Listener<JSONObject> {

    private boolean cab_booking;
    //booking_category for cabs= 1, auto =2;
    private int booking_category = 0;


    WebView webview;
    Window window;
    private ProgressDialog progressDialog;
    Uri uri;
    String access_token, category;
    private float pickup_latitude, pickup_longitude;
    private float drop_latitude, drop_longitude;
    private static final int REQUEST_LOCATION = 10;
    private static final String REQUEST_TAG = "OLA CABS CONFIRM";
    public JSONObject response_json;

    public OlaCabBookResponse olaCabBookResponse ;
    public AutoBookResponse olaAutoBookResponse;


    private LockatedPreferences mLockatedPreferences;
    private FragmentTransaction fragmentTransaction;

    private ProgressBar progressBar;

    /*@Override
    public void onStart()
    {
        super.onStart();
        mLockatedPreferences = new LockatedPreferences(getActivity());
        window = getDialog().getWindow();
        WindowManager.LayoutParams windowParams = window.getAttributes();
        windowParams.dimAmount = 0.75f;
        windowParams.flags |= WindowManager.LayoutParams.FLAG_DIM_BEHIND;
        window.setAttributes(windowParams);
        window.setBackgroundDrawableResource(android.R.color.white);


        *//*progressDialog=new ProgressDialog(getActivity());
        progressDialog.setMessage("Please wait..");
        progressDialog.setTitle("Loading");
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.setIndeterminate(true);*//*

    }
*/

    @Override
    public void onStart() {
        super.onStart();
        mLockatedPreferences = new LockatedPreferences(getActivity());
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(getDialog().getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        getDialog().getWindow().setAttributes(lp);
        getDialog().getWindow().setTitle("One-time connect to Ola");
        getDialog().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
        //getDialog().getWindow().setTitleColor();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(STYLE_NORMAL,0);
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_ola_cab_auth, container);
        progressBar = (ProgressBar) view.findViewById(R.id.mProgressBarView);
        Bundle bundle = getArguments();
        if (bundle != null) {
            //-----------------29-7-2016---------------------

            /*pickup_latitude = bundle.getFloat("pickup_latitude");
            pickup_longitude = bundle.getFloat("pickup_longitude");
            drop_latitude = bundle.getFloat("drop_latitude");
            drop_longitude = bundle.getFloat("drop_longitude");*/

            //------------------------------------------------
            category = bundle.getString("category");
        }
        //olaCabBookResponses = new ArrayList<>();
        assert bundle != null;

        //-----------------29-7-2016---------------------

        /*Log.e("pickup_latitude : ", String.valueOf(pickup_latitude));
        Log.e("pickup_longitude : ", String.valueOf(pickup_longitude));
        Log.e("drop_latitude : ", String.valueOf(drop_latitude));
        Log.e("drop_longitude : ", String.valueOf(drop_longitude));

        //------------------------------------------------
*/
        init(view);
        return view;
    }

   /* @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        View connectToView = getActivity().getLayoutInflater().inflate(R.layout.fragment_ola_cab_auth, new LinearLayout(getActivity()), false);
        init(connectToView);

        progressBar = (ProgressBar) connectToView.findViewById(R.id.mProgressBarView);

        Bundle bundle = getArguments();

        if (bundle != null) {
            pickup_latitude = bundle.getFloat("pickup_latitude");
            pickup_longitude = bundle.getFloat("pickup_longitude");
            drop_latitude = bundle.getFloat("drop_latitude");
            drop_longitude = bundle.getFloat("drop_longitude");
            category = bundle.getString("category");
        }
        //olaCabBookResponses = new ArrayList<>();
        assert bundle != null;

        Log.e("pickup_latitude : ", String.valueOf(pickup_latitude));
        Log.e("pickup_longitude : ", String.valueOf(pickup_longitude));
        Log.e("drop_latitude : ", String.valueOf(drop_latitude));
        Log.e("drop_longitude : ", String.valueOf(drop_longitude));

        Dialog builder = new Dialog(getActivity());
        //builder.requestWindowFeature(Window.FEATURE_NO_TITLE);
        builder.setTitle("One Time Login");
        builder.getWindow().setBackgroundDrawable(new ColorDrawable(Color.WHITE));

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(builder.getWindow().getAttributes());

        //lp.width = WindowManager.LayoutParams.WRAP_CONTENT;
        //lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        Point size = new Point();

        // Store dimensions of the screen in `size`
        Display display = getActivity().getWindowManager().getDefaultDisplay();
        display.getSize(size);

        // Set the width of the dialog proportional to 75% of the screen width
        lp.width = (int) (size.x * 0.6);
        lp.height = (int) (size.y * 0.7);

        builder.show();
        builder.getWindow().setAttributes(lp);
        builder.setContentView(connectToView);

        return builder;

    }*/

    private void init(View connectToView) {
        webview = (WebView) connectToView.findViewById(R.id.webView);
        webview.setOnKeyListener(new View.OnKeyListener()
        {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event)
            {
                if(keyCode == KeyEvent.KEYCODE_BACK && event.getAction() == KeyEvent.ACTION_DOWN)
                {
                    if(webview.canGoBack()){
                        webview.goBack();
                        return true;
                    }
                }
                return false;
            }
        });


        // ola cab api uri   https://m.olacabs.com/api/login
        //webview.loadUrl("http://sandbox-t.olacabs.com/oauth2/authorize?response_type=token&client_id=Y2RkYjRlZWQtMDE5ZC00OGExLTkyYzktMjc1NmViODZjZDA1&redirect_uri=https://www.lockated.com&scope=profile%20booking&state=state123");
        webview.loadUrl("https://devapi.olacabs.com/oauth2/authorize?response_type=token&client_id=NTQ5Y2Q1OTQtN2VhNy00MWNlLTlkODAtZTE5OTY1ODg0MGE5&redirect_uri=https://www.lockated.com&scope=profile%20booking&state=state123");
        webview.getSettings().setJavaScriptEnabled(true);

        webview.setWebViewClient(new WebViewClient() {

            @SuppressLint("LongLogTag")
            @Override
            public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
                super.onReceivedError(view, errorCode, description, failingUrl);
                Toast.makeText(getActivity(), "Oh no! " + description, Toast.LENGTH_SHORT).show();
                Log.e("onReceivedError errorCode", String.valueOf(errorCode));
                Log.e("onReceivedError description",description);
            }

            @TargetApi(Build.VERSION_CODES.LOLLIPOP)
            @Override
            public void onReceivedError(WebView view, WebResourceRequest request, WebResourceError error) {
                super.onReceivedError(view, request, error);
                Toast.makeText(getActivity(), "Oh no! " + error, Toast.LENGTH_SHORT).show();
                Log.e(" onReceivedError URL", "" + uri);
            }

            //This method can be overridden to let WebView handle HTTPS requests.
            @Override
            public boolean shouldOverrideKeyEvent(WebView view, KeyEvent event) {
                return super.shouldOverrideKeyEvent(view, event);
            }

            // Making is will call at the click of the request is a link, override this method returns true Webpage that click inside the link or jump in the current WebView, do not jump to the browser.
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                //progressDialog.show();
                progressBar.setVisibility(View.VISIBLE);
                webview.setVisibility(View.GONE);
                return super.shouldOverrideUrlLoading(view, url);
            }

            //Override this method to process the event in the browser.
            @Override
            public void onLoadResource(WebView view, String url) {
                super.onLoadResource(view, url);
                //Log.e("onLoadResource URL", url);
            }

            //Call in the page loading at the beginning.
            @Override
            public void onPageStarted(WebView view, String url, Bitmap favicon) {
                super.onPageStarted(view, url, favicon);
                Log.e("onPageStarted URL", url);
                if (url.equals("https://www.lockated.com/#error=access_denied&state=state123")) //"http://www.lockated.com/#error=access_denied&state=state123"
                {
                    webview.removeAllViews();
                    webview.destroy();
                    dismiss();
                }
                else if (url.startsWith("https://www.lockated.com/#access_token")) //"https://www.lockated.com/#access_token"
                {
                    Log.e("getting URL", url);

                    uri = Uri.parse(url);
                    String temp_token = uri.getEncodedFragment();
                    Log.e("access_token", "" + access_token);

                    Uri uri1 = Uri.parse("https://www.lockated.com" + "?" + temp_token);
                    access_token = uri1.getQueryParameter("access_token");
                    Log.e("access_token", "" + access_token);
                    updateOlaToken(access_token);
                    // ----------------changes done on 5-8-2016 3.19 pm ------

                        mLockatedPreferences.setOlaToken(access_token);

                    //---------------------------------------------------------

                    if (access_token != null) {
                        if (cab_booking) {
                            webview.removeAllViews();
                            webview.destroy();
                            dismiss();
                        } else {
                            sendConfirmation();
                        }
                    }
                }
                else
                {
                    //progressDialog.show();
                    webview.setVisibility(View.GONE);
                    progressBar.setVisibility(View.VISIBLE);
                }
            }

            //Call the page loads at the end.
            @Override
            public void onPageFinished(WebView view, String url) {
                super.onPageFinished(view, url);
                //progressDialog.dismiss();
                webview.setVisibility(View.VISIBLE);
                progressBar.setVisibility(View.GONE);
                Log.e("onPageFinished URL", url);
            }
        });
    }


    public void sendConfirmation() {
        try {
            Log.e("sendCabConfirmation", "m inside");
            if (ConnectionDetector.isConnectedToInternet(getActivity())) {

                //String url = "http://sandbox-t.olacabs.com/v1/bookings/create";
                String url = "https://devapi.olacabs.com/v1/bookings/create";

                final JSONObject jsonObject;
                if(!category.equals("auto")) {
                    jsonObject = new JSONObject();

                    //pickup_latitude = 12.950121f;
                    //pickup_longitude = 77.642917f;

                    pickup_latitude = mLockatedPreferences.getSrcLat();
                    pickup_longitude = mLockatedPreferences.getSrcLng();

                    jsonObject.put("pickup_lat", pickup_latitude);
                    jsonObject.put("pickup_lng", pickup_longitude);

                    //jsonObject.put("pickup_lat", 12.950121f);
                    //jsonObject.put("pickup_lng", 77.642917f);

                    jsonObject.put("pickup_mode", "NOW");
                    jsonObject.put("category", category);
                }
                else
                {
                    jsonObject = new JSONObject();

                    pickup_latitude = 12.950121f;
                    pickup_longitude = 77.642917f;
                    drop_latitude = 12.951074f;
                    drop_longitude = 77.644598f;

                    jsonObject.put("pickup_lat", pickup_latitude);
                    jsonObject.put("pickup_lng", pickup_longitude);
                    jsonObject.put("drop_lat", drop_latitude);
                    jsonObject.put("drop_lng", drop_longitude);

                    //jsonObject.put("pickup_lat", 12.950121f);
                    //jsonObject.put("pickup_lng", 77.642917f);
                    //jsonObject.put("drop_lat", 12.951074f);
                    //jsonObject.put("drop_lng", 77.644598f);

                    jsonObject.put("pickup_mode", "NOW");
                    jsonObject.put("category", category);

                }

                Log.e("Request JsonObject", "" + jsonObject.toString());

                LockatedVolleyRequestQueue mLockatedVolleyRequestQueue = LockatedVolleyRequestQueue.getInstance(getActivity());
                mLockatedVolleyRequestQueue.sendPostRequestOlaCabsConfirmEx(REQUEST_TAG, Request.Method.POST, url, access_token, jsonObject, new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.e("Response JsonObject", "" + response);

                        try {
                            if (response != null && response.length() > 0) {
                                if (response.has("message") && response.has("code")) {
                                    bookingResponse(cab_booking, booking_category);
                                    Utilities.showToastMessage(getActivity(), response.getString("message"));
                                } else {
                                    if (category.equals("auto")) {
                                        cab_booking = true;
                                        booking_category = 2;

                                        response_json = response;
                                        Gson gson = new Gson();
                                        //OlaCabBookResponse olaCabBookResponse = gson.fromJson(response.toString(), OlaCabBookResponse.class);
                                        //olaCabBookResponses.add(olaCabBookResponse);
                                        olaAutoBookResponse = gson.fromJson(response.toString(), AutoBookResponse.class);

                                        Log.e("BookingId ", "" + olaAutoBookResponse.getBookingId());
                                        Log.e("Message ", "" + olaAutoBookResponse.getMessage());
                                        Log.e("Status ", "" + olaAutoBookResponse.getStatus());

                                        //olaCabBookResponses.add(olaCabCreateResponse);
                                        bookingResponse(cab_booking, booking_category);
                                        Utilities.showToastMessage(getActivity(), "Your Auto is  Booked Successfully");
                                    } else {
                                        cab_booking = true;
                                        booking_category = 1;
                                        response_json = response;
                                        Gson gson = new Gson();
                                        //OlaCabBookResponse olaCabBookResponse = gson.fromJson(response.toString(), OlaCabBookResponse.class);
                                        //olaCabBookResponses.add(olaCabBookResponse);
                                        olaCabBookResponse = gson.fromJson(response.toString(), OlaCabBookResponse.class);

                                        Log.e("BookingId ", "" + olaCabBookResponse.getBookingId());
                                        Log.e("CabNumber ", "" + olaCabBookResponse.getCabNumber());

                                        //olaCabBookResponses.add(olaCabCreateResponse);
                                        bookingResponse(cab_booking, booking_category);
                                        Utilities.showToastMessage(getActivity(), "Your Cab is  Booked Successfully");
                                    }
                                }

                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                    }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.e("VolleyError", "" + error);
                        if (getActivity() != null) {
                            LockatedRequestError.onRequestError(getActivity(), error);
                        }
                    }
                });

            } else {
                Utilities.showToastMessage(getActivity(), getActivity().getResources().getString(R.string.internet_connection_error));
            }

        } catch (Exception e) {
            e.getMessage();
        }

    }

    /*private void bookingResponse(boolean cab_booking) {
        if (cab_booking) {
            //sendBookResponseToServer(response_json);

            OlaCabCancelFragment olaCabCancelFragment = new OlaCabCancelFragment();
            Bundle bundle_new = new Bundle();
            bundle_new.putString("booking_id",olaCabBookResponse.getBookingId());
            bundle_new.putString("cab_number",olaCabBookResponse.getCabNumber());
            bundle_new.putString("cab_type",olaCabBookResponse.getCabType());
            bundle_new.putString("car_model",olaCabBookResponse.getCarModel());
            bundle_new.putString("crn",olaCabBookResponse.getCrn());
            bundle_new.putString("driver_name",olaCabBookResponse.getDriverName());
            bundle_new.putString("driver_number",olaCabBookResponse.getDriverNumber());
            bundle_new.putFloat("driver_lat",olaCabBookResponse.getDriverLat());
            bundle_new.putFloat("driver_lng",olaCabBookResponse.getDriverLng());
            bundle_new.putString("surcharge_value",olaCabBookResponse.getShareRideUrl());
            bundle_new.putInt("eta",olaCabBookResponse.getEta());
            bundle_new.putString("AccessToken",access_token);

            if(olaCabBookResponse.getSurchargeValue()!= null) {
                bundle_new.putString("surcharge_value", "" + olaCabBookResponse.getSurchargeValue());
            }

            OlaMainActivity_Latest.setDriverLocation(olaCabBookResponse.getDriverLat(),olaCabBookResponse.getDriverLng());
            //ScheduledService scheduledService = new ScheduledService(getActivity(),olaCabBookResponse.getBookingId());

           *//* Intent i= new Intent(getActivity(), ScheduledService.class);
            getActivity().startService(i);*//*


            olaCabCancelFragment.setArguments(bundle_new);

            fragmentTransaction = getFragmentManager().beginTransaction();
            fragmentTransaction.replace(R.id.mFrameContainer, olaCabCancelFragment);
            fragmentTransaction.addToBackStack(null);
            fragmentTransaction.commit();
            webview.removeAllViews();
            webview.destroy();
            dismiss();
            Log.e("cab_booking ", "Response true" + "m here");

        } else {
            Log.e("cab_booking ", "Response false" + "m here");
        }
    }*/

    private void bookingResponse(boolean cab_booking, int booking_category) {
        if (cab_booking) {
            //sendBookResponseToServer(response_json);

            if (booking_category == 1) {
                OlaCabCancelFragment olaCabCancelFragment = new OlaCabCancelFragment();
                Bundle bundle_new = new Bundle();
                bundle_new.putString("booking_id", olaCabBookResponse.getBookingId());
                bundle_new.putString("cab_number", olaCabBookResponse.getCabNumber());
                bundle_new.putString("cab_type", olaCabBookResponse.getCabType());
                bundle_new.putString("car_model", olaCabBookResponse.getCarModel());
                bundle_new.putString("crn", olaCabBookResponse.getCrn());
                bundle_new.putString("driver_name", olaCabBookResponse.getDriverName());
                bundle_new.putString("driver_number", olaCabBookResponse.getDriverNumber());
                bundle_new.putFloat("driver_lat", olaCabBookResponse.getDriverLat());
                bundle_new.putFloat("driver_lng", olaCabBookResponse.getDriverLng());
                bundle_new.putFloat("user_lat",pickup_latitude);
                bundle_new.putFloat("user_lng",pickup_longitude);
                bundle_new.putString("share_ride_url", olaCabBookResponse.getShareRideUrl());
                bundle_new.putInt("eta", olaCabBookResponse.getEta());

                //-------------- changes done on 5-8-2016--3.17 pm--------------

                    //bundle_new.putString("AccessToken", access_token);

                //--------------------------------------------------------------

                if (olaCabBookResponse.getSurchargeValue() != null) {
                    bundle_new.putString("surcharge_value", "" + olaCabBookResponse.getSurchargeValue());
                }

                //OlaMainActivity_Latest.setDriverLocation(olaCabBookResponse.getDriverLat(), olaCabBookResponse.getDriverLng());
                olaCabCancelFragment.setArguments(bundle_new);

                FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
                fragmentTransaction.replace(R.id.mFrameContainer, olaCabCancelFragment);
                fragmentTransaction.addToBackStack(null);
                fragmentTransaction.commit();
                dismiss();
                Log.e("cab_booking ", "Response true" + "m here");
            }
            else if (booking_category == 2) {

                OlaAutoCancelFragment olaAutoCancelFragment = new OlaAutoCancelFragment();
                Bundle bundle_new = new Bundle();
                bundle_new.putString("booking_id", olaAutoBookResponse.getBookingId());
                bundle_new.putString("status", olaAutoBookResponse.getStatus());
                bundle_new.putString("message", olaAutoBookResponse.getMessage());
                bundle_new.putString("AccessToken", access_token);


                //OlaMainActivity_Latest.setDriverLocation(olaCabBookResponse.getDriverLat(), olaCabBookResponse.getDriverLng());
                olaAutoCancelFragment.setArguments(bundle_new);

                FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
                fragmentTransaction.replace(R.id.mFrameContainer, olaAutoCancelFragment);
                fragmentTransaction.addToBackStack(null);
                fragmentTransaction.commit();
                dismiss();
                Log.e("cab_booking ", "Response true" + "m here");
            }
        } else {
            dismiss();
            Log.e("cab_booking ", "Response false" + "m here");
        }
    }

    @SuppressLint("LongLogTag")
    private void sendBookResponseToServer(JSONObject jsonObject)
    {
        Log.e(" Inside:",""+"sendBookResponseToServer");
        if (getActivity() != null) {
            if (ConnectionDetector.isConnectedToInternet(getActivity())) {
                String url = "https://www.lockated.com/add_ola_orders.json?token="+mLockatedPreferences.getLockatedToken();
                Log.e("url of sendBookResponseToServer",""+url);
                LockatedVolleyRequestQueue lockatedVolleyRequestQueue = LockatedVolleyRequestQueue.getInstance(getActivity());
                lockatedVolleyRequestQueue.sendPostRequestLocated(REQUEST_TAG, Request.Method.POST, url,jsonObject, this, this);
            } else {
                Utilities.showToastMessage(getActivity(), getActivity().getResources().getString(R.string.internet_connection_error));
            }
        }
    }

    public void updateOlaToken(String s)
    {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("user",new JSONObject().put("olatoken",s));
            if (getActivity() != null) {
                if (ConnectionDetector.isConnectedToInternet(getActivity())) {
                    //String url = ApplicationURL.updateSingleZomatoTransaction + mLockatedPreferences.getLockatedToken();
                    String url = ApplicationURL.updatePersonalInformatio + mLockatedPreferences.getLockatedToken();

                    Log.e("url", url);
                    LockatedVolleyRequestQueue lockatedVolleyRequestQueue = LockatedVolleyRequestQueue.getInstance(getActivity());
                    lockatedVolleyRequestQueue.sendPostRequestOlaCabsConfirmEx(REQUEST_TAG, Request.Method.PUT, url,null, jsonObject, this, this);
                } else {
                    Utilities.showToastMessage(getActivity(), getActivity().getResources().getString(R.string.internet_connection_error));
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onErrorResponse(VolleyError error) {
        Log.e("Error",""+error);
    }

    @Override
    public void onResponse(JSONObject response) {
        Log.e("Response",""+response);
    }
}
