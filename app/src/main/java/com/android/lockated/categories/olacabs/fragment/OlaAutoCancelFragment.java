package com.android.lockated.categories.olacabs.fragment;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.SlidingPaneLayout;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import com.android.lockated.R;
import com.android.lockated.categories.olacabs.activity.OlaMainActivity_Latest;
import com.android.lockated.categories.olacabs.adapter.OlaReasonAdapter;
import com.android.lockated.information.AccountController;
import com.android.lockated.model.AccountData;
import com.android.lockated.model.OlaCab.CancelResponse;
import com.android.lockated.model.OlaCab.OlaAutoCompleteResponse.OlaAutoCompleteResponse;
import com.android.lockated.model.OlaCab.OlaAutoTrackResponse.OlaAutoTrackReponse;
import com.android.lockated.model.OlaCab.OlaCabCancelReason.CancelReasons;
import com.android.lockated.network.ConnectionDetector;
import com.android.lockated.preferences.LockatedPreferences;
import com.android.lockated.request.LockatedVolleyRequestQueue;
import com.android.lockated.utils.Utilities;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

/**
 * Created by HAVEN INFOLINE on 7/11/2016.
 */
public class OlaAutoCancelFragment extends Fragment implements View.OnClickListener, Response.ErrorListener, Response.Listener<JSONObject> {

    private static final String REQUEST_TAG = "OLA AUTO CANCEL";

    TextView txt_ola_auto_bookid,txt_ola_auto_bookmsg;
    Button btn_cancel_auto;
    private String access_token = null;

    public CancelReasons cancelReasons;
    public CancelResponse cancelResponse;

    private ProgressDialog mProgressDialog;

    private AccountController accountController;
    private ArrayList<AccountData> accountDataArrayList;

    private float DriverLat, DriverLng,UserLat,UserLng;

    private String DriverNumber,DriverName,VehicleNumber,VehicleType;


    public OlaAutoTrackReponse trackResponse;
    public OlaAutoCompleteResponse completeResponse;

    private LockatedPreferences mLockatedPreferences;
    String BookingId,BookingMessage,BookingStatus,AccessToken,BookingDateTime;

    String reason;

    private static final int REQUEST_CALL = 11;
    private SlidingPaneLayout mLayout;

    private OlaReasonAdapter adapter;
    JSONObject confirm_jsonObject;
    public OlaAutoCancelFragment() {
        // Required empty public constructor
    }


    @SuppressLint("LongLogTag")
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //OlaMainActivity_Latest.mLayout.setPanelHeight(300);
        OlaMainActivity_Latest.mLayout.setEnabled(false);
        OlaMainActivity_Latest.imageMarker.setVisibility(View.GONE);
        OlaMainActivity_Latest.edt_olaSourceLocation.setFocusable(false);
        OlaMainActivity_Latest.edt_olaSourceLocation.setOnClickListener(null);
        OlaMainActivity_Latest.edt_olaSourceLocation.setOnTouchListener(null);
        OlaMainActivity_Latest.edt_olaSourceLocation.setClickable(false);

        if(OlaMainActivity_Latest.mGoogleApiClient != null) {
            Log.e("onCreateView of OlaCabCancelFragment ","mGoogleApiClient != null"+"Yes");
            OlaMainActivity_Latest.mGoogleApiClient.disconnect();

            // need to be changed
        }
        else {
            Log.e("onCreateView of OlaCabCancelFragment ","mGoogleApiClient == null"+"Yes");
            OlaMainActivity_Latest.mGoogleApiClient.connect();
        }

        Log.e("I am ","onCreate of Auto Cancel");
        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);


        mLockatedPreferences = new LockatedPreferences(getActivity());
        accountController = AccountController.getInstance();
        accountDataArrayList = accountController.getmAccountDataList();


        Log.e("Tesst log before is tpdetail -- ", "tpdetail");
        Log.e("tpdetails onCab", ""+mLockatedPreferences.getTpDetail());
        Log.e("Tesst log before isautoBooked -- ", "teestbefore");
        Log.e("isautoBooked onCab", ""+mLockatedPreferences.getIsAutoBooked());
        Log.e("Tesst log after isautoBooked -- ", "teestafter");

        if (mLockatedPreferences.getIsAutoBooked()) {

            Log.e("Ola Token", "" + accountController.getmAccountDataList().get(0).getOlaToken());
            AccessToken = accountController.getmAccountDataList().get(0).getOlaToken();

            Log.e("I am","getIsBooked");
            String jsonString = mLockatedPreferences.getTpDetail();
            JSONObject jsonObject;
            try {
                jsonObject = new JSONObject(jsonString);
                BookingId = jsonObject.getString("booking_id");
            /*    VehicleNumber = jsonObject.getString("vehicle_number");
                VehicleType = jsonObject.getString("vehicle_type");
                DriverName = jsonObject.getString("driver_name");
                DriverNumber = jsonObject.getString("driver_number");
                DriverLat = (float) jsonObject.getDouble("driver_lat");
                DriverLng = (float) jsonObject.getDouble("driver_lng");
                UserLat = (float) jsonObject.getDouble("user_lat");
                UserLng = (float) jsonObject.getDouble("user_lng");*/
                BookingMessage = jsonObject.getString("message");
                BookingStatus = jsonObject.getString("status");

                //OlaMainActivity_Latest.setDriverLocation(DriverLat,DriverLng);
            } catch (JSONException e) {
                e.printStackTrace();
            }


        }
        else
        {
            if (savedInstanceState == null)
            {
                Bundle bundle = getArguments();
                Log.e("Bundle Arguments", "" + getArguments());
                mLockatedPreferences = new LockatedPreferences(getActivity());
                if (bundle != null) {
                    BookingId = bundle.getString("booking_id");
                    BookingMessage = bundle.getString("message");
                    BookingStatus = bundle.getString("status");
                    AccessToken = bundle.getString("AccessToken");
                    UserLat =  bundle.getFloat("user_lat");
                    UserLng = bundle.getFloat("user_lng");
                }
            }
            else {
                BookingId = savedInstanceState.getString("booking_id");
                UserLat = savedInstanceState.getFloat("user_lat");
                UserLng = savedInstanceState.getFloat("user_lng");
                BookingMessage = savedInstanceState.getString("message");
                BookingStatus = savedInstanceState.getString("status");
                AccessToken = savedInstanceState.getString("AccessToken");

                Log.e("BookingId", "" + savedInstanceState.getString("booking_id"));
                Log.e("UserLat", "" + savedInstanceState.getFloat("user_lat"));
                Log.e("UserLng", "" + savedInstanceState.getFloat("user_lng"));
                Log.e("BookingMessage", "" + savedInstanceState.getString("share_ride_url"));
                Log.e("BookingStatus", "" + savedInstanceState.getInt("eta"));
                Log.e("AccessToken", "" + savedInstanceState.getString("surcharge_value"));

            }


            confirm_jsonObject = new JSONObject();
            try {
                confirm_jsonObject.put("booking_id", BookingId);
                confirm_jsonObject.put("message", BookingMessage);
                confirm_jsonObject.put("status", BookingStatus);
                confirm_jsonObject.put("user_lat",UserLat);
                confirm_jsonObject.put("user_lng",UserLng);
                confirm_jsonObject.put("booking_status","Booked");
                confirm_jsonObject.put("current_status",true);

                mLockatedPreferences.setIsAutoBooked(true);
                sendBookResponseToServer(confirm_jsonObject);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        setHasOptionsMenu(true);
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        outState.putString("booking_id", BookingId);
        outState.putString("message", BookingMessage);
        outState.putString("status", BookingStatus);
        outState.putString("AccessToken", AccessToken);
        outState.putFloat("user_lat", UserLat);
        outState.putFloat("user_lng", UserLng);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view =  inflater.inflate(R.layout.fragment_ola_auto_cancel, container, false);

        OlaMainActivity_Latest.mLayout.setEnabled(false);

        getActivity().setTitle("Cancellation");

        final ScheduledExecutorService executor = Executors.newScheduledThreadPool(1);
        executor.scheduleAtFixedRate(new Runnable() {
            @SuppressLint("LongLogTag")
            @Override
            public void run() {
                Log.e(" Inside:", "" + "Runnable Task");
                if (getActivity() != null) {
                    if (ConnectionDetector.isConnectedToInternet(getActivity())) {
                        String url = "http://sandbox-t.olacabs.com/v1/bookings/track_ride?booking_id="+ BookingId;
                        Log.e("url of sendBookResponseToServer", "" + url);
                        LockatedVolleyRequestQueue lockatedVolleyRequestQueue = LockatedVolleyRequestQueue.getInstance(getActivity());
                        lockatedVolleyRequestQueue.sendPostRequestOlaCabsConfirmEx(REQUEST_TAG, Request.Method.GET, url, AccessToken, null, new Response.Listener<JSONObject>() {
                            @Override
                            public void onResponse(JSONObject response) {
                                Log.e("Response", response.toString());
                                Gson gson = new Gson();
                                try {
                                    if (response.has("message") && response.has("code")) {
                                        Log.e("Response",response.toString());
                                        Utilities.showToastMessage(getActivity(), response.getString("message"));
                                    } else if (response.has("booking_status")) {

                                        if (response.getString("booking_status").equals("PENDING"))
                                        {
                                            Log.e("Response",response.toString());
                                            //trackResponse = gson.fromJson(response.toString(), TrackResponse.class);
                                            OlaMainActivity_Latest.setDriverLocation(UserLat,UserLng,0,0,false);
                                        }

                                        else if (response.getString("booking_status").equals("ALLOTMENT_RETRYING"))
                                        {
                                            Log.e("Response",response.toString());
                                            //trackResponse = gson.fromJson(response.toString(), TrackResponse.class);
                                            //OlaMainActivity_Latest.setDriverLocation(0,0,trackResponse.getDriverLat(),trackResponse.getDriverLng());
                                            OlaMainActivity_Latest.setDriverLocation(UserLat,UserLng,0,0,false);
                                        }

                                        else if (response.getString("booking_status").equals("ALLOTMENT_FAILED"))
                                        {
                                            Log.e("Response",response.toString());
                                            Utilities.showToastMessage(getActivity(), response.getString("Allotment Failed Please Cancel ride and Try Again"));
                                            OlaMainActivity_Latest.setDriverLocation(UserLat,UserLng,0,0,false);
                                            executor.shutdown();

                                            //trackResponse = gson.fromJson(response.toString(), TrackResponse.class);
                                            //OlaMainActivity_Latest.setDriverLocation(0,0,trackResponse.getDriverLat(),trackResponse.getDriverLng());
                                        }

                                        else if (response.getString("booking_status").equals("CALL_DRIVER"))
                                        {
                                            Log.e("Response",response.toString());
                                            trackResponse = gson.fromJson(response.toString(), OlaAutoTrackReponse.class);
                                            OlaMainActivity_Latest.setDriverLocation(UserLat,UserLng,trackResponse.getDriverLat(),trackResponse.getDriverLng(),true);

                                            OlaAutoConfirmCancel olaAutoCabConfirmCancel = new OlaAutoConfirmCancel();

                                            Bundle bundle = new Bundle();
                                            bundle.putString("Response",response.toString());
                                            olaAutoCabConfirmCancel.setArguments(bundle);

                                            FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
                                            fragmentTransaction.replace(R.id.mFrameContainer, olaAutoCabConfirmCancel);
                                            fragmentTransaction.commit();

                                            executor.shutdown();
                                        }
                                        else if (response.getString("booking_status").equals("IN_PROGRESS"))
                                        {
                                            Log.e("Response",response.toString());
                                            DriverLat = (float) response.getDouble("driver_lat");
                                            DriverLng = (float) response.getDouble("driver_lng");
                                            //trackResponse = gson.fromJson(response.toString(), TrackResponse.class);
                                            //OlaMainActivity_Latest.setDriverLocation(0,0,trackResponse.getDriverLat(),trackResponse.getDriverLng());
                                            OlaMainActivity_Latest.setDriverLocation(UserLat,UserLng,DriverLat,DriverLng,true);

                                        }
                                        else if (response.getString("booking_status").equals("INVOICE"))
                                        {
                                            Log.e("Response",response.toString());
                                            //trackResponse = gson.fromJson(response.toString(), TrackResponse.class);
                                            //OlaMainActivity_Latest.setDriverLocation(0,0,trackResponse.getDriverLat(),trackResponse.getDriverLng());
                                        }
                                        else if (response.getString("booking_status").equals("COMPLETED"))
                                        {
                                            Log.e("Response",response.toString());
                                            completeResponse = gson.fromJson(response.toString(), OlaAutoCompleteResponse.class);
                                            //OlaMainActivity_Latest.setDriverLocation(0,0,completeResponse.getDriverLat(),trackResponse.getDriverLng());
                                            sendTrackResponseToServer(response.put("current_status",false));
                                        }
                                        else if (response.getString("booking_status").equals("BOOKING_CANCELLED"))
                                        {
                                            Log.e("Response",response.toString());
                                            //trackResponse = gson.fromJson(response.toString(), TrackResponse.class);
                                            ///OlaMainActivity_Latest.setDriverLocation(trackResponse.getDriverLat(),trackResponse.getDriverLng());
                                            executor.shutdown();
                                        }
                                        else if (response.getString("reason").equals("BOOKING_ALREADY_CANCELLED"))
                                        {
                                            Log.e("Response",response.toString());
                                            //trackResponse = gson.fromJson(response.toString(), TrackResponse.class);
                                            //OlaMainActivity_Latest.setDriverLocation(trackResponse.getDriverLat(),trackResponse.getDriverLng());
                                        }

                                    }
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }
                        }, new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                Log.e("Error Response",error.toString());
                            }
                        });
                    } else {
                        Utilities.showToastMessage(getActivity(), getActivity().getResources().getString(R.string.internet_connection_error));
                    }
                }
            }
        }, 0, 60 * 1000, TimeUnit.MILLISECONDS);
        init(view);
        getCancelReason(AccessToken);
        return view;
    }

    public void init(View view) {
        btn_cancel_auto = (Button) view.findViewById(R.id.btn_cancel_auto);

        txt_ola_auto_bookid = (TextView) view.findViewById(R.id.txt_ola_auto_bookid);
        txt_ola_auto_bookmsg = (TextView) view.findViewById(R.id.txt_ola_auto_bookmsg);


        Log.e("Booking Id ","" + BookingId);
        Log.e("Booking Status ","" + BookingStatus);
        Log.e("Booking Message ","" + BookingMessage);

        txt_ola_auto_bookid.setText(BookingId);
        txt_ola_auto_bookmsg.setText(BookingMessage);


        btn_cancel_auto.setOnClickListener(this);

    }


    public void sendCabCancel() {
        try {
            Log.e("sendCabCancel", "m inside");
            Log.e("reason", reason);
            if (ConnectionDetector.isConnectedToInternet(getActivity())) {

                mProgressDialog = ProgressDialog.show(getActivity(), "", "Please Wait...", false);
                String url = "http://sandbox-t.olacabs.com/v1/bookings/cancel";

                JSONObject jsonObject = new JSONObject();
                jsonObject.put("booking_id", BookingId);
                jsonObject.put("reason", reason);

                Log.e("Request JsonObject", "" + jsonObject.toString());
                LockatedVolleyRequestQueue mLockatedVolleyRequestQueue = LockatedVolleyRequestQueue.getInstance(getActivity());
                mLockatedVolleyRequestQueue.sendPostRequestOlaCabsConfirmEx(REQUEST_TAG, Request.Method.POST, url, AccessToken, jsonObject, this,this);

            } else {
                Utilities.showToastMessage(getActivity(), getActivity().getResources().getString(R.string.internet_connection_error));
            }

        } catch (Exception e) {
            e.getMessage();
        }

    }

    @Override
    public void onResume()
    {
        super.onResume();
    }


    public void getCancelReason(String access_token) {
        if (getActivity() != null) {
            if (ConnectionDetector.isConnectedToInternet(getActivity())) {
                String url = "http://sandbox-t.olacabs.com/v1/bookings/cancel/reasons";
                Log.e("getCancelReason", "" + url);

                LockatedVolleyRequestQueue mLockatedVolleyRequestQueue = LockatedVolleyRequestQueue.getInstance(getActivity());
                mLockatedVolleyRequestQueue.sendPostRequestOlaCabsConfirmEx(REQUEST_TAG, Request.Method.GET, url, access_token,null,new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.e("Cancel Response", "" + response);
                        if (response != null && response.length() > 0) {
                            try {
                                if (response.has("message") && response.has("code")) {
                                    Utilities.showToastMessage(getActivity(), response.getString("message"));
                                }
                                else if(response.has("cancel_reasons"))
                                {
                                    Gson gson = new Gson();
                                    cancelReasons = gson.fromJson(response.get("cancel_reasons").toString(), CancelReasons.class);

                                    for ( int i =0 ; i<cancelReasons.getAuto().size();i++) {
                                        Log.e("Cancel Reason", ""+cancelReasons.getAuto().get(i));
                                    }
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    }
                }, this);
            } else {
                Utilities.showToastMessage(getActivity(), getActivity().getResources().getString(R.string.internet_connection_error));
            }
        }
    }


    @Override
    public void onClick(View v) {
        switch (v.getId())
        {
            case R.id.btn_cancel_auto:
            {
                getReasonAlert("auto");
                break;
            }

        }

    }

    @Override
    public void onErrorResponse(VolleyError error) {
        Log.e("Error Response",error.toString());
    }

    @SuppressLint("LongLogTag")
    @Override
    public void onResponse(JSONObject response) {
        Log.e("Response",""+response);
        if (response != null && response.length() > 0) {
            try {
                if (response.has("message") && response.has("code")) {
                    Utilities.showToastMessage(getActivity(), response.getString("message"));
                    Log.e("Response",response.toString());
                }
                else if (response.has("request_type") && response.has("status"))
                {
                    Gson gson = new Gson();
                    cancelResponse = gson.fromJson(response.toString(), CancelResponse.class);

                    Log.e("request_type ", "" + cancelResponse.getRequestType());
                    Log.e("status ", "" + cancelResponse.getStatus());
                    Log.e("text ", "" + cancelResponse.getText());
                    Log.e("header ", "" + cancelResponse.getHeader());

                    /*response.put("booking_status","BOOKING_CANCELLED");
                    response.put("booking_id",BookingId);*/

                    response.put("booking_status","BOOKING_CANCELLED");
                    response.put("current_status",false);
                    response.put("booking_id",BookingId);

                    Log.e("Response",response.toString());
                    sendBookResponseToServer(response);
                    Utilities.showToastMessage(getActivity(),""+cancelResponse.getText());
                }
                else if(response.has("deliverylabel"))
                {
                    if (response.getString("deliverylabel").equals("BOOKING_CANCELLED")) {
                        Log.e("mLockatedPreferences.getIsAutoBooked  Before setIsBooked", String.valueOf(mLockatedPreferences.getIsAutoBooked()));

                        mProgressDialog.dismiss();
                        mLockatedPreferences.setTpdetail("");
                        mLockatedPreferences.setIsAutoBooked(false);

                        Log.e("mLockatedPreferences.getIsAutoBooked  After setIsBooked", String.valueOf(mLockatedPreferences.getIsAutoBooked()));
                        if (OlaMainActivity_Latest.mGoogleApiClient.isConnected()) {
                            Log.e("OlaMainActivity_Latest.mGoogleApiClient.isConnected","Yes");
                        }
                        else {
                            Log.e("OlaMainActivity_Latest.mGoogleApiClient.isConnected","No");
                            OlaMainActivity_Latest.mGoogleApiClient.connect();
                        }

                        if (OlaMainActivity_Latest.mGoogleApiClient != null) {
                            OlaMainActivity_Latest.mGoogleApiClient.connect();
                            Log.e("OlaMainActivity_Latest.mGoogleApiClient != null", "Yes");
                            Log.e("Made m GoogleApiCliet Connected","True");
                        }
                        else
                        {
                            Log.e("OlaMainActivity_Latest.mGoogleApiClient == null", "Yes");
                        }
                        OlaMainActivity_Latest.imageMarker.setVisibility(View.VISIBLE);
                        OlaMainActivity_Latest.gmap.getUiSettings().setMyLocationButtonEnabled(false);
                        OlaMainActivity_Latest.removeUserCurrentLocation();

                        //--- Cancel Alert----------------------------------------------

                        LayoutInflater layoutInflater = LayoutInflater.from(getActivity());
                        View promptView = layoutInflater.inflate(R.layout.cancel_cab_alert, null);

                        final AlertDialog alertD = new AlertDialog.Builder(getActivity()).create();

                        TextView cancel_response = (TextView) promptView.findViewById(R.id.txt_cancel_response);
                        cancel_response.setText(cancelResponse.getText());
                        TextView cancel_ok = (TextView) promptView.findViewById(R.id.btn_cancel_ok);

                        cancel_ok.setOnClickListener(new View.OnClickListener() {
                            public void onClick(View v) {

                                alertD.dismiss();
                                Intent intent = new Intent(getActivity(), OlaMainActivity_Latest.class);
                                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                startActivity(intent);
                            }
                        });
                        alertD.setView(promptView);
                        alertD.show();
                        //--------------------------------------
                    }
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    private void getReasonAlert(String cabType) {

        Type type = Type.valueOf(cabType); // surround with try/catch
        ArrayList<String> strings ;;
        switch(type) {
            case mini:
                strings = cancelReasons.getMini();
                Log.e("strings",""+strings);
                adapter = new OlaReasonAdapter(getActivity(),R.layout.layout_each_reason,cancelReasons.getMini());
                break;
            case micro:
                strings = cancelReasons.getMicro();
                Log.e("strings",""+strings);
                adapter = new OlaReasonAdapter(getActivity(), R.layout.layout_each_reason, cancelReasons.getMicro());
                break;
            case prime:
                strings = cancelReasons.getPrime();
                Log.e("strings",""+strings);
                adapter = new OlaReasonAdapter(getActivity(),R.layout.layout_each_reason,cancelReasons.getPrime());
                break;
            case sedan:
                strings = cancelReasons.getSedan();
                Log.e("strings",""+strings);
                adapter = new OlaReasonAdapter(getActivity(),R.layout.layout_each_reason,cancelReasons.getSedan());
                break;
            case auto:
                strings = cancelReasons.getAuto();
                Log.e("strings",""+strings);
                adapter = new OlaReasonAdapter(getActivity(),R.layout.layout_each_reason,cancelReasons.getAuto());
                break;
        }
        final AlertDialog.Builder dialog = new AlertDialog.Builder(getActivity());
        View view = getActivity().getLayoutInflater().inflate( R.layout.reason_layout, null );
        dialog.setTitle("Select Reason of Cancel");
        dialog.setView(view);
        final AlertDialog alertDialog = dialog.show();

        ListView listView = (ListView)view.findViewById(R.id.listView_reason);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                reason = parent.getItemAtPosition(position).toString();
                Log.e("Reason",""+reason);
                sendCabCancel();
                alertDialog.dismiss();
            }
        });
        listView.setAdapter(adapter);

    }

    @SuppressLint("LongLogTag")
    private void sendBookResponseToServer(JSONObject jsonObject)
    {
        Log.e(" Inside:",""+"sendBookResponseToServer");
        if (getActivity() != null) {
            if (ConnectionDetector.isConnectedToInternet(getActivity())) {
                String url = "https://www.lockated.com/add_ola_orders.json?token="+mLockatedPreferences.getLockatedToken();
                Log.e("url of sendBookResponseToServer",""+url);
                LockatedVolleyRequestQueue lockatedVolleyRequestQueue = LockatedVolleyRequestQueue.getInstance(getActivity());
                lockatedVolleyRequestQueue.sendPostRequestLocated(REQUEST_TAG, Request.Method.POST, url,jsonObject, this, this);
            } else {
                Utilities.showToastMessage(getActivity(), getActivity().getResources().getString(R.string.internet_connection_error));
            }
        }
    }

    @SuppressLint("LongLogTag")
    private void sendTrackResponseToServer(JSONObject jsonObject)
    {
        Log.e(" Inside:",""+"sendTrackResponseToServer");
        if (getActivity() != null) {
            if (ConnectionDetector.isConnectedToInternet(getActivity())) {
                String url = "http://sandbox-t.olacabs.com/v1/bookings/track_ride?booking_id="+BookingId;
                Log.e("url of sendTrackResponseToServer",""+url);
                LockatedVolleyRequestQueue lockatedVolleyRequestQueue = LockatedVolleyRequestQueue.getInstance(getActivity());
                lockatedVolleyRequestQueue.sendPostRequestOlaCabsConfirmEx(REQUEST_TAG, Request.Method.POST, url,AccessToken,jsonObject, this, this);
            } else {
                Utilities.showToastMessage(getActivity(), getActivity().getResources().getString(R.string.internet_connection_error));
            }
        }
    }

    private  enum Type
    {
        mini,micro,sedan,prime,auto
    }
}
