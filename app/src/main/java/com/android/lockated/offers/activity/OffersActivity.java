package com.android.lockated.offers.activity;


import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.lockated.HomeActivity;
import com.android.lockated.R;
import com.android.lockated.cart.ecommerce.EcommerceCartActivity;
import com.android.lockated.information.AccountController;
import com.android.lockated.landing.AccountFragment;
import com.android.lockated.landing.HomeFragment;
import com.android.lockated.landing.LocationFragment;
import com.android.lockated.landing.SupportFragment;
import com.android.lockated.model.AccountData;
import com.android.lockated.model.CartDetail;
import com.android.lockated.network.ConnectionDetector;
import com.android.lockated.offers.adapter.OffersAdapter;
import com.android.lockated.preferences.LockatedPreferences;
import com.android.lockated.request.LockatedRequestError;
import com.android.lockated.request.LockatedVolleyRequestQueue;
import com.android.lockated.utils.ApplicationURL;
import com.android.lockated.utils.Utilities;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class OffersActivity extends AppCompatActivity implements View.OnClickListener, Response.Listener<JSONObject>, Response.ErrorListener {

    private ImageView mImageViewHome;
    private ImageView mImageViewChat;
    private ImageView mImageViewAccount;
    private ImageView mImageViewLocation;
    private RecyclerView landingContainer;
    private TextView errorMsg;

    private LockatedPreferences mLockatedPreferences;
    private AccountController mAccountController;
    private String REQUEST_TAG = "OffersActivity";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_offers);

        mAccountController = AccountController.getInstance();
        mLockatedPreferences = new LockatedPreferences(this);
        actionBar("Notifications");
        init();
        //setBottomTabView();

    }

    public void actionBar(String name) {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(true);
        getSupportActionBar().setTitle(name);
    }

    public void init() {

        errorMsg = (TextView) findViewById(R.id.noNotices);
        landingContainer = (RecyclerView) findViewById(R.id.mLandingContainer);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        landingContainer.setLayoutManager(linearLayoutManager);
        landingContainer.setHasFixedSize(true);
        getOffers();
    }

    public void getOffers() {

        if (ConnectionDetector.isConnectedToInternet(this)) {
            String url = ApplicationURL.getOffersUrl + mLockatedPreferences.getLockatedToken();
            LockatedVolleyRequestQueue lockatedVolleyRequestQueue = LockatedVolleyRequestQueue.getInstance(this);
            lockatedVolleyRequestQueue.sendRequest(REQUEST_TAG, Request.Method.GET, url, null, this, this);
        } else {
            Utilities.showToastMessage(this, this.getResources().getString(R.string.internet_connection_error));
        }
    }

    /*private void setBottomTabView() {
        LinearLayout mLinearLayoutHome = (LinearLayout) findViewById(R.id.mLinearLayoutHome);
        LinearLayout mLinearLayoutSupport = (LinearLayout) findViewById(R.id.mLinearLayoutSupport);
        LinearLayout mLinearLayoutAccount = (LinearLayout) findViewById(R.id.mLinearLayoutAccount);
        LinearLayout mLinearLayoutLocation = (LinearLayout) findViewById(R.id.mLinearLayoutLocation);

        mImageViewHome = (ImageView) findViewById(R.id.mImageViewHome);
        mImageViewChat = (ImageView) findViewById(R.id.mImageViewChat);
        mImageViewAccount = (ImageView) findViewById(R.id.mImageViewAccount);
        mImageViewLocation = (ImageView) findViewById(R.id.mImageViewLocation);

        mLinearLayoutHome.setOnClickListener(this);
        mLinearLayoutSupport.setOnClickListener(this);
        mLinearLayoutAccount.setOnClickListener(this);
        mLinearLayoutLocation.setOnClickListener(this);

    }*/

    private void onHomeClicked() {
        HomeFragment homeFragment = new HomeFragment();
        replaceFragment(homeFragment);
        onHomeFragmentClicked();
    }

    private void onLocationClicked() {
        LocationFragment locationFragment = new LocationFragment();
        replaceFragment(locationFragment);
        onLocationFragmentClicked();
    }

    private void onAccountClicked() {
        AccountFragment accountFragment = new AccountFragment();
        replaceFragment(accountFragment);
        onAccountFragmentClicked();
    }

    private void onSupportClicked() {
        SupportFragment supportFragment = new SupportFragment();
        supportFragment.setChatId(157, getString(R.string.support_screen));
        replaceFragment(supportFragment);
        onChatFragmentClicked();
    }

    private void replaceFragment(Fragment fragment) {
        String backStateName = fragment.getClass().getName();

        FragmentManager manager = getSupportFragmentManager();
        boolean fragmentPopped = manager.popBackStackImmediate(backStateName, 0);

        if (!fragmentPopped && manager.findFragmentByTag(backStateName) == null) {
            FragmentTransaction ft = manager.beginTransaction();
            ft.replace(R.id.mLandingContainer, fragment, backStateName);
            ft.commit();
        }
    }

    private void onHomeFragmentClicked() {
        mImageViewHome.setImageResource(R.drawable.ic_home_active);
        mImageViewChat.setImageResource(R.drawable.ic_chat);
        mImageViewAccount.setImageResource(R.drawable.ic_account);
        mImageViewLocation.setImageResource(R.drawable.ic_location);
    }

    private void onChatFragmentClicked() {
        mImageViewHome.setImageResource(R.drawable.ic_home);
        mImageViewChat.setImageResource(R.drawable.ic_chat_active);
        mImageViewAccount.setImageResource(R.drawable.ic_account);
        mImageViewLocation.setImageResource(R.drawable.ic_location);
    }

    private void onAccountFragmentClicked() {
        mImageViewHome.setImageResource(R.drawable.ic_home);
        mImageViewChat.setImageResource(R.drawable.ic_chat);
        mImageViewAccount.setImageResource(R.drawable.ic_account_active);
        mImageViewLocation.setImageResource(R.drawable.ic_location);
    }

    private void onLocationFragmentClicked() {
        mImageViewHome.setImageResource(R.drawable.ic_home);
        mImageViewChat.setImageResource(R.drawable.ic_chat);
        mImageViewAccount.setImageResource(R.drawable.ic_account);
        mImageViewLocation.setImageResource(R.drawable.ic_location_active);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.mLinearLayoutHome:
                onHomeClicked();
                break;
            case R.id.mLinearLayoutSupport:
                onSupportClicked();
                break;
            case R.id.mLinearLayoutAccount:
                onAccountClicked();
                break;
            case R.id.mLinearLayoutLocation:
                onLocationClicked();
                break;
        }
    }

    @Override
    public void onErrorResponse(VolleyError error) {
        LockatedRequestError.onRequestError(this, error);
    }

    @Override
    public void onResponse(JSONObject response) {
        try {
            if (response.has("code") && response.getInt("code") == 200) {
                /*if (response.has("offers")) {
                    OffersAdapter offersAdapter = new OffersAdapter(this, response.getJSONArray("offers"));
                    landingContainer.setAdapter(offersAdapter);
                }*/
            } else {
                errorMsg.setVisibility(View.VISIBLE);
                errorMsg.setText("No offers available currently");
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.home, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_cart:
                onCartClicked();
                return false;
            case android.R.id.home:
                Intent intent = new Intent(this, HomeActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                finish();
                //super.onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void onCartClicked() {
        ArrayList<AccountData> accountDatas = AccountController.getInstance().getmAccountDataList();
        if (accountDatas.size() > 0) {
            ArrayList<CartDetail> cartDetails = accountDatas.get(0).getCartDetailList();
            if (cartDetails.size() > 0) {
                if (cartDetails.get(0).getmLineItemData().size() > 0) {
                    Intent intent = new Intent(this, EcommerceCartActivity.class);
                    startActivity(intent);
                } else {
                    Utilities.showToastMessage(this, getString(R.string.empty_cart));
                }
            } else {
                Utilities.showToastMessage(this, getString(R.string.empty_cart));
            }
        }
    }

    @Override
    public void onBackPressed() {
        //super.onBackPressed();
        Intent intent = new Intent(this, HomeActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
        finish();
    }

}