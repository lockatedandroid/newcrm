package com.android.lockated.account.fragment;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v4.content.ContextCompat;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import com.android.lockated.Interfaces.INewAddressAdded;
import com.android.lockated.R;
import com.android.lockated.model.MyAddressData;
import com.android.lockated.network.ConnectionDetector;
import com.android.lockated.preferences.LockatedPreferences;
import com.android.lockated.request.LockatedRequestError;
import com.android.lockated.request.LockatedVolleyRequestQueue;
import com.android.lockated.utils.ApplicationURL;
import com.android.lockated.utils.Utilities;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class AddNewAddressFragment extends DialogFragment implements View.OnClickListener, AdapterView.OnItemSelectedListener, Response.Listener, Response.ErrorListener
{
    private View mAddNewAddressView;

    private TextView mTextViewAddressFirstName;
    private TextView mTextViewAddressLastName;
    private TextView mTextViewAddressLineOne;
    private TextView mTextViewAddressLineTwo;
    private TextView mTextViewAddressCity;
    private TextView mTextViewAddressMobileNumber;
    private TextView mTextViewAddressZipCode;
    private TextView mButtonAddAddressCancel;
    private TextView mButtonAddAddress;

    private Spinner mSpinnerState;

    private EditText mEditTextAddressFirstName;
    private EditText mEditTextAddressLastName;
    private EditText mEditTextAddressLineOne;
    private EditText mEditTextAddressLineTwo;
    private EditText mEditTextAddressCity;
    private EditText mEditTextAddressMobileNumber;
    private EditText mEditTextAddressZipCode;

    private int mStateId;

    private RequestQueue mQueue;
    private ProgressDialog mProgressDialog;
    private LockatedPreferences mLockatedPreferences;

    private INewAddressAdded mINewAddressAdded;
    private MyAddressData mMyAddressDataRow;
    private ArrayList<MyAddressData> mMyAddressDatas;

    private int requestId;

    public static final String REQUEST_TAG = "AddNewAddressFragment";

    public AddNewAddressFragment()
    {

    }

    public void setAddressDataOnEdit(int id, MyAddressData addressData)
    {
        requestId = id;
        mMyAddressDataRow = addressData;
    }

    public void setListener(INewAddressAdded newAddressAdded)
    {
        mINewAddressAdded = newAddressAdded;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setStyle(STYLE_NO_TITLE, 0);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        mAddNewAddressView = inflater.inflate(R.layout.fragment_add_new_address, container);
        init();
        return mAddNewAddressView;
    }

    @Override
    public void onStart()
    {
        super.onStart();
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(getDialog().getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        getDialog().getWindow().setAttributes(lp);
    }

    private void init()
    {
        mMyAddressDatas = new ArrayList<>();
        mLockatedPreferences = new LockatedPreferences(getActivity());

        mTextViewAddressFirstName = (TextView) mAddNewAddressView.findViewById(R.id.mTextViewAddressFirstName);
        mTextViewAddressLastName = (TextView) mAddNewAddressView.findViewById(R.id.mTextViewAddressLastName);
        mTextViewAddressLineOne = (TextView) mAddNewAddressView.findViewById(R.id.mTextViewAddressLineOne);
        mTextViewAddressLineTwo = (TextView) mAddNewAddressView.findViewById(R.id.mTextViewAddressLineTwo);
        mTextViewAddressCity = (TextView) mAddNewAddressView.findViewById(R.id.mTextViewAddressCity);
        mTextViewAddressMobileNumber = (TextView) mAddNewAddressView.findViewById(R.id.mTextViewAddressMobileNumber);
        mTextViewAddressZipCode = (TextView) mAddNewAddressView.findViewById(R.id.mTextViewAddressZipCode);
        mButtonAddAddressCancel = (TextView) mAddNewAddressView.findViewById(R.id.mButtonAddAddressCancel);
        mButtonAddAddress = (TextView) mAddNewAddressView.findViewById(R.id.mButtonAddAddress);

        mSpinnerState = (Spinner) mAddNewAddressView.findViewById(R.id.mSpinnerState);

        mEditTextAddressFirstName = (EditText) mAddNewAddressView.findViewById(R.id.mEditTextAddressFirstName);
        mEditTextAddressLastName = (EditText) mAddNewAddressView.findViewById(R.id.mEditTextAddressLastName);
        mEditTextAddressLineOne = (EditText) mAddNewAddressView.findViewById(R.id.mEditTextAddressLineOne);
        mEditTextAddressLineTwo = (EditText) mAddNewAddressView.findViewById(R.id.mEditTextAddressLineTwo);
        mEditTextAddressCity = (EditText) mAddNewAddressView.findViewById(R.id.mEditTextAddressCity);
        mEditTextAddressMobileNumber = (EditText) mAddNewAddressView.findViewById(R.id.mEditTextAddressMobileNumber);
        mEditTextAddressZipCode = (EditText) mAddNewAddressView.findViewById(R.id.mEditTextAddressZipCode);

        if (requestId == 0)
        {
            mButtonAddAddress.setText(R.string.add);
        }
        else
        {
            mButtonAddAddress.setText(R.string.update);
        }

        mSpinnerState.setOnItemSelectedListener(this);
        mButtonAddAddress.setOnClickListener(this);
        mButtonAddAddressCancel.setOnClickListener(this);

        hideFirstName();
        hideLastName();
        hideAddressLineOne();
        hideAddressLineTwo();
        hideAddressCity();
        hideAddressMobileNumber();
        hideAddressZipCode();
        setEditTextListener();
        disableAddButton();
        setEditedData();
    }

    private void setEditedData()
    {
        if (requestId == 1)
        {
            mSpinnerState.setSelection(mMyAddressDataRow.getStateId()-1);
            mEditTextAddressFirstName.setText(mMyAddressDataRow.getFirstName());
            mEditTextAddressLastName.setText(mMyAddressDataRow.getLastName());
            mEditTextAddressLineOne.setText(mMyAddressDataRow.getAddress1());
            mEditTextAddressLineTwo.setText(mMyAddressDataRow.getAddress2());
            mEditTextAddressCity.setText(mMyAddressDataRow.getCity());
            mEditTextAddressMobileNumber.setText(mMyAddressDataRow.getPhoneNumber());
            mEditTextAddressZipCode.setText(mMyAddressDataRow.getZipcode());
        }
    }

    private void disableAddButton()
    {
        mButtonAddAddress.setEnabled(false);
        mButtonAddAddress.setTextColor(ContextCompat.getColor(getActivity(), R.color.secondary_text));
    }

    private void enableAddButton()
    {
        mButtonAddAddress.setEnabled(true);
        mButtonAddAddress.setTextColor(ContextCompat.getColor(getActivity(), R.color.primary));
    }

    private void hideFirstName()
    {
        mTextViewAddressFirstName.setVisibility(View.INVISIBLE);
    }

    private void hideLastName()
    {
        mTextViewAddressLastName.setVisibility(View.INVISIBLE);
    }

    private void hideAddressLineOne()
    {
        mTextViewAddressLineOne.setVisibility(View.INVISIBLE);
    }

    private void hideAddressLineTwo()
    {
        mTextViewAddressLineTwo.setVisibility(View.INVISIBLE);
    }

    private void hideAddressCity()
    {
        mTextViewAddressCity.setVisibility(View.INVISIBLE);
    }

    private void hideAddressMobileNumber()
    {
        mTextViewAddressMobileNumber.setVisibility(View.INVISIBLE);
    }

    private void hideAddressZipCode()
    {
        mTextViewAddressZipCode.setVisibility(View.INVISIBLE);
    }

    private void showFirstName()
    {
        mTextViewAddressFirstName.setVisibility(View.VISIBLE);
    }

    private void showLastName()
    {
        mTextViewAddressLastName.setVisibility(View.VISIBLE);
    }

    private void showAddressLineOne()
    {
        mTextViewAddressLineOne.setVisibility(View.VISIBLE);
    }

    private void showAddressLineTwo()
    {
        mTextViewAddressLineTwo.setVisibility(View.VISIBLE);
    }

    private void showAddressCity()
    {
        mTextViewAddressCity.setVisibility(View.VISIBLE);
    }

    private void showAddressMobileNumber()
    {
        mTextViewAddressMobileNumber.setVisibility(View.VISIBLE);
    }

    private void showAddressZipCode()
    {
        mTextViewAddressZipCode.setVisibility(View.VISIBLE);
    }

    private void setEditTextListener()
    {
        mEditTextAddressFirstName.addTextChangedListener(new TextWatcher()
        {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after)
            {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count)
            {
                if (s.length() > 0)
                {
                    showFirstName();

                    if (mEditTextAddressLastName.getText().toString().length() > 0 && mEditTextAddressLineOne.getText().toString().length() > 0 && mEditTextAddressLineTwo.getText().toString().length() > 0 && mEditTextAddressCity.getText().toString().length() > 0 && mEditTextAddressMobileNumber.getText().toString().length() > 0 && mEditTextAddressZipCode.getText().toString().length() > 0)
                    {
                        enableAddButton();
                    }
                } else
                {
                    disableAddButton();
                    hideFirstName();
                }
            }

            @Override
            public void afterTextChanged(Editable s)
            {

            }
        });

        mEditTextAddressLastName.addTextChangedListener(new TextWatcher()
        {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after)
            {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count)
            {
                if (s.length() > 0)
                {
                    showLastName();

                    if (mEditTextAddressFirstName.getText().toString().length() > 0 && mEditTextAddressLineOne.getText().toString().length() > 0 && mEditTextAddressLineTwo.getText().toString().length() > 0 && mEditTextAddressCity.getText().toString().length() > 0 && mEditTextAddressMobileNumber.getText().toString().length() > 0 && mEditTextAddressZipCode.getText().toString().length() > 0)
                    {
                        enableAddButton();
                    }
                } else
                {
                    disableAddButton();
                    hideLastName();
                }
            }

            @Override
            public void afterTextChanged(Editable s)
            {

            }
        });

        mEditTextAddressLineOne.addTextChangedListener(new TextWatcher()
        {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after)
            {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count)
            {
                if (s.length() > 0)
                {
                    showAddressLineOne();

                    if (mEditTextAddressFirstName.getText().toString().length() > 0 && mEditTextAddressLastName.getText().toString().length() > 0 && mEditTextAddressLineTwo.getText().toString().length() > 0 && mEditTextAddressCity.getText().toString().length() > 0 && mEditTextAddressMobileNumber.getText().toString().length() > 0 && mEditTextAddressZipCode.getText().toString().length() > 0)
                    {
                        enableAddButton();
                    }
                } else
                {
                    disableAddButton();
                    hideAddressLineOne();
                }
            }

            @Override
            public void afterTextChanged(Editable s)
            {

            }
        });

        mEditTextAddressLineTwo.addTextChangedListener(new TextWatcher()
        {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after)
            {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count)
            {
                if (s.length() > 0)
                {
                    showAddressLineTwo();

                    if (mEditTextAddressFirstName.getText().toString().length() > 0 && mEditTextAddressLastName.getText().toString().length() > 0 && mEditTextAddressLineOne.getText().toString().length() > 0 && mEditTextAddressCity.getText().toString().length() > 0 && mEditTextAddressMobileNumber.getText().toString().length() > 0 && mEditTextAddressZipCode.getText().toString().length() > 0)
                    {
                        enableAddButton();
                    }
                } else
                {
                    disableAddButton();
                    hideAddressLineTwo();
                }
            }

            @Override
            public void afterTextChanged(Editable s)
            {

            }
        });

        mEditTextAddressCity.addTextChangedListener(new TextWatcher()
        {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after)
            {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count)
            {
                if (s.length() > 0)
                {
                    showAddressCity();

                    if (mEditTextAddressFirstName.getText().toString().length() > 0 && mEditTextAddressLastName.getText().toString().length() > 0 && mEditTextAddressLineOne.getText().toString().length() > 0 && mEditTextAddressLineTwo.getText().toString().length() > 0 && mEditTextAddressMobileNumber.getText().toString().length() > 0 && mEditTextAddressZipCode.getText().toString().length() > 0)
                    {
                        enableAddButton();
                    }
                } else
                {
                    disableAddButton();
                    hideAddressCity();
                }
            }

            @Override
            public void afterTextChanged(Editable s)
            {

            }
        });

        mEditTextAddressMobileNumber.addTextChangedListener(new TextWatcher()
        {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after)
            {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count)
            {
                if (s.length() > 0)
                {
                    showAddressMobileNumber();

                    if (mEditTextAddressFirstName.getText().toString().length() > 0 && mEditTextAddressLastName.getText().toString().length() > 0 && mEditTextAddressLineOne.getText().toString().length() > 0 && mEditTextAddressLineTwo.getText().toString().length() > 0 && mEditTextAddressCity.getText().toString().length() > 0 && mEditTextAddressZipCode.getText().toString().length() > 0)
                    {
                        enableAddButton();
                    }
                } else
                {
                    disableAddButton();
                    hideAddressMobileNumber();
                }
            }

            @Override
            public void afterTextChanged(Editable s)
            {

            }
        });

        mEditTextAddressZipCode.addTextChangedListener(new TextWatcher()
        {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after)
            {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count)
            {
                if (s.length() > 0)
                {
                    showAddressZipCode();

                    if (mEditTextAddressFirstName.getText().toString().length() > 0 && mEditTextAddressLastName.getText().toString().length() > 0 && mEditTextAddressLineOne.getText().toString().length() > 0 && mEditTextAddressLineTwo.getText().toString().length() > 0 && mEditTextAddressCity.getText().toString().length() > 0 && mEditTextAddressMobileNumber.getText().toString().length() > 0)
                    {
                        enableAddButton();
                    }
                } else
                {
                    disableAddButton();
                    hideAddressZipCode();
                }
            }

            @Override
            public void afterTextChanged(Editable s)
            {

            }
        });
    }

    private void onAddAddressButtonCicked()
    {
        String strAddressFirstName = mEditTextAddressFirstName.getText().toString();
        String strAddressLastName = mEditTextAddressLastName.getText().toString();
        String strAddressLineOne = mEditTextAddressLineOne.getText().toString();
        String strAddressLineTwo = mEditTextAddressLineTwo.getText().toString();
        String strAddressCity = mEditTextAddressCity.getText().toString();
        String strAddressMobileNumber = mEditTextAddressMobileNumber.getText().toString();
        String strAddressZipCode = mEditTextAddressZipCode.getText().toString();

        boolean addAddressCancel = false;
        View addAddressFocusView = null;
        String message = getActivity().getResources().getString(R.string.signup_blank_field_error);

        if (TextUtils.isEmpty(strAddressFirstName))
        {
            addAddressFocusView = mEditTextAddressFirstName;
            addAddressCancel = true;
            message = getActivity().getResources().getString(R.string.signup_blank_field_error);
        }

        if (TextUtils.isEmpty(strAddressLastName))
        {
            addAddressFocusView = mEditTextAddressLastName;
            addAddressCancel = true;
            message = getActivity().getResources().getString(R.string.signup_blank_field_error);
        }

        if (TextUtils.isEmpty(strAddressLineOne))
        {
            addAddressFocusView = mEditTextAddressLineOne;
            addAddressCancel = true;
            message = getActivity().getResources().getString(R.string.signup_blank_field_error);
        }

        if (TextUtils.isEmpty(strAddressLineTwo))
        {
            addAddressFocusView = mEditTextAddressLineTwo;
            addAddressCancel = true;
            message = getActivity().getResources().getString(R.string.signup_blank_field_error);
        }

        if (TextUtils.isEmpty(strAddressCity))
        {
            addAddressFocusView = mEditTextAddressCity;
            addAddressCancel = true;
            message = getActivity().getResources().getString(R.string.signup_blank_field_error);
        }

        // Validation for Number field, it should not be blank
        if (TextUtils.isEmpty(strAddressMobileNumber))
        {
            addAddressFocusView = mEditTextAddressMobileNumber;
            addAddressCancel = true;
            message = getActivity().getResources().getString(R.string.signup_blank_field_error);
        }

        if (strAddressMobileNumber.length() != 10)
        {
            addAddressFocusView = mEditTextAddressMobileNumber;
            addAddressCancel = true;
            message = getActivity().getResources().getString(R.string.mobile_number_error);
        }

        if (TextUtils.isEmpty(strAddressZipCode))
        {
            addAddressFocusView = mEditTextAddressMobileNumber;
            addAddressCancel = true;
            message = getActivity().getResources().getString(R.string.signup_blank_field_error);
        }

        if (addAddressCancel)
        {
            addAddressFocusView.requestFocus();
            Utilities.showToastMessage(getActivity(), message);
        } else
        {
            if (ConnectionDetector.isConnectedToInternet(getActivity()))
            {
                mProgressDialog = ProgressDialog.show(getActivity(), "", "Please Wait...", false);
                mProgressDialog.show();

                JSONObject jsonObject = new JSONObject();

                try
                {
                    if (requestId == 1)
                    {
                        jsonObject.put("id", mMyAddressDataRow.getAddressId());
                    }
                    jsonObject.put("firstname", strAddressFirstName);
                    jsonObject.put("lastname", strAddressLastName);
                    jsonObject.put("address1", strAddressLineOne);
                    jsonObject.put("address2", strAddressLineTwo);
                    jsonObject.put("city", strAddressCity);
                    jsonObject.put("phone", strAddressMobileNumber);
                    jsonObject.put("zipcode", strAddressZipCode);
                    jsonObject.put("state_id", mStateId);
                    jsonObject.put("country_id", 1);
                }
                catch (JSONException e)
                {
                    e.printStackTrace();
                }

//                int socketTimeout = Utilities.REQUEST_TIME_OUT;
//                mQueue = LockatedVolleyRequestQueue.getInstance(getActivity()).getRequestQueue();
//                LockatedJSONObjectRequest lockatedJSONObjectRequest;
//                if (requestId == 1)
//                {
//                    lockatedJSONObjectRequest = new LockatedJSONObjectRequest(Request.Method.PUT, ApplicationURL.updateAddressUrl + "?token=" + mLockatedPreferences.getLockatedToken(), jsonObject, this, this);
//                }
//                else
//                {
//                    lockatedJSONObjectRequest = new LockatedJSONObjectRequest(Request.Method.POST, ApplicationURL.createAddressUrl + "?token=" + mLockatedPreferences.getLockatedToken(), jsonObject, this, this);
//                }
//                lockatedJSONObjectRequest.setTag(REQUEST_TAG);
//                RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
//                lockatedJSONObjectRequest.setRetryPolicy(policy);
//                mQueue.add(lockatedJSONObjectRequest);

                LockatedVolleyRequestQueue lockatedVolleyRequestQueue = LockatedVolleyRequestQueue.getInstance(getActivity());
                if (requestId == 1)
                {
                    lockatedVolleyRequestQueue.sendRequest(REQUEST_TAG, Request.Method.PUT, ApplicationURL.updateAddressUrl + "?token=" + mLockatedPreferences.getLockatedToken(), jsonObject, this, this);
                }
                else
                {
                    lockatedVolleyRequestQueue.sendRequest(REQUEST_TAG, Request.Method.POST, ApplicationURL.createAddressUrl + "?token=" + mLockatedPreferences.getLockatedToken(), jsonObject, this, this);
                }
            } else
            {
                Utilities.showToastMessage(getActivity(), getActivity().getResources().getString(R.string.internet_connection_error));
            }
        }
    }

    @Override
    public void onClick(View v)
    {
        switch (v.getId())
        {
            case R.id.mButtonAddAddress:
                if (getDialog() != null)
                {
                    onAddAddressButtonCicked();
                }
                break;
            case R.id.mButtonAddAddressCancel:
                if (getDialog() != null)
                {
                    getDialog().dismiss();
                }
                break;
        }
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id)
    {
        mStateId = position+1;
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent)
    {

    }

    @Override
    public void onErrorResponse(VolleyError error)
    {
        mProgressDialog.dismiss();
        if (getActivity() != null)
        {
            LockatedRequestError.onRequestError(getActivity(), error);
        }
    }

    @Override
    public void onResponse(Object response)
    {
        mProgressDialog.dismiss();
        if (getActivity() != null)
        {
            if (getDialog() != null)
            {
                JSONObject jsonObject = (JSONObject) response;
                try
                {
                    JSONArray jsonArray = jsonObject.getJSONArray("addresses");
                    for (int i = 0; i < jsonArray.length(); i++)
                    {
                        JSONObject addressObject = jsonArray.getJSONObject(i);
                        MyAddressData myAddressData = new MyAddressData(addressObject);
                        mMyAddressDatas.add(myAddressData);
                    }

                    mINewAddressAdded.onNewAddressAdded(mMyAddressDatas);
                }
                catch (JSONException e)
                {
                    e.printStackTrace();
                }
                getDialog().dismiss();
            }
        }
    }
}
