package com.android.lockated.crm.fragment;

import android.app.Dialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.android.lockated.R;
import com.android.lockated.utils.Utilities;
import com.android.volley.RequestQueue;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;


public class DetailsFullView extends DialogFragment implements View.OnClickListener, RadioGroup.OnCheckedChangeListener {

    //private TextView mCreatedAtDate;
    //private TextView mEventAt;
    private TextView mDescription;
    private RequestQueue mQueue;
    private RadioButton mAttendingRadioButton;
    private RadioButton mNotAttendingRadioButton;
    private TextView mClose;
    private TextView mEventType;
    private TextView mSubject;
    private TextView mCreatedBY;
    private TextView mStartDateTime;
    private TextView mEndDateTime;
    Window window;
    private View DetailsFullViewView;

    @Override
    public void onStart() {
        super.onStart();

        window = getDialog().getWindow();
        WindowManager.LayoutParams windowParams = window.getAttributes();
        windowParams.dimAmount = 0.75f;
        windowParams.flags |= WindowManager.LayoutParams.FLAG_DIM_BEHIND;
        window.setAttributes(windowParams);
        window.setBackgroundDrawableResource(android.R.color.transparent);

    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        DetailsFullViewView = getActivity().getLayoutInflater().inflate(R.layout.fragment_details_full_view,
                new LinearLayout(getActivity()), false);

        init(DetailsFullViewView);
        Dialog builder = new Dialog(getActivity());
        builder.requestWindowFeature(Window.FEATURE_NO_TITLE);
        builder.getWindow().setBackgroundDrawable(new ColorDrawable(Color.WHITE));

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(builder.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.WRAP_CONTENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        builder.show();
        builder.getWindow().setAttributes(lp);
        builder.setContentView(DetailsFullViewView);

        return builder;

    }

    private void init(View view) {

        mEventType = (TextView) view.findViewById(R.id.mEventType);
        mSubject = (TextView) view.findViewById(R.id.mSubject);
        mCreatedBY = (TextView) view.findViewById(R.id.mCreatedBY);
        mStartDateTime = (TextView) view.findViewById(R.id.mStartDateTime);
        mEndDateTime = (TextView) view.findViewById(R.id.mEndDateTime);
        mDescription = (TextView) view.findViewById(R.id.mDescription);

        mClose = (TextView) view.findViewById(R.id.mClose);
        mClose.setOnClickListener(this);
        mAttendingRadioButton = (RadioButton) view.findViewById(R.id.mAttendingRadioButton);
        mAttendingRadioButton.setOnClickListener(this);
        mNotAttendingRadioButton = (RadioButton) view.findViewById(R.id.mNotAttendingRadioButton);
        mNotAttendingRadioButton.setOnClickListener(this);

        if (getArguments() != null) {
            mEventType.setText(getArguments().getString("event_type"));
            mCreatedBY.setText(getArguments().getString("created_by"));
            mSubject.setText(getArguments().getString("subject"));
            mStartDateTime.setText(dateTrim(getArguments().getString("from_time")));
            mEndDateTime.setText(dateFormat(getArguments().getString("created_at")));
            mDescription.setText(getArguments().getString("description"));
        }

    }

    public String dateTrim(String dateString) {

        String trimDate = "No data";

        try {
            trimDate = dateString.substring(0, dateString.indexOf("T"));
            DateFormat format = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);
            Date date = format.parse(trimDate);
            trimDate = new SimpleDateFormat("dd-MM-yyyy").format(date);
        } catch (Exception e) {
            Utilities.showToastMessage(getActivity(), "Something went wrong");
        }
        return trimDate;

    }

    public String dateFormat(String dateString) {

        String format = "No Data";

        try {
            String trimDate = dateString.substring(0, dateString.indexOf("T"));
            String str[] = trimDate.split("-");
            int selectedYear = Integer.parseInt(str[0]);
            int selectedMonth = Integer.parseInt(str[1]);
            int selectedDay = Integer.parseInt(str[2]);
            Calendar cal = Calendar.getInstance();
            cal.set(Calendar.YEAR, selectedYear);
            cal.set(Calendar.DAY_OF_MONTH, selectedDay);
            cal.set(Calendar.MONTH, selectedMonth - 1);
            format = new SimpleDateFormat("MMM d, EEEE").format(cal.getTime());
        } catch (Exception e) {
            Utilities.showToastMessage(getActivity(), "Something went wrong");
        }

        return format;

    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {

            case R.id.mAttendingRadioButton:

                mNotAttendingRadioButton.setChecked(false);
                mAttendingRadioButton.setChecked(true);

                break;

            case R.id.mNotAttendingRadioButton:

                mNotAttendingRadioButton.setChecked(true);
                mAttendingRadioButton.setChecked(false);

                break;

            case R.id.mClose:

                if (mAttendingRadioButton.isChecked()) {
                } else if (mNotAttendingRadioButton.isChecked()) {
                } else {
                }
                dismiss();
                break;

        }
    }

    @Override
    public void onCheckedChanged(RadioGroup group, int checkedId) {
    /*if(checkedId==R.id.mAttendingRadioButton)
      {}else if(checkedId==R.id.mNotAttendingRadioButton)
    {}*/
    }

}
