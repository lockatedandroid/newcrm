
package com.android.lockated.model.OlaCab.OlaAutoTrackResponse;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class OlaAutoTrackReponse {

    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("request_type")
    @Expose
    private String requestType;
    @SerializedName("booking_id")
    @Expose
    private String bookingId;
    @SerializedName("driver_name")
    @Expose
    private String driverName;
    @SerializedName("driver_number")
    @Expose
    private String driverNumber;
    @SerializedName("vehicle_type")
    @Expose
    private String vehicleType;
    @SerializedName("vehicle_number")
    @Expose
    private String vehicleNumber;
    @SerializedName("driver_lat")
    @Expose
    private float driverLat;
    @SerializedName("driver_lng")
    @Expose
    private float driverLng;
    @SerializedName("duration")
    @Expose
    private Duration duration;
    @SerializedName("distance")
    @Expose
    private Distance distance;
    @SerializedName("booking_status")
    @Expose
    private String bookingStatus;

    /**
     * 
     * @return
     *     The status
     */
    public String getStatus() {
        return status;
    }

    /**
     * 
     * @param status
     *     The status
     */
    public void setStatus(String status) {
        this.status = status;
    }

    /**
     * 
     * @return
     *     The requestType
     */
    public String getRequestType() {
        return requestType;
    }

    /**
     * 
     * @param requestType
     *     The request_type
     */
    public void setRequestType(String requestType) {
        this.requestType = requestType;
    }

    /**
     * 
     * @return
     *     The bookingId
     */
    public String getBookingId() {
        return bookingId;
    }

    /**
     * 
     * @param bookingId
     *     The booking_id
     */
    public void setBookingId(String bookingId) {
        this.bookingId = bookingId;
    }

    /**
     * 
     * @return
     *     The driverName
     */
    public String getDriverName() {
        return driverName;
    }

    /**
     * 
     * @param driverName
     *     The driver_name
     */
    public void setDriverName(String driverName) {
        this.driverName = driverName;
    }

    /**
     * 
     * @return
     *     The driverNumber
     */
    public String getDriverNumber() {
        return driverNumber;
    }

    /**
     * 
     * @param driverNumber
     *     The driver_number
     */
    public void setDriverNumber(String driverNumber) {
        this.driverNumber = driverNumber;
    }

    /**
     * 
     * @return
     *     The vehicleType
     */
    public String getVehicleType() {
        return vehicleType;
    }

    /**
     * 
     * @param vehicleType
     *     The vehicle_type
     */
    public void setVehicleType(String vehicleType) {
        this.vehicleType = vehicleType;
    }

    /**
     * 
     * @return
     *     The vehicleNumber
     */
    public String getVehicleNumber() {
        return vehicleNumber;
    }

    /**
     * 
     * @param vehicleNumber
     *     The vehicle_number
     */
    public void setVehicleNumber(String vehicleNumber) {
        this.vehicleNumber = vehicleNumber;
    }

    /**
     * 
     * @return
     *     The driverLat
     */
    public float getDriverLat() {
        return driverLat;
    }

    /**
     * 
     * @param driverLat
     *     The driver_lat
     */
    public void setDriverLat(float driverLat) {
        this.driverLat = driverLat;
    }

    /**
     * 
     * @return
     *     The driverLng
     */
    public float getDriverLng() {
        return driverLng;
    }

    /**
     * 
     * @param driverLng
     *     The driver_lng
     */
    public void setDriverLng(float driverLng) {
        this.driverLng = driverLng;
    }

    /**
     * 
     * @return
     *     The duration
     */
    public Duration getDuration() {
        return duration;
    }

    /**
     * 
     * @param duration
     *     The duration
     */
    public void setDuration(Duration duration) {
        this.duration = duration;
    }

    /**
     * 
     * @return
     *     The distance
     */
    public Distance getDistance() {
        return distance;
    }

    /**
     * 
     * @param distance
     *     The distance
     */
    public void setDistance(Distance distance) {
        this.distance = distance;
    }

    /**
     * 
     * @return
     *     The bookingStatus
     */
    public String getBookingStatus() {
        return bookingStatus;
    }

    /**
     * 
     * @param bookingStatus
     *     The booking_status
     */
    public void setBookingStatus(String bookingStatus) {
        this.bookingStatus = bookingStatus;
    }

}
