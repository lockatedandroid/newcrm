package com.android.lockated.crm.fragment.events;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Spinner;

import com.android.lockated.R;
import com.android.lockated.account.SpinnerAdapter;
import com.android.lockated.crm.activity.AllEventsActivity;
import com.android.lockated.crm.adapters.CommanListViewAdapter;
import com.android.lockated.utils.Utilities;


public class UserEventsFragment extends Fragment implements AdapterView.OnItemClickListener {
    ListView listNotice;
    String[] nameList = {"All Events", "My Events"};
    CommanListViewAdapter commanListViewAdapter;
    Spinner spinner;
    String[] textArray = { "clouds", "mark", "techcrunch", "times" };
    Integer[] imageArray = { R.drawable.attachment,
            R.drawable.add,
            R.drawable.call,
            R.drawable.cell };
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View allNoticeView = inflater.inflate(R.layout.comman_listview, container, false);
        init(allNoticeView);
        Utilities.ladooIntegration(getActivity(), "Events List");
        return allNoticeView;

    }

    private void init(View allNoticeView) {
       // spinner=(Spinner)allNoticeView.findViewById(R.id.spinner);
       // SpinnerAdapter adapter = new SpinnerAdapter(getActivity(), R.layout.image_text_spinner, textArray, imageArray);
        //spinner.setAdapter(adapter);
        listNotice = (ListView) allNoticeView.findViewById(R.id.noticeList);
        commanListViewAdapter = new CommanListViewAdapter(getActivity(), nameList);
        listNotice.setAdapter(commanListViewAdapter);
        listNotice.setOnItemClickListener(this);
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        Intent intent = new Intent(getActivity(), AllEventsActivity.class);
        intent.putExtra("AllEventsActivity", position);
        startActivity(intent);
    }

}
