package com.android.lockated.account.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.android.lockated.Interfaces.IRecyclerItemClickListener;
import com.android.lockated.R;
import com.android.lockated.holder.RecyclerViewHolder;
import com.android.lockated.model.userSocietyFlat.UserFlat;

import java.util.ArrayList;

public class MySocietyListAdapter extends RecyclerView.Adapter<RecyclerViewHolder> {

    Context context;
    ArrayList<UserFlat> userSocietyArrayList;
    private IRecyclerItemClickListener mIRecyclerItemClickListener;

    public MySocietyListAdapter(Context context, ArrayList<UserFlat> userSocietyArrayList, IRecyclerItemClickListener listener) {
        this.context = context;
        this.userSocietyArrayList = userSocietyArrayList;
        mIRecyclerItemClickListener = listener;
    }

    @Override
    public RecyclerViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.fragment_society_list_row, parent, false);
        return new RecyclerViewHolder(itemView, mIRecyclerItemClickListener);
    }

    @Override
    public void onBindViewHolder(RecyclerViewHolder holder, int position) {
        holder.onMySocietyListViewHolder(userSocietyArrayList.get(position));
    }

    @Override
    public int getItemCount() {
        return userSocietyArrayList.size();
    }

}