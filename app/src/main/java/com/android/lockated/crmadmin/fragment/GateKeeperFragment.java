package com.android.lockated.crmadmin.fragment;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.lockated.LockatedApplication;
import com.android.lockated.R;
import com.android.lockated.crmadmin.activity.GateKeeperActivity;
import com.android.lockated.crmadmin.fragment.gatekeeper.GatekeeperEntryFragment;
import com.android.lockated.model.AdminModel.AdminGatekeeper.GatekeeperAdminModel;
import com.android.lockated.network.ConnectionDetector;
import com.android.lockated.preferences.LockatedPreferences;
import com.android.lockated.request.LockatedJSONObjectRequest;
import com.android.lockated.request.LockatedRequestError;
import com.android.lockated.request.LockatedVolleyRequestQueue;
import com.android.lockated.utils.ApplicationURL;
import com.android.lockated.utils.Utilities;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;


public class GateKeeperFragment extends Fragment implements Response.ErrorListener, Response.Listener<JSONObject>, View.OnClickListener {
    public static final String REQUEST_TAG = "GateKeeperFragment";
    public String name;
    View GateKeeperFragmentView;
    FragmentManager fragmentManager;
    ListView visitorlistView;
    TextView errorMsg;
    GatekeeperAdapter gatekeeperAdapter;
    ProgressBar progressBar;
    String pid;
    ArrayList<GatekeeperAdminModel> gatekeeper_adminModels;
    String SHOW, CREATE, INDEX, UPDATE, EDIT;
    private RequestQueue mQueue;
    private LockatedPreferences mLockatedPreferences;
    private FloatingActionButton fab;

    public FragmentManager setFragmentManager(FragmentManager fragmentManager) {
        this.fragmentManager = fragmentManager;
        return fragmentManager;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        GateKeeperFragmentView = inflater.inflate(R.layout.fragment_gate_keeper, container, false);
        init(GateKeeperFragmentView);
        Utilities.ladooIntegration(getActivity(), getActivity().getResources().getString(R.string.gateKeeperAdmin));
        return GateKeeperFragmentView;
    }

    public void init(View GateKeeperFragmentView) {
        gatekeeper_adminModels = new ArrayList<>();
        mLockatedPreferences = new LockatedPreferences(getActivity());
        progressBar = (ProgressBar) GateKeeperFragmentView.findViewById(R.id.mProgressBarView);
        visitorlistView = (ListView) GateKeeperFragmentView.findViewById(R.id.listView);
        errorMsg = (TextView) GateKeeperFragmentView.findViewById(R.id.noNotices);
        getGatekeeperRole();
        checkPermission();
        gatekeeperAdapter = new GatekeeperAdapter(getActivity(), gatekeeper_adminModels, EDIT);
        visitorlistView.setAdapter(gatekeeperAdapter);
        if (getArguments() != null) {
            name = getArguments().getString("nameString");
        } else {
            name = "GateKeeperFragment";
        }
        fab = (FloatingActionButton) GateKeeperFragmentView.findViewById(R.id.fab);
        fab.setOnClickListener(this);
    }

    @Override
    public void onResume() {
        super.onResume();
        LockatedApplication.getInstance().trackScreenView(getString(R.string.gateKeeperAdmin));
        Utilities.lockatedGoogleAnalytics(getString(R.string.gateKeeperAdmin),
                getString(R.string.visited), getString(R.string.gateKeeperAdmin));
    }

    private void checkPermission() {
        if (SHOW != null && SHOW.equals("true")) {
            getVisitors();
        } else {
            visitorlistView.setVisibility(View.GONE);
            errorMsg.setVisibility(View.VISIBLE);
            errorMsg.setText(R.string.no_permission_error);
        }
    }

    private void getVisitors() {
        progressBar.setVisibility(View.VISIBLE);
        if (getActivity() != null) {
            if (!mLockatedPreferences.getSocietyId().equals("blank")) {
                if (ConnectionDetector.isConnectedToInternet(getActivity())) {
                    mLockatedPreferences = new LockatedPreferences(getActivity());
                    mQueue = LockatedVolleyRequestQueue.getInstance(getActivity()).getRequestQueue();
                    String url = ApplicationURL.getExpectedVisitor + mLockatedPreferences.getLockatedToken()
                            + ApplicationURL.societyId + mLockatedPreferences.getSocietyId();
                    LockatedJSONObjectRequest lockatedJSONObjectRequest = new LockatedJSONObjectRequest(Request.Method.GET,
                            url, null, this, this);
                    lockatedJSONObjectRequest.setTag(REQUEST_TAG);
                    mQueue.add(lockatedJSONObjectRequest);
                } else {
                    Utilities.showToastMessage(getActivity(), getActivity().getResources().getString(R.string.internet_connection_error));
                }
            } else {
                visitorlistView.setVisibility(View.GONE);
                errorMsg.setVisibility(View.VISIBLE);
                errorMsg.setText(R.string.no_data_error);
            }
        }
    }

    @Override
    public void onErrorResponse(VolleyError error) {
        progressBar.setVisibility(View.GONE);
        if (getActivity() != null) {
            LockatedRequestError.onRequestError(getActivity(), error);
        }
    }

    @Override
    public void onResponse(JSONObject response) {
        progressBar.setVisibility(View.GONE);
        if (getActivity() != null) {
            try {
                if (response != null && response.length() > 0) {
                    if (response.has("code") && response.getJSONArray("gatekeepers").length() > 0) {
                        JSONArray jsonArray = response.getJSONArray("gatekeepers");
                        Gson gson = new Gson();
                        for (int i = 0; i < jsonArray.length(); i++) {
                            GatekeeperAdminModel admin = gson.fromJson(jsonArray.getJSONObject(i).toString(), GatekeeperAdminModel.class);
                            gatekeeper_adminModels.add(admin);
                        }
                        gatekeeperAdapter.notifyDataSetChanged();
                    } else {
                        visitorlistView.setVisibility(View.GONE);
                        errorMsg.setVisibility(View.VISIBLE);
                        errorMsg.setText(R.string.no_data_error);
                    }
                } else {
                    Toast.makeText(getActivity(), "No data available", Toast.LENGTH_LONG).show();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            visitorlistView.setVisibility(View.GONE);
            errorMsg.setVisibility(View.VISIBLE);
            errorMsg.setText(R.string.no_data_error);
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.fab:
                GatekeeperEntryFragment gatekeeperEntryFragment = new GatekeeperEntryFragment();
                Bundle bundle = new Bundle();
                bundle.putString("name", name);
                gatekeeperEntryFragment.setArguments(bundle);
                gatekeeperEntryFragment.show(getActivity().getSupportFragmentManager(), "PreviewDialog");
                break;
        }
    }

    public String getGatekeeperRole() {
        String mySocietyRoles = getActivity().getResources().getString(R.string.blank_value);
        if (mLockatedPreferences.getRolesJson() != null && !mLockatedPreferences.getRolesJson().equals("blank")) {
            try {
                JSONObject noticeJsonObj = new JSONObject(mLockatedPreferences.getRolesJson());
                for (int i = 0; i < noticeJsonObj.getJSONArray(getActivity().getResources().getString(R.string.permissions_value)).length(); i++) {
                    JSONObject jsonObject1 = noticeJsonObj.getJSONArray(getActivity().getResources().getString(R.string.permissions_value)).getJSONObject(i);
                    if (jsonObject1.has(getActivity().getResources().getString(R.string.section_value))
                            && jsonObject1.getString(getActivity().getResources().getString(R.string.section_value)).equals("spree_gatekeeper")) {
                        if (jsonObject1.getJSONObject(getActivity().getResources().getString(R.string.permission_value)).has(getActivity().getResources().getString(R.string.index_value))) {
                            mySocietyRoles = jsonObject1.getJSONObject(getActivity().getResources().getString(R.string.permission_value)).getString(getActivity().getResources().getString(R.string.index_value));
                            CREATE = jsonObject1.getJSONObject(getActivity().getResources().getString(R.string.permission_value)).getString("create");
                            INDEX = jsonObject1.getJSONObject(getActivity().getResources().getString(R.string.permission_value)).getString("index");
                            UPDATE = jsonObject1.getJSONObject(getActivity().getResources().getString(R.string.permission_value)).getString("update");
                            EDIT = jsonObject1.getJSONObject(getActivity().getResources().getString(R.string.permission_value)).getString("edit");
                            SHOW = jsonObject1.getJSONObject(getActivity().getResources().getString(R.string.permission_value)).getString("show");
                            //Log.e("Permissions ", " CREATE" + CREATE + "INDEX" + " " + INDEX + "UPDATE" + " " + UPDATE + " " + "EDIT" + EDIT);
                        }
                    }
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return mySocietyRoles;
    }

    public class GatekeeperAdapter extends BaseAdapter {
        private final ArrayList<GatekeeperAdminModel> gatekeeper_arrayList;
        private final String EDIT;
        Context contextMain;
        LayoutInflater layoutInflater;

        public GatekeeperAdapter(Context context, ArrayList<GatekeeperAdminModel> gatekeeper_arrayList, String EDIT) {
            this.contextMain = context;
            this.EDIT = EDIT;
            this.gatekeeper_arrayList = gatekeeper_arrayList;
            layoutInflater = LayoutInflater.from(context);
        }

        @Override
        public int getCount() {
            return gatekeeper_arrayList.size();
        }

        @Override
        public Object getItem(int position) {
            return position;
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
            ViewHolder holder = null;
            if (convertView == null) {
                convertView = layoutInflater.inflate(R.layout.gatekeeper_child, parent, false);
                holder = new ViewHolder();
                convertView.setTag(holder);
            } else {
                holder = (ViewHolder) convertView.getTag();
            }

            holder.txtTitle = (TextView) convertView.findViewById(R.id.mTextVisitorName);
            holder.txtnumber = (TextView) convertView.findViewById(R.id.mTextNumber);
            holder.mExpectedDate = (TextView) convertView.findViewById(R.id.mExpectedDate);
            holder.mTextDateails = (TextView) convertView.findViewById(R.id.mTextDateails);
            holder.mTextStatus = (TextView) convertView.findViewById(R.id.mTextStatus);
            try {
                pid = String.valueOf(gatekeeper_arrayList.get(position).getId());
                holder.txtTitle.setText(gatekeeper_arrayList.get(position).getGuestName());
                holder.txtnumber.setText(gatekeeper_arrayList.get(position).getGuestNumber());
                String date = gatekeeper_arrayList.get(position).getExpectedAt();
                if (date != null) {
                    holder.mExpectedDate.setText(Utilities.dateTime_AMPM_Format(date));
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            if (EDIT != null && EDIT.equals("true")) {
                holder.mTextDateails.setVisibility(View.VISIBLE);
            } else {
                holder.mTextDateails.setVisibility(View.GONE);
            }
            String status = "" + gatekeeper_arrayList.get(position).getApprove();
            if (status.equals("0")) {
                holder.mTextStatus.setText(getResources().getString(R.string.status_pending));
            } else if (status.equals("1")) {
                String accompany = gatekeeper_arrayList.get(position).getAccompany();
                if(accompany!=null&&accompany.equals("1"))
                    holder.mTextStatus.setText(getResources().getString(R.string.status_accompany));
                else
                    holder.mTextStatus.setText(getResources().getString(R.string.status_approve));
            } else {
                holder.mTextStatus.setText(getResources().getString(R.string.status_reject));
            }
            holder.mTextDateails.setTag(position);
            holder.mTextDateails.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int position = (int) v.getTag();
                    Intent intent = new Intent(getActivity(), GateKeeperActivity.class);
                    intent.putExtra("pid", "" + gatekeeper_arrayList.get(position).getId());
                    intent.putExtra("strName", gatekeeper_arrayList.get(position).getGuestName());
                    intent.putExtra("strNumber", gatekeeper_arrayList.get(position).getGuestNumber());
                    intent.putExtra("strExpDate", gatekeeper_arrayList.get(position).getExpectedAt());
                    intent.putExtra("strVehicleNumber", gatekeeper_arrayList.get(position).getGuestVehicleNumber());
                    intent.putExtra("approved", "" + gatekeeper_arrayList.get(position).getApprove());
                    intent.putExtra("accompany",gatekeeper_arrayList.get(position).getAccompany());
                    intent.putExtra("strPurpose", gatekeeper_arrayList.get(position).getVisitPurpose());
                    intent.putExtra("name", name);
                    intent.putExtra("strVisitTO", gatekeeper_arrayList.get(position).getVisitTo());
                    intent.putExtra("strMember", "" + gatekeeper_arrayList.get(position).getPlusPerson());
                    intent.putExtra("strEntryDate", gatekeeper_arrayList.get(position).getGuestEntryTime());
                    intent.putExtra("strExitDate", gatekeeper_arrayList.get(position).getGuestExitTime());
                    intent.putExtra("strFlatNumber", gatekeeper_arrayList.get(position).getFlat());
                    if (gatekeeper_arrayList.get(position).getDocuments() != null && gatekeeper_arrayList.get(position).getDocuments().size() > 0) {
                        intent.putExtra("doc_id", "" + gatekeeper_arrayList.get(position).getDocuments().get(0).getId());
                        intent.putExtra("visitorImage", gatekeeper_arrayList.get(position).getDocuments().get(0).getDocument());
                    } else {
                        intent.putExtra("doc_id", "No Id");
                        intent.putExtra("visitorImage", "No Document");
                    }
                    startActivity(intent);
                }
            });
            return convertView;
        }

        private class ViewHolder {
            TextView txtTitle, mTextStatus;
            TextView txtnumber, mExpectedDate, mTextDateails;
        }
    }

}
