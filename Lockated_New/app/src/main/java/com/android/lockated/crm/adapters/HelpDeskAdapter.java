package com.android.lockated.crm.adapters;

import android.content.Context;
import android.content.Intent;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.android.lockated.R;
import com.android.lockated.crm.activity.HelpDeskDetailViewActivity;
import com.android.lockated.model.usermodel.HelpDesk.Complaint;
import com.android.lockated.utils.Utilities;

import org.json.JSONArray;

import java.util.ArrayList;

public class HelpDeskAdapter extends BaseAdapter {
    private final ArrayList<Complaint> arrayComplaints;
    Context contextMain;
    JSONArray jsonArray;
    LayoutInflater layoutInflater;
    String pid;

    public HelpDeskAdapter(Context context, ArrayList<Complaint> arrayComplaints, FragmentManager childFragmentManager) {
        this.contextMain = context;
        this.arrayComplaints = arrayComplaints;
        layoutInflater = LayoutInflater.from(context);
    }

    @Override
    public int getCount() {
        return arrayComplaints.size();
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    private class ViewHolder {
        TextView txtCategory, mTextDetails;
        TextView txtStatus;
        TextView createdat;
        TextView updatedat;
        TextView txtheading;//title
        TextView txtText;//description
        TextView txtIssueType;//issue type
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        String status;
        ViewHolder holder = null;
        if (convertView == null) {
            convertView = layoutInflater.inflate(R.layout.help_desk_child, parent, false);
            holder = new ViewHolder();
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        holder.mTextDetails = (TextView) convertView.findViewById(R.id.mTextDetails);
        holder.txtCategory = (TextView) convertView.findViewById(R.id.mTextCategory);
        holder.txtStatus = (TextView) convertView.findViewById(R.id.mTextStatus);
        holder.txtheading = (TextView) convertView.findViewById(R.id.mIssueCategory);
        holder.txtText = (TextView) convertView.findViewById(R.id.mIssueDescription);
        holder.updatedat = (TextView) convertView.findViewById(R.id.mUpdatedOn);
        holder.createdat = (TextView) convertView.findViewById(R.id.mPostedOn);
        holder.txtIssueType = (TextView) convertView.findViewById(R.id.mIssueType);
        if (arrayComplaints.get(position).getIssueStatus() != null) {
            status = arrayComplaints.get(position).getIssueStatus().toString();
        } else {
            status = "Pending";
        }
        if (status.equals("null")) {
            holder.txtStatus.setText("Pending");
        } else {
            holder.txtStatus.setText(status);
        }

        holder.txtCategory.setText(arrayComplaints.get(position).getCategoryType());

        holder.txtheading.setText(arrayComplaints.get(position).getHeading());
        holder.txtText.setText(arrayComplaints.get(position).getText());
        holder.createdat.setText(Utilities.ddMMYYYFormat(arrayComplaints.get(position).getCreatedAt()));
        holder.updatedat.setText(Utilities.ddMMYYYFormat(arrayComplaints.get(position).getUpdatedAt()));
        holder.txtIssueType.setText(arrayComplaints.get(position).getIssueType());
        // pid = "" + arrayComplaints.get(position).getId();
        holder.mTextDetails.setTag(position);
        holder.mTextDetails.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int position = (int) v.getTag();
                try {
                    Intent intent = new Intent(contextMain, HelpDeskDetailViewActivity.class);
                    intent.putExtra("pid", "" + arrayComplaints.get(position).getId());
                    intent.putExtra("strIssueType", arrayComplaints.get(position).getIssueType());
                    intent.putExtra("strDescription", arrayComplaints.get(position).getText());
                    intent.putExtra("strIssueCategory", arrayComplaints.get(position).getHeading());//Title
                    intent.putExtra("strmTextCategory", arrayComplaints.get(position).getCategoryType());
                    intent.putExtra("strPostedOn", Utilities.ddMMYYYFormat(arrayComplaints.get(position).getCreatedAt()));
                    intent.putExtra("strmUpdatedOn", Utilities.ddMMYYYFormat(arrayComplaints.get(position).getUpdatedAt()));
                    intent.putExtra("strStatus", "" + arrayComplaints.get(position).getIssueStatus());

                    if (arrayComplaints.get(position).getDocuments() != null && arrayComplaints.get(position).getDocuments().size() > 0) {

                        intent.putExtra("strComplaintImage", arrayComplaints.get(position).getDocuments().get(0).getDocument());
                    } else {
                        intent.putExtra("strComplaintImage", "No Document");

                    }
                    contextMain.startActivity(intent);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

        return convertView;
    }
}