package com.android.lockated.categories.miscellaneous;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import com.android.lockated.LockatedApplication;
import com.android.lockated.R;
import com.android.lockated.account.fragment.MyAddressFragment;
import com.android.lockated.categories.services.model.ServiceRowData;
import com.android.lockated.information.ServiceController;
import com.android.lockated.model.MyAddressData;
import com.android.lockated.network.ConnectionDetector;
import com.android.lockated.preferences.LockatedPreferences;
import com.android.lockated.request.LockatedRequestError;
import com.android.lockated.request.LockatedVolleyRequestQueue;
import com.android.lockated.utils.ApplicationURL;
import com.android.lockated.utils.Utilities;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class MiscellaneousFragment extends Fragment implements View.OnClickListener, Response.Listener, Response.ErrorListener {
    private EditText mEditTextMiscellaneousTitle;
    private EditText mEditTextMiscellaneousDescription;

    private TextView mTextViewMiscellaneousDetailSubmit;
    private TextView mTextViewServiceDeliveryAddress;
    private TextView mTextViewServiceDeliveryAddressChange;

    private ProgressDialog mProgressDialog;

    private MyAddressData myAddressData;
    private ServiceController mServiceController;
    private LockatedPreferences mLockatedPreferences;
    String screenName = "Miscellaneous Screen";
    private static final String REQUEST_TAG = "MiscellaneousFragment";

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View miscellaneousView = inflater.inflate(R.layout.fragment_miscellaneous, container, false);
        init(miscellaneousView);
        Utilities.ladooIntegration(getActivity(), screenName);
        Utilities.lockatedGoogleAnalytics(screenName, getString(R.string.visited), screenName);
        refreshValues();
        return miscellaneousView;
    }

    private void init(View miscellaneousView) {
        mServiceController = ServiceController.getInstance();
        mLockatedPreferences = new LockatedPreferences(getActivity());

        mServiceController.resetData();
        mServiceController.getServiceRowDataList().add(new ServiceRowData());

        mServiceController.getServiceRowDataList().get(0).setCategoryId(((LockatedApplication) getActivity().getApplicationContext()).getCategoryIdTwo());
        mServiceController.getServiceRowDataList().get(0).setSubCategoryId(((LockatedApplication) getActivity().getApplicationContext()).getCategoryId());

        mEditTextMiscellaneousTitle = (EditText) miscellaneousView.findViewById(R.id.mEditTextMiscellaneousTitle);
        mEditTextMiscellaneousDescription = (EditText) miscellaneousView.findViewById(R.id.mEditTextMiscellaneousDescription);

        mTextViewMiscellaneousDetailSubmit = (TextView) miscellaneousView.findViewById(R.id.mTextViewMiscellaneousDetailSubmit);
        mTextViewServiceDeliveryAddress = (TextView) miscellaneousView.findViewById(R.id.mTextViewServiceDeliveryAddress);
        mTextViewServiceDeliveryAddressChange = (TextView) miscellaneousView.findViewById(R.id.mTextViewServiceDeliveryAddressChange);

        mTextViewServiceDeliveryAddress.setOnClickListener(this);
        mTextViewMiscellaneousDetailSubmit.setOnClickListener(this);
        mTextViewServiceDeliveryAddressChange.setOnClickListener(this);
    }

    private void refreshValues() {
        setInputDetails();
        setAddressDetails();
    }

    private void setInputDetails() {
        mEditTextMiscellaneousTitle.setText(mServiceController.getServiceTitle());
        mEditTextMiscellaneousDescription.setText(mServiceController.getServiceDescription());
    }

    private void setAddressDetails() {
        myAddressData = ((LockatedApplication) getActivity().getApplicationContext()).getMyAddressData();
        if (myAddressData != null) {
            mTextViewServiceDeliveryAddressChange.setVisibility(View.VISIBLE);
            mTextViewServiceDeliveryAddress.setText(myAddressData.getFirstName() + " " + myAddressData.getLastName() + "\n\n" + myAddressData.getAddress1() + " " + myAddressData.getAddress2() + " " + myAddressData.getCity() + " " + myAddressData.getState() + " " + myAddressData.getZipcode());
        }
    }

    private void onSelectAddressClicked() {
        myAddressData = ((LockatedApplication) getActivity().getApplicationContext()).getMyAddressData();
        if (myAddressData == null) {
            MyAddressFragment myAddressFragment = new MyAddressFragment();
            myAddressFragment.setViewId(1);
            getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.mVendorContainer, myAddressFragment).addToBackStack("Checkout").commit();
        } else {
            mTextViewServiceDeliveryAddressChange.setVisibility(View.VISIBLE);
            mTextViewServiceDeliveryAddress.setText(myAddressData.getFirstName() + " " + myAddressData.getLastName() + "\n\n" + myAddressData.getAddress1() + " " + myAddressData.getAddress2() + " " + myAddressData.getCity() + " " + myAddressData.getState() + " " + myAddressData.getZipcode());
        }
    }

    private void onAddressChangeClicked() {
        MyAddressFragment myAddressFragment = new MyAddressFragment();
        myAddressFragment.setViewId(1);
        getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.mVendorContainer, myAddressFragment).addToBackStack("Checkout").commit();
    }

    private void onServiceDetailSubmit() {
        String strMiscellaneousTitle = mEditTextMiscellaneousTitle.getText().toString();
        String MiscellaneousDescription = mEditTextMiscellaneousDescription.getText().toString();

        String strMessage = "";
        boolean serviceCancel = false;

        if (TextUtils.isEmpty(strMiscellaneousTitle)) {
            serviceCancel = true;
            strMessage = "Please enter miscellaneous title";
        }

        if (TextUtils.isEmpty(MiscellaneousDescription)) {
            serviceCancel = true;
            strMessage = "Please enter miscellaneous description";
        }

        if (serviceCancel) {
            Utilities.showToastMessage(getActivity(), strMessage);
        } else {
            if (ConnectionDetector.isConnectedToInternet(getActivity())) {
                if (mServiceController.getServiceRowDataList().size() > 0) {
                    if (myAddressData != null) {
                        mProgressDialog = ProgressDialog.show(getActivity(), "", "Please wait...", false);

                        int categoryId = mServiceController.getServiceRowDataList().get(0).getCategoryId();
                        int subCategoryId = mServiceController.getServiceRowDataList().get(0).getSubCategoryId();

                        JSONObject jsonObject = new JSONObject();
                        JSONObject serviceObject = new JSONObject();
                        JSONArray optionArray = new JSONArray();

                        try {
                            for (int i = 0; i < mServiceController.getServiceRowDataList().size(); i++) {
                                JSONObject optionJsonObject = new JSONObject();
                                optionJsonObject.put("service_metadata_id", "");
                                optionJsonObject.put("attribute_one", "");
                                optionJsonObject.put("attribute_two", "");
                                optionJsonObject.put("attribute_three", "");

                                optionArray.put(i, optionJsonObject);
                                serviceObject.put("options", optionArray);
                            }

                            serviceObject.put("category_id", categoryId);
                            serviceObject.put("sub_category_id", subCategoryId);
                            serviceObject.put("pdate", "");
                            serviceObject.put("ptime", "");
                            serviceObject.put("ship_address_id", myAddressData.getAddressId());
                            serviceObject.put("bill_address_id", myAddressData.getAddressId());
                            serviceObject.put("title", mEditTextMiscellaneousTitle.getText().toString());
                            serviceObject.put("description", mEditTextMiscellaneousDescription.getText().toString());

                            jsonObject.put("service_requests", serviceObject);

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                        LockatedVolleyRequestQueue lockatedVolleyRequestQueue = LockatedVolleyRequestQueue.getInstance(getActivity());
                        lockatedVolleyRequestQueue.sendRequest(REQUEST_TAG, Request.Method.POST, ApplicationURL.serviceDataUrl + mLockatedPreferences.getLockatedToken(), jsonObject, this, this);
                    } else {
                        Utilities.showToastMessage(getActivity(), "Please select address to continue.");
                    }
                }
            } else {
                Utilities.showToastMessage(getActivity(), getActivity().getResources().getString(R.string.internet_connection_error));
            }
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.mTextViewServiceDeliveryAddress:
                onSelectAddressClicked();
                break;

            case R.id.mTextViewServiceDeliveryAddressChange:
                onAddressChangeClicked();
                break;

            case R.id.mTextViewMiscellaneousDetailSubmit:
                onServiceDetailSubmit();
                break;

        }
    }

    @Override
    public void onErrorResponse(VolleyError error) {
        if (mProgressDialog != null && mProgressDialog.isShowing()) {
            mProgressDialog.dismiss();
        }

        if (getActivity() != null) {
            LockatedRequestError.onRequestError(getActivity(), error);
        }
    }

    @Override
    public void onResponse(Object response) {
        if (mProgressDialog != null && mProgressDialog.isShowing()) {
            mProgressDialog.dismiss();
        }

        JSONObject jsonObject = (JSONObject) response;

        try {
            int code = jsonObject.optInt("code");
            if (code == 200) {
                mServiceController.resetData();
                ((LockatedApplication) getActivity().getApplicationContext()).setMyAddressData(null);
                ((LockatedApplication) getActivity().getApplicationContext()).setmDeliveryTime("");
                ((LockatedApplication) getActivity().getApplicationContext()).setmDeliveryDate("");
                ((MiscellaneousActivity) getActivity()).addCongartulationScreen(jsonObject.optInt("id") + "");
            } else {
                Utilities.showToastMessage(getActivity(), jsonObject.getString("error"));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}