
package com.android.lockated.model.usermodel.myGroups;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;


public class Example {

    @SerializedName("user_societies")
    @Expose
    private List<UserSociety> userSocieties = new ArrayList<UserSociety>();

    /**
     * @return The userSocieties
     */
    public List<UserSociety> getUserSocieties() {
        return userSocieties;
    }

    /**
     * @param userSocieties The user_societies
     */
    public void setUserSocieties(List<UserSociety> userSocieties) {
        this.userSocieties = userSocieties;
    }

}
