package com.android.lockated.register;

import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.telephony.SmsMessage;
import android.text.TextUtils;
import android.text.method.HideReturnsTransformationMethod;
import android.text.method.PasswordTransformationMethod;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.lockated.R;
import com.android.lockated.information.AccountController;
import com.android.lockated.login.LoginFragment;
import com.android.lockated.network.ConnectionDetector;
import com.android.lockated.preferences.LockatedPreferences;
import com.android.lockated.request.LockatedRequestError;
import com.android.lockated.request.LockatedVolleyRequestQueue;
import com.android.lockated.utils.ApplicationURL;
import com.android.lockated.utils.Utilities;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;

import org.json.JSONException;
import org.json.JSONObject;

public class ResetPasswordFragment extends Fragment implements View.OnClickListener, Response.ErrorListener, Response.Listener<JSONObject> {

    private TextView mTextViewSignUp;
    private TextView mTextViewSignIn;
    private TextView mTextSignUpBar;
    private TextView mTextSignInBar;
    private TextView mButtonSendOtp;
    private TextView mButtonResendOtp;
    private TextView mButtonSubmit;
    private TextView mTextViewPassword;
    private TextView mTextViewEmailAddress;

    private ImageView mImageViewShowNewPassword;
    private ImageView mImageViewShowConfirmPassword;

    private EditText mEditTextPhone;
    private EditText mEditTextOTP;
    private EditText mEditTextNewPassword;
    private EditText mEditTextConfirmPassword;
    private LinearLayout sendResendOtp;
    private LinearLayout setPassword;

    private String mToken;
    private String mNumber;
    private String OTP;
    String calledFrom;
    private boolean isPasswordVisible;

    private RequestQueue mQueue;
    private ProgressDialog mProgressDialog;
    private AccountController mAccountController;
    private LockatedPreferences mLockatedPreferences;
    private LockatedVolleyRequestQueue mLockatedVolleyRequestQueue;

    public static final String REQUEST_OTP = "ResetPasswordFragment";
    public static final String RESET_PASSWORD_REQUEST = "ResetPassword";
    public static final String ACCOUNT_REQUEST_TAG = "AccountDetail";

    private FragmentManager fragmentManager;
    boolean receiverRegistered = false;

    public void setResetPasswordFragment(String calledFrom) {
        this.calledFrom = calledFrom;
    }


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getActivity() != null) {
            getActivity().registerReceiver(mIncommingSmsBroadcastReceiver,
                    new IntentFilter("android.provider.Telephony.SMS_RECEIVED"));
            receiverRegistered = true;
        }
    }

    BroadcastReceiver mIncommingSmsBroadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {

            final Bundle bundle = intent.getExtras();

            if (intent.getAction().equalsIgnoreCase("android.provider.Telephony.SMS_RECEIVED")) {
                try {
                    if (bundle != null) {
                        final Object[] pdusObj = (Object[]) bundle.get("pdus");
                        if (pdusObj != null) {
                            for (int i = 0; i < pdusObj.length; i++) {
                                SmsMessage currentMessage = SmsMessage.createFromPdu((byte[]) pdusObj[i]);
                                //String phoneNumber = currentMessage.getDisplayOriginatingAddress();
                                String message = currentMessage.getDisplayMessageBody();
                                message = message.replaceAll("\\D+", "");
                                Log.e("message", "" + message);
                                mEditTextOTP.setText(message);
                            }
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }

    };

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View mResetPasswordView = inflater.inflate(R.layout.fragment_reset_password, container, false);
        init(mResetPasswordView);

        return mResetPasswordView;
    }

    public FragmentManager setFragmentManager(FragmentManager fragmentManager) {
        this.fragmentManager = fragmentManager;
        return fragmentManager;
    }

    private void init(View mResetPasswordView) {

        mAccountController = AccountController.getInstance();
        mLockatedPreferences = new LockatedPreferences(getActivity());

        mTextViewSignUp = (TextView) getActivity().findViewById(R.id.mTextViewSignUp);
        mTextViewSignIn = (TextView) getActivity().findViewById(R.id.mTextViewSignIn);
        mTextSignUpBar = (TextView) getActivity().findViewById(R.id.mTextSignUpBar);
        mTextSignInBar = (TextView) getActivity().findViewById(R.id.mTextSignInBar);

        mButtonSendOtp = (TextView) mResetPasswordView.findViewById(R.id.mButtonSendOtp);
        mButtonResendOtp = (TextView) mResetPasswordView.findViewById(R.id.mButtonResendOtp);
        mButtonSubmit = (TextView) mResetPasswordView.findViewById(R.id.mButtonSubmit);

        mTextViewPassword = (TextView) mResetPasswordView.findViewById(R.id.mTextViewPassword);
        mTextViewEmailAddress = (TextView) mResetPasswordView.findViewById(R.id.mTextViewEmailAddress);

        mImageViewShowNewPassword = (ImageView) mResetPasswordView.findViewById(R.id.mImageViewShowNewPassword);
        mImageViewShowConfirmPassword = (ImageView) mResetPasswordView.findViewById(R.id.mImageViewShowConfirmPassword);

        mEditTextPhone = (EditText) mResetPasswordView.findViewById(R.id.mEditTextPhone);
        mEditTextOTP = (EditText) mResetPasswordView.findViewById(R.id.mEditTextOTP);
        mEditTextNewPassword = (EditText) mResetPasswordView.findViewById(R.id.mEditTextNewPassword);
        mEditTextConfirmPassword = (EditText) mResetPasswordView.findViewById(R.id.mEditTextConfirmPassword);

        sendResendOtp = (LinearLayout) mResetPasswordView.findViewById(R.id.sendResendOtp);
        setPassword = (LinearLayout) mResetPasswordView.findViewById(R.id.setPassword);

        mButtonSendOtp.setOnClickListener(this);
        mButtonResendOtp.setOnClickListener(this);
        mButtonSubmit.setOnClickListener(this);
        mImageViewShowNewPassword.setOnClickListener(this);
        mImageViewShowConfirmPassword.setOnClickListener(this);

        String mobileNumberText = getArguments().getString("mobileNumber");
        mEditTextPhone.setText(mobileNumberText);
        /*setEditTextListener();
        hideShowPassword();
        disableSignUpButton();*/

    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {

            case R.id.mButtonSendOtp:
                onSendOtpClicked();
                break;

            case R.id.mButtonResendOtp:
                if (getActivity() != null) {
                    getActivity().registerReceiver(mIncommingSmsBroadcastReceiver,
                            new IntentFilter("android.provider.Telephony.SMS_RECEIVED"));
                }
                onSendOtpClicked();
                break;

            case R.id.mButtonSubmit:
                onSubmitNewPasswordClicked();
                break;
            case R.id.mImageViewShowNewPassword:
                onShowPasswordClicked();
                break;
            case R.id.mImageViewShowConfirmPassword:
                onShowConfermPasswordClicked();
                break;
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        if (getActivity() != null) {
            if (receiverRegistered) {
                getActivity().unregisterReceiver(mIncommingSmsBroadcastReceiver);
                receiverRegistered = false;
            }
        }
    }

    private void onShowConfermPasswordClicked() {
        if (isPasswordVisible) {
            isPasswordVisible = false;
            mEditTextConfirmPassword.setTransformationMethod(PasswordTransformationMethod.getInstance());
            mEditTextConfirmPassword.setSelection(mEditTextConfirmPassword.getText().length());
        } else {
            isPasswordVisible = true;
            mEditTextConfirmPassword.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
            mEditTextConfirmPassword.setSelection(mEditTextConfirmPassword.getText().length());
        }
    }

    private void onShowPasswordClicked() {
        if (isPasswordVisible) {
            isPasswordVisible = false;
            mEditTextNewPassword.setTransformationMethod(PasswordTransformationMethod.getInstance());
            mEditTextNewPassword.setSelection(mEditTextNewPassword.getText().length());
        } else {
            isPasswordVisible = true;
            mEditTextNewPassword.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
            mEditTextNewPassword.setSelection(mEditTextNewPassword.getText().length());
        }
    }


    private void onSendOtpClicked() {
        String strUserPhone = mEditTextPhone.getText().toString();
        boolean loginCancel = false;
        View loginFocusView = null;

        // Validation for Password field, it should not be blank
        if (TextUtils.isEmpty(strUserPhone)) {
            loginFocusView = mEditTextPhone;
            loginCancel = true;
        }

        if (loginCancel) {
            loginFocusView.requestFocus();
            Utilities.showToastMessage(getActivity(), getActivity().getResources().getString(R.string.login_blank_field_error));
        } else {
            if (ConnectionDetector.isConnectedToInternet(getActivity())) {
                mProgressDialog = ProgressDialog.show(getActivity(), "", "Please Wait...", false);
                mProgressDialog.show();

                JSONObject jsonObjectFields = new JSONObject();
                try {
                    jsonObjectFields.put("request_otp", "1");
                    jsonObjectFields.put("mobile", strUserPhone);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                String url = ApplicationURL.getForgotPasswordOtp;
                mLockatedVolleyRequestQueue = LockatedVolleyRequestQueue.getInstance(getActivity());
                mLockatedVolleyRequestQueue.sendRequest(REQUEST_OTP, Request.Method.POST, url, jsonObjectFields, this, this);

                Utilities.lockatedGoogleAnalytics(getString(R.string.login), getString(R.string.login), getString(R.string.login));
            } else {
                Utilities.showToastMessage(getActivity(), getActivity().getResources().getString(R.string.internet_connection_error));
            }
        }
    }

    private void onSubmitNewPasswordClicked() {

        String strUserPhone = mEditTextPhone.getText().toString();
        String strUserOtp = mEditTextOTP.getText().toString();
        String strUserNewPassword = mEditTextNewPassword.getText().toString();
        String strUserConfirmPassword = mEditTextConfirmPassword.getText().toString();

        boolean loginCancel = false;
        View loginFocusView = null;

        // Validation for Password field, it should not be blank
        if (TextUtils.isEmpty(strUserPhone)) {
            loginFocusView = mEditTextPhone;
            loginCancel = true;
        }
        if (TextUtils.isEmpty(strUserOtp)) {
            loginFocusView = mEditTextOTP;
            loginCancel = true;
        }
        if (TextUtils.isEmpty(strUserNewPassword)) {
            loginFocusView = mEditTextNewPassword;
            loginCancel = true;
        }
        if (TextUtils.isEmpty(strUserConfirmPassword)) {
            loginFocusView = mEditTextConfirmPassword;
            loginCancel = true;
        }
        if (loginCancel) {
            loginFocusView.requestFocus();
            Utilities.showToastMessage(getActivity(), getActivity().getResources().getString(R.string.login_blank_field_error));
        } else {
            if (ConnectionDetector.isConnectedToInternet(getActivity())) {
                mProgressDialog = ProgressDialog.show(getActivity(), "", "Please Wait...", false);
                mProgressDialog.show();

                JSONObject jsonObjectFields = new JSONObject();
                try {
                    jsonObjectFields.put("verify_otp", "1");
                    jsonObjectFields.put("mobile", strUserPhone);
                    jsonObjectFields.put("otp", strUserOtp);
                    jsonObjectFields.put("newpassword", strUserNewPassword);
                    jsonObjectFields.put("newpassword_confirm", strUserConfirmPassword);

                } catch (JSONException e) {
                    e.printStackTrace();
                }
                String url = ApplicationURL.getForgotPasswordOtp;
                mLockatedVolleyRequestQueue = LockatedVolleyRequestQueue.getInstance(getActivity());
                mLockatedVolleyRequestQueue.sendRequest(RESET_PASSWORD_REQUEST, Request.Method.POST, url, jsonObjectFields, this, this);

                Utilities.lockatedGoogleAnalytics(getString(R.string.login), getString(R.string.login), getString(R.string.login));
            } else {
                Utilities.showToastMessage(getActivity(), getActivity().getResources().getString(R.string.internet_connection_error));
            }
        }
    }

    private void openLoginReplaceFragment() {
        if (!getActivity().isFinishing()) {
            getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.container, new LoginFragment()).commitAllowingStateLoss();
        }
    }

    @Override
    public void onErrorResponse(VolleyError error) {
        mProgressDialog.dismiss();
        if (getActivity() != null) {
            LockatedRequestError.onRequestError(getActivity(), error);
        }
    }

    @Override
    public void onResponse(JSONObject response) {
        mProgressDialog.dismiss();
        if (getActivity() != null) {
            try {
                if (mLockatedVolleyRequestQueue.getLockatedJSONObjectRequest().getTag().toString().equals(RESET_PASSWORD_REQUEST)) {
                    Utilities.showToastMessage(getActivity(), response.getString("message"));
                    if (calledFrom.equals("ChangePasswordActivity")) {
                        getActivity().finish();
                    }
                    if (calledFrom.equals("LoginFragment")) {
                        openLoginReplaceFragment();
                    }
                } else if (mLockatedVolleyRequestQueue.getLockatedJSONObjectRequest().getTag().toString().equals(REQUEST_OTP)) {
                    OTP = response.getString("otp");
                    Utilities.showToastMessage(getActivity(), response.getString("message"));
                    sendResendOtp.setVisibility(View.GONE);
                    setPassword.setVisibility(View.VISIBLE);
                } else if (mLockatedVolleyRequestQueue.getLockatedJSONObjectRequest().getTag().toString().equals(ACCOUNT_REQUEST_TAG)) {
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}
