
package com.android.lockated.model.BaseApi;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.json.JSONException;
import org.json.JSONObject;

public class Society {

    private int id;
    private String buildingName;

    public Society(JSONObject jsonObjectSociety1) {
        try {
            id = jsonObjectSociety1.getInt("id");

            buildingName=jsonObjectSociety1.getString("building_name");
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getBuildingName() {
        return buildingName;
    }

    public void setBuildingName(String buildingName) {
        this.buildingName = buildingName;
    }

}
