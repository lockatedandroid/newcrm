package com.android.lockated.categories.grocery.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.android.lockated.Interfaces.IRecyclerItemClickListener;
import com.android.lockated.R;
import com.android.lockated.cart.CartController;
import com.android.lockated.categories.grocery.model.GroceryItemData;
import com.android.lockated.holder.RecyclerViewHolder;

import java.util.ArrayList;

public class CategoryItemDetailAdapter extends RecyclerView.Adapter<RecyclerViewHolder> {
    private CartController mCartController;
    private ArrayList<GroceryItemData> mGroceryItemData;
    private IRecyclerItemClickListener mIRecyclerItemClickListener;

    public CategoryItemDetailAdapter(ArrayList<GroceryItemData> data, IRecyclerItemClickListener listener) {
        mGroceryItemData = data;
        mIRecyclerItemClickListener = listener;
        mCartController = CartController.getInstance();
    }

    @Override
    public RecyclerViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.fragment_category_detail_item_row, parent, false);
        return new RecyclerViewHolder(itemView, mIRecyclerItemClickListener);
    }

    @Override
    public void onBindViewHolder(RecyclerViewHolder holder, int position) {
        holder.onCategoryDetailViewHolder(mGroceryItemData.get(position));
    }

    @Override
    public int getItemCount() {
        return mGroceryItemData.size();
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }
}
