
package com.android.lockated.model.zomato;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class OrderDetail {

    @SerializedName("Taxes")
    @Expose
    private ArrayList<Tax> taxes = new ArrayList<Tax>();
    @SerializedName("Loyalty_discounts")
    @Expose
    private ArrayList<LoyaltyDiscount> loyaltyDiscounts = new ArrayList<LoyaltyDiscount>();
    @SerializedName("Subtotal2")
    @Expose
    private ArrayList<Subtotal2> subtotal2 = new ArrayList<Subtotal2>();
    @SerializedName("Salt_discounts")
    @Expose
    private ArrayList<SaltDiscount> saltDiscounts = new ArrayList<SaltDiscount>();
    @SerializedName("Dishes")
    @Expose
    private ArrayList<Dish> dishes = new ArrayList<Dish>();
    @SerializedName("Charges")
    @Expose
    private ArrayList<Charge> charges = new ArrayList<Charge>();
    @SerializedName("Total")
    @Expose
    private ArrayList<Total> total = new ArrayList<Total>();
    @SerializedName("Voucher_discounts")
    @Expose
    private ArrayList<VoucherDiscount> voucherDiscounts =  new ArrayList<VoucherDiscount>();

    /**
     * 
     * @return
     *     The taxes
     */
    public ArrayList<Tax> getTaxes() {
        return taxes;
    }

    /**
     * 
     * @param taxes
     *     The Taxes
     */
    public void setTaxes(ArrayList<Tax> taxes) {
        this.taxes = taxes;
    }

    /**
     * 
     * @return
     *     The loyaltyDiscounts
     */
    public ArrayList<LoyaltyDiscount> getLoyaltyDiscounts() {
        return loyaltyDiscounts;
    }

    /**
     * 
     * @param loyaltyDiscounts
     *     The Loyalty_discounts
     */
    public void setLoyaltyDiscounts(ArrayList<LoyaltyDiscount> loyaltyDiscounts) {
        this.loyaltyDiscounts = loyaltyDiscounts;
    }

    /**
     * 
     * @return
     *     The subtotal2
     */
    public ArrayList<Subtotal2> getSubtotal2() {
        return subtotal2;
    }

    /**
     * 
     * @param subtotal2
     *     The Subtotal2
     */
    public void setSubtotal2(ArrayList<Subtotal2> subtotal2) {
        this.subtotal2 = subtotal2;
    }

    /**
     * 
     * @return
     *     The saltDiscounts
     */
    public ArrayList<SaltDiscount> getSaltDiscounts() {
        return saltDiscounts;
    }

    /**
     * 
     * @param saltDiscounts
     *     The Salt_discounts
     */
    public void setSaltDiscounts(ArrayList<SaltDiscount> saltDiscounts) {
        this.saltDiscounts = saltDiscounts;
    }

    /**
     * 
     * @return
     *     The dishes
     */
    public ArrayList<Dish> getDishes() {
        return dishes;
    }

    /**
     * 
     * @param dishes
     *     The Dishes
     */
    public void setDishes(ArrayList<Dish> dishes) {
        this.dishes = dishes;
    }

    /**
     * 
     * @return
     *     The charges
     */
    public ArrayList<Charge> getCharges() {
        return charges;
    }

    /**
     * 
     * @param charges
     *     The Charges
     */
    public void setCharges(ArrayList<Charge> charges) {
        this.charges = charges;
    }

    /**
     * 
     * @return
     *     The total
     */
    public ArrayList<Total> getTotal() {
        return total;
    }

    /**
     * 
     * @param total
     *     The Total
     */
    public void setTotal(ArrayList<Total> total) {
        this.total = total;
    }

    /**
     * 
     * @return
     *     The voucherDiscounts
     */
    public ArrayList<VoucherDiscount> getVoucherDiscounts() {
        return voucherDiscounts;
    }

    /**
     * 
     * @param voucherDiscounts
     *     The Voucher_discounts
     */
    public void setVoucherDiscounts(ArrayList<VoucherDiscount> voucherDiscounts) {
        this.voucherDiscounts = voucherDiscounts;
    }

}
