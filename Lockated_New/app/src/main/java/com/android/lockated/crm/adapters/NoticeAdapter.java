package com.android.lockated.crm.adapters;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.android.lockated.R;
import com.android.lockated.crm.activity.NoticeDetailActivity;
import com.android.lockated.information.AccountController;
import com.android.lockated.model.AccountData;
import com.android.lockated.model.usermodel.NoticeModel.AllNotices.Notice;
import com.android.lockated.model.usermodel.NoticeModel.AllNotices.Noticeboard;
import com.android.lockated.utils.Utilities;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;


public class NoticeAdapter extends RecyclerView.Adapter<NoticeAdapter.NoticeViewHolder> {

    Context contextMain;
    JSONArray jsonArray;
    FragmentManager childFragmentManager;
    AccountController accountController;
    ArrayList<AccountData> datas;
    ArrayList<Noticeboard> noticeboardArrayList;

    public NoticeAdapter(Context context, ArrayList<Noticeboard> noticeboardArrayList, FragmentManager childFragmentManager) {
        this.contextMain = context;
        this.noticeboardArrayList = noticeboardArrayList;
        this.childFragmentManager = childFragmentManager;
        accountController = AccountController.getInstance();
        datas = accountController.getmAccountDataList();
    }

    @Override
    public NoticeViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(contextMain).inflate(R.layout.notice_parent_view, parent, false);
        NoticeViewHolder pvh = new NoticeViewHolder(v);
        return pvh;
    }

    @Override
    public void onBindViewHolder(NoticeViewHolder holder, int position) {

        try {
            /*String dateSep = "(" + jsonArray.getJSONObject(position).get("date") + ")";
            holder.dateSeperator.setText(dateSep);
            NoticeCardAdapter noticeCardAdapter = new NoticeCardAdapter(contextMain,
                    jsonArray.getJSONObject(position).getJSONArray("notices"));
            holder.recycler.setAdapter(noticeCardAdapter);*/

            String dateSep = "(" + noticeboardArrayList.get(position).getDate() + ")";
            holder.dateSeperator.setText(dateSep);
            NoticeCardAdapter noticeCardAdapter = new NoticeCardAdapter(contextMain,
                    noticeboardArrayList.get(position).getNotices());
            holder.recycler.setAdapter(noticeCardAdapter);

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public int getItemCount() {
        //return jsonArray.length();
        return noticeboardArrayList.size();
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }

    public class NoticeViewHolder extends RecyclerView.ViewHolder {

        RecyclerView recycler;
        TextView dateSeperator;

        public NoticeViewHolder(View itemView) {
            super(itemView);

            dateSeperator = (TextView) itemView.findViewById(R.id.dateStamp);
            recycler = (RecyclerView) itemView.findViewById(R.id.recyclerViewFirstAdapter);
            LinearLayoutManager linearLayoutManager = new LinearLayoutManager(contextMain,
                    LinearLayoutManager.HORIZONTAL, false);
            recycler.setLayoutManager(linearLayoutManager);

        }
    }

//======================================================================================================

    public class NoticeCardAdapter extends RecyclerView.Adapter<NoticeCardAdapter.NoticeGridViewHolder> {

        Context contextSub;
        JSONArray jsonArraySub;
        ArrayList<Notice> notices;

        public NoticeCardAdapter(Context context, /*JSONArray jsonArray*/ArrayList<Notice> notices) {
            this.contextSub = context;
            this.notices = notices;
            this.jsonArraySub = jsonArray;
        }

        @Override
        public NoticeGridViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View v = LayoutInflater.from(contextSub).inflate(R.layout.notice_child_view, parent, false);
            NoticeGridViewHolder noticeGridViewHolder = new NoticeGridViewHolder(v);
            return noticeGridViewHolder;
        }

        @Override
        public void onBindViewHolder(NoticeGridViewHolder holder, int position) {

            holder.cardView.setCardBackgroundColor(Color.WHITE);
            try {
                if (Utilities.dateConvertMinify(notices.get(position).getExpireTime()) != "No Date") {
                    holder.mDetailsView.setTag(position);
                    holder.dateTxt1.setText(Utilities.dateConvertMinify(notices.get(position).getExpireTime()));
                    if (!notices.get(position).getNoticeHeading().equals("null")) {
                        holder.subNameTxt1.setText(notices.get(position).getNoticeHeading());
                    } else {
                        holder.subNameTxt1.setText("No Subject");
                    }
                    if (!notices.get(position).getNoticeText().equals("null")) {
                        holder.postedNameTxt1.setText(notices.get(position).getUsername());
                    } else {
                        holder.postedNameTxt1.setText("No Post Available");
                    }
                }

            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        @Override
        public int getItemCount() {
            return notices.size();
        }

        @Override
        public void onAttachedToRecyclerView(RecyclerView recyclerView) {
            super.onAttachedToRecyclerView(recyclerView);
        }

        public class NoticeGridViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

            TextView dateTxt1, timeTxt1, subNameTxt1, postedNameTxt1, mDetailsView;
            CardView cardView;


            public NoticeGridViewHolder(View itemView) {
                super(itemView);

                cardView = (CardView) itemView.findViewById(R.id.cardView);
                dateTxt1 = (TextView) itemView.findViewById(R.id.date1);
                timeTxt1 = (TextView) itemView.findViewById(R.id.time1);
                subNameTxt1 = (TextView) itemView.findViewById(R.id.subjectName1);
                postedNameTxt1 = (TextView) itemView.findViewById(R.id.postedName1);
                mDetailsView = (TextView) itemView.findViewById(R.id.mDetailsView);
                mDetailsView.setTag(itemView);
                mDetailsView.setOnClickListener(this);
            }

            @Override
            public void onClick(View v) {
                switch (v.getId()) {

                    case R.id.mDetailsView:
                        int position = (int) v.getTag();
                        try {
                            viewCardDetail(contextSub, notices, position);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        break;

                }
            }

            public void viewCardDetail(Context context, ArrayList<Notice> notices, int position) {
                JSONObject jsonObject = new JSONObject();
                try {
                    jsonObject.putOpt("event_type", "Notice");
                    jsonObject.putOpt("created_by", notices.get(position).getIdUser());
                    jsonObject.putOpt("notice_heading", notices.get(position).getNoticeHeading());
                    jsonObject.putOpt("description", notices.get(position).getNoticeText());
                    jsonObject.putOpt("created_at", notices.get(position).getCreatedAt());
                    String date;
                    if (notices.get(position).getExpireTime() != null && notices.get(position).getExpireTime() != "null") {
                        date = Utilities.dateConvertMinify(notices.get(position).getExpireTime());
                    } else {
                        date = "No Date";
                    }
                    if (notices.get(position).getDocuments() != null && notices.get(position).getDocuments().size() > 0) {
                        jsonObject.putOpt("document", notices.get(position).getDocuments().get(0).getDocument());
                    } else {
                        jsonObject.putOpt("document", "No Document");
                    }
                    jsonObject.putOpt("expire_time", date);
                    jsonObject.putOpt("username", notices.get(position).getUsername());
                    jsonObject.putOpt("flat", notices.get(position).getFlat());
                } catch (Exception e) {
                    e.printStackTrace();
                }

                Intent allNoticeDetailIntent = new Intent(context, NoticeDetailActivity.class);
                allNoticeDetailIntent.putExtra("NoticeDetailViewData", jsonObject.toString());
                context.startActivity(allNoticeDetailIntent);

            }
        }
    }

}