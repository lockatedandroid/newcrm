package com.android.lockated.Interfaces;

import com.android.lockated.categories.reminder.model.ReminderData;

public interface IReminderAdded
{
    public void onReminderUpdated(ReminderData reminderData, int requestId, int position);
}
