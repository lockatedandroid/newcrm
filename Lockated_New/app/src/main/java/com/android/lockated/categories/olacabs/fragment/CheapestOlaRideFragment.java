package com.android.lockated.categories.olacabs.fragment;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.android.lockated.R;
import com.android.lockated.categories.olacabs.activity.DividerItemDecoration;
import com.android.lockated.categories.olacabs.activity.OlaMainActivity_Latest;
import com.android.lockated.preferences.LockatedPreferences;


public class CheapestOlaRideFragment extends Fragment {   //implements ILocationUpdateListener, IGeocoderAddressUpdateListener, Response.ErrorListener, Response.Listener<String>, TextWatcher, View.OnClickListener

    public static RecyclerView mRecyclerView;
    public static ProgressBar progressBar;
    public static TextView mTextViewNoCFound;
    public static TextView cheap_fareText;
    public static LinearLayout cheap_view;
    private LockatedPreferences mLockatedPreferences;

    public CheapestOlaRideFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        mLockatedPreferences = new LockatedPreferences(getActivity());
        setHasOptionsMenu(true);
    }
   /* @Override
    public void onSaveInstanceState(Bundle outState) {
        *//*super.onSaveInstanceState(outState);*//*
    }*/

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        //Log.e("onCreateView of CheapestOlaRideFragment","Yes");
        //OlaMainActivity_Latest.mGoogleApiClient.connect();//removed comment on 16-8-2016
        if(OlaMainActivity_Latest.mGoogleApiClient !=null) {
            if (OlaMainActivity_Latest.mGoogleApiClient.isConnected()) {
                //Log.e("AvailMainFragment", "mGoogleApiClient is Connected");
            } else {
                //Log.e("AvailMainFragment", "mGoogleApiClient is Not Connected");
                OlaMainActivity_Latest.mGoogleApiClient.connect();
            }
        }

        OlaMainActivity_Latest.edt_olaSourceLocation.setFocusable(true);
        OlaMainActivity_Latest.edt_olaSourceLocation.setClickable(true);
        OlaMainActivity_Latest.edt_olaSourceLocation.setEnabled(true);

        super.onCreateView(inflater,container,savedInstanceState);
        View rootView = inflater.inflate(R.layout.fragment_cheapest_ola_ride, container, false);
        init(rootView);
        return rootView;
    }

    private void init(View view)
    {
        if (OlaMainActivity_Latest.gmap != null) {
            OlaMainActivity_Latest.gmap.getUiSettings().setScrollGesturesEnabled(true);
        }
        cheap_view  = (LinearLayout)view.findViewById(R.id.cheap_view);
        mTextViewNoCFound = (TextView) view.findViewById(R.id.mTextViewNoCFound);
        cheap_fareText = (TextView) view.findViewById(R.id.cheap_fareText);
        progressBar = (ProgressBar) view.findViewById(R.id.mProgressBarView_new);

        mRecyclerView = (RecyclerView) view.findViewById(R.id.mRecyclerView);
        mRecyclerView.addItemDecoration(new DividerItemDecoration(getActivity(),LinearLayoutManager.VERTICAL));
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        mRecyclerView.setLayoutManager(mLayoutManager);
        mRecyclerView.setHasFixedSize(true);
    }

    @Override
    public void onResume()
    {
        super.onResume();
    }
}
