
package com.android.lockated.model.Banner.Society;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class SocietyBanner {

    @SerializedName("society_banners")
    @Expose
    public ArrayList<SocietyBanner_all> societyBanners = new ArrayList<SocietyBanner_all>();

}
