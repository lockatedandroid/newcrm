package com.android.lockated.crm.fragment.events;


import android.Manifest;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.android.lockated.R;
import com.android.lockated.account.adapter.GroupListAdapter;
import com.android.lockated.crm.activity.AllEventsActivity;
import com.android.lockated.information.AccountController;
import com.android.lockated.model.AccountData;
import com.android.lockated.network.ConnectionDetector;
import com.android.lockated.preferences.LockatedPreferences;
import com.android.lockated.request.LockatedRequestError;
import com.android.lockated.request.LockatedVolleyRequestQueue;
import com.android.lockated.utils.ApplicationURL;
import com.android.lockated.utils.DatePickerFragment;
import com.android.lockated.utils.MarshMallowPermission;
import com.android.lockated.utils.ShowImage;
import com.android.lockated.utils.TimePickerFragment;
import com.android.lockated.utils.Utilities;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

public class CreateEventFragment extends Fragment implements View.OnClickListener,
        CompoundButton.OnCheckedChangeListener, RadioGroup.OnCheckedChangeListener,
        Response.ErrorListener, Response.Listener<JSONObject>, TextWatcher {

    FragmentManager fragmentManager;

    private View mFunctionEventView;
    private int RSVP = 0;
    private EditText mNewCategoryDescription;
    private EditText mEditLocation;
    private EditText SpinerFunctionCategory;
    private RadioGroup mRadioGroup;

    private RadioButton mSingleDayRadio;
    private RadioButton mMultipleDayRadio;
    private TextView mTextStartdate;

    private TextView mTextEndDate;
    private TextView mTextStartTime;
    private TextView mTextEndTime;
    private TextView mProceedButton;
    private TextView mPreviewButton;
    private TextView allMembers;
    private TextView groupMembers;
    private ImageView mIageStartDate;

    private ImageView mImageEndDate;
    private ImageView mImageStartTIme;
    private ImageView mImageView;
    private ImageView mImageAttachment;
    private ImageView mImageEndTime;
    private String selectedStartDate;

    private String selectedStartTime;
    private String selectedEndDate;
    private String selectedEndTime;

    private String mDescriptionString;
    private CheckBox mAskForCheckBox;
    private String EditLocationString;
    private String AskForCheckBox;
    private String ShareWithBlock;
    private String SelectCategory;
    private RequestQueue mQueue;
    /*private ProgressBar mProgressBar;*/
    private ProgressDialog mProgressDialog;
    private LockatedPreferences mLockatedPreferences;
    AccountController accountController;
    ArrayList<AccountData> accountDataArrayList;
    int societyId, groupIdValue = 0;
    boolean isSingleEvent = false;
    boolean imageSet = false;
    String encodedImage, mCurrentPhotoPath;
    public static final String REQUEST_TAG = "CreateEventFragment";
    static final int REQUEST_CAMERA_PHOTO = 3;
    static final int REQUEST_TAKE_PHOTO = 100;
    static final int CHOOSE_IMAGE_REQUEST = 101;
    static final int REQUEST_CAMERA = 102;
    static final int REQUEST_STORAGE = 103;
    MarshMallowPermission marshMallowPermission;
    GroupListAdapter groupListAdapter;
    public static final String GET_MY_GROUPS_TAG = "GetMyGroups";
    JSONObject jsonObjectEncodedImage;

    public FragmentManager setFragmentManager(FragmentManager fragmentManager) {
        this.fragmentManager = fragmentManager;
        return fragmentManager;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mFunctionEventView = inflater.inflate(R.layout.fragment_events_function, container, false);
        init();
        return mFunctionEventView;
    }

    private void init() {
        /*mProgressBar = (ProgressBar) mFunctionEventView.findViewById(R.id.mProgressBarView);*/
        mLockatedPreferences = new LockatedPreferences(getActivity());
        marshMallowPermission = new MarshMallowPermission(getActivity());
        jsonObjectEncodedImage = new JSONObject();
        mPreviewButton = (TextView) mFunctionEventView.findViewById(R.id.mPreviewButton);
        mProceedButton = (TextView) mFunctionEventView.findViewById(R.id.mProceedButton);
        allMembers = (TextView) mFunctionEventView.findViewById(R.id.allMembers);
        groupMembers = (TextView) mFunctionEventView.findViewById(R.id.groupMembers);
        mAskForCheckBox = (CheckBox) mFunctionEventView.findViewById(R.id.mAskForCheckBox);
        mIageStartDate = (ImageView) mFunctionEventView.findViewById(R.id.mIageStartDate);
        mImageEndDate = (ImageView) mFunctionEventView.findViewById(R.id.mImageEndDate);
        mImageStartTIme = (ImageView) mFunctionEventView.findViewById(R.id.mImageStartTIme);
        mImageEndTime = (ImageView) mFunctionEventView.findViewById(R.id.mImageEndTime);
        mImageView = (ImageView) mFunctionEventView.findViewById(R.id.mImageView);
        mImageAttachment = (ImageView) mFunctionEventView.findViewById(R.id.mImageAttachment);

        mEditLocation = (EditText) mFunctionEventView.findViewById(R.id.mEditLocation);
        SpinerFunctionCategory = (EditText) mFunctionEventView.findViewById(R.id.SpinerFunctionCategory);
        mNewCategoryDescription = (EditText) mFunctionEventView.findViewById(R.id.mNewCategoryDescription);

        mRadioGroup = (RadioGroup) mFunctionEventView.findViewById(R.id.mRadioGroup);
        mSingleDayRadio = (RadioButton) mFunctionEventView.findViewById(R.id.mSingleDayRadio);
        mMultipleDayRadio = (RadioButton) mFunctionEventView.findViewById(R.id.mMultipleDayRadio);

        mTextStartdate = (TextView) mFunctionEventView.findViewById(R.id.mTextStartdate);
        mTextEndDate = (TextView) mFunctionEventView.findViewById(R.id.mTextEndDate);
        mTextStartTime = (TextView) mFunctionEventView.findViewById(R.id.mTextStartTime);
        mTextEndTime = (TextView) mFunctionEventView.findViewById(R.id.mTextEndTime);

        disableSubmitButton();
        mRadioGroup.setOnCheckedChangeListener(this);
        mSingleDayRadio.setOnClickListener(this);
        mMultipleDayRadio.setOnClickListener(this);

        allMembers.setOnClickListener(this);
        groupMembers.setOnClickListener(this);
        mAskForCheckBox.setOnCheckedChangeListener(this);
        mIageStartDate.setOnClickListener(this);
        mImageEndDate.setOnClickListener(this);
        mImageStartTIme.setOnClickListener(this);
        mImageEndTime.setOnClickListener(this);
        mProceedButton.setOnClickListener(this);
        mPreviewButton.setOnClickListener(this);
        mImageView.setVisibility(View.GONE);
        mImageView.setOnClickListener(this);
        mImageAttachment.setOnClickListener(this);
        SpinerFunctionCategory.addTextChangedListener(this);
        mNewCategoryDescription.addTextChangedListener(this);
        mEditLocation.addTextChangedListener(this);

        if (mLockatedPreferences.getGroupList() != null && !mLockatedPreferences.getGroupList().equals("NoData")) {
            try {
                JSONObject jsonObject = new JSONObject(mLockatedPreferences.getGroupList());
                groupListAdapter = new GroupListAdapter(getActivity(), jsonObject.getJSONArray("usergroups"));
            } catch (JSONException e) {
                e.printStackTrace();
            }
        } else {
            callGroupApi();
        }

    }

    private void callGroupApi() {
        if (getActivity() != null) {

            if (ConnectionDetector.isConnectedToInternet(getActivity())) {
                mProgressDialog = ProgressDialog.show(getActivity(), "", "Please Wait...", false);
                mProgressDialog.show();
                /*mProgressBar.setVisibility(View.VISIBLE);*/
                String url = ApplicationURL.getMyGroups + mLockatedPreferences.getLockatedToken();
                LockatedVolleyRequestQueue lockatedVolleyRequestQueue = LockatedVolleyRequestQueue.getInstance(getActivity());
                lockatedVolleyRequestQueue.sendRequest(GET_MY_GROUPS_TAG, Request.Method.GET, url, null, this, this);
                /*RequestQueue mQueue = LockatedVolleyRequestQueue.getInstance(getActivity()).getRequestQueue();
                String url = ApplicationURL.getMyGroups + mLockatedPreferences.getLockatedToken();
                LockatedJSONObjectRequest lockatedJSONObjectRequest = new LockatedJSONObjectRequest(Request.Method.GET,
                        url, null, this, this);
                lockatedJSONObjectRequest.setTag(GET_MY_GROUPS_TAG);
                mQueue.add(lockatedJSONObjectRequest);*/
            } else {
                Utilities.showToastMessage(getActivity(), getActivity().getResources()
                        .getString(R.string.internet_connection_error));
            }
        }
    }

    private void disableSubmitButton() {
        mPreviewButton.setEnabled(false);
        mPreviewButton.setTextColor(ContextCompat.getColor(getActivity(), R.color.primary_text));
        mProceedButton.setEnabled(false);
        mProceedButton.setTextColor(ContextCompat.getColor(getActivity(), R.color.primary_text));

    }

    private void enableSubmitButton() {
        mPreviewButton.setEnabled(true);
        mPreviewButton.setTextColor(ContextCompat.getColor(getActivity(), R.color.white));
        mProceedButton.setEnabled(true);
        mProceedButton.setTextColor(ContextCompat.getColor(getActivity(), R.color.white));
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {

            case R.id.mIageStartDate:
                openDatePicker(mTextStartdate);
                break;
            case R.id.mImageEndDate:
                openDatePicker(mTextEndDate);
                break;
            case R.id.mImageStartTIme:
                openTimePicker(mTextStartTime);
                break;
            case R.id.mImageEndTime:
                openTimePicker(mTextEndTime);
                break;

            case R.id.mProceedButton:
                //whomeToSendAlertDialog(getActivity(), "Whome to send");
                onProceedClicked(groupIdValue);
                break;
            case R.id.mPreviewButton:
                onPreviewClicked();
                break;
            case R.id.mImageView:
                Intent displayImage = new Intent(getActivity(), ShowImage.class);
                displayImage.putExtra("imagePathString", mCurrentPhotoPath);
                getActivity().startActivity(displayImage);
                break;
            case R.id.mImageAttachment:
                if (!imageSet) {
                    selectImage();
                } else {
                    selectImageAction();
                }
                break;
            case R.id.allMembers:
                changeViewColor(allMembers, groupMembers);
                groupIdValue = 0;
                break;
            case R.id.groupMembers:
                changeViewColor(groupMembers, allMembers);
                groupMemberListAlertDialog(getActivity(), "Whome to send");
                break;

        }

    }

    private void changeViewColor(TextView textView1, TextView textView2) {
        textView1.setTextColor(this.getResources().getColor(R.color.white));
        textView1.setBackgroundColor(this.getResources().getColor(R.color.primary));
        textView2.setTextColor(this.getResources().getColor(R.color.primary));
        textView2.setBackgroundDrawable(this.getResources().getDrawable(R.drawable.border_card_layout));
    }

    private void groupMemberListAlertDialog(Context context, String groupName) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle(groupName);
        builder.setAdapter(groupListAdapter, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                try {
                    JSONObject jsonObject = new JSONObject(mLockatedPreferences.getGroupList());
                    String groupId = jsonObject.getJSONArray("usergroups").getJSONObject(which).getString("id");
                    //onProceedClicked(Integer.valueOf(groupId));
                    groupIdValue = Integer.valueOf(groupId);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
        AlertDialog dialog = builder.create();
        dialog.show();
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = 450;
        dialog.getWindow().setAttributes(lp);

    }

    private void selectImage() {
        final CharSequence[] options = {"Camera", "Gallery"};
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle("Add Attachment!");
        builder.setItems(options, new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int item) {
                if (options[item].equals("Camera")) {
                    checkCameraPermission();
                } else if (options[item].equals("Gallery")) {
                    checkStoragePermission();
                }
            }

        });

        builder.show();

    }

    private void selectImageAction() {
        final CharSequence[] options = {"View Image", "Change Image", "Remove Image"};
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setItems(options, new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int item) {
                if (options[item].equals("View Image")) {
                    Intent displayImage = new Intent(getActivity(), ShowImage.class);
                    displayImage.putExtra("imagePathString", mCurrentPhotoPath);
                    getActivity().startActivity(displayImage);
                } else if (options[item].equals("Change Image")) {
                    selectImage();
                } else {
                    mImageView.setImageBitmap(null);
                    encodedImage = "";
                    imageSet = false;
                }
            }

        });

        builder.show();

    }

    private void checkCameraPermission() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
                requestPermissions(new String[]{Manifest.permission.CAMERA}, REQUEST_CAMERA);
            } else {
                onCameraClicked();
            }
        } else {
            onCameraClicked();
        }
    }

    private void checkStoragePermission() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                requestPermissions(new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, REQUEST_STORAGE);
            } else {
                onGalleryClicked();
            }
        } else {
            onGalleryClicked();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        if (requestCode == REQUEST_CAMERA) {
            if (grantResults.length == 1 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                onCameraClicked();
            }
        } else if (requestCode == REQUEST_STORAGE) {
            if (grantResults.length == 1 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                onGalleryClicked();
            }
        }
    }

    private void onGalleryClicked() {
        Intent i = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(i, CHOOSE_IMAGE_REQUEST);
        imageViewVisible();
    }

    private File createImageFile() throws IOException {
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = "JPEG_" + timeStamp + "_";
        File storageDir = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES);
        File image = File.createTempFile(imageFileName, ".jpg", storageDir);
        mCurrentPhotoPath = image.getAbsolutePath();
        return image;
    }

    private void onCameraClicked() {
        imageViewVisible();
        if (!marshMallowPermission.checkPermissionForCamera()) {
            marshMallowPermission.requestPermissionForCamera();
        } else {
            if (!marshMallowPermission.checkPermissionForExternalStorage()) {
                marshMallowPermission.requestPermissionForExternalStorage();
            } else {
                Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                if (takePictureIntent.resolveActivity(getActivity().getPackageManager()) != null) {
                    File photoFile = null;
                    try {
                        photoFile = createImageFile();
                    } catch (IOException ex) {
                        ex.printStackTrace();
                    }

                    if (photoFile != null) {
                        takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(photoFile));
                        startActivityForResult(takePictureIntent, REQUEST_TAKE_PHOTO);
                    }
                }
            }
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode != getActivity().RESULT_CANCELED) {
            if (requestCode == REQUEST_TAKE_PHOTO && resultCode == getActivity().RESULT_OK) {
                setPic();
            } else if (requestCode == CHOOSE_IMAGE_REQUEST && resultCode == getActivity().RESULT_OK) {
                Uri selectedImageURI = data.getData();
                mCurrentPhotoPath = getPath(selectedImageURI);
                setPic();
            } else if (requestCode == REQUEST_CAMERA_PHOTO && resultCode == getActivity().RESULT_OK) {
                Bitmap thumbnail = (Bitmap) data.getExtras().get("data");
                if (thumbnail != null) {
                    ByteArrayOutputStream bytes = new ByteArrayOutputStream();
                    File destination = new File(Environment.getExternalStorageDirectory(), System.currentTimeMillis() + ".jpg");
                    FileOutputStream fo;
                    try {
                        destination.createNewFile();
                        fo = new FileOutputStream(destination);
                        fo.write(bytes.toByteArray());
                        fo.close();
                    } catch (FileNotFoundException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    imageViewVisible();
                    setCameraPic(thumbnail);
                }
            }
        }
    }

    private void setCameraPic(Bitmap bitmap) {
        if (bitmap != null) {
            encodedImage = Utilities.encodeTobase64(bitmap);
            try {
                jsonObjectEncodedImage.put("encodedImage", encodedImage);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            mImageView.setImageBitmap(bitmap);
            imageSet = true;
        }
    }

    private void setPic() {

        int targetW = mImageView.getWidth();
        int targetH = mImageView.getHeight();
        BitmapFactory.Options bmOptions = new BitmapFactory.Options();
        bmOptions.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(mCurrentPhotoPath, bmOptions);
        int photoW = bmOptions.outWidth;
        int photoH = bmOptions.outHeight;
        int scaleFactor = Math.min(photoW / targetW, photoH / targetH);

        bmOptions.inJustDecodeBounds = false;
        bmOptions.inSampleSize = scaleFactor;
        bmOptions.inPurgeable = true;

        Bitmap bitmap = BitmapFactory.decodeFile(mCurrentPhotoPath, bmOptions);
        if (bitmap != null) {
            encodedImage = Utilities.encodeTobase64(bitmap);
            try {
                jsonObjectEncodedImage.put("encodedImage", encodedImage);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            mImageView.setImageBitmap(bitmap);
            imageSet = true;
        }
    }

    private String getPath(Uri uri) {
        if (uri == null) {
            return null;
        }
        String[] projection = {MediaStore.Images.Media.DATA};
        Cursor cursor = getActivity().getContentResolver().query(uri, projection, null, null, null);
        if (cursor != null) {
            int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
            cursor.moveToFirst();
            return cursor.getString(column_index);
        }
        return uri.getPath();
    }

    private void openTimePicker(TextView selectDate) {
        TimePickerFragment timePickerFragment = new TimePickerFragment();
        timePickerFragment.setTimePickerView(selectDate);
        DialogFragment newFragment = timePickerFragment;
        newFragment.show(getActivity().getSupportFragmentManager(), "TimePicker");
    }

    private void openDatePicker(TextView selectTime) {
        DatePickerFragment datePickerFragment = new DatePickerFragment();
        datePickerFragment.setDatePickerView(selectTime);
        DialogFragment newFragment = datePickerFragment;
        newFragment.show(getActivity().getSupportFragmentManager(), "DatePicker");

    }

    private void onPreviewClicked() {
        mDescriptionString = mNewCategoryDescription.getText().toString();
        selectedStartDate = mTextStartdate.getText().toString();

        selectedStartTime = mTextStartTime.getText().toString();
        selectedEndTime = mTextEndTime.getText().toString();
        EditLocationString = mEditLocation.getText().toString();
        String StrCategory = SpinerFunctionCategory.getText().toString();

        if (isSingleEvent) {
            selectedEndDate = mTextStartdate.getText().toString();
        } else {
            selectedEndDate = mTextEndDate.getText().toString();
        }
        if (TextUtils.isEmpty(StrCategory)) {
            Utilities.showToastMessage(getContext(), "Please fill subject");
        } else if ((TextUtils.isEmpty(mDescriptionString))) {
            Utilities.showToastMessage(getContext(), "Please Fill Description");
        } else if (TextUtils.isEmpty(EditLocationString)) {
            Utilities.showToastMessage(getContext(), "Please Fill Location Details");
        } else if ((selectedStartDate.equals("Start Date"))) {
            Utilities.showToastMessage(getContext(), "Please select Start Date");

        } else if (selectedStartTime.equals("Start Time")) {
            Utilities.showToastMessage(getContext(), "Please select Start Time");

        } else if (selectedEndDate.equals("End Date")) {
            Utilities.showToastMessage(getContext(), "Please select End date");

        } else if (selectedEndTime.equals("End Time")) {
            Utilities.showToastMessage(getContext(), "Please select End Time");

        } else {
            EventPreview eventPreview = new EventPreview();
            Bundle args = new Bundle();
            args.putString("subject", StrCategory);
            args.putString("description", mDescriptionString);
            args.putString("Location", EditLocationString);
            args.putString("rsvp", "" + RSVP);
            args.putString("startdate", selectedStartDate);
            args.putString("enddate", selectedEndDate);
            args.putString("starttime", selectedStartTime);
            args.putString("endtime", selectedEndTime);
            eventPreview.setArguments(args);
            eventPreview.show(fragmentManager, "PreviewDialog");
        }
    }

    private void onProceedClicked(int groupId) {
        mDescriptionString = mNewCategoryDescription.getText().toString();

        selectedStartDate = mTextStartdate.getText().toString();
        selectedEndDate = mTextEndDate.getText().toString();

        selectedStartTime = mTextStartTime.getText().toString();
        selectedEndTime = mTextEndTime.getText().toString();

        EditLocationString = mEditLocation.getText().toString();
        String StrCategory = SpinerFunctionCategory.getText().toString();
        accountController = AccountController.getInstance();
        String Creatername = mLockatedPreferences.getPersonalName() + " " +
                mLockatedPreferences.getLastName();
        String strStartDateTime = mTextStartdate.getText().toString() + "T" + mTextStartTime.getText().toString();
        String strEndtDateTime;
        if (isSingleEvent) {
            selectedEndDate = mTextStartdate.getText().toString();
            strEndtDateTime = mTextStartdate.getText().toString() + "T" + mTextEndTime.getText().toString();
        } else {
            strEndtDateTime = mTextEndDate.getText().toString() + "T" + mTextEndTime.getText().toString();
        }
     /*   accountDataArrayList = accountController.getmAccountDataList();*/
        societyId = Integer.parseInt(mLockatedPreferences.getSocietyId());
        if (TextUtils.isEmpty(StrCategory)) {
            Utilities.showToastMessage(getContext(), "Please select Subject");
        } else if ((TextUtils.isEmpty(mDescriptionString))) {
            Utilities.showToastMessage(getContext(), "Please Fill Description");
        } else if (TextUtils.isEmpty(EditLocationString)) {
            Utilities.showToastMessage(getContext(), "Please Fill Location Details");
        } else if ((selectedStartDate.equals("Start Date"))) {
            Utilities.showToastMessage(getContext(), "Please select Start Date");

        } else if (selectedStartTime.equals("Start Time")) {
            Utilities.showToastMessage(getContext(), "Please select Start Time");

        } else if (selectedEndDate.equals("End Date")) {
            Utilities.showToastMessage(getContext(), "Please select End date");

        } else if (selectedEndTime.equals("End Time")) {
            Utilities.showToastMessage(getContext(), "Please select End Time");

        } else if ((selectedStartDate.equals(selectedEndDate)) && (selectedStartTime.equals(selectedEndTime))) {
            Utilities.showToastMessage(getContext(), "On same date start time and end time can not be same");
        } else {
            if (ConnectionDetector.isConnectedToInternet(getActivity())) {
                /*mProgressBar.setVisibility(View.VISIBLE);*/
                mProgressDialog = ProgressDialog.show(getActivity(), "", "Please Wait...", false);
                mProgressDialog.show();
                JSONObject jsonObjectMain = new JSONObject();
                JSONObject jsonObject = new JSONObject();
                try {
                    if (groupId == 0) {
                        if (encodedImage != null) {
                            jsonObject.put("created_by", Creatername);
                            jsonObject.put("id_society", societyId);
                            jsonObject.put("event_name", StrCategory);
                            jsonObject.put("event_at", EditLocationString);
                            jsonObject.put("event_type", "Function And Event");
                            jsonObject.put("from_time", strStartDateTime);
                            jsonObject.put("to_time", strEndtDateTime);
                            jsonObject.put("description", mDescriptionString);
                            jsonObject.put("shared", "" + groupId);
                            jsonObject.put("group_id", "" + groupId);
                            jsonObject.put("document", jsonObjectEncodedImage.getString("encodedImage"));
                            jsonObject.put("user_society_id", "" + mLockatedPreferences.getUserSocietyId());
                            jsonObjectMain.put("event", jsonObject);
                        } else {
                            jsonObject.put("created_by", Creatername);
                            jsonObject.put("id_society", societyId);
                            jsonObject.put("event_name", StrCategory);
                            jsonObject.put("event_at", EditLocationString);
                            jsonObject.put("event_type", "Function And Event");
                            jsonObject.put("from_time", strStartDateTime);
                            jsonObject.put("to_time", strEndtDateTime);
                            jsonObject.put("description", mDescriptionString);
                            jsonObject.put("shared", "" + groupId);
                            jsonObject.put("group_id", "" + groupId);
                            jsonObject.put("user_society_id", "" + mLockatedPreferences.getUserSocietyId());
                            jsonObjectMain.put("event", jsonObject);
                        }
                    } else {
                        if (encodedImage != null) {
                            jsonObject.put("created_by", Creatername);
                            jsonObject.put("id_society", societyId);
                            jsonObject.put("event_name", StrCategory);
                            jsonObject.put("event_at", EditLocationString);
                            jsonObject.put("event_type", "Function And Event");
                            jsonObject.put("from_time", strStartDateTime);
                            jsonObject.put("to_time", strEndtDateTime);
                            jsonObject.put("description", mDescriptionString);
                            jsonObject.put("shared", "1");
                            jsonObject.put("group_id", "" + groupId);
                            jsonObject.put("user_society_id", "" + mLockatedPreferences.getUserSocietyId());
                            jsonObject.put("document", jsonObjectEncodedImage.getString("encodedImage"));
                            jsonObjectMain.put("event", jsonObject);
                        } else {
                            jsonObject.put("created_by", Creatername);
                            jsonObject.put("id_society", societyId);
                            jsonObject.put("event_name", StrCategory);
                            jsonObject.put("event_at", EditLocationString);
                            jsonObject.put("event_type", "Function And Event");
                            jsonObject.put("from_time", strStartDateTime);
                            jsonObject.put("to_time", strEndtDateTime);
                            jsonObject.put("description", mDescriptionString);
                            jsonObject.put("user_society_id", "" + mLockatedPreferences.getUserSocietyId());
                            jsonObject.put("shared", "1");
                            jsonObject.put("group_id", "" + groupId);
                            jsonObjectMain.put("event", jsonObject);
                        }
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                try {
                    String url = ApplicationURL.addEventDetails + mLockatedPreferences.getLockatedToken();
                    LockatedVolleyRequestQueue lockatedVolleyRequestQueue = LockatedVolleyRequestQueue.getInstance(getActivity());
                    lockatedVolleyRequestQueue.sendRequest(REQUEST_TAG, Request.Method.POST, url, jsonObjectMain, this, this);
                    /*mQueue = LockatedVolleyRequestQueue.getInstance(getActivity()).getRequestQueue();
                    LockatedJSONObjectRequest lockatedJSONObjectRequest = new LockatedJSONObjectRequest(Request.Method.POST,
                            url, jsonObjectMain, this, this);
                    lockatedJSONObjectRequest.setTag(REQUEST_TAG);
                    mQueue.add(lockatedJSONObjectRequest);*/
                } catch (Exception e) {
                    e.printStackTrace();
                }

            } else {
                Utilities.showToastMessage(getActivity(), getActivity().getResources().getString(R.string.internet_connection_error));
            }
        }

    }

    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        if (isChecked) {
            RSVP = 1;
        } else
            RSVP = 0;
    }

    @Override
    public void onCheckedChanged(RadioGroup group, int checkedId) {
        if (checkedId == R.id.mSingleDayRadio) {
            mImageEndDate.setVisibility(View.INVISIBLE);
            mTextEndDate.setVisibility(View.INVISIBLE);
            isSingleEvent = true;
        }
        if (checkedId == R.id.mMultipleDayRadio) {
            mImageEndDate.setVisibility(View.VISIBLE);
            mTextEndDate.setVisibility(View.VISIBLE);
            isSingleEvent = false;
            mTextEndDate.setText("End Date");
        }
    }

    @Override
    public void onErrorResponse(VolleyError error) {
        /*mProgressBar.setVisibility(View.GONE);*/
        mProgressDialog.dismiss();
        if (getActivity() != null) {
            LockatedRequestError.onRequestError(getActivity(), error);
        }
    }

    @Override
    public void onResponse(JSONObject response) {
      /*  mProgressBar.setVisibility(View.GONE);*/
        mProgressDialog.dismiss();
        try {
            if (response.has("id")) {
                Intent intent = new Intent(getActivity(), AllEventsActivity.class);
                intent.putExtra("AllEventsActivity", 1);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                getActivity().startActivity(intent);
            } else if (response.has("usergroups") && response.getJSONArray("usergroups").length() > 0) {
                mLockatedPreferences.setGroupList(response);
                groupListAdapter = new GroupListAdapter(getActivity(), response.getJSONArray("usergroups"));
            }/* else {
                Utilities.showToastMessage(getActivity(), "Something went wrong");
            }*/
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {
        if (s.length() > 0) {
            if (SpinerFunctionCategory.getText().toString().length() > 0
                    && mNewCategoryDescription.getText().toString().length() > 0
                    && mEditLocation.getText().toString().length() > 0) {
                enableSubmitButton();
            }
        } else {
            disableSubmitButton();
        }
    }

    @Override
    public void afterTextChanged(Editable s) {

    }

    private void imageViewVisible() {
        if (mImageView.getVisibility() == View.GONE) {
            mImageView.setVisibility(View.VISIBLE);
        }
    }

    private void imageViewGone() {
        if (mImageView.getVisibility() == View.VISIBLE) {
            mImageView.setVisibility(View.GONE);
        }
    }
}
