package com.android.lockated.model;

import com.android.lockated.utils.ApplicationURL;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class LineItemData
{
    private int id;
    private int quantity;
    private String price;

    private int variantId;
    private String variantName;
    private String variantPrice;
    private String variantWeight;
    private String variantCostPrice;
    private String variantImageUrl;

    public LineItemData(JSONObject jsonObject)
    {
        try
        {
            id = jsonObject.optInt("id");
            quantity = jsonObject.optInt("quantity");
            price = jsonObject.optString("price");

            JSONObject variantObject = new JSONObject(jsonObject.optString("variant"));
            variantId = variantObject.optInt("id");
            variantName = variantObject.optString("name");
            variantPrice = variantObject.optString("price");
            variantWeight = variantObject.optString("weight");
            variantCostPrice = variantObject.optString("cost_price");

            JSONArray imagesJsonArray = variantObject.getJSONArray("images");
            for (int i = 0; i < imagesJsonArray.length(); i++)
            {
                JSONObject imageObject = imagesJsonArray.getJSONObject(i);
                variantImageUrl = ApplicationURL.ImageBaseURL+imageObject.optString("small_url");
            }
        }
        catch (JSONException e)
        {
            e.printStackTrace();
        }
    }

    public int getId()
    {
        return id;
    }

    public int getQuantity()
    {
        return quantity;
    }

    public String getPrice()
    {
        return price;
    }

    public int getVariantId()
    {
        return variantId;
    }

    public String getVariantName()
    {
        return variantName;
    }

    public String getVariantPrice()
    {
        return variantPrice;
    }

    public String getVariantWeight()
    {
        return variantWeight;
    }

    public String getVariantCostPrice()
    {
        return variantCostPrice;
    }

    public String getVariantImageUrl()
    {
        return variantImageUrl;
    }

    public void setQuantity(int quantity)
    {
        this.quantity = quantity;
    }
}
