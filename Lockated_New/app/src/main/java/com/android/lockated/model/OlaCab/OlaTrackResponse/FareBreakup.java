
package com.android.lockated.model.OlaCab.OlaTrackResponse;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class FareBreakup {

    @SerializedName("display_text")
    @Expose
    private String displayText;
    @SerializedName("value")
    @Expose
    private float value;

    /**
     * 
     * @return
     *     The displayText
     */
    public String getDisplayText() {
        return displayText;
    }

    /**
     * 
     * @param displayText
     *     The display_text
     */
    public void setDisplayText(String displayText) {
        this.displayText = displayText;
    }

    /**
     * 
     * @return
     *     The value
     */
    public float getValue() {
        return value;
    }

    /**
     * 
     * @param value
     *     The value
     */
    public void setValue(float value) {
        this.value = value;
    }

}
