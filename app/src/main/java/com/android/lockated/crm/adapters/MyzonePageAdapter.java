package com.android.lockated.crm.adapters;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.android.lockated.crm.fragment.myzone.DirectorysFragment;
import com.android.lockated.crm.fragment.myzone.FacilitiesFragment;


public class MyzonePageAdapter extends FragmentPagerAdapter {
    int tabCount, itemPosition;
    FragmentManager fragmentManager;
    String[] nameList;

    public MyzonePageAdapter(FragmentManager fm, int tabCount, int itemPosition, String[] nameList) {
        super(fm);
        this.tabCount = tabCount;
        fragmentManager = fm;
        this.itemPosition = itemPosition;
        this.nameList = nameList;
    }


    @Override
    public Fragment getItem(int pos) {

        Fragment fragment;

        switch (pos) {

            /*case 0:
                fragment = new VisitorCornerFragment();
                break;*/
            case 0:
                fragment = new FacilitiesFragment();
                break;
            case 1:
                fragment = new DirectorysFragment();
                break;
          /*  case 3:
                fragment = new ClassifidesFragment();
                break;*/
            default:
                fragment = new FacilitiesFragment();
                break;
        }
        return fragment;
    }

    @Override
    public int getCount() {
        return tabCount;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        //return super.getPageTitle(position);
        return nameList[itemPosition];
    }

    @Override
    public int getItemPosition(Object object) {
        return super.getItemPosition(object);
    }

    @Override
    public long getItemId(int position) {
        return super.getItemId(position);
    }

}

