package com.android.lockated.crm.adapters;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.android.lockated.crm.fragment.notices.MyNoticeFragment;
import com.android.lockated.crm.fragment.notices.NoticeFragment;


public class AllNoticesPagerAdapter extends FragmentPagerAdapter {
    int tabCount, itemPosition;
    FragmentManager fragmentManager;
    String[] nameList;

    public AllNoticesPagerAdapter(FragmentManager fm, int tabCount, int itemPosition, String[] nameList) {
        super(fm);
        this.tabCount = tabCount;
        fragmentManager = fm;
        this.itemPosition = itemPosition;
        this.nameList = nameList;
    }


    @Override
    public Fragment getItem(int pos) {

        Fragment fragment;

        switch (pos) {

            case 0:
                fragment = new NoticeFragment();
                break;
            case 1:
                fragment = new MyNoticeFragment();
                break;
            default:
                fragment = new NoticeFragment();
                break;
        }
        return fragment;
    }

    @Override
    public int getCount() {
        return tabCount;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        //return super.getPageTitle(position);
        return nameList[itemPosition];
    }

    @Override
    public int getItemPosition(Object object) {
        return super.getItemPosition(object);
    }

    @Override
    public long getItemId(int position) {
        return super.getItemId(position);
    }

}