package com.android.lockated.request;

import android.annotation.SuppressLint;
import android.content.Context;
import android.util.Log;

import com.android.lockated.utils.Utilities;
import com.android.volley.AuthFailureError;
import com.android.volley.Cache;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Network;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.toolbox.BasicNetwork;
import com.android.volley.toolbox.DiskBasedCache;
import com.android.volley.toolbox.HurlStack;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

public class LockatedVolleyRequestQueue {
    private static LockatedVolleyRequestQueue mInstance;
    private static Context mContext;
    private RequestQueue mRequestQueue;
    private ImageLoader mImageLoader;

    private LockatedJSONObjectRequest mLockatedJSONObjectRequest;

    public static final String REQUEST_TAG = "API-REQUEST";
    RequestQueue requestQueue;

    private LockatedVolleyRequestQueue(Context context) {
        mContext = context;
        mRequestQueue = getRequestQueue();
    }

    public static synchronized LockatedVolleyRequestQueue getInstance(Context context) {
        if (mInstance == null) {
            mInstance = new LockatedVolleyRequestQueue(context);
        }
        return mInstance;
    }

    public void sendRequest(String tag, int method, String url, JSONObject jsonObject,
                            Response.Listener<JSONObject> listener, Response.ErrorListener errorListener) {
        int socketTimeout = Utilities.REQUEST_TIME_OUT;
        requestQueue = mInstance.getRequestQueue();
        mLockatedJSONObjectRequest = new LockatedJSONObjectRequest(method, url, jsonObject, listener, errorListener);
        mLockatedJSONObjectRequest.setTag(tag);
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        mLockatedJSONObjectRequest.setRetryPolicy(policy);
        Log.e("Time Out",""+policy.getCurrentTimeout());
        requestQueue.add(mLockatedJSONObjectRequest);
    }

    public void sendRequest(String tag, int method, String url, JSONObject jsonObject,
                            Response.Listener<String> listener, Response.ErrorListener errorListener, final String message) {
        int socketTimeout = Utilities.REQUEST_TIME_OUT;
        requestQueue = mInstance.getRequestQueue();
        StringRequest sr = new StringRequest(method, url, listener, errorListener) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("msg", message);
                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("Content-Type", "application/x-www-form-urlencoded");
                return params;
            }
        };
        sr.setTag(tag);
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        sr.setRetryPolicy(policy);
        requestQueue.add(sr);
    }

    public void sendGetRequestOlaCabs(String tag, int method, String url, Response.Listener<String> listener, Response.ErrorListener errorListener) {
        int socketTimeout = Utilities.REQUEST_TIME_OUT;
        Log.e("sendGetRequestOlaCabs", "m inside");
        requestQueue = mInstance.getRequestQueue();
        StringRequest sr = new StringRequest(method, url, listener, errorListener) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("Content-Type", "application/json");
                params.put("X-APP-TOKEN","8b65e672a0ab4d6384ef6e11c00e5d39");//LockatedConfig.OLACABS_API_KEY
                return params;
            }
        };
        sr.setTag(tag);
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        sr.setRetryPolicy(policy);
        requestQueue.add(sr);
    }

    @SuppressLint("LongLogTag")
    public void sendPostRequestOlaCabsConfirm(String tag, int method, String url, final String access_token, JSONObject jsonObject, Response.Listener<JSONObject> listener, Response.ErrorListener errorListener) {
        int socketTimeout = Utilities.REQUEST_TIME_OUT;
        Log.e("sendRequestOlaCabs", "m inside");
        requestQueue = mInstance.getRequestQueue();
        try {
            JSONObject jsonObject1 = jsonObject.getJSONObject("body");
            jsonObject1.get("pickup_lat");
            jsonObject1.get("pickup_lng");
            jsonObject1.get("pickup_mode");
            jsonObject1.get("category");

            Map<String, Object> params = new HashMap<>();
            params.put("pickup_lat", jsonObject1.get("pickup_lat"));
            params.put("pickup_lng", jsonObject1.get("pickup_lng"));
            params.put("pickup_mode", jsonObject1.get("pickup_mode"));
            params.put("category", jsonObject1.get("category"));


            Map<String, Object> params_body = new HashMap<>();
            params_body.put("body", params);

            JsonObjectRequest sr = new JsonObjectRequest(method, url, new JSONObject(params_body), listener, errorListener) {
                @Override
                protected Map<String, String> getParams() {
                    return new HashMap<>();
                }

                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String, String> params = new HashMap<>();
                    params.put("Authorization", "Bearer " + access_token);
                    params.put("X-APP-TOKEN", "8b65e672a0ab4d6384ef6e11c00e5d39");   //LockatedConfig.OLACABS_API_KEY
                    params.put("Content-Type", "application/json");
                    return params;
                }
            };
            sr.setTag(tag);
            RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
            sr.setRetryPolicy(policy);
            requestQueue.add(sr);


            Log.e("sendPostRequestOlaCabsConfirm", jsonObject1.get("pickup_lat") + " ," + jsonObject1.get("pickup_lng") + " ," + jsonObject1.get("pickup_mode") + " ," + jsonObject1.get("category"));
        } catch (JSONException e) {
            e.printStackTrace();
        }


    }


    public void sendPostRequestOlaCabsConfirmEx(String tag, int method, String url, final String access_token, final JSONObject jsonObject, Response.Listener<JSONObject> listener, Response.ErrorListener errorListener) {
        int socketTimeout = Utilities.REQUEST_TIME_OUT;
        requestQueue = mInstance.getRequestQueue();
        JsonObjectRequest sr = new JsonObjectRequest(method, url, jsonObject, listener, errorListener) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                return params;
            }

            @Override
            public byte[] getBody() {
                Log.e("jsonObject", "" + jsonObject.toString());
                return jsonObject.toString().getBytes();
            }

            @Override
            public String getBodyContentType() {
                return "application/json";
            } //"application/x-www-form-urlencoded"

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("Authorization", "Bearer " + access_token);
                params.put("X-APP-TOKEN","8b65e672a0ab4d6384ef6e11c00e5d39" ); //LockatedConfig.OLACABS_API_KEY
                params.put("Content-Type", "application/json");
                Log.e("getHeaders", "" + params.toString());
                return params;
            }
        };
        sr.setTag(tag);
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        sr.setRetryPolicy(policy);
        requestQueue.add(sr);
    }

    public void sendPostRequestLocated(String tag, int method, String url,final JSONObject jsonObject, Response.Listener<JSONObject> listener, Response.ErrorListener errorListener) {
        int socketTimeout = Utilities.REQUEST_TIME_OUT;
        requestQueue = mInstance.getRequestQueue();
        JsonObjectRequest sr = new JsonObjectRequest(method, url, jsonObject, listener, errorListener) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                return params;
            }

            @Override
            public byte[] getBody() {
                Log.e("jsonObject", "" + jsonObject.toString());
                return jsonObject.toString().getBytes();
            }

            @Override
            public String getBodyContentType() {
                return "application/x-www-form-urlencoded";
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("Content-Type", "application/json");
                Log.e("getHeaders", "" + params.toString());
                return params;
            }
        };
        sr.setTag(tag);
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        sr.setRetryPolicy(policy);
        requestQueue.add(sr);
    }


    public void sendZomatoRequest(String tag, int method, String url, final JSONObject jsonObject, Response.Listener<JSONObject> listener, Response.ErrorListener errorListener) {
        int socketTimeout = Utilities.REQUEST_TIME_OUT;
        requestQueue = mInstance.getRequestQueue();
        if(jsonObject != null)
        {
            Log.e("jsonObject",""+jsonObject.toString());
        }

        deleteCache(mContext);
        JsonObjectRequest sr = new JsonObjectRequest(method, url, jsonObject, listener, errorListener) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                return params;
            }

            @Override
            public byte[] getBody() {
                Log.e("jsonObject", "" + jsonObject.toString());
                return jsonObject.toString().getBytes();
            }

            @Override
            public String getBodyContentType() {
                return "application/x-www-form-urlencoded";
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("Content-Type", "application/json");
                Log.e("getHeaders", "" + params.toString());
                return params;
            }
        };
        sr.setTag(tag);
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        sr.setRetryPolicy(policy);
        requestQueue.add(sr);
    }

    public RequestQueue getRequestQueue() {
        if (mRequestQueue == null) {
            Cache cache = new DiskBasedCache(mContext.getCacheDir(), 10 * 1024 * 1024);
            Network network = new BasicNetwork(new HurlStack());
            mRequestQueue = new RequestQueue(cache, network);
            mRequestQueue.start();
        }
        return mRequestQueue;
    }

    public ImageLoader getImageLoader() {
        getRequestQueue();
        if (mImageLoader == null) {
            mImageLoader = new ImageLoader(this.mRequestQueue, new LruBitmapCache());
        }
        return this.mImageLoader;
    }

    public LockatedJSONObjectRequest getLockatedJSONObjectRequest() {
        return mLockatedJSONObjectRequest;
    }



    //-------------------------Delete Application Cache Data-------------------------


    public static void deleteCache(Context context) {
        try {
            File dir = context.getCacheDir();
            deleteDir(dir);
        } catch (Exception e) {}
    }

    public static boolean deleteDir(File dir) {
        if (dir != null && dir.isDirectory()) {
            String[] children = dir.list();
            for (int i = 0; i < children.length; i++) {
                boolean success = deleteDir(new File(dir, children[i]));
                if (!success) {
                    return false;
                }
            }
            return dir.delete();
        }
        else if(dir!= null && dir.isFile())
            return dir.delete();
        else {
            return false;
        }
    }

    ///---------------------------------------------------------------------------------
}
