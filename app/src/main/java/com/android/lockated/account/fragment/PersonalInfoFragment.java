package com.android.lockated.account.fragment;

import android.Manifest;
import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.lockated.LockatedApplication;
import com.android.lockated.R;
import com.android.lockated.account.activity.EditPersonalDetailActivity;
import com.android.lockated.information.AccountController;
import com.android.lockated.model.AccountData;
import com.android.lockated.network.ConnectionDetector;
import com.android.lockated.preferences.LockatedPreferences;
import com.android.lockated.register.ChangePasswordActivity;
import com.android.lockated.request.LockatedJSONObjectRequest;
import com.android.lockated.request.LockatedRequestError;
import com.android.lockated.request.LockatedVolleyRequestQueue;
import com.android.lockated.utils.ApplicationURL;
import com.android.lockated.utils.MarshMallowPermission;
import com.android.lockated.utils.ShowImage;
import com.android.lockated.utils.Utilities;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

public class PersonalInfoFragment extends Fragment implements View.OnClickListener, Response.ErrorListener, Response.Listener<JSONObject> {
    private View mPersonalView;
    TextView mTextFirstName, mTextLastName;
    TextView mTextPersonalNunber;
    TextView mTextPersonalEmail;
    TextView mTextChangePassword;
    TextView mTextPersonalLocation;
    ImageView mImageViewEdit;
    ImageView mImageView;
    MarshMallowPermission marshMallowPermission;
    ArrayList<AccountData> accountDataArrayList;
    AccountController accountController;
    private LockatedVolleyRequestQueue mLockatedVolleyRequestQueue;
    public static final String REQUEST_OTP = "ForgotPassword";
    private LockatedPreferences mLockatedPreferences;
    private RequestQueue mQueue;
    ProgressBar progressBar;
    int RESULT_OK = 1;
    //boolean imageSet = false;
    boolean imageSet;
    private String mCurrentPhotoPath, encodedImage;
    private static final int REQUEST_GALLERY = 4;
    static final int REQUEST_CAMERA_PHOTO = 3;
    static final int REQUEST_TAKE_PHOTO = 100;
    static final int CHOOSE_IMAGE_REQUEST = 101;
    static final int REQUEST_CAMERA = 102;
    static final int REQUEST_STORAGE = 103;
    private final int RESULT_CROP = 400;
    public static final String REQUEST_TAG = "PersonalInfoFragment";

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mPersonalView = inflater.inflate(R.layout.fragment_personal, container, false);
        marshMallowPermission = new MarshMallowPermission(getActivity());
        init();
        Utilities.ladooIntegration(getActivity(), getActivity().getString(R.string.personal_info_screen));
        return mPersonalView;
    }

    @Override
    public void onResume() {
        super.onResume();
        LockatedApplication.getInstance().trackScreenView(getActivity().getString(R.string.personal_info_screen));
        // mGetPrefrencesPersonalData();
    }

    private void mGetPrefrencesPersonalData() {
        mTextFirstName.setText(mLockatedPreferences.getPersonalName());
        mTextLastName.setText(mLockatedPreferences.getLastName());
        mTextPersonalNunber.setText(mLockatedPreferences.getContactNumber());
        mTextPersonalEmail.setText(mLockatedPreferences.getEmailAddress());
        Picasso.with(getActivity())
                .load(mLockatedPreferences.getImageAvatar())
                .fit().centerInside()
                .into(mImageView);
    }

    private void init() {
        mLockatedPreferences = new LockatedPreferences(getActivity());
        progressBar = (ProgressBar) mPersonalView.findViewById(R.id.mProgressBarView);
        mTextFirstName = (TextView) mPersonalView.findViewById(R.id.mTextFirstName);
        mTextLastName = (TextView) mPersonalView.findViewById(R.id.mTextLastName);
        mTextPersonalNunber = (TextView) mPersonalView.findViewById(R.id.mTextPersonalNunber);
        mTextPersonalEmail = (TextView) mPersonalView.findViewById(R.id.mTextPersonalEmail);
        mTextPersonalLocation = (TextView) mPersonalView.findViewById(R.id.mTextPersonalLocation);
        mTextChangePassword = (TextView) mPersonalView.findViewById(R.id.mTextChangePassword);
        mImageViewEdit = (ImageView) mPersonalView.findViewById(R.id.mImageViewEdit);
        mImageView = (ImageView) mPersonalView.findViewById(R.id.mImageView);
        getPersonalInformation();
        mTextChangePassword.setOnClickListener(this);
        mImageView.setOnClickListener(this);
        mImageViewEdit.setOnClickListener(this);
    }

    private void getPersonalInformation() {
        progressBar.setVisibility(View.VISIBLE);
        if (ConnectionDetector.isConnectedToInternet(getActivity())) {
            String url = ApplicationURL.getPersonalInformation + mLockatedPreferences.getLockatedToken();
            LockatedVolleyRequestQueue mLockatedVolleyRequestQueue = LockatedVolleyRequestQueue.getInstance(getActivity());
            mLockatedVolleyRequestQueue.sendRequest(REQUEST_TAG, Request.Method.GET, url, null, this, this);
        } else {
            Utilities.showToastMessage(getActivity(), getActivity().getResources().getString(R.string.internet_connection_error));
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.mTextChangePassword:
                onChangePasswordClicked();
                break;
            case R.id.mImageViewEdit:
                callIntent();
                break;
            case R.id.mImageView:
                if (!imageSet) {
                    selectImage();
                } else {
                    selectImageAction();
                }
                break;
        }
    }

    private void selectImageAction() {
        final CharSequence[] options = {"View Image", "Change Image"/*, "Remove Image"*/};
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setItems(options, new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int item) {
                if (options[item].equals("View Image")) {
                    Intent displayImage = new Intent(getActivity(), ShowImage.class);
                    String mImageUrl = mLockatedPreferences.getImageAvatar();
                    displayImage.putExtra("imageUrlString", mImageUrl);
                    getActivity().startActivity(displayImage);
                } else if (options[item].equals("Change Image")) {
                    selectImage();
                } else {
                    mImageView.setImageResource(R.drawable.ic_account_camera);
                    encodedImage = "";
                    imageSet = false;
                }
            }

        });

        builder.show();

    }

    private void callIntent() {
        mLockatedPreferences.setPersonalName(mTextFirstName.getText().toString());
        mLockatedPreferences.setLastName(mTextLastName.getText().toString());
        mLockatedPreferences.setEmailAddress(mTextPersonalEmail.getText().toString());
        mLockatedPreferences.setContactNumber(mTextPersonalNunber.getText().toString());
        Intent intent = new Intent(getActivity(), EditPersonalDetailActivity.class);
        startActivity(intent);
    }

    private void selectImage() {
        final CharSequence[] options = {"Camera", "Gallery"};
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle("Add Photo!");
        builder.setItems(options, new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int item) {
                if (options[item].equals("Camera")) {
                    checkCameraPermission();
                } else if (options[item].equals("Gallery")) {
                    checkStoragePermission();
                }
            }

        });

        builder.show();
    }

    private void checkStoragePermission() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                requestPermissions(new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, REQUEST_STORAGE);
            } else {
                onGalleryClicked();
            }
        } else {
            onGalleryClicked();
        }
    }

    private void onGalleryClicked() {
        Intent i = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(i, CHOOSE_IMAGE_REQUEST);
    }

    private void checkCameraPermission() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
                requestPermissions(new String[]{Manifest.permission.CAMERA}, REQUEST_CAMERA);
            } else {
                onCameraClicked();
            }
        } else {
            onCameraClicked();
        }
    }

    private void onCameraClicked() {
        if (!marshMallowPermission.checkPermissionForCamera()) {
            marshMallowPermission.requestPermissionForCamera();
        } else {
            if (!marshMallowPermission.checkPermissionForExternalStorage()) {
                marshMallowPermission.requestPermissionForExternalStorage();
            } else {
                Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                if (takePictureIntent.resolveActivity(getActivity().getPackageManager()) != null) {
                    File photoFile = null;
                    try {
                        photoFile = createImageFile();
                    } catch (IOException ex) {
                        ex.printStackTrace();
                    }

                    if (photoFile != null) {
                        takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(photoFile));
                        startActivityForResult(takePictureIntent, REQUEST_TAKE_PHOTO);
                    }
                }
            }
        }
    }

    private File createImageFile() throws IOException {
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss", Locale.ENGLISH).format(new Date());
        String imageFileName = "JPEG_" + timeStamp + "_";
        File storageDir = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES);
        File image = File.createTempFile(imageFileName, ".jpg", storageDir);
        mCurrentPhotoPath = image.getAbsolutePath();
        return image;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode != getActivity().RESULT_CANCELED) {
            if (requestCode == REQUEST_TAKE_PHOTO && resultCode == getActivity().RESULT_OK) {
                performCrop(mCurrentPhotoPath);
                //  setPic();
            } else if (requestCode == CHOOSE_IMAGE_REQUEST && resultCode == getActivity().RESULT_OK) {
                Uri selectedImageURI = data.getData();
                mCurrentPhotoPath = getPath(selectedImageURI);
                performCrop(mCurrentPhotoPath);
                // setPic();
            } else if (requestCode == REQUEST_CAMERA_PHOTO && resultCode == getActivity().RESULT_OK) {
                Bitmap thumbnail = (Bitmap) data.getExtras().get("data");
                if (thumbnail != null) {
                    ByteArrayOutputStream bytes = new ByteArrayOutputStream();
                    File destination = new File(Environment.getExternalStorageDirectory(), System.currentTimeMillis() + ".jpg");
                    FileOutputStream fo;
                    try {
                        destination.createNewFile();
                        fo = new FileOutputStream(destination);
                        fo.write(bytes.toByteArray());
                        fo.close();
                    } catch (FileNotFoundException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                    setCameraPic(thumbnail);

                }
            } else if (requestCode == RESULT_CROP) {
                if (resultCode == Activity.RESULT_OK) {
                    Bundle extras = data.getExtras();
                    Bitmap selectedBitmap = extras.getParcelable("data");
                    if (selectedBitmap != null) {
                        encodedImage = Utilities.encodeTobase64(selectedBitmap);
                        mImageView.setImageBitmap(selectedBitmap);
                        mImageView.setScaleType(ImageView.ScaleType.FIT_XY);
                        imageSet = true;
                        onPostPersonalImage();
                    }
                    /*mImageView.setImageBitmap(selectedBitmap);
                    mImageView.setScaleType(ImageView.ScaleType.FIT_XY);*/
                }
            }
        }
    }

    private void performCrop(String picUri) {
        try {
            //Start Crop Activity
            Intent cropIntent = new Intent("com.android.camera.action.CROP");
            // indicate image type and Uri
            File f = new File(picUri);
            Uri contentUri = Uri.fromFile(f);
           /* cropIntent.setDataAndType(contentUri, "image*//*");*/
            cropIntent.setDataAndType(contentUri, "image/*");
            // set crop properties
            cropIntent.putExtra("crop", "true");
            // indicate aspect of desired crop
            cropIntent.putExtra("aspectX", 1);
            cropIntent.putExtra("aspectY", 1);
            // indicate output X and Y
            cropIntent.putExtra("outputX", 280);
            cropIntent.putExtra("outputY", 280);
            // retrieve data on return
            cropIntent.putExtra("return-data", true);
            // start the activity - we handle returning in onActivityResult
            startActivityForResult(cropIntent, RESULT_CROP);
        }
        // respond to users whose devices do not support the crop action
        catch (ActivityNotFoundException anfe) {
            // display an error message
            String errorMessage = "your device doesn't support the crop action!";
            Toast toast = Toast.makeText(getContext(), errorMessage, Toast.LENGTH_SHORT);
            toast.show();
        }

    }

    private String getPath(Uri uri) {
        if (uri == null) {
            return null;
        }
        String[] projection = {MediaStore.Images.Media.DATA};
        Cursor cursor = getActivity().getContentResolver().query(uri, projection, null, null, null);
        if (cursor != null) {
            int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
            cursor.moveToFirst();
            return cursor.getString(column_index);
        }
        return uri.getPath();
    }

    private void setCameraPic(Bitmap bitmap) {
        if (bitmap != null) {
            encodedImage = Utilities.encodeTobase64(bitmap);
            /*Log.w("setCameraPic", encodedImage);*/
            mImageView.setImageBitmap(bitmap);
            imageSet = true;
            onPostPersonalImage();
        }
    }
/*

    private void setPic() {
        int targetW = mImageView.getWidth();
        int targetH = mImageView.getHeight();
        BitmapFactory.Options bmOptions = new BitmapFactory.Options();
        bmOptions.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(mCurrentPhotoPath, bmOptions);
        int photoW = bmOptions.outWidth;
        int photoH = bmOptions.outHeight;
        int scaleFactor = Math.min(photoW / targetW, photoH / targetH);

        bmOptions.inJustDecodeBounds = false;
        bmOptions.inSampleSize = scaleFactor;
        bmOptions.inPurgeable = true;

        Bitmap bitmap = BitmapFactory.decodeFile(mCurrentPhotoPath, bmOptions);
        performCrop(mCurrentPhotoPath);
        */
/* if (bitmap != null) {
            encodedImage = Utilities.encodeTobase64(bitmap);
            Log.e("mCurrentPhotoPath",""+mCurrentPhotoPath);
            Log.w("setPic", encodedImage);
            mImageView.setImageBitmap(bitmap);
            imageSet = true;
            onPostPersonalImage();
        }*//*

    }
*/

    private void onPostPersonalImage() {
        progressBar.setVisibility(View.VISIBLE);
        if (ConnectionDetector.isConnectedToInternet(getActivity())) {
            JSONObject jsonObject = new JSONObject();
            JSONObject jsonObjectMain = new JSONObject();
            try {
                jsonObject.put("avatar", encodedImage);
                jsonObjectMain.put("user", jsonObject);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            try {
                String url = ApplicationURL.updatePersonalInformatio + mLockatedPreferences.getLockatedToken();
                mQueue = LockatedVolleyRequestQueue.getInstance(getActivity()).getRequestQueue();
                LockatedJSONObjectRequest lockatedJSONObjectRequest = new LockatedJSONObjectRequest(Request.Method.PUT,
                        url, jsonObjectMain, this, this);
                lockatedJSONObjectRequest.setTag(REQUEST_TAG);
                mQueue.add(lockatedJSONObjectRequest);
            } catch (Exception e) {
                e.printStackTrace();
            }

        } else {
            Utilities.showToastMessage(getActivity(), getActivity().getResources().getString(R.string.internet_connection_error));
        }
    }

    private void onChangePasswordClicked() {
        progressBar.setVisibility(View.VISIBLE);
        accountController = AccountController.getInstance();
        accountDataArrayList = accountController.getmAccountDataList();
        String strUserPhone = "" + accountDataArrayList.get(0).getMobile();
        if (ConnectionDetector.isConnectedToInternet(getActivity())) {
            JSONObject jsonObjectFields = new JSONObject();
            try {
                jsonObjectFields.put("request_otp", "1");
                jsonObjectFields.put("mobile", strUserPhone);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            String url = ApplicationURL.getForgotPasswordOtp;
            mLockatedVolleyRequestQueue = LockatedVolleyRequestQueue.getInstance(getActivity());
            mLockatedVolleyRequestQueue.sendRequest(REQUEST_OTP, Request.Method.POST, url, jsonObjectFields, this, this);
            Utilities.lockatedGoogleAnalytics(getString(R.string.login), getString(R.string.login), getString(R.string.login));
        } else {
            Utilities.showToastMessage(getActivity(), getActivity().getResources().getString(R.string.internet_connection_error));
        }

    }

    @Override
    public void onErrorResponse(VolleyError error) {
        progressBar.setVisibility(View.GONE);
        if (getActivity() != null) {
            LockatedRequestError.onRequestError(getActivity(), error);
        }
    }

    @Override
    public void onResponse(JSONObject response) {
        progressBar.setVisibility(View.GONE);
        if (getActivity() != null) {
            JSONObject jsonObject = (JSONObject) response;
            try {
                if (jsonObject.has("id") && jsonObject.length() > 0) {
                    if (jsonObject.has("url") && jsonObject.getString("url") != null) {
                        String mImage = jsonObject.getString("url");
                        mLockatedPreferences.setImageAvatar(mImage);
                        Picasso.with(getActivity())
                                .load(mImage).resize(180, 180)
                                .into(mImageView);
                        imageSet = true;

                    } else {
                        if (jsonObject.has("avatar") && jsonObject.getJSONObject("avatar").getString("url") != null) {
                            String mAvatar = jsonObject.getJSONObject("avatar").getString("url");
                            mLockatedPreferences.setImageAvatar(mAvatar);
                            Picasso.with(getActivity())
                                    .load(mAvatar).resize(180, 180)
                                    .into(mImageView);
                            imageSet = true;
                        } else {
                            imageSet = false;
                        }
                    }
                    String fname = jsonObject.getString("firstname");
                    String lname = jsonObject.getString("lastname");
                    String email = jsonObject.getString("email");
                    String mobile = jsonObject.getString("mobile");
                    mTextFirstName.setText(fname);
                    mTextLastName.setText(lname);
                    mTextPersonalEmail.setText(email);
                    mTextPersonalNunber.setText(mobile);

                    mLockatedPreferences.setPersonalName(fname);
                    mLockatedPreferences.setLastName(lname);
                    mLockatedPreferences.setEmailAddress(email);
                    mLockatedPreferences.setContactNumber(mobile);

                } else if (jsonObject.has("code") && jsonObject.has("message") && jsonObject.has("otp")) {
                    changePassword();
                    Utilities.showToastMessage(getActivity(), jsonObject.getString("message"));
                }
            } catch (JSONException e1) {
                e1.printStackTrace();
            }
        }
    }

    private void changePassword() {
       /* accountController = AccountController.getInstance();
        accountDataArrayList = accountController.getmAccountDataList();*/
        String strUserPhone = mLockatedPreferences.getContactNumber();
        Intent changepassword = new Intent(getActivity(), ChangePasswordActivity.class);
        changepassword.putExtra("mobileno", strUserPhone);
        startActivity(changepassword);
    }

}