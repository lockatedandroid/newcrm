
package com.android.lockated.model.AdminModel.AdminEvents.PublshedEvent;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class Event_Published {

    @SerializedName("id")
    @Expose
    private int id;
    @SerializedName("id_society")
    @Expose
    private int idSociety;
    @SerializedName("event_type")
    @Expose
    private String eventType;
    @SerializedName("event_name")
    @Expose
    private String eventName;
    @SerializedName("event_at")
    @Expose
    private String eventAt;
    @SerializedName("from_time")
    @Expose
    private String fromTime;
    @SerializedName("to_time")
    @Expose
    private String toTime;
    @SerializedName("action")
    @Expose
    private Object action;
    @SerializedName("IsDelete")
    @Expose
    private Object IsDelete;
    @SerializedName("created_at")
    @Expose
    private String createdAt;
    @SerializedName("updated_at")
    @Expose
    private String updatedAt;
    @SerializedName("description")
    @Expose
    private String description;
    @SerializedName("publish")
    @Expose
    private int publish;
    @SerializedName("user_id")
    @Expose
    private int userId;
    @SerializedName("canceled_by")
    @Expose
    private Object canceledBy;
    @SerializedName("canceler_id")
    @Expose
    private Object cancelerId;
    @SerializedName("comment")
    @Expose
    private Object comment;
    @SerializedName("shared")
    @Expose
    private int shared;
    @SerializedName("rsvp")
    @Expose
    private String rsvp;
    @SerializedName("username")
    @Expose
    private String username;
    @SerializedName("flat")
    @Expose
    private String flat;
    @SerializedName("documents")
    @Expose
    private ArrayList<DocumentsAdminEventPublish> documentsEvent = new ArrayList<DocumentsAdminEventPublish>();
    /**
     * 
     * @return
     *     The id
     */
    public int getId() {
        return id;
    }

    /**
     * 
     * @param id
     *     The id
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * 
     * @return
     *     The idSociety
     */
    public int getIdSociety() {
        return idSociety;
    }

    /**
     * 
     * @param idSociety
     *     The id_society
     */
    public void setIdSociety(int idSociety) {
        this.idSociety = idSociety;
    }

    /**
     * 
     * @return
     *     The eventType
     */
    public String getEventType() {
        return eventType;
    }

    /**
     * 
     * @param eventType
     *     The event_type
     */
    public void setEventType(String eventType) {
        this.eventType = eventType;
    }

    /**
     * 
     * @return
     *     The eventName
     */
    public String getEventName() {
        return eventName;
    }

    /**
     * 
     * @param eventName
     *     The event_name
     */
    public void setEventName(String eventName) {
        this.eventName = eventName;
    }

    /**
     * 
     * @return
     *     The eventAt
     */
    public String getEventAt() {
        return eventAt;
    }

    /**
     * 
     * @param eventAt
     *     The event_at
     */
    public void setEventAt(String eventAt) {
        this.eventAt = eventAt;
    }

    /**
     * 
     * @return
     *     The fromTime
     */
    public String getFromTime() {
        return fromTime;
    }

    /**
     * 
     * @param fromTime
     *     The from_time
     */
    public void setFromTime(String fromTime) {
        this.fromTime = fromTime;
    }

    /**
     * 
     * @return
     *     The toTime
     */
    public String getToTime() {
        return toTime;
    }

    /**
     * 
     * @param toTime
     *     The to_time
     */
    public void setToTime(String toTime) {
        this.toTime = toTime;
    }

    /**
     * 
     * @return
     *     The action
     */
    public Object getAction() {
        return action;
    }

    /**
     * 
     * @param action
     *     The action
     */
    public void setAction(Object action) {
        this.action = action;
    }

    /**
     * 
     * @return
     *     The IsDelete
     */
    public Object getIsDelete() {
        return IsDelete;
    }

    /**
     * 
     * @param IsDelete
     *     The IsDelete
     */
    public void setIsDelete(Object IsDelete) {
        this.IsDelete = IsDelete;
    }

    /**
     * 
     * @return
     *     The createdAt
     */
    public String getCreatedAt() {
        return createdAt;
    }

    /**
     * 
     * @param createdAt
     *     The created_at
     */
    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    /**
     * 
     * @return
     *     The updatedAt
     */
    public String getUpdatedAt() {
        return updatedAt;
    }

    /**
     * 
     * @param updatedAt
     *     The updated_at
     */
    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    /**
     * 
     * @return
     *     The description
     */
    public String getDescription() {
        return description;
    }

    /**
     * 
     * @param description
     *     The description
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * 
     * @return
     *     The publish
     */
    public int getPublish() {
        return publish;
    }

    /**
     * 
     * @param publish
     *     The publish
     */
    public void setPublish(int publish) {
        this.publish = publish;
    }

    /**
     * 
     * @return
     *     The userId
     */
    public int getUserId() {
        return userId;
    }

    /**
     * 
     * @param userId
     *     The user_id
     */
    public void setUserId(int userId) {
        this.userId = userId;
    }

    /**
     * 
     * @return
     *     The canceledBy
     */
    public Object getCanceledBy() {
        return canceledBy;
    }

    /**
     * 
     * @param canceledBy
     *     The canceled_by
     */
    public void setCanceledBy(Object canceledBy) {
        this.canceledBy = canceledBy;
    }

    /**
     * 
     * @return
     *     The cancelerId
     */
    public Object getCancelerId() {
        return cancelerId;
    }

    /**
     * 
     * @param cancelerId
     *     The canceler_id
     */
    public void setCancelerId(Object cancelerId) {
        this.cancelerId = cancelerId;
    }

    /**
     * 
     * @return
     *     The comment
     */
    public Object getComment() {
        return comment;
    }

    /**
     * 
     * @param comment
     *     The comment
     */
    public void setComment(Object comment) {
        this.comment = comment;
    }

    /**
     * 
     * @return
     *     The shared
     */
    public int getShared() {
        return shared;
    }

    /**
     * 
     * @param shared
     *     The shared
     */
    public void setShared(int shared) {
        this.shared = shared;
    }

    /**
     * 
     * @return
     *     The rsvp
     */
    public String getRsvp() {
        return rsvp;
    }

    /**
     * 
     * @param rsvp
     *     The rsvp
     */
    public void setRsvp(String rsvp) {
        this.rsvp = rsvp;
    }

    /**
     * 
     * @return
     *     The username
     */
    public String getUsername() {
        return username;
    }

    /**
     * 
     * @param username
     *     The username
     */
    public void setUsername(String username) {
        this.username = username;
    }

    /**
     * 
     * @return
     *     The flat
     */
    public String getFlat() {
        return flat;
    }

    /**
     * 
     * @param flat
     *     The flat
     */
    public void setFlat(String flat) {
        this.flat = flat;
    }
    public ArrayList<DocumentsAdminEventPublish> getDocumentsEvent() {
        return documentsEvent;
    }

    public void setDocumentsEvent(ArrayList<DocumentsAdminEventPublish> documentsEvent) {
        this.documentsEvent = documentsEvent;
    }
}
