
package com.android.lockated.model.expectedVisitor;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class GatekeeperExpected {

    @SerializedName("id")
    @Expose
    private int id;
    @SerializedName("guest_number")
    @Expose
    private String guestNumber;
    @SerializedName("guest_vehicle_number")
    @Expose
    private String guestVehicleNumber;
    @SerializedName("visit_purpose")
    @Expose
    private String visitPurpose;
    @SerializedName("guest_name")
    @Expose
    private String guestName;
    @SerializedName("guest_entry_time")
    @Expose
    private String guestEntryTime;
    @SerializedName("guest_exit_time")
    @Expose
    private String guestExitTime;
    @SerializedName("IsDelete")
    @Expose
    private String isDelete;
    @SerializedName("plus_person")
    @Expose
    private int plusPerson;
    @SerializedName("expected_at")
    @Expose
    private String expectedAt;
    @SerializedName("user_society_id")
    @Expose
    private int userSocietyId;
    @SerializedName("approve")
    @Expose
    private int approve;
    @SerializedName("notes")
    @Expose
    private String notes;
    @SerializedName("created_by")
    @Expose
    private String createdBy;
    @SerializedName("created_by_id")
    @Expose
    private String createdById;
    @SerializedName("is_regular")
    @Expose
    private String isRegular;
    @SerializedName("include_mimo")
    @Expose
    private String includeMimo;
    @SerializedName("society_gate_id")
    @Expose
    private String societyGateId;
    @SerializedName("accompany")
    @Expose
    private String accompany;
    @SerializedName("visit_to")
    @Expose
    private String visitTo;
    @SerializedName("vstatus")
    @Expose
    private String vstatus;
    @SerializedName("image")
    @Expose
    private String image;
    @SerializedName("flat")
    @Expose
    private String flat;
    @SerializedName("society_gate")
    @Expose
    private Object societyGate;
    @SerializedName("gatekeeper_mimos")
    @Expose
    private ArrayList<GatekeeperMimo> gatekeeperMimos = new ArrayList<GatekeeperMimo>();

    /**
     * @return The id
     */
    public int getId() {
        return id;
    }

    /**
     * @param id The id
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * @return The guestNumber
     */
    public String getGuestNumber() {
        return guestNumber;
    }

    /**
     * @param guestNumber The guest_number
     */
    public void setGuestNumber(String guestNumber) {
        this.guestNumber = guestNumber;
    }

    /**
     * @return The guestVehicleNumber
     */
    public String getGuestVehicleNumber() {
        return guestVehicleNumber;
    }

    /**
     * @param guestVehicleNumber The guest_vehicle_number
     */
    public void setGuestVehicleNumber(String guestVehicleNumber) {
        this.guestVehicleNumber = guestVehicleNumber;
    }

    /**
     * @return The visitPurpose
     */
    public String getVisitPurpose() {
        return visitPurpose;
    }

    /**
     * @param visitPurpose The visit_purpose
     */
    public void setVisitPurpose(String visitPurpose) {
        this.visitPurpose = visitPurpose;
    }

    /**
     * @return The guestName
     */
    public String getGuestName() {
        return guestName;
    }

    /**
     * @param guestName The guest_name
     */
    public void setGuestName(String guestName) {
        this.guestName = guestName;
    }

    /**
     * @return The guestEntryTime
     */
    public String getGuestEntryTime() {
        return guestEntryTime;
    }

    /**
     * @param guestEntryTime The guest_entry_time
     */
    public void setGuestEntryTime(String guestEntryTime) {
        this.guestEntryTime = guestEntryTime;
    }

    /**
     * @return The guestExitTime
     */
    public String getGuestExitTime() {
        return guestExitTime;
    }

    /**
     * @param guestExitTime The guest_exit_time
     */
    public void setGuestExitTime(String guestExitTime) {
        this.guestExitTime = guestExitTime;
    }

    /**
     * @return The isDelete
     */
    public String getIsDelete() {
        return isDelete;
    }

    /**
     * @param isDelete The IsDelete
     */
    public void setIsDelete(String isDelete) {
        this.isDelete = isDelete;
    }

    /**
     * @return The plusPerson
     */
    public int getPlusPerson() {
        return plusPerson;
    }

    /**
     * @param plusPerson The plus_person
     */
    public void setPlusPerson(int plusPerson) {
        this.plusPerson = plusPerson;
    }

    /**
     * @return The expectedAt
     */
    public String getExpectedAt() {
        return expectedAt;
    }

    /**
     * @param expectedAt The expected_at
     */
    public void setExpectedAt(String expectedAt) {
        this.expectedAt = expectedAt;
    }

    /**
     * @return The userSocietyId
     */
    public int getUserSocietyId() {
        return userSocietyId;
    }

    /**
     * @param userSocietyId The user_society_id
     */
    public void setUserSocietyId(int userSocietyId) {
        this.userSocietyId = userSocietyId;
    }

    /**
     * @return The approve
     */
    public int getApprove() {
        return approve;
    }

    /**
     * @param approve The approve
     */
    public void setApprove(int approve) {
        this.approve = approve;
    }

    /**
     * @return The notes
     */
    public String getNotes() {
        return notes;
    }

    /**
     * @param notes The notes
     */
    public void setNotes(String notes) {
        this.notes = notes;
    }

    /**
     * @return The createdBy
     */
    public String getCreatedBy() {
        return createdBy;
    }

    /**
     * @param createdBy The created_by
     */
    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    /**
     * @return The createdById
     */
    public String getCreatedById() {
        return createdById;
    }

    /**
     * @param createdById The created_by_id
     */
    public void setCreatedById(String createdById) {
        this.createdById = createdById;
    }

    /**
     * @return The isRegular
     */
    public String getIsRegular() {
        return isRegular;
    }

    /**
     * @param isRegular The is_regular
     */
    public void setIsRegular(String isRegular) {
        this.isRegular = isRegular;
    }

    /**
     * @return The includeMimo
     */
    public String getIncludeMimo() {
        return includeMimo;
    }

    /**
     * @param includeMimo The include_mimo
     */
    public void setIncludeMimo(String includeMimo) {
        this.includeMimo = includeMimo;
    }

    /**
     * @return The societyGateId
     */
    public String getSocietyGateId() {
        return societyGateId;
    }

    /**
     * @param societyGateId The society_gate_id
     */
    public void setSocietyGateId(String societyGateId) {
        this.societyGateId = societyGateId;
    }

    /**
     * @return The accompany
     */
    public String getAccompany() {
        return accompany;
    }

    /**
     * @param accompany The accompany
     */
    public void setAccompany(String accompany) {
        this.accompany = accompany;
    }

    /**
     * @return The visitTo
     */
    public String getVisitTo() {
        return visitTo;
    }

    /**
     * @param visitTo The visit_to
     */
    public void setVisitTo(String visitTo) {
        this.visitTo = visitTo;
    }

    /**
     * @return The vstatus
     */
    public String getVstatus() {
        return vstatus;
    }

    /**
     * @param vstatus The vstatus
     */
    public void setVstatus(String vstatus) {
        this.vstatus = vstatus;
    }

    /**
     * @return The image
     */
    public String getImage() {
        return image;
    }

    /**
     * @param image The image
     */
    public void setImage(String image) {
        this.image = image;
    }

    /**
     * @return The flat
     */
    public String getFlat() {
        return flat;
    }

    /**
     * @param flat The flat
     */
    public void setFlat(String flat) {
        this.flat = flat;
    }

    /**
     * @return The societyGate
     */
    public Object getSocietyGate() {
        return societyGate;
    }

    /**
     * @param societyGate The society_gate
     */
    public void setSocietyGate(Object societyGate) {
        this.societyGate = societyGate;
    }

    /**
     * @return The gatekeeperMimos
     */
    public List<GatekeeperMimo> getGatekeeperMimos() {
        return gatekeeperMimos;
    }

    /**
     * @param gatekeeperMimos The gatekeeper_mimos
     */
    public void setGatekeeperMimos(ArrayList<GatekeeperMimo> gatekeeperMimos) {
        this.gatekeeperMimos = gatekeeperMimos;
    }

}
