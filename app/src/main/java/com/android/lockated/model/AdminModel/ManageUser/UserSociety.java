package com.android.lockated.model.AdminModel.ManageUser;

/**
 * Created by webwerks on 26/2/16.
 */


public class UserSociety {

    private int id;
    private String idSociety;
    private Object approve;
    private Object adminId;
    private Object createdAt;
    private Object updatedAt;
    private User user;

    /**
     * @return The id
     */
    public int getId() {
        return id;
    }

    /**
     * @param id The id
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * @return The idSociety
     */
    public String getIdSociety() {
        return idSociety;
    }

    /**
     * @param idSociety The id_society
     */
    public void setIdSociety(String idSociety) {
        this.idSociety = idSociety;
    }

    /**
     * @return The approve
     */
    public Object getApprove() {
        return approve;
    }

    /**
     * @param approve The approve
     */
    public void setApprove(Object approve) {
        this.approve = approve;
    }

    /**
     * @return The adminId
     */
    public Object getAdminId() {
        return adminId;
    }

    /**
     * @param adminId The admin_id
     */
    public void setAdminId(Object adminId) {
        this.adminId = adminId;
    }

    /**
     * @return The createdAt
     */
    public Object getCreatedAt() {
        return createdAt;
    }

    /**
     * @param createdAt The created_at
     */
    public void setCreatedAt(Object createdAt) {
        this.createdAt = createdAt;
    }

    /**
     * @return The updatedAt
     */
    public Object getUpdatedAt() {
        return updatedAt;
    }

    /**
     * @param updatedAt The updated_at
     */
    public void setUpdatedAt(Object updatedAt) {
        this.updatedAt = updatedAt;
    }

    /**
     * @return The user
     */
    public User getUser() {
        return user;
    }

    /**
     * @param user The user
     */
    public void setUser(User user) {
        this.user = user;
    }
}