package com.android.lockated.crmadmin.fragment.gatekeeper;


import android.Manifest;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.drawable.ColorDrawable;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.text.TextUtils;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.android.lockated.R;
import com.android.lockated.crmadmin.activity.AdminMyZoneActivity;
import com.android.lockated.crmadmin.activity.HomeGatekeeperActivity;
import com.android.lockated.information.AccountController;
import com.android.lockated.model.AccountData;
import com.android.lockated.model.usermodel.myGroups.UserSociety;
import com.android.lockated.network.ConnectionDetector;
import com.android.lockated.preferences.LockatedPreferences;
import com.android.lockated.request.LockatedJSONObjectRequest;
import com.android.lockated.request.LockatedRequestError;
import com.android.lockated.request.LockatedVolleyRequestQueue;
import com.android.lockated.utils.ApplicationURL;
import com.android.lockated.utils.DatePickerFragment;
import com.android.lockated.utils.MarshMallowPermission;
import com.android.lockated.utils.ShowImage;
import com.android.lockated.utils.TimePickerFragment;
import com.android.lockated.utils.Utilities;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

public class GatekeeperEntryFragment extends DialogFragment implements View.OnClickListener, Response.Listener<JSONObject>, Response.ErrorListener, AdapterView.OnItemSelectedListener {

    public static final String REQUEST_TAG = "GatekeeperEntryFragment";
    static final int REQUEST_CAMERA_PHOTO = 3;
    static final int REQUEST_TAKE_PHOTO = 100;
    static final int CHOOSE_IMAGE_REQUEST = 101;
    static final int REQUEST_CAMERA = 102;
    static final int REQUEST_STORAGE = 103;
    private static final int REQUEST_GALLERY = 4;
    Window window;
    int RESULT_OK = 1;
    boolean imageSet;
    MarshMallowPermission marshMallowPermission;
    AccountController accountController;
    ArrayList<AccountData> accountDataArrayList;
    String societyId;
    Spinner mVisitToSpinner, mFlatNumberSpinner;

    String CREATE_TAG = "SpinnerEntry";
    ArrayList<Integer> userId;
    ArrayList<String> mFlatNumberArray;
    ArrayList<String> userNameArrayList;
    ArrayList<UserSociety> userSocietyArrayList;
    Integer user_id;
    String memberName;
    ProgressDialog progressDialog;
    private View VisitorEntryView;
    private EditText mVisitorName;
    private EditText mAdditionalMember;
    private EditText mContactNumber;
    private TextView mTextDate;
    private EditText mNotes;
    private TextView mSubmit;
    private TextView mTextTime;
    private EditText mVisitTo;
    private EditText mVisitPurpose;
    private EditText mVehiclrNumber;
    private ImageView mImageExpectedDate;
    private ImageView mImageExpectedTime;
    private ImageView mclose;
    private ImageView mVisitorImage;
    private String strName;
    private String strAdditionalMenber;
    private String strContactNumber;
    private String strDate;
    private String strNotes;
    private String strVisitTo;
    private String strVisitPurpose;
    private String strVehicleNumber;
    private String mCurrentPhotoPath, encodedImage;
    private RequestQueue mQueue;
    private LockatedPreferences mLockatedPreferences;

    @Override
    public void onStart() {
        super.onStart();
        window = getDialog().getWindow();
        WindowManager.LayoutParams windowParams = window.getAttributes();
        windowParams.dimAmount = 0.75f;
        windowParams.flags |= WindowManager.LayoutParams.FLAG_DIM_BEHIND;
        window.setAttributes(windowParams);
        window.setBackgroundDrawableResource(android.R.color.white);
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        VisitorEntryView = getActivity().getLayoutInflater().inflate(R.layout.fragment_gatekeeper_entry,
                new LinearLayout(getActivity()), false);
        marshMallowPermission = new MarshMallowPermission(getActivity());
        init(VisitorEntryView);
        Dialog builder = new Dialog(getActivity());
        builder.requestWindowFeature(Window.FEATURE_NO_TITLE);
        builder.getWindow().setBackgroundDrawable(new ColorDrawable(Color.WHITE));
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(builder.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        builder.show();
        builder.getWindow().setAttributes(lp);
        builder.setContentView(VisitorEntryView);
        String name = getArguments().getString("name");
        return builder;
    }

    private void init(View visitorEntryView) {
        userSocietyArrayList = new ArrayList<>();
        userNameArrayList = new ArrayList<>();
        userId = new ArrayList<>();
        mFlatNumberArray = new ArrayList<>();
        mLockatedPreferences = new LockatedPreferences(getActivity());
        /*mProgressBar = (ProgressBar) visitorEntryView.findViewById(R.id.mProgressBarSmallView);*/
        mVisitPurpose = (EditText) visitorEntryView.findViewById(R.id.mVisitPurpose);
        mVehiclrNumber = (EditText) visitorEntryView.findViewById(R.id.mVehiclrNumber);
        mTextDate = (TextView) visitorEntryView.findViewById(R.id.mTextDate);
        mVisitorName = (EditText) visitorEntryView.findViewById(R.id.mVisitorName);
        mAdditionalMember = (EditText) visitorEntryView.findViewById(R.id.mAdditionalMember);
        mContactNumber = (EditText) visitorEntryView.findViewById(R.id.mContactNumber);
        mImageExpectedDate = (ImageView) visitorEntryView.findViewById(R.id.mImageExpectedDate);
        mVisitorImage = (ImageView) visitorEntryView.findViewById(R.id.mVisitorImage);
        mImageExpectedTime = (ImageView) visitorEntryView.findViewById(R.id.mImageExpectedTime);
        mNotes = (EditText) visitorEntryView.findViewById(R.id.mNotes);
        mTextTime = (TextView) visitorEntryView.findViewById(R.id.mTextTime);
        mSubmit = (TextView) visitorEntryView.findViewById(R.id.mSubmit);
        mclose = (ImageView) visitorEntryView.findViewById(R.id.mclose);
        mVisitToSpinner = (Spinner) visitorEntryView.findViewById(R.id.mVisitToSpinner);
        mFlatNumberSpinner = (Spinner) visitorEntryView.findViewById(R.id.mFlatNumberSpinner);
        setMembersToSpinner();
        mImageExpectedTime.setOnClickListener(this);
        mVisitToSpinner.setOnItemSelectedListener(this);
        mFlatNumberSpinner.setOnItemSelectedListener(this);
        mImageExpectedDate.setOnClickListener(this);
        mSubmit.setOnClickListener(this);
        mVisitorImage.setOnClickListener(this);
        mclose.setOnClickListener(this);
    }

    private void setMembersToSpinner() {
        progressDialog = ProgressDialog.show(getActivity(), "", "Please Wait...", false);
        progressDialog.show();
        if (ConnectionDetector.isConnectedToInternet(getActivity())) {
            String url = ApplicationURL.getAllMemberList + mLockatedPreferences.getSocietyId()
                    + "&token=" + mLockatedPreferences.getLockatedToken();
            LockatedVolleyRequestQueue lockatedVolleyRequestQueue = LockatedVolleyRequestQueue.getInstance(getActivity());
            lockatedVolleyRequestQueue.sendRequest(CREATE_TAG, Request.Method.GET, url, null, this, this);
        } else {
            Utilities.showToastMessage(getActivity(), getActivity().getResources().getString(R.string.internet_connection_error));
        }


    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.mImageExpectedDate:
                selectedDate(mTextDate);
                break;
            case R.id.mImageExpectedTime:
                selectTime(mTextTime);
                break;
            case R.id.mVisitorImage:
                if (!imageSet) {
                    selectImage();
                } else {
                    selectImageAction();
                }
                break;
            case R.id.mSubmit:
                SubmitDetails();
                break;
            case R.id.mclose:
                dismiss();
                break;
        }
    }

    private void selectTime(TextView time) {
        TimePickerFragment timePickerFragment = new TimePickerFragment();
        timePickerFragment.setTimePickerView(time);
        DialogFragment newFragment = timePickerFragment;
        newFragment.show(getActivity().getSupportFragmentManager(), "TimePicker");
    }

    private void selectImage() {
        final CharSequence[] options = {"Camera", "Gallery"};
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle("Add Photo!");
        builder.setItems(options, new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int item) {
                if (options[item].equals("Camera")) {
                    checkCameraPermission();
                } else if (options[item].equals("Gallery")) {
                    checkStoragePermission();
                }
            }

        });

        builder.show();
    }

    private void selectImageAction() {
        final CharSequence[] options = {"View Image", "Change Image", "Remove Image"};
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setItems(options, new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int item) {
                if (options[item].equals("View Image")) {
                    Intent displayImage = new Intent(getActivity(), ShowImage.class);
                    displayImage.putExtra("imagePathString", mCurrentPhotoPath);
                    getActivity().startActivity(displayImage);
                    getActivity().startActivity(displayImage);
                } else if (options[item].equals("Change Image")) {
                    selectImage();
                } else {
                    mVisitorImage.setImageResource(R.drawable.ic_account_camera);
                    encodedImage = "";
                    imageSet = false;
                }
            }

        });
        builder.show();

    }

    private void checkStoragePermission() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                requestPermissions(new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, REQUEST_STORAGE);
            } else {
                onGalleryClicked();
            }
        } else {
            onGalleryClicked();
        }
    }

    private void onGalleryClicked() {
        Intent i = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(i, CHOOSE_IMAGE_REQUEST);
    }

    private void checkCameraPermission() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
                requestPermissions(new String[]{Manifest.permission.CAMERA}, REQUEST_CAMERA);
            } else {
                onCameraClicked();
            }
        } else {
            onCameraClicked();
        }
    }

    private void onCameraClicked() {
        if (!marshMallowPermission.checkPermissionForCamera()) {
            marshMallowPermission.requestPermissionForCamera();
        } else {
            if (!marshMallowPermission.checkPermissionForExternalStorage()) {
                marshMallowPermission.requestPermissionForExternalStorage();
            } else {
                Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                if (takePictureIntent.resolveActivity(getActivity().getPackageManager()) != null) {
                    File photoFile = null;
                    try {
                        photoFile = createImageFile();
                    } catch (IOException ex) {
                        ex.printStackTrace();
                    }

                    if (photoFile != null) {
                        takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(photoFile));
                        startActivityForResult(takePictureIntent, REQUEST_TAKE_PHOTO);
                    }
                }
            }
        }
    }

    private File createImageFile() throws IOException {
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss", Locale.ENGLISH).format(new Date());
        String imageFileName = "JPEG_" + timeStamp + "_";
        File storageDir = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES);
        File image = File.createTempFile(imageFileName, ".jpg", storageDir);
        mCurrentPhotoPath = image.getAbsolutePath();
        return image;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode != getActivity().RESULT_CANCELED) {
            if (requestCode == REQUEST_TAKE_PHOTO && resultCode == getActivity().RESULT_OK) {
                setPic();
            } else if (requestCode == CHOOSE_IMAGE_REQUEST && resultCode == getActivity().RESULT_OK) {
                Uri selectedImageURI = data.getData();
                mCurrentPhotoPath = getPath(selectedImageURI);
                setPic();
            } else if (requestCode == REQUEST_CAMERA_PHOTO && resultCode == getActivity().RESULT_OK) {
                Bitmap thumbnail = (Bitmap) data.getExtras().get("data");
                if (thumbnail != null) {
                    ByteArrayOutputStream bytes = new ByteArrayOutputStream();
                    File destination = new File(Environment.getExternalStorageDirectory(), System.currentTimeMillis() + ".jpg");
                    FileOutputStream fo;
                    try {
                        destination.createNewFile();
                        fo = new FileOutputStream(destination);
                        fo.write(bytes.toByteArray());
                        fo.close();
                    } catch (FileNotFoundException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    setCameraPic(thumbnail);
                }
            }
        }
    }

    private String getPath(Uri uri) {
        if (uri == null) {
            return null;
        }
        String[] projection = {MediaStore.Images.Media.DATA};
        Cursor cursor = getActivity().getContentResolver().query(uri, projection, null, null, null);
        if (cursor != null) {
            int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
            cursor.moveToFirst();
            return cursor.getString(column_index);
        }
        return uri.getPath();
    }

    private void setCameraPic(Bitmap bitmap) {
        if (bitmap != null) {
            encodedImage = Utilities.encodeTobase64(bitmap);
            mVisitorImage.setImageBitmap(bitmap);
            imageSet = true;
        }
    }

    private void setPic() {
        /*int targetW = mVisitorImage.getWidth();
        int targetH = mVisitorImage.getHeight();
        BitmapFactory.Options bmOptions = new BitmapFactory.Options();
        bmOptions.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(mCurrentPhotoPath, bmOptions);
        int photoW = bmOptions.outWidth;
        int photoH = bmOptions.outHeight;
        int scaleFactor = Math.min(photoW / targetW, photoH / targetH);

        bmOptions.inJustDecodeBounds = false;
        bmOptions.inSampleSize = scaleFactor;
        bmOptions.inPurgeable = true;

        Bitmap bitmap = BitmapFactory.decodeFile(mCurrentPhotoPath, bmOptions);
        if (bitmap != null) {
            encodedImage = Utilities.encodeTobase64(bitmap);
            Log.w("setPic", encodedImage);
            mVisitorImage.setImageBitmap(bitmap);
            imageSet = true;
        }*/


        int targetW = mVisitorImage.getWidth();
        int targetH = mVisitorImage.getHeight();
        BitmapFactory.Options bmOptions = new BitmapFactory.Options();
        bmOptions.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(mCurrentPhotoPath, bmOptions);
        int photoW = bmOptions.outWidth;
        int photoH = bmOptions.outHeight;
        int scaleFactor = Math.min(photoW / targetW, photoH / targetH);

        bmOptions.inJustDecodeBounds = false;
        bmOptions.inSampleSize = scaleFactor;
        bmOptions.inPurgeable = true;
        try {

            ExifInterface exif = new ExifInterface(mCurrentPhotoPath);
            int orientation = exif.getAttributeInt(
                    ExifInterface.TAG_ORIENTATION,
                    ExifInterface.ORIENTATION_NORMAL);

            int angle = 0;

            if (orientation == ExifInterface.ORIENTATION_ROTATE_90) {
                angle = 90;
            } else if (orientation == ExifInterface.ORIENTATION_ROTATE_180) {
                angle = 180;
            } else if (orientation == ExifInterface.ORIENTATION_ROTATE_270) {
                angle = 270;
            }

            Matrix mat = new Matrix();
            mat.postRotate(angle);
            Bitmap bitmap;
            Bitmap bmp = BitmapFactory.decodeStream(new FileInputStream(mCurrentPhotoPath),
                    null, bmOptions);
            bitmap = Bitmap.createBitmap(bmp, 0, 0, bmp.getWidth(),
                    bmp.getHeight(), mat, true);
            if (bitmap != null) {
                encodedImage = Utilities.encodeTobase64(bitmap);
                mVisitorImage.setImageBitmap(bitmap);
                imageSet = true;
            }

        } catch (IOException | OutOfMemoryError e) {
            e.printStackTrace();
        }



    }

    private void SubmitDetails() {
        strName = mVisitorName.getText().toString();
        strAdditionalMenber = mAdditionalMember.getText().toString();
        strContactNumber = mContactNumber.getText().toString();
        strDate = mTextDate.getText().toString();
        strNotes = mNotes.getText().toString();
        String strEntryTime = mTextTime.getText().toString();
        strVisitPurpose = mVisitPurpose.getText().toString();
        strVehicleNumber = mVehiclrNumber.getText().toString();
        accountController = AccountController.getInstance();
        String Creatername = mLockatedPreferences.getAccountData().getFirstname() + " " +
                mLockatedPreferences.getAccountData().getLastname();
        accountDataArrayList = accountController.getmAccountDataList();
       /* societyId = accountDataArrayList.get(0).getmResidenceDataList().get(0).getSociety_id();*/
        societyId = mLockatedPreferences.getSocietyId();

        if ((TextUtils.isEmpty(strName))) {
            Utilities.showToastMessage(getActivity(), "Please Fill Name");
        } else if (TextUtils.isEmpty(strAdditionalMenber)) {
            Utilities.showToastMessage(getActivity(), "Please Fill Member");
        } else if (TextUtils.isEmpty(strVisitPurpose)) {
            Utilities.showToastMessage(getActivity(), "Please Fill Purpose");
        } else if (TextUtils.isEmpty(strContactNumber)) {
            Utilities.showToastMessage(getActivity(), "Please Fill Contact Number");
        } else if (strDate.equals("Entry Date")) {
            Utilities.showToastMessage(getActivity(), "Please Select Date");
        } else if (strEntryTime.equals("Entry Time")) {
            Utilities.showToastMessage(getActivity(), "Please Select Time");
        } else if (mVisitToSpinner.getSelectedItemPosition() == 0) {
            Utilities.showToastMessage(getActivity(), "Please Select Member Name");
        } else {
            progressDialog = ProgressDialog.show(getActivity(), "", "Please Wait...", false);
            progressDialog.show();
            if (ConnectionDetector.isConnectedToInternet(getActivity())) {
                JSONObject jsonObjectMain = new JSONObject();
                JSONObject jsonObject = new JSONObject();
                JSONArray jsonArray = new JSONArray();
                try {
                    jsonObject.put("id_society", societyId);
                    jsonObject.put("guest_name", strName);
                    jsonObject.put("expected_at", strDate + "T" + strEntryTime);
                    jsonObject.put("guest_entry_time", strDate + "T" + strEntryTime);
                    jsonObject.put("guest_number", strContactNumber);
                    jsonObject.put("guest_vehicle_number", strVehicleNumber);
                    jsonObject.put("visit_purpose", strVisitPurpose);
                    jsonObject.put("approve", "0");
                    jsonObject.put("user_society_id", "" + mLockatedPreferences.getUserSocietyId());
                    jsonObject.put("visit_to", Creatername);
                    jsonObject.put("id_user", user_id);
                    jsonObject.put("plus_person", strAdditionalMenber);
                    jsonArray.put(encodedImage);
                    jsonObject.put("documents", jsonArray);
                    jsonObjectMain.put("gatekeeper", jsonObject);
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                try {
                    mQueue = LockatedVolleyRequestQueue.getInstance(getActivity()).getRequestQueue();
                    String url = ApplicationURL.addVisitor + mLockatedPreferences.getLockatedToken();
                    LockatedJSONObjectRequest lockatedJSONObjectRequest = new LockatedJSONObjectRequest(Request.Method.POST,
                            url, jsonObjectMain, this, this);
                    lockatedJSONObjectRequest.setTag(REQUEST_TAG);
                    lockatedJSONObjectRequest.setRetryPolicy(new DefaultRetryPolicy(0,DefaultRetryPolicy.DEFAULT_MAX_RETRIES,DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                    mQueue.add(lockatedJSONObjectRequest);
                } catch (Exception e) {
                    e.printStackTrace();
                }

            } else {
                Utilities.showToastMessage(getActivity(), getActivity().getResources().getString(R.string.internet_connection_error));
            }
        }
    }

    private void selectedDate(TextView mDate) {
        DatePickerFragment datePickerFragment = new DatePickerFragment();
        datePickerFragment.setDatePickerView(mDate);
        DialogFragment newFragment = datePickerFragment;
        newFragment.show(getActivity().getSupportFragmentManager(), "DatePicker");
    }

    @Override
    public void onResponse(JSONObject response) {
        progressDialog.dismiss();
        if (response.has("user_societies")) {
            try {
                if (response.getJSONArray("user_societies").length() > 0) {
                    JSONArray jsonArray = response.getJSONArray("user_societies");
                    Gson gson = new Gson();
                    for (int i = 0; i < jsonArray.length(); i++) {
                        UserSociety userSociety = gson.fromJson(jsonArray.getJSONObject(i).toString(), UserSociety.class);
                        userSocietyArrayList.add(userSociety);
                    }
                    for (int i = 0; i < userSocietyArrayList.size(); i++) {
                        int id = userSocietyArrayList.get(i).getUser().getId();
                        String name = userSocietyArrayList.get(i).getUser().getFirstname() + " " + userSocietyArrayList.get(i).getUser().getLastname();
                        String flat = userSocietyArrayList.get(i).getUserFlat().getFlat();
                        /*String block=userSocietyArrayList.get(i).getUserFlat().getBlock();*/
                        userId.add(id);
                        //  mFlatNumberArray.add(flat);
                        userNameArrayList.add(flat + " - " + name);
                    }
                    userId.add(0, 0);
                    userNameArrayList.add(0, "Select Name");
                    mFlatNumberArray.add(0, "Select Flat");

                    ArrayAdapter<String> spinnerNameArrayAdapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item, userNameArrayList);
                    spinnerNameArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                    mVisitToSpinner.setAdapter(spinnerNameArrayAdapter);
                    mVisitToSpinner.setPrompt("Select Name");
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        } else if (response.has("id")) {
            String name;
            if (getArguments() != null) {
                name = getArguments().getString("name");
            } else {
                name = "HomeGatekeeperActivity";
            }
            if (name != null && !name.equals("HomeGatekeeperActivity")) {
                Intent backIntent = new Intent(getActivity(), AdminMyZoneActivity.class);
                backIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                backIntent.putExtra("AdminMyZone", 2);
                startActivity(backIntent);
                dismiss();
            } else {
                Intent homegatekeeper = new Intent(getActivity(), HomeGatekeeperActivity.class);
                homegatekeeper.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(homegatekeeper);
                dismiss();
            }
        }

    }

    @Override
    public void onErrorResponse(VolleyError error) {
        progressDialog.dismiss();
        if (getActivity() != null) {
            LockatedRequestError.onRequestError(getActivity(), error);
        }
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        user_id = userId.get(position);
        memberName = parent.getSelectedItem().toString();
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }
}
