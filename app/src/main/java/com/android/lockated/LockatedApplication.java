package com.android.lockated;

import android.app.Application;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.multidex.MultiDex;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import com.android.lockated.analytics.AnalyticsTrackers;
import com.android.lockated.categories.vendors.model.VendorProductData;
import com.android.lockated.landing.model.TaxonData;
import com.android.lockated.model.MyAddressData;
import com.android.lockated.preferences.LockatedPreferences;
import com.clevertap.android.sdk.ActivityLifecycleCallback;
import com.facebook.FacebookSdk;
import com.facebook.appevents.AppEventsLogger;
import com.genie.Genie;
import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.StandardExceptionParser;
import com.google.android.gms.analytics.Tracker;
import com.google.android.gms.analytics.ecommerce.Product;
import com.google.android.gms.analytics.ecommerce.ProductAction;
import com.library.zomato.ordering.common.OrderSDK;
import com.library.zomato.ordering.common.OrderSDKInitializer;
import com.library.zomato.ordering.data.ZNotification;

import java.util.ArrayList;

public class LockatedApplication extends Application {
    private String mSectionName;
    private String mHeaderName;
    private String mSectionURL;
    private boolean mNotificationCallValue;
    private String mDeliveryDate;
    private String mDeliveryTime;
    private int mTaxonId;
    private int mTaxonomyId;
    private int mSectionId;
    private int mCategoryId;
    private int mCategoryIdTwo;
    private int mSectionIdTwo;

    private boolean isGPSEnabled;

    private MyAddressData myAddressData;

    private ArrayList<TaxonData> mTaxonDatas;
    private ArrayList<VendorProductData> mVendorProductData;

    private static LockatedApplication mInstance;

    //Push Notification
    private static LockatedPreferences lockatedPreferences;

    // Zomato Credentials
    /*String API_KEY = "orderingsdkapikeyandroid";
    String SECRET_KEY = "secretkey";*/
    String API_KEY = "667e36a4ff378517f6d2445ce90c5702";
    String SECRET_KEY = "295213ef731b4b56c26bff0c1af062c1";

    @Override
    public void onCreate() {

        ActivityLifecycleCallback.register(this);

        super.onCreate();
        mInstance = this;

        AnalyticsTrackers.initialize(this);
        AnalyticsTrackers.getInstance().get(AnalyticsTrackers.Target.APP);

        Genie.getInstance().init(this, "bf25c268-d65d-41c6-9cd2-a8179a20c14f", "ca454596-1ede-4d5b-9272-0060b170dd75");

        FacebookSdk.sdkInitialize(getApplicationContext());
        AppEventsLogger.activateApp(this);

        OrderSDKInitializer.getNewInstance(API_KEY, SECRET_KEY).initializeForExternalApp(getApplicationContext());

        LocalBroadcastManager.getInstance(getApplicationContext()).registerReceiver(tabNotificationReceiver,
                new IntentFilter(OrderSDK.getOrderTabBroadcastIntentFilter()));
       /* LocalBroadcastManager.getInstance(getApplicationContext()).registerReceiver(tabNotificationReceiver,
                new IntentFilter(OrderSDK.getOrderTabBroadcastIntentFilter()));*/
    }

    public static synchronized LockatedApplication getInstance() {
        return mInstance;
    }

    public synchronized Tracker getGoogleAnalyticsTracker() {
        AnalyticsTrackers analyticsTrackers = AnalyticsTrackers.getInstance();
        return analyticsTrackers.get(AnalyticsTrackers.Target.APP);
    }

    public static synchronized Tracker getGoogleAnalyticsTrackerCampaign() {
        AnalyticsTrackers analyticsTrackers = AnalyticsTrackers.getInstance();
        return analyticsTrackers.get(AnalyticsTrackers.Target.APP_TRACKER);
    }

    public static synchronized Tracker getGoogleAnalyticsTrackerEcommerce() {
        Log.e("getGATrackerEcom", "called");
        AnalyticsTrackers analyticsTrackers = AnalyticsTrackers.getInstance();
        return analyticsTrackers.get(AnalyticsTrackers.Target.ECOMMERCE_TRACKER);
    }

    public void trackScreenView(String screenName) {
        Tracker t = getGoogleAnalyticsTracker();
        t.setScreenName(screenName);
        t.send(new HitBuilders.ScreenViewBuilder().build());
        GoogleAnalytics.getInstance(this).dispatchLocalHits();
    }

    public void trackException(Exception e) {
        if (e != null) {
            Tracker t = getGoogleAnalyticsTracker();
            t.send(new HitBuilders.ExceptionBuilder()
                    .setDescription(
                            new StandardExceptionParser(this, null)
                                    .getDescription(Thread.currentThread().getName(), e))
                    .setFatal(false)
                    .build()
            );
        }
    }

    public void trackEvent(String category, String action, String label) {
        Tracker t = getGoogleAnalyticsTracker();
        t.send(new HitBuilders.EventBuilder().setCategory(category).setAction(action).setLabel(label).build());
    }

    public void trackCampaign(String screenName) {
        Tracker t = getGoogleAnalyticsTrackerCampaign();
        t.setScreenName(screenName);
        // In this example, campaign information is set using
        // a url string with Google Analytics campaign parameters.
        // Note: This is for illustrative purposes. In most cases campaign
        //information would come from an incoming Intent.
        String campaignData = "https://play.google.com/store/apps/details?id=com.lockated.android&" +
                "referrer=utm_source%3Dgoogle%26utm_medium%3Demail%26utm_term%3Doffer%26utm_content%3DgetOffer%26utm_campaign%3Ddiscount";
        // Campaign data sent with this hit.
        t.send(new HitBuilders.ScreenViewBuilder()
                .setCampaignParamsFromUrl(campaignData)
                .build()
        );
    }

    public void gaMeasureImpression(Product product, String screenName, String productName) {
        HitBuilders.ScreenViewBuilder builder = new HitBuilders.ScreenViewBuilder()
                .addImpression(product, productName);

        Tracker t = getGoogleAnalyticsTrackerCampaign();
        t.setScreenName(screenName);
        t.send(builder.build());
    }

    public static void gaMeasureTransaction(Product product, ProductAction productAction,
                                            String screenName, String email) {
        Log.e("gaMeasureTransaction", "called");
        HitBuilders.ScreenViewBuilder builder = new HitBuilders.ScreenViewBuilder()
                .addProduct(product)
                .setProductAction(productAction);

        //Tracker t = getGoogleAnalyticsTrackerEcommerce();
        Tracker t = getGoogleAnalyticsTrackerEcommerce();
        t.setScreenName(screenName);
        Log.e("screenName", "" + screenName);
        t.set("User Email", email);
        t.send(builder.build());
        Log.e("HitBuilders", "sent");
    }

    public void ecommerceTracking(Product productObject, String screenName, String paymentOption) {
        /*Product product = new Product()
                .setId("P12345")
                .setName("Android Warhol T-Shirt")
                .setCategory("Apparel/T-Shirts")
                .setBrand("Google")
                .setVariant("black")
                .setPrice(29.20)
                .setQuantity(1);*/
        // Add the step number and additional info about the checkout to the action.
        ProductAction productAction = new ProductAction(ProductAction.ACTION_CHECKOUT)
                .setCheckoutStep(1)
                .setCheckoutOptions(paymentOption);
        HitBuilders.ScreenViewBuilder builder = new HitBuilders.ScreenViewBuilder()
                .addProduct(productObject)
                .setProductAction(productAction);

        Tracker t = getGoogleAnalyticsTrackerEcommerce();
        t.setScreenName(screenName);
        t.send(builder.build());
    }

    public int getCategoryId() {
        return mCategoryId;
    }

    public void setCategoryId(int mCategoryId) {
        this.mCategoryId = mCategoryId;
    }

    public int getCategoryIdTwo() {
        return mCategoryIdTwo;
    }

    public void setCategoryIdTwo(int mCategoryIdTwo) {
        this.mCategoryIdTwo = mCategoryIdTwo;
    }

    public ArrayList<VendorProductData> getVendorProductData() {
        return mVendorProductData;
    }

    public void setVendorProductData(ArrayList<VendorProductData> mVendorProductData) {
        this.mVendorProductData = mVendorProductData;
    }

    public ArrayList<TaxonData> getTaxonDatas() {
        return mTaxonDatas;
    }

    public void setTaxonDatas(ArrayList<TaxonData> mTaxonDatas) {
        this.mTaxonDatas = mTaxonDatas;
    }

    public int getTaxonId() {
        return mTaxonId;
    }

    public void setTaxonId(int mTaxonId) {
        this.mTaxonId = mTaxonId;
    }

    public int getTaxonomyId() {
        return mTaxonomyId;
    }

    public void setTaxonomyId(int mTaxonomyId) {
        this.mTaxonomyId = mTaxonomyId;
    }

    public int getmSectionId() {
        return mSectionId;
    }

    public void setmSectionId(int mSectionId) {
        this.mSectionId = mSectionId;
    }

    public void setmSectionIdTwo(int mSectionIdTwo) {
        this.mSectionIdTwo = mSectionIdTwo;
    }

    public int getmSectionIdTwo() {
        return mSectionIdTwo;
    }

    public String getSectionName() {
        return mSectionName;
    }

    public String getHeaderName() {
        return mHeaderName;
    }

    public void setHeaderName(String mHeaderName) {
        this.mHeaderName = mHeaderName;
    }

    public void setSectionName(String mSectionName) {
        this.mSectionName = mSectionName;
    }

    public void setNotificationValue(boolean mNotificationCallValue) {
        this.mNotificationCallValue = mNotificationCallValue;
    }

    public boolean getNotificationValue() {
        return mNotificationCallValue;
    }

    public String getSectionURL() {
        return mSectionURL;
    }

    public void setSectionURL(String mSectionURL) {
        this.mSectionURL = mSectionURL;
    }

    public boolean isGPSEnabled() {
        return isGPSEnabled;
    }

    public void setIsGPSEnabled(boolean isGPSEnabled) {
        this.isGPSEnabled = isGPSEnabled;
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
    }

    @Override
    public void onTrimMemory(int level) {
        super.onTrimMemory(level);
    }

    public MyAddressData getMyAddressData() {
        return myAddressData;
    }

    public void setMyAddressData(MyAddressData myAddressData) {
        this.myAddressData = myAddressData;
    }

    public String getmDeliveryDate() {
        return mDeliveryDate;
    }

    public void setmDeliveryDate(String mDeliveryDate) {
        this.mDeliveryDate = mDeliveryDate;
    }

    public String getmDeliveryTime() {
        return mDeliveryTime;
    }

    public void setmDeliveryTime(String mDeliveryTime) {
        this.mDeliveryTime = mDeliveryTime;
    }

    public LockatedPreferences getPrefManager() {
        if (lockatedPreferences == null) {
            lockatedPreferences = new LockatedPreferences(this);
        }
        return lockatedPreferences;
    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(this);
    }

    private BroadcastReceiver tabNotificationReceiver = new BroadcastReceiver() {
        @Override

        public void onReceive(Context context, Intent intent) {
            Bundle bundle = intent.getExtras();
            if (bundle != null && bundle.containsKey(OrderSDK.getOrderTabBroadcastNotificationKey())
                    && bundle.get(OrderSDK.getOrderTabBroadcastNotificationKey()) != null
                    && bundle.get(OrderSDK.getOrderTabBroadcastNotificationKey()) instanceof ZNotification) {
                ZNotification n = (ZNotification) bundle.getSerializable(OrderSDK.getOrderTabBroadcastNotificationKey());
                assert n != null;
                String tabId = n.getTabIdFromDeeplink();
                HomeActivity.order_status = n.getTabStatus();
                Log.e("tabId",""+tabId);
                Log.e("Order Status", String.valueOf(n.getTabStatus()));
                if (tabId.trim().length() > 0)
                    OrderSDK.getInstance().getTab(tabId);
                Log.e("Im In ", "BroadcastReceiver");

            }
        }

    };
}
