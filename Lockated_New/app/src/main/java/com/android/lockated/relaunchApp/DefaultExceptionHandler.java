package com.android.lockated.relaunchApp;

import android.app.Activity;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;

import com.android.lockated.IndexActivity;
import com.android.lockated.LockatedApplication;

import java.io.IOException;

/**
 * Created by webwerks on 28/3/16.
 */
public class DefaultExceptionHandler implements Thread.UncaughtExceptionHandler {

    private Thread.UncaughtExceptionHandler defaultUEH;
    Activity activity;
    Context context;

    public DefaultExceptionHandler(Activity activity/*Context context*/) {
        this.activity = activity;
        this.context = context;
    }

    @Override
    public void uncaughtException(Thread thread, Throwable ex) {

        try {
            Intent intent = new Intent(activity, IndexActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP
                    | Intent.FLAG_ACTIVITY_CLEAR_TASK
                    | Intent.FLAG_ACTIVITY_NEW_TASK);
            PendingIntent pendingIntent = PendingIntent.getActivity(
                    LockatedApplication.getInstance().getBaseContext(), 0, intent, intent.getFlags());
            //Following code will restart your application after 2 seconds
            AlarmManager mgr = (AlarmManager) LockatedApplication.getInstance().getBaseContext().getSystemService(Context.ALARM_SERVICE);
            mgr.set(AlarmManager.RTC, System.currentTimeMillis() + 1000, pendingIntent);
            //This will finish your activity manually
            activity.finish();
            //context.finish();
            //This will stop your application and take out from it.
            System.exit(2);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}