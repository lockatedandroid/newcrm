
package com.android.lockated.model.usermodel.EventModel.MyEvent;

import java.util.ArrayList;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class MyEvents {

    @SerializedName("classifieds")
    @Expose
    private ArrayList<Classified> classifieds = new ArrayList<Classified>();

    /**
     * 
     * @return
     *     The classifieds
     */
    public ArrayList<Classified> getClassifieds() {
        return classifieds;
    }

    /**
     * 
     * @param classifieds
     *     The classifieds
     */
    public void setClassifieds(ArrayList<Classified> classifieds) {
        this.classifieds = classifieds;
    }

}
