package com.android.lockated.crmadmin.adapter;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.lockated.R;
import com.android.lockated.crmadmin.activity.AdminPollsActivity;
import com.android.lockated.information.AccountController;
import com.android.lockated.model.AccountData;
import com.android.lockated.model.AdminModel.AdminPolls.Poll;
import com.android.lockated.network.ConnectionDetector;
import com.android.lockated.preferences.LockatedPreferences;
import com.android.lockated.request.LockatedJSONObjectRequest;
import com.android.lockated.request.LockatedRequestError;
import com.android.lockated.request.LockatedVolleyRequestQueue;
import com.android.lockated.utils.ApplicationURL;
import com.android.lockated.utils.Utilities;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

public class AdminPublishPollAdapter extends RecyclerView.Adapter<AdminPublishPollAdapter.AdminPublishPollsViewHolder>
        implements View.OnClickListener, Response.ErrorListener, Response.Listener<JSONObject> {

    private final ArrayList<Poll> polls;
    Context contextMain;
    JSONArray jsonArray;
    // ProgressBar progressBar;
    FragmentManager childFragmentManager;
    AccountController accountController;
    ArrayList<AccountData> datas;
    boolean alreadedVoted = false;
    ProgressDialog mProgressDialog;
    private RequestQueue mQueue;
    private LockatedPreferences mLockatedPreferences;
    public static final String REQUEST_TAG = "AdminPollAdapter";

    public AdminPublishPollAdapter(Context context, ArrayList<Poll> polls, FragmentManager childFragmentManager) {
        this.contextMain = context;
        this.polls = polls;
        this.childFragmentManager = childFragmentManager;
        accountController = AccountController.getInstance();
        datas = accountController.getmAccountDataList();

    }

    @Override
    public AdminPublishPollsViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(contextMain).inflate(R.layout.polls_publish_parent_view, parent, false);
        AdminPublishPollsViewHolder pvh = new AdminPublishPollsViewHolder(v);
        return pvh;
    }

    @Override
    public void onBindViewHolder(AdminPublishPollsViewHolder holder, int position) {

        if (polls.get(position).getSubject() != null) {
            holder.subject.setText(polls.get(position).getSubject().toString());
        } else {
            holder.subject.setText("No Subject");
        }
        if (polls.get(position).getEnd() != null) {
            holder.mExitDate.setText(Utilities.ddMonTimeFormat(polls.get(position).getEnd().toString()));
        } else {
            holder.mExitDate.setText("No Date");
        }
        if (polls.get(position).getEnd() != null) {
            holder.mStartDateTime.setText(Utilities.ddMonTimeFormat(polls.get(position).getStart().toString()));
        } else {
            holder.mStartDateTime.setText("No Date");
        }
        if ((polls.get(position).getUser().getFirstname() != null) && (polls.get(position).getUser().getLastname() != null)) {
            String name = polls.get(position).getUser().getFirstname()
                    + " " + polls.get(position).getUser().getLastname();
            holder.mPostedBy.setText(name);

        } else {
            holder.mPostedBy.setText("No Name");
        }
        if (polls.get(position).getFlat() != null) {
            holder.mFlatNo.setText(polls.get(position).getFlat());
        } else {
            holder.mFlatNo.setText("No Flat");
        }
        if (polls.get(position).getCreatedAt() != null) {
            holder.mCreatedAt.setText(Utilities.ddMonTimeFormat(polls.get(position).getCreatedAt()));
        } else {
            holder.mCreatedAt.setText("No Date");
        }

        if (polls.get(position).getPollOptions().size() == 1) {
            holder.secondChoice.setVisibility(View.INVISIBLE);
            holder.thirdChoice.setVisibility(View.INVISIBLE);
            holder.fourthChoice.setVisibility(View.INVISIBLE);
            // holder.secondOption.setVisibility(View.INVISIBLE);
            holder.firstChoice.setText(choiceNumber(contextMain, 1, polls.get(position).getPollOptions()
                    .get(0).getName()));
        } else if (polls.get(position).getPollOptions().size() == 2) {
            holder.thirdChoice.setVisibility(View.INVISIBLE);
            holder.fourthChoice.setVisibility(View.INVISIBLE);
            holder.firstChoice.setText(choiceNumber(contextMain, 1, polls.get(position).getPollOptions()
                    .get(0).getName()));
            holder.secondChoice.setText(choiceNumber(contextMain, 2, polls.get(position).getPollOptions()
                    .get(1).getName()));
        } else if (polls.get(position).getPollOptions().size() == 3) {
            holder.fourthChoice.setVisibility(View.INVISIBLE);
            holder.firstChoice.setText(choiceNumber(contextMain, 1, polls.get(position).getPollOptions()
                    .get(0).getName()));
            holder.secondChoice.setText(choiceNumber(contextMain, 2, polls.get(position).getPollOptions()
                    .get(1).getName()));
            holder.thirdChoice.setText(choiceNumber(contextMain, 3, polls.get(position).getPollOptions()
                    .get(2).getName()));
        } else if (polls.get(position).getPollOptions().size() == 4) {

            holder.firstChoice.setText(choiceNumber(contextMain, 1, polls.get(position).getPollOptions()
                    .get(0).getName()));
            holder.secondChoice.setText(choiceNumber(contextMain, 2, polls.get(position).getPollOptions()
                    .get(1).getName()));
            holder.thirdChoice.setText(choiceNumber(contextMain, 3, polls.get(position).getPollOptions()
                    .get(2).getName()));
            holder.fourthChoice.setText(choiceNumber(contextMain, 4, polls.get(position).getPollOptions()
                    .get(3).getName()));

        } else if (polls.get(position).getPollOptions().size() == 0) {

        }

        holder.firstOption.setTag(position);
        holder.firstOption.setOnClickListener(this);
        holder.secondOption.setTag(position);
        holder.secondOption.setOnClickListener(this);
    }

    public String choiceNumber(Context context, int number, String name) {
        if (number == 1)
            return context.getResources().getString(R.string.one) + name;
        else if (number == 2)
            return context.getResources().getString(R.string.two) + name;
        else if (number == 3)
            return context.getResources().getString(R.string.three) + name;
        else if (number == 4)
            return context.getResources().getString(R.string.four) + name;
        else
            return context.getResources().getString(R.string.one) + name;
    }

    @Override
    public int getItemCount() {
        return polls.size();
        //jsonArray.length();
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }

    @Override
    public void onClick(View v) {
        int pos = (int) v.getTag();
        switch (v.getId()) {

          /*  case R.id.firstOption:
                onPublishPoll(pos);
                break;*/
            case R.id.secondOption:
                onDenyPoll(pos);
                break;
        }
    }


    private void onDenyPoll(int pos) {
        //  progressBar.setVisibility(View.VISIBLE);
        mProgressDialog = ProgressDialog.show(contextMain, "", "Please Wait...", false);
        mProgressDialog.show();
        if (contextMain != null) {
            if (ConnectionDetector.isConnectedToInternet(contextMain)) {
                mLockatedPreferences = new LockatedPreferences(contextMain);
                mQueue = LockatedVolleyRequestQueue.getInstance(contextMain).getRequestQueue();
                LockatedJSONObjectRequest lockatedJSONObjectRequest = null;
                lockatedJSONObjectRequest = new LockatedJSONObjectRequest(Request.Method.GET,
                        ApplicationURL.DenyPoll + polls.get(pos).getId() + "&token=" + mLockatedPreferences.getLockatedToken(), null, this, this);

                lockatedJSONObjectRequest.setTag(REQUEST_TAG);
                mQueue.add(lockatedJSONObjectRequest);
            } else {
                Utilities.showToastMessage(contextMain, contextMain.getResources().getString(R.string.internet_connection_error));
            }
        }
    }

    /*private void onPublishPoll(int pos) {
        if (contextMain != null) {
            if (ConnectionDetector.isConnectedToInternet(contextMain)) {
                mLockatedPreferences = new LockatedPreferences(contextMain);
                mQueue = LockatedVolleyRequestQueue.getInstance(contextMain).getRequestQueue();
                LockatedJSONObjectRequest lockatedJSONObjectRequest = null;
                try {
                    lockatedJSONObjectRequest = new LockatedJSONObjectRequest(Request.Method.GET,
                            ApplicationURL.publishPoll + jsonArray.getJSONObject(pos).getString("id") + "&token=" + mLockatedPreferences.getLockatedToken(), null, this, this);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                try {
                    Log.e("Admin poll url", "" + ApplicationURL.publishPoll + jsonArray.getJSONObject(pos).getString("id") + "&token=" + mLockatedPreferences.getLockatedToken());
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                lockatedJSONObjectRequest.setTag(REQUEST_TAG);
                mQueue.add(lockatedJSONObjectRequest);
            } else {
                Utilities.showToastMessage(contextMain, contextMain.getResources().getString(R.string.internet_connection_error));
            }
        }
    }
*/
    public class AdminPublishPollsViewHolder extends RecyclerView.ViewHolder {

        TextView subject, firstChoice, secondChoice, thirdChoice, fourthChoice, firstOption,
                secondOption, alreadyVotedText, mExitDate;
        TextView mPostedBy, mFlatNo, mCreatedAt, mStartDateTime;
        LinearLayout options, voted;

        public AdminPublishPollsViewHolder(View itemView) {
            super(itemView);
            mPostedBy = (TextView) itemView.findViewById(R.id.mPostedBy);
            mFlatNo = (TextView) itemView.findViewById(R.id.mFlatNo);
            mCreatedAt = (TextView) itemView.findViewById(R.id.mCreatedAt);
            subject = (TextView) itemView.findViewById(R.id.subjectText);
            firstChoice = (TextView) itemView.findViewById(R.id.firstChoice);
            secondChoice = (TextView) itemView.findViewById(R.id.secondChoice);
            thirdChoice = (TextView) itemView.findViewById(R.id.thirdChoice);
            fourthChoice = (TextView) itemView.findViewById(R.id.fourthChoice);
            firstOption = (TextView) itemView.findViewById(R.id.firstOption);
            secondOption = (TextView) itemView.findViewById(R.id.secondOption);
            mExitDate = (TextView) itemView.findViewById(R.id.mExitDate);
            mStartDateTime = (TextView) itemView.findViewById(R.id.mStartDateTime);
            // alreadyVotedText = (TextView) itemView.findViewById(R.id.alreadyVoted);
            options = (LinearLayout) itemView.findViewById(R.id.optionsLinear);
            // voted = (LinearLayout) itemView.findViewById(R.id.alreadyVotedLinear);
        }
    }

    @Override
    public void onResponse(JSONObject response) {
        mProgressDialog.dismiss();
        if (response.has("id") && response.length() > 0) {
            Intent intent = new Intent(contextMain, AdminPollsActivity.class);
            intent.putExtra("AdminPollsActivity", 2);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            contextMain.startActivity(intent);
        }

    }

    @Override
    public void onErrorResponse(VolleyError error) {
        mProgressDialog.dismiss();
        if (contextMain != null) {
            LockatedRequestError.onRequestError(contextMain, error);
        }
    }

}