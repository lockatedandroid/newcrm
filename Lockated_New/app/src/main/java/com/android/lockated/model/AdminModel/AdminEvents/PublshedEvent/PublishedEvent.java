
package com.android.lockated.model.AdminModel.AdminEvents.PublshedEvent;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class PublishedEvent {

    @SerializedName("events")
    @Expose
    private ArrayList<Event> events = new ArrayList<Event>();
    @SerializedName("shared")
    @Expose
    private ArrayList<Object> shared = new ArrayList<Object>();

    /**
     * 
     * @return
     *     The events
     */
    public ArrayList<Event> getEvents() {
        return events;
    }

    /**
     * 
     * @param events
     *     The events
     */
    public void setEvents(ArrayList<Event> events) {
        this.events = events;
    }

    /**
     * 
     * @return
     *     The shared
     */
    public ArrayList<Object> getShared() {
        return shared;
    }

    /**
     * 
     * @param shared
     *     The shared
     */
    public void setShared(ArrayList<Object> shared) {
        this.shared = shared;
    }

}
