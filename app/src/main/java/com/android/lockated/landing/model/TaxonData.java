package com.android.lockated.landing.model;

import org.json.JSONObject;

public class TaxonData
{
    public int id;
    public String name;
    public String pretty_name;
    public String permalink;
    public int parent_id;
    public int taxonomy_id;

    public TaxonData(JSONObject jsonObject)
    {
        id = jsonObject.optInt("id");
        name = jsonObject.optString("name");
        pretty_name = jsonObject.optString("pretty_name");
        permalink = jsonObject.optString("permalink");
        parent_id = jsonObject.optInt("parent_id");
        taxonomy_id = jsonObject.optInt("taxonomy_id");
    }

    public int getId()
    {
        return id;
    }

    public String getName()
    {
        return name;
    }

    public String getPretty_name()
    {
        return pretty_name;
    }

    public String getPermalink()
    {
        return permalink;
    }

    public int getParent_id()
    {
        return parent_id;
    }

    public int getTaxonomy_id()
    {
        return taxonomy_id;
    }
}
