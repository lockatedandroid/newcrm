package com.android.lockated.crmadmin.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Spinner;
import android.widget.TextView;

import com.android.lockated.R;
import com.android.lockated.crmadmin.activity.AdminActivity;
import com.android.lockated.information.AccountController;
import com.android.lockated.model.AccountData;
import com.android.lockated.network.ConnectionDetector;
import com.android.lockated.preferences.LockatedPreferences;
import com.android.lockated.request.LockatedJSONObjectRequest;
import com.android.lockated.request.LockatedRequestError;
import com.android.lockated.request.LockatedVolleyRequestQueue;
import com.android.lockated.utils.Utilities;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

public class PendingClassifiedAdapter extends RecyclerView.Adapter<PendingClassifiedAdapter.PendingClassifiedViewHolder> implements Response.ErrorListener, Response.Listener<JSONObject> {
    Context context;
    JSONArray jsonArray;
    private RequestQueue mQueue;
    private LockatedPreferences mLockatedPreferences;
    private AccountController accountController;
    ArrayList<AccountData> accountDataArrayList;
    int societyId;
    private String Creatername;
    public static final String REQUEST_TAG = "PendingClassifiedAdapter";

    public PendingClassifiedAdapter(Context context, JSONArray jsonArray) {

        this.context = context;
        this.jsonArray = jsonArray;

    }

    @Override
    public PendingClassifiedViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(context).inflate(R.layout.admin_pending_classified_child, parent, false);
        PendingClassifiedViewHolder pvh = new PendingClassifiedViewHolder(v);
        return pvh;
    }

    @Override
    public void onBindViewHolder(PendingClassifiedViewHolder holder, final int position) {
        accountController = AccountController.getInstance();
        Creatername = accountController.getmAccountDataList().get(0).getFirstname() + " " +
                accountController.getmAccountDataList().get(0).getLastname();
        accountDataArrayList = accountController.getmAccountDataList();
        societyId = accountDataArrayList.get(0).getmResidenceDataList().get(0).getSociety_id();

        try {
            holder.mIssueType.setText(jsonArray.getJSONObject(position).getString("issue_type"));
            holder.mIssueDescription.setText(jsonArray.getJSONObject(position).getString("text"));
            holder.mIssueCategory.setText(jsonArray.getJSONObject(position).getString("heading"));
            holder.mPostedOn.setText(ddMMYYYFormat(jsonArray.getJSONObject(position).getString("created_at")));
            holder.mpostedBy.setText(jsonArray.getJSONObject(position).getString(Creatername));
            holder.mUpdatedOn.setText(ddMMYYYFormat(jsonArray.getJSONObject(position).getString("updated_at")));

        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    @Override
    public int getItemCount() {
        return jsonArray.length();
    }

    @Override
    public void onErrorResponse(VolleyError error) {
        if (context != null) {
            LockatedRequestError.onRequestError(context, error);
        }
    }

    @Override
    public void onResponse(JSONObject response) {

        if (context != null) {
            try {
                if (response != null && response.length() > 0) {
                    Intent intent = new Intent(context, AdminActivity.class);
                    intent.putExtra("AdminNotice", 2);
                    context.startActivity(intent);
                } else {
                }

            } catch (Exception e) {
                e.printStackTrace();
            }
        }

    }

    public class PendingClassifiedViewHolder extends RecyclerView.ViewHolder implements AdapterView.OnItemSelectedListener {

        CardView cardView;
        TextView mIssueType, mIssueDescription, mIssueCategory, mpostedBy, mPostedOn, mUpdatedOn;
        Spinner mSpinnerStatus;

        public PendingClassifiedViewHolder(View itemView) {
            super(itemView);

            mIssueType = (TextView) itemView.findViewById(R.id.mIssueType);//personal/socity//issue_type
            mIssueDescription = (TextView) itemView.findViewById(R.id.mIssueDescription);//text
            mIssueCategory = (TextView) itemView.findViewById(R.id.mIssueCategory);//heading
            mSpinnerStatus = (Spinner) itemView.findViewById(R.id.mSpinnerStatus);
            mpostedBy = (TextView) itemView.findViewById(R.id.mpostedBy);//Created_By
            mPostedOn = (TextView) itemView.findViewById(R.id.mPostedOn);//create date
            mUpdatedOn = (TextView) itemView.findViewById(R.id.mUpdatedOn);
            mSpinnerStatus.setOnItemSelectedListener(this);
        }

        @Override
        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {


        }

        @Override
        public void onNothingSelected(AdapterView<?> parent) {

        }
    }

    public void request(JSONObject jsonObject, String url) {
        if (context != null) {
            if (ConnectionDetector.isConnectedToInternet(context)) {
                mLockatedPreferences = new LockatedPreferences(context);
                mQueue = LockatedVolleyRequestQueue.getInstance(context).getRequestQueue();
                LockatedJSONObjectRequest lockatedJSONObjectRequest = new LockatedJSONObjectRequest(Request.Method.GET,
                        url + "&token=" +/*"b5b820ed25016be788a7d40c7012d98a9a9513072d3df523"*/ mLockatedPreferences.getLockatedToken(), jsonObject, this, this);
                lockatedJSONObjectRequest.setTag(REQUEST_TAG);
                mQueue.add(lockatedJSONObjectRequest);
            } else {
                Utilities.showToastMessage(context, context.getResources().getString(R.string.internet_connection_error));
            }
        }

    }

    public String ddMMYYYFormat(String dateStr) {

        if (dateStr != "null") {
            DateFormat format = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);
            Date date = null;
            try {
                date = format.parse(dateStr);
            } catch (ParseException e) {
                e.printStackTrace();
            }
            dateStr = new SimpleDateFormat("dd-MM-yyyy").format(date);
        } else {
            dateStr = "No Date";
        }
        return dateStr;
    }

}
