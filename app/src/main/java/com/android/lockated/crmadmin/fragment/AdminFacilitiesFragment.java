package com.android.lockated.crmadmin.fragment;


import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import com.android.lockated.R;
import com.android.lockated.crm.adapters.CommanListViewAdapter;
import com.android.lockated.crmadmin.activity.AdminFacilitiesActivity;
import com.android.lockated.utils.Utilities;

public class AdminFacilitiesFragment extends Fragment implements AdapterView.OnItemClickListener {

    ListView listNotice;
    String[] nameList = {"Requested Facilities", "Facilities list"};
    CommanListViewAdapter commanListViewAdapter;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        Utilities.ladooIntegration(getActivity(), "Admin Facilities List");
        View allNoticeView = inflater.inflate(R.layout.comman_listview, container, false);
        init(allNoticeView);
        return allNoticeView;
    }

    private void init(View allNoticeView) {
        listNotice = (ListView) allNoticeView.findViewById(R.id.noticeList);
        commanListViewAdapter = new CommanListViewAdapter(getActivity(), nameList);
        listNotice.setAdapter(commanListViewAdapter);
        listNotice.setOnItemClickListener(this);
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        Intent intent = new Intent(getActivity(), AdminFacilitiesActivity.class);
        intent.putExtra("AdminFacilitiesActivity", position);
        startActivity(intent);
    }

}
