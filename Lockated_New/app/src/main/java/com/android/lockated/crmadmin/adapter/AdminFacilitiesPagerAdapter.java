package com.android.lockated.crmadmin.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.android.lockated.crmadmin.fragment.AdminBookedFacilitiesFragment;
import com.android.lockated.crmadmin.fragment.AdminFacilitiesListFragment;


public class AdminFacilitiesPagerAdapter extends FragmentPagerAdapter {
    int tabCount, itemPosition;
    FragmentManager fragmentManager;
    String[] nameList;

    public AdminFacilitiesPagerAdapter(FragmentManager fm, int tabCount, int itemPosition, String[] nameList) {
        super(fm);
        this.tabCount = tabCount;
        fragmentManager = fm;
        this.itemPosition = itemPosition;
        this.nameList = nameList;
    }


    @Override
    public Fragment getItem(int pos) {

        Fragment fragment;

        switch (pos) {

            case 0:
                fragment = new AdminBookedFacilitiesFragment();
                break;
            case 1:
                fragment = new AdminFacilitiesListFragment();
                break;
            default:
                fragment = new AdminBookedFacilitiesFragment();
                break;
        }
        return fragment;
    }

    @Override
    public int getCount() {
        return tabCount;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return nameList[itemPosition];
    }

    @Override
    public int getItemPosition(Object object) {
        return super.getItemPosition(object);
    }

    @Override
    public long getItemId(int position) {
        return super.getItemId(position);
    }

}