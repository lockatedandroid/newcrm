package com.android.lockated.model.OlaCab.OlaTrackResponse;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class TrackResponse {

    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("request_type")
    @Expose
    private String requestType;
    @SerializedName("booking_status")
    @Expose
    private String bookingStatus;
    @SerializedName("booking_id")
    @Expose
    private String bookingId;
    @SerializedName("duration")
    @Expose
    private Duration duration;
    @SerializedName("distance")
    @Expose
    private Distance distance;
    @SerializedName("driver_lat")
    @Expose
    private float driverLat;
    @SerializedName("driver_lng")
    @Expose
    private float driverLng;
    @SerializedName("share_ride_url")
    @Expose
    private String shareRideUrl;
    @SerializedName("ola_money_balance")
    @Expose
    private int olaMoneyBalance;
    @SerializedName("trip_info")
    @Expose
    private TripInfo tripInfo;
    @SerializedName("fare_breakup")
    @Expose
    private ArrayList<FareBreakup> fareBreakup = new ArrayList<FareBreakup>();

    /**
     * 
     * @return
     *     The status
     */
    public String getStatus() {
        return status;
    }

    /**
     * 
     * @param status
     *     The status
     */
    public void setStatus(String status) {
        this.status = status;
    }

    /**
     * 
     * @return
     *     The requestType
     */
    public String getRequestType() {
        return requestType;
    }

    /**
     * 
     * @param requestType
     *     The request_type
     */
    public void setRequestType(String requestType) {
        this.requestType = requestType;
    }

    /**
     * 
     * @return
     *     The bookingStatus
     */
    public String getBookingStatus() {
        return bookingStatus;
    }

    /**
     * 
     * @param bookingStatus
     *     The booking_status
     */
    public void setBookingStatus(String bookingStatus) {
        this.bookingStatus = bookingStatus;
    }

    /**
     * 
     * @return
     *     The bookingId
     */
    public String getBookingId() {
        return bookingId;
    }

    /**
     * 
     * @param bookingId
     *     The booking_id
     */
    public void setBookingId(String bookingId) {
        this.bookingId = bookingId;
    }

    /**
     * 
     * @return
     *     The duration
     */
    public Duration getDuration() {
        return duration;
    }

    /**
     * 
     * @param duration
     *     The duration
     */
    public void setDuration(Duration duration) {
        this.duration = duration;
    }

    /**
     * 
     * @return
     *     The distance
     */
    public Distance getDistance() {
        return distance;
    }

    /**
     * 
     * @param distance
     *     The distance
     */
    public void setDistance(Distance distance) {
        this.distance = distance;
    }

    /**
     * 
     * @return
     *     The driverLat
     */
    public float getDriverLat() {
        return driverLat;
    }

    /**
     * 
     * @param driverLat
     *     The driver_lat
     */
    public void setDriverLat(float driverLat) {
        this.driverLat = driverLat;
    }

    /**
     * 
     * @return
     *     The driverLng
     */
    public float getDriverLng() {
        return driverLng;
    }

    /**
     * 
     * @param driverLng
     *     The driver_lng
     */
    public void setDriverLng(float driverLng) {
        this.driverLng = driverLng;
    }

    /**
     * 
     * @return
     *     The shareRideUrl
     */
    public String getShareRideUrl() {
        return shareRideUrl;
    }

    /**
     * 
     * @param shareRideUrl
     *     The share_ride_url
     */
    public void setShareRideUrl(String shareRideUrl) {
        this.shareRideUrl = shareRideUrl;
    }

    /**
     * 
     * @return
     *     The olaMoneyBalance
     */
    public int getOlaMoneyBalance() {
        return olaMoneyBalance;
    }

    /**
     * 
     * @param olaMoneyBalance
     *     The ola_money_balance
     */
    public void setOlaMoneyBalance(int olaMoneyBalance) {
        this.olaMoneyBalance = olaMoneyBalance;
    }

    /**
     * 
     * @return
     *     The tripInfo
     */
    public TripInfo getTripInfo() {
        return tripInfo;
    }

    /**
     * 
     * @param tripInfo
     *     The trip_info
     */
    public void setTripInfo(TripInfo tripInfo) {
        this.tripInfo = tripInfo;
    }

    /**
     * 
     * @return
     *     The fareBreakup
     */
    public ArrayList<FareBreakup> getFareBreakup() {
        return fareBreakup;
    }

    /**
     * 
     * @param fareBreakup
     *     The fare_breakup
     */
    public void setFareBreakup(ArrayList<FareBreakup> fareBreakup) {
        this.fareBreakup = fareBreakup;
    }

}
