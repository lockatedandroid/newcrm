package com.android.lockated.crm.fragment.myzone.directory;


import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.lockated.R;
import com.android.lockated.crm.activity.MyZoneDirectoryActivity;
import com.android.lockated.information.AccountController;
import com.android.lockated.model.AccountData;
import com.android.lockated.network.ConnectionDetector;
import com.android.lockated.preferences.LockatedPreferences;
import com.android.lockated.request.LockatedJSONObjectRequest;
import com.android.lockated.request.LockatedRequestError;
import com.android.lockated.request.LockatedVolleyRequestQueue;
import com.android.lockated.utils.ApplicationURL;
import com.android.lockated.utils.Utilities;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class AddToDirectory extends DialogFragment implements View.OnClickListener, Response.Listener<JSONObject>, Response.ErrorListener {
    int societyId;
    Window window;
    String pid = null;
    private EditText mFirstName;
    private EditText mLastName;
    private EditText mTypeOfService;
    private EditText mPhoneNumber;
    private EditText mEmailId;
    private TextView mUpdate;
    private ImageView mImageClose;
    private TextView mSubmitContact;
    AccountController accountController;
    ArrayList<AccountData> accountDataArrayList;
    private View AddToDirectoryView;
    private RequestQueue mQueue;
    private ProgressBar mProgressBar;

    private LockatedPreferences mLockatedPreferences;
    public static final String REQUEST_TAG = "AddToDirectory";

    public AddToDirectory() {

    }

    @Override
    public void onStart() {
        super.onStart();

        window = getDialog().getWindow();
        WindowManager.LayoutParams windowParams = window.getAttributes();
        windowParams.dimAmount = 0.75f;
        windowParams.flags |= WindowManager.LayoutParams.FLAG_DIM_BEHIND;
        window.setAttributes(windowParams);
        window.setBackgroundDrawableResource(android.R.color.white);

    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        AddToDirectoryView = getActivity().getLayoutInflater().inflate(R.layout.fragment_add_to_directory,
                new LinearLayout(getActivity()), false);

        init(AddToDirectoryView);

        Dialog builder = new Dialog(getActivity());
        builder.requestWindowFeature(Window.FEATURE_NO_TITLE);
        builder.getWindow().setBackgroundDrawable(new ColorDrawable(Color.WHITE));

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(builder.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.WRAP_CONTENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        builder.show();
        builder.getWindow().setAttributes(lp);
        builder.setContentView(AddToDirectoryView);

        return builder;
    }

    private void init(View addToDirectoryView) {
        pid = (getArguments().getString("pid"));
        mLockatedPreferences = new LockatedPreferences(getActivity());
        mProgressBar = (ProgressBar) addToDirectoryView.findViewById(R.id.mProgressBarSmallView);
        mFirstName = (EditText) addToDirectoryView.findViewById(R.id.mFirstName);
        mLastName = (EditText) addToDirectoryView.findViewById(R.id.mLastName);
        mPhoneNumber = (EditText) addToDirectoryView.findViewById(R.id.mPhoneNumber);
        mEmailId = (EditText) addToDirectoryView.findViewById(R.id.mEmailId);
        mUpdate = (TextView) addToDirectoryView.findViewById(R.id.mUpdate);
        mSubmitContact = (TextView) addToDirectoryView.findViewById(R.id.mSubmitContact);
        mImageClose = (ImageView) addToDirectoryView.findViewById(R.id.mImageClose);
        mUpdate.setOnClickListener(this);
        mSubmitContact.setOnClickListener(this);
        mImageClose.setOnClickListener(this);
        if (pid != null) {
            mFirstName.setText(getArguments().getString("firstname"));
            mLastName.setText(getArguments().getString("lastname"));
            mPhoneNumber.setText(getArguments().getString("mobile"));
            mEmailId.setText(getArguments().getString("email"));
            mUpdate.setVisibility(View.VISIBLE);
            mSubmitContact.setVisibility(View.GONE);
        } else {
            mUpdate.setVisibility(View.GONE);
            mSubmitContact.setVisibility(View.VISIBLE);
        }

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.mUpdate:
                updateDirectory();
                break;
            case R.id.mSubmitContact:
                submitContact();
                break;
            case R.id.mImageClose:
                dismiss();
                break;
        }
    }

    private void updateDirectory() {
        String fName = mFirstName.getText().toString();
        String lName = mLastName.getText().toString();
        String strPhoneNumber = mPhoneNumber.getText().toString();
        String strEmailId = mEmailId.getText().toString();
        mLockatedPreferences = new LockatedPreferences(getActivity());
        mProgressBar.setVisibility(View.VISIBLE);
        if (TextUtils.isEmpty(fName)) {
            Toast.makeText(getActivity(), "Please Enter First Name", Toast.LENGTH_SHORT).show();
        } else if (TextUtils.isEmpty(lName)) {
            Toast.makeText(getActivity(), "Please Enter Last Name", Toast.LENGTH_SHORT).show();

        } else if (TextUtils.isEmpty(strPhoneNumber)) {
            Toast.makeText(getActivity(), "Please Enter Contact Number", Toast.LENGTH_SHORT).show();
        }/* else if (TextUtils.isEmpty(strEmailId)) {
            Toast.makeText(getActivity(), "Please Enter Email", Toast.LENGTH_SHORT).show();

        }*/ else {
            if (pid != null) {
                JSONObject jsonObjectMain = new JSONObject();
                JSONObject jsonObject = new JSONObject();
                try {
                    jsonObject.put("firstname", fName);
                    jsonObject.put("lastname", lName);
                    jsonObject.put("mobile", strPhoneNumber);
                    jsonObject.put("email", strEmailId);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                try {
                    mQueue = LockatedVolleyRequestQueue.getInstance(getActivity()).getRequestQueue();
                    LockatedJSONObjectRequest lockatedJSONObjectRequest = new LockatedJSONObjectRequest(Request.Method.PUT,
                            ApplicationURL.UpdateDirectory + pid + ".json?token="
                                    + mLockatedPreferences.getLockatedToken(),
                            jsonObject, new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject response) {
                            mProgressBar.setVisibility(View.GONE);
                            if (response.has("id")) {
                                Intent intent = new Intent(getActivity(), MyZoneDirectoryActivity.class);
                                intent.putExtra("Listposition", 0);
                                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                getActivity().startActivity(intent);
                                getActivity().finish();
                            } else {
                                Toast.makeText(getContext(), "Some Thing Went Wrong", Toast.LENGTH_SHORT).show();

                            }
                        }


                    }, new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            mProgressBar.setVisibility(View.GONE);
                            if (getActivity() != null) {
                                LockatedRequestError.onRequestError(getActivity(), error);
                            }
                        }
                    });
                    lockatedJSONObjectRequest.setTag(REQUEST_TAG);
                    mQueue.add(lockatedJSONObjectRequest);
                } catch (Exception e) {
                    e.printStackTrace();
                }

            } else {
                Toast.makeText(getActivity(), "No Data To Display", Toast.LENGTH_SHORT).show();
            }
        }
    }

    private void submitContact() {
        String fName = mFirstName.getText().toString();
        String lName = mLastName.getText().toString();
        //  String strTypeOfService = mTypeOfService.getText().toString();
        String strPhoneNumber = mPhoneNumber.getText().toString();
        String strEmailId = mEmailId.getText().toString();

        accountController = AccountController.getInstance();
        accountDataArrayList = accountController.getmAccountDataList();
        societyId = accountDataArrayList.get(0).getmResidenceDataList().get(0).getSociety_id();

        if (TextUtils.isEmpty(fName)) {
            Toast.makeText(getActivity(), "Please Enter First Name", Toast.LENGTH_SHORT).show();
        } else if (TextUtils.isEmpty(lName)) {
            Toast.makeText(getActivity(), "Please Enter Last Name", Toast.LENGTH_SHORT).show();
        } else if (TextUtils.isEmpty(strPhoneNumber)) {
            Toast.makeText(getActivity(), "Please Enter Contact Number", Toast.LENGTH_SHORT).show();
        } else {
            if (ConnectionDetector.isConnectedToInternet(getActivity())) {
                mProgressBar.setVisibility(View.VISIBLE);
                JSONObject jsonObjectMain = new JSONObject();
                JSONObject jsonObject = new JSONObject();
                try {
                    jsonObject.put("id_society", societyId);
                    jsonObject.put("firstname", fName);
                    jsonObject.put("lastname", lName);
                    jsonObject.put("mobile", strPhoneNumber);
                    jsonObject.put("user_society_id", "" + mLockatedPreferences.getUserSocietyId());
                    jsonObject.put("email", strEmailId);
                    jsonObjectMain.put("directory", jsonObject);
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                try {
                    mQueue = LockatedVolleyRequestQueue.getInstance(getActivity()).getRequestQueue();
                    LockatedJSONObjectRequest lockatedJSONObjectRequest = new LockatedJSONObjectRequest(Request.Method.POST,
                            ApplicationURL.addDirectory + mLockatedPreferences.getLockatedToken(), jsonObjectMain, this, this);
                    lockatedJSONObjectRequest.setTag(REQUEST_TAG);
                    mQueue.add(lockatedJSONObjectRequest);
                } catch (Exception e) {
                    e.printStackTrace();
                }

            } else {
                Utilities.showToastMessage(getActivity(), getActivity().getResources().getString(R.string.internet_connection_error));
            }

        }
    }

    @Override
    public void onResponse(JSONObject response) {

        mProgressBar.setVisibility(View.GONE);
        if (response.has("id")) {
            Intent intent = new Intent(getActivity(), MyZoneDirectoryActivity.class);
            intent.putExtra("Listposition", 0);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            getActivity().startActivity(intent);
        } else {
            Utilities.showToastMessage(getContext(), "Some Thing Went Wrong");

        }

    }

    @Override
    public void onErrorResponse(VolleyError error) {
        mProgressBar.setVisibility(View.GONE);
        if (getActivity() != null) {
            LockatedRequestError.onRequestError(getActivity(), error);
        }
    }


}
