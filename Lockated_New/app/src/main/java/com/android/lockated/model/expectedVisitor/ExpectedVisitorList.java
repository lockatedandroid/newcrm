
package com.android.lockated.model.expectedVisitor;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class ExpectedVisitorList {

    @SerializedName("code")
    @Expose
    private int code;
    @SerializedName("gtype")
    @Expose
    private String gtype;
    @SerializedName("gatekeepers")
    @Expose
    private ArrayList<GatekeeperExpected> gatekeepers = new ArrayList<GatekeeperExpected>();

    /**
     * 
     * @return
     *     The code
     */
    public int getCode() {
        return code;
    }

    /**
     * 
     * @param code
     *     The code
     */
    public void setCode(int code) {
        this.code = code;
    }

    /**
     * 
     * @return
     *     The gtype
     */
    public String getGtype() {
        return gtype;
    }

    /**
     * 
     * @param gtype
     *     The gtype
     */
    public void setGtype(String gtype) {
        this.gtype = gtype;
    }

    /**
     * 
     * @return
     *     The gatekeepers
     */
    public List<GatekeeperExpected> getGatekeepers() {
        return gatekeepers;
    }

    /**
     * 
     * @param gatekeepers
     *     The gatekeepers
     */
    public void setGatekeepers(ArrayList<GatekeeperExpected> gatekeepers) {
        this.gatekeepers = gatekeepers;
    }

}
