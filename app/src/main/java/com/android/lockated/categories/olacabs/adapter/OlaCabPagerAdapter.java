package com.android.lockated.categories.olacabs.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.util.Log;

import com.android.lockated.categories.olacabs.fragment.CheapestOlaRideFragment;

public class OlaCabPagerAdapter extends FragmentPagerAdapter {

    FragmentManager fm ;
    CharSequence Titles[];
    int NumbOfTabs;

    public OlaCabPagerAdapter(FragmentManager fm, CharSequence mTitles[], int mNumbOfTabsumb) {
        super(fm);
        this.fm= fm;
        this.Titles = mTitles;
        this.NumbOfTabs = mNumbOfTabsumb;
    }

    @Override
    public Fragment getItem(int position)
    {
        Fragment fragment = null;
        if (position == 0)
        {
            fragment = new CheapestOlaRideFragment();
            Log.e("CheapestOlaRideFragment","YES ");
        } /*else
        {
            fragment = new NearestOlaRideFragment();
            Log.e("NearestOlaRideFragment","YES");
        }*/
        return fragment;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return Titles[position];
    }

    @Override
    public int getCount() {
        return NumbOfTabs;
    }
}