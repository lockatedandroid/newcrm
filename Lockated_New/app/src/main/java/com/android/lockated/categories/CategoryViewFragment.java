package com.android.lockated.categories;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.android.lockated.R;
import com.android.lockated.categories.adapter.CategoryTabAdapter;
import com.android.lockated.component.LockatedPagerSlidingTabStrip;
import com.android.lockated.landing.model.TaxonomiesData;
import com.android.lockated.utils.Utilities;

import java.util.ArrayList;

public class CategoryViewFragment extends Fragment {
    private View mCategoryView;
    private ViewPager mViewPagerDetail;
    private LockatedPagerSlidingTabStrip mViewPagerSlidingTabs;
    private CategoryTabAdapter mCategoryTabAdapter;

    private int mPosition;

    private ArrayList<TaxonomiesData> mTaxonomiesDataList;

    int taxon_id, otype_id;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mCategoryView = inflater.inflate(R.layout.fragment_account, container, false);
        init();
        Utilities.ladooIntegration(getActivity(), "Category View");
        return mCategoryView;
    }

    public void setTaxonomiesData(ArrayList<TaxonomiesData> mTaxonomiesData, int pos, int taxon_id, int otype_id) {
        mTaxonomiesDataList = mTaxonomiesData;
        mPosition = pos;
        this.taxon_id = taxon_id;
        this.otype_id = otype_id;
    }

    private void init() {
        mViewPagerDetail = (ViewPager) mCategoryView.findViewById(R.id.mViewPagerDetail);
        mViewPagerSlidingTabs = (LockatedPagerSlidingTabStrip) mCategoryView.findViewById(R.id.mViewPagerSlidingTabs);

        mViewPagerSlidingTabs.setIndicatorColor(ContextCompat.getColor(getActivity(), R.color.primary));
        mViewPagerSlidingTabs.setTextColor(ContextCompat.getColor(getActivity(), R.color.primary_text));
        mViewPagerSlidingTabs.setActivateTextColor(ContextCompat.getColor(getActivity(), R.color.primary_text));
        mViewPagerSlidingTabs.setDeactivateTextColor(ContextCompat.getColor(getActivity(), R.color.primary_text));

        if (mTaxonomiesDataList != null) {
            mCategoryTabAdapter = new CategoryTabAdapter(getActivity(), getChildFragmentManager(), mTaxonomiesDataList, mPosition, taxon_id, otype_id);
            mViewPagerDetail.setAdapter(mCategoryTabAdapter);
            mViewPagerDetail.setCurrentItem(mPosition);
            mViewPagerSlidingTabs.setViewPager(mViewPagerDetail);
        }
    }
}