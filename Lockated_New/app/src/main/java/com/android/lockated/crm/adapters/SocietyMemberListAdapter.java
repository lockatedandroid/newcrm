package com.android.lockated.crm.adapters;


import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.android.lockated.R;

import org.json.JSONArray;
import org.json.JSONException;

public class SocietyMemberListAdapter extends BaseAdapter {

    LayoutInflater layoutInflater;
    JSONArray jsonArray;

    public SocietyMemberListAdapter(Context context, JSONArray jsonArray) {
        layoutInflater = LayoutInflater.from(context);
        this.jsonArray = jsonArray;
    }

    @Override
    public int getCount() {
        return jsonArray.length();
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        MemberViewHolder memberViewHolder = null;
        if (convertView == null) {
            memberViewHolder = new MemberViewHolder();
            convertView = layoutInflater.inflate(R.layout.member_list_view, parent, false);

            memberViewHolder.name = (TextView) convertView.findViewById(R.id.nameValue);
            memberViewHolder.flatNo = (TextView) convertView.findViewById(R.id.flatNoValue);
            memberViewHolder.phoneNo = (TextView) convertView.findViewById(R.id.phoneNoValue);
            memberViewHolder.emailId = (TextView) convertView.findViewById(R.id.emailIdValue);
            memberViewHolder.designationValue = (TextView) convertView.findViewById(R.id.designationValue);

            convertView.setTag(memberViewHolder);

        } else {
            memberViewHolder = (MemberViewHolder) convertView.getTag();
        }

        try {
            if (jsonArray.getJSONObject(position).getJSONObject("user") != null) {
                String name = jsonArray.getJSONObject(position).getJSONObject("user").getString("firstname")
                        + " " + jsonArray.getJSONObject(position).getJSONObject("user").getString("lastname");
                memberViewHolder.name.setText(checkValue(name));
                memberViewHolder.phoneNo.setText(checkValue(jsonArray.getJSONObject(position).getJSONObject("user").getString("mobile")));
            } else {
                memberViewHolder.name.setText("No Name");
                memberViewHolder.phoneNo.setText("No Number");
            }
            if (jsonArray.getJSONObject(position).getJSONObject("user_flat").has("flat")) {
                memberViewHolder.flatNo.setText(checkValue(jsonArray.getJSONObject(position).getJSONObject("user_flat").getString("flat")));
            } else {
                memberViewHolder.flatNo.setText("No Flat No");
            }
            if (jsonArray.getJSONObject(position).getJSONObject("user").has("email")) {
                memberViewHolder.emailId.setText(checkValue(jsonArray.getJSONObject(position).getJSONObject("user").getString("email")));
            } else {
                memberViewHolder.emailId.setText("No Email");
            }
            memberViewHolder.designationValue.setText(jsonArray.getJSONObject(position).getString("status"));
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return convertView;
    }

    class MemberViewHolder {
        TextView name, designation, flatNo, phoneNo, emailId, designationValue;
    }

    public String checkValue(String valueCheck) {

        if (valueCheck != null && !valueCheck.equals("null")) {
            return valueCheck;
        } else {
            return "No Data";
        }
    }

}
