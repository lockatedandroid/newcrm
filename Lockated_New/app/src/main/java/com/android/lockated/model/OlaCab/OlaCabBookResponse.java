
package com.android.lockated.model.OlaCab;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class OlaCabBookResponse {

    @SerializedName("share_ride_url")
    @Expose
    private String shareRideUrl;
    @SerializedName("eta")
    @Expose
    private int eta;
    @SerializedName("driver_number")
    @Expose
    private String driverNumber;
    @SerializedName("booking_id")
    @Expose
    private String bookingId;
    @SerializedName("driver_lng")
    @Expose
    private float driverLng;
    @SerializedName("driver_name")
    @Expose
    private String driverName;
    @SerializedName("car_model")
    @Expose
    private String carModel;
    @SerializedName("crn")
    @Expose
    private String crn;
    @SerializedName("cab_number")
    @Expose
    private String cabNumber;
    @SerializedName("driver_lat")
    @Expose
    private float driverLat;
    @SerializedName("cab_type")
    @Expose
    private String cabType;

    @SerializedName("surcharge_value")
    @Expose
    private String surchargeValue;


    /**
     * 
     * @return
     *     The shareRideUrl
     */
    public String getShareRideUrl() {
        return shareRideUrl;
    }

    /**
     * 
     * @param shareRideUrl
     *     The share_ride_url
     */
    public void setShareRideUrl(String shareRideUrl) {
        this.shareRideUrl = shareRideUrl;
    }

    /**
     * 
     * @return
     *     The eta
     */
    public int getEta() {
        return eta;
    }

    /**
     * 
     * @param eta
     *     The eta
     */
    public void setEta(int eta) {
        this.eta = eta;
    }

    /**
     * 
     * @return
     *     The driverNumber
     */
    public String getDriverNumber() {
        return driverNumber;
    }

    /**
     * 
     * @param driverNumber
     *     The driver_number
     */
    public void setDriverNumber(String driverNumber) {
        this.driverNumber = driverNumber;
    }

    /**
     * 
     * @return
     *     The bookingId
     */
    public String getBookingId() {
        return bookingId;
    }

    /**
     * 
     * @param bookingId
     *     The booking_id
     */
    public void setBookingId(String bookingId) {
        this.bookingId = bookingId;
    }

    /**
     * 
     * @return
     *     The driverLng
     */
    public float getDriverLng() {
        return driverLng;
    }

    /**
     * 
     * @param driverLng
     *     The driver_lng
     */
    public void setDriverLng(float driverLng) {
        this.driverLng = driverLng;
    }

    /**
     * 
     * @return
     *     The driverName
     */
    public String getDriverName() {
        return driverName;
    }

    /**
     * 
     * @param driverName
     *     The driver_name
     */
    public void setDriverName(String driverName) {
        this.driverName = driverName;
    }

    /**
     * 
     * @return
     *     The carModel
     */
    public String getCarModel() {
        return carModel;
    }

    /**
     * 
     * @param carModel
     *     The car_model
     */
    public void setCarModel(String carModel) {
        this.carModel = carModel;
    }

    /**
     * 
     * @return
     *     The crn
     */
    public String getCrn() {
        return crn;
    }

    /**
     * 
     * @param crn
     *     The crn
     */
    public void setCrn(String crn) {
        this.crn = crn;
    }

    /**
     * 
     * @return
     *     The cabNumber
     */
    public String getCabNumber() {
        return cabNumber;
    }

    /**
     * 
     * @param cabNumber
     *     The cab_number
     */
    public void setCabNumber(String cabNumber) {
        this.cabNumber = cabNumber;
    }

    /**
     * 
     * @return
     *     The driverLat
     */
    public float getDriverLat() {
        return driverLat;
    }

    /**
     * 
     * @param driverLat
     *     The driver_lat
     */
    public void setDriverLat(float driverLat) {
        this.driverLat = driverLat;
    }

    /**
     * 
     * @return
     *     The cabType
     */
    public String getCabType() {
        return cabType;
    }

    /**
     * 
     * @param cabType
     *     The cab_type
     */
    public void setCabType(String cabType) {
        this.cabType = cabType;
    }



    /**
     *
     * @return
     *     The surchargeValue
     */
    public String getSurchargeValue() {
        return surchargeValue;
    }

    /**
     *
     * @param surchargeValue
     *     The surcharge_value
     */
    public void setSurchargeValue(String surchargeValue) {
        this.surchargeValue = surchargeValue;
    }

}
