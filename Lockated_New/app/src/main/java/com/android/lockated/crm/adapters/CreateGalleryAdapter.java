package com.android.lockated.crm.adapters;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.lockated.Interfaces.IRecyclerItemClickListener;
import com.android.lockated.R;
import com.android.lockated.holder.RecyclerViewHolder;
import com.android.lockated.utils.LockatedConfig;
import com.android.lockated.utils.ShowImage;
import com.android.lockated.utils.Utilities;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class CreateGalleryAdapter extends RecyclerView.Adapter<CreateGalleryAdapter.CreateGalleryViewHolder> implements View.OnClickListener {

    Context context;
    Activity activity;
    private int adapterItems;
    private IRecyclerItemClickListener mIRecyclerItemClickListener;
    String encodedImage, mCurrentPhotoPath;
    boolean imageSet = false;

    public CreateGalleryAdapter(int adapterItems, IRecyclerItemClickListener listener, Context context, Activity activity) {
        this.context = context;
        this.activity = activity;
        this.adapterItems = adapterItems;
        mIRecyclerItemClickListener = listener;
    }

    @Override
    public CreateGalleryViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.create_gallery_row, parent, false);
        return new CreateGalleryViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(CreateGalleryViewHolder holder, int position) {
        holder.mImageViewCategory.setImageResource(R.drawable.ic_account_camera);
        holder.mImageViewCategory.setOnClickListener(this);
    }

    @Override
    public int getItemCount() {
        return adapterItems;
    }

    @Override
    public void onClick(View v) {
        //selectImage(v.getId());
    }


    public class CreateGalleryViewHolder extends RecyclerView.ViewHolder {

        ImageView mImageViewCategory;

        public CreateGalleryViewHolder(View itemView) {
            super(itemView);
            mImageViewCategory = (ImageView) itemView.findViewById(R.id.mImageViewCategory);
        }
    }

    /*private void selectImage(final int id) {
        final CharSequence[] options = {"Camera", "Gallery"};
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle("Add Photo!");
        builder.setItems(options, new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int item) {
                if (options[item].equals("Camera")) {
                    checkCameraPermission(id);
                } else if (options[item].equals("Gallery")) {
                    checkStoragePermission(id);
                }
            }

        });

        builder.show();

    }

    private void selectImageAction(final int id) {
        final CharSequence[] options = {"View Image", "Change Image", "Remove Image"};
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setItems(options, new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int item) {
                if (options[item].equals("View Image")) {
                    Intent displayImage = new Intent(context, ShowImage.class);
                    displayImage.putExtra("imagePathString", mCurrentPhotoPath);
                    context.startActivity(displayImage);
                } else if (options[item].equals("Change Image")) {
                    selectImage(id);
                } else {
                    //mImageView.setVisibility(View.GONE);
                    ImageView mImageView = (ImageView) activity.findViewById(id);
                    if (mImageView != null) {
                        mImageView.setImageResource(R.drawable.ic_account_camera);
                    }
                    encodedImage = null;
                    imageSet = false;
                }
            }

        });

        builder.show();

    }

    private void checkCameraPermission(int id) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ActivityCompat.checkSelfPermission(context, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
                activity.requestPermissions(new String[]{Manifest.permission.CAMERA}, LockatedConfig.REQUEST_CAMERA);
            } else {
                onCameraClicked(id);
            }
        } else {
            onCameraClicked(id);
        }
    }

    private void checkStoragePermission(int id) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                activity.requestPermissions(new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, LockatedConfig.REQUEST_STORAGE);
            } else {
                onGalleryClicked(id);
            }
        } else {
            onGalleryClicked(id);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        if (requestCode == LockatedConfig.REQUEST_CAMERA) {
            if (grantResults.length == 1 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                onCameraClicked(0);
            }
        } else if (requestCode == LockatedConfig.REQUEST_STORAGE) {
            if (grantResults.length == 1 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                onGalleryClicked(0);
            }
        }
    }

    private void onGalleryClicked(int id) {
        Intent i = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        imageViewObjectPOJO.setImageView(id);
        activity.startActivityForResult(i, LockatedConfig.CHOOSE_IMAGE_REQUEST);
    }

    private File createImageFile() {
        File image = null;
        try {
            String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss", Locale.ENGLISH).format(new Date());
            String imageFileName = "JPEG_" + timeStamp + "_";
            File storageDir = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES);
            image = File.createTempFile(imageFileName, ".jpg", storageDir);

            mCurrentPhotoPath = image.getAbsolutePath();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return image;
    }

    private void onCameraClicked(int id) {
        if (!marshMallowPermission.checkPermissionForCamera()) {
            marshMallowPermission.requestPermissionForCamera();
        } else {
            if (!marshMallowPermission.checkPermissionForExternalStorage()) {
                marshMallowPermission.requestPermissionForExternalStorage();
            } else {
                Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                if (takePictureIntent.resolveActivity(context.getPackageManager()) != null) {
                    File photoFile = createImageFile();
                    if (photoFile != null) {
                        takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(photoFile));
                        imageViewObjectPOJO.setImageView(id);
                        activity.startActivityForResult(takePictureIntent, LockatedConfig.REQUEST_TAKE_PHOTO);
                    }
                }
            }
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode != 0) {
            if (requestCode == LockatedConfig.REQUEST_TAKE_PHOTO && resultCode == Activity.RESULT_OK) {
                setPic(Integer.valueOf(imageViewObjectPOJO.getImageView().toString()));
            } else if (requestCode == LockatedConfig.CHOOSE_IMAGE_REQUEST && resultCode == Activity.RESULT_OK) {
                Uri selectedImageURI = data.getData();
                mCurrentPhotoPath = getPath(selectedImageURI);
                //setPic();
                setPic(Integer.valueOf(imageViewObjectPOJO.getImageView().toString()));
            } else if (requestCode == LockatedConfig.REQUEST_CAMERA_PHOTO && resultCode == Activity.RESULT_OK) {
                Bitmap thumbnail = (Bitmap) data.getExtras().get("data");
                if (thumbnail != null) {
                    ByteArrayOutputStream bytes = new ByteArrayOutputStream();
                    File destination = new File(Environment.getExternalStorageDirectory(), System.currentTimeMillis() + ".jpg");
                    FileOutputStream fo;
                    try {
                        destination.createNewFile();
                        fo = new FileOutputStream(destination);
                        fo.write(bytes.toByteArray());
                        fo.close();
                    } catch (FileNotFoundException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    //setCameraPic(thumbnail);
                }
            }
        }
    }

    private void setCameraPic(Bitmap bitmap, ImageView mImageView) {
        if (bitmap != null) {
            encodedImage = Utilities.encodeTobase64(bitmap);
            // Log.w("setCameraPic", encodedImage);
            //mImageView.setVisibility(View.VISIBLE);
            mImageView.setImageBitmap(bitmap);
            imageSet = true;
        }
    }

    private void setPic(int id) {
        try {
            ImageView mImageView = (ImageView) activity.findViewById(id);
            if (mImageView != null) {
                int targetW = mImageView.getWidth();
                int targetH = mImageView.getHeight();
                BitmapFactory.Options bmOptions = new BitmapFactory.Options();
                bmOptions.inJustDecodeBounds = true;
                BitmapFactory.decodeFile(mCurrentPhotoPath, bmOptions);
                int photoW = bmOptions.outWidth;
                int photoH = bmOptions.outHeight;
                int scaleFactor = Math.min(photoW / targetW, photoH / targetH);

                bmOptions.inJustDecodeBounds = false;
                bmOptions.inSampleSize = scaleFactor;
                bmOptions.inPurgeable = true;

                Bitmap bitmap = BitmapFactory.decodeFile(mCurrentPhotoPath, bmOptions);
                if (bitmap != null) {
                    mImageView.setImageResource(0);
                    encodedImage = Utilities.encodeTobase64(bitmap);
                    // Log.w("setPic", encodedImage);
                    mImageView.setImageBitmap(bitmap);
                    imageSet = true;
                    imageViewObjectPOJO.setImageView(null);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private String getPath(Uri uri) {
        if (uri == null) {
            return null;
        }
        String[] projection = {MediaStore.Images.Media.DATA};
        Cursor cursor = context.getContentResolver().query(uri, projection, null, null, null);
        if (cursor != null) {
            int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
            cursor.moveToFirst();
            return cursor.getString(column_index);
        }
        return uri.getPath();
    }*/

}