package com.android.lockated.categories.olacabs.adapter;


import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.lockated.R;
import com.android.lockated.categories.olacabs.fragment.ConfirmOlaBookingFragment;
import com.android.lockated.model.OlaCab.OlaCabCategoryDetail;
import com.android.lockated.preferences.LockatedPreferences;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class OlaCabsAdapter extends RecyclerView.Adapter<OlaCabsAdapter.OlaCabsViewHolder> {

    JSONObject surcharge;
    Context context;
    FragmentManager fragmentManager;
    //ArrayList<Category> olaCabCategoryDetails;
    ArrayList<OlaCabCategoryDetail> olaCabCategoryDetails;
    private LockatedPreferences mLockatedPreferences;

    public OlaCabsAdapter(Context context, ArrayList<OlaCabCategoryDetail> olaCabCategoryDetails, FragmentManager fragmentManager) { //ArrayList<Category> olaCabCategoryDetails
        this.context = context;
        this.olaCabCategoryDetails = olaCabCategoryDetails;
        this.fragmentManager = fragmentManager;
        mLockatedPreferences = new LockatedPreferences(context);
        Log.e("olaCabCategoryDetails", "" + this.olaCabCategoryDetails);

    }

    @Override
    public OlaCabsViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View layout = LayoutInflater.from(context).inflate(R.layout.olacabs_each_layout, parent, false);
        return new OlaCabsViewHolder(layout);
    }

    @SuppressLint("LongLogTag")
    @TargetApi(Build.VERSION_CODES.KITKAT)
    @Override
    public void onBindViewHolder(OlaCabsViewHolder holder, final int position) {

        int minFare = 0;
        int maxFare = 0;
        JSONObject fare_object = null;
        if (mLockatedPreferences.getDstLat() == 0 && mLockatedPreferences.getDstLng() == 0) {
            if (!olaCabCategoryDetails.get(0).getCategories().isEmpty()) {
                Log.e("Position Clicked", String.valueOf(position));
                holder.olaCab_type.setText(olaCabCategoryDetails.get(0).getCategories().get(position).getDisplayName());
                holder.olaCab_eta.setText(olaCabCategoryDetails.get(0).getCategories().get(position).getEta() + " Min");
                Picasso.with(context).load(olaCabCategoryDetails.get(0).getCategories().get(position).getImage()).into(holder.ola_image);
                int n = olaCabCategoryDetails.get(0).getCategories().get(position).getFareBreakup().size();

                Log.e("n", "" + n);

                for (int i = 0; i < n; i++) {
                    minFare = fareCalculater(olaCabCategoryDetails.get(0).getCategories().get(position).getFareBreakup().get(0).getBaseFare(), olaCabCategoryDetails.get(0).getCategories().get(position).getFareBreakup().get(0).getCostPerDistance(), olaCabCategoryDetails.get(0).getCategories().get(position).getFareBreakup().get(0).getRideCostPerMinute());
                    Log.e("position" + 1, olaCabCategoryDetails.get(0).getCategories().get(position).getFareBreakup().get(0).getBaseFare() + " " + olaCabCategoryDetails.get(0).getCategories().get(position).getFareBreakup().get(0).getCostPerDistance() + " " + olaCabCategoryDetails.get(0).getCategories().get(position).getFareBreakup().get(0).getRideCostPerMinute());
                    maxFare = minFare + 15;
                    holder.olaCab_fare.setText(context.getString(R.string.rupees_symbol) + "" + minFare + " - " + maxFare);
                    try {

                        fare_object = new JSONObject();
                        fare_object.put("type", olaCabCategoryDetails.get(0).getCategories().get(position).getFareBreakup().get(0).getType());
                        fare_object.put("minimum_distance", olaCabCategoryDetails.get(0).getCategories().get(position).getFareBreakup().get(0).getMinimumDistance());
                        fare_object.put("minimum_time", olaCabCategoryDetails.get(0).getCategories().get(position).getFareBreakup().get(0).getMinimumTime());
                        fare_object.put("base_fare", olaCabCategoryDetails.get(0).getCategories().get(position).getFareBreakup().get(0).getBaseFare());
                        fare_object.put("minimum_fare", olaCabCategoryDetails.get(0).getCategories().get(position).getFareBreakup().get(0).getMinimumFare());
                        fare_object.put("cost_per_distance", olaCabCategoryDetails.get(0).getCategories().get(position).getFareBreakup().get(0).getCostPerDistance());
                        fare_object.put("waiting_cost_per_minute", olaCabCategoryDetails.get(0).getCategories().get(position).getFareBreakup().get(0).getWaitingCostPerMinute());
                        fare_object.put("ride_cost_per_minute", olaCabCategoryDetails.get(0).getCategories().get(position).getFareBreakup().get(0).getRideCostPerMinute());

                        Log.e("type", "" + olaCabCategoryDetails.get(0).getCategories().get(position).getFareBreakup().get(0).getType());
                        Log.e("minimum_distance", "" + olaCabCategoryDetails.get(0).getCategories().get(position).getFareBreakup().get(0).getMinimumDistance());
                        Log.e("minimum_time", "" + olaCabCategoryDetails.get(0).getCategories().get(position).getFareBreakup().get(0).getMinimumTime());
                        Log.e("base_fare", "" + olaCabCategoryDetails.get(0).getCategories().get(position).getFareBreakup().get(0).getBaseFare());
                        Log.e("minimum_fare", "" + olaCabCategoryDetails.get(0).getCategories().get(position).getFareBreakup().get(0).getMinimumFare());
                        Log.e("cost_per_distance", "" + olaCabCategoryDetails.get(0).getCategories().get(position).getFareBreakup().get(0).getCostPerDistance());
                        Log.e("waiting_cost_per_minute", "" + olaCabCategoryDetails.get(0).getCategories().get(position).getFareBreakup().get(0).getWaitingCostPerMinute());
                        Log.e("ride_cost_per_minute", "" + olaCabCategoryDetails.get(0).getCategories().get(position).getFareBreakup().get(0).getRideCostPerMinute());
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                    if (!olaCabCategoryDetails.get(0).getCategories().get(position).getFareBreakup().get(0).getSurcharge().isEmpty()) {

                        holder.layout_surcharge.setVisibility(View.VISIBLE);
                        String getDescription = olaCabCategoryDetails.get(0).getCategories().get(position).getFareBreakup().get(0).getSurcharge().get(0).getDescription();
                        String getName = olaCabCategoryDetails.get(0).getCategories().get(position).getFareBreakup().get(0).getSurcharge().get(0).getName();
                        String getType = olaCabCategoryDetails.get(0).getCategories().get(position).getFareBreakup().get(0).getSurcharge().get(0).getType();
                        String getValue = "" + olaCabCategoryDetails.get(0).getCategories().get(position).getFareBreakup().get(0).getSurcharge().get(0).getValue();

                        Log.e("getDescription", "" + getDescription);
                        Log.e("getName", "" + getName);
                        Log.e("getType", "" + getType);
                        Log.e("getValue", "" + getValue);

                        //String olaCab_fare_surcharge = "(includes" +""+ R.drawable.rupee +""+getValue +"x)";
                        holder.olaCab_fare_surcharge.setText(getValue);
                    }

                    final int finalMinFare = minFare;
                    final int finalMaxFare = maxFare;
                    final JSONObject finalFare_object = fare_object;
                    holder.layout.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {

                            Bundle bundle = new Bundle();
                            bundle.putString("ola_cab_id", olaCabCategoryDetails.get(0).getCategories().get(position).getId());
                            bundle.putString("ola_cab_type", olaCabCategoryDetails.get(0).getCategories().get(position).getDisplayName());
                            bundle.putString("ola_cab_fare", context.getString(R.string.rupees_symbol) + " " + finalMinFare + " - " + finalMaxFare);
                            bundle.putString("ola_cab_eta", "" + olaCabCategoryDetails.get(0).getCategories().get(position).getEta());
                            bundle.putString("ola_cab_image", olaCabCategoryDetails.get(0).getCategories().get(position).getImage());
                            bundle.putString("ola_cab_jsonObject", String.valueOf(finalFare_object));
                            bundle.putBoolean("ola_dest_exist",false);


                            Log.e("ola_cab_id", olaCabCategoryDetails.get(0).getCategories().get(position).getId());
                            Log.e("ola_cab_type", olaCabCategoryDetails.get(0).getCategories().get(position).getDisplayName());
                            Log.e("ola_cab_fare", context.getString(R.string.rupees_symbol) + " " + finalMinFare + " - " + finalMaxFare);
                            Log.e("ola_cab_eta", "" + olaCabCategoryDetails.get(0).getCategories().get(position).getEta());
                            Log.e("ola_cab_image", olaCabCategoryDetails.get(0).getCategories().get(position).getImage());
                            Log.e("ola_cab_jsonObject", String.valueOf(finalFare_object));

                            // bundle.putString("ola_cab_image",olaCabCategoryDetails.get(position).getFareBreakup().get(0));


                            if (!olaCabCategoryDetails.get(0).getCategories().get(position).getFareBreakup().get(0).getSurcharge().isEmpty()) {
                                bundle.putFloat("ola_cab_surcharge", olaCabCategoryDetails.get(0).getCategories().get(position).getFareBreakup().get(0).getSurcharge().get(0).getValue());
                                Log.e("ola_cab_surcharge", "" + olaCabCategoryDetails.get(0).getCategories().get(position).getFareBreakup().get(0).getSurcharge().get(0).getValue());
                            } else {
                                bundle.putFloat("ola_cab_surcharge", 0);
                            }
                            ConfirmOlaBookingFragment confirmOlaBookingFragment = new ConfirmOlaBookingFragment();
                            confirmOlaBookingFragment.setArguments(bundle);

                            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                            fragmentTransaction.replace(R.id.mFrameContainer, confirmOlaBookingFragment);
                            fragmentTransaction.addToBackStack(null);
                            fragmentTransaction.commit();

                            //fragmentTransaction.commitAllowingStateLoss();
                        }
                    });
                }
            }
        } else if (mLockatedPreferences.getDstLat() != 0 && mLockatedPreferences.getDstLng() != 0) {

            Log.e("mLockatedPreferences","I m");
            if (!olaCabCategoryDetails.get(0).getRideEstimate().isEmpty()) {
                Log.e("Position Clicked", String.valueOf(position));
                holder.olaCab_type.setText(olaCabCategoryDetails.get(0).getCategories().get(position).getDisplayName());
                holder.olaCab_eta.setText(olaCabCategoryDetails.get(0).getCategories().get(position).getEta() + " Min");
                Picasso.with(context).load(olaCabCategoryDetails.get(0).getCategories().get(position).getImage()).into(holder.ola_image);
                int n = olaCabCategoryDetails.get(0).getCategories().get(position).getFareBreakup().size();
                Log.e("n", "" + n);
                for (int i = 0; i < n; i++) {
                    maxFare = minFare + 15;
                    holder.olaCab_fare.setText(context.getString(R.string.rupees_symbol) + "" + olaCabCategoryDetails.get(0).getRideEstimate().get(position).getAmountMin() + " - " + olaCabCategoryDetails.get(0).getRideEstimate().get(position).getAmountMax());

                    try {
                        fare_object = new JSONObject();
                        fare_object.put("type", olaCabCategoryDetails.get(0).getCategories().get(position).getFareBreakup().get(0).getType());
                        fare_object.put("minimum_distance", olaCabCategoryDetails.get(0).getCategories().get(position).getFareBreakup().get(0).getMinimumDistance());
                        fare_object.put("minimum_time", olaCabCategoryDetails.get(0).getCategories().get(position).getFareBreakup().get(0).getMinimumTime());
                        fare_object.put("base_fare", olaCabCategoryDetails.get(0).getCategories().get(position).getFareBreakup().get(0).getBaseFare());
                        fare_object.put("minimum_fare", olaCabCategoryDetails.get(0).getCategories().get(position).getFareBreakup().get(0).getMinimumFare());
                        fare_object.put("cost_per_distance", olaCabCategoryDetails.get(0).getCategories().get(position).getFareBreakup().get(0).getCostPerDistance());
                        fare_object.put("waiting_cost_per_minute", olaCabCategoryDetails.get(0).getCategories().get(position).getFareBreakup().get(0).getWaitingCostPerMinute());
                        fare_object.put("ride_cost_per_minute", olaCabCategoryDetails.get(0).getCategories().get(position).getFareBreakup().get(0).getRideCostPerMinute());

                        Log.e("type", "" + olaCabCategoryDetails.get(0).getCategories().get(position).getFareBreakup().get(0).getType());
                        Log.e("minimum_distance", "" + olaCabCategoryDetails.get(0).getCategories().get(position).getFareBreakup().get(0).getMinimumDistance());
                        Log.e("minimum_time", "" + olaCabCategoryDetails.get(0).getCategories().get(position).getFareBreakup().get(0).getMinimumTime());
                        Log.e("base_fare", "" + olaCabCategoryDetails.get(0).getCategories().get(position).getFareBreakup().get(0).getBaseFare());
                        Log.e("minimum_fare", "" + olaCabCategoryDetails.get(0).getCategories().get(position).getFareBreakup().get(0).getMinimumFare());
                        Log.e("cost_per_distance", "" + olaCabCategoryDetails.get(0).getCategories().get(position).getFareBreakup().get(0).getCostPerDistance());
                        Log.e("waiting_cost_per_minute", "" + olaCabCategoryDetails.get(0).getCategories().get(position).getFareBreakup().get(0).getWaitingCostPerMinute());
                        Log.e("ride_cost_per_minute", "" + olaCabCategoryDetails.get(0).getCategories().get(position).getFareBreakup().get(0).getRideCostPerMinute());
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                    if (!olaCabCategoryDetails.get(0).getCategories().get(position).getFareBreakup().get(0).getSurcharge().isEmpty()) {

                        holder.layout_surcharge.setVisibility(View.VISIBLE);
                        String getDescription = olaCabCategoryDetails.get(0).getCategories().get(position).getFareBreakup().get(0).getSurcharge().get(0).getDescription();
                        String getName = olaCabCategoryDetails.get(0).getCategories().get(position).getFareBreakup().get(0).getSurcharge().get(0).getName();
                        String getType = olaCabCategoryDetails.get(0).getCategories().get(position).getFareBreakup().get(0).getSurcharge().get(0).getType();
                        String getValue = "" + olaCabCategoryDetails.get(0).getCategories().get(position).getFareBreakup().get(0).getSurcharge().get(0).getValue();

                        Log.e("getDescription", "" + getDescription);
                        Log.e("getName", "" + getName);
                        Log.e("getType", "" + getType);
                        Log.e("getValue", "" + getValue);

                        //String olaCab_fare_surcharge = "(includes" +""+ R.drawable.rupee +""+getValue +"x)";
                        holder.olaCab_fare_surcharge.setText(getValue);
                    }

                    final JSONObject finalFare_object = fare_object;
                    holder.layout.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {

                            Bundle bundle = new Bundle();
                            bundle.putString("ola_cab_id", olaCabCategoryDetails.get(0).getCategories().get(position).getId());
                            bundle.putString("ola_cab_type", olaCabCategoryDetails.get(0).getCategories().get(position).getDisplayName());
                            bundle.putString("ola_cab_fare", context.getString(R.string.rupees_symbol) + " " + olaCabCategoryDetails.get(0).getRideEstimate().get(position).getAmountMin() + " - " + olaCabCategoryDetails.get(0).getRideEstimate().get(position).getAmountMax());
                            bundle.putString("ola_cab_eta", "" + olaCabCategoryDetails.get(0).getCategories().get(position).getEta());
                            bundle.putString("ola_cab_image", olaCabCategoryDetails.get(0).getCategories().get(position).getImage());
                            bundle.putString("ola_cab_jsonObject", String.valueOf(finalFare_object));
                            bundle.putBoolean("ola_dest_exist",true);

                            Log.e("ola_cab_id", olaCabCategoryDetails.get(0).getCategories().get(position).getId());
                            Log.e("ola_cab_type", olaCabCategoryDetails.get(0).getCategories().get(position).getDisplayName());
                            Log.e("ola_cab_fare", context.getString(R.string.rupees_symbol) + " " + olaCabCategoryDetails.get(0).getRideEstimate().get(position).getAmountMin() + " - " + olaCabCategoryDetails.get(0).getRideEstimate().get(position).getAmountMax());
                            Log.e("ola_cab_eta", "" + olaCabCategoryDetails.get(0).getCategories().get(position).getEta());
                            Log.e("ola_cab_image", olaCabCategoryDetails.get(0).getCategories().get(position).getImage());
                            Log.e("ola_cab_jsonObject", String.valueOf(finalFare_object));

                            // bundle.putString("ola_cab_image",olaCabCategoryDetails.get(position).getFareBreakup().get(0));


                            if (!olaCabCategoryDetails.get(0).getCategories().get(position).getFareBreakup().get(0).getSurcharge().isEmpty()) {
                                bundle.putFloat("ola_cab_surcharge", olaCabCategoryDetails.get(0).getCategories().get(position).getFareBreakup().get(0).getSurcharge().get(0).getValue());
                                Log.e("ola_cab_surcharge", "" + olaCabCategoryDetails.get(0).getCategories().get(position).getFareBreakup().get(0).getSurcharge().get(0).getValue());
                            } else {
                                bundle.putFloat("ola_cab_surcharge", 0);
                            }
                            ConfirmOlaBookingFragment confirmOlaBookingFragment = new ConfirmOlaBookingFragment();
                            confirmOlaBookingFragment.setArguments(bundle);

                            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                            fragmentTransaction.replace(R.id.mFrameContainer, confirmOlaBookingFragment);
                            fragmentTransaction.addToBackStack(null);
                            fragmentTransaction.commit();

                            //fragmentTransaction.commitAllowingStateLoss();
                        }
                    });
                }
            }
        }
    }

    @Override
    public int getItemCount() {
        return olaCabCategoryDetails.size();
    }

    public class OlaCabsViewHolder extends RecyclerView.ViewHolder {
        ImageView ola_image;
        TextView olaCab_type, olaCab_fare, olaCab_eta, olaCab_fare_surcharge;
        LinearLayout layout, layout_surcharge;

        public OlaCabsViewHolder(final View itemView) {
            super(itemView);
            layout = (LinearLayout) itemView.findViewById(R.id.mainView);
            layout_surcharge = (LinearLayout) itemView.findViewById(R.id.layout_surcharge);
            ola_image = (ImageView) itemView.findViewById(R.id.ola_image);


            olaCab_type = (TextView) itemView.findViewById(R.id.olaCab_type);
            olaCab_fare = (TextView) itemView.findViewById(R.id.olaCab_fare);
            olaCab_eta = (TextView) itemView.findViewById(R.id.olaCab_eta);
            olaCab_fare_surcharge = (TextView) itemView.findViewById(R.id.olaCab_fare_surcharge);
        }
    }

    public int fareCalculater(String basefare, String costPerDistance, String costPerMinute) {

        Log.e("basefare", "" + basefare);
        Log.e("costPerDistance", "" + costPerDistance);
        Log.e("costPerMinute", "" + costPerMinute);

        // it is the fareCalculation for 5 KMs . Base Fare is always for 4 KMs. and Adding Cost for 1 KMs .
        int minTime = 15;
        int basefare_new;
        int costPerDistance_new;
        int costPerMinute_new;

        try {
            basefare_new = Integer.parseInt(basefare);
        } catch (NumberFormatException e) {
            double basefare_new_new = Double.parseDouble(basefare);
            basefare_new = (int) basefare_new_new;
        }

        try {
            costPerDistance_new = Integer.parseInt(costPerDistance);
        } catch (NumberFormatException e) {
            double costPerDistance_new_new = Double.parseDouble(costPerDistance);
            costPerDistance_new = (int) costPerDistance_new_new;
        }

        try {
            costPerMinute_new = Integer.parseInt(costPerMinute);
        } catch (NumberFormatException e) {
            double costPerMinute_new_new = Double.parseDouble(costPerMinute);
            costPerMinute_new = (int) costPerMinute_new_new;
        }


        return basefare_new + (costPerDistance_new * costPerMinute_new) + (minTime * costPerMinute_new);
    }
}
