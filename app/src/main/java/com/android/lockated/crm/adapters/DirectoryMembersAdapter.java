package com.android.lockated.crm.adapters;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.lockated.R;
import com.android.lockated.crm.p2pchat.PeerChatActivity;
import com.android.lockated.model.usermodel.Directory.MembersDirectory.UserSociety;
import com.android.lockated.preferences.LockatedPreferences;

import com.android.lockated.request.LockatedRequestError;
import com.android.lockated.utils.Utilities;
import com.android.volley.Response;
import com.android.volley.VolleyError;

import org.json.JSONObject;

import java.util.ArrayList;

public class DirectoryMembersAdapter extends BaseAdapter implements Response.ErrorListener, Response.Listener<JSONObject> {

    private final ArrayList<UserSociety> userSocieties;
    Context contextMain;
    LayoutInflater layoutInflater;
    LockatedPreferences mLockatedPreferences;

    public DirectoryMembersAdapter(Context context, ArrayList<UserSociety> userSocieties) {
        this.contextMain = context;
        this.userSocieties = userSocieties;
        layoutInflater = LayoutInflater.from(context);
        mLockatedPreferences = new LockatedPreferences(contextMain);
    }

    @Override
    public int getCount() {
        return userSocieties.size();
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public void onErrorResponse(VolleyError error) {
        if (contextMain != null) {
            LockatedRequestError.onRequestError(contextMain, error);
        }
    }

    @Override
    public void onResponse(JSONObject response) {
    }

    private class ViewHolder {
        ImageView mImageDelete;
        ImageView mImageEdit;
        TextView mTextNumber;
        TextView mTextDirectoryName;
        TextView mServiceType;
        ImageButton mImageMessage;

    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        ViewHolder holder = null;
        if (convertView == null) {
            convertView = layoutInflater.inflate(R.layout.directory_member_child, parent, false);
            holder = new ViewHolder();
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        holder.mImageEdit = (ImageView) convertView.findViewById(R.id.mImageEdit);
        holder.mImageDelete = (ImageView) convertView.findViewById(R.id.mImageDelete);
        holder.mTextDirectoryName = (TextView) convertView.findViewById(R.id.mTextDirectoryName);
        holder.mTextNumber = (TextView) convertView.findViewById(R.id.mTextNumber);
        holder.mImageMessage = (ImageButton) convertView.findViewById(R.id.mImageMessage);


        if ((userSocieties.get(position).getUser() != null) && !userSocieties.get(position).getUser().toString().equals("null")) {
            String name = userSocieties.get(position).getUser().getFirstname()
                    + " " + userSocieties.get(position).getUser().getLastname();
            holder.mTextDirectoryName.setText(name);
            if (userSocieties.get(position).getUserFlat() != null) {
                holder.mTextNumber.setText(userSocieties.get(position).getUserFlat().getFlat());
            } else {
                holder.mTextNumber.setText("No Flat");
            }
        }
        holder.mImageMessage.setTag(position);
        holder.mImageMessage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int position = (int) v.getTag();
                String userName = "";
                if (userSocieties.get(position).getUser() != null) {
                    userName = userSocieties.get(position).getUser().getFirstname()
                            + " " + userSocieties.get(position).getUser().getLastname();
                }
                if (userSocieties.get(position).getUser().getId() != Integer.valueOf(mLockatedPreferences.getLockatedUserId())) {
                    Intent intent = new Intent(contextMain, PeerChatActivity.class);
                    intent.putExtra("userId", "" + userSocieties.get(position).getUser().getId());
                    intent.putExtra("userName", userName);
                    contextMain.startActivity(intent);
                } else {
                    Utilities.showToastMessage(contextMain, "You cannot chat with yourself");
                }
            }
        });

        return convertView;
    }

}