package com.android.lockated.crm.fragment;

import android.app.Dialog;
import android.app.TimePickerDialog;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.text.format.DateFormat;
import android.widget.TextView;
import android.widget.TimePicker;

import java.util.Calendar;

/**
 * Created by Sagar on 10-02-2016.
 */
public class TimePickerFragmentSagar extends DialogFragment implements TimePickerDialog.OnTimeSetListener {
    private TextView mTextViewTime;

    public void setTimePickerView(TextView textView) {
        mTextViewTime = textView;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        final Calendar c = Calendar.getInstance();
        int hour = c.get(Calendar.HOUR_OF_DAY);
        int minute = c.get(Calendar.MINUTE);

        return new TimePickerDialog(getActivity(), this, hour, minute, DateFormat.is24HourFormat(getActivity()));
    }

    @Override
    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
        if (mTextViewTime != null) {
            String pickedTime = hourOfDay + ":" + minute;
            mTextViewTime.setText(pickedTime);
        }
    }
}
