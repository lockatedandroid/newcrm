
package com.android.lockated.model.AdminModel.AdminDirectoty.MembersDirectory;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class AdminMemberDirectory {

    @SerializedName("user_societies")
    @Expose
    private ArrayList<AdminUserSociety> userSocieties = new ArrayList<AdminUserSociety>();

    /**
     * 
     * @return
     *     The userSocieties
     */
    public ArrayList<AdminUserSociety> getUserSocieties() {
        return userSocieties;
    }

    /**
     * 
     * @param userSocieties
     *     The user_societies
     */
    public void setUserSocieties(ArrayList<AdminUserSociety> userSocieties) {
        this.userSocieties = userSocieties;
    }

}
