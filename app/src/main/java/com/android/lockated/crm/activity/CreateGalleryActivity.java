package com.android.lockated.crm.activity;

import android.Manifest;
import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.RelativeLayout.LayoutParams;

import com.android.lockated.R;
import com.android.lockated.utils.LockatedConfig;
import com.android.lockated.utils.MarshMallowPermission;
import com.android.lockated.utils.ShowImage;
import com.android.lockated.utils.Utilities;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class CreateGalleryActivity extends AppCompatActivity implements View.OnClickListener {

    public static int count = 0;
    boolean imageSet1 = false;
    boolean imageSet2 = false;
    boolean imageSet3 = false;
    boolean imageSet4 = false;
    boolean imageSet5 = false;
    boolean imageSet6 = false;
    boolean imageSet = false;
    String mCurrentPhotoPath;
    String encodedImage1;
    String encodedImage2;
    String encodedImage3;
    String encodedImage4;
    String encodedImage5;
    String encodedImage6;
    ImageView galleryImage1;
    ImageView galleryImage2;
    ImageView galleryImage3;
    ImageView galleryImage4;
    ImageView galleryImage5;
    ImageView galleryImage6;
    MarshMallowPermission marshMallowPermission;
    Button addMoreButton;
    Button submitButton;
    EditText albumName;
    ImageViewObjectPOJO imageViewObjectPOJO;
    static int idCount = 100;
    RelativeLayout rl;
    /*private RecyclerView mRecyclerView;
    private RecyclerView.LayoutManager mLayoutManager;
    CreateGalleryAdapter createGalleryAdapter;
    int adapterItems = 6;*/

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_gallery);

        setToolBar(getString(R.string.create_gallery));
        init();

    }

    private void setToolBar(String name) {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(true);
        getSupportActionBar().setTitle(name);
    }

    private void init() {
        marshMallowPermission = new MarshMallowPermission(this);
        imageViewObjectPOJO = new ImageViewObjectPOJO();

        submitButton = (Button) findViewById(R.id.submitButton);
        addMoreButton = (Button) findViewById(R.id.addMoreButton);
        rl = (RelativeLayout) findViewById(R.id.rl);

        albumName = (EditText) findViewById(R.id.albumName);

        galleryImage1 = (ImageView) findViewById(R.id.galleryImage1);
        galleryImage2 = (ImageView) findViewById(R.id.galleryImage2);
        galleryImage3 = (ImageView) findViewById(R.id.galleryImage3);
        galleryImage4 = (ImageView) findViewById(R.id.galleryImage4);
        galleryImage5 = (ImageView) findViewById(R.id.galleryImage5);
        galleryImage6 = (ImageView) findViewById(R.id.galleryImage6);

        galleryImage1.setImageResource(R.drawable.ic_account_camera);
        galleryImage2.setImageResource(R.drawable.ic_account_camera);
        galleryImage3.setImageResource(R.drawable.ic_account_camera);
        galleryImage4.setImageResource(R.drawable.ic_account_camera);
        galleryImage5.setImageResource(R.drawable.ic_account_camera);
        galleryImage6.setImageResource(R.drawable.ic_account_camera);

        galleryImage1.setOnClickListener(this);
        galleryImage2.setOnClickListener(this);
        galleryImage3.setOnClickListener(this);
        galleryImage4.setOnClickListener(this);
        galleryImage5.setOnClickListener(this);
        galleryImage6.setOnClickListener(this);

        submitButton.setOnClickListener(this);
        addMoreButton.setOnClickListener(this);

        /*mRecyclerView = (RecyclerView) findViewById(R.id.mRecyclerView);
        mLayoutManager = new GridLayoutManager(this, 2);
        mRecyclerView.setLayoutManager(mLayoutManager);
        createGalleryAdapter = new CreateGalleryAdapter(adapterItems, this, this, this);
        mRecyclerView.setAdapter(createGalleryAdapter);*/

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.galleryImage1:
                checkImageSetOrNot(imageSet1, v.getId());
                break;
            case R.id.galleryImage2:
                checkImageSetOrNot(imageSet2, v.getId());
                break;
            case R.id.galleryImage3:
                checkImageSetOrNot(imageSet3, v.getId());
                break;
            case R.id.galleryImage4:
                checkImageSetOrNot(imageSet4, v.getId());
                break;
            case R.id.galleryImage5:
                checkImageSetOrNot(imageSet5, v.getId());
                break;
            case R.id.galleryImage6:
                checkImageSetOrNot(imageSet6, v.getId());
                break;
            case R.id.submitButton:
                getAllImageEncodedValues();
                break;
            case R.id.addMoreButton:
                addMoreImages();
                break;
        }

    }

    private void addMoreImages() {
        ImageView imageView = new ImageView(this);
        idCount = idCount + 1;
        //Log.e("idCount", "" + idCount);
        imageView.setId(idCount);
        imageView.setImageResource(R.drawable.billspayments);
        LayoutParams lp = new LayoutParams(LayoutParams.FILL_PARENT, LayoutParams.WRAP_CONTENT);
        // Add rule to layout parameters
        // Add the ImageView below to Button
        if (idCount == 101) {
            lp.addRule(RelativeLayout.BELOW, addMoreButton.getId());
        } else {
            lp.addRule(RelativeLayout.BELOW, imageView.getId() - 1);
            //Log.e("imageView.getId()", "" + (imageView.getId() - 1));
        }
        // Add layout parameters to ImageView
        imageView.setLayoutParams(lp);
        // Finally, add the ImageView to layout
        rl.addView(imageView);
        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                checkImageSetOrNot(imageSet, v.getId());
            }
        });
    }

    private void checkImageSetOrNot(boolean imageSet, int id) {
        if (!imageSet) {
            selectImage(id);
        } else {
            selectImageAction(id);
        }
    }

    private void selectImage(final int id) {
        final CharSequence[] options = {"Camera", "Gallery"};
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Add Photo!");
        builder.setItems(options, new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int item) {
                if (options[item].equals("Camera")) {
                    checkCameraPermission(id);
                } else if (options[item].equals("Gallery")) {
                    checkStoragePermission(id);
                }
            }

        });

        builder.show();

    }

    private void selectImageAction(final int id) {
        final CharSequence[] options = {"View Image", "Change Image", "Remove Image"};
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setItems(options, new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int item) {
                if (options[item].equals("View Image")) {
                    Intent displayImage = new Intent(CreateGalleryActivity.this, ShowImage.class);
                    displayImage.putExtra("imagePathString", mCurrentPhotoPath);
                    startActivity(displayImage);
                } else if (options[item].equals("Change Image")) {
                    selectImage(id);
                } else {
                    ImageView mImageView = (ImageView) findViewById(id);
                    if (mImageView != null) {
                        mImageView.setImageResource(R.drawable.ic_account_camera);
                        removeEncodedImage(id);
                    }
                }
            }

        });

        builder.show();

    }

    private void checkCameraPermission(int id) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
                requestPermissions(new String[]{Manifest.permission.CAMERA}, LockatedConfig.REQUEST_CAMERA);
            } else {
                onCameraClicked(id);
            }
        } else {
            onCameraClicked(id);
        }
    }

    private void checkStoragePermission(int id) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                requestPermissions(new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, LockatedConfig.REQUEST_STORAGE);
            } else {
                onGalleryClicked(id);
            }
        } else {
            onGalleryClicked(id);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        if (requestCode == LockatedConfig.REQUEST_CAMERA) {
            if (grantResults.length == 1 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                onCameraClicked(0);
            }
        } else if (requestCode == LockatedConfig.REQUEST_STORAGE) {
            if (grantResults.length == 1 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                onGalleryClicked(0);
            }
        }
    }

    private void onGalleryClicked(int id) {
        Intent i = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        imageViewObjectPOJO.setImageView(id);
        startActivityForResult(i, LockatedConfig.CHOOSE_IMAGE_REQUEST);
    }

    private File createImageFile() {
        File image = null;
        try {
            String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss", Locale.ENGLISH).format(new Date());
            String imageFileName = "JPEG_" + timeStamp + "_";
            File storageDir = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES);
            image = File.createTempFile(imageFileName, ".jpg", storageDir);

            mCurrentPhotoPath = image.getAbsolutePath();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return image;
    }

    private void onCameraClicked(int id) {
        if (!marshMallowPermission.checkPermissionForCamera()) {
            marshMallowPermission.requestPermissionForCamera();
        } else {
            if (!marshMallowPermission.checkPermissionForExternalStorage()) {
                marshMallowPermission.requestPermissionForExternalStorage();
            } else {
                Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                if (takePictureIntent.resolveActivity(this.getPackageManager()) != null) {
                    File photoFile = createImageFile();
                    if (photoFile != null) {
                        takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(photoFile));
                        imageViewObjectPOJO.setImageView(id);
                        startActivityForResult(takePictureIntent, LockatedConfig.REQUEST_TAKE_PHOTO);
                    }
                }
            }
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode != 0) {
            if (requestCode == LockatedConfig.REQUEST_TAKE_PHOTO && resultCode == Activity.RESULT_OK) {
                setPic(Integer.valueOf(imageViewObjectPOJO.getImageView().toString()));
            } else if (requestCode == LockatedConfig.CHOOSE_IMAGE_REQUEST && resultCode == Activity.RESULT_OK) {
                Uri selectedImageURI = data.getData();
                mCurrentPhotoPath = getPath(selectedImageURI);
                setPic(Integer.valueOf(imageViewObjectPOJO.getImageView().toString()));
            } else if (requestCode == LockatedConfig.REQUEST_CAMERA_PHOTO && resultCode == Activity.RESULT_OK) {
                Bitmap thumbnail = (Bitmap) data.getExtras().get("data");
                if (thumbnail != null) {
                    ByteArrayOutputStream bytes = new ByteArrayOutputStream();
                    File destination = new File(Environment.getExternalStorageDirectory(), System.currentTimeMillis() + ".jpg");
                    FileOutputStream fo;
                    try {
                        destination.createNewFile();
                        fo = new FileOutputStream(destination);
                        fo.write(bytes.toByteArray());
                        fo.close();
                    } catch (FileNotFoundException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    ImageView mImageView = (ImageView) findViewById(Integer.valueOf(imageViewObjectPOJO.getImageView().toString()));
                    setCameraPic(thumbnail, mImageView, Integer.valueOf(imageViewObjectPOJO.getImageView().toString()));
                }
            }
        }
    }

    private void setCameraPic(Bitmap bitmap, ImageView mImageView, int id) {
        if (bitmap != null) {
            String encodedImage = Utilities.encodeTobase64(bitmap);
            mImageView.setImageBitmap(bitmap);
            setEncodedImage(id, encodedImage);
        }
    }

    private void setPic(int id) {
        try {
            ImageView mImageView = (ImageView) findViewById(id);
            if (mImageView != null) {
                int targetW = mImageView.getWidth();
                int targetH = mImageView.getHeight();
                BitmapFactory.Options bmOptions = new BitmapFactory.Options();
                bmOptions.inJustDecodeBounds = true;
                BitmapFactory.decodeFile(mCurrentPhotoPath, bmOptions);
                int photoW = bmOptions.outWidth;
                int photoH = bmOptions.outHeight;
                int scaleFactor = Math.min(photoW / targetW, photoH / targetH);

                bmOptions.inJustDecodeBounds = false;
                bmOptions.inSampleSize = scaleFactor;
                bmOptions.inPurgeable = true;

                Bitmap bitmap = BitmapFactory.decodeFile(mCurrentPhotoPath, bmOptions);
                if (bitmap != null) {
                    String encodedImage = Utilities.encodeTobase64(bitmap);
                    mImageView.setImageBitmap(bitmap);
                    imageSet1 = true;
                    imageViewObjectPOJO.setImageView(null);
                    setEncodedImage(id, encodedImage);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void setEncodedImage(int id, String encodedImage) {
        //Log.e("encode id", "" + id);
        //Log.e("encodedImage", "" + encodedImage);
        count = count + 1;
        //Log.e("count1", "" + count);
        JSONArray jsonArrayEncode=new JSONArray();
        for (int i = 0; i <= count; i++) {

        }
        if (id == R.id.galleryImage1) {
            encodedImage1 = encodedImage;
        } else if (id == R.id.galleryImage2) {
            encodedImage2 = encodedImage;
        } else if (id == R.id.galleryImage3) {
            encodedImage3 = encodedImage;
        } else if (id == R.id.galleryImage4) {
            encodedImage4 = encodedImage;
        } else if (id == R.id.galleryImage5) {
            encodedImage5 = encodedImage;
        } else if (id == R.id.galleryImage6) {
            encodedImage6 = encodedImage;
        }
    }

    private void removeEncodedImage(int id) {
        if (id == R.id.galleryImage1) {
            encodedImage1 = null;
            imageSet1 = false;
        } else if (id == R.id.galleryImage2) {
            encodedImage2 = null;
            imageSet2 = false;
        } else if (id == R.id.galleryImage3) {
            encodedImage3 = null;
            imageSet3 = false;
        } else if (id == R.id.galleryImage4) {
            encodedImage4 = null;
            imageSet4 = false;
        } else if (id == R.id.galleryImage5) {
            encodedImage5 = null;
            imageSet5 = false;
        } else if (id == R.id.galleryImage6) {
            encodedImage6 = null;
            imageSet6 = false;
        }
    }

    private String getPath(Uri uri) {
        if (uri == null) {
            return null;
        }
        String[] projection = {MediaStore.Images.Media.DATA};
        Cursor cursor = this.getContentResolver().query(uri, projection, null, null, null);
        if (cursor != null) {
            int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
            cursor.moveToFirst();
            return cursor.getString(column_index);
        }
        return uri.getPath();
    }

    private void getAllImageEncodedValues() {

        if (TextUtils.isEmpty(albumName.getText().toString())) {
            JSONObject jsonObject = new JSONObject();
            JSONArray jsonArray = new JSONArray();

            try {
                if (encodedImage1 != null) {
                    jsonArray.put(0, encodedImage1);
                }
                if (encodedImage1 != null) {
                    jsonArray.put(1, encodedImage2);
                }
                if (encodedImage1 != null) {
                    jsonArray.put(2, encodedImage3);
                }
                if (encodedImage1 != null) {
                    jsonArray.put(3, encodedImage4);
                }
                if (encodedImage1 != null) {
                    jsonArray.put(4, encodedImage5);
                }
                if (encodedImage1 != null) {
                    jsonArray.put(5, encodedImage6);
                }
                jsonObject.put("encodedImageJsonArray", jsonArray);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            // Utilities.writeToFile(jsonObject.toString(), "getAllImageEncodedValues");
        } else {
            Utilities.showToastMessage(this, "Please enter album name");
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home: {
                super.onBackPressed();
                return true;
            }
            default:
                return super.onOptionsItemSelected(item);
        }
    }

}