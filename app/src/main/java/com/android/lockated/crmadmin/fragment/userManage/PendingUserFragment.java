package com.android.lockated.crmadmin.fragment.userManage;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.lockated.LockatedApplication;
import com.android.lockated.R;
import com.android.lockated.crmadmin.activity.AdminUserManageActivity;
import com.android.lockated.network.ConnectionDetector;
import com.android.lockated.preferences.LockatedPreferences;
import com.android.lockated.request.LockatedJSONObjectRequest;
import com.android.lockated.request.LockatedRequestError;
import com.android.lockated.request.LockatedVolleyRequestQueue;
import com.android.lockated.utils.ApplicationURL;
import com.android.lockated.utils.Utilities;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;


public class PendingUserFragment extends Fragment implements Response.ErrorListener, Response.Listener<JSONObject> {

    RecyclerView recyclerView;
    TextView errorMsg;
    PendingUserAdapter pendingUserAdapter;
    RequestQueue mQueue;
    String REQUEST_TAG = "PendingUserFragment";
    LockatedPreferences mLockatedPreferences;
    ProgressBar mProgressBarView;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View pendingUser = inflater.inflate(R.layout.fragment_pending_user, container, false);
        Utilities.ladooIntegration(getActivity(), getActivity().getResources().getString(R.string.pendingUser));
        init(pendingUser);
        return pendingUser;
    }

    @Override
    public void onResume() {
        super.onResume();
        LockatedApplication.getInstance().trackScreenView(getString(R.string.pendingUser));
        Utilities.lockatedGoogleAnalytics(getString(R.string.pendingUser),
                getString(R.string.visited), getString(R.string.pendingUser));
        getPendingUser();

    }

    private void init(View pendingUser) {
        mLockatedPreferences = new LockatedPreferences(getActivity());
        recyclerView = (RecyclerView) pendingUser.findViewById(R.id.recyclerView);
        errorMsg = (TextView) pendingUser.findViewById(R.id.noData);
        mProgressBarView = (ProgressBar) pendingUser.findViewById(R.id.mProgressBarView);
        final LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerView.setLayoutManager(linearLayoutManager);
    }

    public void getPendingUser() {

        if (!mLockatedPreferences.getSocietyId().equals("blank")) {
            if (ConnectionDetector.isConnectedToInternet(getActivity())) {
                mProgressBarView.setVisibility(View.VISIBLE);
                String url = ApplicationURL.getPendingUser + mLockatedPreferences.getSocietyId() + "&token="
                        + mLockatedPreferences.getLockatedToken();
                mQueue = LockatedVolleyRequestQueue.getInstance(getActivity()).getRequestQueue();
                LockatedJSONObjectRequest lockatedJSONObjectRequest = new LockatedJSONObjectRequest(Request.Method.GET,
                        url, null, this, this);
                lockatedJSONObjectRequest.setTag(REQUEST_TAG);
                mQueue.add(lockatedJSONObjectRequest);
            } else {
                Utilities.showToastMessage(getActivity(), getActivity().getResources().getString(R.string.internet_connection_error));
            }
        } else {
            recyclerView.setVisibility(View.GONE);
            errorMsg.setVisibility(View.VISIBLE);
            errorMsg.setText(R.string.not_in_society);
        }

    }

    @Override
    public void onErrorResponse(VolleyError error) {
        mProgressBarView.setVisibility(View.INVISIBLE);
        if (getActivity() != null) {
            LockatedRequestError.onRequestError(getActivity(), error);
        }
    }

    @Override
    public void onResponse(JSONObject response) {
        mProgressBarView.setVisibility(View.INVISIBLE);
        if (getActivity() != null) {
            if (response != null && response.length() > 0) {
                try {
                    if (response.getJSONArray("user_societies").length() > 0) {
                        pendingUserAdapter = new PendingUserAdapter(getActivity(), response.getJSONArray("user_societies"));
                        recyclerView.setAdapter(pendingUserAdapter);
                    } else {
                        recyclerView.setVisibility(View.GONE);
                        errorMsg.setVisibility(View.VISIBLE);
                        errorMsg.setText(R.string.no_data_error);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public class PendingUserAdapter extends RecyclerView.Adapter<PendingUserAdapter.PendingUserAdapterViewHolder>
            implements Response.ErrorListener, Response.Listener<JSONObject> {

        Context contextMain;
        JSONArray jsonArray;
        RequestQueue mQueue;
        LockatedPreferences mLockatedPreferences;
        String REQUEST_TAG = "PendingUserAdapterSample";
        int removeItemPosition;
        boolean isName = false, isPhoneNo = false, isFlatNo = false, isDate = false;


        public PendingUserAdapter(Context context, JSONArray jsonArray) {
            this.contextMain = context;
            this.jsonArray = jsonArray;
            mLockatedPreferences = new LockatedPreferences(contextMain);
        }

        @Override
        public PendingUserAdapterViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View v = LayoutInflater.from(contextMain).inflate(R.layout.adapter_pending_user, parent, false);
            PendingUserAdapterViewHolder pvh = new PendingUserAdapterViewHolder(v);
            return pvh;
        }

        @Override
        public void onBindViewHolder(PendingUserAdapterViewHolder holder, final int position) {

            try {

                holder.dateTxt.setText(ddMMYYYFormat(jsonArray.getJSONObject(position).getString("created_at")));

                final String id = jsonArray.getJSONObject(position).getString("id");
                if (!jsonArray.getJSONObject(position).isNull("user")) {
                    String name = jsonArray.getJSONObject(position).getJSONObject("user").getString("firstname")
                            + " " + jsonArray.getJSONObject(position).getJSONObject("user").getString("lastname");
                    holder.nameTxt.setText(name);
                    isName = true;
                } else {
                    holder.nameTxt.setText("No Name Available");
                }
                if (jsonArray.getJSONObject(position).getJSONObject("user_flat").length() != 0
                        && !jsonArray.getJSONObject(position).isNull("user_flat")) {
                    holder.flatNo.setText(jsonArray.getJSONObject(position).getJSONObject("user_flat").getString("flat"));
                    isFlatNo = true;
                }
                if (jsonArray.getJSONObject(position).getJSONObject("user").length() != 0
                        && !jsonArray.getJSONObject(position).isNull("user")) {
                    holder.phoneNo.setText(jsonArray.getJSONObject(position).getJSONObject("user").getString("mobile"));
                }

                if ((isName) || (isPhoneNo) || (isFlatNo) || (isDate)) {
                    holder.accept.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {

                            JSONObject jsonObjectMain = new JSONObject();
                            JSONObject jsonObjectSub = new JSONObject();
                            try {
                                jsonObjectSub.put("approve", "1");
                                jsonObjectMain.put("user_society", jsonObjectSub);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                            getRequest(ApplicationURL.manageUserAdmin + id + ".json?token=", jsonObjectMain);
                        }
                    });
                } else {
                    holder.accept.setEnabled(false);
                    holder.accept.setClickable(false);
                }
                holder.decline.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        JSONObject jsonObjectMain = new JSONObject();
                        JSONObject jsonObjectSub = new JSONObject();
                        try {
                            jsonObjectSub.put("approve", "0");
                            jsonObjectMain.put("user_society", jsonObjectSub);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        getRequest(ApplicationURL.manageUserAdmin + id + ".json?token=", jsonObjectMain);
                    }
                });
            } catch (JSONException e) {
                e.printStackTrace();
            }

        }

        @Override
        public int getItemCount() {
            return jsonArray.length();
        }

        @Override
        public void onAttachedToRecyclerView(RecyclerView recyclerView) {
            super.onAttachedToRecyclerView(recyclerView);
        }

        public class PendingUserAdapterViewHolder extends RecyclerView.ViewHolder {

            TextView nameTxt, dateTxt, phoneNo, flatNo, accept, decline;

            public PendingUserAdapterViewHolder(View itemView) {
                super(itemView);

                nameTxt = (TextView) itemView.findViewById(R.id.nameValue);
                dateTxt = (TextView) itemView.findViewById(R.id.mTextDate);
                accept = (TextView) itemView.findViewById(R.id.mViewDetails);
                decline = (TextView) itemView.findViewById(R.id.mContactAuthor);
                phoneNo = (TextView) itemView.findViewById(R.id.phoneValue);
                flatNo = (TextView) itemView.findViewById(R.id.flatValue);

            }

        }

        public void getRequest(String url, JSONObject jsonObject) {
            if (!mLockatedPreferences.getSocietyId().equals("blank")) {
                if (ConnectionDetector.isConnectedToInternet(contextMain)) {
                    String mainUrl = url + mLockatedPreferences.getLockatedToken()
                            + "&id_society=" + mLockatedPreferences.getSocietyId();
                    LockatedVolleyRequestQueue lockatedVolleyRequestQueue = LockatedVolleyRequestQueue.getInstance(contextMain);
                    lockatedVolleyRequestQueue.sendRequest(REQUEST_TAG, Request.Method.PUT, mainUrl, jsonObject, this, this);
                    /*mQueue = LockatedVolleyRequestQueue.getInstance(contextMain).getRequestQueue();
                    String mainUrl = url + mLockatedPreferences.getLockatedToken()
                            + "&id_society=" + mLockatedPreferences.getSocietyId();
                    LockatedJSONObjectRequest lockatedJSONObjectRequest = new LockatedJSONObjectRequest(Request.Method.PUT,
                            mainUrl, jsonObject, this, this);
                    lockatedJSONObjectRequest.setTag(REQUEST_TAG);
                    mQueue.add(lockatedJSONObjectRequest);*/
                } else {
                    Utilities.showToastMessage(contextMain, contextMain.getResources().getString(R.string.internet_connection_error));
                }
            } else {
                Toast.makeText(contextMain, contextMain.getResources().getString(R.string.not_in_society), Toast.LENGTH_LONG).show();
            }
        }

        @Override
        public void onErrorResponse(VolleyError error) {
            if (contextMain != null) {
                LockatedRequestError.onRequestError(contextMain, error);
            }
        }

        @Override
        public void onResponse(JSONObject response) {
            if (contextMain != null) {
                try {
                    Intent intent = new Intent(contextMain, AdminUserManageActivity.class);
                    intent.putExtra("AdminUserPosition", 0);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    contextMain.startActivity(intent);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }

        public String ddMMYYYFormat(String dateStr) {
            if (dateStr != null && !dateStr.equals("null")) {
                DateFormat format = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);
                Date date;
                try {
                    date = format.parse(dateStr);
                    dateStr = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH).format(date);
                } catch (ParseException e) {
                    e.printStackTrace();
                    dateStr = "No Date";
                }
            } else {
                dateStr = "No Date";
            }
            return dateStr;
        }

    }

}
