
package com.android.lockated.model.usermodel.Facilities.FacilityHistory;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class FacilityHistory {

    @SerializedName("booked")
    @Expose
    private ArrayList<Booked> booked = new ArrayList<Booked>();

    /**
     * 
     * @return
     *     The booked
     */
    public ArrayList<Booked> getBooked() {
        return booked;
    }

    /**
     * 
     * @param booked
     *     The booked
     */
    public void setBooked(ArrayList<Booked> booked) {
        this.booked = booked;
    }

}
