package com.android.lockated.crmadmin.detailView;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.TextView;

import com.android.lockated.R;

public class PublishedHelpDeskActivity extends AppCompatActivity {

    private TextView mFlatNumber;
    private TextView mTextStatus;
    private TextView mpostedBy;
    private TextView mTextCategory;
    private TextView mIssueCategory;
    private TextView mIssueDescription;
    private TextView mPostedOn;
    private TextView mUpdatedOn;
    private TextView mIssueType;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_published_help_desk);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(true);
        getSupportActionBar().setTitle(R.string.detailview);
        init();
        DataGetSet();
    }

    private void init() {

        mFlatNumber = (TextView) findViewById(R.id.mFlatNumber);
        mTextStatus = (TextView) findViewById(R.id.mTextStatus);
        mpostedBy = (TextView) findViewById(R.id.mpostedBy);
        mTextCategory = (TextView) findViewById(R.id.mTextCategory);
        mIssueCategory = (TextView) findViewById(R.id.mIssueCategory);
        mPostedOn = (TextView) findViewById(R.id.mPostedOn);
        mTextStatus = (TextView) findViewById(R.id.mTextStatus);
        mUpdatedOn = (TextView) findViewById(R.id.mUpdatedOn);
        mIssueType = (TextView) findViewById(R.id.mIssueType);
        mIssueDescription = (TextView) findViewById(R.id.mIssueDescription);
    }

    private void DataGetSet() {
        mIssueType.setText(getIntent().getExtras().getString("strIssueType"));
        mIssueDescription.setText(getIntent().getExtras().getString("strDescription"));
        mFlatNumber.setText(getIntent().getExtras().getString("FlatNumber"));
        mpostedBy.setText(getIntent().getExtras().getString("strpostedBy"));
        mUpdatedOn.setText(getIntent().getExtras().getString("strmUpdatedOn"));
        mPostedOn.setText(getIntent().getExtras().getString("strPostedOn"));
        mIssueCategory.setText(getIntent().getExtras().getString("strIssueCategory"));
        mTextCategory.setText(getIntent().getExtras().getString("strmTextCategory"));
        mTextStatus.setText(getIntent().getExtras().getString("strStatus"));
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home: {
                super.onBackPressed();
                return true;
            }
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
