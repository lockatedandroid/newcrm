
package com.android.lockated.model.PurposeArray;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class PurposesArray {

    @SerializedName("soc_visit_purposes")
    @Expose
    private ArrayList<String> socVisitPurposes = new ArrayList<String>();
    @SerializedName("society_mimo_purposes")
    @Expose
    private ArrayList<String> societyMimoPurposes = new ArrayList<String>();

    /**
     * @return The socVisitPurposes
     */
    public ArrayList<String> getSocVisitPurposes() {
        return socVisitPurposes;
    }

    /**
     * @param socVisitPurposes The soc_visit_purposes
     */
    public void setSocVisitPurposes(ArrayList<String> socVisitPurposes) {
        this.socVisitPurposes = socVisitPurposes;
    }

    /**
     * @return The societyMimoPurposes
     */
    public ArrayList<String> getSocietyMimoPurposes() {
        return societyMimoPurposes;
    }

    /**
     * @param societyMimoPurposes The society_mimo_purposes
     */
    public void setSocietyMimoPurposes(ArrayList<String> societyMimoPurposes) {
        this.societyMimoPurposes = societyMimoPurposes;
    }

}
