package com.android.lockated.categories.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.android.lockated.Interfaces.IRecyclerItemClickListener;
import com.android.lockated.Interfaces.IServiceViewListener;
import com.android.lockated.R;
import com.android.lockated.categories.services.model.ServiceRowData;
import com.android.lockated.holder.RecyclerViewHolder;

import java.util.ArrayList;

public class ServiceRowAdapter extends RecyclerView.Adapter<RecyclerViewHolder> {
    private ArrayList<ServiceRowData> mServiceRowDataList;
    private IServiceViewListener mIServiceViewListener;
    private IRecyclerItemClickListener mIRecyclerItemClickListener;

    public ServiceRowAdapter(ArrayList<ServiceRowData> data, IRecyclerItemClickListener listener, IServiceViewListener viewListener) {
        mServiceRowDataList = data;
        mIServiceViewListener = viewListener;
        mIRecyclerItemClickListener = listener;
    }

    @Override
    public RecyclerViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.fragment_service_row, parent, false);
        return new RecyclerViewHolder(itemView, mIRecyclerItemClickListener, mIServiceViewListener);
    }

    @Override
    public void onBindViewHolder(RecyclerViewHolder holder, int position) {
        holder.serviceRowViewHolder(mServiceRowDataList.get(position));
    }

    @Override
    public int getItemCount() {
        return mServiceRowDataList.size();
    }
}
