package com.android.lockated;

import android.Manifest;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.NavigationView;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.android.lockated.cart.ecommerce.EcommerceCartActivity;
import com.android.lockated.categories.zomato.activity.ZomatoOrderItemListActivity;
import com.android.lockated.drawer.PoliciesActivity;
import com.android.lockated.information.AccountController;
import com.android.lockated.landing.AccountFragment;
import com.android.lockated.landing.HomeFragment;
import com.android.lockated.landing.LocationFragment;
import com.android.lockated.landing.SupportFragment;
import com.android.lockated.model.AccountData;
import com.android.lockated.model.CartDetail;
import com.android.lockated.model.ResidenceDatas;
import com.android.lockated.network.ConnectionDetector;
import com.android.lockated.offers.fragment.OffersFragment;
import com.android.lockated.preferences.LockatedPreferences;
import com.android.lockated.pushnotification.UpdateAppActivity;
import com.android.lockated.request.LockatedRequestError;
import com.android.lockated.request.LockatedVolleyRequestQueue;
import com.android.lockated.utils.ApplicationURL;
import com.android.lockated.utils.Utilities;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.genie.Genie;
import com.library.zomato.ordering.common.OrderSDK;
import com.library.zomato.ordering.common.OrderSDKConfig;
import com.library.zomato.ordering.common.OrderSDKInitializer;
import com.library.zomato.ordering.data.ZTab;
import com.library.zomato.ordering.listeners.MainAppCommunicator;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

public class HomeActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener,
        View.OnClickListener, Response.Listener<JSONObject>, Response.ErrorListener, MainAppCommunicator {

    private ImageView mImageViewHome;
    private ImageView mImageViewChat;
    private ImageView mImageViewAccount;
    private ImageView mImageViewLocation;
    private ImageView mImageViewOffers;
    private AccountController mAccountController;
    boolean nValue;
    private boolean doubleBackToExitPressedOnce;


    private LinearLayout mLinearLayoutHome;
    private LinearLayout mLinearLayoutSupport;
    private LinearLayout mLinearLayoutAccount;
    private LinearLayout mLinearLayoutLocation;
    private LinearLayout mLinearLayoutOffers;

    private static final int REQUEST_CALL = 11;
    public static final String REQUEST_TAG = "HomeActivity";
    public static final String GET_ROLES_TAG = "GET_ROLES_TAG";
    public static final String USER_SOCIETIES_TAG = "HomeActivityUserSocieties";
    private static final String GET_REQUEST_POST_TAG = "POST SINGLE ORDER";
    private static final String POST_ZOMATO_TOKEN = "POST ZOMATO TOKEN";

    private LockatedPreferences mLockatedPreferences;
    boolean onSavedInstanceStateCalled;
    /* ArrayList<UserSociety> userSocietyArrayList;
     UserSocietyAdapter userSocietyAdapter;*/
    String tagSocietyId;
    static int order_status;
    String societyNames[];
    //666-default value

    // Zomato Credentials
    String API_KEY = "667e36a4ff378517f6d2445ce90c5702";
    String SECRET_KEY = "295213ef731b4b56c26bff0c1af062c1";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        OrderSDK.getInstance().setMainApplicationCallback(HomeActivity.this);
        OrderSDKConfig.setHeaderActionButton1(getResources().getDrawable(R.drawable.action_booking_zomato), "My Orders");
        onSavedInstanceStateCalled = false;
        try {
            Genie.getInstance().onActivityStart(this);
        } catch (Exception e) {
            e.printStackTrace();
        }
        mLockatedPreferences = new LockatedPreferences(this);
        mAccountController = AccountController.getInstance();

        getResidenceData();
        /*userSocietyArrayList = new ArrayList<>();
        userSocietyAdapter = new UserSocietyAdapter(this, userSocietyArrayList);*/

        /*if (mLockatedPreferences.getAccountData().getmResidenceDataList().size() > 0) {*/
        tagSocietyId = String.valueOf(mLockatedPreferences.getSocietyId());
      /*  }*/
        nValue = mLockatedPreferences.getNotificationValue();
        if (nValue) {
            getScreenDetail();
        }
        addToolBar();
        getRolesData();
        //getUserSocietyLists();
        setBottomTabView(savedInstanceState);
        if (getIntent().getExtras() != null) {
            String calledFragmentName = getIntent().getExtras().getString("EditPersonalInformationFragment");
            if (calledFragmentName != null && calledFragmentName.equals("EditPersonalInformationFragment")) {
                onAccountClicked(0);
            }
        }
        //LockatedApplication lockatedApplication = new LockatedApplication();


    }

    public void addToolBar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        /*toolbar.setLogo(R.drawable.lockated);*/
        LinearLayout logoLayout = (LinearLayout) findViewById(R.id.logoLayout);
        if (logoLayout != null) {
            logoLayout.setVisibility(View.VISIBLE);
        }
        setSupportActionBar(toolbar);
        toolbar.setTitle("");

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, drawer, toolbar,
                R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
    }

    private void setBottomTabView(Bundle savedInstanceState) {

        /*View view;
        LayoutInflater inflater = (LayoutInflater)   this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        view = inflater.inflate(R.layout.tab_bar_view, null);*/

        mLinearLayoutHome = (LinearLayout) findViewById(R.id.mLinearLayoutHome);
        mLinearLayoutSupport = (LinearLayout) findViewById(R.id.mLinearLayoutSupport);
        mLinearLayoutAccount = (LinearLayout) findViewById(R.id.mLinearLayoutAccount);
        mLinearLayoutLocation = (LinearLayout) findViewById(R.id.mLinearLayoutLocation);
        mLinearLayoutOffers = (LinearLayout) findViewById(R.id.mLinearLayoutOffers);

        mImageViewHome = (ImageView) findViewById(R.id.mImageViewHome);
        mImageViewChat = (ImageView) findViewById(R.id.mImageViewChat);
        mImageViewAccount = (ImageView) findViewById(R.id.mImageViewAccount);
        mImageViewLocation = (ImageView) findViewById(R.id.mImageViewLocation);
        mImageViewOffers = (ImageView) findViewById(R.id.mImageViewOffers);

        mLinearLayoutHome.setOnClickListener(this);
        mLinearLayoutSupport.setOnClickListener(this);
        mLinearLayoutAccount.setOnClickListener(this);
        mLinearLayoutLocation.setOnClickListener(this);
        mLinearLayoutOffers.setOnClickListener(this);

        LockatedApplication.getInstance().trackCampaign("HomeActivity");
        notificationResult(savedInstanceState);
    }

    public void homeFragment(int postion, int taxon_id, int otype_id) {
        mLockatedPreferences.setLogout(false);
        HomeFragment homeFragment = new HomeFragment();
        homeFragment.setHomeFragmentData(postion, taxon_id, otype_id);
        getSupportFragmentManager().beginTransaction().add(R.id.mLandingContainer, homeFragment).commit();
        onHomeFragmentClicked();
    }

    public void offerFragment() {
        OffersFragment offersFragment = new OffersFragment();
        getSupportFragmentManager().beginTransaction().add(R.id.mLandingContainer, offersFragment).commit();
    }

    public void getUserSocietyLists() {
        if (ConnectionDetector.isConnectedToInternet(this)) {
            String url = ApplicationURL.getUserSocietyLists + mLockatedPreferences.getLockatedToken();
            LockatedVolleyRequestQueue lockatedVolleyRequestQueue = LockatedVolleyRequestQueue.getInstance(this);
            lockatedVolleyRequestQueue.sendRequest(USER_SOCIETIES_TAG, Request.Method.GET, url, null, this, this);
        } else {
            Utilities.showToastMessage(this, this.getResources().getString(R.string.internet_connection_error));
        }
    }

    public void getResidenceData() {
        if (ConnectionDetector.isConnectedToInternet(this)) {
            String url = ApplicationURL.getResidenceData + mLockatedPreferences.getLockatedToken();
            LockatedVolleyRequestQueue lockatedVolleyRequestQueue = LockatedVolleyRequestQueue.getInstance(this);
            lockatedVolleyRequestQueue.sendRequest(USER_SOCIETIES_TAG, Request.Method.GET, url, null, this, this);
        } else {
            Utilities.showToastMessage(this, this.getResources().getString(R.string.internet_connection_error));
        }
    }

    private void getScreenDetail() {
        if (ConnectionDetector.isConnectedToInternet(this)) {
            String url = ApplicationURL.getScreenUrl + "?token=" + mLockatedPreferences.getLockatedToken();
            LockatedVolleyRequestQueue lockatedVolleyRequestQueue = LockatedVolleyRequestQueue.getInstance(this);
            lockatedVolleyRequestQueue.sendRequest(REQUEST_TAG, Request.Method.GET, url, null, this, this);
        } else {
            Utilities.showToastMessage(this, this.getResources().getString(R.string.internet_connection_error));
        }
    }

    private void getRolesData() {
        if (ConnectionDetector.isConnectedToInternet(this)) {
            String url = ApplicationURL.getRolesDataUrl + mLockatedPreferences.getLockatedToken()
                    + ApplicationURL.societyId + tagSocietyId;
            LockatedVolleyRequestQueue mLockatedVolleyRequestQueue = LockatedVolleyRequestQueue.getInstance(this);
            mLockatedVolleyRequestQueue.sendRequest(GET_ROLES_TAG, Request.Method.GET, url, null, this, this);
        } else {
            Utilities.showToastMessage(this, this.getResources().getString(R.string.internet_connection_error));
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.home, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_cart:
                onCartClicked();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    private void onCartClicked() {
        ArrayList<AccountData> accountDatas = AccountController.getInstance().getmAccountDataList();
        if (accountDatas.size() > 0) {
            ArrayList<CartDetail> cartDetails = accountDatas.get(0).getCartDetailList();
            if (cartDetails.size() > 0) {
                if (cartDetails.get(0).getmLineItemData().size() > 0) {
                    Intent intent = new Intent(HomeActivity.this, EcommerceCartActivity.class);
                    startActivity(intent);
                } else {
                    Utilities.showToastMessage(HomeActivity.this, getString(R.string.empty_cart));
                }
            } else {
                Utilities.showToastMessage(HomeActivity.this, getString(R.string.empty_cart));
            }
        }
    }

    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.about_us) {
            onPoliciesClicked(getString(R.string.about_us), getString(R.string.about_us_link));
            LockatedApplication.getInstance().trackScreenView(getString(R.string.about_us));
            Utilities.lockatedGoogleAnalytics(getString(R.string.about_us), getString(R.string.visited), getString(R.string.about_us));
        } /*else if (id == R.id.alertDialog) {
            if (societyNames.length == 0) {
                Utilities.showToastMessage(this, "No society list available");
            } else if (societyNames.length == 1) {
                Utilities.showToastMessage(this, "You are associated with only " + societyNames[0]);
            } else {
                chooseSocietyAlertDialog(this);
            }
        } */ else if (id == R.id.rating) {
            onRatingClicked();
            Utilities.lockatedGoogleAnalytics(getString(R.string.rating), getString(R.string.visited), getString(R.string.rating));
        } else if (id == R.id.feedback) {
            onFeedbackClicked();
            Utilities.lockatedGoogleAnalytics(getString(R.string.feedback), "Click", getString(R.string.feedback));
        } else if (id == R.id.invite_friends) {
            onShareClicked();
            Utilities.lockatedGoogleAnalytics(getString(R.string.invite_friends), "Invite", getString(R.string.invite_friends));
        } else if (id == R.id.contact_us) {
            onContactUsClicked();
            Utilities.lockatedGoogleAnalytics(getString(R.string.contact_us), "Contact", getString(R.string.contact_us));
        } else if (id == R.id.call_us) {
            TelephonyManager tm = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
            if (tm.getPhoneType() == TelephonyManager.PHONE_TYPE_NONE) {
                Utilities.showToastMessage(this, "Call feature not available in this device");
            } else {
                onCallUsClicked();
            }
            Utilities.lockatedGoogleAnalytics(getString(R.string.call_us), getString(R.string.call_us), getString(R.string.call_us));
        } else if (id == R.id.refund_policy) {
            onPoliciesClicked(getString(R.string.refund_policy), getString(R.string.refund_link));
            LockatedApplication.getInstance().trackScreenView(getString(R.string.refund_policy));
            Utilities.lockatedGoogleAnalytics(getString(R.string.refund_policy), getString(R.string.visited), getString(R.string.refund_policy));
        } else if (id == R.id.privacy_policy) {
            onPoliciesClicked(getString(R.string.privacy_policy), getString(R.string.privacy_link));
            LockatedApplication.getInstance().trackScreenView(getString(R.string.privacy_policy));
            Utilities.lockatedGoogleAnalytics(getString(R.string.privacy_policy), getString(R.string.visited), getString(R.string.privacy_policy));
        } else if (id == R.id.terms_conditions) {
            onPoliciesClicked(getString(R.string.terms_conditions), getString(R.string.terms_condition_link));
            LockatedApplication.getInstance().trackScreenView(getString(R.string.terms_conditions));
            Utilities.lockatedGoogleAnalytics(getString(R.string.terms_conditions), getString(R.string.visited), getString(R.string.terms_conditions));
        } else if (id == R.id.logout) {
            showAlertDialog(HomeActivity.this);
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    private void onCallUsClicked() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                requestPermissions(new String[]{Manifest.permission.CALL_PHONE}, REQUEST_CALL);
            } else {
                callingIntent();
            }
        } else {
            callingIntent();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == REQUEST_CALL) {
            if (grantResults.length == 1 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                callingIntent();
            }
        }
    }

    private void callingIntent() {
        Intent intent = new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + getString(R.string.calling_number)));
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        startActivity(intent);
    }

    private void onRatingClicked() {
        Uri uri = Uri.parse("market://details?id=" + getPackageName());
        Intent myAppLinkToMarket = new Intent(Intent.ACTION_VIEW, uri);
        try {
            startActivity(myAppLinkToMarket);
        } catch (ActivityNotFoundException e) {
            Utilities.showToastMessage(HomeActivity.this, "Unable to find application on Play store.");
        }
    }

    private void onShareClicked() {
        Intent sendIntent = new Intent();
        sendIntent.setAction(Intent.ACTION_SEND);
        sendIntent.putExtra(Intent.EXTRA_TEXT, getString(R.string.sharing_url));
        sendIntent.setType("text/plain");
        startActivity(Intent.createChooser(sendIntent, getResources().getText(R.string.share_message)));
    }

    private void onPoliciesClicked(String name, String url) {
        if (ConnectionDetector.isConnectedToInternet(HomeActivity.this)) {
            Intent detailIntent = new Intent(HomeActivity.this, PoliciesActivity.class);
            ((LockatedApplication) getApplicationContext()).setSectionName(name);
            ((LockatedApplication) getApplicationContext()).setSectionURL(url);
            startActivity(detailIntent);
        } else {
            Utilities.showToastMessage(HomeActivity.this, getString(R.string.internet_connection_error));
        }
    }

    private void onFeedbackClicked() {
        Intent intent = new Intent(Intent.ACTION_SEND);
        intent.setType("message/rfc822");
        intent.putExtra(Intent.EXTRA_EMAIL, new String[]{getString(R.string.feedback_email)});
        intent.putExtra(Intent.EXTRA_SUBJECT, getString(R.string.feedback_subject));
        startActivity(Intent.createChooser(intent, getString(R.string.contact_us_message)));
    }

    private void onContactUsClicked() {
        Intent intent = new Intent(Intent.ACTION_SEND);
        intent.setType("message/rfc822");
        intent.putExtra(Intent.EXTRA_EMAIL, new String[]{getString(R.string.contact_us_email)});
        intent.putExtra(Intent.EXTRA_SUBJECT, getString(R.string.contact_us_subject));
        startActivity(Intent.createChooser(intent, getString(R.string.contact_us_message)));
    }

    public void showAlertDialog(Context context) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setMessage(R.string.logout_dialog_message);

        builder.setPositiveButton(R.string.logout, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.dismiss();
                onLogout();
            }
        });

        builder.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.dismiss();
            }
        });

        AlertDialog dialog = builder.create();
        dialog.show();
    }

    private void onLogout() {
        mAccountController.getmAccountDataList().clear();
        mLockatedPreferences.setLockatedToken("");
        mLockatedPreferences.setLogout(true);
       /* String AvatarImage = mLockatedPreferences.getImageAvatar();
        String mFNmae = mLockatedPreferences.getPersonalName();
        String mLNmae = mLockatedPreferences.getLastName();
        String mEmail = mLockatedPreferences.getEmailAddress();*/
        mLockatedPreferences.clear();
        /*mLockatedPreferences.setImageAvatar(AvatarImage);
        mLockatedPreferences.setPersonalName(mFNmae);
        mLockatedPreferences.setLastName(mLNmae);
        mLockatedPreferences.setEmailAddress(mEmail);*/
        OrderSDK.getInstance().logoutUser();
        OrderSDKInitializer.getNewInstance(API_KEY, SECRET_KEY).initializeForExternalApp(getApplicationContext());
        Utilities.lockatedGoogleAnalytics(getString(R.string.logout), "Logout", getString(R.string.logout));

        ((LockatedApplication) this.getApplicationContext()).setMyAddressData(null);
        Intent intent = new Intent(HomeActivity.this, IndexActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
        finish();
    }

    /*public void chooseSocietyAlertDialog(Context context) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle(R.string.choose_society_dialog_message);
        builder.setAdapter(userSocietyAdapter, null);
        AlertDialog dialog = builder.create();
        dialog.show();
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = 400;
        dialog.getWindow().setAttributes(lp);

    }
*/
    @Override
    public void onErrorResponse(VolleyError error) {
        LockatedRequestError.onRequestError(this, error);
    }

    @Override
    public void onResponse(JSONObject response) {
        try {
            if (response.has("number_verified")) {
                int numberVerified = response.getInt("number_verified");

                String number = response.getString("mobile");
                mLockatedPreferences.setContactNumber(number);

                AccountData accountData = new AccountData(response);
                mLockatedPreferences.setUserSocietyId(accountData.getSelected_user_society());
                mLockatedPreferences.setAccountData(accountData);
                mAccountController.mAccountDataList.add(accountData);
//                mAccountController.notify();

                mLockatedPreferences.setNotificationValue(false);
                if (nValue) {
                    onHomeClicked();
                }
            } else if (response.has("user_flats")) {
                JSONObject jsonObject = (JSONObject) response;
                ResidenceDatas residenceDatas = new ResidenceDatas(jsonObject);
                mAccountController.residenceDatases.add(residenceDatas);
            } else if (response.has("name")) {
                mLockatedPreferences.setRolesJson(response.toString());
                getScreenDetail();
            } /*else if (response.has("user_societies")) {
                JSONArray jsonArray = response.getJSONArray("user_societies");
                Gson gson = new Gson();
                Log.e("length",""+ jsonArray.length());
                for (int i = 0; i < jsonArray.length(); i++) {
                    UserSociety userSocietiesData = gson.fromJson(jsonArray.getJSONObject(i).toString(), UserSociety.class);
                    userSocietyArrayList.add(userSocietiesData);
                }
                societyNames = new String[userSocietyArrayList.size()];
                for (int i = 0; i < userSocietyArrayList.size(); i++) {
                    societyNames[i] = userSocietyArrayList.get(i).getSociety().getBuildingName();
                }
                userSocietyAdapter.notifyDataSetChanged();
            } else {
            }*/
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.mLinearLayoutHome:
                onHomeClicked();
                break;
            case R.id.mLinearLayoutSupport:
                onSupportClicked();
                break;
            case R.id.mLinearLayoutAccount:
                onAccountClicked(0);
                break;
            case R.id.mLinearLayoutLocation:
                onLocationClicked();
                break;
            case R.id.mLinearLayoutOffers:
                onOfferClicked();
                break;
        }
    }

    private void onHomeClicked() {
        HomeFragment homeFragment = new HomeFragment();
        homeFragment.setHomeFragmentData(666, 666, 666);
        replaceFragment(homeFragment);
        onHomeFragmentClicked();
    }

    private void onLocationClicked() {
        LocationFragment locationFragment = new LocationFragment();
        replaceFragment(locationFragment);
        onLocationFragmentClicked();
    }

    public void onAccountClicked(int position) {
        AccountFragment accountFragment = new AccountFragment();
        accountFragment.setFragment(position);
        replaceFragment(accountFragment);
        onAccountFragmentClicked();
    }

    private void onSupportClicked() {
        SupportFragment supportFragment = new SupportFragment();
        supportFragment.setChatId(10, getString(R.string.support_screen));
        replaceFragment(supportFragment);
        onChatFragmentClicked();
    }

    public void onOfferClicked() {
        OffersFragment offersFragment = new OffersFragment();
        replaceFragment(offersFragment);
        onOffersFragmentClicked();
    }

    public void notificationResult(Bundle savedInstanceState) {
        if (getIntent().getExtras() != null && getIntent().getExtras().containsKey("offer")
                && getIntent().getExtras().getString("offer").equalsIgnoreCase("offer")) {
            offerFragment();
        } else if (getIntent().getExtras() != null && getIntent().getExtras().containsKey("offersOtypeId")) {
            int offersOtypeId = getIntent().getExtras().getInt("offersOtypeId");
            int taxon_id = getIntent().getExtras().getInt("taxon_id");
            int otype_id = getIntent().getExtras().getInt("otype_id");
            homeFragment(offersOtypeId, taxon_id, otype_id);
        } else if (getIntent().getExtras() != null && getIntent().getExtras().containsKey("HomeActivityCalled")) {
            String name = getIntent().getExtras().getString("HomeActivityCalled");
            if (name != null && name.equals(getApplicationContext().getResources().getString(R.string.home_clicked))) {
                homeFragment(666, 666, 666);
            } else if (name != null && name.equals(getApplicationContext().getResources().getString(R.string.chat_support_clicked))) {
                onSupportClicked();
            } else if (name != null && name.equals(getApplicationContext().getResources().getString(R.string.account_clicked))) {
                onAccountClicked(0);
            } else if (name != null && name.equals(getApplicationContext().getResources().getString(R.string.location_clicked))) {
                onLocationClicked();
            } else if (name != null && name.equals(getApplicationContext().getResources().getString(R.string.offer_clicked))) {
                onOfferClicked();
            } else {
                homeFragment(666, 666, 666);
            }
        } else if (getIntent().getExtras() != null && getIntent().getExtras().containsKey("FLAG_ACTIVITY_CLEAR_TOP")) {
            updateAppCalled(getIntent().getExtras().getString("force_close"), getIntent().getExtras().getString("message"));
        } else if (savedInstanceState == null) {
            homeFragment(666, 666, 666);
        }
    }

    public void updateAppCalled(String force_close, String message) {
        Intent resultIntent = new Intent(getApplicationContext(), UpdateAppActivity.class);
        resultIntent.putExtra("FLAG_ACTIVITY_CLEAR_TOP", false);
        resultIntent.putExtra("force_close", force_close);
        resultIntent.putExtra("message", message);
        startActivity(resultIntent);
    }

    public void replaceFragment(Fragment fragment) {
        String backStateName = fragment.getClass().getName();
        FragmentManager manager = getSupportFragmentManager();
        boolean fragmentPopped = manager.popBackStackImmediate(backStateName, 0);

        if (!fragmentPopped && manager.findFragmentByTag(backStateName) == null) {
            FragmentTransaction ft = manager.beginTransaction();
            ft.replace(R.id.mLandingContainer, fragment, backStateName);
            ft.commit();
        }
    }

    private void onHomeFragmentClicked() {
        mLinearLayoutHome.setBackgroundColor(getResources().getColor(R.color.selected_edit_Box_login));
        mLinearLayoutSupport.setBackgroundColor(getResources().getColor(R.color.home_tab));
        mLinearLayoutAccount.setBackgroundColor(getResources().getColor(R.color.home_tab));
        mLinearLayoutLocation.setBackgroundColor(getResources().getColor(R.color.home_tab));
        mLinearLayoutOffers.setBackgroundColor(getResources().getColor(R.color.home_tab));
    }

    private void onChatFragmentClicked() {
        mLinearLayoutHome.setBackgroundColor(getResources().getColor(R.color.home_tab));
        mLinearLayoutSupport.setBackgroundColor(getResources().getColor(R.color.selected_edit_Box_login));
        mLinearLayoutAccount.setBackgroundColor(getResources().getColor(R.color.home_tab));
        mLinearLayoutLocation.setBackgroundColor(getResources().getColor(R.color.home_tab));
        mLinearLayoutOffers.setBackgroundColor(getResources().getColor(R.color.home_tab));
    }

    private void onAccountFragmentClicked() {
        mLinearLayoutHome.setBackgroundColor(getResources().getColor(R.color.home_tab));
        mLinearLayoutSupport.setBackgroundColor(getResources().getColor(R.color.home_tab));
        mLinearLayoutAccount.setBackgroundColor(getResources().getColor(R.color.selected_edit_Box_login));
        mLinearLayoutLocation.setBackgroundColor(getResources().getColor(R.color.home_tab));
        mLinearLayoutOffers.setBackgroundColor(getResources().getColor(R.color.home_tab));
    }

    private void onLocationFragmentClicked() {
        mLinearLayoutHome.setBackgroundColor(getResources().getColor(R.color.home_tab));
        mLinearLayoutSupport.setBackgroundColor(getResources().getColor(R.color.home_tab));
        mLinearLayoutAccount.setBackgroundColor(getResources().getColor(R.color.home_tab));
        mLinearLayoutLocation.setBackgroundColor(getResources().getColor(R.color.selected_edit_Box_login));
        mLinearLayoutOffers.setBackgroundColor(getResources().getColor(R.color.home_tab));
    }

    private void onOffersFragmentClicked() {
        mLinearLayoutHome.setBackgroundColor(getResources().getColor(R.color.home_tab));
        mLinearLayoutSupport.setBackgroundColor(getResources().getColor(R.color.home_tab));
        mLinearLayoutAccount.setBackgroundColor(getResources().getColor(R.color.home_tab));
        mLinearLayoutLocation.setBackgroundColor(getResources().getColor(R.color.home_tab));
        mLinearLayoutOffers.setBackgroundColor(getResources().getColor(R.color.selected_edit_Box_login));
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.mLandingContainer);
            if (fragment instanceof SupportFragment) {
                if (((SupportFragment) fragment).isSampleMessageSelected) {
                    ((SupportFragment) fragment).isSampleMessageSelected = false;
                    ((SupportFragment) fragment).mSupportListSample.setVisibility(View.GONE);
                    ((SupportFragment) fragment).mImageViewSampleMessage.setImageResource(R.drawable.ic_sample_message_disable);
                } else {
                    if (!(fragment instanceof HomeFragment)) {
                        onHomeClicked();
                    } else if (fragment instanceof OffersFragment) {
                        onHomeClicked();
                    } else {
                        onApplicationBackPressed();
                    }
                }
            } else if (!(fragment instanceof HomeFragment)) {
                onHomeClicked();
            } else {
                onApplicationBackPressed();
            }
        }
    }

    private void onApplicationBackPressed() {
        if (doubleBackToExitPressedOnce) {
            super.onBackPressed();
            return;
        }

        this.doubleBackToExitPressedOnce = true;
        Utilities.showToastMessage(this, getString(R.string.back_twice));

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                doubleBackToExitPressedOnce = false;
            }
        }, 2000);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        onSavedInstanceStateCalled = true;
    }

    //////Zomato

    @Override
    public void zomatoGetAllActiveTabs(ArrayList<ZTab> arrayList) {

    }

    @Override
    public void zomatoGetTab(ZTab zTab) {
        JSONObject main_jsonObject = new JSONObject();
        JSONArray child_jsonArray_1 = new JSONArray();
        JSONObject child_jsonObject_1 = new JSONObject();
        try {

            main_jsonObject.put("Id", zTab.getId());
            main_jsonObject.put("forandroid", "1");
            main_jsonObject.put("OrderStatus", order_status);
            main_jsonObject.put("CreatedTimestampStr", zTab.getCreatedTimestampStr());
            main_jsonObject.put("DeliveryAddressString", zTab.getDeliveryAddressString());
            main_jsonObject.put("DeliveryLabel", zTab.getDeliveryLabel());
            main_jsonObject.put("DeliveryMessage", zTab.getDeliveryMessage());
            main_jsonObject.put("DeliveryMode", zTab.getDeliveryMode());
            main_jsonObject.put("DeliveryStatus", zTab.getDeliveryStatus());
            main_jsonObject.put("YourDeliveryRating", zTab.getYourDeliveryRating());
            main_jsonObject.put("BillNumber", zTab.getBillNumber());
            main_jsonObject.put("DominosOrderID", zTab.getDominosOrderID());
            main_jsonObject.put("DishString", zTab.getDishString());
            main_jsonObject.put("LinkCode", zTab.getLinkCode());
            main_jsonObject.put("TotalCost", zTab.getTotalCost());
            main_jsonObject.put("OrderTotalAmount", zTab.getOrderTotalAmount());
            main_jsonObject.put("Currency", zTab.getCurrency());
            main_jsonObject.put("getPaymentStatus", zTab.getPaymentStatus());
            main_jsonObject.put("PaymentMethod", zTab.getPaymentMethod());
            main_jsonObject.put("PaymentText", zTab.getPaymentText());
            main_jsonObject.put("TipEnabled", zTab.getTipEnabled());
            main_jsonObject.put("TipPercentage", zTab.getTipPercentage());
            main_jsonObject.put("TipMax", zTab.getTipMax());
            main_jsonObject.put("UserAddress_a", zTab.getUserAddress().a());
            main_jsonObject.put("UserAddress_b", zTab.getUserAddress().b());
            main_jsonObject.put("UserAddress_c", zTab.getUserAddress().c());
            main_jsonObject.put("UserAddress_d", zTab.getUserAddress().d());
            main_jsonObject.put("UserAddress_e", zTab.getUserAddress().e());
            main_jsonObject.put("UserAddress_f", zTab.getUserAddress().f());
            main_jsonObject.put("UserAddress_g", zTab.getUserAddress().g());
            main_jsonObject.put("UserAddress_h", zTab.getUserAddress().h());
            main_jsonObject.put("UserAddress_i", zTab.getUserAddress().i());

            main_jsonObject.put("UserPhoneCountryId", zTab.getUserPhoneCountryId());
            main_jsonObject.put("UserPhone", zTab.getUserPhone());
            main_jsonObject.put("Order Id", zTab.getResTableId());
            main_jsonObject.put("RestaurantAvgPickupTime", zTab.getRestaurant().getAvgPickupTime());
            main_jsonObject.put("RestaurantCostForOne", zTab.getRestaurant().getCostForOne());
            main_jsonObject.put("RestaurantCostForTwo", zTab.getRestaurant().getCostForTwo());
            main_jsonObject.put("RestaurantCostForTwoMultiplier", zTab.getRestaurant().getCostForTwoMultiplier());
            main_jsonObject.put("RestaurantDeliveryActionText", zTab.getRestaurant().getDeliveryActionText());
            main_jsonObject.put("RestaurantDistance", zTab.getRestaurant().getDistance().toString());
            main_jsonObject.put("RestaurantName", zTab.getRestaurant().getName());
            main_jsonObject.put("RestaurantAddress", zTab.getRestaurant().getAddress());
            main_jsonObject.put("RestaurantFeaturedImage", zTab.getRestaurant().getFeaturedImage());
            main_jsonObject.put("RestaurantPhone", zTab.getRestaurant().getPhone());
            main_jsonObject.put("RestaurantMinOrder", zTab.getRestaurant().getMinOrder());
            main_jsonObject.put("RestaurantLatitude", zTab.getRestaurant().getLatitude());
            main_jsonObject.put("RestaurantLongitude", zTab.getRestaurant().getLongitude());
            main_jsonObject.put("RestaurantLocality", zTab.getRestaurant().getLocality());
            main_jsonObject.put("RestaurantAcceptanceRate", zTab.getRestaurant().getAcceptanceRate());

            main_jsonObject.put("OrderDetails", child_jsonArray_1);


            child_jsonArray_1.put(child_jsonObject_1);

            JSONArray child_jsonArray_2 = new JSONArray();
            JSONArray child_jsonArray_3 = new JSONArray();
            JSONArray child_jsonArray_4 = new JSONArray();
            JSONArray child_jsonArray_5 = new JSONArray();
            JSONArray child_jsonArray_6 = new JSONArray();
            JSONArray child_jsonArray_7 = new JSONArray();
            JSONArray child_jsonArray_8 = new JSONArray();
            JSONArray child_jsonArray_9 = new JSONArray();

            /*JSONObject child_jsonObject_3 = new JSONObject();
            JSONObject child_jsonObject_4 = new JSONObject();
            JSONObject child_jsonObject_5 = new JSONObject();
            JSONObject child_jsonObject_6 = new JSONObject();
            JSONObject child_jsonObject_7 = new JSONObject();
            JSONObject child_jsonObject_8 = new JSONObject();
            JSONObject child_jsonObject_9 = new JSONObject();
            JSONObject child_jsonObject_10 = new JSONObject();*/

            child_jsonObject_1.put("Charges", child_jsonArray_2);
            child_jsonObject_1.put("Dishes", child_jsonArray_3);
            child_jsonObject_1.put("Loyalty_discounts", child_jsonArray_4);
            child_jsonObject_1.put("Salt_discounts", child_jsonArray_5);
            child_jsonObject_1.put("Subtotal2", child_jsonArray_6);
            child_jsonObject_1.put("Total", child_jsonArray_7);
            child_jsonObject_1.put("Taxes", child_jsonArray_8);
            child_jsonObject_1.put("Voucher_discounts", child_jsonArray_9);


            for (int i = 0; i < zTab.getOrder().getCharges().size(); i++) {
                JSONObject jsonObject = new JSONObject();
                jsonObject.put("Display_cost", zTab.getOrder().getCharges().get(i).getDisplay_cost());
                jsonObject.put("Choice_text", zTab.getOrder().getCharges().get(i).getChoice_text());
                jsonObject.put("Item_id", zTab.getOrder().getCharges().get(i).getItem_id());
                jsonObject.put("Item_name", zTab.getOrder().getCharges().get(i).getItem_name());
                jsonObject.put("Old_total_cost", zTab.getOrder().getCharges().get(i).getOld_total_cost());
                jsonObject.put("Quantity", zTab.getOrder().getCharges().get(i).getQuantity());
                jsonObject.put("Tag_ids", zTab.getOrder().getCharges().get(i).getTag_ids());
                jsonObject.put("Total_cost", zTab.getOrder().getCharges().get(i).getTotal_cost());
                jsonObject.put("Type", zTab.getOrder().getCharges().get(i).getType());
                jsonObject.put("Unit_cost", zTab.getOrder().getCharges().get(i).getUnit_cost());
                jsonObject.put("Tax_id", zTab.getOrder().getCharges().get(i).getTax_id());
                child_jsonArray_2.put(jsonObject);
            }

            //zTab.getOrder().getDishes().
            for (int i = 0; i < zTab.getOrder().getDishes().size(); i++) {
                JSONObject jsonObject = new JSONObject();
                jsonObject.put("Display_cost", zTab.getOrder().getDishes().get(i).getDisplay_cost());
                jsonObject.put("Choice_text", zTab.getOrder().getDishes().get(i).getChoice_text());
                jsonObject.put("Item_id", zTab.getOrder().getDishes().get(i).getItem_id());
                jsonObject.put("Item_name", zTab.getOrder().getDishes().get(i).getItem_name());
                jsonObject.put("Old_total_cost", zTab.getOrder().getDishes().get(i).getOld_total_cost());
                jsonObject.put("Quantity", zTab.getOrder().getDishes().get(i).getQuantity());
                jsonObject.put("Tag_ids", zTab.getOrder().getDishes().get(i).getTag_ids());
                jsonObject.put("Total_cost", zTab.getOrder().getDishes().get(i).getTotal_cost());
                jsonObject.put("Type", zTab.getOrder().getDishes().get(i).getType());
                jsonObject.put("Unit_cost", zTab.getOrder().getDishes().get(i).getUnit_cost());
                jsonObject.put("Tax_id", zTab.getOrder().getDishes().get(i).getTax_id());
                child_jsonArray_3.put(jsonObject);
            }


          /*  for (int i = 0; i < zTab.getOrder().getDishes().size(); i++) {
                Log.e("Display_cost" + "" + i, "" + zTab.getOrder().getDishes().get(i).getDisplay_cost());
                Log.e("Choice_text" + "" + i, "" + zTab.getOrder().getDishes().get(i).getChoice_text());
                Log.e("Item_id" + "" + i, "" + zTab.getOrder().getDishes().get(i).getItem_id());
                Log.e("Item_name" + "" + i, "" + zTab.getOrder().getDishes().get(i).getItem_name());
                Log.e("Old_total_cost" + "" + i, "" + zTab.getOrder().getDishes().get(i).getOld_total_cost());
                Log.e("Quantity" + "" + i, "" + zTab.getOrder().getDishes().get(i).getQuantity());
                Log.e("Tag_ids" + "" + i, "" + zTab.getOrder().getDishes().get(i).getTag_ids());
                Log.e("Total_cost" + "" + i, "" + zTab.getOrder().getDishes().get(i).getTotal_cost());
                Log.e("Type" + "" + i, "" + zTab.getOrder().getDishes().get(i).getType());
                Log.e("Unit_cost" + "" + i, "" + zTab.getOrder().getDishes().get(i).getUnit_cost());
                Log.e("Tax_id" + "" + i, "" + zTab.getOrder().getDishes().get(i).getTax_id());
            }*/

            for (int i = 0; i < zTab.getOrder().getLoyalty_discounts().size(); i++) {
                JSONObject jsonObject = new JSONObject();
                jsonObject.put("Display_cost", zTab.getOrder().getLoyalty_discounts().get(i).getDisplay_cost());
                jsonObject.put("Choice_text", zTab.getOrder().getLoyalty_discounts().get(i).getChoice_text());
                jsonObject.put("Item_id", zTab.getOrder().getLoyalty_discounts().get(i).getItem_id());
                jsonObject.put("Item_name", zTab.getOrder().getLoyalty_discounts().get(i).getItem_name());
                jsonObject.put("Old_total_cost", zTab.getOrder().getLoyalty_discounts().get(i).getOld_total_cost());
                jsonObject.put("Quantity", zTab.getOrder().getLoyalty_discounts().get(i).getQuantity());
                jsonObject.put("Tag_ids", zTab.getOrder().getLoyalty_discounts().get(i).getTag_ids());
                jsonObject.put("Total_cost", zTab.getOrder().getLoyalty_discounts().get(i).getTotal_cost());
                jsonObject.put("Type", zTab.getOrder().getLoyalty_discounts().get(i).getType());
                jsonObject.put("Unit_cost", zTab.getOrder().getLoyalty_discounts().get(i).getUnit_cost());
                jsonObject.put("Tax_id", zTab.getOrder().getLoyalty_discounts().get(i).getTax_id());
                child_jsonArray_4.put(jsonObject);
            }

            for (int i = 0; i < zTab.getOrder().getSalt_discounts().size(); i++) {
                JSONObject jsonObject = new JSONObject();
                jsonObject.put("Display_cost", zTab.getOrder().getSalt_discounts().get(i).getDisplay_cost());
                jsonObject.put("Choice_text", zTab.getOrder().getSalt_discounts().get(i).getChoice_text());
                jsonObject.put("Item_id", zTab.getOrder().getSalt_discounts().get(i).getItem_id());
                jsonObject.put("Item_name", zTab.getOrder().getSalt_discounts().get(i).getItem_name());
                jsonObject.put("Old_total_cost", zTab.getOrder().getSalt_discounts().get(i).getOld_total_cost());
                jsonObject.put("Quantity", zTab.getOrder().getSalt_discounts().get(i).getQuantity());
                jsonObject.put("Tag_ids", zTab.getOrder().getSalt_discounts().get(i).getTag_ids());
                jsonObject.put("Total_cost", zTab.getOrder().getSalt_discounts().get(i).getTotal_cost());
                jsonObject.put("Type", zTab.getOrder().getSalt_discounts().get(i).getType());
                jsonObject.put("Unit_cost", zTab.getOrder().getSalt_discounts().get(i).getUnit_cost());
                jsonObject.put("Tax_id", zTab.getOrder().getSalt_discounts().get(i).getTax_id());
                child_jsonArray_5.put(jsonObject);
            }

            for (int i = 0; i < zTab.getOrder().getSubtotal2().size(); i++) {
                JSONObject jsonObject = new JSONObject();
                jsonObject.put("Display_cost", zTab.getOrder().getSubtotal2().get(i).getDisplay_cost());
                jsonObject.put("Choice_text", zTab.getOrder().getSubtotal2().get(i).getChoice_text());
                jsonObject.put("Item_id", zTab.getOrder().getSubtotal2().get(i).getItem_id());
                jsonObject.put("Item_name", zTab.getOrder().getSubtotal2().get(i).getItem_name());
                jsonObject.put("Old_total_cost", zTab.getOrder().getSubtotal2().get(i).getOld_total_cost());
                jsonObject.put("Quantity", zTab.getOrder().getSubtotal2().get(i).getQuantity());
                jsonObject.put("Tag_ids", zTab.getOrder().getSubtotal2().get(i).getTag_ids());
                jsonObject.put("Total_cost", zTab.getOrder().getSubtotal2().get(i).getTotal_cost());
                jsonObject.put("Type", zTab.getOrder().getSubtotal2().get(i).getType());
                jsonObject.put("Unit_cost", zTab.getOrder().getSubtotal2().get(i).getUnit_cost());
                jsonObject.put("Tax_id", zTab.getOrder().getSubtotal2().get(i).getTax_id());
                child_jsonArray_6.put(jsonObject);
            }

            for (int i = 0; i < zTab.getOrder().getTotal().size(); i++) {
                JSONObject jsonObject = new JSONObject();
                jsonObject.put("Display_cost", zTab.getOrder().getTotal().get(i).getDisplay_cost());
                jsonObject.put("Choice_text", zTab.getOrder().getTotal().get(i).getChoice_text());
                jsonObject.put("Item_id", zTab.getOrder().getTotal().get(i).getItem_id());
                jsonObject.put("Item_name", zTab.getOrder().getTotal().get(i).getItem_name());
                jsonObject.put("Old_total_cost", zTab.getOrder().getTotal().get(i).getOld_total_cost());
                jsonObject.put("Quantity", zTab.getOrder().getTotal().get(i).getQuantity());
                jsonObject.put("Tag_ids", zTab.getOrder().getTotal().get(i).getTag_ids());
                jsonObject.put("Total_cost", zTab.getOrder().getTotal().get(i).getTotal_cost());
                jsonObject.put("Type", zTab.getOrder().getTotal().get(i).getType());
                jsonObject.put("Unit_cost", zTab.getOrder().getTotal().get(i).getUnit_cost());
                jsonObject.put("Tax_id", zTab.getOrder().getTotal().get(i).getTax_id());
                child_jsonArray_7.put(jsonObject);
            }


            for (int i = 0; i < zTab.getOrder().getTaxes().size(); i++) {
                JSONObject jsonObject = new JSONObject();
                jsonObject.put("Display_cost", zTab.getOrder().getTaxes().get(i).getDisplay_cost());
                jsonObject.put("Choice_text", zTab.getOrder().getTaxes().get(i).getChoice_text());
                jsonObject.put("Item_id", zTab.getOrder().getTaxes().get(i).getItem_id());
                jsonObject.put("Item_name", zTab.getOrder().getTaxes().get(i).getItem_name());
                jsonObject.put("Old_total_cost", zTab.getOrder().getTaxes().get(i).getOld_total_cost());
                jsonObject.put("Quantity", zTab.getOrder().getTaxes().get(i).getQuantity());
                jsonObject.put("Tag_ids", zTab.getOrder().getTaxes().get(i).getTag_ids());
                jsonObject.put("Total_cost", zTab.getOrder().getTaxes().get(i).getTotal_cost());
                jsonObject.put("Type", zTab.getOrder().getTaxes().get(i).getType());
                jsonObject.put("Unit_cost", zTab.getOrder().getTaxes().get(i).getUnit_cost());
                jsonObject.put("Tax_id", zTab.getOrder().getTaxes().get(i).getTax_id());
                child_jsonArray_8.put(jsonObject);
            }

            for (int i = 0; i < zTab.getOrder().getVoucher_discounts().size(); i++) {
                JSONObject jsonObject = new JSONObject();
                jsonObject.put("Display_cost", zTab.getOrder().getVoucher_discounts().get(i).getDisplay_cost());
                jsonObject.put("Choice_text", zTab.getOrder().getVoucher_discounts().get(i).getChoice_text());
                jsonObject.put("Item_id", zTab.getOrder().getVoucher_discounts().get(i).getItem_id());
                jsonObject.put("Item_name", zTab.getOrder().getVoucher_discounts().get(i).getItem_name());
                jsonObject.put("Old_total_cost", zTab.getOrder().getVoucher_discounts().get(i).getOld_total_cost());
                jsonObject.put("Quantity", zTab.getOrder().getVoucher_discounts().get(i).getQuantity());
                jsonObject.put("Tag_ids", zTab.getOrder().getVoucher_discounts().get(i).getTag_ids());
                jsonObject.put("Total_cost", zTab.getOrder().getVoucher_discounts().get(i).getTotal_cost());
                jsonObject.put("Type", zTab.getOrder().getVoucher_discounts().get(i).getType());
                jsonObject.put("Unit_cost", zTab.getOrder().getVoucher_discounts().get(i).getUnit_cost());
                jsonObject.put("Tax_id", zTab.getOrder().getVoucher_discounts().get(i).getTax_id());
                child_jsonArray_9.put(jsonObject);
            }


            postZomatoOrder(main_jsonObject);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void zomatoTrackEvent(String s, String s1, HashMap<String, Object> hashMap) {

    }

    @Override
    public void zomatoOrderingFlowDismissed(boolean b) {

    }

    @Override
    public void zomatoUserToken(String s) {
        mLockatedPreferences.setZomatoToken(s);
        updateZomatoOrder(s);
    }

    @Override
    public void zomatoActionButtonClicked(String s) {
        if (s == "My Orders") {
            Intent intent = new Intent(HomeActivity.this, ZomatoOrderItemListActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
            //OrderSDK.getInstance().openOrderDetailsPage(context,"3993669");
        }
    }

    public void postZomatoOrder(JSONObject jsonObject) {
        try {
            if (ConnectionDetector.isConnectedToInternet(this)) {
                String url = ApplicationURL.postSingleZomatoTransaction + mLockatedPreferences.getLockatedToken();
                LockatedVolleyRequestQueue lockatedVolleyRequestQueue = LockatedVolleyRequestQueue.getInstance(HomeActivity.this);
                lockatedVolleyRequestQueue.sendZomatoRequest(GET_REQUEST_POST_TAG, Request.Method.POST, url, jsonObject, this, this);
            } else {
                Utilities.showToastMessage(this, this.getResources().getString(R.string.internet_connection_error));
            }
        } catch (Exception e) {
            Log.e("Exception", e.getMessage());
        }
    }

    public void updateZomatoOrder(String s) {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("user", new JSONObject().put("zomatotoken", s));
            if (ConnectionDetector.isConnectedToInternet(this)) {
                String url = ApplicationURL.updatePersonalInformatio + mLockatedPreferences.getLockatedToken();

                /*Log.e("url", url);*/
                LockatedVolleyRequestQueue lockatedVolleyRequestQueue = LockatedVolleyRequestQueue.getInstance(HomeActivity.this);
                lockatedVolleyRequestQueue.sendZomatoRequest(POST_ZOMATO_TOKEN, Request.Method.PUT, url, jsonObject, this, this);
            } else {
                Utilities.showToastMessage(this, this.getResources().getString(R.string.internet_connection_error));
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
    ////////////
}
