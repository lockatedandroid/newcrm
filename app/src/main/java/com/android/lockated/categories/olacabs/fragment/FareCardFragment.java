package com.android.lockated.categories.olacabs.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.lockated.R;

import org.json.JSONObject;

/**
 * Created by HAVEN INFOLINE on 7/9/2016.
 */
public class FareCardFragment  extends DialogFragment {

    String fare_type,fare_minimum_distance,fare_minimum_time,fare_base_fare,fare_minimum_fare,fare_cost_per_distance,fare_waiting_cost_per_minute,fare_ride_cost_per_minute,fareObject;
    int base_fare,minimum_fare,cost_per_distance,waiting_cost_per_minute,ride_cost_per_minute;
    private Window window;
    Float Surcharge;
    JSONObject fareJsonObejct;
    LinearLayout min_distance_layout,txt_surcharge_layout;
    TextView  txt_base_fare,txt_distance, txt_minutes,txt_surcharge;
    View fare_divider;


    /*@Override
    public void onStart()
    {
        super.onStart();
        window = getDialog().getWindow();
        WindowManager.LayoutParams windowParams = window.getAttributes();
        windowParams.dimAmount = 0.75f;
        windowParams.flags |= WindowManager.LayoutParams.FLAG_DIM_BEHIND;
        window.setAttributes(windowParams);
        window.setBackgroundDrawableResource(android.R.color.white);

    }
*/
    @Override
    public void onStart() {
        super.onStart();
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(getDialog().getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.WRAP_CONTENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        getDialog().getWindow().setAttributes(lp);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(STYLE_NO_TITLE, 0);
    }

    /*@Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
    }*/


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.ratecard_popup, container);
        init(view);
        return view;
    }

    public void init(View view)
    {
        min_distance_layout = (LinearLayout)view.findViewById(R.id.min_distance_layout);
        txt_surcharge_layout = (LinearLayout)view.findViewById(R.id.txt_surcharge_layout);
        txt_base_fare = (TextView) view.findViewById(R.id.txt_base_fare);
        txt_distance = (TextView) view.findViewById(R.id.txt_distance);
        txt_minutes = (TextView) view.findViewById(R.id.txt_minutes);
        txt_surcharge =(TextView) view.findViewById(R.id.txt_surcharge);
        fare_divider = (View)view.findViewById(R.id.fare_divider);
        Bundle bundle = getArguments();

        if (bundle != null) {
            fareObject = bundle.getString("FareObject");
            Surcharge = bundle.getFloat("Surcharge");
            //Log.e("Fare Object :",fareObject);
        }

        try {
            fareJsonObejct = new JSONObject(fareObject);

            //Log.e("fareJsonObejct",fareJsonObejct.toString());
            fare_type = fareJsonObejct.getString("type");
            fare_minimum_distance = fareJsonObejct.getString("minimum_distance");
            fare_minimum_time = fareJsonObejct.getString("minimum_time");
            fare_base_fare = fareJsonObejct.getString("base_fare");
            fare_minimum_fare = fareJsonObejct.getString("minimum_fare");
            fare_cost_per_distance = fareJsonObejct.getString("cost_per_distance");
            fare_waiting_cost_per_minute = fareJsonObejct.getString("waiting_cost_per_minute");
            fare_ride_cost_per_minute = fareJsonObejct.getString("ride_cost_per_minute");

        } catch (Exception e) {
            e.getStackTrace();
        }

        //Log.e("fare_base_fare",fare_base_fare);
        //Log.e("fare_cost_per_distance",""+fare_cost_per_distance);
        //Log.e("fare_ride_cost_per_minute",""+fare_ride_cost_per_minute);

        try {
            base_fare = Integer.parseInt(fare_base_fare);
        } catch (NumberFormatException e) {
            double base_fare_new = Double.parseDouble(fare_base_fare);
            base_fare = (int) base_fare_new;
        }

        try {
            cost_per_distance = Integer.parseInt(fare_cost_per_distance);
        } catch (NumberFormatException e) {
            double cost_per_distance_new = Double.parseDouble(fare_cost_per_distance);
            cost_per_distance = (int) cost_per_distance_new;
        }

        try {
            ride_cost_per_minute = Integer.parseInt(fare_ride_cost_per_minute);
        } catch (NumberFormatException e) {
            double ride_cost_per_minute_new = Double.parseDouble(fare_ride_cost_per_minute);
            ride_cost_per_minute = (int) ride_cost_per_minute_new;
        }

        txt_base_fare.setText(""+base_fare);

        if (fare_cost_per_distance == null )
        {
            if (min_distance_layout.getVisibility()==View.VISIBLE)
            {
                min_distance_layout.setVisibility(View.GONE);
            }
            else
            {
                min_distance_layout.setVisibility(View.GONE);
            }
        }
        else
        {
            min_distance_layout.setVisibility(View.VISIBLE);
        }
        txt_distance.setText(""+cost_per_distance +"/km");
        txt_minutes.setText(""+ride_cost_per_minute +"/min");
        if (Surcharge != 0)
        {
            if (fare_divider.getVisibility()==View.GONE)
            {
                fare_divider.setVisibility(View.VISIBLE);
            }
            if (txt_surcharge_layout.getVisibility() == View.GONE)
            {
                txt_surcharge_layout.setVisibility(View.VISIBLE);
            }
            txt_surcharge.setText("These charges does not include "+Surcharge+"x surge");
        }
        else
        {
            if (fare_divider.getVisibility()==View.VISIBLE)
            {
                fare_divider.setVisibility(View.GONE);
            }
            if (txt_surcharge_layout.getVisibility() == View.VISIBLE)
            {
                txt_surcharge_layout.setVisibility(View.GONE);
            }
        }
    }
}
    /*@NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        View connectToView = getActivity().getLayoutInflater().inflate(R.layout.ratecard_popup, new LinearLayout(getActivity()), false);

        txt_base_fare = (TextView) connectToView.findViewById(R.id.txt_base_fare);
        txt_distance = (TextView) connectToView.findViewById(R.id.txt_distance);
        txt_minutes = (TextView) connectToView.findViewById(R.id.txt_minutes);
        txt_surcharge =(TextView) connectToView.findViewById(R.id.txt_surcharge);

        Bundle bundle = getArguments();

        if (bundle != null) {
            fareObject = bundle.getString("FareObject");
            Surcharge = bundle.getFloat("Surcharge");
            Log.e("Fare Object :",fareObject);
        }

        try {
            fareJsonObejct = new JSONObject(fareObject);

            Log.e("fareJsonObejct",fareJsonObejct.toString());
            fare_type = fareJsonObejct.getString("type");
            fare_minimum_distance = fareJsonObejct.getString("minimum_distance");
            fare_minimum_time = fareJsonObejct.getString("minimum_time");
            fare_base_fare = fareJsonObejct.getString("base_fare");
            fare_minimum_fare = fareJsonObejct.getString("minimum_fare");
            fare_cost_per_distance = fareJsonObejct.getString("cost_per_distance");
            fare_waiting_cost_per_minute = fareJsonObejct.getString("waiting_cost_per_minute");
            fare_ride_cost_per_minute = fareJsonObejct.getString("ride_cost_per_minute");


        } catch (Exception e) {
            e.getStackTrace();
        }

        Log.e("fare_base_fare",fare_base_fare);
        Log.e("fare_cost_per_distance",fare_cost_per_distance);
        Log.e("fare_ride_cost_per_minute",fare_ride_cost_per_minute);

        txt_base_fare.setText(fare_base_fare);
        txt_distance.setText(fare_cost_per_distance + "/km");
        txt_minutes.setText(fare_ride_cost_per_minute + "/min");
        if (Surcharge != 0)
        {
            txt_surcharge.setText("These rates will reflect by"+Surcharge+"x surge");
        }

        Dialog builder = new Dialog(getActivity());
        builder.requestWindowFeature(Window.FEATURE_NO_TITLE);
        builder.getWindow().setBackgroundDrawable(new ColorDrawable(Color.WHITE));

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(builder.getWindow().getAttributes());

        lp.width = WindowManager.LayoutParams.WRAP_CONTENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;

       *//* Point size = new Point();

        // Store dimensions of the screen in `size`
        Display display = getActivity().getWindowManager().getDefaultDisplay();
        display.getSize(size);

        // Set the width of the dialog proportional to 75% of the screen width
        //lp.width = 300;
        //lp.height = 300;

        lp.width = (int) (size.x * 0.7);
        lp.height = (int) (size.y * 0.4);*//*


        builder.getWindow().setAttributes(lp);
        builder.setContentView(connectToView);
        builder.show();
        return builder;
    }
}*/

