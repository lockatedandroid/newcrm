package com.android.lockated.Interfaces;

public interface IServiceViewListener
{
    void onServiceRowBrand(String brand, int position);
    void onServiceRowModel(String model, int position);
}
