package com.android.lockated.pushnotification;

import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;

import com.android.lockated.R;
import com.android.lockated.utils.Utilities;

public class UpdateAppActivity extends AppCompatActivity {

    boolean forceCloseApp;
    String message;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_update_app);

        WindowManager.LayoutParams windowManager = getWindow().getAttributes();
        windowManager.dimAmount = 0.75f;
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);

        if (getIntent().getExtras() != null) {
            forceCloseApp = getIntent().getExtras().getBoolean("force_close");
            message = getIntent().getExtras().getString("message");
        }

        showAlertDialog(this, forceCloseApp, message);
    }

    public void showAlertDialog(Context context, final boolean forceCloseApp, String message) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setMessage(message);
        builder.setCancelable(false);
        builder.setPositiveButton(R.string.update_app, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.dismiss();
                onUpdateAppClicked();
                finish();
            }
        });

        builder.setNegativeButton(R.string.not_now, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                if (forceCloseApp) {
                    Intent homeIntent = new Intent(Intent.ACTION_MAIN);
                    homeIntent.addCategory(Intent.CATEGORY_HOME);
                    homeIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(homeIntent);
                } else {
                    dialog.dismiss();
                    UpdateAppActivity.this.finish();
                }
            }
        });
        AlertDialog dialog = builder.create();
        builder.setIcon(R.drawable.lockated_logo);
        dialog.show();
    }

    private void onUpdateAppClicked() {
        Uri uri = Uri.parse("market://details?id=" + getPackageName());
        Intent myAppLinkToMarket = new Intent(Intent.ACTION_VIEW, uri);
        try {
            startActivity(myAppLinkToMarket);
        } catch (ActivityNotFoundException e) {
            Utilities.showToastMessage(this, "Unable to find application on Play store.");
        }
    }

}
