package com.android.lockated.categories.grocery.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.android.lockated.R;
import com.android.lockated.categories.grocery.adapter.CategoryDetailTabAdapter;
import com.android.lockated.component.LockatedPagerSlidingTabStrip;
import com.android.lockated.landing.model.TaxonData;
import com.android.lockated.utils.Utilities;

import java.util.ArrayList;

public class CategoryProductViewFragment extends Fragment implements ViewPager.OnPageChangeListener {
    private View mCategoryView;
    private ViewPager mViewPagerDetail;
    private LockatedPagerSlidingTabStrip mViewPagerSlidingTabs;
    private CategoryDetailTabAdapter mCategoryDetailTabAdapter;

    private int mPosition;

    private ArrayList<TaxonData> mTaxonDataList;

    public static OnPagerPageChangeListener mOnPagerPageChangeListener;

    public interface OnPagerPageChangeListener {
        void onPageChanged(int position);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mCategoryView = inflater.inflate(R.layout.fragment_account, container, false);
        init();
        Utilities.ladooIntegration(getActivity(), "Category Product View Screen");
        Utilities.lockatedGoogleAnalytics(getString(R.string.category_product_view_screen),
                getString(R.string.visited), getString(R.string.category_product_view_screen));
        return mCategoryView;
    }

    public void setTaxonData(ArrayList<TaxonData> mTaxonData, int pos, OnPagerPageChangeListener mPageChangeListener) {
        mTaxonDataList = mTaxonData;
        mPosition = pos;
        mOnPagerPageChangeListener = mPageChangeListener;
    }

    private void init() {
        mViewPagerDetail = (ViewPager) mCategoryView.findViewById(R.id.mViewPagerDetail);
        mViewPagerSlidingTabs = (LockatedPagerSlidingTabStrip) mCategoryView.findViewById(R.id.mViewPagerSlidingTabs);

        mViewPagerSlidingTabs.setIndicatorColor(ContextCompat.getColor(getActivity(), R.color.primary));
        mViewPagerSlidingTabs.setTextColor(ContextCompat.getColor(getActivity(), R.color.primary_text));
        mViewPagerSlidingTabs.setActivateTextColor(ContextCompat.getColor(getActivity(), R.color.primary_text));
        mViewPagerSlidingTabs.setDeactivateTextColor(ContextCompat.getColor(getActivity(), R.color.primary_text));

        mViewPagerDetail.addOnPageChangeListener(this);

        if (mTaxonDataList != null && mTaxonDataList.size() > 0) {
            mCategoryDetailTabAdapter = new CategoryDetailTabAdapter(getChildFragmentManager(), mTaxonDataList);
            mViewPagerDetail.setAdapter(mCategoryDetailTabAdapter);
            mViewPagerDetail.setCurrentItem(mPosition);
            mViewPagerSlidingTabs.setViewPager(mViewPagerDetail);
        }
    }


    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
        mOnPagerPageChangeListener.onPageChanged(position);
    }

    @Override
    public void onPageSelected(int position) {
        mOnPagerPageChangeListener.onPageChanged(position);
    }

    @Override
    public void onPageScrollStateChanged(int state) {
    }
}