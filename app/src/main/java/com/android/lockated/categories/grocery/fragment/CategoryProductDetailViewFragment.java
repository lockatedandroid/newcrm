package com.android.lockated.categories.grocery.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.android.lockated.Interfaces.IRecyclerItemClickListener;
import com.android.lockated.R;
import com.android.lockated.cart.CartController;
import com.android.lockated.categories.grocery.GroceryActivity;
import com.android.lockated.categories.grocery.adapter.CategoryItemDetailAdapter;
import com.android.lockated.categories.grocery.model.GroceryItemData;
import com.android.lockated.network.ConnectionDetector;
import com.android.lockated.preferences.LockatedPreferences;
import com.android.lockated.request.LockatedJSONObjectRequest;
import com.android.lockated.request.LockatedRequestError;
import com.android.lockated.request.LockatedVolleyRequestQueue;
import com.android.lockated.utils.ApplicationURL;
import com.android.lockated.utils.RecyclerScrollChangeListener;
import com.android.lockated.utils.Utilities;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class CategoryProductDetailViewFragment extends Fragment implements IRecyclerItemClickListener, Response.Listener, Response.ErrorListener {
    private View mCategoryView;
    private ProgressBar mProgressBarView;
    private boolean descripsion_flag;

    private int mProductId;
    private int mCurrentPage = 1;

    private CartController mCartController;
    private ArrayList<GroceryItemData> mGroceryItemDataList;

    LockatedJSONObjectRequest mLockatedJSONObjectRequest;

    private LockatedPreferences mLockatedPreferences;
    private CategoryItemDetailAdapter mCategoryItemDetailAdapter;

    public static final String REQUEST_TAG = "CategoryProductDetailViewFragment";
    public static final String ADD_PRODUCT_TAG = "AddProductToCart";
    RecyclerView mRecyclerView;
    TextView noNotices;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mCategoryView = inflater.inflate(R.layout.fragment_category_detail_item, container, false);
        init();
        /*getProductDetails(mCurrentPage);*/
        return mCategoryView;
    }

    @Override
    public void onResume() {
        super.onResume();
        mCurrentPage = 1;
        getProductDetails(mCurrentPage);
    }

    public void setProductId(int id) {
        mProductId = id;
    }

    private void init() {
        mGroceryItemDataList = new ArrayList<>();
        mCartController = CartController.getInstance();
        mLockatedPreferences = new LockatedPreferences(getActivity());
        noNotices = (TextView) mCategoryView.findViewById(R.id.noNotices);
        mProgressBarView = (ProgressBar) mCategoryView.findViewById(R.id.mProgressBarView);
        mRecyclerView = (RecyclerView) mCategoryView.findViewById(R.id.mRecyclerView);

        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        mRecyclerView.setLayoutManager(mLayoutManager);
        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.addOnScrollListener(new RecyclerScrollChangeListener() {
            @Override
            public void onLoadMore(int page, int totalItemsCount) {
                mCurrentPage = page;
                getProductDetails(mCurrentPage);
            }
        });
        mCategoryItemDetailAdapter = new CategoryItemDetailAdapter(mGroceryItemDataList, this);
        mRecyclerView.setAdapter(mCategoryItemDetailAdapter);
    }

    private void getProductDetails(int pageNumber) {
        if (ConnectionDetector.isConnectedToInternet(getActivity())) {
            mProgressBarView.setVisibility(View.VISIBLE);
            String url = ApplicationURL.getProductDetailUrl + mProductId + "&token=" + mLockatedPreferences.getLockatedToken() + "&page=" + pageNumber + "&per_page=25";
            //Log.e("getProductDetails url",url);
            LockatedVolleyRequestQueue lockatedVolleyRequestQueue = LockatedVolleyRequestQueue.getInstance(getActivity());
            lockatedVolleyRequestQueue.sendRequest(REQUEST_TAG, Request.Method.GET, url, null, this, this);
        } else {
            Utilities.showToastMessage(getActivity(), getActivity().getResources().getString(R.string.internet_connection_error));
        }
    }

    private void onQuantityAddClicked(GroceryItemData groceryItemData) {
        groceryItemData.setQuantity(groceryItemData.getQuantity() + 1);
        ((GroceryActivity) getActivity()).productAddToCart(groceryItemData);
        mCategoryItemDetailAdapter.notifyDataSetChanged();
    }

    private void onQuantityRemoveClicked(GroceryItemData groceryItemData) {
        groceryItemData.setQuantity(groceryItemData.getQuantity() - 1);
        ((GroceryActivity) getActivity()).productAddToCart(groceryItemData);
        mCategoryItemDetailAdapter.notifyDataSetChanged();
    }

    private void onProductDescriptionClicked(GroceryItemData groceryItemData)  {
        Log.e("onProductDescriptionClicked clicked",""+true);
        LayoutInflater vi = (LayoutInflater)getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View itemView = vi.inflate(R.layout.fragment_category_detail_item_row,null);


        TextView mTextProductDescription = (TextView) itemView.findViewById(R.id.mTextProductDescription);
        LinearLayout mLayoutProductDescription =(LinearLayout) itemView.findViewById(R.id.mLayoutProductDescription);
        ImageView mImageProductDescription =(ImageView) itemView.findViewById(R.id.mImageProductDescription);
        /*if (mLayoutProductDescription.getVisibility()==View.VISIBLE) {
                if (mTextProductDescription.getVisibility()==View.VISIBLE)
                {
                    Log.e("mTextProductDescription 1 ","true");
                    mImageProductDescription.setImageResource(R.drawable.new_ic_up);
                    mTextProductDescription.setVisibility(View.GONE);
                }
                else
                {
                    Log.e("mTextProductDescription 2","true");
                    mImageProductDescription.setImageResource(R.drawable.new_ic_down);
                    mTextProductDescription.setVisibility(View.VISIBLE);
                }
        }
*/
        if(descripsion_flag)
        {
            Log.e("descripsion_flag","true");
            mTextProductDescription.setVisibility(View.GONE);
            mImageProductDescription.setImageResource(R.drawable.new_ic_down);
            descripsion_flag = false;
        }
        else
        {
            Log.e("descripsion_flag","false");
            mTextProductDescription.setVisibility(View.VISIBLE);
            mImageProductDescription.setImageResource(R.drawable.new_ic_up);
            descripsion_flag = true;
        }
    }
    @Override
    public void onErrorResponse(VolleyError error) {
        if (getActivity() != null) {
            mProgressBarView.setVisibility(View.INVISIBLE);
            LockatedRequestError.onRequestError(getActivity(), error);
        }
    }

    @Override
    public void onResponse(Object response) {
        if (getActivity() != null) {
            mProgressBarView.setVisibility(View.INVISIBLE);
            JSONObject jsonObject = (JSONObject) response;
            //Log.e("Response",jsonObject.toString());
            try {
                JSONArray jsonArray = jsonObject.getJSONArray("products");
                if (jsonArray.length() > 0) {
                    for (int i = 0; i < jsonArray.length(); i++) {
                        JSONObject productsObject = jsonArray.getJSONObject(i);
                        GroceryItemData groceryItemData = new GroceryItemData(productsObject);
                        mGroceryItemDataList.add(mGroceryItemDataList.size(), groceryItemData);
                        /*mCategoryItemDetailAdapter = new CategoryItemDetailAdapter(mGroceryItemDataList, this);
                        mRecyclerView.setAdapter(mCategoryItemDetailAdapter);*/
                        //Log.e("mGroceryItemDataList",""+mGroceryItemDataList);
                    }
                }/* else {
                    Log.e("mGroceryItemDataList.size()", "" + mGroceryItemDataList.size());
                    if (jsonArray.length() == 0 && mGroceryItemDataList.size() == 0) {

                        //}else if (mGroceryItemDataList.size() == 0) {
                        noNotices.setVisibility(View.VISIBLE);
                        noNotices.setText("No Products to Display");
                    }
                }*/
                mCategoryItemDetailAdapter.notifyDataSetChanged();
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void onRecyclerItemClick(View view, int position) {
        GroceryItemData groceryItemData = mGroceryItemDataList.get(position);
        //Log.e("GroceryItemData",""+groceryItemData);
        switch (view.getId()) {
            case R.id.mImageViewProductQuantityAdd:
                onQuantityAddClicked(groceryItemData);
                break;
            case R.id.mImageViewProductQuantityRemove:
                onQuantityRemoveClicked(groceryItemData);
                break;
            case R.id.mImageProductDescription:
                onProductDescriptionClicked(groceryItemData);
                break;
        }
    }

}
