
package com.android.lockated.model.OlaCab.OlaAutoCompleteResponse;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class OlaAutoCompleteResponse {

    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("request_type")
    @Expose
    private String requestType;
    @SerializedName("booking_id")
    @Expose
    private String bookingId;
    @SerializedName("pickup_time")
    @Expose
    private String pickupTime;
    @SerializedName("ola_money_balance")
    @Expose
    private int olaMoneyBalance;
    @SerializedName("trip_info")
    @Expose
    private TripInfo tripInfo;
    @SerializedName("booking_status")
    @Expose
    private String bookingStatus;

    /**
     * 
     * @return
     *     The status
     */
    public String getStatus() {
        return status;
    }

    /**
     * 
     * @param status
     *     The status
     */
    public void setStatus(String status) {
        this.status = status;
    }

    /**
     * 
     * @return
     *     The requestType
     */
    public String getRequestType() {
        return requestType;
    }

    /**
     * 
     * @param requestType
     *     The request_type
     */
    public void setRequestType(String requestType) {
        this.requestType = requestType;
    }

    /**
     * 
     * @return
     *     The bookingId
     */
    public String getBookingId() {
        return bookingId;
    }

    /**
     * 
     * @param bookingId
     *     The booking_id
     */
    public void setBookingId(String bookingId) {
        this.bookingId = bookingId;
    }

    /**
     * 
     * @return
     *     The pickupTime
     */
    public String getPickupTime() {
        return pickupTime;
    }

    /**
     * 
     * @param pickupTime
     *     The pickup_time
     */
    public void setPickupTime(String pickupTime) {
        this.pickupTime = pickupTime;
    }

    /**
     * 
     * @return
     *     The olaMoneyBalance
     */
    public int getOlaMoneyBalance() {
        return olaMoneyBalance;
    }

    /**
     * 
     * @param olaMoneyBalance
     *     The ola_money_balance
     */
    public void setOlaMoneyBalance(int olaMoneyBalance) {
        this.olaMoneyBalance = olaMoneyBalance;
    }

    /**
     * 
     * @return
     *     The tripInfo
     */
    public TripInfo getTripInfo() {
        return tripInfo;
    }

    /**
     * 
     * @param tripInfo
     *     The trip_info
     */
    public void setTripInfo(TripInfo tripInfo) {
        this.tripInfo = tripInfo;
    }

    /**
     * 
     * @return
     *     The bookingStatus
     */
    public String getBookingStatus() {
        return bookingStatus;
    }

    /**
     * 
     * @param bookingStatus
     *     The booking_status
     */
    public void setBookingStatus(String bookingStatus) {
        this.bookingStatus = bookingStatus;
    }

}
