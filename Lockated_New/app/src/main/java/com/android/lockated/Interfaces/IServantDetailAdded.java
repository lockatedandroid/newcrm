package com.android.lockated.Interfaces;

import com.android.lockated.model.ServantData;

import java.util.ArrayList;

public interface IServantDetailAdded
{
    public void onNewServantAdded(ArrayList<ServantData> servantDataList, int requestId);
    public void onNewServantUpdated(ServantData servantData, int requestId, int position);
}
