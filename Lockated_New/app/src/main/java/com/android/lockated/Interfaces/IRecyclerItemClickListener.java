package com.android.lockated.Interfaces;

import android.view.View;

public interface IRecyclerItemClickListener
{
    void onRecyclerItemClick(View view, int position);
}
