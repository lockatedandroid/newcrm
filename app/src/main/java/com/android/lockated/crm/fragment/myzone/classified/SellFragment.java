package com.android.lockated.crm.fragment.myzone.classified;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.TextView;

import com.android.lockated.LockatedApplication;
import com.android.lockated.R;
import com.android.lockated.crm.activity.PostClassifiedDetailsActivity;
import com.android.lockated.crm.adapters.SellAdapter;
import com.android.lockated.information.AccountController;
import com.android.lockated.model.AccountData;
import com.android.lockated.model.usermodel.Classified.AllClassified.AllClassifiedList;
import com.android.lockated.network.ConnectionDetector;
import com.android.lockated.preferences.LockatedPreferences;
import com.android.lockated.request.LockatedJSONObjectRequest;
import com.android.lockated.request.LockatedRequestError;
import com.android.lockated.request.LockatedVolleyRequestQueue;
import com.android.lockated.utils.ApplicationURL;
import com.android.lockated.utils.Utilities;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class SellFragment extends Fragment implements Response.ErrorListener, Response.Listener<JSONObject>, View.OnClickListener {
    FloatingActionButton fab;
    ListView SellList;
    int societyid;
    TextView errorMsg;
    SellAdapter sellAdapter;
    AccountController accountController;
    ArrayList<AccountData> accountDataArrayList;
    ArrayList<AllClassifiedList> sellClassifieds;
    private RequestQueue mQueue;
    private LockatedPreferences mLockatedPreferences;
    public static final String REQUEST_TAG = "SellFragment";
    String SHOW, CREATE, INDEX, UPDATE, EDIT;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View SellView = inflater.inflate(R.layout.fragment_sell, container, false);
        init(SellView);
        return SellView;
    }

    @Override
    public void onResume() {
        super.onResume();
        LockatedApplication.getInstance().trackScreenView(getString(R.string.sell));
        Utilities.lockatedGoogleAnalytics(getString(R.string.sell),
                getString(R.string.visited), getString(R.string.sell));
       /* sellClassifieds.clear();
        getClassifiedList();*/
    }

    private void init(View SellView) {
        sellClassifieds = new ArrayList<>();
        mLockatedPreferences = new LockatedPreferences(getActivity());
       /* accountController = AccountController.getInstance();
        accountDataArrayList = accountController.getmAccountDataList();*/
        societyid = Integer.parseInt(mLockatedPreferences.getSocietyId());
        errorMsg = (TextView) SellView.findViewById(R.id.noNotices);
        SellList = (ListView) SellView.findViewById(R.id.listView);
        sellAdapter = new SellAdapter(getActivity(), sellClassifieds);
        SellList.setAdapter(sellAdapter);
        fab = (FloatingActionButton) SellView.findViewById(R.id.fab);
        fab.setOnClickListener(this);
        getClasssifiedRole();
        checkPermission();
    }

    private void checkPermission() {
        if (CREATE!=null&&CREATE.equals("true")) {
            fab.setVisibility(View.VISIBLE);
        } else {
            fab.setVisibility(View.GONE);
        }
        if (SHOW!=null&&SHOW.equals("true")) {
            getClassifiedList();
        } else {
            SellList.setVisibility(View.GONE);
            errorMsg.setVisibility(View.VISIBLE);
            errorMsg.setText(R.string.no_permission_error);
        }
    }

    private void getClassifiedList() {
        if (getActivity() != null) {
            if (ConnectionDetector.isConnectedToInternet(getActivity())) {
                String url = ApplicationURL.getuserClassified + mLockatedPreferences.getLockatedToken()
                        + ApplicationURL.societyId + societyid;
                mQueue = LockatedVolleyRequestQueue.getInstance(getActivity()).getRequestQueue();
                LockatedJSONObjectRequest lockatedJSONObjectRequest = new LockatedJSONObjectRequest(Request.Method.GET,
                        url, null, this, this);
                lockatedJSONObjectRequest.setTag(REQUEST_TAG);
                mQueue.add(lockatedJSONObjectRequest);
            } else {
                Utilities.showToastMessage(getActivity(), getActivity().getResources().getString(R.string.internet_connection_error));
            }
        }
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.fab:
                Intent postClassifiedIntent = new Intent(getActivity(), PostClassifiedDetailsActivity.class);
                startActivity(postClassifiedIntent);
                break;
        }
    }

    @Override
    public void onErrorResponse(VolleyError error) {
        if (getActivity() != null) {
            LockatedRequestError.onRequestError(getActivity(), error);
        }
    }

    @Override
    public void onResponse(JSONObject response) {
        if (getActivity() != null) {
            try {
                if (response != null && response.length() > 0) {
                    if (response.getJSONArray("classifieds").length() > 0) {
                        JSONArray jsonArray = response.getJSONArray("classifieds");
                        Gson gson = new Gson();
                        for (int i = 0; i < jsonArray.length(); i++) {
                            AllClassifiedList classified = gson.fromJson(jsonArray.getJSONObject(i).toString(), AllClassifiedList.class);
                            sellClassifieds.add(classified);
                        }
                        sellAdapter.notifyDataSetChanged();
                    } else {
                        SellList.setVisibility(View.GONE);
                        errorMsg.setVisibility(View.VISIBLE);
                        errorMsg.setText(R.string.no_data_error);
                    }
                } else {
                    SellList.setVisibility(View.GONE);
                    errorMsg.setVisibility(View.VISIBLE);
                    errorMsg.setText(R.string.no_data_error);
                }

            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public String getClasssifiedRole() {
        String mySocietyRoles = getActivity().getResources().getString(R.string.blank_value);
        if (mLockatedPreferences.getRolesJson() != null && !mLockatedPreferences.getRolesJson().equals("blank")) {
            try {
                JSONObject noticeJsonObj = new JSONObject(mLockatedPreferences.getRolesJson());
                for (int i = 0; i < noticeJsonObj.getJSONArray(getActivity().getResources().getString(R.string.permissions_value)).length(); i++) {
                    JSONObject jsonObject1 = noticeJsonObj.getJSONArray(getActivity().getResources().getString(R.string.permissions_value)).getJSONObject(i);
                    if (jsonObject1.has(getActivity().getResources().getString(R.string.section_value))
                            && jsonObject1.getString(getActivity().getResources().getString(R.string.section_value)).equals("spree_classifieds")) {
                        if (jsonObject1.getJSONObject(getActivity().getResources().getString(R.string.permission_value)).has(getActivity().getResources().getString(R.string.index_value))) {
                            mySocietyRoles = jsonObject1.getJSONObject(getActivity().getResources().getString(R.string.permission_value)).getString(getActivity().getResources().getString(R.string.index_value));
                            CREATE = jsonObject1.getJSONObject(getActivity().getResources().getString(R.string.permission_value)).getString("create");
                            INDEX = jsonObject1.getJSONObject(getActivity().getResources().getString(R.string.permission_value)).getString("index");
                            UPDATE = jsonObject1.getJSONObject(getActivity().getResources().getString(R.string.permission_value)).getString("update");
                            EDIT = jsonObject1.getJSONObject(getActivity().getResources().getString(R.string.permission_value)).getString("edit");
                            SHOW = jsonObject1.getJSONObject(getActivity().getResources().getString(R.string.permission_value)).getString("show");
                        }
                    }
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return mySocietyRoles;
    }

}