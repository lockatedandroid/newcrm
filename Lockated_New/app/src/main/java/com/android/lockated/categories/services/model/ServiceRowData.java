package com.android.lockated.categories.services.model;

public class ServiceRowData
{
    private int id = -1;
    private int attributeId = -1;
    private int categoryId = 0;
    private int subCategoryId = 0;
    private String serviceBrand = "";
    private String serviceModel = "";
    private String serviceName = "Select Service";
    private String subServiceName = "Sub Service";
    private String serviceCharges = "Approximate charges will be showing here.";

    public int getId()
    {
        return id;
    }

    public void setId(int id)
    {
        this.id = id;
    }

    public int getAttributeId()
    {
        return attributeId;
    }

    public void setAttributeId(int attributeId)
    {
        this.attributeId = attributeId;
    }

    public int getCategoryId()
    {
        return categoryId;
    }

    public void setCategoryId(int categoryId)
    {
        this.categoryId = categoryId;
    }

    public int getSubCategoryId()
    {
        return subCategoryId;
    }

    public void setSubCategoryId(int subCategoryId)
    {
        this.subCategoryId = subCategoryId;
    }

    public String getServiceBrand()
    {
        return serviceBrand;
    }

    public void setServiceBrand(String serviceBrand)
    {
        this.serviceBrand = serviceBrand;
    }

    public String getServiceModel()
    {
        return serviceModel;
    }

    public void setServiceModel(String serviceModel)
    {
        this.serviceModel = serviceModel;
    }

    public String getServiceName()
    {
        return serviceName;
    }

    public void setServiceName(String serviceName)
    {
        this.serviceName = serviceName;
    }

    public String getSubServiceName()
    {
        return subServiceName;
    }

    public void setSubServiceName(String subServiceName)
    {
        this.subServiceName = subServiceName;
    }

    public String getServiceCharges()
    {
        return serviceCharges;
    }

    public void setServiceCharges(String serviceCharges)
    {
        this.serviceCharges = serviceCharges;
    }
}
