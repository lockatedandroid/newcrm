package com.android.lockated.crm.fragment.myzone.classified;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.android.lockated.LockatedApplication;
import com.android.lockated.R;
import com.android.lockated.crm.activity.PostClassifiedDetailsActivity;
import com.android.lockated.crm.adapters.MyClassifiedAdapter;
import com.android.lockated.information.AccountController;
import com.android.lockated.model.AccountData;
import com.android.lockated.model.usermodel.Classified.MyClassifiedMain;
import com.android.lockated.network.ConnectionDetector;
import com.android.lockated.preferences.LockatedPreferences;
import com.android.lockated.request.LockatedJSONObjectRequest;
import com.android.lockated.request.LockatedRequestError;
import com.android.lockated.request.LockatedVolleyRequestQueue;
import com.android.lockated.utils.ApplicationURL;
import com.android.lockated.utils.Utilities;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class MyClassifiedFragment extends Fragment implements Response.ErrorListener,
        Response.Listener<JSONObject>, View.OnClickListener {

    FloatingActionButton fab;
    ListView SellList;
    int societyid;
    String pid;
    ProgressBar progressBar;
    TextView errorMsg;
    AccountController accountController;
    ArrayList<AccountData> accountDataArrayList;
    ArrayList<MyClassifiedMain> allClassifieds;
    private RequestQueue mQueue;
    MyClassifiedAdapter myClassifiedAdapter;
    private LockatedPreferences mLockatedPreferences;
    public static final String REQUEST_TAG = "MyClassifiedFragment";
    String SHOW, CREATE, INDEX, UPDATE, EDIT;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View SellView = inflater.inflate(R.layout.fragment_my_classified, container, false);
        init(SellView);
        return SellView;
    }

    @Override
    public void onResume() {
        super.onResume();
        LockatedApplication.getInstance().trackScreenView(getString(R.string.myClassifieds));
        Utilities.lockatedGoogleAnalytics(getString(R.string.myClassifieds),
                getString(R.string.visited), getString(R.string.myClassifieds));

   /*     allClassifieds.clear();

        getMyClassifiedList();
        Log.e("onResume","getMyClassifiedList");*/

    }

    private void getMyClassifiedList() {
        if (getActivity() != null) {
            if (ConnectionDetector.isConnectedToInternet(getActivity())) {
                String url = ApplicationURL.getMyCreatedClassifieds + mLockatedPreferences.getLockatedToken()
                        + ApplicationURL.societyId + societyid;
                mQueue = LockatedVolleyRequestQueue.getInstance(getActivity()).getRequestQueue();
                LockatedJSONObjectRequest lockatedJSONObjectRequest = new LockatedJSONObjectRequest(Request.Method.GET,
                        url, null, this, this);
                lockatedJSONObjectRequest.setTag(REQUEST_TAG);
                mQueue.add(lockatedJSONObjectRequest);
            } else {
                Utilities.showToastMessage(getActivity(), getActivity().getResources().getString(R.string.internet_connection_error));
            }
        }
    }

    private void init(View sellView) {

        mLockatedPreferences = new LockatedPreferences(getActivity());
        allClassifieds = new ArrayList<>();
       /* accountController = AccountController.getInstance();
        accountDataArrayList = accountController.getmAccountDataList();*/
        societyid = Integer.parseInt(mLockatedPreferences.getSocietyId());
        progressBar = (ProgressBar) sellView.findViewById(R.id.mProgressBarView);
        SellList = (ListView) sellView.findViewById(R.id.listView);
        errorMsg = (TextView) sellView.findViewById(R.id.noNotices);
        fab = (FloatingActionButton) sellView.findViewById(R.id.fab);
        getClasssifiedRole();
        checkPermission();
        myClassifiedAdapter = new MyClassifiedAdapter(getActivity(),
                allClassifieds);
        SellList.setAdapter(myClassifiedAdapter);
        fab.setOnClickListener(this);
    }

    private void checkPermission() {
        if (CREATE!=null&&CREATE.equals("true")) {
            fab.setVisibility(View.VISIBLE);
        } else {
            fab.setVisibility(View.GONE);
        }
        if (SHOW!=null&&SHOW.equals("true")) {
            getMyClassifiedList();
        } else {
            SellList.setVisibility(View.GONE);
            errorMsg.setVisibility(View.VISIBLE);
            errorMsg.setText(R.string.no_permission_error);
        }
    }

    @Override
    public void onClick(View v) {
        Intent postClassifiedIntent = new Intent(getActivity(), PostClassifiedDetailsActivity.class);
        startActivity(postClassifiedIntent);
    }

    @Override
    public void onErrorResponse(VolleyError error) {
        if (getActivity() != null) {
            LockatedRequestError.onRequestError(getActivity(), error);
        }
    }

    @Override
    public void onResponse(JSONObject response) {
        if (getActivity() != null) {
            try {
                if (response != null && response.length() > 0) {
                    if (response.has("classifieds") && response.getJSONArray("classifieds").length() > 0) {

                        JSONArray jsonArray = response.getJSONArray("classifieds");
                        Gson gson = new Gson();
                        for (int i = 0; i < jsonArray.length(); i++) {
                            MyClassifiedMain allClassified = gson.fromJson(jsonArray.getJSONObject(i).toString(),
                                    MyClassifiedMain.class);
                            allClassifieds.add(allClassified);
                        }
                        myClassifiedAdapter.notifyDataSetChanged();
                    } else {
                        SellList.setVisibility(View.GONE);
                        errorMsg.setVisibility(View.VISIBLE);
                        errorMsg.setText(R.string.no_data_error);
                    }
                } else {
                    SellList.setVisibility(View.GONE);
                    errorMsg.setVisibility(View.VISIBLE);
                    errorMsg.setText(R.string.no_data_error);
                }

            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public String getClasssifiedRole() {
        String mySocietyRoles = getActivity().getResources().getString(R.string.blank_value);
        if (mLockatedPreferences.getRolesJson() != null && !mLockatedPreferences.getRolesJson().equals("blank")) {
            try {
                JSONObject noticeJsonObj = new JSONObject(mLockatedPreferences.getRolesJson());
                for (int i = 0; i < noticeJsonObj.getJSONArray(getActivity().getResources().getString(R.string.permissions_value)).length(); i++) {
                    JSONObject jsonObject1 = noticeJsonObj.getJSONArray(getActivity().getResources().getString(R.string.permissions_value)).getJSONObject(i);
                    if (jsonObject1.has(getActivity().getResources().getString(R.string.section_value))
                            && jsonObject1.getString(getActivity().getResources().getString(R.string.section_value)).equals("spree_classifieds")) {
                        if (jsonObject1.getJSONObject(getActivity().getResources().getString(R.string.permission_value)).has(getActivity().getResources().getString(R.string.index_value))) {
                            mySocietyRoles = jsonObject1.getJSONObject(getActivity().getResources().getString(R.string.permission_value)).getString(getActivity().getResources().getString(R.string.index_value));
                            CREATE = jsonObject1.getJSONObject(getActivity().getResources().getString(R.string.permission_value)).getString("create");
                            INDEX = jsonObject1.getJSONObject(getActivity().getResources().getString(R.string.permission_value)).getString("index");
                            UPDATE = jsonObject1.getJSONObject(getActivity().getResources().getString(R.string.permission_value)).getString("update");
                            EDIT = jsonObject1.getJSONObject(getActivity().getResources().getString(R.string.permission_value)).getString("edit");
                            SHOW = jsonObject1.getJSONObject(getActivity().getResources().getString(R.string.permission_value)).getString("show");
                        }
                    }
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return mySocietyRoles;
    }
}
