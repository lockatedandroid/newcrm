package com.android.lockated.register;

import android.content.Intent;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;

import com.android.lockated.HomeActivity;
import com.android.lockated.R;

public class ChangePasswordActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_password);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(true);
        getSupportActionBar().setTitle(R.string.change_password);
        init();
    }

    private void init() {
        FragmentManager fragmentManager = getSupportFragmentManager();
        ResetPasswordFragment resetPasswordFragment = new ResetPasswordFragment();
        resetPasswordFragment.setResetPasswordFragment("ChangePasswordActivity");

        String mMobileNumber = getIntent().getExtras().getString("mobileno");
        Bundle bundle = new Bundle();
        bundle.putString("mobileNumber", mMobileNumber);
        resetPasswordFragment.setArguments(bundle);
        resetPasswordFragment.setFragmentManager(fragmentManager);

        getSupportFragmentManager().beginTransaction().add(R.id.passwordContainer, resetPasswordFragment).commit();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                callIntent();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        callIntent();
    }

    private void callIntent() {
        Intent intent = new Intent(this, HomeActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.putExtra("EditPersonalInformationFragment", "EditPersonalInformationFragment");
        startActivity(intent);
        finish();
    }

}
