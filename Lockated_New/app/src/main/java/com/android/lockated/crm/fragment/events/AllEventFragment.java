package com.android.lockated.crm.fragment.events;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.android.lockated.LockatedApplication;
import com.android.lockated.R;
import com.android.lockated.crm.activity.CreateNoticeListEventActivity;
import com.android.lockated.crm.adapters.EventAdapter;
import com.android.lockated.model.usermodel.EventModel.Events;
import com.android.lockated.network.ConnectionDetector;
import com.android.lockated.preferences.LockatedPreferences;
import com.android.lockated.request.LockatedRequestError;
import com.android.lockated.request.LockatedVolleyRequestQueue;
import com.android.lockated.utils.ApplicationURL;
import com.android.lockated.utils.Utilities;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class AllEventFragment extends Fragment implements Response.Listener<JSONObject>, Response.ErrorListener, View.OnClickListener {
    public static final String REQUEST_TAG = "AllEventFragment";
    TextView errorMsg;
    RecyclerView eventList;
    ProgressBar progressBar;
    JSONObject eventJsonObj;
    FloatingActionButton fab;
    SwipeRefreshLayout mSwipeRefreshLayout;
    EventAdapter eventAdapter;
    ArrayList<Events> eventArrayList;
    String SHOW, CREATE, INDEX, UPDATE, EDIT;
    private RequestQueue mQueue;
    private LockatedPreferences mLockatedPreferences;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View eventView = inflater.inflate(R.layout.fragment_event, container, false);
        Utilities.ladooIntegration(getActivity(), "All Events");
        init(eventView);
        return eventView;

    }

    private void init(View view) {
        eventArrayList = new ArrayList<>();
        eventList = (RecyclerView) view.findViewById(R.id.eventList);
        final LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        eventList.setLayoutManager(linearLayoutManager);
        eventList.setHasFixedSize(true);
        progressBar = (ProgressBar) view.findViewById(R.id.mProgressBarView);
        errorMsg = (TextView) view.findViewById(R.id.noNotices);
        mLockatedPreferences = new LockatedPreferences(getActivity());
        fab = (FloatingActionButton) view.findViewById(R.id.fab);
        mSwipeRefreshLayout = (SwipeRefreshLayout)view.findViewById(R.id.swipe_refresh_layout);
       // swipToRefresh();
        getEventRole();
        checkPermission();
        fab.setOnClickListener(this);
        eventAdapter = new EventAdapter(getActivity(), eventArrayList, getChildFragmentManager());
        eventList.setAdapter(eventAdapter);
      /*  try {
            eventJsonObj = new JSONObject(mLockatedPreferences.getRolesJson());
            userType = eventJsonObj.getString("name");
            if (!userType.equals("admin") || !userType.equals("society_admin")) {
                show = eventJsonObj.getJSONArray("permissions").getJSONObject(5).getJSONObject("permission").getString("show");
                create = eventJsonObj.getJSONArray("permissions").getJSONObject(5).getJSONObject("permission").getString("new");
                edit = eventJsonObj.getJSONArray("permissions").getJSONObject(5).getJSONObject("permission").getString("edit");
            }


        } catch (JSONException e) {
            e.printStackTrace();
            create = "false";
            edit = "false";
            show = "false";
        }

        if (create.equals("false")) {
            fab.setVisibility(View.GONE);
        }
*/
    }

    private void swipToRefresh() {
        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                callApi();

            }
        });
        mSwipeRefreshLayout.setColorSchemeResources(
                android.R.color.holo_orange_light,
                android.R.color.holo_red_light,
                android.R.color.holo_green_light,
                android.R.color.holo_purple);
    }

    private void onItemsLoadComplete() {
        mSwipeRefreshLayout.setRefreshing(false);
    }

    private void checkPermission() {
        if (CREATE!=null&&CREATE.equals("true")) {
            fab.setVisibility(View.VISIBLE);
        } else {
            fab.setVisibility(View.GONE);
        }
        if (SHOW!=null&&SHOW.equals("true")) {
            callApi();
        } else {
            eventList.setVisibility(View.GONE);
            errorMsg.setVisibility(View.VISIBLE);
            errorMsg.setText(R.string.no_permission_error);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        LockatedApplication.getInstance().trackScreenView(getString(R.string.all_events));
        Utilities.lockatedGoogleAnalytics(getString(R.string.all_events),
                getString(R.string.visited), getString(R.string.all_events));
        //  eventArrayList.clear();
    }

    public void callApi() {
        progressBar.setVisibility(View.VISIBLE);
        if (ConnectionDetector.isConnectedToInternet(getActivity())) {
            mLockatedPreferences = new LockatedPreferences(getActivity());
            String url = ApplicationURL.getEventsUrl + mLockatedPreferences.getLockatedToken()
                    + "&id_society=" + mLockatedPreferences.getSocietyId();
            LockatedVolleyRequestQueue lockatedVolleyRequestQueue = LockatedVolleyRequestQueue.getInstance(getActivity());
            lockatedVolleyRequestQueue.sendRequest(REQUEST_TAG, Request.Method.GET, url, null, this, this);
        } else {
            Utilities.showToastMessage(getActivity(), getActivity().getResources().getString(R.string.internet_connection_error));
        }
    }

    @Override
    public void onResponse(JSONObject response) {
      //  onItemsLoadComplete();
        progressBar.setVisibility(View.GONE);
        if (getActivity() != null) {
            try {
                if (response != null && response.length() > 0) {
                    if (response.has("events") && response.getJSONArray("events").length() > 0) {
                        JSONArray jsonArray = response.getJSONArray("events");
                        Gson gson = new Gson();
                        for (int i = 0; i < jsonArray.length(); i++) {
                            Events events = gson.fromJson(jsonArray.getJSONObject(i).toString(), Events.class);
                            eventArrayList.add(events);
                        }
                        eventAdapter.notifyDataSetChanged();
                    } else {
                        eventList.setVisibility(View.GONE);
                        errorMsg.setVisibility(View.VISIBLE);
                        errorMsg.setText(R.string.no_data_error);
                    }
                } else {
                    eventList.setVisibility(View.GONE);
                    errorMsg.setVisibility(View.VISIBLE);
                    errorMsg.setText(R.string.no_data_error);
                }

            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void onErrorResponse(VolleyError error) {
        progressBar.setVisibility(View.GONE);
        if (getActivity() != null) {
            LockatedRequestError.onRequestError(getActivity(), error);
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.fab:
                Intent intent = new Intent(getActivity(), CreateNoticeListEventActivity.class);
                intent.setClass(getActivity(),CreateNoticeListEventActivity.class);
                startActivity(intent);
                break;
        }
    }

    public String getEventRole() {
        String mySocietyRoles = getActivity().getResources().getString(R.string.blank_value);
        if (mLockatedPreferences.getRolesJson() != null && !mLockatedPreferences.getRolesJson().equals("blank")) {
            try {
                JSONObject noticeJsonObj = new JSONObject(mLockatedPreferences.getRolesJson());
                for (int i = 0; i < noticeJsonObj.getJSONArray(getActivity().getResources().getString(R.string.permissions_value)).length(); i++) {
                    JSONObject jsonObject1 = noticeJsonObj.getJSONArray(getActivity().getResources().getString(R.string.permissions_value)).getJSONObject(i);
                    if (jsonObject1.has(getActivity().getResources().getString(R.string.section_value))
                            && jsonObject1.getString(getActivity().getResources().getString(R.string.section_value)).equals("spree_events")) {
                        if (jsonObject1.getJSONObject(getActivity().getResources().getString(R.string.permission_value)).has(getActivity().getResources().getString(R.string.index_value))) {
                            mySocietyRoles = jsonObject1.getJSONObject(getActivity().getResources().getString(R.string.permission_value)).getString(getActivity().getResources().getString(R.string.index_value));
                            CREATE = jsonObject1.getJSONObject(getActivity().getResources().getString(R.string.permission_value)).getString("create");
                            INDEX = jsonObject1.getJSONObject(getActivity().getResources().getString(R.string.permission_value)).getString("index");
                            UPDATE = jsonObject1.getJSONObject(getActivity().getResources().getString(R.string.permission_value)).getString("update");
                            EDIT = jsonObject1.getJSONObject(getActivity().getResources().getString(R.string.permission_value)).getString("edit");
                            SHOW = jsonObject1.getJSONObject(getActivity().getResources().getString(R.string.permission_value)).getString("show");
                            //Log.e("Permissions ", " CREATE" + CREATE + "INDEX" + " " + INDEX + "UPDATE" + " " + UPDATE + " " + "EDIT" + EDIT);
                        }
                    }
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return mySocietyRoles;
    }

}