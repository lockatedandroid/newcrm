package com.android.lockated.search;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.android.lockated.Interfaces.IRecyclerItemClickListener;
import com.android.lockated.LockatedApplication;
import com.android.lockated.R;
import com.android.lockated.cart.CartController;
import com.android.lockated.cart.ecommerce.EcommerceCartActivity;
import com.android.lockated.categories.grocery.adapter.CategoryItemDetailAdapter;
import com.android.lockated.categories.grocery.model.GroceryItemData;
import com.android.lockated.information.AccountController;
import com.android.lockated.model.AccountData;
import com.android.lockated.model.CartDetail;
import com.android.lockated.network.ConnectionDetector;
import com.android.lockated.preferences.LockatedPreferences;
import com.android.lockated.request.LockatedJSONObjectRequest;
import com.android.lockated.request.LockatedRequestError;
import com.android.lockated.request.LockatedVolleyRequestQueue;
import com.android.lockated.search.adapter.SearchAdapter;
import com.android.lockated.utils.ApplicationURL;
import com.android.lockated.utils.Utilities;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class SearchActivity extends AppCompatActivity implements View.OnClickListener, IRecyclerItemClickListener,
        Response.Listener, Response.ErrorListener, AdapterView.OnItemSelectedListener,
        AdapterView.OnItemClickListener, TextWatcher {

    private ProgressBar mProgressBarView;
    private ProgressDialog mProgressDialog;

    //private EditText mEditTextSearchQuery;
    private AutoCompleteTextView mEditTextSearchQuery;
    private TextView mTextViewSearchQuery;
    private TextView mTextViewProductDetailViewNext;
    private TextView mTextViewProductDetailViewContinue;
    private TextView mTextViewProductDetailViewTotalPrice;

    private int mTaxonId;
    private int mTaxonomyId;

    private CartController mCartController;
    private ArrayList<GroceryItemData> mGroceryItemDataList;

    private LockatedPreferences mLockatedPreferences;
    private LockatedVolleyRequestQueue lockatedVolleyRequestQueue;
    private LockatedJSONObjectRequest mLockatedJSONObjectRequest;
    private CategoryItemDetailAdapter mCategoryItemDetailAdapter;

    ArrayAdapter adapter;
    //SearchAdapter searchAdapter;

    public static final String REQUEST_TAG = "SearchActivity";
    private static final String ADD_PRODUCT_TAG = "SearchActivity:AddProduct";
    private static final String REQUEST_LOAD_ALL = "LoadAllProducts";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);

        Toolbar toolbar = (Toolbar) findViewById(R.id.mSearchToolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(true);
        getSupportActionBar().setTitle("");

        init();
        setListener();
        loadSearchQueryList();
    }

    private void init() {
        mGroceryItemDataList = new ArrayList<>();
        mCartController = CartController.getInstance();
        mLockatedPreferences = new LockatedPreferences(SearchActivity.this);

        mTaxonId = ((LockatedApplication) getApplicationContext()).getTaxonId();
        mTaxonomyId = ((LockatedApplication) getApplicationContext()).getTaxonomyId();

        mEditTextSearchQuery = (AutoCompleteTextView) findViewById(R.id.mEditTextSearchQuery);
        mEditTextSearchQuery.setDropDownBackgroundResource(R.color.chat_bubble_white);
        mEditTextSearchQuery.setOnItemSelectedListener(this);
        mEditTextSearchQuery.setOnItemClickListener(this);
        mEditTextSearchQuery.addTextChangedListener(this);

        mTextViewSearchQuery = (TextView) findViewById(R.id.mTextViewSearchQuery);

        mProgressBarView = (ProgressBar) findViewById(R.id.mProgressBarView);
        RecyclerView mRecyclerView = (RecyclerView) findViewById(R.id.mRecyclerView);

        mTextViewProductDetailViewNext = (TextView) findViewById(R.id.mTextViewProductDetailViewNext);
        mTextViewProductDetailViewContinue = (TextView) findViewById(R.id.mTextViewProductDetailViewContinue);
        mTextViewProductDetailViewTotalPrice = (TextView) findViewById(R.id.mTextViewProductDetailViewTotalPrice);

        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(SearchActivity.this);
        mRecyclerView.setLayoutManager(mLayoutManager);
        mRecyclerView.setHasFixedSize(true);

        mTextViewSearchQuery.setOnClickListener(this);
        mTextViewProductDetailViewNext.setOnClickListener(this);
        mTextViewProductDetailViewContinue.setOnClickListener(this);

        mCategoryItemDetailAdapter = new CategoryItemDetailAdapter(mGroceryItemDataList, this);
        mRecyclerView.setAdapter(mCategoryItemDetailAdapter);
    }

    private void setListener() {
        mEditTextSearchQuery.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.length() > 0) {
                    mEditTextSearchQuery.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_edit_text_clear, 0);
                } else {
                    mEditTextSearchQuery.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        mEditTextSearchQuery.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                final int DRAWABLE_RIGHT = 2;
                if (event.getAction() == MotionEvent.ACTION_UP) {
                    if (mEditTextSearchQuery.getCompoundDrawables()[DRAWABLE_RIGHT] != null) {
                        if (event.getX() >= (mEditTextSearchQuery.getRight() -
                                mEditTextSearchQuery.getCompoundDrawables()[DRAWABLE_RIGHT].getBounds().width())) {
                            mEditTextSearchQuery.setText("");
                            return true;
                        }
                    }
                }
                return false;
            }
        });

        mEditTextSearchQuery.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    onSearchQueryClearClicked();
                    return true;
                }
                return false;
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.home, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_cart: {
                onCartClicked();
                return true;
            }
            case android.R.id.home: {
                super.onBackPressed();
                return true;
            }
        }
        return super.onOptionsItemSelected(item);
    }

    private void onCartClicked() {
        ArrayList<AccountData> accountDatas = AccountController.getInstance().getmAccountDataList();
        if (accountDatas.size() > 0) {
            ArrayList<CartDetail> cartDetails = accountDatas.get(0).getCartDetailList();
            if (cartDetails.size() > 0) {
                if (cartDetails.get(0).getmLineItemData().size() > 0) {
                    Intent intent = new Intent(SearchActivity.this, EcommerceCartActivity.class);
                    startActivity(intent);
                } else {
                    Utilities.showToastMessage(SearchActivity.this, getString(R.string.empty_cart));
                }
            } else {
                Utilities.showToastMessage(SearchActivity.this, getString(R.string.empty_cart));
            }
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.mTextViewSearchQuery:
                onSearchQueryClearClicked();
                break;
            case R.id.mTextViewProductDetailViewNext:
                onNextButtonClicked();
                break;
            case R.id.mTextViewProductDetailViewContinue:
                SearchActivity.this.finish();
                break;
        }
    }

    private void onNextButtonClicked() {
        ArrayList<AccountData> mAccountData = AccountController.getInstance().getmAccountDataList();
        if (mAccountData.size() > 0) {
            if (mAccountData.get(0).getCartDetailList().size() > 0) {
                int cartSize = mCartController.mGroceryCartDataList.size();
                if (cartSize > 0) {
                    JSONObject jsonObject = new JSONObject();
                    JSONArray jsonArray = new JSONArray();
                    try {
                        jsonObject.put("line_items", jsonArray);
                        for (int i = 0; i < mCartController.mGroceryCartDataList.size(); i++) {
                            JSONObject itemObject = new JSONObject();
                            itemObject.put("variant_id", mCartController.mGroceryCartDataList.get(i).getMasterId());
                            itemObject.put("quantity", mCartController.mGroceryCartDataList.get(i).getQuantity());
                            jsonArray.put(i, itemObject);
                        }

                        addAnotherItemToCart(jsonObject);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                } else {
                    if (mAccountData.get(0).getCartDetailList().get(0).getmLineItemData().size() > 0) {
                        openEcommerceCartActivity();
                    } else {
                        Utilities.showToastMessage(SearchActivity.this, "Please Select an Item to Proceed");
                    }
                }
            } else {
                int cartSize = mCartController.mGroceryCartDataList.size();
                if (cartSize > 0) {
                    JSONObject jsonObject = new JSONObject();
                    JSONObject orderObject = new JSONObject();
                    JSONArray jsonArray = new JSONArray();
                    try {
                        jsonObject.put("order", orderObject);
                        orderObject.put("line_items", jsonArray);
                        for (int i = 0; i < mCartController.mGroceryCartDataList.size(); i++) {
                            JSONObject itemObject = new JSONObject();
                            itemObject.put("variant_id", mCartController.mGroceryCartDataList.get(i).getMasterId());
                            itemObject.put("quantity", mCartController.mGroceryCartDataList.get(i).getQuantity());
                            jsonArray.put(i, itemObject);
                        }

                        addProductToCart(jsonObject);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                } else {
                    if (mAccountData.get(0).getCartDetailList().size() > 0) {
                        if (mAccountData.get(0).getCartDetailList().get(0).getmLineItemData().size() > 0) {
                            openEcommerceCartActivity();
                        } else {
                            Utilities.showToastMessage(SearchActivity.this, getString(R.string.item_selection_error));
                        }
                    } else {
                        Utilities.showToastMessage(SearchActivity.this, getString(R.string.item_selection_error));
                    }
                }
            }
        }
    }

    private void addAnotherItemToCart(JSONObject jsonObject) {
        if (ConnectionDetector.isConnectedToInternet(SearchActivity.this)) {
            mProgressDialog = ProgressDialog.show(SearchActivity.this, "", "Please Wait...", false);
            lockatedVolleyRequestQueue = LockatedVolleyRequestQueue.getInstance(SearchActivity.this);
            String url = ApplicationURL.addAnotherItemToCartUrl + mLockatedPreferences.getLockatedToken();
            lockatedVolleyRequestQueue.sendRequest(ADD_PRODUCT_TAG, Request.Method.POST, url, jsonObject, this, this);
        } else {
            Utilities.showToastMessage(SearchActivity.this, getString(R.string.internet_connection_error));
        }
    }

    private void addProductToCart(JSONObject jsonObject) {
        if (ConnectionDetector.isConnectedToInternet(SearchActivity.this)) {
            mProgressDialog = ProgressDialog.show(SearchActivity.this, "", "Please Wait...", false);
            lockatedVolleyRequestQueue = LockatedVolleyRequestQueue.getInstance(SearchActivity.this);
            lockatedVolleyRequestQueue.sendRequest(ADD_PRODUCT_TAG, Request.Method.POST, ApplicationURL.addProductToCartUrl
                    + mLockatedPreferences.getLockatedToken(), jsonObject, this, this);
        } else {
            Utilities.showToastMessage(SearchActivity.this, getString(R.string.internet_connection_error));
        }
    }

    private void openEcommerceCartActivity() {
        Intent intent = new Intent(SearchActivity.this, EcommerceCartActivity.class);
        startActivity(intent);
    }

    private void onSearchQueryClearClicked() {
        String strQuery = mEditTextSearchQuery.getText().toString();
        if (strQuery.length() > 0) {
            if (strQuery.contains(" ")) {
                strQuery = strQuery.replaceAll(" ", "%20");
            }
            hideSoftInput();
            lockatedVolleyRequestQueue = LockatedVolleyRequestQueue.getInstance(SearchActivity.this);
            if (ConnectionDetector.isConnectedToInternet(SearchActivity.this)) {
                mProgressBarView.setVisibility(View.VISIBLE);
                lockatedVolleyRequestQueue = LockatedVolleyRequestQueue.getInstance(SearchActivity.this);
                lockatedVolleyRequestQueue.sendRequest(REQUEST_TAG, Request.Method.GET, ApplicationURL.getSearchedProductUrl
                        + mLockatedPreferences.getLockatedToken() + "&q[name_cont]=" + strQuery
                        + "&taxonomy_id=" + mTaxonomyId + "&taxon_id=" + mTaxonId, null, this, this);
            } else {
                Utilities.showToastMessage(SearchActivity.this, getString(R.string.internet_connection_error));
            }
        } else {
            Utilities.showToastMessage(SearchActivity.this, "Please enter something to search.");
        }
    }

    public boolean hideSoftInput() {
        InputMethodManager imm = (InputMethodManager) getSystemService(Activity.INPUT_METHOD_SERVICE);
        return imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
    }

    private void loadSearchQueryList() {
        if (ConnectionDetector.isConnectedToInternet(SearchActivity.this)) {
            mProgressBarView.setVisibility(View.VISIBLE);
            lockatedVolleyRequestQueue = LockatedVolleyRequestQueue.getInstance(SearchActivity.this);

            lockatedVolleyRequestQueue.sendRequest(REQUEST_LOAD_ALL, Request.Method.GET, ApplicationURL.getAllProductNameUrl
                    + mLockatedPreferences.getLockatedToken() + "&taxonomy_id=" + mTaxonomyId, null, this, this);
        } else {
            Utilities.showToastMessage(SearchActivity.this, getString(R.string.internet_connection_error));
        }
    }

    @Override
    public void onRecyclerItemClick(View view, int position) {
        GroceryItemData groceryItemData = mGroceryItemDataList.get(position);
        switch (view.getId()) {
            case R.id.mImageViewProductQuantityAdd:
                onQuantityAddClicked(groceryItemData);
                break;
            case R.id.mImageViewProductQuantityRemove:
                onQuantityRemoveClicked(groceryItemData);
                break;
        }
    }

    private void onQuantityAddClicked(GroceryItemData groceryItemData) {
        groceryItemData.setQuantity(groceryItemData.getQuantity() + 1);
        productAddToCart(groceryItemData);
        mCategoryItemDetailAdapter.notifyDataSetChanged();
    }

    private void onQuantityRemoveClicked(GroceryItemData groceryItemData) {
        groceryItemData.setQuantity(groceryItemData.getQuantity() - 1);
        productAddToCart(groceryItemData);
        mCategoryItemDetailAdapter.notifyDataSetChanged();
    }

    public void productAddToCart(GroceryItemData groceryItemData) {
        if (mCartController.mGroceryCartDataList.size() > 0) {
            if (mCartController.mGroceryCartDataList.contains(groceryItemData)) {
                if (groceryItemData.getQuantity() != 0) {
                    mCartController.mGroceryCartDataList.remove(groceryItemData);
                    mCartController.mGroceryCartDataList.add(groceryItemData);
                } else {
                    mCartController.mGroceryCartDataList.remove(groceryItemData);
                }
            } else {
                mCartController.mGroceryCartDataList.add(groceryItemData);
            }
        } else {
            mCartController.mGroceryCartDataList.add(groceryItemData);
        }

        updatePriceInfo();
    }

    private void updatePriceInfo() {
        double price = 0.00;
        for (int i = 0; i < mCartController.mGroceryCartDataList.size(); i++) {
            price += (Double.valueOf(mCartController.mGroceryCartDataList.get(i).getPrice()) * mCartController.mGroceryCartDataList.get(i).getQuantity());
        }

        mTextViewProductDetailViewTotalPrice.setText(getString(R.string.rupees_symbol) + " " + price + "");
    }

    @Override
    public void onErrorResponse(VolleyError error) {
        mProgressBarView.setVisibility(View.INVISIBLE);
        if (!isFinishing()) {
            if (mProgressDialog != null && mProgressDialog.isShowing()) {
                mProgressDialog.dismiss();
            }
            LockatedRequestError.onRequestError(SearchActivity.this, error);
        }
    }

    @Override
    public void onResponse(Object response) {
        ArrayList<String> itemNameList = new ArrayList<>();
        if (!isFinishing()) {
            if (mProgressDialog != null && mProgressDialog.isShowing()) {
                mProgressDialog.dismiss();
            }

            mProgressBarView.setVisibility(View.INVISIBLE);
            if (lockatedVolleyRequestQueue.getLockatedJSONObjectRequest().getTag().toString().equals(REQUEST_TAG)) {
                JSONObject jsonObject = (JSONObject) response;
                try {
                    mGroceryItemDataList.clear();
                    JSONArray jsonArray = jsonObject.getJSONArray("products");
                    if (jsonArray.length() > 0) {
                        mGroceryItemDataList.clear();
                        for (int i = 0; i < jsonArray.length(); i++) {
                            JSONObject productsObject = jsonArray.getJSONObject(i);
                            GroceryItemData groceryItemData = new GroceryItemData(productsObject);
                            mGroceryItemDataList.add(mGroceryItemDataList.size(), groceryItemData);
                        }
                    } else {
                        Utilities.showToastMessage(SearchActivity.this, "No Result Found.");
                    }

                    mCategoryItemDetailAdapter.notifyDataSetChanged();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else if (lockatedVolleyRequestQueue.getLockatedJSONObjectRequest().getTag().toString().equals(ADD_PRODUCT_TAG)) {
                ArrayList<AccountData> accountDatas = AccountController.getInstance().getmAccountDataList();
                if (accountDatas.size() > 0) {
                    accountDatas.get(0).getCartDetailList().clear();

                    JSONObject jsonObject = (JSONObject) response;
                    CartDetail cartDetail = new CartDetail(jsonObject);
                    accountDatas.get(0).getCartDetailList().add(cartDetail);
                    mCartController.resetItems();

                    openEcommerceCartActivity();
                }
            } else if (lockatedVolleyRequestQueue.getLockatedJSONObjectRequest().getTag().toString().equals(REQUEST_LOAD_ALL)) {
                JSONObject jsonObject = (JSONObject) response;
                try {
                    if (jsonObject.has("products"))
                        if (jsonObject.getJSONArray("products").length() > 0) {
                            mGroceryItemDataList.clear();
                            for (int i = 0; i < jsonObject.getJSONArray("products").length(); i++) {
                                itemNameList.add(jsonObject.getJSONArray("products").get(i).toString());
                            }
                        } else {
                            Utilities.showToastMessage(SearchActivity.this, "No Result Found.");
                        }
                    adapter = new ArrayAdapter<>(this, android.R.layout.preference_category, itemNameList);
                    //searchAdapter = new SearchAdapter(this, itemNameList);
                    mEditTextSearchQuery.setThreshold(1);
                    mEditTextSearchQuery.setAdapter(adapter);
                    //mEditTextSearchQuery.setAdapter(searchAdapter);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        hideSoftInput();
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {
        hideSoftInput();
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        onSearchQueryClearClicked();
        hideSoftInput();
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {

    }

    @Override
    public void afterTextChanged(Editable s) {

    }

}
