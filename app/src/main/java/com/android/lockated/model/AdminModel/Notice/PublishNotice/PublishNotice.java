
package com.android.lockated.model.AdminModel.Notice.PublishNotice;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class PublishNotice {

    @SerializedName("daily_feed")
    @Expose
    private Object dailyFeed;
    @SerializedName("publishNoticeboards")
    @Expose
    private ArrayList<PublishNoticeboard> publishNoticeboards = new ArrayList<PublishNoticeboard>();
    @SerializedName("shared")
    @Expose
    private ArrayList<Object> shared = new ArrayList<Object>();

    /**
     * 
     * @return
     *     The dailyFeed
     */
    public Object getDailyFeed() {
        return dailyFeed;
    }

    /**
     * 
     * @param dailyFeed
     *     The daily_feed
     */
    public void setDailyFeed(Object dailyFeed) {
        this.dailyFeed = dailyFeed;
    }

    /**
     * 
     * @return
     *     The publishNoticeboards
     */
    public ArrayList<PublishNoticeboard> getPublishNoticeboards() {
        return publishNoticeboards;
    }

    /**
     * 
     * @param publishNoticeboards
     *     The publishNoticeboards
     */
    public void setPublishNoticeboards(ArrayList<PublishNoticeboard> publishNoticeboards) {
        this.publishNoticeboards = publishNoticeboards;
    }

    /**
     * 
     * @return
     *     The shared
     */
    public ArrayList<Object> getShared() {
        return shared;
    }

    /**
     * 
     * @param shared
     *     The shared
     */
    public void setShared(ArrayList<Object> shared) {
        this.shared = shared;
    }

}
