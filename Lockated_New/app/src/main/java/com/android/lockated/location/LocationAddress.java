package com.android.lockated.location;

import android.content.Context;
import android.location.Address;
import android.location.Geocoder;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.List;
import java.util.Locale;

public class LocationAddress {
    private static final String TAG = "LocationAddress";

    public void getAddressFromLocation(final double latitude, final double longitude,
                                       final Context context, final Handler handler) {
        Thread thread = new Thread() {
            @Override
            public void run() {
                Geocoder geocoder = new Geocoder(context, Locale.getDefault());
                String result = null;
                try {
                    List<Address> addressList = geocoder.getFromLocation(latitude, longitude, 1);
                    if (addressList != null && addressList.size() > 0) {
                        Address address = addressList.get(0);
                        JSONObject jsonObject = new JSONObject();

                        try {
                            jsonObject.put("locality", address.getLocality());
                            jsonObject.put("state", address.getAdminArea());
                            jsonObject.put("postalCode", address.getPostalCode());
                            jsonObject.put("countryName", address.getCountryName());

                            JSONArray addressArray = new JSONArray();
                            for (int i = 0; i < address.getMaxAddressLineIndex(); i++) {
                                JSONObject addressObject = new JSONObject();
                                addressObject.put("address", address.getAddressLine(i));
                                addressArray.put(i, addressObject);
                            }

                            jsonObject.put("address", addressArray);
                            result = jsonObject.toString();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                } catch (IOException e) {
                } finally {
                    Message message = Message.obtain();
                    message.setTarget(handler);
                    if (result != null) {
                        message.what = 1;
                        Bundle bundle = new Bundle();
                        bundle.putString("address", result);
                        message.setData(bundle);
                    } else {
                        message.what = 1;
                        Bundle bundle = new Bundle();
                        bundle.putString("address", result);
                        message.setData(bundle);
                    }
                    message.sendToTarget();
                }
            }
        };
        thread.start();
    }
}
