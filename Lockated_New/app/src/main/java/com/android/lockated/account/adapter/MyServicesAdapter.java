package com.android.lockated.account.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.android.lockated.Interfaces.IRecyclerItemClickListener;
import com.android.lockated.R;
import com.android.lockated.holder.RecyclerViewHolder;
import com.android.lockated.model.MyServices.ServiceRequest;

import java.util.ArrayList;

public class MyServicesAdapter extends RecyclerView.Adapter<RecyclerViewHolder> {
    ArrayList<ServiceRequest> myServicesArrayList;
    private IRecyclerItemClickListener mIRecyclerItemClickListener;

    public MyServicesAdapter(ArrayList<ServiceRequest> data, IRecyclerItemClickListener listener) {
        myServicesArrayList = data;
        mIRecyclerItemClickListener = listener;
    }

    @Override
    public RecyclerViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.fragment_orders_row, parent, false);
        return new RecyclerViewHolder(itemView, mIRecyclerItemClickListener);
    }

    @Override
    public void onBindViewHolder(RecyclerViewHolder holder, int position) {
        holder.onServiceViewHolder(myServicesArrayList.get(position));
    }

    @Override
    public int getItemCount() {
        return myServicesArrayList.size();
    }
}
