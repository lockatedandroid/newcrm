package com.android.lockated.crm.fragment;

import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.android.lockated.LockatedApplication;
import com.android.lockated.R;
import com.android.lockated.crm.adapters.AboutSocietyAmenitiesAdapter;
import com.android.lockated.preferences.LockatedPreferences;
import com.android.lockated.request.LockatedRequestError;
import com.android.lockated.request.LockatedVolleyRequestQueue;
import com.android.lockated.utils.ApplicationURL;
import com.android.lockated.utils.DatePickerAllDates;
import com.android.lockated.utils.Utilities;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;

import org.json.JSONException;
import org.json.JSONObject;


public class AboutSocietyFragment extends Fragment implements Response.Listener<JSONObject>, Response.ErrorListener, View.OnClickListener {

    private TextView societyName, address, submitText, established;
    private EditText description, regId, residents;
    private String SOCIETY_DETAIL = "AboutSocietyFragment";
    LockatedPreferences lockatedPreferences;
    private ProgressBar progressBar;
    RecyclerView amenitiesRecycler;
    ImageView mIageStartDate;
    AboutSocietyAmenitiesAdapter aboutSocietyAmenitiesAdapter;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View aboutUsView = inflater.inflate(R.layout.fragment_about_society, container, false);
        init(aboutUsView);
        Utilities.ladooIntegration(getActivity(), getActivity().getResources().getString(R.string.aboutSociety));
        return aboutUsView;
    }

    @Override
    public void onResume() {
        super.onResume();
        LockatedApplication.getInstance().trackScreenView(getString(R.string.aboutSociety));
        Utilities.lockatedGoogleAnalytics(getString(R.string.aboutSociety),
                getString(R.string.visited), getString(R.string.aboutSociety));
        getSocietyDetails();
    }

    private void init(View aboutUsView) {

        lockatedPreferences = new LockatedPreferences(getActivity());
        amenitiesRecycler = (RecyclerView) aboutUsView.findViewById(R.id.amenitiesValue);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity(),
                LinearLayoutManager.HORIZONTAL, false);
        amenitiesRecycler.setLayoutManager(linearLayoutManager);
        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        progressBar = (ProgressBar) aboutUsView.findViewById(R.id.mProgressBarView);
        societyName = (TextView) aboutUsView.findViewById(R.id.societyNameValue);
        address = (TextView) aboutUsView.findViewById(R.id.address1Value);
        regId = (EditText) aboutUsView.findViewById(R.id.registrationValue);
        residents = (EditText) aboutUsView.findViewById(R.id.residentValue);
        established = (TextView) aboutUsView.findViewById(R.id.establishedValue);
        description = (EditText) aboutUsView.findViewById(R.id.descriptionValue);
        submitText = (TextView) aboutUsView.findViewById(R.id.submit);
        mIageStartDate = (ImageView) aboutUsView.findViewById(R.id.mIageStartDate);
        if (Utilities.getUserRoles(getActivity()).equals("true")) {
            description.setEnabled(true);
            regId.setEnabled(true);
            residents.setEnabled(true);
            established.setEnabled(true);
            submitText.setVisibility(View.VISIBLE);
            mIageStartDate.setVisibility(View.VISIBLE);
        }
        mIageStartDate.setOnClickListener(this);
        submitText.setOnClickListener(this);
    }

    private void getSocietyDetails() {
        if (getActivity() != null) {
            progressBar.setVisibility(View.VISIBLE);
            String url = ApplicationURL.getSocietyDetails + lockatedPreferences.getSocietyId() +
                    ".json?token=" + lockatedPreferences.getLockatedToken();
            Log.e("url",""+url);
            LockatedVolleyRequestQueue lockatedVolleyRequestQueue = LockatedVolleyRequestQueue.getInstance(getActivity());
            lockatedVolleyRequestQueue.sendRequest(SOCIETY_DETAIL, Request.Method.GET, url, null, this, this);
        }
    }

    private void updateSocietyDetails(JSONObject jsonObject) {
        if (getActivity() != null) {
            progressBar.setVisibility(View.VISIBLE);
            String url = ApplicationURL.putSocietyDetails + lockatedPreferences.getSocietyId() +
                    ".json?token=" + lockatedPreferences.getLockatedToken();
            LockatedVolleyRequestQueue lockatedVolleyRequestQueue = LockatedVolleyRequestQueue.getInstance(getActivity());
            lockatedVolleyRequestQueue.sendRequest(SOCIETY_DETAIL, Request.Method.PUT, url, jsonObject, this, this);
        }
    }

    public String checkValue(String valueCheck) {
        if (valueCheck != null && !valueCheck.equals("null")) {
            return valueCheck;
        } else {
            return "No Data";
        }
    }

    @Override
    public void onErrorResponse(VolleyError error) {
        progressBar.setVisibility(View.GONE);
        if (getActivity() != null) {
            LockatedRequestError.onRequestError(getActivity(), error);
        }
    }

    @Override
    public void onResponse(JSONObject response) {
        progressBar.setVisibility(View.GONE);
        if (response != null) {
            try {
                if (response.has("society")&&response.getJSONObject("society").length()>0) {
                    try {
                        societyName.setText(checkValue(response.getJSONObject("society").getString("building_name")));
                        String addr = response.getJSONObject("society").getString("address1")
                                + "\n" + response.getJSONObject("society").getString("address2");
                        address.setText(checkValue(addr));
                        regId.setText(checkValue(response.getJSONObject("society").getString("registration")));
                        residents.setText(checkValue(response.getJSONObject("society").getString("residents")));
                        established.setText(checkValue(response.getJSONObject("society").getString("established")));
                        description.setText(checkValue(response.getJSONObject("society").getString("description")));

                        if (response.getJSONObject("society").has("amenities")
                                && response.getJSONObject("society").getJSONArray("amenities").length() > 0) {
                            aboutSocietyAmenitiesAdapter = new AboutSocietyAmenitiesAdapter(getActivity(),
                                    response.getJSONObject("society").getJSONArray("amenities"));
                            amenitiesRecycler.setAdapter(aboutSocietyAmenitiesAdapter);
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
                else {
                    amenitiesRecycler.setVisibility(View.GONE);
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

    }

    private void openDatePicker(TextView selectTime) {
        DatePickerAllDates datePickerFragment = new DatePickerAllDates();
        datePickerFragment.setDatePickerView(selectTime);
        DialogFragment newFragment = datePickerFragment;
        newFragment.show(getActivity().getSupportFragmentManager(), "DatePicker");

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.submit:
                try {
                    JSONObject jsonObject = new JSONObject();
                    JSONObject jsonObjectMain = new JSONObject();
                    jsonObject.put("description", description.getText().toString());
                    jsonObject.put("registration", regId.getText().toString());
                    jsonObject.put("residents", residents.getText().toString());
                    jsonObject.put("established", established.getText().toString());
                    jsonObjectMain.put("society", jsonObject);
                    updateSocietyDetails(jsonObjectMain);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                break;

            case R.id.mIageStartDate:
                openDatePicker(established);
                break;

        }
    }

}