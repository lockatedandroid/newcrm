package com.android.lockated.Interfaces;

import android.location.Location;
import android.os.Bundle;

import com.google.android.gms.common.ConnectionResult;

public interface ILocationUpdateListener
{
    public void onSuccessUpdate(Bundle bundle);
    public void onLocationUpdate(Location location);
    public void onFailedUpdate(ConnectionResult connectionResult);
}
