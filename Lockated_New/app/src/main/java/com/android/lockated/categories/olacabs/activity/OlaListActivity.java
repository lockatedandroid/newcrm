package com.android.lockated.categories.olacabs.activity;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.android.lockated.R;
import com.android.lockated.categories.olacabs.adapter.OlaBookAdapter;
import com.android.lockated.model.OlaCab.OlaDetailModel.ThirdpartyTransaction;
import com.android.lockated.network.ConnectionDetector;
import com.android.lockated.preferences.LockatedPreferences;
import com.android.lockated.request.LockatedRequestError;
import com.android.lockated.request.LockatedVolleyRequestQueue;
import com.android.lockated.utils.ApplicationURL;
import com.android.lockated.utils.Utilities;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class OlaListActivity extends AppCompatActivity implements Response.ErrorListener, Response.Listener<JSONObject> {

    RecyclerView recyclerView;
    ProgressBar progressBar;
    TextView errorMsg;
    ArrayList<ThirdpartyTransaction> thirdpartyTransactions;
    OlaBookAdapter olaBookAdapter;
    private LockatedPreferences mLockatedPreferences;
    private static final String GET_REQUEST_GET_TAG = "GET ALL TRANSACTION";
    private String screenName = "OlaListActivity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ola_list);
        Toolbar toolbar = (Toolbar) findViewById(R.id.custom_toolbar);
        setSupportActionBar(toolbar);

        Utilities.ladooIntegration(OlaListActivity.this, screenName);
        Utilities.lockatedGoogleAnalytics(screenName, getString(R.string.visited), screenName);

        getSupportActionBar().setTitle("Bookings");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(true);

        progressBar = (ProgressBar) findViewById(R.id.mProgressBarView);
        thirdpartyTransactions = new ArrayList<>();
        mLockatedPreferences = new LockatedPreferences(this);
        init();
        getAllZomatoOrder();
    }

    public void init() {
        recyclerView = (RecyclerView) findViewById(R.id.mRecyclerView);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(mLayoutManager);
        //srecyclerView.setItemAnimator(new DefaultItemAnimator());
        olaBookAdapter = new OlaBookAdapter(OlaListActivity.this,thirdpartyTransactions);
        recyclerView.addItemDecoration(new DividerItemDecoration(OlaListActivity.this, LinearLayoutManager.VERTICAL));
        recyclerView.setAdapter(olaBookAdapter);
        errorMsg=(TextView)findViewById(R.id.errorMsg);
    }


    public void getAllZomatoOrder() {
        if (ConnectionDetector.isConnectedToInternet(this)) {
            progressBar.setVisibility(View.VISIBLE);
            recyclerView.setVisibility(View.GONE);
            Log.e("Lockated Token", mLockatedPreferences.getLockatedToken());
            String url = ApplicationURL.getAllOlaTransaction + mLockatedPreferences.getLockatedToken();
            //String url = "https://www.lockated.com/get_all_thirdparty_transactions.json?token=b06b9c80acb8e5b06cea63d4cc74a2297897a923cd2753cc";
            Log.e("url", url);
            LockatedVolleyRequestQueue lockatedVolleyRequestQueue = LockatedVolleyRequestQueue.getInstance(OlaListActivity.this);
            lockatedVolleyRequestQueue.sendGetRequestOlaCabs(GET_REQUEST_GET_TAG, Request.Method.GET, url, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    progressBar.setVisibility(View.GONE);
                    Log.e("Response ", response.toString());

                    JSONObject jsonObject;
                    try {
                        jsonObject = new JSONObject(response);
                        if (jsonObject.has("thirdparty_transactions") && jsonObject.getJSONArray("thirdparty_transactions").length() > 0) {
                            recyclerView.setVisibility(View.VISIBLE);
                            JSONArray jsonArray = jsonObject.getJSONArray("thirdparty_transactions");
                            thirdpartyTransactions.clear();
                            Gson gson = new Gson();
                            for (int i = 0; i < jsonArray.length(); i++)
                            {
                                    ThirdpartyTransaction thirdpartyTransaction = gson.fromJson(jsonArray.getJSONObject(i).toString(), ThirdpartyTransaction.class);
                                    thirdpartyTransactions.add(thirdpartyTransaction);
                            }

                            //olaBookAdapter = new OlaBookAdapter(OlaListActivity.this,thirdpartyTransactions);
                            //recyclerView.addItemDecoration(new DividerItemDecoration(OlaListActivity.this, LinearLayoutManager.VERTICAL));
                            //recyclerView.setAdapter(olaBookAdapter);
                        }
                        else if (jsonObject.has("thirdparty_transactions") && jsonObject.getJSONArray("thirdparty_transactions").length()==0)
                        {
                            recyclerView.setVisibility(View.GONE);
                            errorMsg.setVisibility(View.VISIBLE);
                            errorMsg.setText(R.string.no_data_error);
                        }

                        else
                        {
                            if (thirdpartyTransactions.size() > 0) {
                                thirdpartyTransactions.clear();
                            }
                        }
                        olaBookAdapter.notifyDataSetChanged();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }, this);
        } else {
            Utilities.showToastMessage(this, this.getResources().getString(R.string.internet_connection_error));
        }
    }

    @Override
    public void onErrorResponse(VolleyError error) {
        progressBar.setVisibility(View.GONE);
        LockatedRequestError.onRequestError(this, error);
    }

    @Override
    public void onResponse(JSONObject response) {
        Log.e("Response",""+response);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId())
        {
            case android.R.id.home: {
                onBackPressed();
                return true;
            }
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        finish();
    }
}
