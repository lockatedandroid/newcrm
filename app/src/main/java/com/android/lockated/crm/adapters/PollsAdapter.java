package com.android.lockated.crm.adapters;


import android.content.Context;
import android.content.Intent;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.lockated.R;
import com.android.lockated.crm.activity.CrmPollsListActivity;
import com.android.lockated.information.AccountController;
import com.android.lockated.model.AccountData;
import com.android.lockated.model.usermodel.Polls.AllPolls.AllPoll;
import com.android.lockated.model.usermodel.Polls.AllPolls.AllPollOption;
import com.android.lockated.network.ConnectionDetector;
import com.android.lockated.preferences.LockatedPreferences;
import com.android.lockated.request.LockatedRequestError;
import com.android.lockated.request.LockatedVolleyRequestQueue;
import com.android.lockated.utils.ApplicationURL;
import com.android.lockated.utils.Utilities;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class PollsAdapter extends RecyclerView.Adapter<PollsAdapter.PollsViewHolder>
        implements View.OnClickListener, Response.Listener<JSONObject>, Response.ErrorListener {

    private final ArrayList<AllPoll> allPolls;
    Context contextMain;
    JSONArray jsonArray;
    FragmentManager childFragmentManager;
    AccountController accountController;
    ArrayList<AccountData> datas;
    boolean alreadedVoted;

    public
    PollsAdapter(Context context, ArrayList<AllPoll> allPolls, FragmentManager childFragmentManager) {
        this.contextMain = context;
        this.allPolls = allPolls;
        this.childFragmentManager = childFragmentManager;
        accountController = AccountController.getInstance();
        datas = accountController.getmAccountDataList();
    }

    @Override
    public PollsViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(contextMain).inflate(R.layout.polls_parent_view, parent, false);
        PollsViewHolder pvh = new PollsViewHolder(v);
        return pvh;
    }

    @Override
    public void onBindViewHolder(PollsViewHolder holder, int position) {
        alreadedVoted = false;
        holder.subject.setText(allPolls.get(position).getSubject());
        if (allPolls.get(position).getAlluser() != null) {
            String name = allPolls.get(position).getAlluser().getFirstname()
                    + " " + allPolls.get(position).getAlluser().getLastname();
            holder.mPostedBy.setText(name);
        } else {
            holder.mPostedBy.setText("No Name");
        }
        if (allPolls.get(position).getFlat() != null) {

            holder.mFlatNo.setText(allPolls.get(position).getFlat());
        } else {
            holder.mFlatNo.setText("No Flat");
        }
        if (allPolls.get(position).getCreatedAt() != null) {
            holder.mCreatedAt.setText(Utilities.ddMonTimeFormat(allPolls.get(position).getCreatedAt()));
        } else {
            holder.mCreatedAt.setText("No Date");
        }
        if (allPolls.get(position).getStart() != null) {
            holder.mStartDateTime.setText(Utilities.ddMonTimeFormat(allPolls.get(position).getStart()));
        } else {
            holder.mStartDateTime.setText("No Date");
        }
        if (allPolls.get(position).getStart() != null) {
            holder.mEndDateTime.setText(Utilities.ddMonTimeFormat(allPolls.get(position).getEnd()));
        } else {
            holder.mEndDateTime.setText("No Date");
        }
        if (allPolls.get(position).getAllPollOptions().size() == 1) {
            holder.secondChoice.setVisibility(View.GONE);
            holder.thirdChoice.setVisibility(View.GONE);
            holder.fourthChoice.setVisibility(View.GONE);
            holder.secondVote.setVisibility(View.GONE);
            holder.thirdVote.setVisibility(View.GONE);
            holder.fourthVote.setVisibility(View.GONE);
            holder.secondOption.setVisibility(View.GONE);
            holder.thirdOption.setVisibility(View.GONE);
            holder.fourthOption.setVisibility(View.GONE);
            holder.firstVote.setText(allPolls.get(position).getAllPollOptions().get(0).getVoteShare() + "%");
            holder.firstChoice.setText(choiceNumber(contextMain, 1, allPolls.get(position).getAllPollOptions().
                    get(0).getName()));
            holder.firstOption.setText("1");
        } else if (allPolls.get(position).getAllPollOptions().size() == 2) {
            holder.thirdChoice.setVisibility(View.GONE);
            holder.fourthChoice.setVisibility(View.GONE);
            holder.thirdVote.setVisibility(View.GONE);
            holder.fourthVote.setVisibility(View.GONE);
            holder.thirdOption.setVisibility(View.GONE);
            holder.fourthOption.setVisibility(View.GONE);
            holder.firstVote.setText(allPolls.get(position).getAllPollOptions()
                    .get(0).getVoteShare() + "%");
            holder.secondVote.setText(allPolls.get(position).getAllPollOptions()
                    .get(1).getVoteShare() + "%");
            holder.firstChoice.setText(choiceNumber(contextMain, 1, allPolls.get(position).getAllPollOptions()
                    .get(0).getName()));
            holder.secondChoice.setText(choiceNumber(contextMain, 2, allPolls.get(position).getAllPollOptions()
                    .get(1).getName()));
            holder.firstOption.setText("1");
            holder.secondOption.setText("2");
        } else if (allPolls.get(position).getAllPollOptions().size() == 3) {
            holder.fourthChoice.setVisibility(View.GONE);
            holder.fourthVote.setVisibility(View.GONE);
            holder.fourthOption.setVisibility(View.GONE);
            holder.firstVote.setText(allPolls.get(position).getAllPollOptions()
                    .get(0).getVoteShare() + "%");
            holder.secondVote.setText(allPolls.get(position).getAllPollOptions()
                    .get(1).getVoteShare() + "%");
            holder.thirdVote.setText(allPolls.get(position).getAllPollOptions()
                    .get(2).getVoteShare() + "%");
            holder.firstChoice.setText(choiceNumber(contextMain, 1, allPolls.get(position).getAllPollOptions()
                    .get(0).getName()));
            holder.secondChoice.setText(choiceNumber(contextMain, 2, allPolls.get(position).getAllPollOptions()
                    .get(1).getName()));
            holder.thirdChoice.setText(choiceNumber(contextMain, 3, allPolls.get(position).getAllPollOptions()
                    .get(2).getName()));
            holder.firstOption.setText("1");
            holder.secondOption.setText("2");
            holder.thirdOption.setText("3");
        } else if (allPolls.get(position).getAllPollOptions().size() == 4) {
            holder.firstChoice.setText(choiceNumber(contextMain, 1, allPolls.get(position).getAllPollOptions()
                    .get(0).getName()));
            holder.secondChoice.setText(choiceNumber(contextMain, 2, allPolls.get(position).getAllPollOptions()
                    .get(1).getName()));
            holder.thirdChoice.setText(choiceNumber(contextMain, 3, allPolls.get(position).getAllPollOptions()
                    .get(2).getName()));
            holder.fourthChoice.setText(choiceNumber(contextMain, 4, allPolls.get(position).getAllPollOptions()
                    .get(3).getName()));

            holder.firstVote.setText(allPolls.get(position).getAllPollOptions()
                    .get(0).getVoteShare() + "%");
            holder.secondVote.setText(allPolls.get(position).getAllPollOptions()
                    .get(1).getVoteShare() + "%");
            holder.thirdVote.setText(allPolls.get(position).getAllPollOptions()
                    .get(2).getVoteShare() + "%");
            holder.fourthVote.setText(allPolls.get(position).getAllPollOptions()
                    .get(3).getVoteShare() + "%");

            holder.firstOption.setText("1");
            holder.secondOption.setText("2");
            holder.thirdOption.setText("3");
            holder.fourthOption.setText("4");
        } else if (allPolls.get(position).getAllPollOptions().size() == 0) {
            holder.secondChoice.setVisibility(View.GONE);
            holder.thirdChoice.setVisibility(View.GONE);
            holder.fourthChoice.setVisibility(View.GONE);
            holder.secondOption.setVisibility(View.GONE);
            holder.thirdOption.setVisibility(View.GONE);
            holder.fourthOption.setVisibility(View.GONE);
        }

        if (checkIfVoted(allPolls.get(position))) {
            holder.options.setVisibility(View.GONE);
            holder.voted.setVisibility(View.VISIBLE);
        }

        holder.firstOption.setOnClickListener(this);
        holder.secondOption.setOnClickListener(this);
        holder.thirdOption.setOnClickListener(this);
        holder.fourthOption.setOnClickListener(this);

        holder.firstOption.setTag(position);
        holder.secondOption.setTag(position);
        holder.thirdOption.setTag(position);
        holder.fourthOption.setTag(position);
    }

    @Override
    public int getItemCount() {
        return allPolls.size();
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }

    @Override
    public void onClick(View v) {
        int pos;
        switch (v.getId()) {
            case R.id.firstOption:
                pos = (int) v.getTag();
                voteForPoll(pos, 0);
                break;

            case R.id.secondOption:
                pos = (int) v.getTag();
                voteForPoll(pos, 1);
                break;

            case R.id.thirdOption:
                pos = (int) v.getTag();
                voteForPoll(pos, 2);
                break;

            case R.id.fourthOption:
                pos = (int) v.getTag();
                voteForPoll(pos, 3);
                break;
        }
    }

    public String choiceNumber(Context context, int number, String name) {
        if (number == 1)
            return context.getResources().getString(R.string.one) + name;
        else if (number == 2)
            return context.getResources().getString(R.string.two) + name;
        else if (number == 3)
            return context.getResources().getString(R.string.three) + name;
        else if (number == 4)
            return context.getResources().getString(R.string.four) + name;
        else
            return context.getResources().getString(R.string.one) + name;
    }

    private void voteForPoll(int pos, int optionPosition) {
        if (ConnectionDetector.isConnectedToInternet(contextMain)) {
            String REQUEST_FACILITIES = "PollOption";
            LockatedPreferences mLockatedPreferences = new LockatedPreferences(contextMain);
            String url = ApplicationURL.votePollUrl + mLockatedPreferences.getLockatedToken();
            LockatedVolleyRequestQueue mLockatedVolleyRequestQueue = LockatedVolleyRequestQueue.getInstance(contextMain);
            JSONObject jsonObject = new JSONObject();
            try {
                jsonObject.put("poll_id", allPolls.get(pos).getAllPollOptions().get(optionPosition).getPollId());
                jsonObject.put("poll_option_id", allPolls.get(pos).getAllPollOptions().get(optionPosition).getId());
            } catch (JSONException e) {
                e.printStackTrace();
            }
            mLockatedVolleyRequestQueue.sendRequest(REQUEST_FACILITIES, Request.Method.POST, url, jsonObject, this, this);
        } else {
            Utilities.showToastMessage(contextMain, contextMain.getResources().getString(R.string.internet_connection_error));
        }
    }

    ///////voted
    public boolean checkIfVoted(AllPoll allPoll) {
        if (allPoll.getAllPollOptions() != null) {
            if (allPoll.getAllPollOptions().size() > 0) {
                ArrayList<AllPollOption> allPollOptions = allPoll.getAllPollOptions();
                for (int j = 0; j < allPollOptions.size(); j++) {
                    if (!alreadedVoted) {
                        if (allPollOptions.get(j).getHasVoted() != null) {
                            alreadedVoted = Boolean.parseBoolean(allPollOptions.get(j).getHasVoted());
                        }
                    }
                }
            }
        }
        return alreadedVoted;
    }

    public class PollsViewHolder extends RecyclerView.ViewHolder {
        TextView subject, firstChoice, secondChoice, thirdChoice, fourthChoice, firstOption,
                secondOption, thirdOption, fourthOption, alreadyVotedText, firstVote, secondVote, thirdVote, fourthVote, mStartDateTime, mEndDateTime;
        LinearLayout options, voted;
        TextView mPostedBy, mFlatNo, mCreatedAt;

        public PollsViewHolder(View itemView) {
            super(itemView);
            firstVote = (TextView) itemView.findViewById(R.id.firstVote);
            secondVote = (TextView) itemView.findViewById(R.id.secondVote);
            thirdVote = (TextView) itemView.findViewById(R.id.thirdVote);
            fourthVote = (TextView) itemView.findViewById(R.id.fourthVote);
            mPostedBy = (TextView) itemView.findViewById(R.id.mPostedBy);
            mFlatNo = (TextView) itemView.findViewById(R.id.mFlatNo);
            mCreatedAt = (TextView) itemView.findViewById(R.id.mCreatedAt);
            subject = (TextView) itemView.findViewById(R.id.subjectText);
            firstChoice = (TextView) itemView.findViewById(R.id.firstChoice);
            secondChoice = (TextView) itemView.findViewById(R.id.secondChoice);
            thirdChoice = (TextView) itemView.findViewById(R.id.thirdChoice);
            fourthChoice = (TextView) itemView.findViewById(R.id.fourthChoice);
            firstOption = (TextView) itemView.findViewById(R.id.firstOption);
            secondOption = (TextView) itemView.findViewById(R.id.secondOption);
            thirdOption = (TextView) itemView.findViewById(R.id.thirdOption);
            fourthOption = (TextView) itemView.findViewById(R.id.fourthOption);
            alreadyVotedText = (TextView) itemView.findViewById(R.id.alreadyVoted);
            mStartDateTime = (TextView) itemView.findViewById(R.id.mStartDateTime);
            mEndDateTime = (TextView) itemView.findViewById(R.id.mEndDateTime);
            options = (LinearLayout) itemView.findViewById(R.id.optionsLinear);
            voted = (LinearLayout) itemView.findViewById(R.id.alreadyVotedLinear);
        }
    }

    @Override
    public void onResponse(JSONObject response) {
        if (response.has("id")) {
            Intent intent = new Intent(contextMain, CrmPollsListActivity.class);
            intent.putExtra("CrmPollsListActivity", 0);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            contextMain.startActivity(intent);
        }
    }

    @Override
    public void onErrorResponse(VolleyError error) {
        if (contextMain != null) {
            LockatedRequestError.onRequestError(contextMain, error);
        }
    }
/*
    public void viewCardDetail(JSONObject jsonObject) {

        Bundle args = new Bundle();
        try {
            args.putString("event_type", "Notice");
            args.putString("created_by", jsonObject.getString("id_user"));
            args.putString("subject", jsonObject.getString("notice_heading"));
            args.putString("description", jsonObject.getString("notice_text"));
            args.putString("created_at", jsonObject.getString("created_at"));
            args.putString("expire_time", jsonObject.getString("expire_time"));
            args.putString("username", jsonObject.getString("username"));
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }*/
}