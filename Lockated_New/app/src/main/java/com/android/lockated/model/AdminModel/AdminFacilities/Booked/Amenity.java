
package com.android.lockated.model.AdminModel.AdminFacilities.Booked;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Amenity {

    @SerializedName("id")
    @Expose
    private int id;
    @SerializedName("society_id")
    @Expose
    private int societyId;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("description")
    @Expose
    private String description;
    @SerializedName("chargeable")
    @Expose
    private int chargeable;
    @SerializedName("cost")
    @Expose
    private int cost;
    @SerializedName("cost_type")
    @Expose
    private String costType;
    @SerializedName("active")
    @Expose
    private int active;
    @SerializedName("created_at")
    @Expose
    private String createdAt;
    @SerializedName("updated_at")
    @Expose
    private String updatedAt;

    /**
     * 
     * @return
     *     The id
     */
    public int getId() {
        return id;
    }

    /**
     * 
     * @param id
     *     The id
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * 
     * @return
     *     The societyId
     */
    public int getSocietyId() {
        return societyId;
    }

    /**
     * 
     * @param societyId
     *     The society_id
     */
    public void setSocietyId(int societyId) {
        this.societyId = societyId;
    }

    /**
     * 
     * @return
     *     The name
     */
    public String getName() {
        return name;
    }

    /**
     * 
     * @param name
     *     The name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * 
     * @return
     *     The description
     */
    public String getDescription() {
        return description;
    }

    /**
     * 
     * @param description
     *     The description
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * 
     * @return
     *     The chargeable
     */
    public int getChargeable() {
        return chargeable;
    }

    /**
     * 
     * @param chargeable
     *     The chargeable
     */
    public void setChargeable(int chargeable) {
        this.chargeable = chargeable;
    }

    /**
     * 
     * @return
     *     The cost
     */
    public int getCost() {
        return cost;
    }

    /**
     * 
     * @param cost
     *     The cost
     */
    public void setCost(int cost) {
        this.cost = cost;
    }

    /**
     * 
     * @return
     *     The costType
     */
    public String getCostType() {
        return costType;
    }

    /**
     * 
     * @param costType
     *     The cost_type
     */
    public void setCostType(String costType) {
        this.costType = costType;
    }

    /**
     * 
     * @return
     *     The active
     */
    public int getActive() {
        return active;
    }

    /**
     * 
     * @param active
     *     The active
     */
    public void setActive(int active) {
        this.active = active;
    }

    /**
     * 
     * @return
     *     The createdAt
     */
    public String getCreatedAt() {
        return createdAt;
    }

    /**
     * 
     * @param createdAt
     *     The created_at
     */
    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    /**
     * 
     * @return
     *     The updatedAt
     */
    public String getUpdatedAt() {
        return updatedAt;
    }

    /**
     * 
     * @param updatedAt
     *     The updated_at
     */
    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

}
