
package com.android.lockated.model.usermodel.HelpDesk;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class Complaint {

    @SerializedName("id")
    @Expose
    private int id;
    @SerializedName("id_society")
    @Expose
    private int idSociety;
    @SerializedName("id_user")
    @Expose
    private int idUser;
    @SerializedName("heading")
    @Expose
    private String heading;
    @SerializedName("text")
    @Expose
    private String text;
    @SerializedName("active")
    @Expose
    private Object active;
    @SerializedName("action")
    @Expose
    private Object action;
    @SerializedName("IsDelete")
    @Expose
    private Object IsDelete;
    @SerializedName("flat_number")
    @Expose
    private int flatNumber;
    @SerializedName("issue_type")
    @Expose
    private String issueType;
    @SerializedName("issue_status")
    @Expose
    private Object issueStatus;
    @SerializedName("created_at")
    @Expose
    private String createdAt;
    @SerializedName("updated_at")
    @Expose
    private String updatedAt;
    @SerializedName("url")
    @Expose
    private String url;
    @SerializedName("category_type")
    @Expose
    private String categoryType;
    @SerializedName("updated_by")
    @Expose
    private Object updatedBy;
    @SerializedName("posted_by")
    @Expose
    private String postedBy;
    @SerializedName("documents")
    @Expose
    private ArrayList<Document> documents = new ArrayList<Document>();

    /**
     * @return The id
     */
    public int getId() {
        return id;
    }

    /**
     * @param id The id
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * @return The idSociety
     */
    public int getIdSociety() {
        return idSociety;
    }

    /**
     * @param idSociety The id_society
     */
    public void setIdSociety(int idSociety) {
        this.idSociety = idSociety;
    }

    /**
     * @return The idUser
     */
    public int getIdUser() {
        return idUser;
    }

    /**
     * @param idUser The id_user
     */
    public void setIdUser(int idUser) {
        this.idUser = idUser;
    }

    /**
     * @return The heading
     */
    public String getHeading() {
        return heading;
    }

    /**
     * @param heading The heading
     */
    public void setHeading(String heading) {
        this.heading = heading;
    }

    /**
     * @return The text
     */
    public String getText() {
        return text;
    }

    /**
     * @param text The text
     */
    public void setText(String text) {
        this.text = text;
    }

    /**
     * @return The active
     */
    public Object getActive() {
        return active;
    }

    /**
     * @param active The active
     */
    public void setActive(Object active) {
        this.active = active;
    }

    /**
     * @return The action
     */
    public Object getAction() {
        return action;
    }

    /**
     * @param action The action
     */
    public void setAction(Object action) {
        this.action = action;
    }

    /**
     * @return The IsDelete
     */
    public Object getIsDelete() {
        return IsDelete;
    }

    /**
     * @param IsDelete The IsDelete
     */
    public void setIsDelete(Object IsDelete) {
        this.IsDelete = IsDelete;
    }

    /**
     * @return The flatNumber
     */
    public int getFlatNumber() {
        return flatNumber;
    }

    /**
     * @param flatNumber The flat_number
     */
    public void setFlatNumber(int flatNumber) {
        this.flatNumber = flatNumber;
    }

    /**
     * @return The issueType
     */
    public String getIssueType() {
        return issueType;
    }

    /**
     * @param issueType The issue_type
     */
    public void setIssueType(String issueType) {
        this.issueType = issueType;
    }

    /**
     * @return The issueStatus
     */
    public Object getIssueStatus() {
        return issueStatus;
    }

    /**
     * @param issueStatus The issue_status
     */
    public void setIssueStatus(Object issueStatus) {
        this.issueStatus = issueStatus;
    }

    /**
     * @return The createdAt
     */
    public String getCreatedAt() {
        return createdAt;
    }

    /**
     * @param createdAt The created_at
     */
    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    /**
     * @return The updatedAt
     */
    public String getUpdatedAt() {
        return updatedAt;
    }

    /**
     * @param updatedAt The updated_at
     */
    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    /**
     * @return The url
     */
    public String getUrl() {
        return url;
    }

    /**
     * @param url The url
     */
    public void setUrl(String url) {
        this.url = url;
    }

    /**
     * @return The categoryType
     */
    public String getCategoryType() {
        return categoryType;
    }

    /**
     * @param categoryType The category_type
     */
    public void setCategoryType(String categoryType) {
        this.categoryType = categoryType;
    }

    /**
     * @return The updatedBy
     */
    public Object getUpdatedBy() {
        return updatedBy;
    }

    /**
     * @param updatedBy The updated_by
     */
    public void setUpdatedBy(Object updatedBy) {
        this.updatedBy = updatedBy;
    }

    /**
     * @return The postedBy
     */
    public String getPostedBy() {
        return postedBy;
    }

    /**
     * @param postedBy The posted_by
     */
    public void setPostedBy(String postedBy) {
        this.postedBy = postedBy;
    }

    public ArrayList<Document> getDocuments() {
        return documents;
    }

    public void setDocuments(ArrayList<Document> documents) {
        this.documents = documents;
    }
}
