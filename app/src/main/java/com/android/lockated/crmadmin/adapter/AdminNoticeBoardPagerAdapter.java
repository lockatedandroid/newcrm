package com.android.lockated.crmadmin.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.android.lockated.crmadmin.fragment.adminNotices.PendingNoticeFragment;
import com.android.lockated.crmadmin.fragment.adminNotices.PublishedNoticeFragment;


public class AdminNoticeBoardPagerAdapter extends FragmentPagerAdapter {

    int tabCount;
    FragmentManager fragmentManager;
    String[] nameList;

    public AdminNoticeBoardPagerAdapter(FragmentManager fm, int tabCount, String[] nameList) {
        super(fm);
        this.tabCount = tabCount;
        fragmentManager = fm;
        this.nameList = nameList;
    }

    @Override
    public Fragment getItem(int pos) {

        Fragment fragment;

        switch (pos) {

            case 0:
                fragment = new PendingNoticeFragment();
                break;
            case 1:
                fragment = new PublishedNoticeFragment();
                break;
            default:
                fragment = new PendingNoticeFragment();
                break;
        }
        return fragment;
    }

    @Override
    public int getCount() {
        return tabCount;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return super.getPageTitle(position);
    }

    @Override
    public int getItemPosition(Object object) {
        return super.getItemPosition(object);
    }

    @Override
    public long getItemId(int position) {
        return super.getItemId(position);
    }

}
